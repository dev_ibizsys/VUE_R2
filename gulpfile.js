const gulp = require("gulp");
const ts = require("gulp-typescript");  //typescript 文件编译
const ibizsys = ts.createProject("ibizsys.json");   //typescript 文件内容
const apputil = ts.createProject("apputil.json");   //typescript 文件内容
const activiti = ts.createProject("activiti.json");   //typescript 文件内容
const less = require('gulp-less');  //less 编译
const concat = require('gulp-concat'); // 合并插件

const rename = require('gulp-rename');  //重命名
const uglify = require('gulp-uglify');  //js 压缩
const cssnano = require('gulp-cssnano');    // CSS 压缩
const autoprefixer = require('gulp-autoprefixer');  // 添加 CSS 浏览器前缀
const sourcemaps = require('gulp-sourcemaps');  //确保本地已安装gulp-sourcemaps [cnpm install gulp-sourcemaps --save-dev]

const stripdebug = require('gulp-strip-debug');  // 去除控制台输入

// 编译activiti
gulp.task("translate_activiti_ts", function () {
    return activiti.src()
        .pipe(activiti())
        .js.pipe(gulp.dest("activiti/dist"));
});

// 合并编译后的activiti JavaScript文件
gulp.task('concat_activiti', function () {
    // 目录（顺序）
    const activiti = [
        'activiti/dist/util/ibiz-activiti-util.js',
        'activiti/dist/util/ibiz-xml-writer.js',
        'activiti/dist/interface/activiti-workflow-editor.js',
        'activiti/dist/service/work-flow-edit.service.js',
        'activiti/dist/control/ibiz-wf-design-field2.js',
        'activiti/dist/components/ibiz-activiti.component.js',
        'activiti/dist/components/ibiz-activiti-full-screen.component.js'
    ]
    return gulp.src(activiti)
        .pipe(concat('activiti.js'))
        .pipe(gulp.dest('activiti/dist'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(stripdebug())
        .pipe(gulp.dest('activiti/dist'));
});


// 合并资源依赖JS文件（非压缩版）
gulp.task('concat_assets', function () {
    const assets = [
        'assets/plugins/polyfill/polyfill.min.js',
        'assets/plugins/vue/vue.min.js',
        'assets/plugins/vue/vue-router.min.js',
        'assets/plugins/rxjs/rxjs.umd.min.js',
        'assets/plugins/axios/axios.min.js',
        'assets/plugins/iview/iview.min.js',
        'assets/plugins/element-ui/element-ui.js',
        'assets/plugins/echarts/echarts.min.js',
        'assets/plugins/cookie/js.cookie.min.js',
    ];
    return gulp.src(assets)
        .pipe(concat('assets.js'))
        .pipe(gulp.dest('assets/dist'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest('assets/dist'));
});

// 合并资源依赖JS文件（非压缩版）
gulp.task('concat_assets2', function () {
    const assets2 = [
        'assets/plugins/ag-grid/ag-grid-enterprise/dist/ag-grid-enterprise.noStyle.js',
        'assets/plugins/ag-grid/ag-grid-vue2/vueComponentFactory.js',
        'assets/plugins/ag-grid/ag-grid-vue2/vueFrameworkFactory.js',
        'assets/plugins/ag-grid/ag-grid-vue2/vueFrameworkComponentWrapper.js',
        'assets/plugins/ag-grid/ag-grid-vue2/agGridVue.min.js',
    ];
    return gulp.src(assets2)
        .pipe(concat('assets2.js'))
        .pipe(gulp.dest('assets/dist'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest('assets/dist'));
});

gulp.task('translate_ibizsys_less', function () {
    return gulp.src('./src/ibizsys/css/**/*.less')
        .pipe(autoprefixer('last 6 version'))   // 添加 CSS 浏览器前缀，兼容最新的5个版本
        .pipe(less())   //编译
        .pipe(sourcemaps.init())    //资源地图创建
        .pipe(sourcemaps.write())   //资源地图写入
        .pipe(gulp.dest('ibizsys/dist/css'))    //输出未压缩的css文件
        .pipe(rename({ suffix: '.min' }))   // 对管道里的文件流添加 .min 的重命名
        .pipe(cssnano())    // 压缩 CSS目录下，此时每个文件都有压缩（*.min.css）和未压缩(*.css)两个版本
        .pipe(gulp.dest('ibizsys/dist/css'))    // 输出到 dist/css 目录下（不影响此时管道里的文件流）
});

gulp.task("translate_apputil_ts", function () {
    return apputil.src()
        .pipe(apputil())
        .js.pipe(stripdebug())
        .pipe(gulp.dest("apputil/dist"));
});


gulp.task("translate_ibizsys_ts", function () {
    return ibizsys.src()
        .pipe(ibizsys())
        .js.pipe(gulp.dest("ibizsys/dist"));
});

// 合并资源依赖JS文件（非压缩版）
gulp.task('concat_ibizsys2', function () {
    const ibizsys2 = [
        'ibizsys/dist/widget/ibiz-data-grid2.js',
        'ibizsys/dist/components/ibiz-ag-grid-paging/ibiz-ag-grid-paging.component.js',
        'ibizsys/dist/components/ibiz-grid-datepicker/ibiz-grid-datepicker.component.js',
        'ibizsys/dist/components/ibiz-grid-timepicker/ibiz-grid-timepicker.component.js',
        'ibizsys/dist/components/ibiz-grid-span/ibiz-grid-span.component.js',
        'ibizsys/dist/components/ibiz-grid-input/ibiz-grid-input.component.js',
        'ibizsys/dist/components/ibiz-grid-select/ibiz-grid-select.component.js',
        'ibizsys/dist/components/ibiz-grid-picker/ibiz-grid-picker.component.js',
        'ibizsys/dist/components/ibiz-costom-text-column-filter/ibiz-costom-text-column-filter.component.js',
        'ibizsys/dist/components/ibiz-custom-select-column-filter/ibiz-custom-select-column-filter.component.js',
    ];
    return gulp.src(ibizsys2)
        .pipe(concat('ibizsys2.js'))
        .pipe(gulp.dest('ibizsys/dist'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(gulp.dest('ibizsys/dist'));
});

// 合并编译后的ibizsys JavaScript文件（非压缩版）
gulp.task('concat_ibizsys', function () {
    // 目录（顺序）
    const ibizsys = [
        // app
        'ibizsys/dist/util/ibiz-app.js',
        // util
        'ibizsys/dist/util/ibiz-interceptor.js',
        'ibizsys/dist/util/ibiz-util.js',
        'ibizsys/dist/util/ibiz-http.js',
        'ibizsys/dist/util/ibiz-auth-guard.js',
        'ibizsys/dist/util/ibiz-notification.js',
        // 基础文件
        'ibizsys/dist/ibiz-object.js',
        'ibizsys/dist/util/ibiz-code-list.js',
        'ibizsys/dist/util/ibiz-ui-counter.js',
        // 表单界面部件
        'ibizsys/dist/formitem/ibiz-form-item.js',
        'ibizsys/dist/formitem/ibiz-form-field.js',
        'ibizsys/dist/formitem/ibiz-form-drpanel.js',
        'ibizsys/dist/formitem/ibiz-form-group.js',
        'ibizsys/dist/formitem/ibiz-form-iframe.js',
        'ibizsys/dist/formitem/ibiz-form-raw-item.js',
        'ibizsys/dist/formitem/ibiz-form-tab-page.js',
        'ibizsys/dist/formitem/ibiz-form-tab-panel.js',
        'ibizsys/dist/formitem/ibiz-form-button.js',
        // 部件
        'ibizsys/dist/widget/ibiz-control.js',
        'ibizsys/dist/widget/ibiz-app-menu.js',
        'ibizsys/dist/widget/ibiz-md-control.js',
        'ibizsys/dist/widget/ibiz-data-view.js',
        'ibizsys/dist/widget/ibiz-data-grid.js',
        'ibizsys/dist/widget/ibiz-list.js',
        'ibizsys/dist/widget/ibiz-toolbar.js',
        'ibizsys/dist/widget/ibiz-form.js',
        'ibizsys/dist/widget/ibiz-edit-form.js',
        'ibizsys/dist/widget/ibiz-search-form.js',
        'ibizsys/dist/widget/ibiz-tab.js',
        'ibizsys/dist/widget/ibiz-exp-tab.js',
        'ibizsys/dist/widget/ibiz-dr-tab.js',
        'ibizsys/dist/widget/ibiz-view-panel.js',
        'ibizsys/dist/widget/ibiz-pickup-view-panel.js',
        'ibizsys/dist/widget/ibiz-tree.js',
        'ibizsys/dist/widget/ibiz-tree-exp-bar.js',
        'ibizsys/dist/widget/ibiz-wf-exp-bar.js',
        'ibizsys/dist/widget/ibiz-mpickup-result.js',
        'ibizsys/dist/widget/ibiz-chart.js',
        'ibizsys/dist/widget/ibiz-dr-bar.js',
        'ibizsys/dist/widget/ibiz-custom.js',
        'ibizsys/dist/widget/ibiz-dashboard.js',
        'ibizsys/dist/widget/ibiz-report-panel.js',
        'ibizsys/dist/widget/ibiz-wizard-panel.js',
        'ibizsys/dist/widget/ibiz-multi-edit-view-panel.js',
        'ibizsys/dist/widget/ibiz-time-line.js',
        // 控制器
        'ibizsys/dist/app/ibiz-view-controller-base.js',
        'ibizsys/dist/app/ibiz-view-controller.js',
        'ibizsys/dist/app/ibiz-main-view-controller.js',
        'ibizsys/dist/app/ibiz-html-view-controller.js',
        'ibizsys/dist/app/ibiz-redirect-view-controller.js',
        'ibizsys/dist/app/ibiz-custom-view-controller.js',
        'ibizsys/dist/app/ibiz-index-view-controller.js',
        'ibizsys/dist/app/ibiz-md-view-controller.js',
        'ibizsys/dist/app/ibiz-data-view-controller.js',
        'ibizsys/dist/app/ibiz-pickup-data-view-controller.js',
        'ibizsys/dist/app/ibiz-grid-view-controller.js',
        'ibizsys/dist/app/ibiz-grid-view2-controller.js',
        'ibizsys/dist/app/ibiz-grid-view9-controller.js',
        'ibizsys/dist/app/ibiz-medit-view9-controller.js',
        'ibizsys/dist/app/ibiz-pickup-grid-view-controller.js',
        'ibizsys/dist/app/ibiz-edit-view-controller.js',
        'ibizsys/dist/app/ibiz-edit-view2-controller.js',
        'ibizsys/dist/app/ibiz-edit-view3-controller.js',
        'ibizsys/dist/app/ibiz-edit-view9-controller.js',
        'ibizsys/dist/app/ibiz-option-view-controller.js',
        'ibizsys/dist/app/ibiz-pickup-view-controller.js',
        'ibizsys/dist/app/ibiz-pickup-view2-controller.js',
        'ibizsys/dist/app/ibiz-mpickup-view-controller.js',
        'ibizsys/dist/app/ibiz-mpickup-view2-controller.js',
        'ibizsys/dist/app/ibiz-tree-exp-view-controller.js',
        'ibizsys/dist/app/ibiz-exp-view-controller.js',
        'ibizsys/dist/app/ibiz-entity-tab-exp-view-controller.js',
        'ibizsys/dist/app/ibiz-tree-view-controller.js',
        'ibizsys/dist/app/ibiz-pickup-tree-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-exp-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-edit-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-start-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-action-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-edit-view2-controller.js',
        'ibizsys/dist/app/ibiz-wf-edit-view3-controller.js',
        'ibizsys/dist/app/ibiz-search-view-controller.js',
        'ibizsys/dist/app/ibiz-chart-view-controller.js',
        'ibizsys/dist/app/ibiz-portal-view-controller.js',
        'ibizsys/dist/app/ibiz-report-view-controller.js',
        'ibizsys/dist/app/ibiz-wizard-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-step-actor-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-step-data-trace-chart-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-proxy-start-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-proxy-result-view-controller.js',
        'ibizsys/dist/app/ibiz-wf-proxy-data-view-controller.js',
        // 组件
        'ibizsys/dist/components/ibiz-app-menu/ibiz-app-menu.component.js',
        'ibizsys/dist/components/ibiz-form/ibiz-form.component.js',
        'ibizsys/dist/components/ibiz-form-group/ibiz-form-group.component.js',
        'ibizsys/dist/components/ibiz-form-item/ibiz-form-item.component.js',
        'ibizsys/dist/components/ibiz-form-editor/ibiz-form-editor.component.js',
        'ibizsys/dist/components/ibiz-exp-bar/ibiz-exp-bar.component.js',
        'ibizsys/dist/components/ibiz-modal/ibiz-modal.component.js',
        'ibizsys/dist/components/ibiz-picker/ibiz-picker.component.js',
        'ibizsys/dist/components/ibiz-check-list/ibiz-check-list.component.js',
        'ibizsys/dist/components/ibiz-file-upload/ibiz-file-upload.component.js',
        'ibizsys/dist/components/ibiz-mpicker/ibiz-mpicker.component.js',
        'ibizsys/dist/components/ibiz-picture-upload/ibiz-picture-upload.component.js',
        'ibizsys/dist/components/ibiz-rich-text-editor/ibiz-rich-text-editor.component.js',
        'ibizsys/dist/components/ibiz-autocomplete/ibiz-autocomplete.component.js',
        'ibizsys/dist/components/ibiz-echarts/ibiz-echarts.component.js',
        'ibizsys/dist/components/ibiz-importdata-view/ibiz-importdata-view.component.js',
        'ibizsys/dist/components/ibiz-card/ibiz-card.component.js',
        'ibizsys/dist/components/ibiz-tool-bar/ibiz-tool-bar.component.js',
        'ibizsys/dist/components/ibiz-grid-select-editor/ibiz-grid-select-editor.component.js',
        'ibizsys/dist/components/ibiz-grid-picker-editor/ibiz-grid-picker-editor.component.js',
        'ibizsys/dist/components/ibiz-group-menu/ibiz-group-menu.component.js',
        'ibizsys/dist/components/ibiz-picture-one-upload/ibiz-picture-one-upload.component.js',
        'ibizsys/dist/components/ibiz-range-picker/ibiz-daterange-picker.component.js',
    ]
    return gulp.src(ibizsys)
        .pipe(concat('ibizsys.js'))
        .pipe(gulp.dest('ibizsys/dist'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglify())
        .pipe(stripdebug())
        .pipe(gulp.dest('ibizsys/dist'));
});