/*
 * Activiti Modeler component part of the Activiti project
 * Copyright 2005-2014 Alfresco Software, Ltd. All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */
'use strict';

angular.module('activitiModeler')
	.controller('StencilController', ['$rootScope', '$scope', '$http', '$modal', '$timeout', function($rootScope, $scope, $http, $modal, $timeout) {

		//获取url中的参数
		$scope.GetQueryString = function(name) {
			var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
			var r = window.location.search.substr(1).match(reg);
			if(r != null) {
				return unescape(r[2]);
			}
			return null;
		}

		// 设置全屏编辑按钮是否启用
		var fsmode = $scope.GetQueryString('fsmode');
		if (fsmode === 'true') {
			$scope.$isFullScreen = fsmode;
		} else {
			$scope.$isFullScreen = false;
		}
		
		// 浏览器大小发生变化事件
		window.onresize = function() {
			$scope.treeHeight = document.documentElement.clientHeight - 59;
		}
		// 全屏按钮事件
		$scope.fullScreen = function() {
			if(typeof window.ibzOryxFullScree === 'function') {
				window.ibzOryxFullScree();
			}
		}

		// Property window toggle state
		$scope.propertyWindowState = {
			'collapsed': false
		};

		// Add reference to global header-config
		$scope.headerConfig = KISBPM.HEADER_CONFIG;

		$scope.propertyWindowState.toggle = function() {
			$scope.propertyWindowState.collapsed = !$scope.propertyWindowState.collapsed;
			$timeout(function() {
				jQuery(window).trigger('resize');
			});
		};

		// Code that is dependent on an initialised Editor is wrapped in a promise for the editor
		$scope.editorFactory.promise.then(function() {

			/* Build stencil item list */

			// Build simple json representation of stencil set
			var stencilItemGroups = [];

			// Helper method: find a group in an array
			var findGroup = function(name, groupArray) {
				for(var index = 0; index < groupArray.length; index++) {
					if(groupArray[index].name === name) {
						return groupArray[index];
					}
				}
				return null;
			};

			// Helper method: add a new group to an array of groups
			var addGroup = function(groupName, groupArray) {
				var group = {
					name: groupName,
					items: [],
					paletteItems: [],
					groups: [],
					visible: true
				};
				groupArray.push(group);
				return group;
			};

			var strStecli = "{\r\n" +
				"  \"title\" : \"流程编辑器\",\r\n" +
				"  \"namespace\" : \"http://b3mn.org/stencilset/bpmn2.0#\",\r\n" +
				"  \"description\" : \"BPMN流程编辑器\",\r\n" +
				"  \"propertyPackages\" : [ {\r\n" +
				"    \"name\" : \"process_idpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"process_id\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"流程标识\",\r\n" +
				"      \"value\" : \"process\",\r\n" +
				"      \"description\" : \"Unique identifier of the process definition.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"overrideidpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"overrideid\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"主键（ID）\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"流程唯一标识.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"namepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"name\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"名称\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"BPMN元素名称.\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : \"text_name\"\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"documentationpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"documentation\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"描述信息\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"BPMN元素描述.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"process_authorpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"process_author\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"流程作者\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"流程定义者姓名.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"process_versionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"process_version\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"流程版本\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"标识文档版本为目的.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"process_namespacepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"process_namespace\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"目标名称空间\",\r\n" +
				"      \"value\" : \"http://www.activiti.org/processdef\",\r\n" +
				"      \"description\" : \"工作流目标命名空间.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"asynchronousdefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"asynchronousdefinition\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"异步\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"定义为一个异步任务.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"exclusivedefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"exclusivedefinition\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"互斥任务\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"定义为一个互斥任务.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"executionlistenerspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"executionlisteners\",\r\n" +
				"      \"type\" : \"multiplecomplex\",\r\n" +
				"      \"title\" : \"执行监听器\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Listeners for an activity, process, sequence flow, start and end event.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"tasklistenerspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"tasklisteners\",\r\n" +
				"      \"type\" : \"multiplecomplex\",\r\n" +
				"      \"title\" : \"任务监听器\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"监听用户任务.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"eventlistenerspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"eventlisteners\",\r\n" +
				"      \"type\" : \"multiplecomplex\",\r\n" +
				"      \"title\" : \"事件监听器\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Listeners for any event happening in the Activiti Engine. It's also possible to rethrow the event as a signal, message or error event\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"usertaskassignmentpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"usertaskassignment\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"分配用户\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"分配任务给用户\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"formpropertiespackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"formproperties\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"表单属性\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"定义表单属性\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"formkeydefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"formkeydefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"表单编号\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"用户任务表单编号.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"duedatedefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"duedatedefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"到期时间\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"用户任务到期时间.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"prioritydefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"prioritydefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"优先级\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"用户任务的优先级.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"servicetaskclasspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"servicetaskclass\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"类\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Class that implements the service task logic.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"servicetaskexpressionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"servicetaskexpression\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"表达式\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"服务任务 logic defined with an expression.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"servicetaskdelegateexpressionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"servicetaskdelegateexpression\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"委托表达式\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"服务任务 logic defined with a delegate expression.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"servicetaskfieldspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"servicetaskfields\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"类字段\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Field extensions\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"servicetaskresultvariablepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"servicetaskresultvariable\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"结果变量名\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Process variable name to store the service task result.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"scriptformatpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"scriptformat\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"脚本格式\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Script format of the script task.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"scripttextpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"scripttext\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"脚本\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Script text of the script task.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"ruletask_rulespackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"ruletask_rules\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"规则\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Rules of the rule task.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"ruletask_variables_inputpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"ruletask_variables_input\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"输入变量\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Input variables of the rule task.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"ruletask_excludepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"ruletask_exclude\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"排除\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"Use the rules property as exclusion.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"ruletask_resultpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"ruletask_result\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"结果变量\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Result variable of the rule task.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtasktopackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtaskto\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"至\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The recipients if the e-mail. Multiple recipients are defined in a comma-separated list.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtaskfrompackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtaskfrom\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"表单\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The sender e-mail address. If not provided, the default configured from address is used.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtasksubjectpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtasksubject\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"主题\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The subject of the e-mail.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtaskccpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtaskcc\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"抄送\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The cc's of the e-mail. Multiple recipients are defined in a comma-separated list\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtaskbccpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtaskbcc\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"隐藏抄送\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The bcc's of the e-mail. Multiple recipients are defined in a comma-separated list\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtasktextpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtasktext\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"文本\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The content of the e-mail, in case one needs to send plain none-rich e-mails. Can be used in combination with html, for e-mail clients that don't support rich content. The client will then fall back to this text-only alternative.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtaskhtmlpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtaskhtml\",\r\n" +
				"      \"type\" : \"Text\",\r\n" +
				"      \"title\" : \"Html\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"A piece of HTML that is the content of the e-mail.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"mailtaskcharsetpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"mailtaskcharset\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"字符集(编码格式)\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"修改邮件字符集，是许多除英语之外的语言所必须的. \",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"callactivitycalledelementpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"callactivitycalledelement\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"调用元素\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"流程引用.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"callactivityinparameterspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"callactivityinparameters\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"输入参数\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Definition of the input parameters\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"callactivityoutparameterspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"callactivityoutparameters\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"输出参数\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Definition of the output parameters\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"cameltaskcamelcontextpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"cameltaskcamelcontext\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"Camel context\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"An optional camel context definition, if left empty the default is used.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"muletaskendpointurlpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"muletaskendpointurl\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"Endpoint url\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"A required endpoint url to sent the message to Mule.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"muletasklanguagepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"muletasklanguage\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"语言\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"A required definition for the language to resolve the payload expression, like juel.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"muletaskpayloadexpressionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"muletaskpayloadexpression\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"Payload expression\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"A required definition for the payload of the message sent to Mule.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"muletaskresultvariablepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"muletaskresultvariable\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"Result variable\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"An optional result variable for the payload returned.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"conditionsequenceflowpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"conditionsequenceflow\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"流条件\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The condition of the sequence flow\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"defaultflowpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"defaultflow\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"默认流\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"Define the sequence flow as default\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : \"default\"\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"conditionalflowpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"conditionalflow\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"条件流\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"Define the sequence flow with a condition\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"timercycledefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"timercycledefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"时间周期(e.g. R3/PT10H)\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the timer with a ISO-8601 cycle.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"timerdatedefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"timerdatedefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"采用ISO-8601日期时间\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the timer with a ISO-8601 date definition.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"timerdurationdefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"timerdurationdefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"持续时间(e.g. PT5M)\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the timer with a ISO-8601 duration.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"timerenddatedefinitionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"timerenddatedefinition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"Time End Date in ISO-8601\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the timer with a ISO-8601 duration.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"messagerefpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"messageref\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"消息引用\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the message name.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"signalrefpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"signalref\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"信号引用\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"定义信号名称.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"errorrefpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"errorref\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"错误引用\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"定义错误名称.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"cancelactivitypackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"cancelactivity\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"取消任务\",\r\n" +
				"      \"value\" : \"true\",\r\n" +
				"      \"description\" : \"Should the activity be cancelled\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : [ \"frame\", \"frame2\" ]\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"initiatorpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"initiator\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"启动器\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Initiator of the process.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"textpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"text\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"文本\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"The text of the text annotation.\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : \"text\"\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"multiinstance_typepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"multiinstance_type\",\r\n" +
				"      \"type\" : \"kisbpm-multiinstance\",\r\n" +
				"      \"title\" : \"多实例类型\",\r\n" +
				"      \"value\" : \"None\",\r\n" +
				"      \"description\" : \"Repeated activity execution (parallel or sequential) can be displayed through different loop types\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : \"multiinstance\"\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"multiinstance_cardinalitypackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"multiinstance_cardinality\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"基数（多实例）\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the cardinality of multi instance.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"multiinstance_collectionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"multiinstance_collection\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"集合（多实例）\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the collection for the multi instance.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"multiinstance_variablepackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"multiinstance_variable\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"元素变量（多实例）\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the element variable for the multi instance.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"multiinstance_conditionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"multiinstance_condition\",\r\n" +
				"      \"type\" : \"String\",\r\n" +
				"      \"title\" : \"完成条件（多实例）\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Define the completion condition for the multi instance.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"isforcompensationpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"isforcompensation\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"是否补偿\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"A flag that identifies whether this activity is intended for the purposes of compensation.\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : \"compensation\"\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"sequencefloworderpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"sequencefloworder\",\r\n" +
				"      \"type\" : \"Complex\",\r\n" +
				"      \"title\" : \"Flow order\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Order outgoing sequence flows.\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"signaldefinitionspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"signaldefinitions\",\r\n" +
				"      \"type\" : \"multiplecomplex\",\r\n" +
				"      \"title\" : \"信号定义\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Signal definitions\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"messagedefinitionspackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"messagedefinitions\",\r\n" +
				"      \"type\" : \"multiplecomplex\",\r\n" +
				"      \"title\" : \"消息定义\",\r\n" +
				"      \"value\" : \"\",\r\n" +
				"      \"description\" : \"Message definitions\",\r\n" +
				"      \"popular\" : true\r\n" +
				"    } ]\r\n" +
				"  }, {\r\n" +
				"    \"name\" : \"istransactionpackage\",\r\n" +
				"    \"properties\" : [ {\r\n" +
				"      \"id\" : \"istransaction\",\r\n" +
				"      \"type\" : \"Boolean\",\r\n" +
				"      \"title\" : \"Is a transaction sub process\",\r\n" +
				"      \"value\" : \"false\",\r\n" +
				"      \"description\" : \"A flag that identifies whether this sub process is of type transaction.\",\r\n" +
				"      \"popular\" : true,\r\n" +
				"      \"refToView\" : \"border\"\r\n" +
				"    } ]\r\n" +
				"  } ],\r\n" +
				"  \"stencils\" : [ {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BPMNDiagram\",\r\n" +
				"    \"title\" : \"BPMN-Diagram\",\r\n" +
				"    \"description\" : \"A BPMN 2.0 diagram.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n   width=\\\"800\\\"\\n   height=\\\"600\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <g pointer-events=\\\"fill\\\" >\\n    <polygon stroke=\\\"black\\\" fill=\\\"black\\\" stroke-width=\\\"1\\\" points=\\\"0,0 0,590 9,599 799,599 799,9 790,0\\\" stroke-linecap=\\\"butt\\\" stroke-linejoin=\\\"miter\\\" stroke-miterlimit=\\\"10\\\" />\\n    <rect id=\\\"diagramcanvas\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"790\\\" height=\\\"590\\\" stroke=\\\"black\\\" stroke-width=\\\"2\\\" fill=\\\"white\\\" />\\n    \\t<text font-size=\\\"22\\\" id=\\\"diagramtext\\\" x=\\\"400\\\" y=\\\"25\\\" oryx:align=\\\"top center\\\" stroke=\\\"#373e48\\\"></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"diagram.png\",\r\n" +
				"    \"groups\" : [ \"Diagram\" ],\r\n" +
				"    \"mayBeRoot\" : true,\r\n" +
				"    \"hide\" : true,\r\n" +
				"    \"propertyPackages\" : [ \"process_idpackage\", \"namepackage\", \"documentationpackage\", \"process_authorpackage\", \"process_versionpackage\", \"process_namespacepackage\", \"executionlistenerspackage\", \"eventlistenerspackage\", \"signaldefinitionspackage\", \"messagedefinitionspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"StartNoneEvent\",\r\n" +
				"    \"title\" : \"开始事件（*）\",\r\n" +
				"    \"description\" : \"A start event without a specific trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"startevent/none.png\",\r\n" +
				"    \"groups\" : [ \"开始事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"initiatorpackage\", \"formkeydefinitionpackage\", \"formpropertiespackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"Startevents_all\", \"StartEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"StartTimerEvent\",\r\n" +
				"    \"title\" : \"定时开始事件\",\r\n" +
				"    \"description\" : \"有定时任务触发器的开始事件\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path id=\\\"path1\\\" transform=\\\"translate(6,6)\\\"\\n    \\td=\\\"M 10 0 C 4.4771525 0 0 4.4771525 0 10 C 0 15.522847 4.4771525 20 10 20 C 15.522847 20 20 15.522847 20 10 C 20 4.4771525 15.522847 1.1842379e-15 10 0 z M 9.09375 1.03125 C 9.2292164 1.0174926 9.362825 1.0389311 9.5 1.03125 L 9.5 3.5 L 10.5 3.5 L 10.5 1.03125 C 15.063526 1.2867831 18.713217 4.9364738 18.96875 9.5 L 16.5 9.5 L 16.5 10.5 L 18.96875 10.5 C 18.713217 15.063526 15.063526 18.713217 10.5 18.96875 L 10.5 16.5 L 9.5 16.5 L 9.5 18.96875 C 4.9364738 18.713217 1.2867831 15.063526 1.03125 10.5 L 3.5 10.5 L 3.5 9.5 L 1.03125 9.5 C 1.279102 5.0736488 4.7225326 1.4751713 9.09375 1.03125 z M 9.5 5 L 9.5 8.0625 C 8.6373007 8.2844627 8 9.0680195 8 10 C 8 11.104569 8.8954305 12 10 12 C 10.931981 12 11.715537 11.362699 11.9375 10.5 L 14 10.5 L 14 9.5 L 11.9375 9.5 C 11.756642 8.7970599 11.20294 8.2433585 10.5 8.0625 L 10.5 5 L 9.5 5 z \\\"  \\n    \\tfill=\\\"#585858\\\" stroke=\\\"none\\\" />\\n   \\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"startevent/timer.png\",\r\n" +
				"    \"groups\" : [ \"开始事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"timercycledefinitionpackage\", \"timerdatedefinitionpackage\", \"timerdurationdefinitionpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"Startevents_all\", \"StartEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"StartSignalEvent\",\r\n" +
				"    \"title\" : \"信号开始事件\",\r\n" +
				"    \"description\" : \"有信号触发器的开始事件.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <path\\n       d=\\\"M 8.7124971,21.247342 L 23.333334,21.247342 L 16.022915,8.5759512 L 8.7124971,21.247342 z\\\"\\n       id=\\\"triangle\\\"\\n       stroke=\\\"#585858\\\"\\n       style=\\\"fill:none;stroke-width:1.4;stroke-miterlimit:4;stroke-dasharray:none\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"startevent/signal.png\",\r\n" +
				"    \"groups\" : [ \"开始事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"signalrefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"Startevents_all\", \"StartEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"StartMessageEvent\",\r\n" +
				"    \"title\" : \"消息开始事件\",\r\n" +
				"    \"description\" : \"有消息触发器的开始事件.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path transform=\\\"translate(7,7)\\\" id=\\\"path1\\\" stroke=\\\"none\\\" fill=\\\"#585858\\\" stroke-width=\\\"1\\\" d=\\\"m 0.5,2.5 0,13 17,0 0,-13 z M 2,4 6.5,8.5 2,13 z M 4,4 14,4 9,9 z m 12,0 0,9 -4.5,-4.5 z M 7.5,9.5 9,11 10.5,9.5 15,14 3,14 z\\\"/>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"startevent/message.png\",\r\n" +
				"    \"groups\" : [ \"开始事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"messagerefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"Startevents_all\", \"StartEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"StartErrorEvent\",\r\n" +
				"    \"title\" : \"错误开始事件\",\r\n" +
				"    \"description\" : \"捕获抛出BMP错误的开始事件.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path\\n         stroke=\\\"#585858\\\"\\n         style=\\\"fill:none;stroke-width:1.5;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10\\\"\\n         d=\\\"M 22.820839,11.171502 L 19.36734,24.58992 L 13.54138,14.281819 L 9.3386512,20.071607 L 13.048949,6.8323057 L 18.996148,16.132659 L 22.820839,11.171502 z\\\"\\n         id=\\\"errorPolygon\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"startevent/error.png\",\r\n" +
				"    \"groups\" : [ \"开始事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"errorrefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"Startevents_all\", \"StartEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"UserTask\",\r\n" +
				"    \"title\" : \"用户任务\",\r\n" +
				"    \"description\" : \"由特定用户完成的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n\\t\\n\\t<g id=\\\"userTask\\\" transform=\\\"translate(3,3)\\\">\\n\\t\\t<path oryx:anchors=\\\"top left\\\"\\n       \\t\\tstyle=\\\"fill:#d1b575;stroke:none;\\\"\\n       \\t\\t d=\\\"m 1,17 16,0 0,-1.7778 -5.333332,-3.5555 0,-1.7778 c 1.244444,0 1.244444,-2.3111 1.244444,-2.3111 l 0,-3.0222 C 12.555557,0.8221 9.0000001,1.0001 9.0000001,1.0001 c 0,0 -3.5555556,-0.178 -3.9111111,3.5555 l 0,3.0222 c 0,0 0,2.3111 1.2444443,2.3111 l 0,1.7778 L 1,15.2222 1,17 17,17\\\" \\n         />\\n\\t\\t\\n\\t</g>\\n  \\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\t\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.user.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\", \"usertaskassignmentpackage\", \"formkeydefinitionpackage\", \"duedatedefinitionpackage\", \"prioritydefinitionpackage\", \"formpropertiespackage\", \"tasklistenerspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ServiceTask\",\r\n" +
				"    \"title\" : \"服务任务\",\r\n" +
				"    \"description\" : \"由服务逻辑自动完成的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n\\t\\n\\t<g id=\\\"serviceTask\\\" transform=\\\"translate(3,3)\\\">\\n\\t<path oryx:anchors=\\\"top left\\\"\\n\\t\\tstyle=\\\"fill:#72a7d0;stroke:none\\\"\\n     d=\\\"M 8,1 7.5,2.875 c 0,0 -0.02438,0.250763 -0.40625,0.4375 C 7.05724,3.330353 7.04387,3.358818 7,3.375 6.6676654,3.4929791 6.3336971,3.6092802 6.03125,3.78125 6.02349,3.78566 6.007733,3.77681 6,3.78125 5.8811373,3.761018 5.8125,3.71875 5.8125,3.71875 l -1.6875,-1 -1.40625,1.4375 0.96875,1.65625 c 0,0 0.065705,0.068637 0.09375,0.1875 0.002,0.00849 -0.00169,0.022138 0,0.03125 C 3.6092802,6.3336971 3.4929791,6.6676654 3.375,7 3.3629836,7.0338489 3.3239228,7.0596246 3.3125,7.09375 3.125763,7.4756184 2.875,7.5 2.875,7.5 L 1,8 l 0,2 1.875,0.5 c 0,0 0.250763,0.02438 0.4375,0.40625 0.017853,0.03651 0.046318,0.04988 0.0625,0.09375 0.1129372,0.318132 0.2124732,0.646641 0.375,0.9375 -0.00302,0.215512 -0.09375,0.34375 -0.09375,0.34375 L 2.6875,13.9375 4.09375,15.34375 5.78125,14.375 c 0,0 0.1229911,-0.09744 0.34375,-0.09375 0.2720511,0.147787 0.5795915,0.23888 0.875,0.34375 0.033849,0.01202 0.059625,0.05108 0.09375,0.0625 C 7.4756199,14.874237 7.5,15.125 7.5,15.125 L 8,17 l 2,0 0.5,-1.875 c 0,0 0.02438,-0.250763 0.40625,-0.4375 0.03651,-0.01785 0.04988,-0.04632 0.09375,-0.0625 0.332335,-0.117979 0.666303,-0.23428 0.96875,-0.40625 0.177303,0.0173 0.28125,0.09375 0.28125,0.09375 l 1.65625,0.96875 1.40625,-1.40625 -0.96875,-1.65625 c 0,0 -0.07645,-0.103947 -0.09375,-0.28125 0.162527,-0.290859 0.262063,-0.619368 0.375,-0.9375 0.01618,-0.04387 0.04465,-0.05724 0.0625,-0.09375 C 14.874237,10.52438 15.125,10.5 15.125,10.5 L 17,10 17,8 15.125,7.5 c 0,0 -0.250763,-0.024382 -0.4375,-0.40625 C 14.669647,7.0572406 14.641181,7.0438697 14.625,7 14.55912,6.8144282 14.520616,6.6141566 14.4375,6.4375 c -0.224363,-0.4866 0,-0.71875 0,-0.71875 L 15.40625,4.0625 14,2.625 l -1.65625,1 c 0,0 -0.253337,0.1695664 -0.71875,-0.03125 l -0.03125,0 C 11.405359,3.5035185 11.198648,3.4455201 11,3.375 10.95613,3.3588185 10.942759,3.3303534 10.90625,3.3125 10.524382,3.125763 10.5,2.875 10.5,2.875 L 10,1 8,1 z m 1,5 c 1.656854,0 3,1.3431458 3,3 0,1.656854 -1.343146,3 -3,3 C 7.3431458,12 6,10.656854 6,9 6,7.3431458 7.3431458,6 9,6 z\\\" />\\n\\t</g>\\n  \\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\t\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.service.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\", \"servicetaskclasspackage\", \"servicetaskexpressionpackage\", \"servicetaskdelegateexpressionpackage\", \"servicetaskfieldspackage\", \"servicetaskresultvariablepackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ScriptTask\",\r\n" +
				"    \"title\" : \"脚本任务\",\r\n" +
				"    \"description\" : \"由脚本逻辑自动完成的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n\\t\\n\\t<g id=\\\"scriptTask\\\" transform=\\\"translate(2,2)\\\">\\n\\t\\t<path oryx:anchors=\\\"top left\\\"\\n\\t\\t\\td=\\\"m 5,2 0,0.094 c 0.23706,0.064 0.53189,0.1645 0.8125,0.375 0.5582,0.4186 1.05109,1.228 1.15625,2.5312 l 8.03125,0 1,0 1,0 c 0,-3 -2,-3 -2,-3 l -10,0 z M 4,3 4,13 2,13 c 0,3 2,3 2,3 l 9,0 c 0,0 2,0 2,-3 L 15,6 6,6 6,5.5 C 6,4.1111 5.5595,3.529 5.1875,3.25 4.8155,2.971 4.5,3 4.5,3 L 4,3 z\\\"\\n     \\t\\tstyle=\\\"fill:#72a7d0;stroke:none\\\"\\n\\t\\t/>\\n\\t</g>\\n  \\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\t\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.script.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"scriptformatpackage\", \"scripttextpackage\", \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BusinessRule\",\r\n" +
				"    \"title\" : \"业务规则任务\",\r\n" +
				"    \"description\" : \"由规则逻辑自动完成的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n  \\t<defs>\\n\\t\\t<radialGradient id=\\\"background\\\" cx=\\\"10%\\\" cy=\\\"10%\\\" r=\\\"100%\\\" fx=\\\"10%\\\" fy=\\\"10%\\\">\\n\\t\\t\\t<stop offset=\\\"0%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"1\\\"/>\\n\\t\\t\\t<stop id=\\\"fill_el\\\" offset=\\\"100%\\\" stop-color=\\\"#ffffcc\\\" stop-opacity=\\\"1\\\"/>\\n\\t\\t</radialGradient>\\n\\t</defs>\\n\\t\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    \\n\\t<g id=\\\"businessRuleTask\\\" transform=\\\"translate(4,3)\\\">\\n\\t\\t<path oryx:anchors=\\\"top left\\\" \\n\\t\\t\\t d=\\\"m 1,2 0,14 16,0 0,-14 z m 1.45458,5.6000386 2.90906,0 0,2.7999224 -2.90906,0 z m 4.36364,0 8.72718,0 0,2.7999224 -8.72718,0 z m -4.36364,4.1998844 2.90906,0 0,2.800116 -2.90906,0 z m 4.36364,0 8.72718,0 0,2.800116 -8.72718,0 z\\\"\\n     \\t\\tstyle=\\\"fill:#72a7d0;stroke:none\\\"\\n\\t\\t/>\\n\\t</g>\\n\\t\\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.business.rule.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\", \"ruletask_rulespackage\", \"ruletask_variables_inputpackage\", \"ruletask_excludepackage\", \"ruletask_resultpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ReceiveTask\",\r\n" +
				"    \"title\" : \"接收任务\",\r\n" +
				"    \"description\" : \"等待接收信号的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    \\n\\t<g id=\\\"receiveTask\\\" transform=\\\"translate(4,3)\\\">\\n\\t\\t<path oryx:anchors=\\\"left top\\\" \\n\\t\\t\\t style=\\\"fill:#16964d;stroke:none;\\\"\\n     \\t\\t d=\\\"m 0.5,2.5 0,13 17,0 0,-13 z M 2,4 6.5,8.5 2,13 z M 4,4 14,4 9,9 z m 12,0 0,9 -4.5,-4.5 z M 7.5,9.5 9,11 10.5,9.5 15,14 3,14 z\\\"\\n\\t\\t />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.receive.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ManualTask\",\r\n" +
				"    \"title\" : \"人工任务\",\r\n" +
				"    \"description\" : \"无需逻辑自动完成的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    <g id=\\\"manualTask\\\" transform=\\\"translate(3,1)\\\">\\n    \\t<path oryx:anchors=\\\"top left\\\"\\n    \\t\\tstyle=\\\"fill:#d1b575;stroke=none\\\"\\n     \\t\\td=\\\"m 17,9.3290326 c -0.0069,0.5512461 -0.455166,1.0455894 -0.940778,1.0376604 l -5.792746,0 c 0.0053,0.119381 0.0026,0.237107 0.0061,0.355965 l 5.154918,0 c 0.482032,-0.0096 0.925529,0.49051 0.919525,1.037574 -0.0078,0.537128 -0.446283,1.017531 -0.919521,1.007683 l -5.245273,0 c -0.01507,0.104484 -0.03389,0.204081 -0.05316,0.301591 l 2.630175,0 c 0.454137,-0.0096 0.872112,0.461754 0.866386,0.977186 C 13.619526,14.554106 13.206293,15.009498 12.75924,15 L 3.7753054,15 C 3.6045812,15 3.433552,14.94423 3.2916363,14.837136 c -0.00174,0 -0.00436,0 -0.00609,0 C 1.7212035,14.367801 0.99998255,11.458641 1,11.458641 L 1,7.4588393 c 0,0 0.6623144,-1.316333 1.8390583,-2.0872584 1.1767614,-0.7711868 6.8053358,-2.40497 7.2587847,-2.8052901 0.453484,-0.40032 1.660213,1.4859942 0.04775,2.4010487 C 8.5332315,5.882394 8.507351,5.7996113 8.4370292,5.7936859 l 6.3569748,-0.00871 c 0.497046,-0.00958 0.952273,0.5097676 0.94612,1.0738232 -0.0053,0.556126 -0.456176,1.0566566 -0.94612,1.0496854 l -4.72435,0 c 0.01307,0.1149374 0.0244,0.2281319 0.03721,0.3498661 l 5.952195,0 c 0.494517,-0.00871 0.947906,0.5066305 0.940795,1.0679848 z\\\"\\n    \\t/>\\n\\t</g>\\n\\t\\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.manual.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"MailTask\",\r\n" +
				"    \"title\" : \"邮件任务\",\r\n" +
				"    \"description\" : \"发送邮件通知的任务.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    \\n\\t<g id=\\\"sendTask\\\" transform=\\\"translate(4,3)\\\">\\n\\t\\n\\t<!-- path here -->\\n\\t\\t<path oryx:anchors=\\\"top left\\\"\\n\\t\\t\\tstyle=\\\"fill:#16964d;stroke:none;\\\"\\n     \\t\\td=\\\"M 1 3 L 9 11 L 17 3 L 1 3 z M 1 5 L 1 13 L 5 9 L 1 5 z M 17 5 L 13 9 L 17 13 L 17 5 z M 6 10 L 1 15 L 17 15 L 12 10 L 9 13 L 6 10 z \\\"\\n     \\t/>\\n\\t</g>\\n\\t\\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.send.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\", \"mailtasktopackage\", \"mailtaskfrompackage\", \"mailtasksubjectpackage\", \"mailtaskccpackage\", \"mailtaskbccpackage\", \"mailtasktextpackage\", \"mailtaskhtmlpackage\", \"mailtaskcharsetpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"CamelTask\",\r\n" +
				"    \"title\" : \"骆驼任务\",\r\n" +
				"    \"description\" : \"An task that sends a message to Camel\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n\\t\\n\\t<g id=\\\"camelTask\\\" transform=\\\"translate(4,4)\\\">\\n\\t\\t<path\\n     style=\\\"fill:#bd4848;fill-opacity:1\\\"\\n     d=\\\"m 8.1878027,15.383782 c -0.824818,-0.3427 0.375093,-1.1925 0.404055,-1.7743 0.230509,-0.8159 -0.217173,-1.5329 -0.550642,-2.2283 -0.106244,-0.5273 -0.03299,-1.8886005 -0.747194,-1.7818005 -0.712355,0.3776 -0.9225,1.2309005 -1.253911,1.9055005 -0.175574,1.0874 -0.630353,2.114 -0.775834,3.2123 -0.244009,0.4224 -1.741203,0.3888 -1.554386,-0.1397 0.651324,-0.3302 1.13227,-0.9222 1.180246,-1.6705 0.0082,-0.7042 -0.133578,-1.3681 0.302178,-2.0083 0.08617,-0.3202 0.356348,-1.0224005 -0.218996,-0.8051 -0.694517,0.2372 -1.651062,0.6128 -2.057645,-0.2959005 -0.696769,0.3057005 -1.102947,-0.611 -1.393127,-1.0565 -0.231079,-0.6218 -0.437041,-1.3041 -0.202103,-1.9476 -0.185217,-0.7514 -0.39751099,-1.5209 -0.35214999,-2.301 -0.243425,-0.7796 0.86000899,-1.2456 0.08581,-1.8855 -0.76078999,0.1964 -1.41630099,-0.7569 -0.79351899,-1.2877 0.58743,-0.52829998 1.49031699,-0.242 2.09856399,-0.77049998 0.816875,-0.3212 1.256619,0.65019998 1.923119,0.71939998 0.01194,0.7333 -0.0031,1.5042 -0.18417,2.2232 -0.194069,0.564 -0.811196,1.6968 0.06669,1.9398 0.738382,-0.173 1.095723,-0.9364 1.659041,-1.3729 0.727298,-0.3962 1.093982,-1.117 1.344137,-1.8675 0.400558,-0.8287 1.697676,-0.6854 1.955367,0.1758 0.103564,0.5511 0.9073983,1.7538 1.2472763,0.6846 0.121868,-0.6687 0.785541,-1.4454 1.518183,-1.0431 0.813587,0.4875 0.658233,1.6033 1.285504,2.2454 0.768715,0.8117 1.745394,1.4801 2.196633,2.5469 0.313781,0.8074 0.568552,1.707 0.496624,2.5733 -0.35485,0.8576005 -1.224508,-0.216 -0.64725,-0.7284 0.01868,-0.3794 -0.01834,-1.3264 -0.370249,-1.3272 -0.123187,0.7586 -0.152778,1.547 -0.10869,2.3154 0.270285,0.6662005 1.310741,0.7653005 1.060553,1.6763005 -0.03493,0.9801 0.294343,1.9505 0.148048,2.9272 -0.320479,0.2406 -0.79575,0.097 -1.185062,0.1512 -0.165725,0.3657 -0.40138,0.921 -1.020848,0.6744 -0.564671,0.1141 -1.246404,-0.266 -0.578559,-0.7715 0.679736,-0.5602 0.898618,-1.5362 0.687058,-2.3673 -0.529674,-1.108 -1.275984,-2.0954005 -1.839206,-3.1831005 -0.634619,-0.1004 -1.251945,0.6779 -1.956789,0.7408 -0.6065893,-0.038 -1.0354363,-0.06 -0.8495673,0.6969005 0.01681,0.711 0.152396,1.3997 0.157345,2.1104 0.07947,0.7464 0.171287,1.4944 0.238271,2.2351 0.237411,1.0076 -0.687542,1.1488 -1.414811,0.8598 z m 6.8675483,-1.8379 c 0.114364,-0.3658 0.206751,-1.2704 -0.114466,-1.3553 -0.152626,0.5835 -0.225018,1.1888 -0.227537,1.7919 0.147087,-0.1166 0.265559,-0.2643 0.342003,-0.4366 z\\\"\\n     />\\n\\t</g>\\n  \\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\t\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.camel.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\", \"cameltaskcamelcontextpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"MuleTask\",\r\n" +
				"    \"title\" : \"Mule任务\",\r\n" +
				"    \"description\" : \"An task that sends a message to Mule\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n\\t\\n\\t<g id=\\\"muleTask\\\" transform=\\\"translate(4,4)\\\">\\n\\t\\t<path\\n     style=\\\"fill:#bd4848;fill-opacity:1\\\"\\n     d=\\\"M 8,0 C 3.581722,0 0,3.5817 0,8 c 0,4.4183 3.581722,8 8,8 4.418278,0 8,-3.5817 8,-8 L 16,7.6562 C 15.813571,3.3775 12.282847,0 8,0 z M 5.1875,2.7812 8,7.3437 10.8125,2.7812 c 1.323522,0.4299 2.329453,1.5645 2.8125,2.8438 1.136151,2.8609 -0.380702,6.4569 -3.25,7.5937 -0.217837,-0.6102 -0.438416,-1.2022 -0.65625,-1.8125 0.701032,-0.2274 1.313373,-0.6949 1.71875,-1.3125 0.73624,-1.2317 0.939877,-2.6305 -0.03125,-4.3125 l -2.75,4.0625 -0.65625,0 -0.65625,0 -2.75,-4 C 3.5268433,7.6916 3.82626,8.862 4.5625,10.0937 4.967877,10.7113 5.580218,11.1788 6.28125,11.4062 6.063416,12.0165 5.842837,12.6085 5.625,13.2187 2.755702,12.0819 1.238849,8.4858 2.375,5.625 2.858047,4.3457 3.863978,3.2112 5.1875,2.7812 z\\\"\\n     />\\n\\t</g>\\n  \\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\t\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.mule.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\", \"muletaskendpointurlpackage\", \"muletasklanguagepackage\", \"muletaskpayloadexpressionpackage\", \"muletaskresultvariablepackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"SendTask\",\r\n" +
				"    \"title\" : \"发送任务\",\r\n" +
				"    \"description\" : \"An task that sends a message\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    \\n\\t<g id=\\\"sendTask\\\" transform=\\\"translate(4,3)\\\">\\n\\t\\n\\t<!-- path here -->\\n\\t\\t<path oryx:anchors=\\\"top left\\\"\\n\\t\\t\\tstyle=\\\"fill:#16964d;stroke:none;\\\"\\n     \\t\\td=\\\"M 1 3 L 9 11 L 17 3 L 1 3 z M 1 5 L 1 13 L 5 9 L 1 5 z M 17 5 L 13 9 L 17 13 L 17 5 z M 6 10 L 1 15 L 17 15 L 12 10 L 9 13 L 6 10 z \\\"\\n     \\t/>\\n\\t</g>\\n\\t\\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/list/type.send.png\",\r\n" +
				"    \"groups\" : [ \"任务\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"ActivitiesMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"SubProcess\",\r\n" +
				"    \"title\" : \"子流程\",\r\n" +
				"    \"description\" : \"子流程范围\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n   width=\\\"200\\\"\\n   height=\\\"160\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"50\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"80\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"110\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"70\\\" oryx:cy=\\\"159\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"100\\\" oryx:cy=\\\"159\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"130\\\" oryx:cy=\\\"159\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"199\\\" oryx:cy=\\\"50\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"199\\\" oryx:cy=\\\"80\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"199\\\" oryx:cy=\\\"110\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"70\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"100\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"130\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"100\\\" oryx:cy=\\\"80\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"120 100\\\" oryx:maximumSize=\\\"\\\" >\\n    <rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"190\\\" height=\\\"160\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"200\\\" height=\\\"160\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#ffffff\\\" />\\n\\t<text \\n\\t\\tfont-size=\\\"12\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"8\\\" \\n\\t\\ty=\\\"10\\\" \\n\\t\\toryx:align=\\\"top left\\\"\\n\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\toryx:anchors=\\\"left top\\\" \\n\\t\\tstroke=\\\"#373e48\\\">\\n\\t</text>\\n\\t\\n\\t<g \\tid=\\\"parallel\\\"\\n\\t\\ttransform=\\\"translate(1)\\\">\\n\\t\\t<path \\n\\t\\t\\tid=\\\"parallelpath\\\"\\n\\t\\t\\toryx:anchors=\\\"bottom\\\" \\n\\t\\t\\tfill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M96 145 v10 M100 145 v10 M104 145 v10\\\" \\n\\t\\t\\tstroke-width=\\\"2\\\"\\n\\t\\t/>\\n\\t</g>\\n\\t<g \\tid=\\\"sequential\\\"\\n\\t\\ttransform=\\\"translate(1)\\\">\\n\\t\\t<path \\n\\t\\t\\tid=\\\"sequentialpath\\\"\\n\\t\\t\\toryx:anchors=\\\"bottom\\\" \\n\\t\\t\\tfill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M95,154h10 M95,150h10 M95,146h10\\\"\\n\\t\\t/>\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/expanded.subprocess.png\",\r\n" +
				"    \"groups\" : [ \"结构\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"EventSubProcess\",\r\n" +
				"    \"title\" : \"事件子流程\",\r\n" +
				"    \"description\" : \"事件周日子流程范围\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n   width=\\\"200\\\"\\n   height=\\\"160\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"0\\\" oryx:cy=\\\"80\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"100\\\" oryx:cy=\\\"160\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"200\\\" oryx:cy=\\\"80\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"100\\\" oryx:cy=\\\"0\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"100\\\" oryx:cy=\\\"80\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"120 100\\\" oryx:maximumSize=\\\"\\\" >\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"190\\\" height=\\\"160\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:anchors=\\\"bottom top right left\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"200\\\" height=\\\"160\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" stroke-dasharray=\\\"2,2,2\\\" fill=\\\"#ffffff\\\" />\\n    \\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"8\\\" \\n\\t\\t\\ty=\\\"10\\\" \\n\\t\\t\\toryx:align=\\\"top left\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\toryx:anchors=\\\"left top\\\" \\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    \\t\\n\\t<g id=\\\"none\\\"></g>\\n\\t\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/event.subprocess.png\",\r\n" +
				"    \"groups\" : [ \"结构\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"CallActivity\",\r\n" +
				"    \"title\" : \"调用活动\",\r\n" +
				"    \"description\" : \"A call activity\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n\\n   width=\\\"102\\\"\\n   height=\\\"82\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"1\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"left\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"79\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"20\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"40\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"99\\\" oryx:cy=\\\"60\\\" oryx:anchors=\\\"right\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"25\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"75\\\" oryx:cy=\\\"1\\\" oryx:anchors=\\\"top\\\" />\\n  \\t\\n  \\t<oryx:magnet oryx:cx=\\\"50\\\" oryx:cy=\\\"40\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\" oryx:minimumSize=\\\"50 40\\\">\\n\\t<rect id=\\\"text_frame\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"1\\\" y=\\\"1\\\" width=\\\"94\\\" height=\\\"79\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"none\\\" stroke-width=\\\"0\\\" fill=\\\"none\\\" />\\n    <rect oryx:resize=\\\"vertical horizontal\\\" oryx:anchors=\\\"bottom top right left\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"4\\\" fill=\\\"none\\\" />\\n\\t<rect id=\\\"bg_frame\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"100\\\" height=\\\"80\\\" rx=\\\"10\\\" ry=\\\"10\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"1\\\" fill=\\\"#f9f9f9\\\" />\\n\\t\\t<text \\n\\t\\t\\tfont-size=\\\"12\\\" \\n\\t\\t\\tid=\\\"text_name\\\" \\n\\t\\t\\tx=\\\"50\\\" \\n\\t\\t\\ty=\\\"40\\\" \\n\\t\\t\\toryx:align=\\\"middle center\\\"\\n\\t\\t\\toryx:fittoelem=\\\"text_frame\\\"\\n\\t\\t\\tstroke=\\\"#373e48\\\">\\n\\t\\t</text>\\n    \\n\\t<g id=\\\"parallel\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M46 70 v8 M50 70 v8 M54 70 v8\\\" stroke-width=\\\"2\\\" />\\n\\t</g>\\n\\t\\n\\t<g id=\\\"sequential\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" stroke-width=\\\"2\\\" d=\\\"M46,76h10M46,72h10 M46,68h10\\\"/>\\n\\t</g>\\n\\n\\t<g id=\\\"compensation\\\">\\n\\t\\t<path oryx:anchors=\\\"bottom\\\" fill=\\\"none\\\" stroke=\\\"#bbbbbb\\\" d=\\\"M 62 74 L 66 70 L 66 78 L 62 74 L 62 70 L 58 74 L 62 78 L 62 74\\\" stroke-width=\\\"1\\\" />\\n\\t</g>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"activity/task.png\",\r\n" +
				"    \"groups\" : [ \"结构\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"asynchronousdefinitionpackage\", \"exclusivedefinitionpackage\", \"executionlistenerspackage\", \"callactivitycalledelementpackage\", \"callactivityinparameterspackage\", \"callactivityoutparameterspackage\", \"multiinstance_typepackage\", \"multiinstance_cardinalitypackage\", \"multiinstance_collectionpackage\", \"multiinstance_variablepackage\", \"multiinstance_conditionpackage\", \"isforcompensationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"Activity\", \"sequence_start\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ExclusiveGateway\",\r\n" +
				"    \"title\" : \"互斥网关\",\r\n" +
				"    \"description\" : \"一个选择的网关\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   version=\\\"1.0\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\">\\n  <defs\\n     id=\\\"defs4\\\" />\\n  <oryx:magnets>\\n    <oryx:magnet\\n       oryx:default=\\\"yes\\\"\\n       oryx:cy=\\\"16\\\"\\n       oryx:cx=\\\"16\\\" />\\n  </oryx:magnets>\\t\\t\\t\\t\\t\\n  <g>\\n  \\n    <path\\n       d=\\\"M -4.5,16 L 16,-4.5 L 35.5,16 L 16,35.5z\\\"\\n       id=\\\"bg_frame\\\"\\n       fill=\\\"#ffffff\\\"\\n       stroke=\\\"#585858\\\"\\n       style=\\\"stroke-width:1\\\" />\\n    <g\\n       id=\\\"cross\\\">\\n      <path\\n      \\tid=\\\"crosspath\\\"\\n      \\tstroke=\\\"#585858\\\"\\n      \\tfill=\\\"#585858\\\"\\n        d=\\\"M 8.75,7.55 L 12.75,7.55 L 23.15,24.45 L 19.25,24.45 z\\\"\\n        style=\\\"stroke-width:1\\\" />\\n      <path\\n      \\tid=\\\"crosspath2\\\"\\n      \\tstroke=\\\"#585858\\\"\\n      \\tfill=\\\"#585858\\\"\\n        d=\\\"M 8.75,24.45 L 19.25,7.55 L 23.15,7.55 L 12.75,24.45 z\\\"\\n        style=\\\"stroke-width:1\\\" />\\n    </g>\\n\\t\\n\\t<text id=\\\"text_name\\\" x=\\\"26\\\" y=\\\"26\\\" oryx:align=\\\"left top\\\"/>\\n\\t\\n  </g>\\n</svg>\\n\",\r\n" +
				"    \"icon\" : \"gateway/exclusive.databased.png\",\r\n" +
				"    \"groups\" : [ \"网关\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"sequencefloworderpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"GatewaysMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ParallelGateway\",\r\n" +
				"    \"title\" : \"并行网关\",\r\n" +
				"    \"description\" : \"并行处理的网关\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   version=\\\"1.0\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\">\\n   \\n  <oryx:magnets>\\n    <oryx:magnet\\n       oryx:default=\\\"yes\\\"\\n       oryx:cy=\\\"16\\\"\\n       oryx:cx=\\\"16\\\" />\\n  </oryx:magnets>\\n  <g>\\n    <path\\n       d=\\\"M -4.5,16 L 16,-4.5 L 35.5,16 L 16,35.5z\\\"\\n       id=\\\"bg_frame\\\"\\n       fill=\\\"#ffffff\\\"\\n       stroke=\\\"#585858\\\"\\n       style=\\\"stroke-width:1\\\" />\\n    <path\\n       d=\\\"M 6.75,16 L 25.75,16 M 16,6.75 L 16,25.75\\\"\\n       id=\\\"path9\\\"\\n       stroke=\\\"#585858\\\"\\n       style=\\\"fill:none;stroke-width:3\\\" />\\n    \\n\\t<text id=\\\"text_name\\\" x=\\\"26\\\" y=\\\"26\\\" oryx:align=\\\"left top\\\"/>\\n\\t\\n  </g>\\n</svg>\\n\",\r\n" +
				"    \"icon\" : \"gateway/parallel.png\",\r\n" +
				"    \"groups\" : [ \"网关\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"sequencefloworderpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"GatewaysMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"InclusiveGateway\",\r\n" +
				"    \"title\" : \"包容性网关\",\r\n" +
				"    \"description\" : \"An inclusive gateway\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   version=\\\"1.0\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\">\\n  <oryx:magnets>\\n    <oryx:magnet\\n       oryx:default=\\\"yes\\\"\\n       oryx:cy=\\\"16\\\"\\n       oryx:cx=\\\"16\\\" />\\n  </oryx:magnets>\\n  <g>\\n\\n    <path\\n       d=\\\"M -4.5,16 L 16,-4.5 L 35.5,16 L 16,35.5z\\\"\\n       id=\\\"bg_frame\\\"\\n       fill=\\\"#ffffff\\\"\\n       stroke=\\\"#585858\\\"\\n       style=\\\"stroke-width:1\\\" />\\n    <circle\\n    \\tid=\\\"circle\\\"\\n    \\tstroke=\\\"#585858\\\"\\n\\t\\tcx=\\\"16\\\"\\n\\t\\tcy=\\\"16\\\"\\n\\t\\tr=\\\"9.75\\\"\\n\\t\\tstyle=\\\"fill:none;stroke-width:2.5\\\" />\\n    \\n\\t<text id=\\\"text_name\\\" x=\\\"26\\\" y=\\\"26\\\" oryx:align=\\\"left top\\\"/>\\n\\t\\n  </g>\\n</svg>\\n\",\r\n" +
				"    \"icon\" : \"gateway/inclusive.png\",\r\n" +
				"    \"groups\" : [ \"网关\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"sequencefloworderpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"GatewaysMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"EventGateway\",\r\n" +
				"    \"title\" : \"事件网关\",\r\n" +
				"    \"description\" : \"An event gateway\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   version=\\\"1.0\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\">\\n  <oryx:magnets>\\n    <oryx:magnet\\n       oryx:default=\\\"yes\\\"\\n       oryx:cy=\\\"16\\\"\\n       oryx:cx=\\\"16\\\" />\\n  </oryx:magnets>\\n  \\n  <g> \\n  \\t\\n\\t<path\\n\\t\\td=\\\"M -4.5,16 L 16,-4.5 L 35.5,16 L 16,35.5z\\\"\\n\\t\\tid=\\\"bg_frame\\\"\\n\\t\\tfill=\\\"#ffffff\\\"\\n\\t\\tstroke=\\\"#585858\\\"\\n\\t\\tstyle=\\\"stroke-width:1\\\" />\\n\\t<circle\\n\\t\\tid=\\\"circle\\\"\\n\\t\\tcx=\\\"16\\\"\\n\\t\\tcy=\\\"16\\\"\\n\\t\\tr=\\\"10.4\\\"\\n\\t\\tstroke=\\\"#585858\\\"\\n\\t\\tstyle=\\\"fill:none;stroke-width:0.5\\\" />\\n\\t<circle\\n\\t\\tid=\\\"circle2\\\"\\n\\t\\tcx=\\\"16\\\"\\n\\t\\tcy=\\\"16\\\"\\n\\t\\tr=\\\"11.7\\\"\\n\\t\\tstroke=\\\"#585858\\\"\\n\\t\\tstyle=\\\"fill:none;stroke-width:0.5\\\" />\\n\\t<path\\n\\t\\td=\\\"M 20.327514,22.344972 L 11.259248,22.344216 L 8.4577203,13.719549 L 15.794545,8.389969 L 23.130481,13.720774 L 20.327514,22.344972 z\\\"\\n\\t\\tid=\\\"middlePolygon\\\"\\n\\t\\tstroke=\\\"#585858\\\"\\n\\t\\tstyle=\\\"fill:none;fill-opacity:1;stroke-width:1.39999998;stroke-linejoin:bevel;stroke-opacity:1\\\" />\\n\\t\\n\\t\\n\\t<g id=\\\"instantiate\\\">\\n\\t\\n\\t\\t<path\\n\\t\\t\\td=\\\"M -4.5,16 L 16,-4.5 L 35.5,16 L 16,35.5z\\\"\\n\\t\\t\\tid=\\\"bg_frame2\\\"\\n\\t\\t\\tfill=\\\"#ffffff\\\"\\n\\t\\t\\tstroke=\\\"#585858\\\"\\n\\t\\t\\tstyle=\\\"stroke-width:1\\\" />\\n\\t\\t<circle\\n\\t\\t\\tid=\\\"circle3\\\"\\n\\t\\t\\tcx=\\\"16\\\"\\n\\t\\t\\tcy=\\\"16\\\"\\n\\t\\t\\tr=\\\"11\\\"\\n\\t\\t\\tstroke=\\\"#585858\\\"\\n\\t\\t\\tstyle=\\\"fill:none;stroke-width:1\\\" />\\n\\t\\t<path\\n\\t\\t\\td=\\\"M 20.327514,22.344972 L 11.259248,22.344216 L 8.4577203,13.719549 L 15.794545,8.389969 L 23.130481,13.720774 L 20.327514,22.344972 z\\\"\\n\\t\\t\\tid=\\\"middlePolygon2\\\"\\n\\t\\t\\tstroke=\\\"#585858\\\"\\n\\t\\t\\tstyle=\\\"fill:none;fill-opacity:1;stroke-width:1.39999998;stroke-linejoin:bevel;stroke-opacity:1\\\" />\\n\\t\\n\\t\\n\\t\\t<g id=\\\"parallel\\\">\\n\\t\\t\\t<path\\n\\t\\t\\t\\td=\\\"M -4.5,16 L 16,-4.5 L 35.5,16 L 16,35.5z\\\"\\n\\t\\t\\t\\tid=\\\"bg_frame3\\\"\\n\\t\\t\\t\\tfill=\\\"#ffffff\\\"\\n\\t\\t\\t\\tstroke=\\\"#585858\\\"\\n\\t\\t\\t\\tstyle=\\\"stroke-width:1\\\" />\\n\\t\\t\\t<circle id=\\\"frame5\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n\\t\\t\\t\\n\\t\\t\\t<path\\n\\t\\t\\t\\td=\\\"M 6.75,14 L6.75,18 L14,18 L14,24.75 L18,24.75 L18,18 L24.75,18 L24.75,14 L18,14 L18,6.75 L14,6.75 L14,14z\\\"\\n\\t\\t\\t\\tid=\\\"path92\\\"\\n\\t\\t\\t\\tstroke=\\\"#585858\\\"\\n\\t\\t\\t\\tstyle=\\\"fill:none;stroke-width:1\\\" />\\n\\t\\t\\n\\t\\t</g>\\n\\t\\n\\t</g>\\n\\t\\n\\t<text id=\\\"text_name\\\" x=\\\"26\\\" y=\\\"26\\\" oryx:align=\\\"left top\\\"/>\\n\\t\\n  </g>\\t\\n\\t\\n</svg>\\n\",\r\n" +
				"    \"icon\" : \"gateway/eventbased.png\",\r\n" +
				"    \"groups\" : [ \"网关\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"sequencefloworderpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"GatewaysMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BoundaryErrorEvent\",\r\n" +
				"    \"title\" : \"边界错误事件\",\r\n" +
				"    \"description\" : \"A boundary event that catches a BPMN error\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path\\n         stroke=\\\"#585858\\\"\\n         style=\\\"fill:none;stroke-width:1.5;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10\\\"\\n         d=\\\"M 22.820839,11.171502 L 19.36734,24.58992 L 13.54138,14.281819 L 9.3386512,20.071607 L 13.048949,6.8323057 L 18.996148,16.132659 L 22.820839,11.171502 z\\\"\\n         id=\\\"errorPolygon\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/error.png\",\r\n" +
				"    \"groups\" : [ \"边界事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"errorrefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"BoundaryEventsMorph\", \"IntermediateEventOnActivityBoundary\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BoundaryTimerEvent\",\r\n" +
				"    \"title\" : \"边界定时事件\",\r\n" +
				"    \"description\" : \"A boundary event with a timer trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\t\\n    <circle \\n    \\tid=\\\"frame2_non_interrupting\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"12\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"none\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 4.5, 3\\\" />\\n    \\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame2\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path id=\\\"path1\\\" transform=\\\"translate(6,6)\\\"\\n    \\td=\\\"M 10 0 C 4.4771525 0 0 4.4771525 0 10 C 0 15.522847 4.4771525 20 10 20 C 15.522847 20 20 15.522847 20 10 C 20 4.4771525 15.522847 1.1842379e-15 10 0 z M 9.09375 1.03125 C 9.2292164 1.0174926 9.362825 1.0389311 9.5 1.03125 L 9.5 3.5 L 10.5 3.5 L 10.5 1.03125 C 15.063526 1.2867831 18.713217 4.9364738 18.96875 9.5 L 16.5 9.5 L 16.5 10.5 L 18.96875 10.5 C 18.713217 15.063526 15.063526 18.713217 10.5 18.96875 L 10.5 16.5 L 9.5 16.5 L 9.5 18.96875 C 4.9364738 18.713217 1.2867831 15.063526 1.03125 10.5 L 3.5 10.5 L 3.5 9.5 L 1.03125 9.5 C 1.279102 5.0736488 4.7225326 1.4751713 9.09375 1.03125 z M 9.5 5 L 9.5 8.0625 C 8.6373007 8.2844627 8 9.0680195 8 10 C 8 11.104569 8.8954305 12 10 12 C 10.931981 12 11.715537 11.362699 11.9375 10.5 L 14 10.5 L 14 9.5 L 11.9375 9.5 C 11.756642 8.7970599 11.20294 8.2433585 10.5 8.0625 L 10.5 5 L 9.5 5 z \\\"  \\n    \\tfill=\\\"#585858\\\" stroke=\\\"none\\\" />\\n    \\t\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/timer.png\",\r\n" +
				"    \"groups\" : [ \"边界事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"timercycledefinitionpackage\", \"timerdatedefinitionpackage\", \"timerdurationdefinitionpackage\", \"cancelactivitypackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"BoundaryEventsMorph\", \"IntermediateEventOnActivityBoundary\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BoundarySignalEvent\",\r\n" +
				"    \"title\" : \"边界信号事件\",\r\n" +
				"    \"description\" : \"A boundary event with a signal trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\t\\n    <circle \\n    \\tid=\\\"frame2_non_interrupting\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"12\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"none\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 4.5, 3\\\" />\\n    \\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame2\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n\\t<path\\n\\t   id=\\\"signalCatching\\\"\\n\\t   stroke=\\\"#585858\\\"\\n       d=\\\"M 8.7124971,21.247342 L 23.333334,21.247342 L 16.022915,8.5759512 L 8.7124971,21.247342 z\\\"\\n       style=\\\"fill:none;stroke-width:1.4;stroke-miterlimit:4;stroke-dasharray:none\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/signal.png\",\r\n" +
				"    \"groups\" : [ \"边界事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"signalrefpackage\", \"cancelactivitypackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"BoundaryEventsMorph\", \"IntermediateEventOnActivityBoundary\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BoundaryMessageEvent\",\r\n" +
				"    \"title\" : \"边界消息事件\",\r\n" +
				"    \"description\" : \"A boundary event with a message trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"red\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\t\\n    <circle \\n    \\tid=\\\"frame2_non_interrupting\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"12\\\" \\n    \\tstroke=\\\"green\\\" \\n    \\tfill=\\\"none\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 4.5, 3\\\" />\\n    \\t\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame2\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n\\t<g id=\\\"messageCatching\\\">\\n\\t\\t<path transform=\\\"translate(7,7)\\\" id=\\\"path1\\\" stroke=\\\"none\\\" fill=\\\"#585858\\\" stroke-width=\\\"1\\\" d=\\\"M 1 3 L 9 11 L 17 3 L 1 3 z M 1 5 L 1 13 L 5 9 L 1 5 z M 17 5 L 13 9 L 17 13 L 17 5 z M 6 10 L 1 15 L 17 15 L 12 10 L 9 13 L 6 10 z \\\"/>\\n\\t</g>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n\\t\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/message.png\",\r\n" +
				"    \"groups\" : [ \"边界事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"messagerefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"BoundaryEventsMorph\", \"IntermediateEventOnActivityBoundary\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"BoundaryCancelEvent\",\r\n" +
				"    \"title\" : \"边界取消事件\",\r\n" +
				"    \"description\" : \"A boundary cancel event\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n  \\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path\\n       d=\\\"M 7.2839105,10.27369 L 10.151395,7.4062062 L 15.886362,13.141174 L 21.621331,7.4062056 L 24.488814,10.273689 L 18.753846,16.008657 L 24.488815,21.743626 L 21.621331,24.611111 L 15.886362,18.876142 L 10.151394,24.611109 L 7.283911,21.743625 L 13.018878,16.008658 L 7.2839105,10.27369 z\\\"\\n       id=\\\"cancelCross\\\" fill=\\\"none\\\" stroke=\\\"#585858\\\" stroke-width=\\\"1.7\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/cancel.png\",\r\n" +
				"    \"groups\" : [ \"边界事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"BoundaryEventsMorph\", \"IntermediateEventOnActivityBoundary\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"边界补偿事件\",\r\n" +
				"    \"title\" : \"Boundary compensation event\",\r\n" +
				"    \"description\" : \"A boundary compensation event\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n\\t\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <polygon id=\\\"poly1\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1.4\\\" points=\\\"15,9 15,23 8,16\\\" stroke-linecap=\\\"butt\\\" stroke-linejoin=\\\"miter\\\" stroke-miterlimit=\\\"10\\\" />\\n    <polygon id=\\\"poly2\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1.4\\\" points=\\\"22,9 22,23 15,16\\\" stroke-linecap=\\\"butt\\\" stroke-linejoin=\\\"miter\\\" stroke-miterlimit=\\\"10\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/compensation.png\",\r\n" +
				"    \"groups\" : [ \"边界事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"BoundaryEventsMorph\", \"IntermediateEventOnActivityBoundary\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"CatchTimerEvent\",\r\n" +
				"    \"title\" : \"中间定时器捕获事件\",\r\n" +
				"    \"description\" : \"An intermediate catching event with a timer trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\t\\n    <circle \\n    \\tid=\\\"frame2_non_interrupting\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"12\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"none\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 4.5, 3\\\" />\\n    \\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame2\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n    <path id=\\\"path1\\\" transform=\\\"translate(6,6)\\\"\\n    \\td=\\\"M 10 0 C 4.4771525 0 0 4.4771525 0 10 C 0 15.522847 4.4771525 20 10 20 C 15.522847 20 20 15.522847 20 10 C 20 4.4771525 15.522847 1.1842379e-15 10 0 z M 9.09375 1.03125 C 9.2292164 1.0174926 9.362825 1.0389311 9.5 1.03125 L 9.5 3.5 L 10.5 3.5 L 10.5 1.03125 C 15.063526 1.2867831 18.713217 4.9364738 18.96875 9.5 L 16.5 9.5 L 16.5 10.5 L 18.96875 10.5 C 18.713217 15.063526 15.063526 18.713217 10.5 18.96875 L 10.5 16.5 L 9.5 16.5 L 9.5 18.96875 C 4.9364738 18.713217 1.2867831 15.063526 1.03125 10.5 L 3.5 10.5 L 3.5 9.5 L 1.03125 9.5 C 1.279102 5.0736488 4.7225326 1.4751713 9.09375 1.03125 z M 9.5 5 L 9.5 8.0625 C 8.6373007 8.2844627 8 9.0680195 8 10 C 8 11.104569 8.8954305 12 10 12 C 10.931981 12 11.715537 11.362699 11.9375 10.5 L 14 10.5 L 14 9.5 L 11.9375 9.5 C 11.756642 8.7970599 11.20294 8.2433585 10.5 8.0625 L 10.5 5 L 9.5 5 z \\\"  \\n    \\tfill=\\\"#585858\\\" stroke=\\\"none\\\" />\\n    \\t\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/timer.png\",\r\n" +
				"    \"groups\" : [ \"中间捕捉事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"timercycledefinitionpackage\", \"timerdatedefinitionpackage\", \"timerdurationdefinitionpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"sequence_end\", \"CatchEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"CatchSignalEvent\",\r\n" +
				"    \"title\" : \"中间信号捕捉事件\",\r\n" +
				"    \"description\" : \"An intermediate catching event with a signal trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\t\\n    <circle \\n    \\tid=\\\"frame2_non_interrupting\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"12\\\" \\n    \\tstroke=\\\"#585858\\\" \\n    \\tfill=\\\"none\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 4.5, 3\\\" />\\n    \\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame2\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n\\t<path\\n\\t   id=\\\"signalCatching\\\"\\n\\t   stroke=\\\"#585858\\\"\\n       d=\\\"M 8.7124971,21.247342 L 23.333334,21.247342 L 16.022915,8.5759512 L 8.7124971,21.247342 z\\\"\\n       style=\\\"fill:none;stroke-width:1.4;stroke-miterlimit:4;stroke-dasharray:none\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/signal.png\",\r\n" +
				"    \"groups\" : [ \"中间捕捉事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"signalrefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"sequence_end\", \"CatchEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"CatchMessageEvent\",\r\n" +
				"    \"title\" : \"中间消息捕捉事件\",\r\n" +
				"    \"description\" : \"An intermediate catching event with a message trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle \\n    \\tid=\\\"bg_frame\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"15\\\" \\n    \\tstroke=\\\"red\\\" \\n    \\tfill=\\\"#ffffff\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 5.5, 3\\\" />\\n    \\t\\n    <circle \\n    \\tid=\\\"frame2_non_interrupting\\\" \\n    \\tcx=\\\"16\\\" \\n    \\tcy=\\\"16\\\" \\n    \\tr=\\\"12\\\" \\n    \\tstroke=\\\"green\\\" \\n    \\tfill=\\\"none\\\" \\n    \\tstroke-width=\\\"1\\\"\\n    \\tstyle=\\\"stroke-dasharray: 4.5, 3\\\" />\\n    \\t\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame2\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    \\n\\t<g id=\\\"messageCatching\\\">\\n\\t\\t<path transform=\\\"translate(7,7)\\\" id=\\\"path1\\\" stroke=\\\"none\\\" fill=\\\"#585858\\\" stroke-width=\\\"1\\\" d=\\\"M 1 3 L 9 11 L 17 3 L 1 3 z M 1 5 L 1 13 L 5 9 L 1 5 z M 17 5 L 13 9 L 17 13 L 17 5 z M 6 10 L 1 15 L 17 15 L 12 10 L 9 13 L 6 10 z \\\"/>\\n\\t</g>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n\\t\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"catching/message.png\",\r\n" +
				"    \"groups\" : [ \"中间捕捉事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"messagerefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"sequence_start\", \"sequence_end\", \"CatchEventsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ThrowNoneEvent\",\r\n" +
				"    \"title\" : \"中间无抛出事件\",\r\n" +
				"    \"description\" : \"An intermediate event without a specific trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n  \\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"throwing/none.png\",\r\n" +
				"    \"groups\" : [ \"中间抛出事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"ThrowEventsMorph\", \"sequence_start\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"ThrowSignalEvent\",\r\n" +
				"    \"title\" : \"中间信号抛出事件\",\r\n" +
				"    \"description\" : \"An intermediate event with a signal trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"15\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"1\\\"/>\\n    <circle id=\\\"frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"12\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"1\\\"/>\\n    <path\\n\\t   id=\\\"signalThrowing\\\"\\n       d=\\\"M 8.7124971,21.247342 L 23.333334,21.247342 L 16.022915,8.5759512 L 8.7124971,21.247342 z\\\"\\n       fill=\\\"#585858\\\"\\n       stroke=\\\"#585858\\\"\\n       style=\\\"stroke-width:1.4;stroke-miterlimit:4;stroke-dasharray:none\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"33\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"throwing/signal.png\",\r\n" +
				"    \"groups\" : [ \"中间抛出事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"signalrefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"ThrowEventsMorph\", \"sequence_start\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"EndNoneEvent\",\r\n" +
				"    \"title\" : \"结束事件\",\r\n" +
				"    \"description\" : \"An end event without a specific trigger\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"14\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"3\\\"/>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"32\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"endevent/none.png\",\r\n" +
				"    \"groups\" : [ \"结束事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"EndEventsMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"EndErrorEvent\",\r\n" +
				"    \"title\" : \"结束错误事件\",\r\n" +
				"    \"description\" : \"An end event that throws an error event\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <oryx:docker oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" />\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"14\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"3\\\"/>\\n    \\n    <path\\n         fill=\\\"#585858\\\"\\n         stroke=\\\"#585858\\\"\\n         style=\\\"stroke-width:1.5;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10\\\"\\n         d=\\\"M 22.820839,11.171502 L 19.36734,24.58992 L 13.54138,14.281819 L 9.3386512,20.071607 L 13.048949,6.8323057 L 18.996148,16.132659 L 22.820839,11.171502 z\\\"\\n         id=\\\"errorPolygon\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"32\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"endevent/error.png\",\r\n" +
				"    \"groups\" : [ \"结束事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\", \"errorrefpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"EndEventsMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"EndCancelEvent\",\r\n" +
				"    \"title\" : \"结束取消事件\",\r\n" +
				"    \"description\" : \"A cancel end event\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"14\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"3\\\"/>\\n    \\n    <path id=\\\"path1\\\" d=\\\"M 9 9 L 23 23 M 9 23 L 23 9\\\" fill=\\\"none\\\" stroke=\\\"#585858\\\" stroke-width=\\\"5\\\" />\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"32\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"endevent/cancel.png\",\r\n" +
				"    \"groups\" : [ \"结束事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"EndEventsMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"EndTerminateEvent\",\r\n" +
				"    \"title\" : \"结束终止事件\",\r\n" +
				"    \"description\" : \"A terminate end event\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   width=\\\"40\\\"\\n   height=\\\"40\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"16\\\" oryx:cy=\\\"16\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"fill\\\">\\n    <circle id=\\\"bg_frame\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"14\\\" stroke=\\\"#585858\\\" fill=\\\"#ffffff\\\" stroke-width=\\\"3\\\"/>\\n    \\n    <circle id=\\\"circle1\\\" cx=\\\"16\\\" cy=\\\"16\\\" r=\\\"9\\\" stroke=\\\"#585858\\\" fill=\\\"#585858\\\" stroke-width=\\\"1\\\"/>\\n\\t<text font-size=\\\"11\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\tx=\\\"16\\\" y=\\\"32\\\" \\n\\t\\toryx:align=\\\"top center\\\" \\n\\t\\tstroke=\\\"#373e48\\\"\\n\\t></text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"endevent/terminate.png\",\r\n" +
				"    \"groups\" : [ \"结束事件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"executionlistenerspackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"EndEventsMorph\", \"sequence_end\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"Pool\",\r\n" +
				"    \"title\" : \"池\",\r\n" +
				"    \"description\" : \"A pool to stucture the process definition\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n   width=\\\"600\\\"\\n   height=\\\"250\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"0\\\" oryx:cy=\\\"124\\\" oryx:anchors=\\\"left\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"299\\\" oryx:cy=\\\"249\\\" oryx:anchors=\\\"bottom\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"599\\\" oryx:cy=\\\"124\\\" oryx:anchors=\\\"right\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"299\\\" oryx:cy=\\\"0\\\" oryx:anchors=\\\"top\\\" />\\n  \\t<oryx:magnet oryx:cx=\\\"299\\\" oryx:cy=\\\"124\\\" oryx:default=\\\"yes\\\" />\\n  </oryx:magnets>\\n  <g pointer-events=\\\"none\\\" >\\n    <defs>\\n\\t\\t<radialGradient id=\\\"background\\\" cx=\\\"0%\\\" cy=\\\"10%\\\" r=\\\"100%\\\" fx=\\\"20%\\\" fy=\\\"10%\\\">\\n\\t\\t\\t<stop offset=\\\"0%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"1\\\"/>\\n\\t\\t\\t<stop id=\\\"fill_el\\\" offset=\\\"100%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"1\\\"/>\\n\\t\\t</radialGradient>\\n\\t</defs>\\n\\t  \\t\\n  \\t<rect\\n  \\t\\tid=\\\"border\\\"\\n  \\t\\tclass=\\\"stripable-element-force\\\"\\n  \\t\\toryx:resize=\\\"vertical horizontal\\\"\\n  \\t\\tx=\\\"0\\\"\\n  \\t\\ty=\\\"0\\\"\\n  \\t\\twidth=\\\"600\\\"\\n  \\t\\theight=\\\"250\\\"\\n  \\t\\tfill=\\\"none\\\"\\n  \\t\\tstroke-width=\\\"9\\\"\\n  \\t\\tstroke=\\\"none\\\"\\n  \\t\\tvisibility=\\\"visible\\\"\\n  \\t\\tpointer-events=\\\"stroke\\\"\\n  \\t/>\\n    <rect\\n    \\tid=\\\"c\\\"\\n    \\toryx:resize=\\\"vertical horizontal\\\"\\n    \\tx=\\\"0\\\"\\n    \\ty=\\\"0\\\"\\n    \\twidth=\\\"600\\\" \\n    \\theight=\\\"250\\\" \\n    \\tstroke=\\\"black\\\" \\n    \\tfill=\\\"url(#background) white\\\"\\n    \\tfill-opacity=\\\"0.3\\\" \\n    />\\n    \\n\\t<rect \\n\\t\\tid=\\\"caption\\\"\\n\\t\\toryx:anchors=\\\"left top bottom\\\"\\n\\t\\tx=\\\"0\\\"\\n\\t\\ty=\\\"0\\\"\\n\\t\\twidth=\\\"30\\\"\\n\\t\\theight=\\\"250\\\"\\n\\t\\tstroke=\\\"black\\\"\\n\\t\\tstroke-width=\\\"1\\\"\\n\\t\\tfill=\\\"url(#background) white\\\"\\n\\t\\tpointer-events=\\\"all\\\"\\n\\t/>\\n\\t\\n\\t<rect \\n\\t\\tid=\\\"captionDisableAntialiasing\\\"\\n\\t\\toryx:anchors=\\\"left top bottom\\\"\\n\\t\\tx=\\\"0\\\"\\n\\t\\ty=\\\"0\\\"\\n\\t\\twidth=\\\"30\\\"\\n\\t\\theight=\\\"250\\\"\\n\\t\\tstroke=\\\"black\\\"\\n\\t\\tstroke-width=\\\"1\\\"\\n\\t\\tfill=\\\"url(#background) white\\\"\\n\\t\\tpointer-events=\\\"all\\\"\\n\\t/>\\n\\t\\n    <text x=\\\"13\\\" y=\\\"125\\\" font-size=\\\"12\\\" id=\\\"text_name\\\" oryx:fittoelem=\\\"caption\\\" oryx:align=\\\"middle center\\\" oryx:anchors=\\\"left\\\" oryx:rotate=\\\"270\\\" fill=\\\"black\\\" stroke=\\\"black\\\"></text>\\n    \\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"swimlane/pool.png\",\r\n" +
				"    \"groups\" : [ \"泳道\" ],\r\n" +
				"    \"layout\" : [ {\r\n" +
				"      \"type\" : \"layout.bpmn2_0.pool\"\r\n" +
				"    } ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"process_idpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"canContainArtifacts\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"Lane\",\r\n" +
				"    \"title\" : \"道\",\r\n" +
				"    \"description\" : \"A lane to stucture the process definition\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n   width=\\\"600\\\"\\n   height=\\\"250\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <g pointer-events=\\\"none\\\" >\\n  \\n     <defs>\\n\\t\\t<radialGradient id=\\\"background\\\" cx=\\\"0%\\\" cy=\\\"10%\\\" r=\\\"200%\\\" fx=\\\"20%\\\" fy=\\\"10%\\\">\\n\\t\\t\\t<stop offset=\\\"0%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"1\\\"/>\\n\\t\\t\\t<stop id=\\\"fill_el\\\" offset=\\\"100%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"0\\\"/>\\n\\t\\t</radialGradient>\\n\\t</defs>\\n\\t\\n  \\t<rect id=\\\"border_invisible\\\" class=\\\"stripable-element-force\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"600\\\" height=\\\"250\\\" fill=\\\"none\\\" stroke-width=\\\"10\\\" stroke=\\\"white\\\" visibility=\\\"hidden\\\" pointer-events=\\\"stroke\\\"/>\\t\\t\\n\\t<rect id=\\\"border\\\" oryx:resize=\\\"vertical horizontal\\\" x=\\\"0\\\" y=\\\"0\\\" width=\\\"600\\\" height=\\\"250\\\" stroke=\\\"black\\\" stroke-width=\\\"1\\\" fill=\\\"url(#background) white\\\" pointer-events=\\\"none\\\" />\\n\\t\\n\\t\\n\\t<rect \\n\\t\\tid=\\\"caption\\\"\\n\\t\\toryx:anchors=\\\"left top bottom\\\"\\n\\t\\tx=\\\"0\\\"\\n\\t\\ty=\\\"1\\\"\\n\\t\\twidth=\\\"30\\\"\\n\\t\\theight=\\\"248\\\"\\n\\t\\tstroke=\\\"black\\\"\\n\\t\\tstroke-width=\\\"0\\\"\\n\\t\\tfill=\\\"white\\\"\\n\\t\\tvisibility=\\\"hidden\\\"\\n\\t\\tclass=\\\"stripable-element-force\\\"\\n\\t\\tpointer-events=\\\"all\\\"\\n\\t/>\\n\\t\\n\\t<path\\n\\t\\tstroke=\\\"black\\\"\\n\\t\\tstroke-width=\\\"1\\\"\\n\\t\\tfill=\\\"none\\\"\\n\\t\\td=\\\"M 0,0 L 0,250\\\"\\n        oryx:anchors=\\\"left top bottom\\\"\\n        id=\\\"captionDisableAntialiasing\\\"\\n    />\\n\\t\\n\\t<!--rect \\n\\t\\tid=\\\"captionDisableAntialiasing\\\"\\n\\t\\toryx:anchors=\\\"left top bottom\\\"\\n\\t\\tx=\\\"0\\\"\\n\\t\\ty=\\\"0\\\"\\n\\t\\twidth=\\\"30\\\"\\n\\t\\theight=\\\"250\\\"\\n\\t\\tstroke=\\\"black\\\"\\n\\t\\tstroke-width=\\\"1\\\"\\n\\t\\tfill=\\\"url(#background) white\\\"\\n\\t/-->\\n\\t\\n    <text \\n\\t\\tx=\\\"13\\\"\\n\\t\\ty=\\\"125\\\"\\n\\t\\toryx:rotate=\\\"270\\\" \\n\\t\\tfont-size=\\\"12\\\" \\n\\t\\tid=\\\"text_name\\\" \\n\\t\\toryx:align=\\\"middle center\\\" \\n\\t\\toryx:anchors=\\\"left\\\"\\n\\t\\toryx:fittoelem=\\\"caption\\\"\\n\\t\\tfill=\\\"black\\\" \\n\\t\\tstroke=\\\"black\\\">\\n\\t</text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"swimlane/lane.png\",\r\n" +
				"    \"groups\" : [ \"泳道\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"PoolChild\", \"canContainArtifacts\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"edge\",\r\n" +
				"    \"id\" : \"SequenceFlow\",\r\n" +
				"    \"title\" : \"Sequence flow\",\r\n" +
				"    \"description\" : \"Sequence flow defines the execution order of activities.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\r\\n<svg\\r\\n\\txmlns=\\\"http://www.w3.org/2000/svg\\\"\\r\\n\\txmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\r\\n\\tversion=\\\"1.0\\\"\\r\\n\\toryx:edge=\\\"edge\\\" >\\r\\n\\t<defs>\\r\\n\\t  \\t<marker id=\\\"start\\\" refX=\\\"1\\\" refY=\\\"5\\\" markerUnits=\\\"userSpaceOnUse\\\" markerWidth=\\\"17\\\" markerHeight=\\\"11\\\" orient=\\\"auto\\\">\\r\\n\\t  \\t\\t<!-- <path id=\\\"conditional\\\"   d=\\\"M 0 6 L 8 1 L 15 5 L 8 9 L 1 5\\\" fill=\\\"white\\\" stroke=\\\"black\\\" stroke-width=\\\"1\\\" /> -->\\r\\n\\t\\t\\t<path id=\\\"default\\\" d=\\\"M 5 0 L 11 10\\\" fill=\\\"white\\\" stroke=\\\"#585858\\\" stroke-width=\\\"1\\\" />\\r\\n\\t  \\t</marker>\\r\\n\\t  \\t<marker id=\\\"end\\\" refX=\\\"15\\\" refY=\\\"6\\\" markerUnits=\\\"userSpaceOnUse\\\" markerWidth=\\\"15\\\" markerHeight=\\\"12\\\" orient=\\\"auto\\\">\\r\\n\\t  \\t\\t<path id=\\\"arrowhead\\\" d=\\\"M 0 1 L 15 6 L 0 11z\\\" fill=\\\"#585858\\\" stroke=\\\"#585858\\\" stroke-linejoin=\\\"round\\\" stroke-width=\\\"2\\\" />\\r\\n\\t  \\t</marker>\\r\\n\\t</defs>\\r\\n\\t<g id=\\\"edge\\\">\\r\\n\\t\\t<path id=\\\"bg_frame\\\" d=\\\"M10 50 L210 50\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"2\\\" stroke-linecap=\\\"round\\\" stroke-linejoin=\\\"round\\\" marker-start=\\\"url(#start)\\\" marker-end=\\\"url(#end)\\\" />\\r\\n\\t\\t<text id=\\\"text_name\\\" x=\\\"0\\\" y=\\\"0\\\" oryx:edgePosition=\\\"startTop\\\"/>\\r\\n\\t</g>\\r\\n</svg>\",\r\n" +
				"    \"icon\" : \"connector/sequenceflow.png\",\r\n" +
				"    \"groups\" : [ \"链接对象\" ],\r\n" +
				"    \"layout\" : [ {\r\n" +
				"      \"type\" : \"layout.bpmn2_0.sequenceflow\"\r\n" +
				"    } ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"conditionsequenceflowpackage\", \"executionlistenerspackage\", \"defaultflowpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"ConnectingObjectsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"edge\",\r\n" +
				"    \"id\" : \"MessageFlow\",\r\n" +
				"    \"title\" : \"Message flow\",\r\n" +
				"    \"description\" : \"Message flow to connect elements in different pools.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\r\\n<svg\\r\\n\\txmlns=\\\"http://www.w3.org/2000/svg\\\"\\r\\n\\txmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\r\\n\\tversion=\\\"1.0\\\"\\r\\n\\toryx:edge=\\\"edge\\\" >\\r\\n\\t<defs>\\r\\n\\t\\t<marker id=\\\"start\\\" oryx:optional=\\\"yes\\\" oryx:enabled=\\\"yes\\\" refX=\\\"5\\\" refY=\\\"5\\\" markerUnits=\\\"userSpaceOnUse\\\" markerWidth=\\\"10\\\" markerHeight=\\\"10\\\" orient=\\\"auto\\\">\\r\\n\\t  \\t\\t<!-- <path d=\\\"M 10 10 L 0 5 L 10 0\\\" fill=\\\"none\\\" stroke=\\\"#585858\\\" /> -->\\r\\n\\t  \\t\\t<circle id=\\\"arrowhead\\\" cx=\\\"5\\\" cy=\\\"5\\\" r=\\\"5\\\" fill=\\\"white\\\" stroke=\\\"black\\\" />\\r\\n\\t  \\t</marker>\\r\\n\\r\\n\\t  \\t<marker id=\\\"end\\\" refX=\\\"10\\\" refY=\\\"5\\\" markerUnits=\\\"userSpaceOnUse\\\" markerWidth=\\\"10\\\" markerHeight=\\\"10\\\" orient=\\\"auto\\\">\\r\\n\\t  \\t\\t<path id=\\\"arrowhead2\\\" d=\\\"M 0 0 L 10 5 L 0 10 L 0 0\\\" fill=\\\"white\\\" stroke=\\\"#585858\\\" />\\r\\n\\t  \\t</marker>\\r\\n\\t</defs>\\r\\n\\t<g id=\\\"edge\\\">\\r\\n\\t    <path id=\\\"bg_frame\\\" d=\\\"M10 50 L210 50\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"2\\\" stroke-dasharray=\\\"3, 4\\\" marker-start=\\\"url(#start)\\\" marker-end=\\\"url(#end)\\\" />\\r\\n\\t\\t<text id=\\\"text_name\\\" x=\\\"0\\\" y=\\\"0\\\" oryx:edgePosition=\\\"midTop\\\"/>\\r\\n\\t</g>\\r\\n</svg>\",\r\n" +
				"    \"icon\" : \"connector/messageflow.png\",\r\n" +
				"    \"groups\" : [ \"链接对象\" ],\r\n" +
				"    \"layout\" : [ {\r\n" +
				"      \"type\" : \"layout.bpmn2_0.sequenceflow\"\r\n" +
				"    } ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"ConnectingObjectsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"edge\",\r\n" +
				"    \"id\" : \"Association\",\r\n" +
				"    \"title\" : \"Association\",\r\n" +
				"    \"description\" : \"Associates a text annotation with an element.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\r\\n<svg\\r\\n\\txmlns=\\\"http://www.w3.org/2000/svg\\\"\\r\\n\\txmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\r\\n\\tversion=\\\"1.0\\\"\\r\\n\\toryx:edge=\\\"edge\\\" >\\r\\n\\t<g id=\\\"edge\\\">\\r\\n\\t    <path id=\\\"bg_frame\\\" d=\\\"M10 50 L210 50\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"2\\\" stroke-dasharray=\\\"3, 4\\\" />\\r\\n\\t\\t<text id=\\\"name\\\" x=\\\"0\\\" y=\\\"0\\\" oryx:edgePosition=\\\"midTop\\\" oryx:offsetTop=\\\"6\\\" style=\\\"font-size:9px;\\\"/>\\r\\n\\t</g>\\r\\n</svg>\",\r\n" +
				"    \"icon\" : \"connector/association.undirected.png\",\r\n" +
				"    \"groups\" : [ \"链接对象\" ],\r\n" +
				"    \"layout\" : [ {\r\n" +
				"      \"type\" : \"layout.bpmn2_0.sequenceflow\"\r\n" +
				"    } ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"ConnectingObjectsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"edge\",\r\n" +
				"    \"id\" : \"DataAssociation\",\r\n" +
				"    \"title\" : \"DataAssociation\",\r\n" +
				"    \"description\" : \"Associates a data element with an activity.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\r\\n<svg\\r\\n\\txmlns=\\\"http://www.w3.org/2000/svg\\\"\\r\\n\\txmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\r\\n\\tversion=\\\"1.0\\\"\\r\\n\\toryx:edge=\\\"edge\\\" >\\r\\n\\t<defs>\\r\\n\\t  \\t<marker id=\\\"end\\\" refX=\\\"10\\\" refY=\\\"5\\\" markerUnits=\\\"userSpaceOnUse\\\" markerWidth=\\\"10\\\" markerHeight=\\\"10\\\" orient=\\\"auto\\\">\\r\\n\\t  \\t\\t<path id=\\\"arrowhead\\\" d=\\\"M 0 0 L 10 5 L 0 10\\\" fill=\\\"none\\\" stroke=\\\"#585858\\\" />\\r\\n\\t  \\t</marker>\\r\\n\\t</defs>\\r\\n\\t<g id=\\\"edge\\\">\\r\\n\\t    <path id=\\\"bg_frame\\\" d=\\\"M10 50 L210 50\\\" stroke=\\\"#585858\\\" fill=\\\"none\\\" stroke-width=\\\"2\\\" stroke-dasharray=\\\"3, 4\\\" marker-end=\\\"url(#end)\\\" />\\r\\n\\t\\t<text id=\\\"name\\\" x=\\\"0\\\" y=\\\"0\\\" oryx:edgePosition=\\\"midTop\\\" oryx:offsetTop=\\\"6\\\" style=\\\"font-size:9px;\\\"/>\\r\\n\\t</g>\\r\\n</svg>\",\r\n" +
				"    \"icon\" : \"connector/association.unidirectional.png\",\r\n" +
				"    \"groups\" : [ \"链接对象\" ],\r\n" +
				"    \"layout\" : [ {\r\n" +
				"      \"type\" : \"layout.bpmn2_0.sequenceflow\"\r\n" +
				"    } ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"ConnectingObjectsMorph\", \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"TextAnnotation\",\r\n" +
				"    \"title\" : \"文本注释\",\r\n" +
				"    \"description\" : \"Annotates elements with description text.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\" standalone=\\\"no\\\"?>\\n<svg\\n   xmlns=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\n   xmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\n   xmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\n   width=\\\"102\\\"\\n   height=\\\"51\\\"\\n   version=\\\"1.0\\\">\\n  <defs></defs>\\n  <oryx:magnets>\\n  \\t<oryx:magnet oryx:cx=\\\"2\\\" oryx:cy=\\\"25\\\" oryx:anchors=\\\"left\\\" oryx:default=\\\"yes\\\"/>\\n  </oryx:magnets>\\n  <g pointer-events=\\\"all\\\" oryx:minimumSize=\\\"10 20\\\" oryx:maximumSize=\\\"\\\" >\\n  <rect \\n\\tid=\\\"textannotationrect\\\"\\n\\toryx:resize=\\\"vertical horizontal\\\"\\n\\tx=\\\"1\\\" \\n\\ty=\\\"1\\\"\\n\\twidth=\\\"100\\\"\\n\\theight=\\\"50\\\"\\n\\tstroke=\\\"none\\\"\\n\\tfill=\\\"none\\\" />\\n  <path \\n  \\tid = \\\"frame\\\"\\n\\td=\\\"M20,1 L1,1 L1,50 L20,50\\\" \\n\\toryx:anchors=\\\"top bottom left\\\" \\n\\tstroke=\\\"#585858\\\" \\n\\tfill=\\\"none\\\" \\n\\tstroke-width=\\\"1\\\" />\\n    \\n    <text \\n\\t\\tfont-size=\\\"12\\\" \\n\\t\\tid=\\\"text\\\" \\n\\t\\tx=\\\"5\\\" \\n\\t\\ty=\\\"25\\\" \\n\\t\\toryx:align=\\\"middle left\\\"\\n\\t\\toryx:fittoelem=\\\"textannotationrect\\\"\\n\\t\\toryx:anchors=\\\"left\\\"\\n\\t\\tstroke=\\\"#373e48\\\">\\n\\t</text>\\n  </g>\\n</svg>\",\r\n" +
				"    \"icon\" : \"artifact/text.annotation.png\",\r\n" +
				"    \"groups\" : [ \"组件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\", \"textpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"all\" ]\r\n" +
				"  }, {\r\n" +
				"    \"type\" : \"node\",\r\n" +
				"    \"id\" : \"DataStore\",\r\n" +
				"    \"title\" : \"Data store\",\r\n" +
				"    \"description\" : \"Reference to a data store.\",\r\n" +
				"    \"view\" : \"<?xml version=\\\"1.0\\\" encoding=\\\"utf-8\\\" standalone=\\\"no\\\" ?>\\r\\n<svg \\r\\n\\txmlns=\\\"http://www.w3.org/2000/svg\\\"\\r\\n\\txmlns:svg=\\\"http://www.w3.org/2000/svg\\\"\\r\\n   \\txmlns:oryx=\\\"http://www.b3mn.org/oryx\\\"\\r\\n   \\txmlns:xlink=\\\"http://www.w3.org/1999/xlink\\\"\\r\\n\\t\\r\\n\\twidth=\\\"63.001px\\\" \\r\\n\\theight=\\\"61.173px\\\"\\r\\n\\tversion=\\\"1.0\\\">\\r\\n\\t<defs></defs>\\r\\n\\t<oryx:magnets>\\r\\n\\t\\t<oryx:magnet oryx:cx=\\\"0\\\" oryx:cy=\\\"30.5865\\\" oryx:anchors=\\\"left\\\" />\\r\\n\\t\\t<oryx:magnet oryx:cx=\\\"31.5005\\\" oryx:cy=\\\"61.173\\\" oryx:anchors=\\\"bottom\\\" />\\r\\n\\t\\t<oryx:magnet oryx:cx=\\\"63.001\\\" oryx:cy=\\\"30.5865\\\" oryx:anchors=\\\"right\\\" />\\r\\n\\t\\t<oryx:magnet oryx:cx=\\\"31.5005\\\" oryx:cy=\\\"0\\\" oryx:anchors=\\\"top\\\" />\\r\\n\\t\\t<oryx:magnet oryx:cx=\\\"31.5005\\\" oryx:cy=\\\"30.5865\\\" oryx:default=\\\"yes\\\" />\\r\\n\\t</oryx:magnets>\\r\\n\\t\\r\\n\\t<g>\\r\\n\\t\\t<defs>\\r\\n\\t\\t\\t<radialGradient id=\\\"background\\\" cx=\\\"30%\\\" cy=\\\"30%\\\" r=\\\"50%\\\" fx=\\\"0%\\\" fy=\\\"0%\\\">\\r\\n\\t\\t\\t\\t<stop offset=\\\"0%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"1\\\"></stop>\\r\\n\\t\\t\\t\\t<stop offset=\\\"100%\\\" stop-color=\\\"#ffffff\\\" stop-opacity=\\\"1\\\" id=\\\"fill_el\\\"></stop>\\r\\n\\t\\t\\t</radialGradient>\\r\\n\\t\\t</defs>\\r\\n\\t\\t\\r\\n\\t\\t<path id=\\\"bg_frame\\\" fill=\\\"url(#background) #ffffff\\\" stroke=\\\"#000000\\\" d=\\\"M31.634,0.662c20.013,0,31.292,3.05,31.292,5.729c0,2.678,0,45.096,0,48.244\\r\\n\\t\\t\\tc0,3.148-16.42,6.2-31.388,6.2c-14.968,0-30.613-2.955-30.613-6.298c0-3.342,0-45.728,0-48.05\\r\\n\\t\\t\\tC0.925,4.165,11.622,0.662,31.634,0.662z\\\"/>\\r\\n\\t\\t<path id=\\\"bg_frame2\\\" fill=\\\"none\\\" stroke=\\\"#000000\\\" d=\\\"\\r\\n\\t\\t\\tM62.926,15.69c0,1.986-3.62,6.551-31.267,6.551c-27.646,0-30.734-4.686-30.734-6.454 M0.925,11.137\\r\\n\\t\\t\\tc0,1.769,3.088,6.455,30.734,6.455c27.647,0,31.267-4.565,31.267-6.551 M0.925,6.487c0,2.35,3.088,6.455,30.734,6.455\\r\\n\\t\\t\\tc27.647,0,31.267-3.912,31.267-6.552 M62.926,6.391v4.844 M0.949,6.391v4.844 M62.926,11.041v4.844 M0.949,11.041v4.844\\\"/>\\r\\n\\t\\t\\t \\t\\r\\n\\t\\t<text font-size=\\\"12\\\" id=\\\"text_name\\\" x=\\\"31\\\" y=\\\"66\\\" oryx:align=\\\"center top\\\" stroke=\\\"black\\\" />\\r\\n\\t\\t\\t \\r\\n\\t</g>\\r\\n</svg>\\r\\n\",\r\n" +
				"    \"icon\" : \"dataobject/data.store.png\",\r\n" +
				"    \"groups\" : [ \"组件\" ],\r\n" +
				"    \"propertyPackages\" : [ \"overrideidpackage\", \"namepackage\", \"documentationpackage\" ],\r\n" +
				"    \"hiddenPropertyPackages\" : [ ],\r\n" +
				"    \"roles\" : [ \"all\" ]\r\n" +
				"  } ],\r\n" +
				"  \"rules\" : {\r\n" +
				"    \"cardinalityRules\" : [ {\r\n" +
				"      \"role\" : \"Startevents_all\",\r\n" +
				"      \"incomingEdges\" : [ {\r\n" +
				"        \"role\" : \"SequenceFlow\",\r\n" +
				"        \"maximum\" : 0\r\n" +
				"      } ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"Endevents_all\",\r\n" +
				"      \"outgoingEdges\" : [ {\r\n" +
				"        \"role\" : \"SequenceFlow\",\r\n" +
				"        \"maximum\" : 0\r\n" +
				"      } ]\r\n" +
				"    } ],\r\n" +
				"    \"connectionRules\" : [ {\r\n" +
				"      \"role\" : \"SequenceFlow\",\r\n" +
				"      \"connects\" : [ {\r\n" +
				"        \"from\" : \"sequence_start\",\r\n" +
				"        \"to\" : [ \"sequence_end\" ]\r\n" +
				"      } ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"Association\",\r\n" +
				"      \"connects\" : [ {\r\n" +
				"        \"from\" : \"sequence_start\",\r\n" +
				"        \"to\" : [ \"TextAnnotation\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"sequence_end\",\r\n" +
				"        \"to\" : [ \"TextAnnotation\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"TextAnnotation\",\r\n" +
				"        \"to\" : [ \"sequence_end\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"BoundaryCompensationEvent\",\r\n" +
				"        \"to\" : [ \"sequence_end\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"TextAnnotation\",\r\n" +
				"        \"to\" : [ \"sequence_start\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"BoundaryCompensationEvent\",\r\n" +
				"        \"to\" : [ \"sequence_start\" ]\r\n" +
				"      } ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"DataAssociation\",\r\n" +
				"      \"connects\" : [ {\r\n" +
				"        \"from\" : \"sequence_start\",\r\n" +
				"        \"to\" : [ \"DataStore\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"sequence_end\",\r\n" +
				"        \"to\" : [ \"DataStore\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"DataStore\",\r\n" +
				"        \"to\" : [ \"sequence_end\" ]\r\n" +
				"      }, {\r\n" +
				"        \"from\" : \"DataStore\",\r\n" +
				"        \"to\" : [ \"sequence_start\" ]\r\n" +
				"      } ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"IntermediateEventOnActivityBoundary\",\r\n" +
				"      \"connects\" : [ {\r\n" +
				"        \"from\" : \"Activity\",\r\n" +
				"        \"to\" : [ \"IntermediateEventOnActivityBoundary\" ]\r\n" +
				"      } ]\r\n" +
				"    } ],\r\n" +
				"    \"containmentRules\" : [ {\r\n" +
				"      \"role\" : \"BPMNDiagram\",\r\n" +
				"      \"contains\" : [ \"all\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"SubProcess\",\r\n" +
				"      \"contains\" : [ \"sequence_start\", \"sequence_end\", \"from_task_event\", \"to_task_event\", \"EventSubProcess\", \"TextAnnotation\", \"DataStore\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"EventSubProcess\",\r\n" +
				"      \"contains\" : [ \"sequence_start\", \"sequence_end\", \"from_task_event\", \"to_task_event\", \"TextAnnotation\", \"DataStore\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"Pool\",\r\n" +
				"      \"contains\" : [ \"Lane\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"Lane\",\r\n" +
				"      \"contains\" : [ \"sequence_start\", \"sequence_end\", \"EventSubProcess\", \"TextAnnotation\", \"DataStore\" ]\r\n" +
				"    } ],\r\n" +
				"    \"morphingRules\" : [ {\r\n" +
				"      \"role\" : \"ActivitiesMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"UserTask\" ],\r\n" +
				"      \"preserveBounds\" : true\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"GatewaysMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"ExclusiveGateway\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"StartEventsMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"StartNoneEvent\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"EndEventsMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"StartNoneEvent\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"CatchEventsMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"CatchTimerEvent\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"ThrowEventsMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"ThrowNoneEvent\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"BoundaryEventsMorph\",\r\n" +
				"      \"baseMorphs\" : [ \"ThrowNoneEvent\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"BoundaryCompensationEvent\",\r\n" +
				"      \"baseMorphs\" : [ \"BoundaryCompensationEvent\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"TextAnnotation\",\r\n" +
				"      \"baseMorphs\" : [ \"TextAnnotation\" ]\r\n" +
				"    }, {\r\n" +
				"      \"role\" : \"DataStore\",\r\n" +
				"      \"baseMorphs\" : [ \"DataStore\" ]\r\n" +
				"    } ]\r\n" +
				"  }\r\n" +
				"}";

			var data = JSON.parse(strStecli);
			console.log(data);

			var quickMenuDefinition = ['UserTask', 'EndNoneEvent', 'ExclusiveGateway',
				'CatchTimerEvent', 'ThrowNoneEvent', 'TextAnnotation',
				'SequenceFlow', 'Association'
			];
			var ignoreForPaletteDefinition = ['SequenceFlow', 'MessageFlow', 'Association', 'DataAssociation', 'DataStore', 'SendTask'];
			var quickMenuItems = [];

			var morphRoles = [];
			for(var i = 0; i < data.rules.morphingRules.length; i++) {
				var role = data.rules.morphingRules[i].role;
				var roleItem = {
					'role': role,
					'morphOptions': []
				};
				morphRoles.push(roleItem);
			}

			// Check all received items
			for(var stencilIndex = 0; stencilIndex < data.stencils.length; stencilIndex++) {
				// Check if the root group is the 'diagram' group. If so, this item should not be shown.
				var currentGroupName = data.stencils[stencilIndex].groups[0];
				if(currentGroupName === 'Diagram' || currentGroupName === 'Form') {
					continue; // go to next item
				}

				var removed = false;
				if(data.stencils[stencilIndex].removed) {
					removed = true;
				}

				var currentGroup = undefined;
				if(!removed) {
					// Check if this group already exists. If not, we create a new one

					if(currentGroupName !== null && currentGroupName !== undefined && currentGroupName.length > 0) {

						currentGroup = findGroup(currentGroupName, stencilItemGroups); // Find group in root groups array
						if(currentGroup === null) {
							currentGroup = addGroup(currentGroupName, stencilItemGroups);
						}

						// Add all child groups (if any)
						for(var groupIndex = 1; groupIndex < data.stencils[stencilIndex].groups.length; groupIndex++) {
							var childGroupName = data.stencils[stencilIndex].groups[groupIndex];
							var childGroup = findGroup(childGroupName, currentGroup.groups);
							if(childGroup === null) {
								childGroup = addGroup(childGroupName, currentGroup.groups);
							}

							// The current group variable holds the parent of the next group (if any),
							// and is basically the last element in the array of groups defined in the stencil item
							currentGroup = childGroup;

						}

					}
				}

				// Construct the stencil item
				var stencilItem = {
					'id': data.stencils[stencilIndex].id,
					'name': data.stencils[stencilIndex].title,
					'description': data.stencils[stencilIndex].description,
					'icon': data.stencils[stencilIndex].icon,
					'type': data.stencils[stencilIndex].type,
					'roles': data.stencils[stencilIndex].roles,
					'removed': removed,
					'customIcon': false,
					'canConnect': false,
					'canConnectTo': false,
					'canConnectAssociation': false
				};

				if(data.stencils[stencilIndex].customIconId && data.stencils[stencilIndex].customIconId > 0) {
					stencilItem.customIcon = true;
					stencilItem.icon = data.stencils[stencilIndex].customIconId;
				}

				if(!removed) {
					if(quickMenuDefinition.indexOf(stencilItem.id) >= 0) {
						quickMenuItems[quickMenuDefinition.indexOf(stencilItem.id)] = stencilItem;
					}
				}

				if(stencilItem.id === 'TextAnnotation' || stencilItem.id === 'BoundaryCompensationEvent') {
					stencilItem.canConnectAssociation = true;
				}

				for(var i = 0; i < data.stencils[stencilIndex].roles.length; i++) {
					var stencilRole = data.stencils[stencilIndex].roles[i];
					if(stencilRole === 'sequence_start') {
						stencilItem.canConnect = true;
					} else if(stencilRole === 'sequence_end') {
						stencilItem.canConnectTo = true;
					}

					for(var j = 0; j < morphRoles.length; j++) {
						if(stencilRole === morphRoles[j].role) {
							if(!removed) {
								morphRoles[j].morphOptions.push(stencilItem);
							}
							stencilItem.morphRole = morphRoles[j].role;
							break;
						}
					}
				}

				if(currentGroup) {
					// Add the stencil item to the correct group
					currentGroup.items.push(stencilItem);
					if(ignoreForPaletteDefinition.indexOf(stencilItem.id) < 0) {
						currentGroup.paletteItems.push(stencilItem);
					}

				} else {
					// It's a root stencil element
					if(!removed) {
						stencilItemGroups.push(stencilItem);
					}
				}
			}

			for(var i = 0; i < stencilItemGroups.length; i++) {
				if(stencilItemGroups[i].paletteItems && stencilItemGroups[i].paletteItems.length == 0) {
					stencilItemGroups[i].visible = false;
				}
			}

			$scope.stencilItemGroups = stencilItemGroups;

			var containmentRules = [];
			for(var i = 0; i < data.rules.containmentRules.length; i++) {
				var rule = data.rules.containmentRules[i];
				containmentRules.push(rule);
			}
			$scope.containmentRules = containmentRules;

			// remove quick menu items which are not available anymore due to custom pallette
			var availableQuickMenuItems = [];
			for(var i = 0; i < quickMenuItems.length; i++) {
				if(quickMenuItems[i]) {
					availableQuickMenuItems[availableQuickMenuItems.length] = quickMenuItems[i];
				}
			}

			$scope.quickMenuItems = availableQuickMenuItems;
			$scope.morphRoles = morphRoles;

			/*
			 StencilSet items
			 */
			/*$http({method: 'GET', url: KISBPM.URL.getStencilSet()}).success(function (data, status, headers, config) {

            	var quickMenuDefinition = ['UserTask', 'EndNoneEvent', 'ExclusiveGateway', 
            	                           'CatchTimerEvent', 'ThrowNoneEvent', 'TextAnnotation',
            	                           'SequenceFlow', 'Association'];
            	var ignoreForPaletteDefinition = ['SequenceFlow', 'MessageFlow', 'Association', 'DataAssociation', 'DataStore', 'SendTask'];
            	var quickMenuItems = [];
            	
            	var morphRoles = [];
                for (var i = 0; i < data.rules.morphingRules.length; i++) 
                {
                    var role = data.rules.morphingRules[i].role;
                    var roleItem = {'role': role, 'morphOptions': []};
                    morphRoles.push(roleItem);
                }
            	
                // Check all received items
                for (var stencilIndex = 0; stencilIndex < data.stencils.length; stencilIndex++) 
                {
                	// Check if the root group is the 'diagram' group. If so, this item should not be shown.
                    var currentGroupName = data.stencils[stencilIndex].groups[0];
                    if (currentGroupName === 'Diagram' || currentGroupName === 'Form') {
                        continue;  // go to next item
                    }
                    
                    var removed = false;
                    if (data.stencils[stencilIndex].removed) {
                        removed = true;
                    }

                    var currentGroup = undefined;
                    if (!removed) {
                        // Check if this group already exists. If not, we create a new one

                        if (currentGroupName !== null && currentGroupName !== undefined && currentGroupName.length > 0) {

                            currentGroup = findGroup(currentGroupName, stencilItemGroups); // Find group in root groups array
                            if (currentGroup === null) {
                                currentGroup = addGroup(currentGroupName, stencilItemGroups);
                            }

                            // Add all child groups (if any)
                            for (var groupIndex = 1; groupIndex < data.stencils[stencilIndex].groups.length; groupIndex++) {
                                var childGroupName = data.stencils[stencilIndex].groups[groupIndex];
                                var childGroup = findGroup(childGroupName, currentGroup.groups);
                                if (childGroup === null) {
                                    childGroup = addGroup(childGroupName, currentGroup.groups);
                                }

                                // The current group variable holds the parent of the next group (if any),
                                // and is basically the last element in the array of groups defined in the stencil item
                                currentGroup = childGroup;

                            }

                        }
                    }
                    
                    // Construct the stencil item
                    var stencilItem = {'id': data.stencils[stencilIndex].id,
                        'name': data.stencils[stencilIndex].title,
                        'description': data.stencils[stencilIndex].description,
                        'icon': data.stencils[stencilIndex].icon,
                        'type': data.stencils[stencilIndex].type,
                        'roles': data.stencils[stencilIndex].roles,
                        'removed': removed,
                        'customIcon': false,
                        'canConnect': false,
                        'canConnectTo': false,
                        'canConnectAssociation': false};
                    
                    if (data.stencils[stencilIndex].customIconId && data.stencils[stencilIndex].customIconId > 0) {
                        stencilItem.customIcon = true;
                        stencilItem.icon = data.stencils[stencilIndex].customIconId;
                    }
                    
                    if (!removed) {
                        if (quickMenuDefinition.indexOf(stencilItem.id) >= 0) {
                        	quickMenuItems[quickMenuDefinition.indexOf(stencilItem.id)] = stencilItem;
                        }
                    }
                    
                    if (stencilItem.id === 'TextAnnotation' || stencilItem.id === 'BoundaryCompensationEvent') {
                    	stencilItem.canConnectAssociation = true;
                    }
                    
                    for (var i = 0; i < data.stencils[stencilIndex].roles.length; i++) {
                    	var stencilRole = data.stencils[stencilIndex].roles[i];
                    	if (stencilRole === 'sequence_start') {
                    		stencilItem.canConnect = true;
                    	} else if (stencilRole === 'sequence_end') {
                    		stencilItem.canConnectTo = true;
                    	}
                    	
                    	for (var j = 0; j < morphRoles.length; j++) {
                    		if (stencilRole === morphRoles[j].role) {
                    		    if (!removed) {
                    			     morphRoles[j].morphOptions.push(stencilItem);
                    			}
                    			stencilItem.morphRole = morphRoles[j].role;
                    			break;
                    		}
                    	}
                    }

                    if (currentGroup) {
	                    // Add the stencil item to the correct group
	                    currentGroup.items.push(stencilItem);
	                    if (ignoreForPaletteDefinition.indexOf(stencilItem.id) < 0) {
	                    	currentGroup.paletteItems.push(stencilItem);
	                    }

                    } else {
                        // It's a root stencil element
                        if (!removed) {
                            stencilItemGroups.push(stencilItem);
                        }
                    }
                }
                
                for (var i = 0; i < stencilItemGroups.length; i++) 
                {
                	if (stencilItemGroups[i].paletteItems && stencilItemGroups[i].paletteItems.length == 0)
                	{
                		stencilItemGroups[i].visible = false;
                	}
                }
                
                $scope.stencilItemGroups = stencilItemGroups;

                var containmentRules = [];
                for (var i = 0; i < data.rules.containmentRules.length; i++) 
                {
                    var rule = data.rules.containmentRules[i];
                    containmentRules.push(rule);
                }
                $scope.containmentRules = containmentRules;
                
                // remove quick menu items which are not available anymore due to custom pallette
                var availableQuickMenuItems = [];
                for (var i = 0; i < quickMenuItems.length; i++) 
                {
                    if (quickMenuItems[i]) {
                        availableQuickMenuItems[availableQuickMenuItems.length] = quickMenuItems[i];
                    }
                }
                
                $scope.quickMenuItems = availableQuickMenuItems;
                $scope.morphRoles = morphRoles;
            }).
            
            error(function (data, status, headers, config) {
                console.log('Something went wrong when fetching stencil items:' + JSON.stringify(data));
            });*/

			/*
			 * Listen to selection change events: show properties
			 */
			$scope.editor.registerOnEvent(ORYX.CONFIG.EVENT_SELECTION_CHANGED, function(event) {
				var shapes = event.elements;
				var canvasSelected = false;
				if(shapes && shapes.length == 0) {
					shapes = [$scope.editor.getCanvas()];
					canvasSelected = true;
				}
				if(shapes && shapes.length > 0) {

					var selectedShape = shapes.first();
					var stencil = selectedShape.getStencil();

					if($rootScope.selectedElementBeforeScrolling && stencil.id().indexOf('BPMNDiagram') !== -1) {
						// ignore canvas event because of empty selection when scrolling stops
						return;
					}

					if($rootScope.selectedElementBeforeScrolling && $rootScope.selectedElementBeforeScrolling.getId() === selectedShape.getId()) {
						$rootScope.selectedElementBeforeScrolling = null;
						return;
					}

					// Store previous selection
					$scope.previousSelectedShape = $scope.selectedShape;

					// Only do something if another element is selected (Oryx fires this event multiple times)
					if($scope.selectedShape !== undefined && $scope.selectedShape.getId() === selectedShape.getId()) {
						if($rootScope.forceSelectionRefresh) {
							// Switch the flag again, this run will force refresh
							$rootScope.forceSelectionRefresh = false;
						} else {
							// Selected the same element again, no need to update anything
							return;
						}
					}

					var selectedItem = {
						'title': '',
						'properties': []
					};

					if(canvasSelected) {
						selectedItem.auditData = {
							'author': $scope.modelData.createdByUser,
							'createDate': $scope.modelData.createDate
						};
					}

					// Gather properties of selected item
					var properties = stencil.properties();
					for(var i = 0; i < properties.length; i++) {
						var property = properties[i];
						if(property.popular() == false) continue;
						var key = property.prefix() + "-" + property.id();

						if(key === 'oryx-name') {
							selectedItem.title = selectedShape.properties[key];
						}

						// First we check if there is a config for 'key-type' and then for 'type' alone
						var propertyConfig = KISBPM.PROPERTY_CONFIG[key + '-' + property.type()];
						if(propertyConfig === undefined || propertyConfig === null) {
							propertyConfig = KISBPM.PROPERTY_CONFIG[property.type()];
						}

						if(propertyConfig === undefined || propertyConfig === null) {
							console.log('WARNING: no property configuration defined for ' + key + ' of type ' + property.type());
						} else {

							if(selectedShape.properties[key] === 'true') {
								selectedShape.properties[key] = true;
							}

							if(KISBPM.CONFIG.showRemovedProperties == false && property.isHidden()) {
								continue;
							}

							var currentProperty = {
								'key': key,
								'title': property.title(),
								'type': property.type(),
								'mode': 'read',
								'hidden': property.isHidden(),
								'value': selectedShape.properties[key]
							};

							if((currentProperty.type === 'complex' || currentProperty.type === 'multiplecomplex') && currentProperty.value && currentProperty.value.length > 0) {
								try {
									currentProperty.value = JSON.parse(currentProperty.value);
								} catch(err) {
									// ignore
								}
							}

							if(propertyConfig.readModeTemplateUrl !== undefined && propertyConfig.readModeTemplateUrl !== null) {
								currentProperty.readModeTemplateUrl = propertyConfig.readModeTemplateUrl + '?version=' + $rootScope.staticIncludeVersion;
							}
							if(propertyConfig.writeModeTemplateUrl !== null && propertyConfig.writeModeTemplateUrl !== null) {
								currentProperty.writeModeTemplateUrl = propertyConfig.writeModeTemplateUrl + '?version=' + $rootScope.staticIncludeVersion;
							}

							if(propertyConfig.templateUrl !== undefined && propertyConfig.templateUrl !== null) {
								currentProperty.templateUrl = propertyConfig.templateUrl + '?version=' + $rootScope.staticIncludeVersion;
								currentProperty.hasReadWriteMode = false;
							} else {
								currentProperty.hasReadWriteMode = true;
							}

							if(currentProperty.value === undefined ||
								currentProperty.value === null ||
								currentProperty.value.length == 0) {
								currentProperty.noValue = true;
							}

							selectedItem.properties.push(currentProperty);
						}
					}

					// Need to wrap this in an $apply block, see http://jimhoskins.com/2012/12/17/angularjs-and-apply.html
					$scope.safeApply(function() {
						$scope.selectedItem = selectedItem;
						$scope.selectedShape = selectedShape;
					});

				} else {
					$scope.safeApply(function() {
						$scope.selectedItem = {};
						$scope.selectedShape = null;
					});
				}
			});

			$scope.editor.registerOnEvent(ORYX.CONFIG.EVENT_SELECTION_CHANGED, function(event) {

				KISBPM.eventBus.dispatch(KISBPM.eventBus.EVENT_TYPE_HIDE_SHAPE_BUTTONS);
				var shapes = event.elements;

				if(shapes && shapes.length == 1) {

					var selectedShape = shapes.first();

					var a = $scope.editor.getCanvas().node.getScreenCTM();

					var absoluteXY = selectedShape.absoluteXY();

					absoluteXY.x *= a.a;
					absoluteXY.y *= a.d;

					var additionalIEZoom = 1;
					if(!isNaN(screen.logicalXDPI) && !isNaN(screen.systemXDPI)) {
						var ua = navigator.userAgent;
						if(ua.indexOf('MSIE') >= 0) {
							//IE 10 and below
							var zoom = Math.round((screen.deviceXDPI / screen.logicalXDPI) * 100);
							if(zoom !== 100) {
								additionalIEZoom = zoom / 100
							}
						}
					}

					if(additionalIEZoom === 1) {
						absoluteXY.y = absoluteXY.y - jQuery("#canvasSection").offset().top + 5;
						absoluteXY.x = absoluteXY.x - jQuery("#canvasSection").offset().left;

					} else {
						var canvasOffsetLeft = jQuery("#canvasSection").offset().left;
						var canvasScrollLeft = jQuery("#canvasSection").scrollLeft();
						var canvasScrollTop = jQuery("#canvasSection").scrollTop();

						var offset = a.e - (canvasOffsetLeft * additionalIEZoom);
						var additionaloffset = 0;
						if(offset > 10) {
							additionaloffset = (offset / additionalIEZoom) - offset;
						}
						absoluteXY.y = absoluteXY.y - (jQuery("#canvasSection").offset().top * additionalIEZoom) + 5 + ((canvasScrollTop * additionalIEZoom) - canvasScrollTop);
						absoluteXY.x = absoluteXY.x - (canvasOffsetLeft * additionalIEZoom) + additionaloffset + ((canvasScrollLeft * additionalIEZoom) - canvasScrollLeft);
					}

					var bounds = new ORYX.Core.Bounds(a.e + absoluteXY.x, a.f + absoluteXY.y, a.e + absoluteXY.x + a.a * selectedShape.bounds.width(), a.f + absoluteXY.y + a.d * selectedShape.bounds.height());
					var shapeXY = bounds.upperLeft();

					var stencilItem = $scope.getStencilItemById(selectedShape.getStencil().idWithoutNs());
					var morphShapes = [];
					if(stencilItem && stencilItem.morphRole) {
						for(var i = 0; i < $scope.morphRoles.length; i++) {
							if($scope.morphRoles[i].role === stencilItem.morphRole) {
								morphShapes = $scope.morphRoles[i].morphOptions;
							}
						}
					}

					var x = shapeXY.x;
					if(bounds.width() < 48) {
						x -= 24;
					}

					if(morphShapes && morphShapes.length > 0) {
						// In case the element is not wide enough, start the 2 bottom-buttons more to the left
						// to prevent overflow in the right-menu
						var morphButton = document.getElementById('morph-button');
						morphButton.style.display = "block";
						morphButton.style.left = x + 24 + 'px';
						morphButton.style.top = (shapeXY.y + bounds.height() + 2) + 'px';
					}

					var deleteButton = document.getElementById('delete-button');
					deleteButton.style.display = "block";
					deleteButton.style.left = x + 'px';
					deleteButton.style.top = (shapeXY.y + bounds.height() + 2) + 'px';

					if(stencilItem && (stencilItem.canConnect || stencilItem.canConnectAssociation)) {
						var quickButtonCounter = 0;
						var quickButtonX = shapeXY.x + bounds.width() + 5;
						var quickButtonY = shapeXY.y;
						jQuery('.Oryx_button').each(function(i, obj) {
							if(obj.id !== 'morph-button' && obj.id != 'delete-button') {
								quickButtonCounter++;
								if(quickButtonCounter > 3) {
									quickButtonX = shapeXY.x + bounds.width() + 5;
									quickButtonY += 24;
									quickButtonCounter = 1;

								} else if(quickButtonCounter > 1) {
									quickButtonX += 24;
								}
								obj.style.display = "block";
								obj.style.left = quickButtonX + 'px';
								obj.style.top = quickButtonY + 'px';
							}
						});
					}
				}
			});

			if(!$rootScope.stencilInitialized) {
				KISBPM.eventBus.addListener(KISBPM.eventBus.EVENT_TYPE_HIDE_SHAPE_BUTTONS, function(event) {
					jQuery('.Oryx_button').each(function(i, obj) {
						obj.style.display = "none";
					});
				});

				/*
				 * Listen to property updates and act upon them
				 */
				KISBPM.eventBus.addListener(KISBPM.eventBus.EVENT_TYPE_PROPERTY_VALUE_CHANGED, function(event) {
					if(event.property && event.property.key) {
						// If the name property is been updated, we also need to change the title of the currently selected item
						if(event.property.key === 'oryx-name' && $scope.selectedItem !== undefined && $scope.selectedItem !== null) {
							$scope.selectedItem.title = event.newValue;
						}

						// Update "no value" flag
						event.property.noValue = (event.property.value === undefined ||
							event.property.value === null ||
							event.property.value.length == 0);
					}
				});

				$rootScope.stencilInitialized = true;
			}

			$scope.morphShape = function() {
				$scope.safeApply(function() {

					var shapes = $rootScope.editor.getSelection();
					if(shapes && shapes.length == 1) {
						$rootScope.currentSelectedShape = shapes.first();
						var stencilItem = $scope.getStencilItemById($rootScope.currentSelectedShape.getStencil().idWithoutNs());
						var morphShapes = [];
						for(var i = 0; i < $scope.morphRoles.length; i++) {
							if($scope.morphRoles[i].role === stencilItem.morphRole) {
								morphShapes = $scope.morphRoles[i].morphOptions.slice();
							}
						}

						// Method to open shape select dialog (used later on)
						var showSelectShapeDialog = function() {
							$rootScope.morphShapes = morphShapes;
							$modal({
								backdrop: false,
								keyboard: true,
								template: 'editor-app/popups/select-shape.html?version=' + Date.now()
							});
						};

						showSelectShapeDialog();
					}
				});
			};

			$scope.deleteShape = function() {
				KISBPM.TOOLBAR.ACTIONS.deleteItem({
					'$scope': $scope
				});
			};

			$scope.quickAddItem = function(newItemId) {
				$scope.safeApply(function() {

					var shapes = $rootScope.editor.getSelection();
					if(shapes && shapes.length == 1) {
						$rootScope.currentSelectedShape = shapes.first();

						var containedStencil = undefined;
						var stencilSets = $scope.editor.getStencilSets().values();
						for(var i = 0; i < stencilSets.length; i++) {
							var stencilSet = stencilSets[i];
							var nodes = stencilSet.nodes();
							for(var j = 0; j < nodes.length; j++) {
								if(nodes[j].idWithoutNs() === newItemId) {
									containedStencil = nodes[j];
									break;
								}
							}
						}

						if(!containedStencil) return;

						var option = {
							type: $scope.currentSelectedShape.getStencil().namespace() + newItemId,
							namespace: $scope.currentSelectedShape.getStencil().namespace()
						};
						option['connectedShape'] = $rootScope.currentSelectedShape;
						option['parent'] = $rootScope.currentSelectedShape.parent;
						option['containedStencil'] = containedStencil;

						var args = {
							sourceShape: $rootScope.currentSelectedShape,
							targetStencil: containedStencil
						};
						var targetStencil = $scope.editor.getRules().connectMorph(args);
						if(!targetStencil) {
							return;
						} // Check if there can be a target shape
						option['connectingType'] = targetStencil.id();

						var command = new KISBPM.CreateCommand(option, undefined, undefined, $scope.editor);

						$scope.editor.executeCommands([command]);
					}
				});
			};

		}); // end of $scope.editorFactory.promise block

		/* Click handler for clicking a property */
		$scope.propertyClicked = function(index) {
			if(!$scope.selectedItem.properties[index].hidden) {
				$scope.selectedItem.properties[index].mode = "write";
			}
		};

		/* Helper method to retrieve the template url for a property */
		$scope.getPropertyTemplateUrl = function(index) {
			return $scope.selectedItem.properties[index].templateUrl;
		};
		$scope.getPropertyReadModeTemplateUrl = function(index) {
			return $scope.selectedItem.properties[index].readModeTemplateUrl;
		};
		$scope.getPropertyWriteModeTemplateUrl = function(index) {
			return $scope.selectedItem.properties[index].writeModeTemplateUrl;
		};

		/* Method available to all sub controllers (for property controllers) to update the internal Oryx model */
		$scope.updatePropertyInModel = function(property, shapeId) {

			var shape = $scope.selectedShape;
			// Some updates may happen when selected shape is already changed, so when an additional
			// shapeId is supplied, we need to make sure the correct shape is updated (current or previous)
			if(shapeId) {
				if(shape.id != shapeId && $scope.previousSelectedShape && $scope.previousSelectedShape.id == shapeId) {
					shape = $scope.previousSelectedShape;
				} else {
					shape = null;
				}
			}

			if(!shape) {
				// When no shape is selected, or no shape is found for the alternative
				// shape ID, do nothing
				return;
			}
			var key = property.key;
			var newValue = property.value;
			var oldValue = shape.properties[key];

			if(newValue != oldValue) {
				var commandClass = ORYX.Core.Command.extend({
					construct: function() {
						this.key = key;
						this.oldValue = oldValue;
						this.newValue = newValue;
						this.shape = shape;
						this.facade = $scope.editor;
					},
					execute: function() {
						this.shape.setProperty(this.key, this.newValue);
						this.facade.getCanvas().update();
						this.facade.updateSelection();
					},
					rollback: function() {
						this.shape.setProperty(this.key, this.oldValue);
						this.facade.getCanvas().update();
						this.facade.updateSelection();
					}
				});
				// Instantiate the class
				var command = new commandClass();

				// Execute the command
				$scope.editor.executeCommands([command]);
				$scope.editor.handleEvents({
					type: ORYX.CONFIG.EVENT_PROPWINDOW_PROP_CHANGED,
					elements: [shape],
					key: key
				});

				// Switch the property back to read mode, now the update is done
				property.mode = 'read';

				// Fire event to all who is interested
				// Fire event to all who want to know about this
				var event = {
					type: KISBPM.eventBus.EVENT_TYPE_PROPERTY_VALUE_CHANGED,
					property: property,
					oldValue: oldValue,
					newValue: newValue
				};
				KISBPM.eventBus.dispatch(event.type, event);
			} else {
				// Switch the property back to read mode, no update was needed
				property.mode = 'read';
			}

		};

		/**
		 * Helper method that searches a group for an item with the given id.
		 * If not found, will return undefined.
		 */
		$scope.findStencilItemInGroup = function(stencilItemId, group) {

			var item;

			// Check all items directly in this group
			for(var j = 0; j < group.items.length; j++) {
				item = group.items[j];
				if(item.id === stencilItemId) {
					return item;
				}
			}

			// Check the child groups
			if(group.groups && group.groups.length > 0) {
				for(var k = 0; k < group.groups.length; k++) {
					item = $scope.findStencilItemInGroup(stencilItemId, group.groups[k]);
					if(item) {
						return item;
					}
				}
			}

			return undefined;
		};

		/**
		 * Helper method to find a stencil item.
		 */
		$scope.getStencilItemById = function(stencilItemId) {
			for(var i = 0; i < $scope.stencilItemGroups.length; i++) {
				var element = $scope.stencilItemGroups[i];

				// Real group
				if(element.items !== null && element.items !== undefined) {
					var item = $scope.findStencilItemInGroup(stencilItemId, element);
					if(item) {
						return item;
					}
				} else { // Root stencil item
					if(element.id === stencilItemId) {
						return element;
					}
				}
			}
			return undefined;
		};

		/*
		 * DRAG AND DROP FUNCTIONALITY
		 */

		$scope.dropCallback = function(event, ui) {

			$scope.editor.handleEvents({
				type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
				highlightId: "shapeRepo.attached"
			});
			$scope.editor.handleEvents({
				type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
				highlightId: "shapeRepo.added"
			});

			$scope.editor.handleEvents({
				type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
				highlightId: "shapeMenu"
			});

			KISBPM.eventBus.dispatch(KISBPM.eventBus.EVENT_TYPE_HIDE_SHAPE_BUTTONS);

			if($scope.dragCanContain) {

				var item = $scope.getStencilItemById(ui.draggable[0].id);

				var pos = {
					x: event.pageX,
					y: event.pageY
				};

				var additionalIEZoom = 1;
				if(!isNaN(screen.logicalXDPI) && !isNaN(screen.systemXDPI)) {
					var ua = navigator.userAgent;
					if(ua.indexOf('MSIE') >= 0) {
						//IE 10 and below
						var zoom = Math.round((screen.deviceXDPI / screen.logicalXDPI) * 100);
						if(zoom !== 100) {
							additionalIEZoom = zoom / 100;
						}
					}
				}

				var screenCTM = $scope.editor.getCanvas().node.getScreenCTM();

				// Correcting the UpperLeft-Offset
				pos.x -= (screenCTM.e / additionalIEZoom);
				pos.y -= (screenCTM.f / additionalIEZoom);
				// Correcting the Zoom-Factor
				pos.x /= screenCTM.a;
				pos.y /= screenCTM.d;

				// Correcting the ScrollOffset
				pos.x -= document.documentElement.scrollLeft;
				pos.y -= document.documentElement.scrollTop;

				var parentAbs = $scope.dragCurrentParent.absoluteXY();
				pos.x -= parentAbs.x;
				pos.y -= parentAbs.y;

				var containedStencil = undefined;
				var stencilSets = $scope.editor.getStencilSets().values();
				for(var i = 0; i < stencilSets.length; i++) {
					var stencilSet = stencilSets[i];
					var nodes = stencilSet.nodes();
					for(var j = 0; j < nodes.length; j++) {
						if(nodes[j].idWithoutNs() === ui.draggable[0].id) {
							containedStencil = nodes[j];
							break;
						}
					}

					if(!containedStencil) {
						var edges = stencilSet.edges();
						for(var j = 0; j < edges.length; j++) {
							if(edges[j].idWithoutNs() === ui.draggable[0].id) {
								containedStencil = edges[j];
								break;
							}
						}
					}
				}

				if(!containedStencil) return;

				if($scope.quickMenu) {
					var shapes = $scope.editor.getSelection();
					if(shapes && shapes.length == 1) {
						var currentSelectedShape = shapes.first();

						var option = {};
						option.type = currentSelectedShape.getStencil().namespace() + ui.draggable[0].id;
						option.namespace = currentSelectedShape.getStencil().namespace();
						option.connectedShape = currentSelectedShape;
						option.parent = $scope.dragCurrentParent;
						option.containedStencil = containedStencil;

						// If the ctrl key is not pressed, 
						// snapp the new shape to the center 
						// if it is near to the center of the other shape
						if(!event.ctrlKey) {
							// Get the center of the shape
							var cShape = currentSelectedShape.bounds.center();
							// Snapp +-20 Pixel horizontal to the center 
							if(20 > Math.abs(cShape.x - pos.x)) {
								pos.x = cShape.x;
							}
							// Snapp +-20 Pixel vertical to the center 
							if(20 > Math.abs(cShape.y - pos.y)) {
								pos.y = cShape.y;
							}
						}

						option.position = pos;

						if(containedStencil.idWithoutNs() !== 'SequenceFlow' && containedStencil.idWithoutNs() !== 'Association' &&
							containedStencil.idWithoutNs() !== 'MessageFlow' && containedStencil.idWithoutNs() !== 'DataAssociation') {
							var args = {
								sourceShape: currentSelectedShape,
								targetStencil: containedStencil
							};
							var targetStencil = $scope.editor.getRules().connectMorph(args);
							if(!targetStencil) {
								return;
							} // Check if there can be a target shape
							option.connectingType = targetStencil.id();
						}

						var command = new KISBPM.CreateCommand(option, $scope.dropTargetElement, pos, $scope.editor);

						$scope.editor.executeCommands([command]);
						if(ui.draggable[0].id === 'SequenceFlow') {
							console.log(option.connectedShape);
							console.log(option.parent);
							console.log(option.containedStencil);
						}
					}
				} else {
					var canAttach = false;
					if(containedStencil.idWithoutNs() === 'BoundaryErrorEvent' || containedStencil.idWithoutNs() === 'BoundaryTimerEvent' ||
						containedStencil.idWithoutNs() === 'BoundarySignalEvent' || containedStencil.idWithoutNs() === 'BoundaryMessageEvent' ||
						containedStencil.idWithoutNs() === 'BoundaryCancelEvent' || containedStencil.idWithoutNs() === 'BoundaryCompensationEvent') {
						// Modify position, otherwise boundary event will get position related to left corner of the canvas instead of the container
						pos = $scope.editor.eventCoordinates(event);
						canAttach = true;
					}

					var option = {};
					option['type'] = $scope.modelData.model.stencilset.namespace + item.id;
					option['namespace'] = $scope.modelData.model.stencilset.namespace;
					option['position'] = pos;
					option['parent'] = $scope.dragCurrentParent;

					var commandClass = ORYX.Core.Command.extend({
						construct: function(option, dockedShape, canAttach, position, facade) {
							this.option = option;
							this.docker = null;
							this.dockedShape = dockedShape;
							this.dockedShapeParent = dockedShape.parent || facade.getCanvas();
							this.position = position;
							this.facade = facade;
							this.selection = this.facade.getSelection();
							this.shape = null;
							this.parent = null;
							this.canAttach = canAttach;
						},
						execute: function() {
							if(!this.shape) {
								this.shape = this.facade.createShape(option);
								this.parent = this.shape.parent;
							} else if(this.parent) {
								this.parent.add(this.shape);
							}

							if(this.canAttach && this.shape.dockers && this.shape.dockers.length) {
								this.docker = this.shape.dockers[0];

								this.dockedShapeParent.add(this.docker.parent);

								// Set the Docker to the new Shape
								this.docker.setDockedShape(undefined);
								this.docker.bounds.centerMoveTo(this.position);
								if(this.dockedShape !== this.facade.getCanvas()) {
									this.docker.setDockedShape(this.dockedShape);
								}
								this.facade.setSelection([this.docker.parent]);
							}

							this.facade.getCanvas().update();
							this.facade.updateSelection();

						},
						rollback: function() {
							if(this.shape) {
								this.facade.setSelection(this.selection.without(this.shape));
								this.facade.deleteShape(this.shape);
							}
							if(this.canAttach && this.docker) {
								this.docker.setDockedShape(undefined);
							}
							this.facade.getCanvas().update();
							this.facade.updateSelection();

						}
					});

					// Update canvas
					var command = new commandClass(option, $scope.dragCurrentParent, canAttach, pos, $scope.editor);
					$scope.editor.executeCommands([command]);

					// Fire event to all who want to know about this
					var dropEvent = {
						type: KISBPM.eventBus.EVENT_TYPE_ITEM_DROPPED,
						droppedItem: item,
						position: pos
					};
					KISBPM.eventBus.dispatch(dropEvent.type, dropEvent);
				}
			}

			$scope.dragCurrentParent = undefined;
			$scope.dragCurrentParentId = undefined;
			$scope.dragCurrentParentStencil = undefined;
			$scope.dragCanContain = undefined;
			$scope.quickMenu = undefined;
			$scope.dropTargetElement = undefined;
		};

		$scope.overCallback = function(event, ui) {
			$scope.dragModeOver = true;
		};

		$scope.outCallback = function(event, ui) {
			$scope.dragModeOver = false;
		};

		$scope.startDragCallback = function(event, ui) {
			$scope.dragModeOver = false;
			$scope.quickMenu = false;
			if(!ui.helper.hasClass('stencil-item-dragged')) {
				ui.helper.addClass('stencil-item-dragged');
			}
		};

		$scope.startDragCallbackQuickMenu = function(event, ui) {
			$scope.dragModeOver = false;
			$scope.quickMenu = true;
		};

		$scope.dragCallback = function(event, ui) {

			if($scope.dragModeOver != false) {

				var coord = $scope.editor.eventCoordinatesXY(event.pageX, event.pageY);

				var additionalIEZoom = 1;
				if(!isNaN(screen.logicalXDPI) && !isNaN(screen.systemXDPI)) {
					var ua = navigator.userAgent;
					if(ua.indexOf('MSIE') >= 0) {
						//IE 10 and below
						var zoom = Math.round((screen.deviceXDPI / screen.logicalXDPI) * 100);
						if(zoom !== 100) {
							additionalIEZoom = zoom / 100
						}
					}
				}

				if(additionalIEZoom !== 1) {
					coord.x = coord.x / additionalIEZoom;
					coord.y = coord.y / additionalIEZoom;
				}

				var aShapes = $scope.editor.getCanvas().getAbstractShapesAtPosition(coord);

				if(aShapes.length <= 0) {
					if(event.helper) {
						$scope.dragCanContain = false;
						return false;
					}
				}

				if(aShapes[0] instanceof ORYX.Core.Canvas) {
					$scope.editor.getCanvas().setHightlightStateBasedOnX(coord.x);
				}

				if(aShapes.length == 1 && aShapes[0] instanceof ORYX.Core.Canvas) {
					var parentCandidate = aShapes[0];

					$scope.dragCanContain = true;
					$scope.dragCurrentParent = parentCandidate;
					$scope.dragCurrentParentId = parentCandidate.id;

					$scope.editor.handleEvents({
						type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
						highlightId: "shapeRepo.attached"
					});
					$scope.editor.handleEvents({
						type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
						highlightId: "shapeRepo.added"
					});
					return false;
				} else {
					var item = $scope.getStencilItemById(event.target.id);

					var parentCandidate = aShapes.reverse().find(function(candidate) {
						return(candidate instanceof ORYX.Core.Canvas ||
							candidate instanceof ORYX.Core.Node ||
							candidate instanceof ORYX.Core.Edge);
					});

					if(!parentCandidate) {
						$scope.dragCanContain = false;
						return false;
					}

					if(item.type === "node") {

						// check if the draggable is a boundary event and the parent an Activity
						var _canContain = false;
						var parentStencilId = parentCandidate.getStencil().id();

						if($scope.dragCurrentParentId && $scope.dragCurrentParentId === parentCandidate.id) {
							return false;
						}

						var parentItem = $scope.getStencilItemById(parentCandidate.getStencil().idWithoutNs());
						if(parentItem.roles.indexOf("Activity") > -1) {
							if(item.roles.indexOf("IntermediateEventOnActivityBoundary") > -1) {
								_canContain = true;
							}
						} else if(parentCandidate.getStencil().idWithoutNs() === 'Pool') {
							if(item.id === 'Lane') {
								_canContain = true;
							}
						}

						if(_canContain) {
							$scope.editor.handleEvents({
								type: ORYX.CONFIG.EVENT_HIGHLIGHT_SHOW,
								highlightId: "shapeRepo.attached",
								elements: [parentCandidate],
								style: ORYX.CONFIG.SELECTION_HIGHLIGHT_STYLE_RECTANGLE,
								color: ORYX.CONFIG.SELECTION_VALID_COLOR
							});

							$scope.editor.handleEvents({
								type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
								highlightId: "shapeRepo.added"
							});
						} else {
							for(var i = 0; i < $scope.containmentRules.length; i++) {
								var rule = $scope.containmentRules[i];
								if(rule.role === parentItem.id) {
									for(var j = 0; j < rule.contains.length; j++) {
										if(item.roles.indexOf(rule.contains[j]) > -1) {
											_canContain = true;
											break;
										}
									}

									if(_canContain) {
										break;
									}
								}
							}

							// Show Highlight
							$scope.editor.handleEvents({
								type: ORYX.CONFIG.EVENT_HIGHLIGHT_SHOW,
								highlightId: 'shapeRepo.added',
								elements: [parentCandidate],
								color: _canContain ? ORYX.CONFIG.SELECTION_VALID_COLOR : ORYX.CONFIG.SELECTION_INVALID_COLOR
							});

							$scope.editor.handleEvents({
								type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
								highlightId: "shapeRepo.attached"
							});
						}

						$scope.dragCurrentParent = parentCandidate;
						$scope.dragCurrentParentId = parentCandidate.id;
						$scope.dragCurrentParentStencil = parentStencilId;
						$scope.dragCanContain = _canContain;

					} else {
						var canvasCandidate = $scope.editor.getCanvas();
						var canConnect = false;

						var targetStencil = $scope.getStencilItemById(parentCandidate.getStencil().idWithoutNs());
						if(targetStencil) {
							var associationConnect = false;
							if(stencil.idWithoutNs() === 'Association' && (curCan.getStencil().idWithoutNs() === 'TextAnnotation' || curCan.getStencil().idWithoutNs() === 'BoundaryCompensationEvent')) {
								associationConnect = true;
							} else if(stencil.idWithoutNs() === 'DataAssociation' && curCan.getStencil().idWithoutNs() === 'DataStore') {
								associationConnect = true;
							}

							if(targetStencil.canConnectTo || associationConnect) {
								canConnect = true;
							}
						}

						//Edge
						$scope.dragCurrentParent = canvasCandidate;
						$scope.dragCurrentParentId = canvasCandidate.id;
						$scope.dragCurrentParentStencil = canvasCandidate.getStencil().id();
						$scope.dragCanContain = canConnect;

						// Show Highlight
						$scope.editor.handleEvents({
							type: ORYX.CONFIG.EVENT_HIGHLIGHT_SHOW,
							highlightId: 'shapeRepo.added',
							elements: [canvasCandidate],
							color: ORYX.CONFIG.SELECTION_VALID_COLOR
						});

						$scope.editor.handleEvents({
							type: ORYX.CONFIG.EVENT_HIGHLIGHT_HIDE,
							highlightId: "shapeRepo.attached"
						});
					}
				}
			}
		};

		$scope.dragCallbackQuickMenu = function(event, ui) {

			if($scope.dragModeOver != false) {
				var coord = $scope.editor.eventCoordinatesXY(event.pageX, event.pageY);

				var additionalIEZoom = 1;
				if(!isNaN(screen.logicalXDPI) && !isNaN(screen.systemXDPI)) {
					var ua = navigator.userAgent;
					if(ua.indexOf('MSIE') >= 0) {
						//IE 10 and below
						var zoom = Math.round((screen.deviceXDPI / screen.logicalXDPI) * 100);
						if(zoom !== 100) {
							additionalIEZoom = zoom / 100
						}
					}
				}

				if(additionalIEZoom !== 1) {
					coord.x = coord.x / additionalIEZoom;
					coord.y = coord.y / additionalIEZoom;
				}

				var aShapes = $scope.editor.getCanvas().getAbstractShapesAtPosition(coord);

				if(aShapes.length <= 0) {
					if(event.helper) {
						$scope.dragCanContain = false;
						return false;
					}
				}

				if(aShapes[0] instanceof ORYX.Core.Canvas) {
					$scope.editor.getCanvas().setHightlightStateBasedOnX(coord.x);
				}

				var stencil = undefined;
				var stencilSets = $scope.editor.getStencilSets().values();
				for(var i = 0; i < stencilSets.length; i++) {
					var stencilSet = stencilSets[i];
					var nodes = stencilSet.nodes();
					for(var j = 0; j < nodes.length; j++) {
						if(nodes[j].idWithoutNs() === event.target.id) {
							stencil = nodes[j];
							break;
						}
					}

					if(!stencil) {
						var edges = stencilSet.edges();
						for(var j = 0; j < edges.length; j++) {
							if(edges[j].idWithoutNs() === event.target.id) {
								stencil = edges[j];
								break;
							}
						}
					}
				}

				var candidate = aShapes.last();

				var isValid = false;
				if(stencil.type() === "node") {
					//check containment rules
					var canContain = $scope.editor.getRules().canContain({
						containingShape: candidate,
						containedStencil: stencil
					});

					var parentCandidate = aShapes.reverse().find(function(candidate) {
						return(candidate instanceof ORYX.Core.Canvas ||
							candidate instanceof ORYX.Core.Node ||
							candidate instanceof ORYX.Core.Edge);
					});

					if(!parentCandidate) {
						$scope.dragCanContain = false;
						return false;
					}

					$scope.dragCurrentParent = parentCandidate;
					$scope.dragCurrentParentId = parentCandidate.id;
					$scope.dragCurrentParentStencil = parentCandidate.getStencil().id();
					$scope.dragCanContain = canContain;
					$scope.dropTargetElement = parentCandidate;
					isValid = canContain;

				} else { //Edge

					var shapes = $scope.editor.getSelection();
					if(shapes && shapes.length == 1) {
						var currentSelectedShape = shapes.first();
						var curCan = candidate;
						var canConnect = false;

						var targetStencil = $scope.getStencilItemById(curCan.getStencil().idWithoutNs());
						if(targetStencil) {
							var associationConnect = false;
							if(stencil.idWithoutNs() === 'Association' && (curCan.getStencil().idWithoutNs() === 'TextAnnotation' || curCan.getStencil().idWithoutNs() === 'BoundaryCompensationEvent')) {
								associationConnect = true;
							} else if(stencil.idWithoutNs() === 'DataAssociation' && curCan.getStencil().idWithoutNs() === 'DataStore') {
								associationConnect = true;
							}

							if(targetStencil.canConnectTo || associationConnect) {
								while(!canConnect && curCan && !(curCan instanceof ORYX.Core.Canvas)) {
									candidate = curCan;
									//check connection rules
									canConnect = $scope.editor.getRules().canConnect({
										sourceShape: currentSelectedShape,
										edgeStencil: stencil,
										targetShape: curCan
									});
									curCan = curCan.parent;
								}
							}
						}
						var parentCandidate = $scope.editor.getCanvas();

						isValid = canConnect;
						$scope.dragCurrentParent = parentCandidate;
						$scope.dragCurrentParentId = parentCandidate.id;
						$scope.dragCurrentParentStencil = parentCandidate.getStencil().id();
						$scope.dragCanContain = canConnect;
						$scope.dropTargetElement = candidate;
					}

				}

				$scope.editor.handleEvents({
					type: ORYX.CONFIG.EVENT_HIGHLIGHT_SHOW,
					highlightId: 'shapeMenu',
					elements: [candidate],
					color: isValid ? ORYX.CONFIG.SELECTION_VALID_COLOR : ORYX.CONFIG.SELECTION_INVALID_COLOR
				});
			}
		};

	}]);

var KISBPM = KISBPM || {};
//create command for undo/redo
KISBPM.CreateCommand = ORYX.Core.Command.extend({
	construct: function(option, currentReference, position, facade) {
		this.option = option;
		this.currentReference = currentReference;
		this.position = position;
		this.facade = facade;
		this.shape;
		this.edge;
		this.targetRefPos;
		this.sourceRefPos;
		/*
		 * clone options parameters
		 */
		this.connectedShape = option.connectedShape;
		this.connectingType = option.connectingType;
		this.namespace = option.namespace;
		this.type = option.type;
		this.containedStencil = option.containedStencil;
		this.parent = option.parent;
		this.currentReference = currentReference;
		this.shapeOptions = option.shapeOptions;
	},
	execute: function() {

		if(this.shape) {
			if(this.shape instanceof ORYX.Core.Node) {
				this.parent.add(this.shape);
				if(this.edge) {
					this.facade.getCanvas().add(this.edge);
					this.edge.dockers.first().setDockedShape(this.connectedShape);
					this.edge.dockers.first().setReferencePoint(this.sourceRefPos);
					this.edge.dockers.last().setDockedShape(this.shape);
					this.edge.dockers.last().setReferencePoint(this.targetRefPos);
				}

				this.facade.setSelection([this.shape]);

			} else if(this.shape instanceof ORYX.Core.Edge) {
				this.facade.getCanvas().add(this.shape);
				this.shape.dockers.first().setDockedShape(this.connectedShape);
				this.shape.dockers.first().setReferencePoint(this.sourceRefPos);
			}
		} else {
			this.shape = this.facade.createShape(this.option);
			this.edge = (!(this.shape instanceof ORYX.Core.Edge)) ? this.shape.getIncomingShapes().first() : undefined;
		}

		if(this.currentReference && this.position) {

			if(this.shape instanceof ORYX.Core.Edge) {

				if(!(this.currentReference instanceof ORYX.Core.Canvas)) {
					this.shape.dockers.last().setDockedShape(this.currentReference);

					if(this.currentReference.getStencil().idWithoutNs() === 'TextAnnotation') {
						var midpoint = {};
						midpoint.x = 0;
						midpoint.y = this.currentReference.bounds.height() / 2;
						this.shape.dockers.last().setReferencePoint(midpoint);
					} else {
						this.shape.dockers.last().setReferencePoint(this.currentReference.bounds.midPoint());
					}
				} else {
					this.shape.dockers.last().bounds.centerMoveTo(this.position);
				}
				this.sourceRefPos = this.shape.dockers.first().referencePoint;
				this.targetRefPos = this.shape.dockers.last().referencePoint;

			} else if(this.edge) {
				this.sourceRefPos = this.edge.dockers.first().referencePoint;
				this.targetRefPos = this.edge.dockers.last().referencePoint;
			}
		} else {
			var containedStencil = this.containedStencil;
			var connectedShape = this.connectedShape;
			var bc = connectedShape.bounds;
			var bs = this.shape.bounds;

			var pos = bc.center();
			if(containedStencil.defaultAlign() === "north") {
				pos.y -= (bc.height() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET + (bs.height() / 2);
			} else if(containedStencil.defaultAlign() === "northeast") {
				pos.x += (bc.width() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.width() / 2);
				pos.y -= (bc.height() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.height() / 2);
			} else if(containedStencil.defaultAlign() === "southeast") {
				pos.x += (bc.width() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.width() / 2);
				pos.y += (bc.height() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.height() / 2);
			} else if(containedStencil.defaultAlign() === "south") {
				pos.y += (bc.height() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET + (bs.height() / 2);
			} else if(containedStencil.defaultAlign() === "southwest") {
				pos.x -= (bc.width() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.width() / 2);
				pos.y += (bc.height() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.height() / 2);
			} else if(containedStencil.defaultAlign() === "west") {
				pos.x -= (bc.width() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET + (bs.width() / 2);
			} else if(containedStencil.defaultAlign() === "northwest") {
				pos.x -= (bc.width() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.width() / 2);
				pos.y -= (bc.height() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET_CORNER + (bs.height() / 2);
			} else {
				pos.x += (bc.width() / 2) + ORYX.CONFIG.SHAPEMENU_CREATE_OFFSET + (bs.width() / 2);
			}

			// Move shape to the new position
			this.shape.bounds.centerMoveTo(pos);

			// Move all dockers of a node to the position
			if(this.shape instanceof ORYX.Core.Node) {
				(this.shape.dockers || []).each(function(docker) {
					docker.bounds.centerMoveTo(pos);
				});
			}

			//this.shape.update();
			this.position = pos;

			if(this.edge) {
				this.sourceRefPos = this.edge.dockers.first().referencePoint;
				this.targetRefPos = this.edge.dockers.last().referencePoint;
			}
		}

		this.facade.getCanvas().update();
		this.facade.updateSelection();

	},
	rollback: function() {
		this.facade.deleteShape(this.shape);
		if(this.edge) {
			this.facade.deleteShape(this.edge);
		}
		//this.currentParent.update();
		this.facade.setSelection(this.facade.getSelection().without(this.shape, this.edge));
	}
});