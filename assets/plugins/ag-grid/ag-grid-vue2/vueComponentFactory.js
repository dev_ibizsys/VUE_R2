"use strict";
var VueComponentFactory = /** @class */ (function () {
    function VueComponentFactory($el, parent) {
        this.$el = null;
        this.parent = null;
        this.$el = $el;
        this.parent = parent;
    }
    VueComponentFactory.prototype.createRendererFromComponent = function (component) {
        var parent = this.parent;
        var componentType = VueComponentFactory.getComponentType(parent, component);
        if (!componentType) {
            return;
        }
        var CellRendererComponent = /** @class */ (function () {
            function CellRendererComponent() {
                this.component = null;
            }
            CellRendererComponent.prototype.init = function (params) {
                this.component = VueComponentFactory.createAndMountComponent(params, componentType, parent);
            };
            CellRendererComponent.prototype.getGui = function () {
                return this.component.$el;
            };
            CellRendererComponent.prototype.destroy = function () {
                this.component.$destroy();
            };
            return CellRendererComponent;
        }());
        return CellRendererComponent;
    };
    VueComponentFactory.prototype.createEditorFromComponent = function (component) {
        var parent = this.parent;
        var componentType = VueComponentFactory.getComponentType(parent, component);
        if (!componentType) {
            return;
        }
        var CellEditor = /** @class */ (function () {
            function CellEditor() {
                this.component = null;
            }
            CellEditor.prototype.init = function (params) {
                this.component = VueComponentFactory.createAndMountComponent(params, componentType, parent);
            };
            CellEditor.prototype.getValue = function () {
                return this.component.getValue();
            };
            CellEditor.prototype.getGui = function () {
                return this.component.$el;
            };
            CellEditor.prototype.destroy = function () {
                this.component.$destroy();
            };
            CellEditor.prototype.isPopup = function () {
                return this.component.isPopup ?
                    this.component.isPopup() : false;
            };
            CellEditor.prototype.isCancelBeforeStart = function () {
                return this.component.isCancelBeforeStart ?
                    this.component.isCancelBeforeStart() : false;
            };
            CellEditor.prototype.isCancelAfterEnd = function () {
                return this.component.isCancelAfterEnd ?
                    this.component.isCancelAfterEnd() : false;
            };
            CellEditor.prototype.focusIn = function () {
                if (this.component.focusIn) {
                    this.component.focusIn();
                }
            };
            CellEditor.prototype.focusOut = function () {
                if (this.component.focusOut) {
                    this.component.focusOut();
                }
            };
            return CellEditor;
        }());
        return CellEditor;
    };
    VueComponentFactory.prototype.createFilterFromComponent = function (component) {
        var parent = this.parent;
        var componentType = VueComponentFactory.getComponentType(parent, component);
        if (!componentType) {
            return;
        }
        var Filter = /** @class */ (function () {
            function Filter() {
                this.component = null;
            }
            Filter.prototype.init = function (params) {
                this.component = VueComponentFactory.createAndMountComponent(params, componentType, parent);
            };
            Filter.prototype.getGui = function () {
                return this.component.$el;
            };
            Filter.prototype.destroy = function () {
                this.component.$destroy();
            };
            Filter.prototype.isFilterActive = function () {
                return this.component.isFilterActive();
            };
            Filter.prototype.doesFilterPass = function (params) {
                return this.component.doesFilterPass(params);
            };
            Filter.prototype.getModel = function () {
                return this.component.getModel();
            };
            Filter.prototype.setModel = function (model) {
                this.component.setModel(model);
            };
            Filter.prototype.afterGuiAttached = function (params) {
                if (this.component.afterGuiAttached) {
                    this.component.afterGuiAttached(params);
                }
            };
            Filter.prototype.getFrameworkComponentInstance = function () {
                return this.component;
            };
            return Filter;
        }());
        return Filter;
    };
    VueComponentFactory.getComponentType = function (parent, component) {
        if (typeof component === 'string') {
            var componentInstance = parent.$parent.$options.components[component];
            if (!componentInstance) {
                console.error("Could not find component with name of " + component + ". Is it in Vue.components?");
                return null;
            }
            return Vue.extend(componentInstance);
        }
        else {
            // assume a type
            return component;
        }
    };
    VueComponentFactory.createAndMountComponent = function (params, componentType, parent) {
        var details = {
            data: {
                params: Object.freeze(params)
            },
            parent: parent,
            router: parent.$router,
            store: parent.$store
        };
        var component = new componentType(details);
        component.$mount();
        return component;
    };
    return VueComponentFactory;
}());
