"use strict";
var VueFrameworkComponentWrapper = /** @class */ (function () {
    function VueFrameworkComponentWrapper(parent) {
        this._parent = parent;
    }
    VueFrameworkComponentWrapper.prototype.wrap = function (component, methodList, optionalMethods) {
        var parent = this._parent;
        var componentType = VueComponentFactory.getComponentType(parent, component);
        if (!componentType) {
            return;
        }
        var DynamicComponent = /** @class */ (function () {
            function DynamicComponent() {
            }
            DynamicComponent.prototype.init = function (params) {
                this.component = VueComponentFactory.createAndMountComponent(params, componentType, parent);
            };
            DynamicComponent.prototype.getGui = function () {
                return this.component.$el;
            };
            DynamicComponent.prototype.destroy = function () {
                this.component.$destroy();
            };
            DynamicComponent.prototype.getFrameworkComponentInstance = function () {
                return this.component;
            };
            return DynamicComponent;
        }());
        var wrapper = new DynamicComponent();
        methodList.forEach((function (methodName) {
            wrapper[methodName] = function () {
                if (wrapper.getFrameworkComponentInstance()[methodName]) {
                    var componentRef = this.getFrameworkComponentInstance();
                    return wrapper.getFrameworkComponentInstance()[methodName].apply(componentRef, arguments);
                }
                else {
                    console.warn('ag-Grid: Vue component is missing the method ' + methodName + '()');
                    return null;
                }
            };
        }));
        optionalMethods.forEach((function (methodName) {
            wrapper[methodName] = function () {
                if (wrapper.getFrameworkComponentInstance()[methodName]) {
                    var componentRef = this.getFrameworkComponentInstance();
                    return wrapper.getFrameworkComponentInstance()[methodName].apply(componentRef, arguments);
                }
            };
        }));
        return wrapper;
    };
    return VueFrameworkComponentWrapper;
}());
VueFrameworkComponentWrapper.prototype.__agBeanMetaData = {
    beanName: "frameworkComponentWrapper"
};
