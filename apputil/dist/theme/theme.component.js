"use strict";
Vue.component("ibiz-theme", {
    template: "\n        <div>\n            <Poptip title=\"\u4E3B\u9898\" popper-class=\"ibiz-theme\" placement=\"bottom-end\">\n                <a>\n                    <Icon class=\"ibiz-theme-icon\" type=\"md-settings\"/>\n                </a>\n                <template slot=\"content\">\n                    <div class=\"ibiz-theme-color\">\n                        <template v-for=\"(theme,index) of defaultThemes\">\n                            <Tooltip :content=\"theme.title\">\n                                <div class=\"ibiz-theme-item\" :class=\"{active: selectTheme==theme.tag}\" :style=\"{background: theme.color}\" @click=\"themeChange(theme.tag)\"></div>\n                            </Tooltip>\n                        </template>\n                        <template v-for=\"(theme,index) of themes\">\n                            <Tooltip :content=\"theme.title\">\n                                <div :key=\"index\" class=\"ibiz-theme-item\" :class=\"{active: selectTheme==theme.tag}\" :style=\"{background: theme.color}\" @click=\"themeChange(theme.tag)\"></div>\n                            </Tooltip>\n                        </template>\n                    </div>\n                    <div>\n                        <Form :label-width=\"60\" label-position=\"left\">\n                            <FormItem label=\"\u5B57\u4F53\">\n                                <Select :value=\"selectFont\" size=\"small\" style=\"width:100px\" @on-change=\"fontChange\" transfer>\n                                    <Option v-for=\"font in fontFamilys\" :value=\"font.value\" :key=\"font.value\">{{ font.label }}</Option>\n                                </Select>\n                            </FormItem>\n                        </Form>\n                    </div>\n                </template>\n            </Poptip>\n        </div>\n    ",
    data: function () {
        var data = {
            selectTheme: '',
            defaultThemes: [
                {
                    tag: 'ibiz-default-theme',
                    title: 'light',
                    color: '#f6f6f6',
                },
                {
                    title: 'Blue',
                    tag: 'ibiz_theme_blue',
                    color: '#6ba1d1'
                },
                {
                    title: 'Dark Blue',
                    tag: 'ibiz_theme_darkblue',
                    color: '#606d80'
                }
            ],
            themes: IBizTheme,
            selectFont: '',
            fontFamilys: [
                {
                    label: '微软雅黑',
                    value: 'Microsoft YaHei',
                },
                {
                    label: '黑体',
                    value: 'SimHei',
                },
                {
                    label: '幼圆',
                    value: 'YouYuan',
                },
            ]
        };
        return data;
    },
    mounted: function () {
        var win = window;
        if (win.iBizApp) {
            this.selectTheme = win.iBizApp.getThemeClass();
            this.selectFont = win.iBizApp.getFontFamily();
        }
    },
    methods: {
        'themeChange': function (val) {
            if (!Object.is(this.activeTheme, val)) {
                this.selectTheme = val;
                this.$root.setThemeClass(val);
            }
        },
        'fontChange': function (val) {
            if (!Object.is(this.selectFont, val)) {
                this.selectFont = val;
                this.$root.setFontFamily(val);
            }
        }
    }
});
