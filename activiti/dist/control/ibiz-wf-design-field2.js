"use strict";
/**
 * 工作流设计工具
 *
 * @class IBizWFDesignField2
 */
var IBizWFDesignField2 = /** @class */ (function () {
    function IBizWFDesignField2(opts) {
        if (opts === void 0) { opts = {}; }
        this.$applicationRef = null;
        /**
         * activit设计工具iframe的window对象
         *
         * @private
         * @type {ActivitiWorkflowEditor}
         * @memberof IBizWFDesignField2
         */
        this.$flowFrame = null;
        /**
         * 打开编辑视图服务
         *
         * @private
         * @type {WorkFlowEditService}
         * @memberof IBizWFDesignField2
         */
        this.$workFlowEditService = null;
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$lastValue = null;
        /**
         *
         *
         * @private
         * @type {boolean}
         * @memberof IBizWFDesignField2
         */
        this.$flowFrameLoaded = false;
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$logicJson = {};
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$logicJson2 = {};
        /**
         *
         *
         * @private
         * @type {string}
         * @memberof IBizWFDesignField2
         */
        this.$logicJsonString = '';
        /**
         * 值改变发射流
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizWFDesignField2
         */
        this.$valueChangeSubject = new rxjs.Subject();
        /**
         * 全屏按钮事件
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizWFDesignField2
         */
        this.$fullScreenSubject = new rxjs.Subject();
        /**
         * vue对象
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$vue = null;
        /**
         * 工作流
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.workFlow = {};
        this.$vue = opts.vue;
        Object.assign(this.workFlow, opts.workFlow);
        this.$workFlowEditService = new WorkFlowEditService({ vue: this.$vue, workFlow: opts.workFlow });
    }
    /**
     *
     *
     * @param {any} value
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.setValue = function (value) {
        var wfEngineType = this.getWFEngineType();
        this.$lastValue = value;
        if (wfEngineType && wfEngineType === 'ACTIVITI') {
            this.loadFromXML(value);
        }
    };
    /**
     *
     *
     * @param {any} strXML
     * @returns {void}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.loadFromXML = function (strXML) {
        var xmlDoc = IBizActivitiUtil.parseXML(strXML);
        if (xmlDoc == null) {
            alert('工作流流程不正确');
            return;
        }
        for (var i = 0; i < xmlDoc.childNodes.length; i++) {
            if (xmlDoc.childNodes[i].nodeName === 'WFCONFIG') {
                this.loadLogic(xmlDoc.childNodes[i]);
                break;
            }
        }
        if (this.$flowFrameLoaded) {
            this.loadConfig();
        }
    };
    /**
     *
     *
     * @param {any} xmlNode
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.loadLogic = function (xmlNode) {
        var xml = {};
        IBizActivitiUtil.loadXMLNode(xmlNode, xml);
        this.$logicJson = {};
        this.$logicJson2 = {};
        Object.assign(this.$logicJson, xml);
        Object.assign(this.$logicJson2, xml);
        Object.assign(this.$logicJson, { srfparenttype: this.workFlow.NODE.type, srfder1nid: this.workFlow.NODE.der1nid, srfparentkey: xml[this.workFlow.FLOW.id] });
        Object.assign(this.$logicJson2, { srfparenttype: this.workFlow.LINK.type, srfder1nid: this.workFlow.LINK.der1nid, srfparentkey: xml[this.workFlow.FLOW.id] });
        this.$logicJsonString = xml.activitimodel;
        delete this.$logicJson.activitimodel;
        delete this.$logicJson2.activitimodel;
    };
    /**
     *
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.loadConfig = function () {
        console.log('loadConfig');
        if (this.$logicJsonString) {
            this.$flowFrame.setModel(this.$logicJsonString);
        }
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getValue = function () {
        var wfEngineType = this.getWFEngineType();
        if (wfEngineType === 'ACTIVITI') {
            return this.exportToXml();
        }
        return this.$lastValue;
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.exportToXml = function () {
        console.log('exportToXml');
        if (this.$flowFrameLoaded) {
            var value = this.$flowFrame.getModel();
            var writer = new IBizXMLWriter();
            writer.beginNode('WFCONFIG');
            if (this.$logicJson) {
                writer.attrib(this.workFlow.FLOW.id.toUpperCase(), this.$logicJson[this.workFlow.FLOW.id]);
                // writer.attrib('PSWFID', this.$logicJson.pswfid);
                // writer.attrib('PSWFVERSIONID', this.$logicJson.pswfversionid);
                // writer.attrib('PSSYSTEMID', this.$logicJson.pssystemid);
            }
            writer.attrib('ACTIVITIMODEL', value);
            writer.endNode();
            writer.close();
            var strXML = '<?xml version="1.0" encoding="utf-8" ?>' + writer.toString();
            return strXML;
        }
        else {
            return this.$lastValue;
        }
    };
    /**
     * 设置iframeWindow对象
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.setFlowFrame = function (flowFrame) {
        this.$flowFrame = flowFrame;
    };
    /**
     *
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onFlowFrameLoad = function () {
        console.log('onFlowFrameLoad');
        this.$flowFrameLoaded = true;
        this.$flowFrame.setParentObj(this);
    };
    /**
     * 获取当前工作流编辑器类型
     *
     * @returns {String}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getWFEngineType = function () {
        return 'ACTIVITI';
    };
    /**
     * 发射全屏事件
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.showFullScreen = function () {
        this.$fullScreenSubject.next(this.getValue());
    };
    /**
     * 开启全屏模式
     *
     * @returns {Observable<string>}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onFullScreen = function () {
        return this.$fullScreenSubject.asObservable();
    };
    /**
     * 全屏模式关闭回调
     *
     * @param {any} win
     * @returns {void}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onFullScreenWindowClose = function (res) {
        var activeData = res.activeData;
        if (!activeData) {
            console.log('没有返回任何数据!');
            return;
        }
        var wfmodel = activeData.wfmodel;
        this.setValue(wfmodel);
    };
    /**
     *
     *
     * @param {any} shape
     * @returns {void}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editShape = function (shape) {
        console.log('editShape');
        if (!shape) {
            return;
        }
        var stencilid = shape.getStencil().id();
        var type = stencilid.replace('http://b3mn.org/stencilset/bpmn2.0#', '');
        var keyvalue = shape.properties['oryx-documentation'];
        if (type === 'StartNoneEvent') {
            this.editProcess(shape, 'START', keyvalue);
            return;
        }
        if (type === 'UserTask') {
            this.editProcess(shape, 'INTERACTIVE', keyvalue);
            return;
        }
        if (type === 'ServiceTask') {
            this.editProcess(shape, 'PROCESS', keyvalue);
            return;
        }
        if (type === 'ExclusiveGateway') {
            this.editProcess(shape, 'EXCLUSIVEGATEWAY', keyvalue);
            return;
        }
        if (type === 'InclusiveGateway') {
            this.editProcess(shape, 'INCLUSIVEGATEWAY', keyvalue);
            return;
        }
        if (type === 'SubProcess') {
            this.editProcess(shape, 'EMBED', keyvalue);
            return;
        }
        if (type === 'CatchTimerEvent') {
            this.editProcess(shape, 'TIMEREVENT', keyvalue);
            return;
        }
        if (type === 'ParallelGateway') {
            this.editProcess(shape, 'PARALLELGATEWAY', keyvalue);
            return;
        }
        if (type === 'TextAnnotation') {
            this.editTextAnnotation(shape);
            return;
        }
        if (type === 'SequenceFlow') {
            var fromShape = this.getFromShape(shape);
            if (!fromShape) {
                alert('无法获取连接源处理对象');
                return;
            }
            var stencilid2 = fromShape.getStencil().id();
            var type2 = stencilid2.replace('http://b3mn.org/stencilset/bpmn2.0#', '');
            if (type2 === 'UserTask') {
                this.editLink(shape, 'IAACTION', keyvalue);
            }
            else {
                this.editLink(shape, 'ROUTE', keyvalue);
            }
            return;
        }
        if (type === 'EndNoneEvent') {
            this.editProcess(shape, 'END', keyvalue);
            return;
        }
    };
    /**
     * 注释节点编辑
     *
     * @param {any} tag
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editTextAnnotation = function (tag) {
        throw new Error('暂无视图无法打开');
        // 需要的视图WFDesignField2MemoView
        // this.$workFlowEditService.openEditTextAnnotationView({ 'viewParam': { memo: tag.properties['oryx-text'] }, 'modalZIndex': 800 }).subscribe(
        //     res => {
        //         if (res && Object.is('OK', res.ret)) {
        //             this.onNodeWindowClose(res);
        //         }
        //     }
        // );
    };
    /**
     *
     *
     * @param {any} tag
     * @param {any} detailtype
     * @param {any} keyvalue
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editProcess = function (tag, detailtype, keyvalue) {
        var _this = this;
        if (keyvalue === void 0) { keyvalue = ''; }
        var param = {
            srfparenttype: this.$logicJson.srfparenttype,
            srfder1nid: this.$logicJson.srfder1nid,
            srfparentkey: this.$logicJson.srfparentkey,
            wfprocesstype: detailtype
        };
        if (keyvalue && !Object.is(keyvalue, '')) {
            Object.assign(param, { srfkey: keyvalue });
        }
        this.$workFlowEditService.openProcessEditView(detailtype, { 'viewParam': param, 'modalZIndex': 600 }).subscribe(function (res) {
            if (res && Object.is('OK', res.ret)) {
                res.activeNode = tag;
                _this.onNodeWindowClose(res);
                // this.clearTickTimer();
                // this.allTick();
            }
        });
        // this.modalTick();
        // this.allTick();
    };
    /**
     *
     *
     * @param {any} obj
     * @returns {*}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getFromShape = function (obj) {
        var shapeFacade = this.getShapeFacade(obj);
        if (!shapeFacade) {
            return null;
        }
        var strResourceId = obj['resourceId'];
        if (!strResourceId) {
            return null;
        }
        var shapes = shapeFacade.getCanvas().getChildShapes(true);
        if (!shapes || shapes.length === 0) {
            return null;
        }
        for (var i = 0; i < shapes.length; i++) {
            var shape = shapes[i];
            var outgoings = shape['outgoing'];
            if (!outgoings || outgoings.length === 0) {
                continue;
            }
            for (var j = 0; j < outgoings.length; j++) {
                var outgoing = outgoings[j];
                if (outgoing['resourceId'] === strResourceId) {
                    return shape;
                }
            }
        }
        return null;
    };
    /**
     *
     *
     * @param {any} obj
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getShapeFacade = function (obj) {
        if (obj.facade) {
            return obj.facade;
        }
        if (obj.parent) {
            return this.getShapeFacade(obj.parent);
        }
        return null;
    };
    /**
     *
     *
     * @param {any} tag
     * @param {any} linktype
     * @param {any} keyvalue
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editLink = function (tag, linktype, keyvalue) {
        var _this = this;
        var loadParam = {
            srfparenttype: this.$logicJson2.srfparenttype,
            srfder1nid: this.$logicJson2.srfder1nid,
            srfparentkey: this.$logicJson2.srfparentkey,
            wflinktype: linktype,
        };
        if (keyvalue && !Object.is(keyvalue, '')) {
            Object.assign(loadParam, { srfkey: keyvalue });
        }
        var _data = {};
        if (!keyvalue || Object.is(keyvalue, '')) {
            _data[this.workFlow.FORM.id] = 'SRFTEMPKEY:' + this.newGuid();
            _data[this.workFlow.FORM.name] = '源处理名称';
        }
        Object.assign(loadParam, _data);
        this.$workFlowEditService.openLinkEditView(linktype, { 'viewParam': loadParam, 'modalZIndex': 600 }).subscribe(function (res) {
            if (res && Object.is('OK', res.ret)) {
                res.activeLink = tag;
                _this.onLinkWindowClose(res);
                // this.clearTickTimer();
                // this.allTick();
            }
        });
        // this.allTick();
        // this.modalTick();
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.newGuid = function () {
        var guid = '';
        for (var i = 1; i <= 32; i++) {
            var n = Math.floor(Math.random() * 16.0).toString(16);
            guid += n;
            if ((i === 8) || (i === 12) || (i === 16) || (i === 20)) {
                guid += '-';
            }
        }
        return guid;
    };
    /**
     * 节点编辑视图关闭回调
     *
     * @param {any} res
     * @returns {*}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onNodeWindowClose = function (res) {
        var node = res.activeNode;
        var activeData = res.activeData;
        if (!activeData) {
            console.log('没有返回任何数据!');
            return;
        }
        var xml = {};
        Object.assign(xml, activeData);
        node.setProperty('oryx-name', xml[this.workFlow.NODE.name]);
        node.setProperty('oryx-documentation', xml[this.workFlow.NODE.id]);
        this.$flowFrame.updateProcess(node);
        this.$valueChangeSubject.next(this.getValue());
    };
    /**
     * 链接线编辑视图关闭回调
     *
     * @param {any} win
     * @param {any} eOpts
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onLinkWindowClose = function (res) {
        var link = res.activeLink;
        var activeData = res.activeData;
        if (!activeData) {
            console.log('没有返回任何数据!');
            return;
        }
        var xml = {};
        Object.assign(xml, activeData);
        // let logicname = xml.logicname;
        // if (!logicname) {
        //     logicname = xml.pswflinkname;
        // }
        link.setProperty('oryx-name', xml[this.workFlow.LINK.name]);
        link.setProperty('oryx-documentation', xml[this.workFlow.LINK.id]);
        // 更新
        this.$flowFrame.updateConnection(link);
        this.$valueChangeSubject.next(this.getValue());
    };
    /**
     * 对创建的模态框及其子组件进行脏检查，检查完毕后进行一次全局脏检查
     *
     * @private
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.allTick = function () {
        for (var i = 0; i < 3; i++) {
            setTimeout(function () {
                // this.$applicationRef.tick();
            }, 300);
        }
    };
    /**
     * 对打开的模态框进行脏检查
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.modalTick = function () {
        var components = this.$applicationRef.components;
        if (this.$tickTimer) {
            clearInterval(this.$tickTimer);
            this.$tickTimer = undefined;
        }
        this.$tickTimer = setInterval(function () {
            // this.$applicationRef.tick();
        }, 100);
    };
    /**
     * 清除脏检查定时器
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.clearTickTimer = function () {
        if (this.$tickTimer) {
            clearInterval(this.$tickTimer);
            this.$tickTimer = undefined;
        }
    };
    /**
     * 使用该方法订阅工作流值改变
     *
     * @returns {Observable<string>}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.wfDataChange = function () {
        return this.$valueChangeSubject.asObservable();
    };
    return IBizWFDesignField2;
}());
