"use strict";
/**
 * 工作流编辑服务
 *
 * @class WorkFlowEditService
 */
var WorkFlowEditService = /** @class */ (function () {
    /**
     * Creates an instance of WorkFlowEditService.
     * 创建 WorkFlowEditService 实例
     *
     * @param {*} opts
     * @memberof WorkFlowEditService
     */
    function WorkFlowEditService(opts) {
        if (opts === void 0) { opts = {}; }
        /**
         * vue  对象
         *
         * @private
         * @type {*}
         * @memberof WorkFlowEditService
         */
        this.$vue = null;
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof WorkFlowEditService
         */
        this.workFlow = {};
        this.$vue = opts.vue;
        Object.assign(this.workFlow, opts.workFlow);
    }
    WorkFlowEditService.prototype.openProcessEditView = function (viewType, opt) {
        if (opt === void 0) { opt = {}; }
        return this.openProcess(viewType, opt);
    };
    WorkFlowEditService.prototype.openProcess = function (viewType, opt) {
        var subject = new rxjs.Subject();
        if (!this.$vue) {
            return null;
        }
        var view = { subject: subject, viewparam: opt.viewParam };
        if (Object.is(viewType, 'START') && this.workFlow.START && Object.keys(this.workFlow.START).length > 0) {
            Object.assign(view, this.workFlow.START);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'INTERACTIVE') && this.workFlow.INTERACTIVE && Object.keys(this.workFlow.INTERACTIVE).length > 0) {
            Object.assign(view, this.workFlow.INTERACTIVE);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'PROCESS') && this.workFlow.PROCESS && Object.keys(this.workFlow.PROCESS).length > 0) {
            Object.assign(view, this.workFlow.PROCESS);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'END') && this.workFlow.END && Object.keys(this.workFlow.END).length > 0) {
            Object.assign(view, this.workFlow.END);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'EMBED') && this.workFlow.EMBED && Object.keys(this.workFlow.EMBED).length > 0) {
            Object.assign(view, this.workFlow.EMBED);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'PARALLEL') && this.workFlow.PARALLEL && Object.keys(this.workFlow.PARALLEL).length > 0) {
            Object.assign(view, this.workFlow.PARALLEL);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'PARALLELGATEWAY') && this.workFlow.PARALLELGATEWAY && Object.keys(this.workFlow.PARALLELGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.PARALLELGATEWAY);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'EXCLUSIVEGATEWAY') && this.workFlow.EXCLUSIVEGATEWAY && Object.keys(this.workFlow.EXCLUSIVEGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.EXCLUSIVEGATEWAY);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'INCLUSIVEGATEWAY') && this.workFlow.INCLUSIVEGATEWAY && Object.keys(this.workFlow.INCLUSIVEGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.INCLUSIVEGATEWAY);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'TIMEREVENT') && this.workFlow.TIMEREVENT && Object.keys(this.workFlow.TIMEREVENT).length > 0) {
            Object.assign(view, this.workFlow.TIMEREVENT);
            this.$vue.$root.addModal(view);
        }
        return subject;
    };
    WorkFlowEditService.prototype.openLinkEditView = function (viewType, opt) {
        if (opt === void 0) { opt = {}; }
        return this.openLink(viewType, opt);
    };
    WorkFlowEditService.prototype.openLink = function (viewType, opt) {
        if (!this.$vue) {
            return null;
        }
        var subject = new rxjs.Subject();
        var view = { subject: subject, viewparam: opt.viewParam };
        if (Object.is(viewType, 'ROUTE') && this.workFlow.ROUTE && Object.keys(this.workFlow.ROUTE).length > 0) {
            Object.assign(view, this.workFlow.ROUTE);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'IAACTION') && this.workFlow.IAACTION && Object.keys(this.workFlow.IAACTION).length > 0) {
            Object.assign(view, this.workFlow.IAACTION);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'TIMEOUT') && this.workFlow.TIMEOUT && Object.keys(this.workFlow.TIMEOUT).length > 0) {
            Object.assign(view, this.workFlow.TIMEOUT);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'WFRETURN') && this.workFlow.WFRETURN && Object.keys(this.workFlow.WFRETURN).length > 0) {
            Object.assign(view, this.workFlow.WFRETURN);
            this.$vue.$root.addModal(view);
        }
        return subject;
    };
    WorkFlowEditService.prototype.openEditTextAnnotationView = function (opt) {
        return;
    };
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {WorkFlowEditService}
     * @memberof WorkFlowEditService
     */
    WorkFlowEditService.workFlowEditService = null;
    return WorkFlowEditService;
}());
