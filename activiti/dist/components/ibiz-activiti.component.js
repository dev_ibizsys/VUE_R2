"use strict";
var _this = this;
Vue.component('ibiz-activiti', {
    template: "\n    <div style=\"width:100%; height:600px;\">\n        <iframe  width=\"100%\" height=\"100%\" ref=\"ibizactiviti\" :src=\"url\" frameborder=\"0\"></iframe>\n    </div>\n    ",
    props: ['field', 'form'],
    data: function () {
        var data = {
            wfDesign: new IBizWFDesignField2({ vue: this, workFlow: this.field.editorParams }),
            activitimodel: null,
            $iframeIsLoadEnd: false,
            formLoaded: false,
        };
        var url = "../../../packages/assets/plugins/static/activiti/index.html?_=" + data.wfDesign.newGuid();
        Object.assign(data, { url: url });
        return data;
    },
    mounted: function () {
        var _this = this;
        this.wfDesign.wfDataChange().subscribe(function (res) {
            _this.activitimodel = res;
            _this.onChangeCallback(_this.activitimodel);
        });
        this.wfDesign.onFullScreen().subscribe(function (model) {
            var subject = new rxjs.Subject();
            var view = {
                subject: subject,
                viewname: 'ibiz-activiti-full-screen',
                modalviewname: 'ibiz-activiti-full-screen',
                title: '工作流设计',
                viewparam: { activitiModel: model, workFlow: _this.field.editorParams },
            };
            _this.$root.addModal(view);
            subject.subscribe(function (result) {
                if (result && Object.is(result.ret, 'OK')) {
                    console.log(result);
                    _this.wfDesign.setValue(result.activitiModel);
                }
            });
        });
        if (this.field) {
            this.writeValue(this.field.getValue());
            this.field.getValue = function () {
                if (_this.$iframeIsLoadEnd && _this.activitimodel) {
                    _this.activitimodel = _this.wfDesign.getValue();
                    _this.field.setValue(_this.activitimodel);
                    return _this.activitimodel;
                }
                return undefined;
            };
        }
    },
    activated: function () {
        console.log(123123123);
        this.initActiviti();
    },
    methods: {
        initActiviti: function () {
            var _this = this;
            var iframe = this.$refs.ibizactiviti;
            if (iframe) {
                this.workflowDesign = iframe.contentWindow;
                if (this.workflowDesign) {
                    this.workflowDesign.editorOnLoad = function () {
                        console.log('form before ');
                        if (_this.formLoaded) {
                            console.log('form after ');
                            _this.wfDesign.setFlowFrame(_this.workflowDesign);
                            _this.wfDesign.onFlowFrameLoad();
                            _this.$iframeIsLoadEnd = true;
                            delete _this.workflowDesign.editorOnLoad;
                        }
                    };
                }
                else {
                    console.error('设计工具初始化失败');
                }
            }
            if (this.form) {
                this.form.on('FORMLOADED').subscribe(function () {
                    // this.formLoaded = true;
                });
            }
        },
        onTouchedCallback: function () {
            return function () { };
        },
        onChangeCallback: function (data) {
            if (_this.field) {
                _this.field.setValue(data);
            }
        },
        writeValue: function (value) {
            if (value) {
                this.activitimodel = value;
                if (this.wfDesign) {
                    this.wfDesign.setValue(this.activitimodel);
                }
            }
        },
        registerOnChange: function (fn) {
            this.onChangeCallback = fn;
        },
        registerOnTouched: function (fn) {
            this.onTouchedCallback = fn;
        },
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            this.writeValue(newVal);
            if (!Object.is(newVal, '')) {
                this.formLoaded = true;
                console.log(newVal);
            }
        }
    },
});
