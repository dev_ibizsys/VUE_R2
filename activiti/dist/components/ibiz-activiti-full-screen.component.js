"use strict";
Vue.component('ibiz-activiti-full-screen', {
    template: "\n        <div style=\"width:100%; height:100%;\">\n            <iframe width=\"100%\" height=\"100%\" ref=\"ibizactivitifullscreen\" :src=\"url\" frameborder=\"0\" ></iframe>\n        </div>\n    ",
    props: ['params'],
    data: function () {
        var data = {
            wfDesign: new IBizWFDesignField2({ vue: this, workFlow: this.params.workFlow }),
            $iframeIsLoadEnd: false,
            workflowDesign: null,
        };
        var url = "../../../packages/assets/plugins/static/activiti/index.html?fsmode=true&_=" + data.wfDesign.newGuid();
        Object.assign(data, { url: url });
        if (this.params && this.params.activitiModel) {
            Object.assign(data, { activitiModel: this.params.activitiModel });
        }
        return data;
    },
    mounted: function () {
        this.designInit();
        if (this.wfDesign) {
            this.wfDesign.setValue(this.activitiModel);
        }
    },
    methods: {
        designInit: function () {
            var _this = this;
            var iframe = this.$refs.ibizactivitifullscreen;
            if (!iframe) {
                return;
            }
            this.workflowDesign = iframe.contentWindow;
            if (this.workflowDesign) {
                this.workflowDesign.onload = function () {
                    _this.wfDesign.setFlowFrame(_this.workflowDesign);
                    _this.wfDesign.onFlowFrameLoad();
                    _this.$iframeIsLoadEnd = true;
                    // this.wfDesign.setValue(this.activitiModel);
                };
                this.workflowDesign.onSelectionChanged = function () {
                    var activitiModel = _this.wfDesign.getValue();
                    _this.$emit('dataChange', { ret: 'OK', activitiModel: activitiModel });
                };
                this.isDesignInitEnd = true;
            }
            else {
                console.error('设计工具初始化失败');
            }
        }
    }
});
