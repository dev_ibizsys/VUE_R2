"use strict";
/**
 * xml工具类
 *
 * @class IBizXMLWriter
 */
var IBizXMLWriter = /** @class */ (function () {
    function IBizXMLWriter() {
        /**
         *
         *
         * @type {any[]}
         * @memberof IBizXMLWriter
         */
        this.XML = [];
        /**
         *
         *
         * @type {any[]}
         * @memberof IBizXMLWriter
         */
        this.nodes = [];
        /**
         *
         *
         * @memberof IBizXMLWriter
         */
        this.State = '';
    }
    /**
     *
     *
     * @param {any} Str
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.formatXML = function (Str) {
        if (Str) {
            return Str.replace(/&/g, '&amp;').replace(/\"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g, '&#xA;').replace(/\r/g, '&#xD;');
        }
        return '';
    };
    /**
     *
     *
     * @param {any} Name
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.beginNode = function (Name) {
        if (!Name) {
            return;
        }
        if (this.State === 'beg') {
            this.XML.push('>');
        }
        this.State = 'beg';
        this.nodes.push(Name);
        this.XML.push('<' + Name);
    };
    /**
     *
     *
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.endNode = function () {
        if (this.State === 'beg') {
            this.XML.push('/>');
            this.nodes.pop();
        }
        else if (this.nodes.length > 0) {
            this.XML.push('</' + this.nodes.pop() + '>');
        }
        this.State = '';
    };
    /**
     *
     *
     * @param {any} Name
     * @param {any} Value
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.attrib = function (Name, Value) {
        if (this.State !== 'beg' || !Name) {
            return;
        }
        this.XML.push(' ' + Name + '="' + this.formatXML(Value) + '"');
    };
    /**
     *
     *
     * @param {any} Value
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.writeString = function (Value) {
        if (this.State === 'beg') {
            this.XML.push('>');
        }
        this.XML.push(this.formatXML(Value));
        this.State = '';
    };
    /**
     *
     *
     * @param {any} Name
     * @param {any} Value
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.node = function (Name, Value) {
        if (!Name) {
            return;
        }
        if (this.State === 'beg') {
            this.XML.push('>');
        }
        this.XML.push((Value === '' || !Value) ? '<' + Name + '/>' : '<' + Name + '>' + this.formatXML(Value) + '</' + Name + '>');
        this.State = '';
    };
    /**
     *
     *
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.close = function () {
        while (this.nodes.length > 0) {
            this.endNode();
        }
        this.State = 'closed';
    };
    /**
     *
     *
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.toString = function () { return this.XML.join(''); };
    return IBizXMLWriter;
}());
