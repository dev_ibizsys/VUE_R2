"use strict";
var IBizActivitiUtil = /** @class */ (function () {
    function IBizActivitiUtil() {
    }
    /**
     * 将文本格式的xml转换为dom模式
     *
     * @static
     * @param {string} strXml
     * @returns {Document}
     * @memberof IBizActivitiUtil
     */
    IBizActivitiUtil.parseXML = function (strXml) {
        if (strXml) {
            return new DOMParser().parseFromString(strXml, 'text/xml');
        }
    };
    /**
     * 将xml转换为object对象
     *
     * @static
     * @param {*} node
     * @param {*} [obj={}]
     * @memberof IBizActivitiUtil
     */
    IBizActivitiUtil.loadXMLNode = function (node, obj) {
        if (obj === void 0) { obj = {}; }
        if (node && node.attributes) {
            var arr = node.attributes;
            for (var i = 0; i < arr.length; i++) {
                var A = arr.item(i).name;
                var B = arr.item(i).value;
                A = A.toLowerCase();
                obj[A] = B;
            }
        }
    };
    /**
     * 将object转换为xml对象
     *
     * @static
     * @param {*} XML
     * @param {*} obj
     * @memberof IBizActivitiUtil
     */
    IBizActivitiUtil.saveXMLNode = function (XML, obj) {
        var proName = '';
        for (proName in obj) {
            var value = obj[proName];
            if (!value || value instanceof Object || typeof (value) === 'function') {
                continue;
            }
            var proValue = obj[proName].toString();
            if (proValue !== '') {
                XML.attrib(proName, proValue);
            }
        }
    };
    return IBizActivitiUtil;
}());

"use strict";
/**
 * xml工具类
 *
 * @class IBizXMLWriter
 */
var IBizXMLWriter = /** @class */ (function () {
    function IBizXMLWriter() {
        /**
         *
         *
         * @type {any[]}
         * @memberof IBizXMLWriter
         */
        this.XML = [];
        /**
         *
         *
         * @type {any[]}
         * @memberof IBizXMLWriter
         */
        this.nodes = [];
        /**
         *
         *
         * @memberof IBizXMLWriter
         */
        this.State = '';
    }
    /**
     *
     *
     * @param {any} Str
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.formatXML = function (Str) {
        if (Str) {
            return Str.replace(/&/g, '&amp;').replace(/\"/g, '&quot;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\n/g, '&#xA;').replace(/\r/g, '&#xD;');
        }
        return '';
    };
    /**
     *
     *
     * @param {any} Name
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.beginNode = function (Name) {
        if (!Name) {
            return;
        }
        if (this.State === 'beg') {
            this.XML.push('>');
        }
        this.State = 'beg';
        this.nodes.push(Name);
        this.XML.push('<' + Name);
    };
    /**
     *
     *
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.endNode = function () {
        if (this.State === 'beg') {
            this.XML.push('/>');
            this.nodes.pop();
        }
        else if (this.nodes.length > 0) {
            this.XML.push('</' + this.nodes.pop() + '>');
        }
        this.State = '';
    };
    /**
     *
     *
     * @param {any} Name
     * @param {any} Value
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.attrib = function (Name, Value) {
        if (this.State !== 'beg' || !Name) {
            return;
        }
        this.XML.push(' ' + Name + '="' + this.formatXML(Value) + '"');
    };
    /**
     *
     *
     * @param {any} Value
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.writeString = function (Value) {
        if (this.State === 'beg') {
            this.XML.push('>');
        }
        this.XML.push(this.formatXML(Value));
        this.State = '';
    };
    /**
     *
     *
     * @param {any} Name
     * @param {any} Value
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.node = function (Name, Value) {
        if (!Name) {
            return;
        }
        if (this.State === 'beg') {
            this.XML.push('>');
        }
        this.XML.push((Value === '' || !Value) ? '<' + Name + '/>' : '<' + Name + '>' + this.formatXML(Value) + '</' + Name + '>');
        this.State = '';
    };
    /**
     *
     *
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.close = function () {
        while (this.nodes.length > 0) {
            this.endNode();
        }
        this.State = 'closed';
    };
    /**
     *
     *
     * @returns
     * @memberof XMLWriter
     */
    IBizXMLWriter.prototype.toString = function () { return this.XML.join(''); };
    return IBizXMLWriter;
}());

"use strict";

"use strict";
/**
 * 工作流编辑服务
 *
 * @class WorkFlowEditService
 */
var WorkFlowEditService = /** @class */ (function () {
    /**
     * Creates an instance of WorkFlowEditService.
     * 创建 WorkFlowEditService 实例
     *
     * @param {*} opts
     * @memberof WorkFlowEditService
     */
    function WorkFlowEditService(opts) {
        if (opts === void 0) { opts = {}; }
        /**
         * vue  对象
         *
         * @private
         * @type {*}
         * @memberof WorkFlowEditService
         */
        this.$vue = null;
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof WorkFlowEditService
         */
        this.workFlow = {};
        this.$vue = opts.vue;
        Object.assign(this.workFlow, opts.workFlow);
    }
    WorkFlowEditService.prototype.openProcessEditView = function (viewType, opt) {
        if (opt === void 0) { opt = {}; }
        return this.openProcess(viewType, opt);
    };
    WorkFlowEditService.prototype.openProcess = function (viewType, opt) {
        var subject = new rxjs.Subject();
        if (!this.$vue) {
            return null;
        }
        var view = { subject: subject, viewparam: opt.viewParam };
        if (Object.is(viewType, 'START') && this.workFlow.START && Object.keys(this.workFlow.START).length > 0) {
            Object.assign(view, this.workFlow.START);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'INTERACTIVE') && this.workFlow.INTERACTIVE && Object.keys(this.workFlow.INTERACTIVE).length > 0) {
            Object.assign(view, this.workFlow.INTERACTIVE);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'PROCESS') && this.workFlow.PROCESS && Object.keys(this.workFlow.PROCESS).length > 0) {
            Object.assign(view, this.workFlow.PROCESS);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'END') && this.workFlow.END && Object.keys(this.workFlow.END).length > 0) {
            Object.assign(view, this.workFlow.END);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'EMBED') && this.workFlow.EMBED && Object.keys(this.workFlow.EMBED).length > 0) {
            Object.assign(view, this.workFlow.EMBED);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'PARALLEL') && this.workFlow.PARALLEL && Object.keys(this.workFlow.PARALLEL).length > 0) {
            Object.assign(view, this.workFlow.PARALLEL);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'PARALLELGATEWAY') && this.workFlow.PARALLELGATEWAY && Object.keys(this.workFlow.PARALLELGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.PARALLELGATEWAY);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'EXCLUSIVEGATEWAY') && this.workFlow.EXCLUSIVEGATEWAY && Object.keys(this.workFlow.EXCLUSIVEGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.EXCLUSIVEGATEWAY);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'INCLUSIVEGATEWAY') && this.workFlow.INCLUSIVEGATEWAY && Object.keys(this.workFlow.INCLUSIVEGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.INCLUSIVEGATEWAY);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'TIMEREVENT') && this.workFlow.TIMEREVENT && Object.keys(this.workFlow.TIMEREVENT).length > 0) {
            Object.assign(view, this.workFlow.TIMEREVENT);
            this.$vue.$root.addModal(view);
        }
        return subject;
    };
    WorkFlowEditService.prototype.openLinkEditView = function (viewType, opt) {
        if (opt === void 0) { opt = {}; }
        return this.openLink(viewType, opt);
    };
    WorkFlowEditService.prototype.openLink = function (viewType, opt) {
        if (!this.$vue) {
            return null;
        }
        var subject = new rxjs.Subject();
        var view = { subject: subject, viewparam: opt.viewParam };
        if (Object.is(viewType, 'ROUTE') && this.workFlow.ROUTE && Object.keys(this.workFlow.ROUTE).length > 0) {
            Object.assign(view, this.workFlow.ROUTE);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'IAACTION') && this.workFlow.IAACTION && Object.keys(this.workFlow.IAACTION).length > 0) {
            Object.assign(view, this.workFlow.IAACTION);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'TIMEOUT') && this.workFlow.TIMEOUT && Object.keys(this.workFlow.TIMEOUT).length > 0) {
            Object.assign(view, this.workFlow.TIMEOUT);
            this.$vue.$root.addModal(view);
        }
        else if (Object.is(viewType, 'WFRETURN') && this.workFlow.WFRETURN && Object.keys(this.workFlow.WFRETURN).length > 0) {
            Object.assign(view, this.workFlow.WFRETURN);
            this.$vue.$root.addModal(view);
        }
        return subject;
    };
    WorkFlowEditService.prototype.openEditTextAnnotationView = function (opt) {
        return;
    };
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {WorkFlowEditService}
     * @memberof WorkFlowEditService
     */
    WorkFlowEditService.workFlowEditService = null;
    return WorkFlowEditService;
}());

"use strict";
/**
 * 工作流设计工具
 *
 * @class IBizWFDesignField2
 */
var IBizWFDesignField2 = /** @class */ (function () {
    function IBizWFDesignField2(opts) {
        if (opts === void 0) { opts = {}; }
        this.$applicationRef = null;
        /**
         * activit设计工具iframe的window对象
         *
         * @private
         * @type {ActivitiWorkflowEditor}
         * @memberof IBizWFDesignField2
         */
        this.$flowFrame = null;
        /**
         * 打开编辑视图服务
         *
         * @private
         * @type {WorkFlowEditService}
         * @memberof IBizWFDesignField2
         */
        this.$workFlowEditService = null;
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$lastValue = null;
        /**
         *
         *
         * @private
         * @type {boolean}
         * @memberof IBizWFDesignField2
         */
        this.$flowFrameLoaded = false;
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$logicJson = {};
        /**
         *
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$logicJson2 = {};
        /**
         *
         *
         * @private
         * @type {string}
         * @memberof IBizWFDesignField2
         */
        this.$logicJsonString = '';
        /**
         * 值改变发射流
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizWFDesignField2
         */
        this.$valueChangeSubject = new rxjs.Subject();
        /**
         * 全屏按钮事件
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizWFDesignField2
         */
        this.$fullScreenSubject = new rxjs.Subject();
        /**
         * vue对象
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.$vue = null;
        /**
         * 工作流
         *
         * @private
         * @type {*}
         * @memberof IBizWFDesignField2
         */
        this.workFlow = {};
        this.$vue = opts.vue;
        Object.assign(this.workFlow, opts.workFlow);
        this.$workFlowEditService = new WorkFlowEditService({ vue: this.$vue, workFlow: opts.workFlow });
    }
    /**
     *
     *
     * @param {any} value
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.setValue = function (value) {
        var wfEngineType = this.getWFEngineType();
        this.$lastValue = value;
        if (wfEngineType && wfEngineType === 'ACTIVITI') {
            this.loadFromXML(value);
        }
    };
    /**
     *
     *
     * @param {any} strXML
     * @returns {void}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.loadFromXML = function (strXML) {
        var xmlDoc = IBizActivitiUtil.parseXML(strXML);
        if (xmlDoc == null) {
            alert('工作流流程不正确');
            return;
        }
        for (var i = 0; i < xmlDoc.childNodes.length; i++) {
            if (xmlDoc.childNodes[i].nodeName === 'WFCONFIG') {
                this.loadLogic(xmlDoc.childNodes[i]);
                break;
            }
        }
        if (this.$flowFrameLoaded) {
            this.loadConfig();
        }
    };
    /**
     *
     *
     * @param {any} xmlNode
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.loadLogic = function (xmlNode) {
        var xml = {};
        IBizActivitiUtil.loadXMLNode(xmlNode, xml);
        this.$logicJson = {};
        this.$logicJson2 = {};
        Object.assign(this.$logicJson, xml);
        Object.assign(this.$logicJson2, xml);
        Object.assign(this.$logicJson, { srfparenttype: this.workFlow.NODE.type, srfder1nid: this.workFlow.NODE.der1nid, srfparentkey: xml[this.workFlow.FLOW.id] });
        Object.assign(this.$logicJson2, { srfparenttype: this.workFlow.LINK.type, srfder1nid: this.workFlow.LINK.der1nid, srfparentkey: xml[this.workFlow.FLOW.id] });
        this.$logicJsonString = xml.activitimodel;
        delete this.$logicJson.activitimodel;
        delete this.$logicJson2.activitimodel;
    };
    /**
     *
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.loadConfig = function () {
        console.log('loadConfig');
        if (this.$logicJsonString) {
            this.$flowFrame.setModel(this.$logicJsonString);
        }
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getValue = function () {
        var wfEngineType = this.getWFEngineType();
        if (wfEngineType === 'ACTIVITI') {
            return this.exportToXml();
        }
        return this.$lastValue;
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.exportToXml = function () {
        console.log('exportToXml');
        if (this.$flowFrameLoaded) {
            var value = this.$flowFrame.getModel();
            var writer = new IBizXMLWriter();
            writer.beginNode('WFCONFIG');
            if (this.$logicJson) {
                writer.attrib(this.workFlow.FLOW.id.toUpperCase(), this.$logicJson[this.workFlow.FLOW.id]);
                // writer.attrib('PSWFID', this.$logicJson.pswfid);
                // writer.attrib('PSWFVERSIONID', this.$logicJson.pswfversionid);
                // writer.attrib('PSSYSTEMID', this.$logicJson.pssystemid);
            }
            writer.attrib('ACTIVITIMODEL', value);
            writer.endNode();
            writer.close();
            var strXML = '<?xml version="1.0" encoding="utf-8" ?>' + writer.toString();
            return strXML;
        }
        else {
            return this.$lastValue;
        }
    };
    /**
     * 设置iframeWindow对象
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.setFlowFrame = function (flowFrame) {
        this.$flowFrame = flowFrame;
    };
    /**
     *
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onFlowFrameLoad = function () {
        console.log('onFlowFrameLoad');
        this.$flowFrameLoaded = true;
        this.$flowFrame.setParentObj(this);
    };
    /**
     * 获取当前工作流编辑器类型
     *
     * @returns {String}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getWFEngineType = function () {
        return 'ACTIVITI';
    };
    /**
     * 发射全屏事件
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.showFullScreen = function () {
        this.$fullScreenSubject.next(this.getValue());
    };
    /**
     * 开启全屏模式
     *
     * @returns {Observable<string>}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onFullScreen = function () {
        return this.$fullScreenSubject.asObservable();
    };
    /**
     * 全屏模式关闭回调
     *
     * @param {any} win
     * @returns {void}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onFullScreenWindowClose = function (res) {
        var activeData = res.activeData;
        if (!activeData) {
            console.log('没有返回任何数据!');
            return;
        }
        var wfmodel = activeData.wfmodel;
        this.setValue(wfmodel);
    };
    /**
     *
     *
     * @param {any} shape
     * @returns {void}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editShape = function (shape) {
        console.log('editShape');
        if (!shape) {
            return;
        }
        var stencilid = shape.getStencil().id();
        var type = stencilid.replace('http://b3mn.org/stencilset/bpmn2.0#', '');
        var keyvalue = shape.properties['oryx-documentation'];
        if (type === 'StartNoneEvent') {
            this.editProcess(shape, 'START', keyvalue);
            return;
        }
        if (type === 'UserTask') {
            this.editProcess(shape, 'INTERACTIVE', keyvalue);
            return;
        }
        if (type === 'ServiceTask') {
            this.editProcess(shape, 'PROCESS', keyvalue);
            return;
        }
        if (type === 'ExclusiveGateway') {
            this.editProcess(shape, 'EXCLUSIVEGATEWAY', keyvalue);
            return;
        }
        if (type === 'InclusiveGateway') {
            this.editProcess(shape, 'INCLUSIVEGATEWAY', keyvalue);
            return;
        }
        if (type === 'SubProcess') {
            this.editProcess(shape, 'EMBED', keyvalue);
            return;
        }
        if (type === 'CatchTimerEvent') {
            this.editProcess(shape, 'TIMEREVENT', keyvalue);
            return;
        }
        if (type === 'ParallelGateway') {
            this.editProcess(shape, 'PARALLELGATEWAY', keyvalue);
            return;
        }
        if (type === 'TextAnnotation') {
            this.editTextAnnotation(shape);
            return;
        }
        if (type === 'SequenceFlow') {
            var fromShape = this.getFromShape(shape);
            if (!fromShape) {
                alert('无法获取连接源处理对象');
                return;
            }
            var stencilid2 = fromShape.getStencil().id();
            var type2 = stencilid2.replace('http://b3mn.org/stencilset/bpmn2.0#', '');
            if (type2 === 'UserTask') {
                this.editLink(shape, 'IAACTION', keyvalue);
            }
            else {
                this.editLink(shape, 'ROUTE', keyvalue);
            }
            return;
        }
        if (type === 'EndNoneEvent') {
            this.editProcess(shape, 'END', keyvalue);
            return;
        }
    };
    /**
     * 注释节点编辑
     *
     * @param {any} tag
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editTextAnnotation = function (tag) {
        throw new Error('暂无视图无法打开');
        // 需要的视图WFDesignField2MemoView
        // this.$workFlowEditService.openEditTextAnnotationView({ 'viewParam': { memo: tag.properties['oryx-text'] }, 'modalZIndex': 800 }).subscribe(
        //     res => {
        //         if (res && Object.is('OK', res.ret)) {
        //             this.onNodeWindowClose(res);
        //         }
        //     }
        // );
    };
    /**
     *
     *
     * @param {any} tag
     * @param {any} detailtype
     * @param {any} keyvalue
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editProcess = function (tag, detailtype, keyvalue) {
        var _this = this;
        if (keyvalue === void 0) { keyvalue = ''; }
        var param = {
            srfparenttype: this.$logicJson.srfparenttype,
            srfder1nid: this.$logicJson.srfder1nid,
            srfparentkey: this.$logicJson.srfparentkey,
            wfprocesstype: detailtype
        };
        if (keyvalue && !Object.is(keyvalue, '')) {
            Object.assign(param, { srfkey: keyvalue });
        }
        this.$workFlowEditService.openProcessEditView(detailtype, { 'viewParam': param, 'modalZIndex': 600 }).subscribe(function (res) {
            if (res && Object.is('OK', res.ret)) {
                res.activeNode = tag;
                _this.onNodeWindowClose(res);
                // this.clearTickTimer();
                // this.allTick();
            }
        });
        // this.modalTick();
        // this.allTick();
    };
    /**
     *
     *
     * @param {any} obj
     * @returns {*}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getFromShape = function (obj) {
        var shapeFacade = this.getShapeFacade(obj);
        if (!shapeFacade) {
            return null;
        }
        var strResourceId = obj['resourceId'];
        if (!strResourceId) {
            return null;
        }
        var shapes = shapeFacade.getCanvas().getChildShapes(true);
        if (!shapes || shapes.length === 0) {
            return null;
        }
        for (var i = 0; i < shapes.length; i++) {
            var shape = shapes[i];
            var outgoings = shape['outgoing'];
            if (!outgoings || outgoings.length === 0) {
                continue;
            }
            for (var j = 0; j < outgoings.length; j++) {
                var outgoing = outgoings[j];
                if (outgoing['resourceId'] === strResourceId) {
                    return shape;
                }
            }
        }
        return null;
    };
    /**
     *
     *
     * @param {any} obj
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.getShapeFacade = function (obj) {
        if (obj.facade) {
            return obj.facade;
        }
        if (obj.parent) {
            return this.getShapeFacade(obj.parent);
        }
        return null;
    };
    /**
     *
     *
     * @param {any} tag
     * @param {any} linktype
     * @param {any} keyvalue
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.editLink = function (tag, linktype, keyvalue) {
        var _this = this;
        var loadParam = {
            srfparenttype: this.$logicJson2.srfparenttype,
            srfder1nid: this.$logicJson2.srfder1nid,
            srfparentkey: this.$logicJson2.srfparentkey,
            wflinktype: linktype,
        };
        if (keyvalue && !Object.is(keyvalue, '')) {
            Object.assign(loadParam, { srfkey: keyvalue });
        }
        var _data = {};
        if (!keyvalue || Object.is(keyvalue, '')) {
            _data[this.workFlow.FORM.id] = 'SRFTEMPKEY:' + this.newGuid();
            _data[this.workFlow.FORM.name] = '源处理名称';
        }
        Object.assign(loadParam, _data);
        this.$workFlowEditService.openLinkEditView(linktype, { 'viewParam': loadParam, 'modalZIndex': 600 }).subscribe(function (res) {
            if (res && Object.is('OK', res.ret)) {
                res.activeLink = tag;
                _this.onLinkWindowClose(res);
                // this.clearTickTimer();
                // this.allTick();
            }
        });
        // this.allTick();
        // this.modalTick();
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.newGuid = function () {
        var guid = '';
        for (var i = 1; i <= 32; i++) {
            var n = Math.floor(Math.random() * 16.0).toString(16);
            guid += n;
            if ((i === 8) || (i === 12) || (i === 16) || (i === 20)) {
                guid += '-';
            }
        }
        return guid;
    };
    /**
     * 节点编辑视图关闭回调
     *
     * @param {any} res
     * @returns {*}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onNodeWindowClose = function (res) {
        var node = res.activeNode;
        var activeData = res.activeData;
        if (!activeData) {
            console.log('没有返回任何数据!');
            return;
        }
        var xml = {};
        Object.assign(xml, activeData);
        node.setProperty('oryx-name', xml[this.workFlow.NODE.name]);
        node.setProperty('oryx-documentation', xml[this.workFlow.NODE.id]);
        this.$flowFrame.updateProcess(node);
        this.$valueChangeSubject.next(this.getValue());
    };
    /**
     * 链接线编辑视图关闭回调
     *
     * @param {any} win
     * @param {any} eOpts
     * @returns
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.onLinkWindowClose = function (res) {
        var link = res.activeLink;
        var activeData = res.activeData;
        if (!activeData) {
            console.log('没有返回任何数据!');
            return;
        }
        var xml = {};
        Object.assign(xml, activeData);
        // let logicname = xml.logicname;
        // if (!logicname) {
        //     logicname = xml.pswflinkname;
        // }
        link.setProperty('oryx-name', xml[this.workFlow.LINK.name]);
        link.setProperty('oryx-documentation', xml[this.workFlow.LINK.id]);
        // 更新
        this.$flowFrame.updateConnection(link);
        this.$valueChangeSubject.next(this.getValue());
    };
    /**
     * 对创建的模态框及其子组件进行脏检查，检查完毕后进行一次全局脏检查
     *
     * @private
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.allTick = function () {
        for (var i = 0; i < 3; i++) {
            setTimeout(function () {
                // this.$applicationRef.tick();
            }, 300);
        }
    };
    /**
     * 对打开的模态框进行脏检查
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.modalTick = function () {
        var components = this.$applicationRef.components;
        if (this.$tickTimer) {
            clearInterval(this.$tickTimer);
            this.$tickTimer = undefined;
        }
        this.$tickTimer = setInterval(function () {
            // this.$applicationRef.tick();
        }, 100);
    };
    /**
     * 清除脏检查定时器
     *
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.clearTickTimer = function () {
        if (this.$tickTimer) {
            clearInterval(this.$tickTimer);
            this.$tickTimer = undefined;
        }
    };
    /**
     * 使用该方法订阅工作流值改变
     *
     * @returns {Observable<string>}
     * @memberof IBizWFDesignField2
     */
    IBizWFDesignField2.prototype.wfDataChange = function () {
        return this.$valueChangeSubject.asObservable();
    };
    return IBizWFDesignField2;
}());

"use strict";
var _this = this;
Vue.component('ibiz-activiti', {
    template: "\n    <div style=\"width:100%; height:600px;\">\n        <iframe  width=\"100%\" height=\"100%\" ref=\"ibizactiviti\" :src=\"url\" frameborder=\"0\"></iframe>\n    </div>\n    ",
    props: ['field', 'form'],
    data: function () {
        var data = {
            wfDesign: new IBizWFDesignField2({ vue: this, workFlow: this.field.editorParams }),
            activitimodel: null,
            $iframeIsLoadEnd: false,
            formLoaded: false,
        };
        var url = "../../../packages/assets/plugins/static/activiti/index.html?_=" + data.wfDesign.newGuid();
        Object.assign(data, { url: url });
        return data;
    },
    mounted: function () {
        var _this = this;
        this.wfDesign.wfDataChange().subscribe(function (res) {
            _this.activitimodel = res;
            _this.onChangeCallback(_this.activitimodel);
        });
        this.wfDesign.onFullScreen().subscribe(function (model) {
            var subject = new rxjs.Subject();
            var view = {
                subject: subject,
                viewname: 'ibiz-activiti-full-screen',
                modalviewname: 'ibiz-activiti-full-screen',
                title: '工作流设计',
                viewparam: { activitiModel: model, workFlow: _this.field.editorParams },
            };
            _this.$root.addModal(view);
            subject.subscribe(function (result) {
                if (result && Object.is(result.ret, 'OK')) {
                    console.log(result);
                    _this.wfDesign.setValue(result.activitiModel);
                }
            });
        });
        if (this.field) {
            this.writeValue(this.field.getValue());
            this.field.getValue = function () {
                if (_this.$iframeIsLoadEnd && _this.activitimodel) {
                    _this.activitimodel = _this.wfDesign.getValue();
                    _this.field.setValue(_this.activitimodel);
                    return _this.activitimodel;
                }
                return undefined;
            };
        }
    },
    activated: function () {
        console.log(123123123);
        this.initActiviti();
    },
    methods: {
        initActiviti: function () {
            var _this = this;
            var iframe = this.$refs.ibizactiviti;
            if (iframe) {
                this.workflowDesign = iframe.contentWindow;
                if (this.workflowDesign) {
                    this.workflowDesign.editorOnLoad = function () {
                        console.log('form before ');
                        if (_this.formLoaded) {
                            console.log('form after ');
                            _this.wfDesign.setFlowFrame(_this.workflowDesign);
                            _this.wfDesign.onFlowFrameLoad();
                            _this.$iframeIsLoadEnd = true;
                            delete _this.workflowDesign.editorOnLoad;
                        }
                    };
                }
                else {
                    console.error('设计工具初始化失败');
                }
            }
            if (this.form) {
                this.form.on('FORMLOADED').subscribe(function () {
                    // this.formLoaded = true;
                });
            }
        },
        onTouchedCallback: function () {
            return function () { };
        },
        onChangeCallback: function (data) {
            if (_this.field) {
                _this.field.setValue(data);
            }
        },
        writeValue: function (value) {
            if (value) {
                this.activitimodel = value;
                if (this.wfDesign) {
                    this.wfDesign.setValue(this.activitimodel);
                }
            }
        },
        registerOnChange: function (fn) {
            this.onChangeCallback = fn;
        },
        registerOnTouched: function (fn) {
            this.onTouchedCallback = fn;
        },
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            this.writeValue(newVal);
            if (!Object.is(newVal, '')) {
                this.formLoaded = true;
                console.log(newVal);
            }
        }
    },
});

"use strict";
Vue.component('ibiz-activiti-full-screen', {
    template: "\n        <div style=\"width:100%; height:100%;\">\n            <iframe width=\"100%\" height=\"100%\" ref=\"ibizactivitifullscreen\" :src=\"url\" frameborder=\"0\" ></iframe>\n        </div>\n    ",
    props: ['params'],
    data: function () {
        var data = {
            wfDesign: new IBizWFDesignField2({ vue: this, workFlow: this.params.workFlow }),
            $iframeIsLoadEnd: false,
            workflowDesign: null,
        };
        var url = "../../../packages/assets/plugins/static/activiti/index.html?fsmode=true&_=" + data.wfDesign.newGuid();
        Object.assign(data, { url: url });
        if (this.params && this.params.activitiModel) {
            Object.assign(data, { activitiModel: this.params.activitiModel });
        }
        return data;
    },
    mounted: function () {
        this.designInit();
        if (this.wfDesign) {
            this.wfDesign.setValue(this.activitiModel);
        }
    },
    methods: {
        designInit: function () {
            var _this = this;
            var iframe = this.$refs.ibizactivitifullscreen;
            if (!iframe) {
                return;
            }
            this.workflowDesign = iframe.contentWindow;
            if (this.workflowDesign) {
                this.workflowDesign.onload = function () {
                    _this.wfDesign.setFlowFrame(_this.workflowDesign);
                    _this.wfDesign.onFlowFrameLoad();
                    _this.$iframeIsLoadEnd = true;
                    // this.wfDesign.setValue(this.activitiModel);
                };
                this.workflowDesign.onSelectionChanged = function () {
                    var activitiModel = _this.wfDesign.getValue();
                    _this.$emit('dataChange', { ret: 'OK', activitiModel: activitiModel });
                };
                this.isDesignInitEnd = true;
            }
            else {
                console.error('设计工具初始化失败');
            }
        }
    }
});
