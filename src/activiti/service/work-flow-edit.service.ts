/**
 * 工作流编辑服务
 *
 * @class WorkFlowEditService
 */
class WorkFlowEditService {

    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {WorkFlowEditService}
     * @memberof WorkFlowEditService
     */
    private static workFlowEditService: WorkFlowEditService = null;

    /**
     * vue  对象
     *
     * @private
     * @type {*}
     * @memberof WorkFlowEditService
     */
    private $vue: any = null;

    /**
     * 
     *
     * @private
     * @type {*}
     * @memberof WorkFlowEditService
     */
    private workFlow: any = {};

    /**
     * Creates an instance of WorkFlowEditService.
     * 创建 WorkFlowEditService 实例
     * 
     * @param {*} opts
     * @memberof WorkFlowEditService
     */
    constructor(opts: any = {}) {
        this.$vue = opts.vue;
        Object.assign(this.workFlow, opts.workFlow);
    }

    public openProcessEditView(viewType: string, opt: any = {}): Observable<any> {
        return this.openProcess(viewType, opt);
    }

    private openProcess(viewType: string, opt: any): Observable<any> {
        let subject: Subject<any> = new rxjs.Subject();
        if (!this.$vue) {
            return null;
        }
        let view = { subject: subject, viewparam: opt.viewParam };
        if (Object.is(viewType, 'START') && this.workFlow.START && Object.keys(this.workFlow.START).length > 0) {
            Object.assign(view, this.workFlow.START);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'INTERACTIVE') && this.workFlow.INTERACTIVE && Object.keys(this.workFlow.INTERACTIVE).length > 0) {
            Object.assign(view, this.workFlow.INTERACTIVE);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'PROCESS') && this.workFlow.PROCESS && Object.keys(this.workFlow.PROCESS).length > 0) {
            Object.assign(view, this.workFlow.PROCESS);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'END') && this.workFlow.END && Object.keys(this.workFlow.END).length > 0) {
            Object.assign(view, this.workFlow.END);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'EMBED') && this.workFlow.EMBED && Object.keys(this.workFlow.EMBED).length > 0) {
            Object.assign(view, this.workFlow.EMBED);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'PARALLEL') && this.workFlow.PARALLEL && Object.keys(this.workFlow.PARALLEL).length > 0) {
            Object.assign(view, this.workFlow.PARALLEL);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'PARALLELGATEWAY') && this.workFlow.PARALLELGATEWAY && Object.keys(this.workFlow.PARALLELGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.PARALLELGATEWAY);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'EXCLUSIVEGATEWAY') && this.workFlow.EXCLUSIVEGATEWAY && Object.keys(this.workFlow.EXCLUSIVEGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.EXCLUSIVEGATEWAY);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'INCLUSIVEGATEWAY') && this.workFlow.INCLUSIVEGATEWAY && Object.keys(this.workFlow.INCLUSIVEGATEWAY).length > 0) {
            Object.assign(view, this.workFlow.INCLUSIVEGATEWAY);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'TIMEREVENT') && this.workFlow.TIMEREVENT && Object.keys(this.workFlow.TIMEREVENT).length > 0) {
            Object.assign(view, this.workFlow.TIMEREVENT);
            this.$vue.$root.addModal(view);
        }
        return subject;
    }

    public openLinkEditView(viewType: string, opt: any = {}): Observable<any> {
        return this.openLink(viewType, opt);
    }

    private openLink(viewType: string, opt: any): Observable<any> {
        if (!this.$vue) {
            return null;
        }
        let subject: Subject<any> = new rxjs.Subject();
        let view = { subject: subject, viewparam: opt.viewParam };
        if (Object.is(viewType, 'ROUTE') && this.workFlow.ROUTE && Object.keys(this.workFlow.ROUTE).length > 0) {
            Object.assign(view, this.workFlow.ROUTE);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'IAACTION') && this.workFlow.IAACTION && Object.keys(this.workFlow.IAACTION).length > 0) {
            Object.assign(view, this.workFlow.IAACTION);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'TIMEOUT') && this.workFlow.TIMEOUT && Object.keys(this.workFlow.TIMEOUT).length > 0) {
            Object.assign(view, this.workFlow.TIMEOUT);
            this.$vue.$root.addModal(view);
        } else if (Object.is(viewType, 'WFRETURN') && this.workFlow.WFRETURN && Object.keys(this.workFlow.WFRETURN).length > 0) {
            Object.assign(view, this.workFlow.WFRETURN);
            this.$vue.$root.addModal(view);
        }
        return subject;
    }

    public openEditTextAnnotationView(opt: any): Observable<any> {
        return;
    }
}