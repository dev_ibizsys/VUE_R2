Vue.component('ibiz-activiti', {
    template: `
    <div style="width:100%; height:600px;">
        <iframe  width="100%" height="100%" ref="ibizactiviti" :src="url" frameborder="0"></iframe>
    </div>
    `,
    props: ['field', 'form'],
    data: function () {
        let data: any = {
            wfDesign: new IBizWFDesignField2({ vue: this, workFlow: this.field.editorParams }),
            activitimodel: null,
            $iframeIsLoadEnd: false,
            formLoaded: false,
        };
        const url = `../../../packages/assets/plugins/static/activiti/index.html?_=${data.wfDesign.newGuid()}`;
        Object.assign(data, { url: url });
        return data;
    },
    mounted: function () {
        this.wfDesign.wfDataChange().subscribe(
            (res: string) => {
                this.activitimodel = res;
                this.onChangeCallback(this.activitimodel);
            }
        );
        this.wfDesign.onFullScreen().subscribe((model: string) => {
            let subject: Subject<any> = new rxjs.Subject();
            let view: any = {
                subject: subject,
                viewname: 'ibiz-activiti-full-screen',
                modalviewname: 'ibiz-activiti-full-screen',
                title: '工作流设计',
                viewparam: { activitiModel: model, workFlow: this.field.editorParams },
            };
            this.$root.addModal(view);
            subject.subscribe((result: any) => {
                if (result && Object.is(result.ret, 'OK')) {
                    console.log(result);
                    this.wfDesign.setValue(result.activitiModel);
                }
            });
        });
        if (this.field) {
            this.writeValue(this.field.getValue());
            this.field.getValue = () => {
                if (this.$iframeIsLoadEnd && this.activitimodel) {
                    this.activitimodel = this.wfDesign.getValue();
                    this.field.setValue(this.activitimodel);
                    return this.activitimodel;
                }
                return undefined;
            };
        }
    },
    activated: function () {
        console.log(123123123);
        this.initActiviti();
    },
    methods: {
        initActiviti(): void {
            let iframe: any = this.$refs.ibizactiviti;
            if (iframe) {
                this.workflowDesign = iframe.contentWindow;
                if (this.workflowDesign) {
                    this.workflowDesign.editorOnLoad = () => {
                        console.log('form before ');
                        if (this.formLoaded) {
                            console.log('form after ');
                            this.wfDesign.setFlowFrame(this.workflowDesign);
                            this.wfDesign.onFlowFrameLoad();
                            this.$iframeIsLoadEnd = true;
                            delete this.workflowDesign.editorOnLoad;
                        }
                    };
                } else {
                    console.error('设计工具初始化失败');
                }
            }
            if (this.form) {
                this.form.on('FORMLOADED').subscribe(() => {
                    // this.formLoaded = true;
                });
            }
        },
        onTouchedCallback: () => {
            return function () { };
        },
        onChangeCallback: (data) => {
            if (this.field) {
                this.field.setValue(data);
            }
        },
        writeValue(value: any): void {
            if (value) {
                this.activitimodel = value;
                if (this.wfDesign) {
                    this.wfDesign.setValue(this.activitimodel);
                }
            }
        },
        registerOnChange(fn: any): void {
            this.onChangeCallback = fn;
        },
        registerOnTouched(fn: any): void {
            this.onTouchedCallback = fn;
        },
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            this.writeValue(newVal);
            if (!Object.is(newVal, '')) {
                this.formLoaded = true;
                console.log(newVal);
            }
        }
    },
});