Vue.component('ibiz-activiti-full-screen', {
    template: `
        <div style="width:100%; height:100%;">
            <iframe width="100%" height="100%" ref="ibizactivitifullscreen" :src="url" frameborder="0" ></iframe>
        </div>
    `,
    props: ['params'],
    data: function () {
        let data: any = {
            wfDesign: new IBizWFDesignField2({ vue: this, workFlow: this.params.workFlow }),
            $iframeIsLoadEnd: false,
            workflowDesign: null,
        };
        const url = `../../../packages/assets/plugins/static/activiti/index.html?fsmode=true&_=${data.wfDesign.newGuid()}`;
        Object.assign(data, { url: url });
        if (this.params && this.params.activitiModel) {
            Object.assign(data, { activitiModel: this.params.activitiModel });
        }
        return data;
    },
    mounted: function () {
        this.designInit();
        if (this.wfDesign) {
            this.wfDesign.setValue(this.activitiModel);
        }
    },
    methods: {
        designInit() {
            let iframe: any = this.$refs.ibizactivitifullscreen;
            if (!iframe) {
                return;
            }
            this.workflowDesign = iframe.contentWindow;
            if (this.workflowDesign) {
                this.workflowDesign.onload = () => {
                    this.wfDesign.setFlowFrame(this.workflowDesign);
                    this.wfDesign.onFlowFrameLoad();
                    this.$iframeIsLoadEnd = true;
                    // this.wfDesign.setValue(this.activitiModel);
                };
                this.workflowDesign.onSelectionChanged = () => {
                    let activitiModel = this.wfDesign.getValue();
                    this.$emit('dataChange', { ret: 'OK', activitiModel: activitiModel });
                };
                this.isDesignInitEnd = true;
            } else {
                console.error('设计工具初始化失败');
            }
        }
    }
});