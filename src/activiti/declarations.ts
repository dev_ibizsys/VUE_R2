interface Observable<T> {
    // inherited from index/Observable
    // static create: Function
    // static if: typeof iif
    // static throw: typeof throwError
    // constructor(subscribe?: (this: Observable<T>, subscriber: Subscriber<T>) => TeardownLogic)
    _isScalar: boolean
    source: Observable<any>
    // operator: Operator<any, T>
    // lift<R>(operator: Operator<T, R>): Observable<R>
    subscribe(observerOrNext?: PartialObserver1<T> | ((value: T) => void), error?: (error: any) => void, complete?: () => void): Subscription1
    // _trySubscribe(sink: Subscriber<T>): TeardownLogic
    // forEach(next: (value: T) => void, promiseCtor?: PromiseConstructorLike): Promise<void>
    pipe(...operations: OperatorFunction1<any, any>[]): Observable<any>
    // toPromise(promiseCtor?: PromiseConstructorLike): Promise<T>
};

declare type UnaryFunction1<T, R> = (source: T) => R;
declare type OperatorFunction1<T, R> = UnaryFunction1<Observable<T>, Observable<R>>;

interface NextObserver<T> {
    closed?: boolean;
    next: (value: T) => void;
    error?: (err: any) => void;
    complete?: () => void;
}
interface ErrorObserver<T> {
    closed?: boolean;
    next?: (value: T) => void;
    error: (err: any) => void;
    complete?: () => void;
}
interface CompletionObserver<T> {
    closed?: boolean;
    next?: (value: T) => void;
    error?: (err: any) => void;
    complete: () => void;
}

declare type PartialObserver1<T> = NextObserver<T> | ErrorObserver<T> | CompletionObserver<T>;
declare type Subscription1 = any;

interface Subject<T> extends Observable<T> {
    // static create: Function
    constructor()
    // observers: Observer<T>[]
    closed: false
    isStopped: false
    hasError: false
    thrownError: any
    // lift<R>(operator: Operator<T, R>): Observable<R>
    next(value?: T)
    error(err: any)
    complete()
    unsubscribe()
    // _trySubscribe(subscriber: Subscriber<T>): TeardownLogic
    // _subscribe(subscriber: Subscriber<T>): Subscription
    asObservable(): Observable<T>
}

declare var rxjs;
declare var Vue;
