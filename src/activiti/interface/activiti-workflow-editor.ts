/**
 * activiti暴露的api接口
 *
 * @interface ActivitiWorkflowEditor
 */
interface ActivitiWorkflowEditor {
    /**
     * 初始化视图
     * 
     * @param {*} arg 
     * @memberof ActivitiWorkflow
     */
    initView(arg: any): void;
    /**
     * 设置模型
     * 
     * @param {*} data 
     * @memberof ActivitiWorkflow
     */
    setModel(data: any): void;
    /**
     * 获取模型
     * 
     * @memberof ActivitiWorkflow
     */
    getModel(): string;
    /**
     * 设置父控制器对象
     * 
     * @param {any} obj 
     * @memberof ActivitiWorkflow
     */
    setParentObj(obj: any): void;
    /**
     * 
     * 
     * @memberof ActivitiWorkflow
     */
    resetAll(): void;
    /**
     * 
     * 
     * @memberof ActivitiWorkflow
     */
    onLoad(): void;
    /**
     * 
     * 
     * @param {*} obj 
     * @memberof ActivitiWorkflow
     */
    updateProcess(obj: any): void;
    /**
     * 
     * 
     * @param {*} obj 
     * @memberof ActivitiWorkflow
     */
    updateConnection(obj: any): void;
    /**
     * 
     * 
     * @returns {*} 
     * @memberof ActivitiWorkflow
     */
    getFacade(): any;
}