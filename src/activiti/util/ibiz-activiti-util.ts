class IBizActivitiUtil {

    /**
     * 将文本格式的xml转换为dom模式
     *
     * @static
     * @param {string} strXml
     * @returns {Document}
     * @memberof IBizActivitiUtil
     */
    public static parseXML(strXml: string): Document {
        if (strXml) {
            return new DOMParser().parseFromString(strXml, 'text/xml');
        }
    }

    /**
     * 将xml转换为object对象
     *
     * @static
     * @param {*} node
     * @param {*} [obj={}]
     * @memberof IBizActivitiUtil
     */
    public static loadXMLNode(node: any, obj: any = {}): void {
        if (node && node.attributes) {
            let arr: any = node.attributes;
            for (let i = 0; i < arr.length; i++) {
                let A = arr.item(i).name;
                const B = arr.item(i).value;
                A = A.toLowerCase();
                obj[A] = B;
            }
        }
    }

    /**
     * 将object转换为xml对象
     *
     * @static
     * @param {*} XML
     * @param {*} obj
     * @memberof IBizActivitiUtil
     */
    public static saveXMLNode(XML: any, obj: any) {
        let proName = '';
        for (proName in obj) {
            let value = obj[proName];
            if (!value || value instanceof Object || typeof (value) === 'function') {
                continue;
            }
            let proValue = obj[proName].toString();
            if (proValue !== '') {
                XML.attrib(proName, proValue);
            }
        }
    }
}