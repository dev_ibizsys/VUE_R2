/**
 * 表单属性项
 *
 * @class IBizFormField
 * @extends {IBizFormItem}
 */
class IBizFormField extends IBizFormItem {

    /**
     * label 宽度
     *
     * @type {number}
     * @memberof IBizFormField
     */
    public labelWidth: number = 130;

    /**
     * 实体属性输入旧值
     *
     * @private
     * @type {string}
     * @memberof IBizFormField
     */
    private oldVal: string = '';

    /**
     * 编辑器参数
     *
     * @type {*}
     * @memberof IBizFormField
     */
    public editorParams: any = {};

    /**
     * 输入防抖
     *
     * @private
     * @type {Subject<any>}
     * @memberof IBizFormField
     */
    private subject: Subject<any> = new rxjs.Subject();

    /**
     * Creates an instance of IBizFormField.
     * 创建 IBizFormField 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormField
     */
    constructor(opts: any = {}) {
        super(opts);
        this.labelWidth = opts.labelWidth;
        if (opts.editorParams) {
            Object.assign(this.editorParams, opts.editorParams);
        }
        this.subject
            .pipe(
                rxjs.operators.debounceTime(300),
                rxjs.operators.distinctUntilChanged()
            ).subscribe((data: any) => {
                this.value = data;
            });
    }

    /**
     * 值设定
     *
     * @memberof IBizFormField
     */
    public setData($event: any) {
        if (!event) {
            return;
        }
        if (!$event.target) {
            return;
        }
        this.subject.next($event.target.value);
    }

    /**
     * 设置旧值
     *
     * @param {string} val
     * @memberof IBizFormField
     */
    public setOldValue(val: string): void {
        this.oldVal = val;
    }

    /**
     * 获取旧值
     *
     * @returns {string}
     * @memberof IBizFormField
     */
    public getOldValue(): string {
        return this.oldVal;
    }

    /**
     * 属性值变化
     *
     * @param {*} event
     * @memberof IBizFormField
     */
    public valueChange(event: any): void {

        if (Object.is(event, this.value)) {
            return;
        }
        this.value = event;
    }

    /**
     * 输入框失去焦点触发
     *
     * @param {*} _value
     * @returns {void}
     * @memberof IBizFormField
     */
    public onBlur(_value: any): void {

        if (!_value || Object.is(_value, this.value)) {
            return;
        }
        this.value = _value;
    }

    /**
     * 键盘事件
     *
     * @param {*} $event
     * @returns {void}
     * @memberof IBizFormField
     */
    public onKeydown($event: any): void {
        if (!$event) {
            return;
        }
        if ($event.keyCode !== 13) {
            return;
        }
        if (Object.is($event.target.value, this.value)) {
            return;
        }

        this.value = $event.target.value;
    }

}
