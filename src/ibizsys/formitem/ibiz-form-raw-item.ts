/**
 * 表单直接内容
 *
 * @class IBizFormRawItem
 * @extends {IBizFormItem}
 */
class IBizFormRawItem extends IBizFormItem {

    /**
     * Creates an instance of IBizFormRawItem.
     * 创建 IBizFormRawItem 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormRawItem
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
