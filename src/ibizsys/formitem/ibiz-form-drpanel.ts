/**
 * 表单关系部件
 *
 * @class IBizFormDRPanel
 * @extends {IBizFormItem}
 */
class IBizFormDRPanel extends IBizFormItem {

    /**
     * 是否开启遮罩
     *
     * @type {boolean}
     * @memberof IBizFormDRPanel
     */
    public blockUI: boolean = false;

    /**
     * 遮罩提示信息
     *
     * @type {string}
     * @memberof IBizFormDRPanel
     */
    public blockUITipInfo: string = '请先保存主数据';

    /**
     * 刷新数据
     *
     * @private
     * @type {*}
     * @memberof IBizFormDRPanel
     */
    private hookItems: any = {};

    /**
     * 父数据
     *
     * @type {string}
     * @memberof IBizFormDRPanel
     */
    public srfParentData: any = {};

    /**
     * 父模式
     *
     * @type {*}
     * @memberof IBizFormDRPanel
     */
    public srfParentMode: any = {};

    /**
     * 关联数据
     *
     * @type {*}
     * @memberof IBizFormDRPanel
     */
    public srfReferData: any = {};

    /**
     * 刷新关联视图计数
     *
     * @type {number}
     * @memberof IBizFormDRPanel
     */
    public refreshRefView: number = 0;

    /**
     * 关系表单项值变化，刷新关联视图
     *
     * @type {string}
     * @memberof IBizFormDRPanel
     */
    public refreshItems: string = '';

    /**
     * 关系页触发保存
     *
     * @type {Number}
     * @memberof IBizFormDRPanel
     */
    public saveRefView: number = 0;

    /**
     * 关系页是否值变更
     *
     * @type {boolean}
     * @memberof IBizFormDRPanel
     */
    public refViewDirty: boolean = false;

    /**
     * 关系页视图类型
     *
     * @type {String}
     * @memberof IBizFormDRPanel
     */
    public refViewType: String = '';

    /**
     * 视图参数
     *
     * @private
     * @type {*}
     * @memberof IBizFormDRPanel
     */
    private viewParamJO: any = {};

    /**
     * 是否刷新关系数据
     *
     * @type {boolean}
     * @memberof IBizFormDRPanel
     */
    public isRelationalData: boolean = true;

    /**
     * Creates an instance of IBizFormDRPanel.
     * 创建 IBizFormDRPanel 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormDRPanel
     */
    constructor(opts: any = {}) {
        super(opts);
        if (opts.dritem.parentmode) {
            Object.assign(this.srfParentMode, opts.dritem.parentmode);
        }
        if (opts.dritem.paramjo) {
            Object.assign(this.viewParamJO, opts.dritem.paramjo);
        }
        if (opts.dritem.refviewtype) {
            this.refViewType = opts.dritem.refviewtype;
        }
        if (opts.refreshitems && !Object.is(opts.refreshitems, '')) {
            this.refreshItems = opts.refreshitems;
        }
        Object.assign(this.srfParentMode, this.viewParamJO);
        this.initDRPanel();
    }

    /**
     * 初始化关系面板
     *
     * @private
     * @memberof IBizFormDRPanel
     */
    private initDRPanel(): void {
        if (this.form) {
            const form: any = this.form;

            form.on(IBizForm.FORMFIELDCHANGED).subscribe((data) => {
                this.srfReferData = {};
                const srfReferData = form.getActiveData();
                Object.assign(this.srfReferData, { srfreferdata: JSON.stringify(srfReferData) });
            });
            form.on(IBizForm.FORMLOADED).subscribe((data) => {
                this.refreshDRUIPart();
            });
            // 表单保存之前
            form.on(IBizEditForm.FORMBEFORESAVE).subscribe((data) => {
                return this.saveDRData(data);
            });
            // 表单保存完成
            form.on(IBizForm.FORMSAVED).subscribe((data) => {
                this.refreshDRUIPart();
            });
            // 表单项更新
            form.on(IBizForm.UPDATEFORMITEMS).subscribe((data: any) => {
                if (!data.data) {
                    return;
                }
                let refreshRefview: boolean = false;
                Object.keys(data.data).some((name: string) => {
                    if (this.isRefreshItem(name)) {
                        refreshRefview = true;
                        return true;
                    }
                });
                if (refreshRefview) {
                    this.refreshDRUIPart();
                }
            });

            const items: Array<any> = this.refreshItems.split(';');

            if (items.length > 0) {
                items.forEach(item => {
                    this.hookItems[item.toLowerCase()] = true;
                });
                form.on(IBizForm.FORMFIELDCHANGED).subscribe((data) => {
                    if (form.$ignoreformfieldchange) {
                        return;
                    }

                    let fieldname = '';
                    if (data != null) {
                        fieldname = data.name;
                    } else {
                        return;
                    }
                    if (this.isRefreshItem(fieldname)) {
                        this.refreshDRUIPart();
                    }
                });
            }
        }
    }

    /**
     * 保存关系数据，继续父数据保存，返回true,否则返回false
     * 
     * @returns {boolean} 
     * @memberof IBizDRPanelComponent
     */
    public saveDRData(arg: any = {}): void {
        if (Object.is(this.refViewType, 'DEMEDITVIEW9') && this.refViewDirty) {
            this.saveRefView++;
            arg.srfcancel = true;
            return;
        }
        arg.srfcancel = false;
    }

    /**
     * 是否是刷新数据项
     * 
     * @param {string} name 
     * @returns {boolean} 
     * @memberof IBizDRPanelComponent
     */
    public isRefreshItem(name: string): boolean {
        if (!name || name === '') {
            return false;
        }

        if (this.hookItems[name.toLowerCase()]) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 刷新关系数据
     * 
     * @returns {void} 
     * @memberof IBizDRPanelComponent
     */
    public refreshDRUIPart(): void {

        if (Object.is(this.srfParentMode.srfparenttype, 'CUSTOM')) {
            this.isRelationalData = false;
        }

        const form = this.form;
        const _field = form.findField('srfkey');
        if (!_field) {
            return;
        }
        const _value = _field.getValue();
        if (this.isRelationalData) {
            if (!_value || _value == null || Object.is(_value, '')) {
                this.blockUIStart();
                return;
            } else {
                this.blockUIStop();
            }
        }

        const srfReferData = form.getActiveData();

        this.srfParentData = {};
        this.srfReferData = {};
        Object.assign(this.srfParentData, { srfparentkey: _value });
        Object.assign(this.srfReferData, { srfreferdata: JSON.stringify(srfReferData) });
        setTimeout(() => {
            this.refreshRefView += 1;
        }, 10);
    }

    /**
     * 开启遮罩
     * 
     * @private
     * @memberof IBizDRPanelComponent
     */
    private blockUIStart(): void {
        this.blockUI = true;
    }

    /**
     * 关闭遮罩
     * 
     * @private
     * @memberof IBizDRPanelComponent
     */
    private blockUIStop(): void {
        this.blockUI = false;
    }

    /**
     * 关系页数据保存完成
     *
     * @memberof IBizFormDRPanel
     */
    public onDrDataSaved(): void {
        this.refViewDirty = false;
        if (this.getForm()) {
            this.getForm().save2();
        }
    }

    /**
     * 关系页发生值变更
     *
     * @memberof IBizFormDRPanel
     */
    public onDrDataChange(flag: boolean): void {
        this.refViewDirty = flag;
    }
}

