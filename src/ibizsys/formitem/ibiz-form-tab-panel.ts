/**
 * 表单分页面板
 *
 * @class IBizFormTabPanel
 * @extends {IBizFormItem}
 */
class IBizFormTabPanel extends IBizFormItem {

    /**
     * Creates an instance of IBizFormTabPanel.
     * 创建 IBizFormTabPanel 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormTabPanel
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
