
/**
 *  表单IFrame部件
 *
 * @class IBizFormIFrame
 * @extends {IBizFormItem}
 */
class IBizFormIFrame extends IBizFormItem {

    /**
     * Creates an instance of IBizFormIFrame.
     * 创建 IBizFormIFrame 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizFormIFrame
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
