/**
 * 导航分页
 *
 * @class IBizExpTab
 * @extends {IBizTab}
 */
class IBizExpTab extends IBizTab {

    /**
     * Creates an instance of IBizExpTab.
     * 创建 IBizExpTab 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizExpTab
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
