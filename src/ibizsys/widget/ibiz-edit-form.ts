/**
 * 编辑表单
 *
 * @class IBizEditForm
 * @extends {IBizForm}
 */
class IBizEditForm extends IBizForm {

    /**
     * Creates an instance of IBizEditForm.
     * 创建 IBizEditForm 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizEditForm
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据保存
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    public save2(opt: any = {}): void {

        if (!this.isAllowSave()) {
            this.iBizNotification.error('错误', '表单项值异常');
            return;
        }

        // tslint:disable-next-line:prefer-const
        let arg: any = {};
        if (opt) {
            Object.assign(arg, opt);
        }
        const data = this.getValues();
        Object.assign(arg, data);

        if (Object.is(data.srfuf, '1')) {
            Object.assign(arg, { srfaction: 'update', srfctrlid: this.getName() });
        } else {
            Object.assign(arg, { srfaction: 'create', srfctrlid: this.getName() });
        }

        arg.srfcancel = false;
        this.fire(IBizEditForm.FORMBEFORESAVE, arg);
        if (arg.srfcancel) {
            return;
        }
        delete arg.srfcancel;

        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;

        this.submit(arg).subscribe((action) => {
            this.resetFormError();
            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            this.setDataAccAction(action.dataaccaction);
            this.fillForm(action.data);
            this.formDirty = false;
            // 判断是否有提示
            if (action.info && !Object.is(action.info, '')) {
                // IBiz.alert('', action.info, 1);
                this.iBizNotification.info('', action.info);
            }
            // this.fireEvent('formsaved', this, action);
            this.fire(IBizForm.FORMSAVED, action);
            this.ignoreUFI = false;
            this.ignoreformfieldchange = false;
            // this.fireEvent('formfieldchanged', null);
            this.fire(IBizForm.FORMFIELDCHANGED, null);
            this.onSaved();
        }, (action) => {
            if (action.error) {
                this.setFormError(action.error, 'BACKEND');
            }

            this.ignoreUFI = false;
            this.ignoreformfieldchange = false;

            // this.fireEvent(IBizEditForm.FORMSAVEERROR, this);
            this.fire(IBizEditForm.FORMSAVEERROR, this);

            action.failureType = 'SERVER_INVALID';
            if (action.ret === 10) {
                this.iBizNotification.confirm('保存错误信息', '保存数据发生错误, ' + this.getActionErrorInfo(action) + ', 是否要重新加载数据？').subscribe(result => {
                    if (result && Object.is(result, 'OK')) {
                        this.reload();
                    }
                });
            } else if (action.ret === 19) {
                const calcel: any = {};
                const ok: any = {};
                if (action.confirmoptions && Array.isArray(action.confirmoptions)) {
                    action.confirmoptions.forEach((confirmoption: any) => {
                        if (Object.is(confirmoption.confirmvalue, 'ok')) {
                            Object.assign(ok, { [action.confirmkey]: confirmoption.confirmvalue });
                        }
                        if (Object.is(confirmoption.confirmvalue, 'calcel')) {
                            Object.assign(calcel, { [action.confirmkey]: confirmoption.confirmvalue });
                        }
                    });
                }

                this.iBizNotification.confirm(action.confirmtitle, action.confirmmsg).subscribe(result => {
                    if (Object.is(result, 'OK')) {
                        this.save2(ok);
                    }
                });
            } else {
                this.iBizNotification.error('保存错误信息', '保存数据发生错误,' + this.getActionErrorInfo(action));
            }
        });
    }

    /**
     * 
     * 
     * @memberof IBizEditForm
     */
    public onSaved(): void {

    }

    /**
     * 表单数据刷新
     * 
     * @memberof IBizEditForm
     */
    public reload(): void {

        let field = this.findField('srfkey');
        // tslint:disable-next-line:prefer-const
        let loadarg: any = {};
        if (field) {
            loadarg.srfkey = field.getValue();
            if (loadarg.srfkey.indexOf('SRFTEMPKEY:') === 0) {
                field = this.findField('srforikey');
                if (field) {
                    loadarg.srfkey = field.getValue();
                }
            }
            const viewController = this.getViewController();
            if (viewController) {
                const viewParmams: any = viewController.getViewParam();
                if (!Object.is(loadarg.srfkey, viewParmams.srfkey)) {
                    loadarg.srfkey = viewParmams.srfkey;
                }
            }
        }
        this.autoLoad(loadarg);
    }

    /**
     * 删除
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    public remove(opt: any = {}): void {
        // tslint:disable-next-line:prefer-const
        let arg: any = {};
        if (opt) {
            Object.assign(arg, opt);
        }

        if (!arg.srfkey) {
            const field = this.findField('srfkey');
            if (field) {
                arg.srfkey = field.getValue();
            }
        }

        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert(IGM('IBIZEDITFORM.REMOVEFAILED.TITLE', '删除错误信息'), IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.iBizNotification.error('删除错误信息', '当前表单未加载数据！');
            return;
        }
        Object.assign(arg, { srfaction: 'remove', srfctrlid: this.getName() });
        this.ignoreUFI = true;

        this.load(arg).subscribe((action) => {
            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            // this.fireEvent(IBizForm.FORMREMOVED);
            this.fire(IBizForm.FORMREMOVED, this);
        }, (action) => {
            action.failureType = 'SERVER_INVALID';
            this.iBizNotification.error('删除错误信息', '删除数据发生错误, ' + this.getActionErrorInfo(action));
            this.ignoreUFI = false;
        });
    }

    /**
     * 工作流启动
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    public wfstart(opt: any = {}): void {
        if (!this.isAllowSave()) {
            this.iBizNotification.error('错误', '表单项值异常');
            return;
        }

        // tslint:disable-next-line:prefer-const
        let arg: any = {};
        if (opt) {
            Object.assign(arg, opt);
        }

        if (!arg.srfkey) {
            let field = this.findField('srfkey');
            if (field) {
                arg.srfkey = field.getValue();
            }
            field = this.findField('srforikey');
            if (field) {
                // tslint:disable-next-line:prefer-const
                let v = field.getValue();
                if (v && !Object.is(v, '')) {
                    arg.srfkey = v;
                }
            }
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert(IGM('IBIZEDITFORM.WFSTARTFAILED.TITLE', '启动流程错误信息'), IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.iBizNotification.error('启动流程错误信息', '当前表单未加载数据！');
            return;
        }

        Object.assign(arg, { srfaction: 'wfstart', srfctrlid: this.getName() });
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;

        this.iBizHttp.post(this.getBackendUrl(), arg).subscribe((action) => {
            if (action.ret !== 0) {
                action.failureType = 'SERVER_INVALID';
                this.iBizNotification.error('启动流程错误信息', '启动流程发生错误,' + this.getActionErrorInfo(action));
                this.ignoreUFI = false;
                this.ignoreformfieldchange = false;
                return;
            }

            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            this.setDataAccAction(action.dataaccaction);
            this.fillForm(action.data);
            this.formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED);
            // this.fireEvent(IBizForm.FORMWFSTARTED);
            this.fire(IBizForm.FORMWFSTARTED, this);
            this.ignoreUFI = false;
            this.ignoreformfieldchange = false;
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            this.fire(IBizForm.FORMFIELDCHANGED, null);
        }, (action) => {
            if (action.error) {
                this.setFormError(action.error, 'BACKEND');
            }
            action.failureType = 'SERVER_INVALID';
            this.iBizNotification.error('启动流程错误信息', '启动流程发生错误,' + this.getActionErrorInfo(action));
            this.ignoreUFI = false;
            this.ignoreformfieldchange = false;
        });
    }

    /**
     * 工作流提交
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    public wfsubmit(opt: any = {}): void {
        if (!this.isAllowSave()) {
            this.iBizNotification.error('错误', '表单项值异常');
            return;
        }

        // tslint:disable-next-line:prefer-const
        let arg: any = {};
        if (opt) {
            Object.assign(arg, opt);
        }

        const data = this.getValues();
        Object.assign(arg, data);
        Object.assign(arg, { srfaction: 'wfsubmit', srfctrlid: this.getName() });

        //        if (!arg.srfkey) {
        //            var field = this.findField('srfkey');
        //            if (field) {
        //                arg.srfkey = field.getValue();
        //            }
        //        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert(IGM('IBIZEDITFORM.WFSUBMITFAILED.TITLE', '提交流程错误信息'), IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.iBizNotification.error('提交流程错误信息', '当前表单未加载数据！');
            return;
        }

        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;

        this.load(arg).subscribe((action) => {
            this.setFieldAsyncConfig(action.config);
            this.setFieldJurisdiction(action.data);
            this.setFieldState(action.state);
            this.setDataAccAction(action.dataaccaction);
            this.fillForm(action.data);
            this.formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED);
            // this.fireEvent(IBizForm.FORMWFSUBMITTED);
            this.fire(IBizForm.FORMWFSUBMITTED, this);
            this.ignoreUFI = false;
            this.ignoreformfieldchange = false;
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            this.fire(IBizForm.FORMFIELDCHANGED, null);
        }, (action) => {
            if (action.error) {
                this.setFormError(action.error, 'BACKEND');
            }
            action.failureType = 'SERVER_INVALID';
            this.iBizNotification.error('提交流程错误信息', '工作流提交发生错误,' + this.getActionErrorInfo(action));
            this.ignoreUFI = false;
            this.ignoreformfieldchange = false;
        });
    }

    /**
     * 界面行为
     * 
     * @param {*} [arg={}] 
     * @memberof IBizEditForm
     */
    public doUIAction(arg: any = {}): void {
        // tslint:disable-next-line:prefer-const
        let opt: any = {};
        if (arg) {
            Object.assign(opt, arg);
        }
        Object.assign(opt, { srfaction: 'uiaction', srfctrlid: this.getName() });

        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe((data) => {
            if (data.ret === 0) {
                // IBiz.processResultBefore(data);
                this.fire(IBizEditForm.UIACTIONFINISHED, data);
                if (data.reloadData) {
                    this.reload();
                }
                if (data.info && !Object.is(data.info, '')) {
                    this.iBizNotification.info('', data.info);
                }
                // IBiz.processResult(data);
            } else {
                this.iBizNotification.error('界面操作错误信息', '操作失败,' + data.errorMessage);
            }
        }, (error) => {
            this.iBizNotification.error('界面操作错误信息', '操作失败,' + error.info);
        });

    }

    /**
     * 表单类型
     *
     * @returns {string}
     * @memberof IBizEditForm
     */
    public getFormType(): string {
        return 'EDITFORM';
    }

    /**
     * 是否允许保存
     *
     * @returns {boolean}
     * @memberof IBizEditForm
     */
    public isAllowSave(): boolean {
        let allow = true;
        const fieldNames = Object.keys(this.fields);
        fieldNames.some((name: string) => {
            const field = this.findField(name);
            if (field && field.hasActiveError() && Object.is(field.getErrorType(), 'FRONTEND')) {
                allow = false;
                return true;
            }
        });
        return allow;
    }

    /**
     * 表单权限发生变化
     */
    public static UIACTIONFINISHED = 'UIACTIONFINISHED';

    /**
     * 表单保存之前触发
     */
    public static FORMBEFORESAVE = 'FORMBEFORESAVE';


    /**
     * 表单保存错误触发
     */
    public static FORMSAVEERROR = 'FORMSAVEERROR';
}
