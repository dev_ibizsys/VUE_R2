/**
 * 图表
 *
 * @class IBizChart
 * @extends {IBizControl}
 */
class IBizChart extends IBizControl {

    /**
     * 图表数据
     * 
     * @type {Array<any>}
     * @memberof IBizChart
     */
    public data: Array<any> = [];

    /**
     * Creates an instance of IBizChart.
     * 创建 IBizChart 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizChart
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 加载报表数据
     * 
     * @memberof IBizChart
     */
    public load(): void {
        this.buildChart();
    }

    /**
     * 处理图表内容
     * 
     * @memberof IBizChart
     */
    private buildChart(): void {
        let params: any = {};
        this.fire(IBizChart.BEFORELOAD, params);
        Object.assign(params, { srfrender: 'ECHARTS3', srfaction: 'FETCH', srfctrlid: this.getName() });
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe((result) => {
            if (result.ret === 0) {
                this.data = result.data;
                const data = this.getChartConfig();
                let target = {};
                Object.assign(target, data, this.data);
                this.fire(IBizChart.LOADED, target);
            } else {
                this.iBizNotification.error('系统异常', result.info);
                
            }
        }, (error) => {
            this.iBizNotification.error('系统异常', error.info);
        });
    }

    /**
     * 获取图表基础配置数据
     */
    public getChartConfig(): any {
        const opts: any = {};
        return opts;
    }

    /**
     * 加载之前
     *
     * @static
     * @memberof IBizChart
     */
    public static BEFORELOAD = 'BEFORELOAD';

    /**
     * 加载完成
     *
     * @static
     * @memberof IBizChart
     */
    public static LOADED = 'LOADED';

    /**
     * 点击
     *
     * @static
     * @memberof IBizChart
     */
    public static DBLCLICK = 'DBLCLICK';
}

