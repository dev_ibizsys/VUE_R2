/**
 * 分页
 *
 * @class IBizTab
 * @extends {IBizControl}
 */
class IBizTab extends IBizControl {

    /**
     * 激活分页
     *
     * @type {*}
     * @memberof IBizTab
     */
    public activeTab: any = {};

    /**
     * 分页部件对象
     * 
     * @type {*}
     * @memberof IBizTab
     */
    public tabs: any = {};

    /**
     * Creates an instance of IBizTab.
     * 创建 IBizTab 实例
     * @param {*} [opts={}] 
     * @memberof IBizTab
     */
    constructor(opts: any = {}) {
        super(opts);
        this.regTabs();
    }

    /**
     * 注册所有分页部件对象
     * 
     * @memberof IBizTab
     */
    public regTabs(): void {

    }

    /**
     * 注册分页部件对象
     * 
     * @param {*} [tab={}] 
     * @memberof IBizTab
     */
    public regTab(tab: any = {}): void {
        if (Object.keys(tab).length > 0 && tab.name) {
            let viewController: IBizEditView3Controller = this.getViewController();
            let view: any = viewController.getDRItemView({ viewid: tab.name });
            if (view) {
                Object.assign(tab, view);
            }
            this.tabs[tab.name] = tab;
        }
    }

    /**
     * 获取分页部件对象
     *
     * @param {string} name
     * @param {string} [routepath]
     * @returns {*}
     * @memberof IBizTab
     */
    public getTab(name: string, routepath?: string): any {
        let tab_arr: Array<any> = Object.values(this.tabs);
        let tab: any = {};
        tab_arr.some((_tab: any) => {
            if (Object.is(_tab.name, name) || (_tab.routepath && Object.is(_tab.routepath, routepath))) {
                Object.assign(tab, _tab);
                return true;
            }
        });
        return tab;
    }

    /**
     * 设置激活分页
     *
     * @param {*} [tab={}]
     * @memberof IBizTab
     */
    public setActiveTab(tab: any = {}): void {
        if (!Object.is(tab.name, this.activeTab.name)) {
            this.activeTab = {};
            Object.assign(this.activeTab, tab);
        }
    }

    /**
     * 设置分页状态
     *
     * @param {boolean} state
     * @memberof IBizTab
     */
    public setTabState(state: boolean): void {

    }
}
