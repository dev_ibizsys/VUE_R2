/**
 * 多视图面板
 *
 * @class IBizViewPanel
 * @extends {IBizTab}
 */
class IBizViewPanel extends IBizTab {

    /**
     * Creates an instance of IBizViewPanel.
     * 创建 IBizViewPanel 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizViewPanel
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
