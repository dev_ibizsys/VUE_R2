/**
 * 面板
 *
 * @class IBizPanel
 * @extends {IBizControl}
 */
class IBizPanel extends IBizControl {

    /**
     * 面板模型对象
     *
     * @type {*}
     * @memberof IBizPanel
     */
    public model: any = {};

    /**
     * 面板项集合
     *
     * @type {*}
     * @memberof IBizPanel
     */
    public items: any = {};

    /**
     *Creates an instance of IBizPanel.
     *创建IBizPanel的一个实例
     * @param {*} [opt={}]
     * @memberof IBizPanel
     */
    constructor(opt: any = {}) {
        super(opt);
        this.regItems();
        this.regModelItems();
        this.listenModel();
        this.regPanelLogic();
    }

    /**
     * 注册面板项
     *
     * @memberof IBizPanel
     */
    public regItems() {

    }

    /**
     * 注册面板模型项
     *
     * @memberof IBizPanel
     */
    public regModelItems() {

    }

    
    /**
     * 注册面板逻辑
     *
     * @memberof IBizPanel
     */
    public regPanelLogic() {

    }

    /**
     * 注册面板项属性
     *
     * @param {*} item
     * @memberof IBizPanel
     */
    public regItem(item: any): void {
        if(!this.items) {
            this.items = {};
        }
        if(item) {
            this.items[item.getName()] = item;
        }
    }

    /**
     * 获取面板项属性
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizPanel
     */
    public getItem(name): any {
        return this.items[name];
    }

    /**
     * 注册面板模型属性项
     *
     * @param {string} name
     * @param {*} val
     * @param {*} item
     * @memberof IBizPanel
     */
    public regModelItem(name: string, val: any, item: any): void {
        if(!this.model) {
            this.model = {};
        }
        if(name) {
            this.model[name] = item;
        }
    }

    /**
     * 获取面板模型属性项
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizPanel
     */
    public getModelItem(name): any {
        return this.model[name];
    }

    /**
     * 获取面板模型
     *
     * @returns {*}
     * @memberof IBizPanel
     */
    public getModel(): any {
        return this.model;
    }

    public listenModel(): void {
        let _this = this;
        IBizListenProperty.regListenObject(_this.model).subscribe((args) => {
            _this.onModelChanged(args);
        });
    }

    /**
     * 模型变化
     *
     * @memberof IBizPanel
     */
    onModelChanged(args): void {
        this.fire(IBizPanel.MODELCHANGED, args);
    }

    /**
     * 模型变化事件
     *
     * @static
     * @memberof IBizPanel
     */
    public static MODELCHANGED = 'MODELCHANGED';
 }