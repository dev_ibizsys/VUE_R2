/**
 * 数据关系栏
 *
 * @class IBizDRBar
 * @extends {IBizControl}
 */
class IBizDRBar extends IBizControl {

    /**
     * 展开数据项
     *
     * @type {Array<string>}
     * @memberof IBizDRBar
     */
    public expandItems: Array<string> = [];

    /**
     * 关系部件是否收缩，默认展开
     * 
     * @type {boolean}
     * @memberof IBizDRBar
     */
    public isCollapsed: boolean = true;

    /**
     * 关系数据项
     * 
     * @type {Array<any>}
     * @memberof IBizDRBar
     */
    public items: Array<any> = [];

    /**
     * 选中项
     * 
     * @type {*}
     * @memberof IBizDRBar
     */
    public selectItem: any = {};

    /**
     * 父数据对象
     *
     * @type {*}
     * @memberof IBizDRBar
     */
    public srfParentData: any = {};

    /**
     * Creates an instance of IBizDRBar.
     * 创建IBizDRBar实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizDRBar
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 加载关系数据
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizDRBar
     */
    public load(arg: any = {}): void {
        let param: any = { srfaction: 'fetch', srfctrlid: this.getName() };
        Object.assign(param, arg);

        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(result => {
            if (result.ret === 0) {
                this.formarItems(result.items);
                this.itemSelect(result.items, {});
                this.items = [...result.items];
                this.fire(IBizDRBar.DRBARLOADED, this.items);
            }
        }, error => {
            console.log(error);
        });
    }

    /**
     * 选中项
     *
     * @private
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {boolean}
     * @memberof IBizDRBar
     */
    private itemSelect(items: Array<any>, data: any = {}): boolean {
        let bNeedReSelect: boolean = false;
        items.forEach(item => {
            let counterid = item.counterid;
            let countermode = item.countermode;

            item.show = true;
            let count = data[counterid];
            if (!count) {
                count = 0;
            }
            if (count === 0 && countermode && countermode === 1) {
                item.show = false;
                // 判断是否选中列，如果是则重置选中
                if (this.selectItem && Object.is(this.selectItem.id, item.id)) {
                    bNeedReSelect = true;
                }
            }

            item.counterdata = count;
            if (item.items) {
                bNeedReSelect = this.itemSelect(item.items, data);
            }
        });
        return bNeedReSelect;
    }

    /**
     * 格式化数据项
     *
     * @private
     * @param {*} _items
     * @returns {*}
     * @memberof IBizDRBar
     */
    private formarItems(_items: any): any {
        _items.forEach(item => {
            item.disabled = true;
            item.class = 'drbadge';
            if (item.items) {
                this.expandItems.push(item.id);
                this.formarItems(item.items);
            }
        });
    }


    /**
     * 菜单节点选中处理
     * 
     * @param {*} [item={}] 
     * @returns {void} 
     * @memberof IBizDRBar
     */
    public expandedAndSelectSubMenu(item: any = {}): void {
        if (Object.is(item.id, 'form')) {
            this.setSelectItem(item);
            this.fire(IBizDRBar.DRBARSELECTCHANGE, item);
            return;
        }

        const viewController: any = this.getViewController();
        let refitem: any = viewController.getDRItemView(item.dritem);
        if (!refitem || Object.keys(refitem).length == 0) {
            return;
        }

        let view: any = { viewparam: {} };
        if (item.dritem.viewparam) {
            Object.assign(view.viewparam, item.dritem.viewparam)
        }
        Object.assign(view.viewparam, this.srfParentData);
        Object.assign(view, refitem, item);
        
        this.fire(IBizDRBar.DRBARSELECTCHANGE, view);
    }

    /**
     * 菜单项选中事件
     * 
     * @param {*} item 
     * @returns {void} 
     * @memberof IBizTreeExpBarService
     */
    public selection(item: any): void {
        if (item.items && item.items.length > 0) {
            return;
        }

        if (Object.is(item.id, 'form')) {
            this.fire(IBizDRBar.DRBARSELECTCHANGE, item);
            return;
        }

        const viewController: any = this.getViewController();
        let refitem: any = viewController.getDRItemView(item.dritem);
        if (!refitem && Object.keys(refitem).length == 0) {
            return;
        }

        let view: any = { viewparam: {} };
        if (item.dritem.viewparam) {
            Object.assign(view.viewparam, item.dritem.viewparam)
        }
        Object.assign(view.viewparam, this.srfParentData);
        Object.assign(view, refitem, item);
        
        this.fire(IBizDRBar.DRBARSELECTCHANGE, view);
    }

    /**
     * 重新加载关系数据
     * 
     * @memberof IBizDRBar
     */
    public reLoad(arg: any = {}): void {
        this.load(arg);
    }

    /**
     * 获取所有关系数据项
     * 
     * @returns {Array<any>} 
     * @memberof IBizDRBar
     */
    public getItems(): Array<any> {
        return this.items;
    }

    /**
     * 获取关系数据项
     *
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {*}
     * @memberof IBizDRBar
     */
    public getItem(items: Array<any>, data: any = {}): any {
        let _item: any = {};
        let viewController: any = this.getViewController();
        items.some((item: any) => {
            let drItem: any = viewController.getDRItemView({ viewid: item.id });
            if (drItem && Object.is(drItem.routepath, data.routepath)) {
                Object.assign(_item, drItem, item);
                return true;
            }
            if (item.items && item.items.length > 0) {
                let subItem = this.getItem(item.items, data);
                if (subItem && Object.keys(subItem).length > 0) {
                    Object.assign(_item, subItem);
                    return;
                }
            }
        });
        return _item;
    }

    /**
     * 设置关系数据选中项
     *
     * @param {*} [_item={}]
     * @memberof IBizDRBar
     */
    public setSelectItem(_item: any = {}): void {
        if (!Object.is(this.selectItem.id, _item.id)) {
            this.selectItem = {};
            Object.assign(this.selectItem, _item);
        }
    }

    /**
     * 设置关系数据项状态
     *
     * @param {Array<any>} items
     * @param {boolean} state
     * @memberof IBizDRBar
     */
    public setDRBarItemState(items: Array<any>, state: boolean) {
        items.forEach((item: any) => {
            item.disabled = Object.is(item.id, 'form') ? false : state;
            if (item.items && item.items.length > 0) {
                this.setDRBarItemState(item.items, state);
            }
        });
    }

    /**
     * 设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizDRBar
     */
    public setParentData(data: any = {}): void {
        this.srfParentData = {};
        Object.assign(this.srfParentData, data);
    }

    /**
     * 数据关系项加载完成
     *
     * @static
     * @memberof IBizDRBar
     */
    public static DRBARLOADED = 'DRBARLOADED';

    /**
     * 数据关系项选中
     *
     * @static
     * @memberof IBizDRBar
     */
    public static DRBARSELECTCHANGE = 'DRBARSELECTCHANGE';

}