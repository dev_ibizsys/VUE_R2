/**
 * 自定义部件
 *
 * @class IBizCustom
 * @extends {IBizControl}
 */
class IBizCustom extends IBizControl {

    /**
     * Creates an instance of IBizCustom.
     * 创建 IBizCustom 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizCustom
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

