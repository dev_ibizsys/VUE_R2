/**
 * 数据视图部件控制器
 *
 * @class IBizDataView
 * @extends {IBizMDControl}
 */
class IBizDataView extends IBizMDControl {

    /**
     * 双击选中项
     * 
     * @private
     * @type {Array<any>}
     * @memberof IBizDataView
     */
    private dblselection: Array<any> = [];

    /**
     * 选中项
     * 
     * @type {*}
     * @memberof IBizDataView
     */
    public selectItem: any = {};

    /**
     * Creates an instance of IBizDataView.
     * 创建 IBizDataView 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizDataView
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据加载
     * 
     * @param {*} [arg={}] 
     * @memberof IBizDataView
     */
    public load(arg: any = {}): void {
        let opt: any = {};
        Object.assign(opt, arg);

        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });

        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);

        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe((response: any) => {
            this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    this.iBizNotification.error('', response.errorMessage);
                }

                return;
            }
            this.items = response.items;
            this.fire(IBizMDControl.LOADED, response.items);
        }, (error: any) => {
            this.endLoading();
            console.log(error.info);
        }
        );
    }

    /**
     * 选择变化
     * 
     * @param {*} [item={}] 
     * @memberof IBizDataView
     */
    public selectChange(data: any = {}): void {

        const arr: Array<any> = this.selection.filter((item) => Object.is(data.srfkey, item.srfkey));
        this.selection = [];
        this.selectItem = {};
        if (arr.length !== 1) {
            this.selection.push(data);
            Object.assign(this.selectItem, data);
        }

        this.fire(IBizDataView.SELECTIONCHANGE, this.selection);
    }


    /**
     * 双击选择变化
     * 
     * @param {*} [data={}] 
     * @memberof IBizDataView
     */
    public DBClickSelectChange(data: any = {}): void {

        const arr: Array<any> = this.dblselection.filter((item) => Object.is(data.srfkey, item.srfkey));
        this.dblselection = [];
        this.selectItem = {};
        if (arr.length !== 1) {
            this.dblselection.push(data);

            Object.assign(this.selectItem, data);
        }

        this.fire(IBizDataView.DATAACTIVATED, this.dblselection);
    }

    /**
     * 数据项选中
     *
     * @static
     * @memberof IBizDataView
     */
    public static SELECTIONCHANGE = 'SELECTIONCHANGE';

    /**
     * 数据项双击激活
     *
     * @static
     * @memberof IBizDataView
     */
    public static DATAACTIVATED = 'DATAACTIVATED';
}

