/**
 * 向导面板
 *
 * @class IBizWizardPanel
 * @extends {IBizControl}
 */
class IBizWizardPanel extends IBizControl {

    /**
     * 向导表单集合
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public forms: any = {};

    /**
     * 向导表单名称集合
     *
     * @type {Array<string>}
     * @memberof IBizWizardPanel
     */
    public formNames: Array<String> = [];

    /**
     * 向导表单行为集合
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public acitons: any = {};

    /**
     * 当前激活表单名称
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public curForm: any = {};

    /**
     * 当前激活表单名称
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public curFormName: String = null;

    /**
     * 向导面板参数
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public params: any = {};

    /**
     * 执行过的表单集合
     *
     * @type {Array<String>}
     * @memberof IBizWizardPanel
     */
    public prevForms: Array<String> = [];

    /**
     * 步骤集合
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public steps: any = [];

    /**
     * 当前步骤
     *
     * @type {String}
     * @memberof IBizWizardPanel
     */
    public curStep: String = '';

    /**
     * 表单步骤集合
     *
     * @type {*}
     * @memberof IBizWizardPanel
     */
    public formSteps: any = {};

    /**
     * 下一步后续操作行为
     *
     * @type {String}
     * @memberof IBizWizardPanel
     */
    public afterformsaveaction: String = '';

    /**
     * Creates an instance of IBizWizardPanel.
     * 创建 IBizWizardPanel 实例
     * 
     * @param {*} [otps={}]
     * @memberof IBizWizardPanel
     */
    constructor(opt: any = {}) {
        super(opt);
        this.regWizardForms();
        this.regFormActions();
        this.regSteps();
        this.regFormSteps();
        this.setDefFrom();
    }

    /**
     * 注册向导表单
     *
     * @memberof IBizWizardPanel
     */
    public regWizardForms(): void {

    }

    /**
     * 注册表单面板行为
     *
     * @memberof IBizWizardPanel
     */
    public regFormActions(): void {
        
    }

        /**
     * 注册向导表单项
     *
     * @param {*} form
     * @memberof IBizWizardPanel
     */
    public regForm(form): void {
        if(form) {
            // 表单保存之前
            form.on(IBizEditForm.FORMBEFORESAVE).subscribe((data) => {
                // this.onFormBeforeSaved(data);
            });
            // 表单保存完成
            form.on(IBizForm.FORMSAVED).subscribe((data) => {
                this.onFormSaved(data);
            });
            // 表单加载完成
            form.on(IBizForm.FORMLOADED).subscribe((data) => {
                this.fire(IBizWizardPanel.WIZARDFORMLOADED, data);
            });
            // 表单属性值变化
            form.on(IBizForm.FORMFIELDCHANGED).subscribe((data) => {
                if(data) {
                    Object.assign(data, {formName: form.getName()});
                    this.fire(IBizWizardPanel.WIZARDFORMFIELDCHANGED, data);
                } else {
                    this.fire(IBizWizardPanel.WIZARDFORMFIELDCHANGED, {formName: form.getName(), name: ''});
                }
            });
            this.formNames.push(form.getName());
            this.forms[form.getName()] = form;
        }
    }

    /**
     * 注册表单行为项
     *
     * @param {*} name
     * @param {*} action
     * @returns {void}
     * @memberof IBizWizardPanel
     */
    public regAction(name, action): void {
        if(name) {
            this.acitons[name] = action;
        }
    }

    /**
     * 注册步骤
     *
     * @memberof IBizWizardPanel
     */
    public regSteps(): void {

    }

    /**
     * 注册表单步骤集合
     *
     * @memberof IBizWizardPanel
     */
    public regFormSteps(): void {

    }

    /**
     * 注册表单步骤项
     *
     * @param {*} name
     * @param {*} item
     * @memberof IBizWizardPanel
     */
    public regFormStep(name, item): void {
        if(name){
            this.formSteps[name] = item;
        }
    }

    /**
     * 设置默认表单
     *
     * @memberof IBizWizardPanel
     */
    public setDefFrom(): void {

    }

    /**
     * 获取表单
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizWizardPanel
     */
    public getForm(name): any {
        return this.forms[name];
    }

    /**
     * 获取表单对应步骤
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizWizardPanel
     */
    public getStep(name): any {
        if(name) {
            return this.formSteps[name];
        }
    }

    /**
     * 获取步骤对应下标
     *
     * @param {*} tag
     * @returns {Number}
     * @memberof IBizWizardPanel
     */
    public getStepIndex(tag): Number {
        if(tag) {
            let index = this.steps.findIndex((step) => Object.is(step.tag, tag));
            if(index >= 0) {
                return index;
            } else if(Object.is(tag, 'finish')) {
                return this.steps.length;
            }
        }
        return 0;
    }

    /**
     * 向导面板加载
     *
     * @param {*} arg
     * @memberof IBizWizardPanel
     */
    public autoLoad(arg: any): void{
        let param: any = { srfaction: 'init', srfctrlid: this.getName() };
        Object.assign(param, arg);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe((data) => {
            if (data.ret === 0) {
                this.params = data.data;
                if(this.curForm) {
                    this.curForm.autoLoad(this.params);
                }
                this.fire(IBizWizardPanel.WIZARDPANEL_INITED, data);
            } else {
                
            }
        }, (data) => {
            console.log(data);
        });
    }

    /**
     * 表单保存完成
     *
     * @param {*} data
     * @memberof IBizWizardPanel
     */
    public onFormSaved(data: any): void {
        let field = this.curForm.findField("srfnextform");
        if(Object.is(this.afterformsaveaction, 'NEXT') && field && !Object.is(field.getValue(), '')) {
            this.changeForm(field.getValue());
        } else if (Object.is(this.afterformsaveaction, 'NEXT') && this.getNext() ) {
            this.changeForm(this.getNext());
        } else {
            this.curFormName = "finish";
            this.curForm = null;
            this.curStep = 'finish';
            this.finish();
        }
        this.fire(IBizWizardPanel.WIZARDFORMSAVED, data);
    }

    /**
     * 执行上一步
     *
     * @memberof IBizWizardPanel
     */
    public goPrev(): void {
        let length = this.prevForms.length;
        if(length > 0) {
            let name = this.prevForms[length - 1];
            this.changeForm(name);
            this.prevForms.splice(length - 1, 1);
        }
    }

    /**
     * 执行下一步
     *
     * @memberof IBizWizardPanel
     */
    public goNext(): void {
        if(this.curForm) {
            this.afterformsaveaction = 'NEXT';
            this.prevForms.push(this.curFormName);
            this.curForm.save2();
        }
    }

    /**
     * 执行完成
     *
     * @memberof IBizWizardPanel
     */
    public goFinish(): void {
        if(this.curForm) {
            this.afterformsaveaction = 'FINISH';
            this.curForm.save2();
        }
    }

    /**
     * 切换表单
     *
     * @param {*} name
     * @memberof IBizWizardPanel
     */
    public changeForm(name): void {
        if(name && this.getForm(name)) {
            this.curFormName = name;
            this.curForm = this.getForm(name);
            this.curStep = this.getStep(name);
            this.curForm.autoLoad(this.params);
            this.fire(IBizWizardPanel.WIZARDFORMCHANGED, this);
        }
    }

    /**
     * 获取下一个表单名称
     *
     * @returns {String}
     * @memberof IBizWizardPanel
     */
    public getNext(): String {
        let index = this.formNames.indexOf(this.curFormName);
        if(index >= 0) {
            if(this.formNames[index + 1]) {
                return this.formNames[index + 1];
            }
        }
        return undefined;
    }

    /**
     * 完成
     *
     * @memberof IBizWizardPanel
     */
    public finish() {
        var param = { srfaction:"finish", srfctrlid: this.getName() };
        Object.assign(param, this.params);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe((data) => {
            if (data.ret === 0) {
                this.fire(IBizWizardPanel.WIZARDPANEL_FINISH, data);
            }
        }, (data) => {
            console.log(data);
        });
    }

    /**
     * 是否显示该行为按钮
     *
     * @param {*} form
     * @param {*} action
     * @returns
     * @memberof IBizWizardPanel
     */
    public isVisible(name, action) {
        if(name) {
            let formActions: Array<String> = this.acitons[name];
            if(formActions) {
                let index = formActions.findIndex((item) => Object.is(item, action));
                if(index >= 0) {
                    return true;
                } 
            }
        }
        return false;
    }

    /**
     * 向导表单项值改变
     */
    public static WIZARDFORMFIELDCHANGED = 'WIZARDFORMFIELDCHANGE';

    /**
     * 向导表单保存完成
     */
    public static WIZARDFORMSAVED = 'WIZARDFORMSAVED';

    /**
     * 向导表单加载完成
     */
    public static WIZARDFORMLOADED = 'WIZARDFORMLOADED';

    /**
     * 向导面板初始化完成
     */
    public static WIZARDPANEL_INITED = 'WIZARDPANEL_INITED';

    /**
     * 向导面板完成
     */
    public static WIZARDPANEL_FINISH = 'WIZARDPANEL_FINISH';

    /**
     * 向导表单切换
     */
    public static WIZARDFORMCHANGED = 'WIZARDFORMCHANGED';
}