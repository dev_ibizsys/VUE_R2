/**
 * 报表
 *
 * @class IBizReportPanel
 * @extends {IBizControl}
 */
class IBizReportPanel extends IBizControl {

    /**
     * 是否显示报表，本地开发不显示报表
     * 
     * @type {boolean}
     * @memberof IBizReportPanel
     */
    public showReport: boolean = false;

    /**
     * 报表视图参数
     * 
     * @type {string}
     * @memberof IBizReportPanel
     */
    public viewurl: string;

    /**
     * 报表ID
     * 
     * @type {string}
     * @memberof IBizReportPanel
     */
    public reportid: string = '';

    /**
     * Creates an instance of IBizReportPanel.
     * 创建 IBizReportPanel 实例 
     * 
     * @param {*} [opts={}] 
     * @memberof IBizReportPanel
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据加载
     * 
     * @memberof IBizReportPanel
     */
    public load(): void {
        this.buildReport();
    }

    /**
     * 处理报表内容
     * 
     * @memberof IBizReportPanel
     */
    private buildReport(): void {

        let params: any = {};
        this.fire(IBizReportPanel.BEFORELOAD, params);
        if (params.srfaction) {
            delete params.srfaction;
        }

        if (Object.is(this.reportid, '')) {
            return;
        }
        Object.assign(params, { srfreportid: this.reportid });

        if (!IBizEnvironment.LocalDeve) {
            if (Object.keys(params).length !== 0) {
                this.showReport = true;
                let _data: Array<any> = [];
                const _paramsArr: Array<any> = Object.keys(params);
                _paramsArr.forEach(key => {
                    _data.push(`{key}={params[key]}`);
                });
                const urlData: string = _data.join('&');
                this.viewurl = `..{IBizEnvironment.PDFReport}?{urlData}`;
            }
        }
    }

    /**
     * 加载之前
     *
     * @static
     * @memberof IBizReportPanel
     */
    public static BEFORELOAD = 'BEFORELOAD';
}