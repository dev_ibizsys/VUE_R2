/**
 * 多编辑视图面板部件服务对象
 *
 * @class IBizMultiEditViewPanel
 * @extends {IBizControl}
 */
class IBizMultiEditViewPanel extends IBizControl {

    /**
     * 编辑数据集合
     *
     * @type {Array<any>}
     * @memberof IBizMultiEditViewPanel
     */
    public editCtrls: Array<any> = [];

    /**
     * 当前激活tab
     *
     * @type {Number}
     * @memberof IBizMultiEditViewPanel
     */
    public activeTab: number = 0;

    /**
     * 待删除主键集合
     *
     * @type {Array<String>}
     * @memberof IBizMultiEditViewPanel
     */
    public removeKeys: Array<String> = [];

    /**
     * 提交数量
     *
     * @memberof IBizMultiEditViewPanel
     */
    public submitCount = 0;

    /**
     * Creates an instance of IBizMultiEditViewPanel.
     * 创建 IBizMultiEditViewPanel 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizMultiEditViewPanel
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 数据记载
     *
     * @param {*} [params={}]
     * @memberof IBizMultiEditViewPanel
     */
    public load(params: any = {}): void {
        let opt: any = { srfctrlid: this.getName(), srfaction: 'fetch' };
        if (params) {
            Object.assign(opt, params);
        }
        this.fire(IBizMDControl.BEFORELOAD, opt);
        Object.assign(opt, { start: 0, limit: 500 });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe((success: any) => {
            if (success.ret == 0) {
                success.items.forEach(item => {
                    item.saveRefView = 0;
                    item.isDirty = false;
                });
                this.editCtrls = success.items;
            }
        }, (error: any) => {
            console.log(error);
        });
    }

    /**
     * 刷新加载数据
     *
     * @memberof IBizMultiEditViewPanel
     */
    public refresh(): void {
        this.load();
    }

    /**
     * 添加编辑页
     *
     * @memberof IBizMultiEditViewPanel
     */
    public addEditView(): void {
        this.editCtrls.push({ srfmajortext: 'New', isDirty: true, saveRefView: 0 });
        this.activeTab = this.editCtrls.length - 1;

        this.onDataChanged();
    }

    /**
     * 表单保存完成后回调
     *
     * @param {*} [data={}]
     * @param {*} [item={}]
     * @memberof IBizMultiEditViewPanel
     */
    public onEditFormSaved(data: any = {}, item: any = {}): void {
        Object.assign(item, data);
        item.isDirty = false;
        item.saveRefView = 0;
        if(this.submitCount > 0) {
            this.submitCount--;
            if (this.submitCount === 0) {
                if (this.removeKeys.length > 0) {
                    this.doRemove();
                } else {
                    this.fire(IBizMultiEditViewPanel.DATASAVED, this);
                }
            }
        } else {
            this.onDataChanged();
        }
    }

    public onBeforeRemove() {
        return (num) => {
            let item = this.editCtrls[num];
            if (item) {
                if(item.srfkey && this.removeKeys.indexOf(item.srfkey) < 0) {
                    this.removeKeys.push(item.srfkey);
                }
                this.editCtrls.splice(num, 1);
            }
            if(num == this.activeTab) {
                if(num >= this.editCtrls.length) {
                    this.activeTab = this.editCtrls.length - 1;
                }
            }
            this.onDataChanged();
            return new Promise((resolve, reject) => {});
        }
    }

    /**
     * 删除编辑数据
     *
     * @param {*} item
     * @memberof IBizMultiEditViewPanel
     */
    public onRemoveEditView(num) {
        let item = this.editCtrls[num];
        if (item) {
            if(item.srfkey && this.removeKeys.indexOf(item.srfkey) < 0) {
                this.removeKeys.push(item.srfkey);
            }
            this.editCtrls.splice(num, 1);
        }
        this.onDataChanged();
    }

    /**
     * 是否发生值变更
     *
     * @returns {boolean}
     * @memberof IBizMultiEditViewPanel
     */
    public isDirty(): boolean {
        if (this.removeKeys.length > 0) {
            return true;
        }
        let items: Array<any> = this.editCtrls.filter((item) => item.isDirty);
        if (items.length > 0) {
            return true;
        }
        return false;
    }

    /**
     * 执行保存
     *
     * @memberof IBizMultiEditViewPanel
     */
    public doSave(): void {
        let items: Array<any> = this.editCtrls.filter((item) => item.isDirty);
        if (items && items.length > 0) {
            items.forEach((item) => {
                this.submitCount++;
                item.saveRefView++;
            });
        } else {
            if (this.removeKeys.length > 0) {
                this.doRemove();
            }
        }
    }

    /**
     * 删除数据
     *
     * @memberof IBizMultiEditViewPanel
     */
    public doRemove(): void {
        let params: any = { srfaction: 'remove', srfctrlid: 'meditviewpanel', 'srfkeys': this.removeKeys.join(';') };
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe((success: any) => {
            if (success.ret == 0) {
                this.fire(IBizMultiEditViewPanel.DATASAVED, this);
            }
        }, (error: any) => {
            console.log(error);
        });
    }

    /**
     * 数据发生改变
     *
     * @memberof IBizMultiEditViewPanel
     */
    public onDataChanged(): void {
        let isDirty = false;
        let items: Array<any> = this.editCtrls.filter((item) => item.isDirty);
        if ((items && items.length > 0) || this.removeKeys.length > 0) {
            isDirty = true;
        }
        this.fire(IBizMultiEditViewPanel.EDITVIEWCHANGED, isDirty);
    }

    /**
     * 表单项改变
     *
     * @memberof IBizMultiEditViewPanel
     */
    public onEditFormFieldChanged(param: any, item: any): void {
        if (!param) {
            return;
        }
        item[param.name] = param.value;
        item.isDirty = true;
        this.onDataChanged();
    }

    /**
     * 获取选项编辑页控制对象事件
     */
    public static FINDEDITVIEWCONTROLLER = 'FINDEDITVIEWCONTROLLER';

    /**
     * 编辑页变化
     */
    public static EDITVIEWCHANGED = 'EDITVIEWCHANGED';

    /**
     * 保存完成
     */
    public static DATASAVED = 'DATASAVED';
}