/**
 * 关系分页
 *
 * @class IBizDRTab
 * @extends {IBizTab}
 */
class IBizDRTab extends IBizTab {

    /**
     * 父数据对象
     * 
     * @type {*}
     * @memberof IBizDRTab
     */
    public srfParentData: any = {};

    /**
     * Creates an instance of IBizDRTab.
     * 创建 IBizDRTab 实例
     * @param {*} [opts={}] 
     * @memberof IBizDRTab
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 设置父数据
     * 
     * @param {*} [data={}] 
     * @memberof IBizDRTab
     */
    public setParentData(data: any = {}): void {
        this.srfParentData = {};
        Object.assign(this.srfParentData, data);
    }

    /**
     * 获取父数据
     *
     * @returns {*}
     * @memberof IBizDRTab
     */
    public getParentData(): any {
        return this.srfParentData;
    }

    /**
     * 分页部件选中变化
     *
     * @param {string} name
     * @returns {void}
     * @memberof IBizDRTab
     */
    public onTabSelectionChange(name: string): void {
        let viewid: string = name;
        let controller = this.getViewController();

        let parentKey: string = '';
        if (this.srfParentData.srfparentkey) {
            parentKey = this.srfParentData.srfparentkey;
        }

        if (!parentKey || Object.is(parentKey, '')) {
            this.iBizNotification.warning('警告', '请先建立主数据');
            let tab = this.getTab('form');
            this.setActiveTab(tab);
            return;
        }

        if (Object.is(viewid, 'form')) {
            this.fire(IBizDRTab.SELECTCHANGE, { parentMode: {}, parentData: {}, viewid: 'form' });
            let tab = this.getTab('form');
            this.setActiveTab(tab);
            return;
        }

        const dritem: any = { viewid: viewid.toLocaleUpperCase() };
        if (!dritem.viewid || Object.is(dritem.viewid, '')) {
            return;
        }

        const viewItem: any = controller.getDRItemView(dritem);
        if (viewItem == null || !viewItem.viewparam) {
            return;
        }

        let item: any = { viewparam: {} };
        Object.assign(item, viewItem);

        Object.assign(item.viewparam, this.getParentData());

        let tab = this.getTab(viewid);
        Object.assign(item, { index: tab.index, name: tab.name });
        this.fire(IBizDRTab.SELECTCHANGE, item);
    }

    /**
     * 设置分页状态
     *
     * @param {boolean} state
     * @memberof IBizDRTab
     */
    public setTabState(state: boolean): void {
        let tab_names: Array<string> = Object.keys(this.tabs);
        tab_names.forEach((name: string) => {
            if (Object.is(name, 'form')) {
                this.tabs[name].disabled = false;
            } else {
                this.tabs[name].disabled = state;
            }
        });
    }

    /**
     * 关系分页选中
     *
     * @static
     * @memberof IBizDRTab
     */
    public static SELECTCHANGE = 'SELECTCHANGE';
}

