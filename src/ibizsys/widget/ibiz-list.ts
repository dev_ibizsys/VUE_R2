/**
 * 列表部件
 *
 * @class IBizList
 * @extends {IBizMDControl}
 */
class IBizList extends IBizMDControl {

    /**
     * Creates an instance of IBizList.
     * 创建 IBizList 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizList
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 
     * 
     * @param {*} [arg={}] 
     * @returns {void} 
     * @memberof IBizList
     */
    public load(arg: any = {}): void {
        let opt: any = {};
        Object.assign(opt, arg);

        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });

        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);

        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe((response: any) => {
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    this.iBizNotification.error('', response.errorMessage);

                }
                return;
            }
            this.items = response.items;
            this.fire(IBizMDControl.LOADED, response.items);

        }, (error: any) => {
            console.log(error.info);
        });
    }

    /**
     * 删除单条数据
     * 
     * @param {*} item 
     * @memberof IBizList
     */
    public doRemove(item: any): void {
        if (item) {
        }
    }
    /**
     * 删除所选数据
     * 
     * @memberof IBizList
     */
    public doRemoveAll(): void {

    }
}

