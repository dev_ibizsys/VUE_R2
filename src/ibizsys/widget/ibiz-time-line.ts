/**
 * 时间轴
 *
 * @class IBizTimeline
 * @extends {IBizControl}
 */
class IBizTimeline extends IBizControl {

    /**
     * 数据集合
     *
     * @type {Array<any>}
     * @memberof IBizTimeline
     */
    public items: Array<any> = [];

    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @memberof IBizTimeline
     */
    public load(arg: any = {}): void {
        let opt: any = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(response => {
            if (response.ret === 0) {
                this.items = response.items;
            } else {
                this.iBizNotification.error('', response.errorMessage);
            }
        },error => {
            console.log(error.info);
        });
    }

    /**
     * 刷新数据
     *
     * @param {*} [arg={}]
     * @memberof IBizTimeline
     */
    public refresh(arg: any = {}): void {
        this.load(arg);
    }
}