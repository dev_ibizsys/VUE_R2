/**
 * 嵌入编辑视图控制器
 *
 * @class IBizEditView9Controller
 * @extends {IBizEditViewController}
 */
class IBizEditView9Controller extends IBizEditViewController {

    /**
     * Creates an instance of IBizEditView9Controller.
     * 创建 IBizEditView9Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizEditView9Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图刷新
     * 
     * @memberof IBizEditView9Controller
     */
    public onRefresh(): void {
        const editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    }

    /**
     * 视图参数变化，嵌入表单，手动刷新数据
     * 
     * @param {*} change 
     * @memberof IBizEditView9Controller
     */
    public viewParamChange(change: any) {
        if (change.srfparentkey && !Object.is(change.srfparentkey, '')) {
            this.addViewParam({ srfkey: change.srfparentkey });
            this.onRefresh();
        }
    }
}
