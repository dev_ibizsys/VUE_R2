/**
 * 索引关系数据选择视图（部件视图）
 *
 * @class IBizDataViewController
 * @extends {IBizMDViewController}
 */
class IBizDataViewController extends IBizMDViewController {

    /**
     * 是否支持多项数据选择  <Input>
     *
     * @private
     * @type {boolean}
     * @memberof IBizDataViewController
     */
    private multiselect: boolean = true;

    /**
     * 当前选择数据 <Input>
     *
     * @private
     * @type {*}
     * @memberof IBizDataViewController
     */
    private currentValue: any = null;

    /**
     * 删除数据 <Input>
     *
     * @private
     * @type {*}
     * @memberof IBizDataViewController
     */
    private deleteData: any = null;

    /**
     * 数据选中事件 <Output>
     *
     * @private
     * @type {string}
     * @memberof IBizDataViewController
     */
    private selectionChange: string = 'selectionChange';

    /**
     * 行数据激活事件 <Output>
     *
     * @private
     * @type {string}
     * @memberof IBizDataViewController
     */
    private dataActivated: string = 'dataActivated';

    /**
     * Creates an instance of IBizDataViewController.
     * 创建 IBizDataViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizDataViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizDataViewController
     */
    public onInitComponents(): void {

        const dataViewControl: IBizDataView = this.getMDCtrl();
        if (dataViewControl) {
            // 数据视图部件行激活
            dataViewControl.on(IBizDataView.DATAACTIVATED).subscribe((data: any) => {
                this.onDataActivated(data);
            });
            // 数据视图部件行选中
            dataViewControl.on(IBizDataView.SELECTIONCHANGE).subscribe((data: any) => {
                this.onSelectionChange(data);
            });
        }
    }

    /**
     * 部件加载
     * 
     * @memberof IBizDataViewController
     */
    public onLoad(): void {
        const dataViewControl = this.getMDCtrl();
        if (dataViewControl) {
            dataViewControl.load();
        }
    }

    /**
     * 部件数据激活
     * 
     * @param {*} data 
     * @memberof IBizDataViewController
     */
    public onDataActivated(data: Array<any>): void {
        super.onDataActivated(data);
        this.$vue.$emit(this.dataActivated, data);
    }

    /**
     * 部件数据行选中
     * 
     * @param {*} data 
     * @memberof IBizDataViewController
     */
    public onSelectionChange(data: Array<any>): void {
        super.onSelectionChange(data);
        this.$vue.$emit(this.selectionChange, data);
    }

    /**
     * 获取部件
     * 
     * @returns {*} 
     * @memberof IBizDataViewController
     */
    public getMDCtrl(): any {
        return this.getDataView();
    }

    /**
     * 获取数据视图部件
     * 
     * @returns {*} 
     * @memberof IBizDataViewController
     */
    public getDataView(): any {
        return this.getControl('dataview');
    }
}
