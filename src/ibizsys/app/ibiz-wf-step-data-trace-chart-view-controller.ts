/**
 * 流程图
 *
 * @class IBizWFStepDataTraceChartViewController
 * @extends {IBizMDViewController}
 */
class IBizWFStepDataTraceChartViewController extends IBizMDViewController {

    /**
     * 实体名称
     *
     * @type {string}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    public srfdeid: string = '';

    /**
     *Creates an instance of IBizWFStepDataTraceChartViewController.
     * @param {*} [opts={}]
     * @memberof IBizWFStepDataTraceChartViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    public getMDCtrl(): any {
        return undefined;
    }

    /**
     * 获取图片路径
     *
     * @returns {string}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    public getImgPath(): string {
        return '';
    }
}