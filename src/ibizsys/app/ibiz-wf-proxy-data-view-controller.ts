/**
 * 工作流代理数据视图
 *
 * @class IBizWFProxyDataViewController
 * @extends {IBizMainViewController}
 */
class IBizWFProxyDataViewController extends IBizMainViewController {

    /**
     * 代理数据视图url
     *
     * @type {string}
     * @memberof IBizWFProxyDataViewController
     */
    public viewurl: string = '';

    /**
     * Creates an instance of IBizWFProxyDataViewController.
     * 创建 IBizWFProxyDataViewController 实例
     * @param {*} [opts={}]
     * @memberof IBizWFProxyDataViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 刷新数据 （处理关系数据变更）
     *
     * @memberof IBizWFProxyDataViewController
     */
    public onRefresh(): void {
        super.onRefresh();
        this.loadModel();
    }

    /**
     * 模型加载之前
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyDataViewController
     */
    public beforeLoadMode(data: any = {}): void {
        super.beforeLoadMode(data);
        if (this.getParentMode()) {
            Object.assign(data, this.getParentMode());
        }
        if (this.getParentData()){
            Object.assign(data, this.getParentData());
        }
        if (this.getReferData()) {
            Object.assign(data, this.getReferData());
        }
    }

    /**
     * 工作流加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyDataViewController
     */
    public afterLoadMode(data: any = {}): void {
        super.afterLoadMode(data);
        this.viewurl = data.viewurl;
    }
}