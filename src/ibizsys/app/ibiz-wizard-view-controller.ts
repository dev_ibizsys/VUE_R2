/**
 * 向导视图控制器
 *
 * @class IBizWizardViewController
 * @extends {IBizMainViewController}
 */
class IBizWizardViewController extends IBizMainViewController {

    /**
     *Creates an instance of IBizWizardViewController.
     * @param {*} [opts={}]
     * @memberof IBizWizardViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化面板
     * 
     * @memberof IBizEditViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();

        const wizardpanel: any = this.getWizardPanel();
        if (wizardpanel) {
            // 向导面板初始化完成
            wizardpanel.on(IBizWizardPanel.WIZARDPANEL_INITED).subscribe((data) => {
                this.onWizardInited(data);
            });
            // 向导面板完成
            wizardpanel.on(IBizWizardPanel.WIZARDPANEL_FINISH).subscribe((data) => {
                this.onWizardFinesh(data);
            });
            // 向导表单保存完成
            wizardpanel.on(IBizWizardPanel.WIZARDFORMSAVED).subscribe((data) => {
                this.onWizardFormSaved(data);
            });
            // 向导表单切换
            wizardpanel.on(IBizWizardPanel.WIZARDFORMCHANGED).subscribe((data) => {
                this.onWizardFormChanged(data);
            });
            // 向导表单项更新
            wizardpanel.on(IBizWizardPanel.WIZARDFORMFIELDCHANGED).subscribe((data) => {
                if(data) {
                    this.onWizardFormFieldChanged(data.formName, data.name, data.field, data.value);
                }
            });
        }
    }

    /**
     * 加载面板数据
     * 
     * @memberof IBizWizardViewController
     */
    public onLoad(): void {
        if (this.getWizardPanel()) {
            this.getWizardPanel().autoLoad(this.getViewParam());
        }
    }

    /**
     * 获取向导面板对象
     *
     * @returns {*}
     * @memberof IBizWizardViewController
     */
    public getWizardPanel(): any {
        return this.getControl('wizardpanel');
    }

    /**
     * 向导表单保存完成
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    public onWizardFormSaved(data): void {

    }

    /**
     * 向导面板初始化完成
     *
     * @memberof IBizWizardViewController
     */
    public onWizardInited(data): void {

    }

    /**
     * 向导面板完成
     *
     * @memberof IBizWizardViewController
     */
    public onWizardFinesh(data): void {
        let _this = this;
        _this.$vue.$emit('dataChange', data);
        _this.refreshReferView(data);
    }

    /**
     * 向导表单切换
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    public onWizardFormChanged(data): void {

    }

    /**
     * 向导表单项更新
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    public onWizardFormFieldChanged(formName, fieldname, field, value): void {

    }

    /**
     * 刷新关联数据
     * 
     * @memberof IBizEditViewController
     */
    public refreshReferView(data: any): void {
        let iBizApp: IBizApp = IBizApp.getInstance();
        if (iBizApp) {
            let parentWindow: any = iBizApp.getParentWindow();
            let viewparam = this.getViewParam();
            if (parentWindow && viewparam.openerid && !Object.is(viewparam.openerid, '')) {
                try {
                    let pWinIBizApp: IBizApp = parentWindow.getIBizApp();
                    pWinIBizApp.fireRefreshView({ openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) });
                } catch (error) {
                    parentWindow.postMessage({ ret: 'OK', type: 'REFERVIEW', openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) }, '*');
                    // if (window && window.parent) {
                    //     let win = window;
                    // }
                }
            }
        }

        if (this.isModal()) {
            let result: any = { ret: 'OK', activeData: data };
            this.dataChange(result);
        }
    }
}