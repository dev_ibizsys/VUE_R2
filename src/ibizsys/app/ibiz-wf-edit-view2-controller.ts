/**
 * 工作流左右关系编辑视图
 *
 * @class IBizWFEditView2Controller
 * @extends {IBizEditView2Controller}
 */
class IBizWFEditView2Controller extends IBizEditView2Controller {

    /**
     * Creates an instance of IBizWFEditView2Controller.
     * 创建 IBizWFEditView2Controller  实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizWFEditView2Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}
