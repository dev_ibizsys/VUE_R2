/**
 * 实体重定向视图
 *
 * @class IBizRedirectViewController
 * @extends {IBizMainViewController}
 */
class IBizRedirectViewController extends IBizMainViewController {

    /**
     * Creates an instance of IBizRedirectViewController.
     * 创建 IBizRedirectViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizRedirectViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图加载
     *
     * @memberof IBizRedirectViewController
     */
    public onLoad(): void {
        super.onLoad();
        this.loadRedirectData();
    }

    /**
     * 加载重新向数据
     *
     * @private
     * @memberof IBizRedirectViewController
     */
    private loadRedirectData(): void {
        let params: any = { srfaction: 'GETRDVIEWURL' };
        Object.assign(params, this.getViewParam());
        this.beforeLoadRedirectData(params);
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe((success: any) => {
            this.afterLoadRedirectData(success);
            if (success.ret === 0) {
                if (success.rdview && !Object.is(success.rdview, '')) {
                    window.location = success.rdview;
                }
            }
        }, (error: any) => {
            this.afterLoadRedirectData(error);
        });
    }

    /**
     * 加载重定向数据之前
     *
     * @param {*} [data={}]
     * @memberof IBizRedirectViewController
     */
    public beforeLoadRedirectData(data: any = {}): void {

    }

    /**
     * 加载重定向数据之后
     *
     * @param {*} [data={}]
     * @memberof IBizRedirectViewController
     */
    public afterLoadRedirectData(data: any = {}): void {

    }
}