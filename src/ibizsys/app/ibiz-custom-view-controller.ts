/**
 * 自定义视图控制器对象
 *
 * @class IBizCustomViewController
 * @extends {IBizMainViewController}
 */
class IBizCustomViewController extends IBizMainViewController {

    /**
     * Creates an instance of IBizCustomViewController.
     * 创建 IBizCustomViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizCustomViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 加载部件
     * 
     * @param {*} [opt={}] 
     * @memberof IBizMDViewController
     */
    public onLoad(opt: any = {}): void {
        if (this.controls) {
            this.controls.forEach((obj, key) => {
                if (obj instanceof IBizToolbar) {
                    return;
                }
                if (obj.load) {
                    obj.load(opt);
                }
            });
        }
    }
}