/**
 * 左右编辑视图控制器
 * 
 * @export
 * @class IBizEditView2Controller
 * @extends {IBizEditViewController}
 */
class IBizEditView2Controller extends IBizEditViewController {

    /**
     * Creates an instance of IBizEditView2Controller.
     * 创建IBizEditView2Controller实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizEditView2Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化部件
     * 
     * @memberof IBizEditView2Controller
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const drBar: any = this.getDRBar();
        if (drBar) {
            // 关系数据部件加载完成
            drBar.on(IBizDRBar.DRBARLOADED).subscribe((item) => {
                this.onDRBarLoaded(item);
            });
            // 关系部件选中变化
            drBar.on(IBizDRBar.DRBARSELECTCHANGE).subscribe((item) => {
                this.onDRBarItemChangeSelect(item);
            });
        }
    }

    /**
     * 视图加载,只触发关系栏加载
     * 
     * @returns {void} 
     * @memberof IBizEditView2Controller
     */
    public onLoad(): void {
        const drBar: any = this.getDRBar();
        if (drBar) {
            drBar.load();
        }

    }

    /**
     * 关系数据项加载完成，触发视图加载
     * 
     * @private
     * @param {*} [item={}] 
     * @returns {void} 
     * @memberof IBizEditView2Controller
     */
    private onDRBarLoaded(items: Array<any>): void {
        if (!this.getForm()) {
            this.iBizNotification.error('错误', '表单不存在!');
        }

        super.onLoad();
    }

    /**
     * 表单部件加载完成
     * 
     * @param {any} params 
     * @memberof IBizEditView2Controller
     */
    public onFormLoaded(): void {
        super.onFormLoaded();

        let drBar: any = this.getDRBar();

        const form = this.getForm();
        const _field = form.findField('srfkey');
        const _srfuf = form.findField('srfuf');

        let matched: Array<any> = this.$route.matched;

        let formState = (Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '')) ? 'new' : 'edit';
        drBar.setDRBarItemState(drBar.getItems(), Object.is(formState, 'new') ? true : false);
        drBar.setSelectItem({ id: 'form' });

        if (this.isHideEditForm()) {
            if (Object.is(formState, 'new')) {
                this.iBizNotification.warning('警告', '新建模式，表单主数据不存在，该表单已被配置隐藏');
                return;
            }
        }

        if (matched[this.route_index + 1]) {
            let nextItem: any = drBar.getItem(drBar.getItems(), { routepath: matched[this.route_index + 1].name });
            if (nextItem && Object.keys(nextItem).length) {
                this.isShowToolBar = false;
                drBar.setSelectItem(nextItem);
            }
        }
    }
    
    /**
     * 表单保存完成
     *
     * @param {*} result
     * @memberof IBizEditView2Controller
     */
    public onFormSaved(result: any): void {
        super.onFormSaved(result);
        let drBar: any = this.getDRBar();
        drBar.setDRBarItemState(drBar.getItems(), false);
    }

    /**
     * 是否影藏编辑表单
     * 
     * @returns {boolean} 
     * @memberof IBizEditView2Controller
     */
    public isHideEditForm(): boolean {
        return false;
    }

    /**
     * 关联数据更新
     * 
     * @returns {void} 
     * @memberof IBizEditView2Controller
     */
    public updateViewInfo(): void {
        super.updateViewInfo();
        const form = this.getForm();
        if (!form) {
            return;
        }
        const field = form.findField('srfkey');
        if (!field) {
            return;
        }
        let keyvalue = field.getValue();

        const srforikey = form.findField('srforikey');
        if (field) {
            const keyvalue2 = field.getValue();
            if (keyvalue2 && !Object.is(keyvalue2, '')) {
                keyvalue = keyvalue2;
            }
        }
        let deid = '';
        const deidfield = form.findField('srfdeid');
        if (deidfield) {
            deid = deidfield.getValue();
        }
        let parentData: any = { srfparentkey: keyvalue };
        if (!Object.is(deid, '')) {
            parentData.srfparentdeid = deid;
        }
        if (this.getDRBar()) {
            this.getDRBar().setParentData(parentData);
        }
    }

    /**
     * 数据项选中变化
     * 
     * @private
     * @param {*} [item={}] 
     * @memberof IBizEditView2Controller
     */
    private onDRBarItemChangeSelect(item: any = {}): void {
        let _isShowToolBar: boolean = Object.is(item.id, 'form') ? true : false;
        this.isShowToolBar = _isShowToolBar;

        this.setDRBarSelect(item);

        if (_isShowToolBar) {
            this.$router.push({ path: this.route_url });
        } else {
            this.openView(item.routepath, item.viewparam);

        }
    }

    /**
     * 获取数据关系部件
     * 
     * @returns {any} 
     * @memberof IBizEditView2Controller
     */
    public getDRBar(): any {
        return this.getControl('drbar');
    }

    /**
     * 设置数据关系部件选中
     *
     * @param {*} [item={}]
     * @memberof IBizEditView2Controller
     */
    public setDRBarSelect(item: any = {}): void {
        if (this.getDRBar()) {
            this.getDRBar().setSelectItem(item);
        }
    }

    /**
     * 获取关系视图
     *
     * @param {*} [view={}]
     * @returns {*}
     * @memberof IBizEditView2Controller
     */
    public getDRItemView(view: any = {}): any {

    }
}

