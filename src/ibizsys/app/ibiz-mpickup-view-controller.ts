/**
 * 多项数据选择视图控制器
 *
 * @class IBizMPickupViewController
 * @extends {IBizMainViewController}
 */
class IBizMPickupViewController extends IBizMainViewController {

    /**
     * 按钮文本--确定
     *
     * @type {string}
     * @memberof IBizMPickupViewController
     */
    public okBtnText: string = '确定';

    /**
     * 按钮文本--取消
     *
     * @type {string}
     * @memberof IBizMPickupViewController
     */
    public cancelBtnText: string = '取消';

    /**
     * 是否是iframe嵌入视图
     *
     * @private
     * @type {boolean}
     * @memberof IBizMPickupViewController
     */
    private srfembed: boolean = false;

    /**
     * 父视图id
     *
     * @private
     * @type {string}
     * @memberof IBizMPickupViewController
     */
    private parentOpenerid: string = '';

    /**
     * 多项选择数据集服务对象
     * 
     * @type {IBizMPickupResult}
     * @memberof IBizMPickupViewController
     */
    public MPickupResult: IBizMPickupResult = null;

    /**
     * Creates an instance of IBizMPickupViewController.
     * 创建 IBizMPickupViewController 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizMPickupViewController
     */
    constructor(opts: any = {}) {
        super(opts);

        this.MPickupResult = new IBizMPickupResult({
            name: 'mpickupresult',
            viewController: this,
            url: opts.url,
        });
        this.regControl('mpickupresult', this.MPickupResult);
    }

    /**
     * 视图部件初始化
     *
     * @memberof IBizMPickupViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const pickupViewPanel = this.getPickupViewPanel();
        if (pickupViewPanel && this.MPickupResult) {
            // 选择视图面板数据选中
            pickupViewPanel.on(IBizPickupViewPanel.SELECTIONCHANGE).subscribe((args) => {
                this.MPickupResult.setCurSelections(args);
            });
            // 选择视图面板数据激活
            pickupViewPanel.on(IBizPickupViewPanel.DATAACTIVATED).subscribe((args) => {
                this.MPickupResult.appendDatas(args);
            });
            // 选择视图面板所有数据
            pickupViewPanel.on(IBizPickupViewPanel.ALLDATA).subscribe((args) => {
                this.MPickupResult.setAllData(args);
            });
        }
    }

    /**
     * 准备视图参数
     *
     * @memberof IBizMPickupViewController
     */
    public parseViewParams(): void {
        super.parseViewParams();

        let viewparams: any = this.getViewParam();
        if (viewparams.hasOwnProperty('srfembed')) {
            this.srfembed = Object.is(viewparams.srfembed, 'true') ? true : false;
        }
        if (viewparams.hasOwnProperty('openerid')) {
            this.parentOpenerid = viewparams.openerid;
        }
    }

    /**
     * 处理视图参数
     * 
     * @memberof IBizMPickupViewController
     */
    public onInit(): void {
        super.onInit();
        if (this.getViewParam() && Array.isArray(this.getViewParam().selectedData)) {
            if (this.MPickupResult) {
                this.MPickupResult.appendDatas(this.getViewParam().selectedData);
            }
        }
    }

    /**
     * 数据选择，确定功能
     * 
     * @memberof IBizPickupViewController
     */
    public onClickOkButton(): void {

        if (this.MPickupResult.selections.length === 0) {
            return;
        }
        // this.nzModalSubject.next({ ret: 'OK', selection: this.MPickupResult.selections });
        // this.nzModalSubject.next('DATACHANGE');
        // this.closeWindow();
        if (this.srfembed && window && window.parent && !Object.is(this.parentOpenerid, '')) {
            let win = window;
            win.parent.postMessage({ ret: 'OK', type: 'SETSELECTIONS', selections: this.MPickupResult.selections, openerid: this.parentOpenerid }, '*');
            return;
        }
        this.closeModal({ ret: 'OK', selections: this.MPickupResult.selections });
    }


    /**
     * 关闭显示选择视图
     * 
     * @param {*} type 
     * @memberof IBizMPickupViewController
     */
    public onClickCancelButton(type: any): void {
        // this.nzModalSubject.destroy(type);
        if (this.srfembed && window && window.parent && !Object.is(this.parentOpenerid, '')) {
            let win = window;
            win.parent.postMessage({ ret: 'OK', type: 'SETSELECTIONS', selections: null, openerid: this.parentOpenerid }, '*');
            return;
        }
        this.closeModal();
    }

    /**
     * 获取选中视图面板
     *
     * @returns {*}
     * @memberof IBizMPickupViewController
     */
    public getPickupViewPanel(): any {
        return this.getControl('pickupviewpanel');
    }
}

