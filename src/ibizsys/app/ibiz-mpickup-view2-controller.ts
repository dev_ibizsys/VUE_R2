/**
 * 左右关系多项数据选择视图控制器
 *
 * @class IBizMPickupView2Controller
 * @extends {IBizMPickupViewController}
 */
class IBizMPickupView2Controller extends IBizMPickupViewController {

    /**
     * Creates an instance of IBizMPickupView2Controller.
     * 创建 IBizMPickupView2Controller 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizMPickupView2Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}