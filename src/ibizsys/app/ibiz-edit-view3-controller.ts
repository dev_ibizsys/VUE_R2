/**
 * 分页编辑视图
 *
 * @class IBizEditView3Controller
 * @extends {IBizEditViewController}
 */
class IBizEditView3Controller extends IBizEditViewController {

    /**
     * Creates an instance of IBizEditView3Controller.
     * 创建 IBizEditView3Controller 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizEditView3Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图部件初始化，注册所有事件
     * 
     * @memberof IBizEditView3Controller
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const drTab: any = this.getDRTab();
        if (drTab) {
            // 分页导航选中
            drTab.on(IBizDRTab.SELECTCHANGE).subscribe((data) => {
                this.doDRTabSelectChange(data);
            });
        }
    }

    /**
     * 表单加载完成
     * 
     * @memberof IBizEditView3Controller
     */
    public onFormLoaded(): void {
        super.onFormLoaded();

        this.setDrTabState();

        const drtab: any = this.getDRTab();
        const form = this.getForm();
        const _field = form.findField('srfkey');
        const _srfuf = form.findField('srfuf');

        let tab: any = {};
        if (this.isHideEditForm()) {
            if (Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '')) {
                this.iBizNotification.warning('警告', '新建模式，表单主数据不存在，该表单已被配置隐藏');
                return;
            }
        }

        if (form.findField('srfkey') && !Object.is(form.findField('srfkey').getValue(), '')) {
            Object.assign(tab, this.getActivatedDRTab());
        }
        if (Object.keys(tab).length) {
            drtab.setActiveTab(tab);
        }
    }

    /**
     * 表单保存完成
     *
     * @param {*} result
     * @memberof IBizEditView3Controller
     */
    public onFormSaved(result: any): void {
        super.onFormSaved(result);
        this.setDrTabState();
    }

    /**
     * 是否隐藏编辑表单
     * 
     * @returns {boolean} 
     * @memberof IBizEditView3Controller
     */
    public isHideEditForm(): boolean {
        return false;
    }

    /**
     * 视图信息更新
     * 
     * @returns {void} 
     * @memberof IBizEditView3Controller
     */
    public updateViewInfo(): void {
        super.updateViewInfo();
        const form = this.getForm();
        if (!form) {
            return;
        }
        const field = form.findField('srfkey');
        if (!field) {
            return;
        }
        let keyvalue = field.getValue();

        const srforikey = form.findField('srforikey');
        if (field) {
            const keyvalue2 = field.getValue();
            if (keyvalue2 && !Object.is(keyvalue2, '')) {
                keyvalue = keyvalue2;
            }
        }
        let deid = '';
        const deidfield = form.findField('srfdeid');
        if (deidfield) {
            deid = deidfield.getValue();
        }
        let parentData: any = { srfparentkey: keyvalue };
        if (!Object.is(deid, '')) {
            parentData.srfparentdeid = deid;
        }
        if (this.getDRTab()) {
            this.getDRTab().setParentData(parentData);
        }
    }

    /**
     * 关系分页部件选择变化处理
     * 
     * @param {*} [data={}] 
     * @memberof IBizEditView3Controller
     */
    public doDRTabSelectChange(data: any = {}): void {
        let _isShowToolBar: boolean = Object.is(data.viewid, 'form') ? true : false;
        this.isShowToolBar = _isShowToolBar;;

        this.setActiveTab(data);
        if (_isShowToolBar) {
            this.$router.push({ path: this.route_url });
        } else {
            this.openView(data.routepath, data.viewparam);

        }
    }

    /**
     * 获取关系视图参数
     * 
     * @param {*} [arg={}] 
     * @returns {*} 
     * @memberof IBizEditView3Controller
     */
    public getDRItemView(arg: any = {}): any {

    }

    /**
     * 刷新视图
     * 
     * @memberof IBizEditView3Controller
     */
    public onRefresh(): void {
        if (this.getDRTab() && this.getDRTab().refresh && this.getDRTab().refresh instanceof Function) {
            this.getDRTab().refresh();
        }
    }

    /**
     * 获取关系分页部件
     * 
     * @returns {*} 
     * @memberof IBizEditView3Controller
     */
    public getDRTab(): any {
        return this.getControl('drtab');
    }

    /**
     * 获取激活关系分页
     *
     * @private
     * @returns {*}
     * @memberof IBizEditView3Controller
     */
    private getActivatedDRTab(): any {
        let _tab: any = {};
        let matched: Array<any> = this.$route.matched;
        let drTab: any = this.getDRTab();
        if (matched[1]) {
            let next_route_name = matched[1].name;
            let tab = drTab.getTab(null, next_route_name);
            if (tab) {
                this.isShowToolBar = false;
                Object.assign(_tab, tab);
            }
        } else {
            let tab = drTab.getTab('form');
            if (tab) {
                this.isShowToolBar = true;
                Object.assign(_tab, tab);
            }
        }
        return _tab;
    }

    /**
     * 设置关系分页状态
     *
     * @memberof IBizEditView3Controller
     */
    public setDrTabState(): void {
        const form = this.getForm();
        const _field = form.findField('srfkey');
        const _srfuf = form.findField('srfuf');
        const state: boolean = Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '');
        if (this.getDRTab()) {
            this.getDRTab().setTabState(state);
        }
    }

    /**
     * 设置激活分页
     *
     * @param {*} [tab={}]
     * @memberof IBizEditView3Controller
     */
    public setActiveTab(tab: any = {}): void {
        if (this.getDRTab()) {
            this.getDRTab().setActiveTab(tab);
        }
    }
}
