/**
 * 数据图表视图控制器
 *
 * @class IBizChartViewController
 * @extends {IBizSearchViewController}
 */
class IBizChartViewController extends IBizSearchViewController {

    /**
     * Creates an instance of IBizChartViewController.
     * 创建 IBizChartViewController 对象
     * 
     * @param {*} [opts={}] 
     * @memberof IBizChartViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图部件初始化
     * 
     * @memberof IBizChartViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const chart = this.getChart();
        if (chart) {
            chart.on(IBizChart.LOADED).subscribe((data: any) => {
                this.onStoreLoad(data);
            });
            chart.on(IBizChart.BEFORELOAD).subscribe((data: any) => {
                this.onStoreBeforeLoad(data);
            });
            chart.on(IBizChart.DBLCLICK).subscribe((data: any) => {
                this.onDataActivated();
            });
        }
    }

    /**
     * 视图加载
     * 
     * @memberof IBizChartViewController
     */
    public onLoad(): void {
        super.onLoad();
        if (!this.getSearchForm() && this.isLoadDefault()) {
            if(!this.isFreeLayout()) {
                if(this.getChart()) {
                    this.getChart().load();
                }
            } else {
                this.otherLoad();
            }
        }
    }

    /**
     * 获取图表部件
     * 
     * @returns {*} 
     * @memberof IBizChartViewController
     */
    public getChart(): any {
        return this.getControl('chart');
    }

    /**
     * 搜索表单触发加载
     * 
     * @param {boolean} isload 是否加载
     * @memberof IBizChartViewController
     */
    public onSearchFormSearched(): void {
        if(!this.isFreeLayout()) {
            if(this.getChart()) {
                this.getChart().load();
            }
        } else {
            this.otherLoad();
        }
    }

    /**
     * 表单重置完成
     * 
     * @memberof IBizChartViewController
     */
    public onSearchFormReseted(): void {
        if(!this.isFreeLayout()) {
            if(this.getChart()) {
                this.getChart().load();
            }
        } else {
            this.otherLoad();
        }
    }

    /**
     * 视图部件刷新
     * 
     * @memberof IBizChartViewController
     */
    public onRefresh(): void {
        super.onRefresh();
        if(!this.isFreeLayout()) {
            if(this.getChart()) {
                this.getChart().load();
            }
        } else {
            this.otherLoad();
        }
    }

    /**
     * 附加额外部件加载
     *
     * @memberof IBizChartViewController
     */
    public otherLoad(): void {
        if(this.controls) {
            this.controls.forEach((obj, key) => {
                if(obj instanceof IBizForm || obj instanceof IBizToolbar) {
                    return;
                }
                if(obj.load) {
                    obj.load();
                }
            });
        }
    }
}