/**
 * 左右关系单项选择视图控制器
 *
 * @class IBizPickupView2Controller
 * @extends {IBizPickupViewController}
 */
class IBizPickupView2Controller extends IBizPickupViewController {

    /**
     * Creates an instance of IBizPickupView2Controller.
     * 创建 IBizPickupView2Controller 实例
     * 
     * @param {*} [opts={}] 
     * @memberof IBizPickupView2Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}