/**
 * 工作流启动视图
 *
 * @class IBizHtmlViewController
 * @extends {IBizMainViewController}
 */
class IBizHtmlViewController extends IBizMainViewController {

    /**
     * 代理应用启动视图url
     *
     * @type {string}
     * @memberof IBizHtmlViewController
     */
    public viewurl: string = '';

    /**
     * Creates an instance of IBizHtmlViewController.
     * 创建 IBizHtmlViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizHtmlViewController
     */
    constructor(opts: any = {}) {
        super(opts);
        let iBizApp: IBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.onPostMessage().subscribe((data: any = {}) => {
                if (!Object.is(this.getId(), data.openerid) && !Object.is(data.type, 'SETSELECTIONS')) {
                    return;
                }
                this.setViewResult(data);
            });
        }
    }

    /**
     * 刷新数据 （处理关系数据变更）
     *
     * @memberof IBizHtmlViewController
     */
    public onRefresh(): void {
        super.onRefresh();
        this.loadModel();
    }

    /**
     * 模型加载之前
     *
     * @param {*} [data={}]
     * @memberof IBizHtmlViewController
     */
    public beforeLoadMode(data: any = {}): void {
        super.beforeLoadMode(data);
        if (this.getParentMode()) {
            Object.assign(data, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(data, this.getParentData());
        }
        if (this.getReferData()) {
            Object.assign(data, this.getReferData());
        }
    }

    /**
     * 视图模型加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizHtmlViewController
     */
    public afterLoadMode(data: any = {}): void {
        super.afterLoadMode(data);
        let url: string = data.viewurl;
        this.viewurl = url.replace('-OPENERID-', this.getId());
    }

    /**
     * 设置视图结果数据
     *
     * @param {*} data
     * @memberof IBizHtmlViewController
     */
    public setViewResult(data: any): void {
        if (data.selections && Array.isArray(data.selections)) {
            this.closeModal({ ret: 'OK', selections: data.selections });
        } else {
            this.closeModal();
        }
    }
}