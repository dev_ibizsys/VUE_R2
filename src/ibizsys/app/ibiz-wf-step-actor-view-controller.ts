/**
 * 流程步骤视图
 *
 * @class IBizWFStepActorViewController
 * @extends {IBizMDViewController}
 */
class IBizWFStepActorViewController extends IBizMDViewController {

    /**
     * 流程步骤对象
     *
     * @type {*}
     * @memberof IBizWFStepActorViewController
     */
    public wfstepactor: any = null;

    /**
     *Creates an instance of IBizWFStepActorViewController.
     * @param {*} [opts={}]
     * @memberof IBizWFStepActorViewController
     */
    constructor(opts: any = {}) {
        super(opts);
        this.wfstepactor = new IBizTimeline({
            name: 'grid',
            url: opts.backendurl,
            viewController: this,
            $route: opts.$route,
            $router: opts.$router,
            $vue: opts.$vue
        });
        this.controls.set('wfstepactor', this.wfstepactor);
    }

    /**
     * 初始化面板
     *
     * @memberof IBizWFStepActorViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
    }

    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof IBizWFStepActorViewController
     */
    public getMDCtrl(): any {
        return this.getControl('wfstepactor');
    }
    
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizWFStepActorViewController
     */
    public isEnableQuickSearch(): boolean {
        return false;
    }
}