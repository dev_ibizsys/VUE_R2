/**
 * 工作流启动视图
 *
 * @class IBizWFProxyStartViewController
 * @extends {IBizMainViewController}
 */
class IBizWFProxyStartViewController extends IBizMainViewController {

    /**
     * 代理应用启动视图url
     *
     * @type {string}
     * @memberof IBizWFProxyStartViewController
     */
    public startviewurl: string = '';

    /**
     * Creates an instance of IBizWFProxyStartViewController.
     * 创建 IBizWFProxyStartViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizWFProxyStartViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图模型加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyStartViewController
     */
    public afterLoadMode(data: any = {}): void {
        super.afterLoadMode(data);
        this.startviewurl = data.startviewurl;
    }
}