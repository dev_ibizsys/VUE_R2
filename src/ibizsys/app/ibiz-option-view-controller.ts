/**
 * 实体选项操作视图
 *
 * @class IBizOptionViewController
 * @extends {IBizEditViewController}
 */
class IBizOptionViewController extends IBizEditViewController {

    /**
     * Creates an instance of IBizOptionViewController.
     * 创建 IBizOptionViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizOptionViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 确定
     * 
     * @memberof IBizOptionViewController
     */
    public onClickOkButton(): void {
        this.doSaveAndExit();
    }

    /**
     * 关闭串口
     * 
     * @memberof IBizOptionViewController
     */
    public onClickCancelButton(): void {
        // var me = this;
        // var window = me.window;
        // if(window){
        // 	window.dialogResult = 'cancel';
        // }
        // me.closeWindow();
        // this.closeView();
        this.viewParam = {};
        this.closeWindow();
    }

    /**
     * 视图加载
     *
     * @memberof IBizOptionViewController
     */
    public onLoad(): void {
        const editForm: any = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    }

    /**
     * 表单保存完成
     * 
     * @returns {void} 
     * @memberof IBizOptionViewController
     */
    public onFormSaved(data: any): void {
        // var me = this;
        // var window = me.window;
        // if (window) {
        // 	window.dialogResult = 'ok';
        // 	window.activeData = me.getForm().getValues();
        // 	window.selectedData = window.activeData
        // }
        // me.closeWindow();
        this.refreshReferView();
        // this.closeView();
        this.viewParam = {};
        if (data.url && !Object.is(data.url, '')) {
            this.addViewParam({ ru: data.url });
        }
        this.closeWindow();
        return;
    }
}