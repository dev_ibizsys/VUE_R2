/**
 * 工作流结果视图
 *
 * @class IBizWFProxyResultViewController
 * @extends {IBizMainViewController}
 */
class IBizWFProxyResultViewController extends IBizMainViewController {


    /**
     * Creates an instance of IBizWFProxyResultViewController.
     * 创建 IBizWFProxyResultViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizWFProxyResultViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}