/**
 * 实体分页导航视图控制器
 * 
 * @export
 * @class IBizEntityIndexViewController
 * @extends {IBizEditViewController}
 */
class IBizEntityTabExpViewController extends IBizExpViewController {
    /**
     * 默认选中项下标
     * 
     * @memberof IBizEditView2Controller
     */
    public selectIndex = 0;

    /**
     * 默认激活下标页
     *
     * @memberof IBizEntityTabExpViewController
     */
    public defaultActiveTab = 0;

    /**
     * 所有tab选中项
     * 
     * @type {Array<any>}
     * @memberof IBizEntityTabExpViewController
     */
    public tabs: Array<any> = [];

    /**
     * tab选中项
     * 
     * @type {*}
     * @memberof IBizEntityTabExpViewController
     */
    public tabItem: any = {};

    /**
     * Creates an instance of IBizEntityTabExpViewController.
     * 创建 IBizEntityTabExpViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizEntityTabExpViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizEntityTabExpViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();
        this.regTabs();
    }

    /**
     * 视图加载
     * 
     * @memberof IBizEntityTabExpViewController
     */
    public onLoad(): void {
        super.onLoad();
        this.loadDefaultTab();
    }

    /**
     * 加载默认数据项
     * 
     * @memberof IBizEntityTabExpViewController
     */
    public loadDefaultTab(): void {
        if (this.tabs.length > 0) {
            const tab = this.tabs[0];
            const viewParams = this.getExpItemView({ viewid: tab.name });
            this.openView(viewParams.routepath, Object.assign(tab.viewParams, viewParams));
        }
    }

    /**
     * 选择卡变化
     * 
     * @param {*} [item={}] 
     * @memberof IBizEntityTabExpViewController
     */
    public changeTab(name: any = {}): void {
        if(name) {
            const viewParams = this.getExpItemView({ viewid: name });
            if(viewParams) {
                let index = this.tabs.findIndex((tab) => Object.is(tab.name, name));
                let param: any = {}
                if(index >= 0) {
                    param = this.tabs[index].viewParams;
                }
                this.openView(viewParams.routepath, Object.assign(param, viewParams));
            }
        }
    }

    /**
     * 注册Tab选择卡
     * 
     * @memberof IBizEntityTabExpViewController
     */
    public regTabs(): void {

    }

    /**
     * 保存Tab选项卡
     * 
     * @param {*} [tab={}] 
     * @memberof IBizEntityTabExpViewController
     */
    public regTab(tab: any = {}): void {
        this.tabs.push(tab);
    }
}

