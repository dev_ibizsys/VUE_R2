/**
 * 表格视图控制器 (部件视图)
 *
 * @class IBizGridView9Controller
 * @extends {IBizGridViewController}
 */
class IBizGridView9Controller extends IBizGridViewController {

    /**
     * 选中行数据
     * 
     * @type {*}
     * @memberof IBizGridView9Controller
     */
    public selectItem: any = {};

    /**
     * Creates an instance of IBizGridView9Controller.
     * 创建 IBizGridView9Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizGridView9Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图初始化，处理视图父数据，清除其他参数
     * 
     * @memberof IBizGridView9Controller
     */
    public onInited(): void {
        super.onInited();

        if (this.viewParam && this.viewParam.srfkey) {
            delete this.viewParam.srfkey;
        }
    }

    /**
     * 视图部件加载,不提供预加载方法
     * 
     * @param {*} [opt={}] 
     * @memberof IBizGridView9Controller
     */
    public load(opt: any = {}): void {

    }

    /**
     * 部件加载完成
     * 
     * @param {Array<any>} args 
     * @memberof IBizGridView9Controller
     */
    public onStoreLoad(args: Array<any>): void {
        super.onStoreLoad(args);
        if (args.length > 0) {
            setTimeout(() => {
                this.selectItem = {};
                Object.assign(this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
            }, 1);
        }
    }

    /**
     * 选中值变化
     * 
     * @param {Array<any>} args 
     * @memberof IBizGridView9Controller
     */
    public onSelectionChange(args: Array<any>): void {

        if (args.length > 0) {
            this.selectItem = {};
            Object.assign(this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
        }
        super.onSelectionChange(args);
    }

    /**
     * 获取UI操作参数
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizGridView9Controller
     */
    public getFrontUIActionParam(uiaction: any = {}, params: any = {}): any {
        let arg: any = {};
        let front_arg = super.getFrontUIActionParam(uiaction, params);
        Object.assign(arg, front_arg);
        Object.assign(arg, this.getViewParam());
        return arg;
    }

    /**
     * 实体界面行为参数
     *
     * @param {*} [uiaction={}]
     * @returns {*}
     * @memberof IBizGridView9Controller
     */
    public getBackendUIActionParam(uiaction: any = {}): any {
        let arg: any = {};
        let back_arg = super.getBackendUIActionParam(uiaction);
        Object.assign(arg, back_arg);
        Object.assign(arg, this.getViewParam());
        return arg;
    }
}
