/**
 * 工作流编辑视图控制器
 *
 * @class IBizWFEditViewController
 * @extends {IBizEditViewController}
 */
class IBizWFEditViewController extends IBizEditViewController {

    /**
     * Creates an instance of IBizWFEditViewController.
     * 创建 IBizWFEditViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizWFEditViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }
}

