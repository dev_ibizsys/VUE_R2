/**
 * 多数据编辑视图控制器对象
 *
 * @class IBizMEditView9Controller
 * @extends {IBizMDViewController}
 */
class IBizMEditView9Controller extends IBizMDViewController {

    /**
     * Creates an instance of IBizMEditView9Controller.
     * 创建 IBizMEditView9Controller 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizMEditView9Controller
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 初始化部件对象
     *
     * @memberof IBizMEditView9Controller
     */
    public onInitComponents(): void {
        super.onInitComponents();
        const meditviewpanel = this.getMDCtrl();
        if(meditviewpanel) {
            meditviewpanel.on(IBizMultiEditViewPanel.FINDEDITVIEWCONTROLLER).subscribe((data) => {
                
            });
            //数据发生改变
            meditviewpanel.on(IBizMultiEditViewPanel.EDITVIEWCHANGED).subscribe((data) => {
                this.onDataChanged(data);
            });
            //数据保存完成
            meditviewpanel.on(IBizMultiEditViewPanel.DATASAVED).subscribe((data) => {
                this.onDataSaved();
            });
        }
    }

    /**
     * 刷新
     *
     * @memberof IBizMEditView9Controller
     */
    public load(): void {
        
    }

    /**
     * 加载
     *
     * @memberof IBizMEditView9Controller
     */
    public onRefresh(): void {
        if(this.getMDCtrl) {
            this.getMDCtrl().refresh();
        }
    }

    /**
     * 获取多数据部件对象
     *
     * @returns {*}
     * @memberof IBizMEditView9Controller
     */
    public getMDCtrl(): any {
        return this.getControl('meditviewpanel');
    }

    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizMEditView9Controller
     */
    public isEnableQuickSearch(): boolean {
        return false;
    }

    /**
     * 父页面触发保存
     *
     * @memberof IBizMEditView9Controller
     */
    public saveData(): void {
        if(this.getMDCtrl() && this.getMDCtrl().isDirty()) {
            this.getMDCtrl().doSave();
            return;
        }
        this.onDataSaved();
    }

    /**
     * 数据发生改变
     *
     * @memberof IBizMEditView9Controller
     */
    public onDataChanged(data): void {
        this.$vue.$emit('drDataChanged', data);
    }

    /**
     * 数据保存完成
     *
     * @memberof IBizMEditView9Controller
     */
    public onDataSaved(): void {
        this.$vue.$emit('drDataSaved');
    }
}