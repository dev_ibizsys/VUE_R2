/**
 * Portal视图控制器
 *
 * @class IBizPortalViewController
 * @extends {IBizMainViewController}
 */
class IBizPortalViewController extends IBizMainViewController {

    /**
     * 门户部件
     *
     * @private
     * @type {Map<string, any>}
     * @memberof IBizPortalViewController
     */
    private portalCtrls: Map<string, any> = new Map();

    /**
     * Creates an instance of IBizPortalViewController.
     * 创建 IBizPortalViewController 实例
     * 
     * @param {*} [opts={}]
     * @memberof IBizPortalViewController
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * 视图初始化
     *
     * @memberof IBizPortalViewController
     */
    public onInit(): void {
        this.regPortalCtrls();
        super.onInit();
    }

    /**
     * 部件初始化
     * 
     * @memberof IBizPortalViewController
     */
    public onInitComponents(): void {
        super.onInitComponents();

        this.portalCtrls.forEach((ctrl: any) => {
            // 应用菜单选中
            ctrl.on(IBizAppMenu.MENUSELECTION).subscribe((data: any) => {
                this.onMenuSelection(data);
            });
        });
    }

    /**
     * 视图加载
     *
     * @memberof IBizPortalViewController
     */
    public onLoad(): void {
        super.onLoad();
        this.portalCtrls.forEach((ctrl: any) => {
            if (ctrl.load instanceof Function) {
                ctrl.load(this.viewParam);
            }
        });
    }

    /**
     * 注册所有Portal部件
     *
     * @memberof IBizPortalViewController
     */
    public regPortalCtrls(): void {

    }

    /**
     * 注册所有Portal部件
     * 
     * @param {string} name 
     * @param {*} ctrl 
     * @memberof IBizPortalViewController
     */
    public regPortalCtrl(name: string, ctrl: any): void {
        this.portalCtrls.set(name, ctrl);
    }

    /**
     * 菜单选中跳转路由
     *
     * @param {Array<any>} data
     * @memberof IBizPortalViewController
     */
    public onMenuSelection(data: Array<any>) {
        let item: any = {};
        Object.assign(item, data[0]);
        this.openView(item.routepath, item.openviewparam);
    }
}


