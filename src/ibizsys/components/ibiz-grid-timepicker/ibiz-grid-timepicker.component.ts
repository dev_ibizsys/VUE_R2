Vue.component('ibiz-grid-timepicker', {
    template: `
    <time-picker style="width: 100%;" type="time" :format="fmt" v-model="value" transfer @on-change="setValue"></time-picker>
    `,
    data: function() {
        return {
            value: '',
            fmt: '',
            rowData: null,
            column: null,
            grid: null
        }
    },
    created: function() {
        this.value = this.params.value;
        this.fmt = this.params.fmt;
    },
    methods: {
        getValue: function() {
            return this.value;
        },
        setValue: function(val) {
            this.grid.colValueChange(this.column.name, val, this.rowData);
        }
    }
});