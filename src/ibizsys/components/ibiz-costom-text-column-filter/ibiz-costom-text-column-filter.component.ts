Vue.component("costom-text-column-filter", {
    template: `
        <div class="ibiz-ag-filter text-filter">
            <input type="text" :value="value" @change="valueChanged($event)"/>
            <Icon :class="{selectClear: value != ''}" type="ios-close-circle" @click="clear"/>
        </div>
    `,
    data: function () {
        return {
            value: ""
        };
    },
    beforeMount() {

    },
    mounted() {},
    methods: {
        valueChanged(event) {
            this.value = event.target.value;
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        onParentModelChanged(parentModel) {
        	if(parentModel) {
        		this.value = !parentModel ? "" : parentModel.filter
        	} else {
                if(this.value) {
                    setTimeout(() => {
                        this.params.onFloatingFilterChanged({
                            model: this.buildModel()
                        });
                    }, 50)
                }
            }
        },
        clear() {
        	this.value = "";
        	this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        buildModel() {
            return {
                filterType: 'text',
                type: 'contains',
                filter: this.value
            };
        }
    }
})