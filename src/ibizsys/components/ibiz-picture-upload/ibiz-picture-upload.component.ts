Vue.component("ibiz-picture-upload", {
    template: `
    <div class="ibiz-picture-upload">
        <ul class="el-upload-list el-upload-list--picture-card">
            <template v-for="(file,index) of files">
                <li :key="index" class="el-upload-list__item is-success">
                    <img :src="file.url" class="el-upload-list__item-thumbnail" style="min-height:100px;min-width:100px;">
                    <a class="el-upload-list__item-name">
                        <i class="el-icon-document"></i> {{ file.name }}
                    </a>
                    <i class="el-icon-close"></i>
                    <label class="el-upload-list__item-status-label">
                        <i class="el-icon-upload-success el-icon-check"></i>
                    </label>
                    <span class="el-upload-list__item-actions">
                        <span class="el-upload-list__item-preview">
                            <i class="el-icon-zoom-in" @click="onPreview(file)"></i>
                        </span>
                        <span class="el-upload-list__item-download">
                            <i class="el-icon-download" @click="onDownload(file)"></i>
                        </span>
                        <span v-if="!field.disabled" class="el-upload-list__item-delete">
                            <i class="el-icon-delete" @click="onRemove(file)"></i>
                        </span>
                    </span>
                </li>
            </template>
        </ul>
        <el-upload v-if="!field.disabled" :show-file-list="false" list-type="picture-card" :file-list="files" :action="uploadUrl" :before-upload="beforeUpload" :on-success="onSuccess" :on-error="onError">
            <i class="el-icon-plus"></i>
        </el-upload>
        <Modal v-model="dialogVisible" footer-hide>
            <img width="100%" :src="dialogImageUrl" alt="">
        </Modal>
    <div>
    `,
    props: ['field', 'name'],
    data: function () {
        let data: any = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            dialogVisible: false,
            dialogImageUrl: '',
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        let form: any = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        let editorParams: any = {};
        let uploadparams: string = '';
        let exportparams: string = '';

        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }

        let upload_keys: Array<string> = uploadparams.split(';');
        let export_keys: Array<string> = exportparams.split(';');
        let custom_arr: Array<string> = [];
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach((name: string) => {
                if (editorParams.customparams[name]) {
                    custom_arr.push(`${name}=${editorParams.customparams[name]}`);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe((data: any) => {
            this.upload_arr = [];
            this.export_arr = [];
            upload_keys.forEach((key: string) => {
                let _field = form.findField(key);
                if (!_field) {
                    return;
                }
                let _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                this.upload_arr.push(`${key}=${_value}`);
            });
            export_keys.forEach((key: string) => {
                let _field = form.findField(key);
                if (!_field) {
                    return;
                }
                let _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                this.export_arr.push(`${key}=${_value}`);
            });

            let uploadUrl: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            let downloadUrl: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;

            if (this.upload_arr.length > 0 || this.custom_arr.length > 0) {
                uploadUrl = `${uploadUrl}?${this.upload_arr.join('&')}${this.upload_arr.length > 0 ? '&' : ''}${this.custom_arr.join('&')}`;
            }
            this.uploadUrl = uploadUrl;

            if (this.export_arr.length > 0 || this.custom_arr.length > 0) {
                downloadUrl = `${downloadUrl}?${this.export_arr.join('&')}${this.export_arr.length > 0 ? '&' : ''}${this.custom_arr.join('&')}`;
            }
            this.downloadUrl = downloadUrl;

            this.files.forEach((file: any) => {
                let _url: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = `${_url}?fileid=${file.id}`;
                if (this.export_arr.length > 0) {
                    _url = `${_url}&${this.export_arr.join('&')}`;
                }
                if (this.custom_arr.length > 0) {
                    _url = `${_url}&${this.custom_arr.join('&')}`;
                }
                file.url = _url;
            });
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach((file: any) => {
                    let _url: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = `${_url}?fileid=${file.id}`;
                    if (this.export_arr.length > 0) {
                        _url = `${_url}&${this.export_arr.join('&')}`;
                    }
                    if (this.custom_arr.length > 0) {
                        _url = `${_url}&${this.custom_arr.join('&')}`;
                    }
                    file.url = _url;
                });
            } else {
                this.files = [];
            }
        }
    },
    methods: {
        onPreview(file) {
            this.dialogImageUrl = file.url;
            this.dialogVisible = true;
        },
        beforeUpload(file) {
            console.log('上传之前');
        },
        onSuccess(response, file, fileList) {
            let arr: Array<any> = [];
            arr = [...response.files];
            this.files.forEach(f => {
                arr.push({ name: f.name, id: f.id });
            });
            let value: String = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onError(error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove(file) {
            if (this.field && this.field.disabled) {
                return;
            }
            let arr: Array<any> = [];
            this.files.forEach(f => {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            let value: String = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onDownload(file) {
            window.open(file.url);
        }
    }
});