Vue.component("costom-select-column-filter", {
    template: `
    	<div class="ibiz-ag-filter select-filter">
	        <select ref="select" @change="valueChanged">
	            <option style='display: none'></option>
	            <template v-for="item of items">
	            	<option :value="item.value">{{ item.text }}</option>
	            </template>
	        </select>
	        <Icon :class="{selectClear: value != ''}" type="ios-close-circle" @click="clear"/>
        </div>
    `,
    data: function () {
        return {
	    	value: '',
	        items: [],
	        http: IBizHttp.getInstance(),
	        grid: null
        };
    },
    beforeMount() {
    	var _this = this;
        this.grid = this.params.grid;
        if (Object.is(this.params.selectType, 'STATIC')) {
            var viewController = this.grid.getViewController();
            this.items = viewController.getCodeList(this.params.codelistId).data;
        }
        else if (Object.is(this.params.selectType, 'DYNAMIC')) {
            var param = {};
            this.http.post(this.params.url, param).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.items = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    mounted: function() {
    	
    },
    methods: {
        valueChanged(event) {
            this.value = event.target.value;
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        clear() {
        	this.value = "";
        	this.$refs.select.value = "";
        	this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        onParentModelChanged(parentModel) {
        	if(parentModel) {
        		this.value = !parentModel ? "" : parentModel.filter
        	} else {
                if(this.value) {
                    setTimeout(() => {
                        this.params.onFloatingFilterChanged({
                            model: this.buildModel()
                        });
                    }, 50)
                }
            }
        },
        buildModel() {
            return {
                filterType: 'text',
                type: 'contains',
                filter: this.value
            };
        }
    }
})