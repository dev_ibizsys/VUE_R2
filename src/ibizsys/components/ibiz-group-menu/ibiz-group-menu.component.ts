Vue.component('ibiz-group-menu', {
    template: `
        <div class="ibiz-group-menu">
            <row>
                <i-col span="24" class="first-level" v-for="first in menu.items" v-if="first.items && first.items.length > 0">
                    <div class="first-level-title">
                        <h2>
                            <span class="first-level-title-icon">
                                <i v-if="first.icon == ''" :class="[first.iconcls == '' ? '' : first.iconcls ]"></i>
                                <img v-else :src="first.icon"/>
                            </span>
                            <span class="first-level-title-text">{{first.text}}</span>
                        </h2>
                    </div>
                    <row>
	                    <i-col :xs="6" v-for="second in first.items" class="second-level">
		                    <div @click="openMenu(second)">
                                <div class="second-level-icon">
                                    <i v-if="second.icon == ''" :class="[second.iconcls == '' ? 'fa fa-cogs' : second.iconcls ]"></i>
                                    <img v-else :src="second.icon"/>
                                </div>
		                        <h4 class="second-level-text">{{second.text}}</h4>
		                    </div>
		                </i-col>
                    </row>
                </i-col>
            </row>
        </div>
    `,
    props: {
        menu: {
            type: Object
        },
        viewController: {
            type: Object
        }
    },
    data: function () {
        let data: any = {};
        return data;
    },
    mounted: function () {

    },
    methods: {
        // 打开菜单节点
        openMenu(second: any = {}) {
            if (this.menu) {
                this.menu.onSelectChange(second);
            }
        }
    }
});