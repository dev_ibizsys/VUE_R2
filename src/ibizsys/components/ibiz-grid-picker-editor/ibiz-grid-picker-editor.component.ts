Vue.component('ibiz-grid-picker-editor', {
    template: `
            <div class="ibiz-picker ibiz-grid-picker-editor">
            <el-autocomplete class="text-value" v-if="editorType != 'dropdown' && editorType != 'pickup-no-ac'" value-key="text" :disabled="disabled" v-model="value" size="small" :fetch-suggestions="onSearch" @select="onACSelect" @blur="onBlur" style="width:100%;">
                <template slot="suffix">
                    <i v-if="value != '' && !disabled" class="el-icon-circle-close" @click="onClear"></i>
                    <i v-if="editorType != 'ac'" class="el-icon-search"  @click="openView"></i>
                </template>
            </el-autocomplete>
            <el-input class="text-value" :value="value" v-if="editorType == 'pickup-no-ac'" readonly size="small" :disabled="disabled">
                <template slot="suffix">
                    <i v-if="value != '' && !disabled" class="el-icon-circle-close" @click="onClear"></i>
                    <i class="el-icon-search"  @click="openView"></i>
                </template>
            </el-input>
            <el-select v-if="editorType == 'dropdown'" remote :remote-method="onSearch" :value="value" size="small" filterable @change="onSelect" :disabled="disabled" style="width:100%;" clearable @clear="onClear" @visible-change="onSelectOpen">
                <el-option v-for="(item, index) of items" :value="item.value" :label="item.text" :disabled="item.disabled"></el-option>
            </el-select>
            <span v-if="editorType == 'dropdown'" style="position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;">
                <i v-if="!open" class="el-icon-arrow-down"></i>
                <i v-if="open" class="el-icon-arrow-up"></i>
            </span>
        </div>
    `,
    props: {
        itemvalue: {
            type: String
        },
        editorType: {
            type: String,
        },
        editorParams: {
            type: Object
        },
        name: {
            type: String,
        },
        text: {
            type: String,
        },
        disabled: {
            type: Boolean
        },
        placeHolder: {
            type: String
        },
        grid: {
            type: Object
        },
        data: {
            type: Object
        }
    },
    data: function () {
        let data: any = {
            iBizHttp: IBizHttp.getInstance(),
            iBizNotification: IBizNotification.getInstance(),
            value: '',
            open: false,
            items: []
        };
        data.value = this.itemvalue;
        Object.assign(data, this.editorParams);
        return data;
    },
    mounted: function () {
        if (this.editorType == 'dropdown') {
            this.onSearch('');
        }
    },
    watch: {
        'itemvalue': function (newVal, oldVal) {
            this.value = newVal;
        }
    },
    methods: {
        onSelectOpen(flag) {
            this.open = flag;
        },
        onBlur() {
            if (this.field && this.value != this.field.value) {
                if (this.forceSelection) {
                    this.value = this.field.value;
                } else {
                    this.onACSelect({ text: this.value, value: '' })
                }
            }
        },
        //  填充条件
        fillPickupCondition(arg: any): boolean {
            if (this.grid) {
                if (!this.data) {
                    this.iBizNotification.error('操作失败', '未能找到当前数据，无法继续操作');
                    return false;
                }
                if (this.itemParam && this.itemParam.fetchcond) {
                    let fetchparam = {};
                    let fetchCond = this.itemParam.fetchcond;
                    if (fetchCond) {
                        for (let cond in fetchCond) {
                            let value = this.data[fetchCond[cond]];
                            if (!value) {
                                this.iBizNotification.error('操作失败', '未能找到当前表格数据项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                Object.assign(arg, { srfreferitem: this.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.data) });
                return true;
            } else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        onSearch(query: any, func: any) {
            if (!this.url || Object.is(this.url, '')) {
                return;
            }
            let param: any = { srfaction: 'itemfetch', query: query };
            let bcancel = this.fillPickupCondition(param);

            if (!bcancel) {
                this.iBizNotification.warning({ title: '异常', desc: '条件不满足' });
                return;
            }
            this.iBizHttp.post(this.url, param).subscribe((data) => {
                this.items = data.items;
                if (typeof func == 'function') {
                    func(data.items);
                }
            })
        },
        onSelect(value: any) {
            let index = this.items.findIndex((item) => Object.is(item.value, value));
            if (index >= 0) {
                const item = this.items[index];
                this.onACSelect(item);
            }
        },
        // 清空选中数据
        onClear() {
            if (this.grid) {
                if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                    this.grid.colValueChange(this.valueItem, '', this.data);
                }
                this.grid.colValueChange(this.name, '', this.data);
            }
        },
        // 选中项设置
        onACSelect(item: any = {}) {
            if (this.grid) {
                if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                    this.grid.colValueChange(this.valueItem, item.value, this.data);
                }
                this.grid.colValueChange(this.name, item.text, this.data);
            }
        },
        // 打开选择视图
        openView() {
            if (this.disabled) {
                return;
            }

            let view = { viewparam: {} };
            let viewController: any = null;

            if (this.grid) {
                viewController = this.grid.getViewController();
            }

            if (this.data && Object.keys(this.data).length > 0) {
                Object.assign(view.viewparam, { srfkey: this.data['srfkey'] });
            }

            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }

            const bcancel: boolean = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }

            if (this.pickupView && Object.keys(this.pickupView).length > 0) {
                const subject: Subject<any> = new rxjs.Subject();
                Object.assign(view, this.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe((result: any) => {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    let item: any = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }

                    if (this.grid) {
                        if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                            this.grid.colValueChange(this.valueItem, item.srfkey, this.data);
                        }
                        this.grid.colValueChange(this.name, item.srfmajortext, this.data);
                    }
                })
            }
        }
    }
});