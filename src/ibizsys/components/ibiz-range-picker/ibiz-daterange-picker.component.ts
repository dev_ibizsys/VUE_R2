Vue.component('ibiz-range-picker', {
	template: `
	<div style="display: flex;">
		<template v-for="(formitem,index) of formItems">
			<div v-if="index > 0" style="text-align: center;width: 50px;">—</div>
			<template v-if="format">
				<date-picker v-if="format == 'yyyy-MM-dd HH:mm:ss'" type="datetime" :format="format"
					:value="formitem.value"
					:disabled="field.disabled" transfer
					@on-change="formitem.setValue($event)" style="flex-grow: 1;">
				</date-picker>
				<date-picker v-else-if="format == 'yyyy-MM-dd' || format == 'YYYY-MM-DD'" type="date" format="yyyy-MM-dd"
					:value="formitem.value"
					:disabled="field.disabled" transfer
					@on-change="formitem.setValue($event)" style="flex-grow: 1;">
				</date-picker>
				<time-picker v-else type="time" :format="format" :value="formitem.value" 
					:disabled="field.disabled" transfer 
					@on-change="formitem.setValue($event)">
				</time-picker>
			</template>
			<template v-else>
				<i-input v-model="formitem.value" :disabled="field.disabled"></i-input>
			</template>
		</template>
	</div>
	`,
	props: ['field', 'format', 'editorType', 'refFormItem'],
	data: function() {
		let data: any = {
			formItems: []
		};
		let formItems = [];
		Object.assign(data, { form: this.field.getForm() });
		formItems = this.refFormItem.split(';');
		formItems.forEach((item) => {
			let field = data.form.findField(item);
			if(field) {
				data.formItems.push(field);
			}
		})
		return data;
	}
})