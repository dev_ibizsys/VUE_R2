Vue.component('ibiz-grid-select-editor', {
    template: `
            <i-select :value="itemvalue" :placeholder="placeHolder" :disabled="disabled" clearable :transfer="true" @on-change="onChange($event)">
                <i-option v-for="(item, index) of codelist" :value="item.value" :key="item.value">{{ item.text }}</i-option>
            </i-select>
        `,
    props: {
        itemvalue: {
            type: String
        },
        codelist: {
            type: Array,
            default: []
        },
        codelisttype: {
            type: String,
        },
        url: {
            type: String,
        },
        name: {
            type: String,
        },
        text: {
            type: String,
        },
        disabled: {
            type: Boolean
        },
        placeHolder: {
            type: String
        },
        grid: {
            type: Object
        },
        data: {
            type: Object
        }
    },
    data: function () {
        let data: any = {};
        return data;
    },
    mounted: function () {
        if (Object.is(this.codelisttype, 'DYNAMIC')) {
            let iBizHttp: IBizHttp = IBizHttp.getInstance();
            iBizHttp.post(this.url, {}).subscribe((success: any) => {
                if (success.ret === 0) {
                    this.codelist = [...success.items];
                }
            }, (error: any) => {
                console.log(error);
            });
        }
    },
    methods: {
        onChange($event) {
            if (!this.grid) {
                return;
            }

            this.grid.colValueChange(this.name, $event, this.data);
            if (Object.is(this.codelisttype, 'DYNAMIC')) {
                const code: Array<any> = this.codelist.filter(item => Object.is(item.value, $event));
                const text: string = (code && code.length === 1) ? code[0].text : '';
                this.grid.colValueChange(this.text, text, this.data);
            }
        }
    }
});