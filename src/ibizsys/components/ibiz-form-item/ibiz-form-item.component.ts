Vue.component('ibiz-form-item', {
    template: `
        <div>
            <form-item :class="item.hasError ? 'ivu-form-item-error' :''" :label-width="item.showCaption ? item.labelWidth : 0" :required="!item.allowEmpty">
                <span slot="label" class="" v-if="item.labelWidth === 0 ? false : item.showCaption">{{ item.emptyCaption ? '' : item.caption }}</span>
                <slot></slot>
            </form-item>
        </div>
    `,
    props: ['form', 'item', 'name'],
    data: function () {
        let data: any = {};
        return data;
    }
});