Vue.component('ibiz-importdata-view', {
    template: `
        <div class="ibiz-import-data">
            <el-upload accept="application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ref="upload"
                :action="url" :on-change="handleChange" :file-list="fileList" :auto-upload="false" :show-file-list="false">
                <el-button slot="trigger" size="small" type="primary">选取文件</el-button>
                <el-button style="margin-left: 10px;" size="small" type="primary" @click="submitUpload">上传到服务器</el-button>
                <div class="down-data-template"><a :href="downUrl">下载导入数据模板</a></div>
            </el-upload>
            <div class="import-files">
                <ul class="el-upload-list el-upload-list--text">
                    <li class="el-upload-list__item is-ready" v-for="file in result">
                        <div>
                            <a class="el-upload-list__item-name">
                                <i class="el-icon-document"></i>{{file.name}}
                            </a>
                        </div>
                        <div class="file-info">
                            <a v-if="file.response.errorfileid && file.response.errorfileid != ''" class="errorLink">下载导入失败数据文件</a>
                            <div v-if="file.response.processinfo && file.response.processinfo != ''" v-html="file.response.processinfo"></div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    `,
    props: ['params'],
    data: function () {
        let data: any = {
            fileList: [],
            url: `/${IBizEnvironment.BaseUrl}${IBizEnvironment.UploadDEData}`,
            downUrl: `/${IBizEnvironment.BaseUrl}${IBizEnvironment.ExportExcel}`,
            result: []
        };
        if (this.params) {
            Object.assign(data, this.params);
        }
        data.url = !Object.is(data.dename, '') ? `${data.url}?srfdeid=${data.dename}` : data.url;
        data.downUrl = !Object.is(data.dename, '') ? `${data.downUrl}?srfdeid=${data.dename}` : data.downUrl;
        return data;
    },
    methods: {
        submitUpload() {
            this.$refs.upload.submit();
        },
        handleChange(file, fileList) {
            if (!file.response) {
                Object.assign(file, { response: {} });
            }
            this.result = [];
            setTimeout(() => {
                this.result = [...fileList];
            });

            fileList.some((item: any) => {
                if (item.response && item.response.ret === 0) {
                    this.$emit('dataChange', { ret: 'OK' });
                    return true;
                }
            });

        }
    }
});