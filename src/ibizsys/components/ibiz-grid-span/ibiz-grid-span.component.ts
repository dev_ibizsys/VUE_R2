Vue.component('ibiz-grid-span', {
    template: `
    <i-input :value="value" style="width: 100%" disabled readonly/>
    `,
    data: function() {
        return {
            value: ''
        }
    },
    created: function() {
        this.value = this.params.value;
    },
    methods: {
        getValue: function() {
            return this.value;
        }
    }
});