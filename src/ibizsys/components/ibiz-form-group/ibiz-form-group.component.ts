Vue.component('ibiz-form-group', {
    template: `
        <div>
            <template v-if="group.showCaption">
                <card :bordered="false" :dis-hover="true">
                    <p class="" slot="title"> {{ group.caption }}</p>
                    <row>
                        <slot></slot>
                    </row>
                </card>
            </template>
            <template v-else>
                <row>
                    <slot></slot>
                </row>
            </template>
        </div>
    `,
    props: ['form', 'group', 'name'],
    data: function () {
        let data: any = { };
        return data;
    }
});