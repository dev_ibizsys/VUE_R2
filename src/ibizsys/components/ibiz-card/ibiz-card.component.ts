Vue.component("ibiz-card", {
    template: `
    <div :class="classes">
        <div :class="headClasses" v-if="showHead">
            <slot name="title">
                <p v-if="title">
                    <Icon v-if="icon" :type="icon"></Icon>
                    <span>{{title}}</span>
                </p>
            </slot>
            <div :class="extraClasses" v-if="showExtra"><slot name="extra"></slot></div>
            <div style="clear: both;"></div>
        </div>
        <div :class="bodyClasses" :style="bodyStyles"><slot></slot></div>
    </div>
    `,
    props: {
        bordered: {
            type: Boolean,
            default: true
        },
        disHover: {
            type: Boolean,
            default: false
        },
        shadow: {
            type: Boolean,
            default: false
        },
        padding: {
            type: Number,
            default: 16
        },
        title: {
            type: String,
        },
        icon: {
            type: String,
        }
    },
    data: function() {
        return {
            showHead: true,
            showExtra: true,
            prefixCls: 'ivu-card'
        };
    },
    computed: {
        classes () {
            return [
                `${this.prefixCls}`,
                {
                    [`${this.prefixCls}-bordered`]: this.bordered && !this.shadow,
                    [`${this.prefixCls}-dis-hover`]: this.disHover || this.shadow,
                    [`${this.prefixCls}-shadow`]: this.shadow
                }
            ];
        },
        headClasses () {
            return `ibiz-card-head`;
        },
        extraClasses () {
            return `${this.prefixCls}-extra`;
        },
        bodyClasses () {
            return `${this.prefixCls}-body`;
        },
        bodyStyles () {
            if (this.padding !== 16) {
                return {
                    padding: `${this.padding}px`
                };
            } else {
                return '';
            }
        }
    },
    mounted () {
        this.showHead = this.title || this.$slots.title !== undefined;
        this.showExtra = this.$slots.extra !== undefined;
    }
});