Vue.component('ibiz-form-editor', {
    template: `
        <div class="ibiz-form-editor">
            <slot></slot>
            <div class="editor-tipinfo" :class="item.hasError ? 'show-tip-info': 'hide-tip-into'" >
                <tooltip max-width="200" transfer="true" theme="light">
                    <icon type="ios-warning-outline"></icon>
                    <div slot="content">
                        <p style="color:red;">{{item.errorInfo}}</p>
                    </div>
                </tooltip>
            </div>
        </div>
    `,
    props: ['item'],
    data: function () {
        let data: any = {};
        return data;
    }
});