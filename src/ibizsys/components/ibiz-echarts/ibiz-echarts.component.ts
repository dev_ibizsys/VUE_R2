Vue.component("ibiz-echarts", {
    template: `
    `,
    props: ['elementId', 'control'],
    data: function() {
        let data: any = {
            echartsObj: null
        };
        return data;
    },
    mounted: function() {
        let _this = this;
        _this.echartsObj = echarts.init(_this.$el);
        if(_this.control) {
            _this.control.on(IBizChart.LOADED).subscribe((data) => {
                let opt = _this.renderData(data)
                _this.echartsObj.setOption(opt);
                _this.echartsObj.resize();
            });
        } else {
            _this.echartsObj.setOption({});
            _this.echartsObj.resize();
        }
        window.onresize = function () {
            setTimeout(function() {
                _this.echartsObj.resize();
            },1);
		}
    },
    watch: {

    },
    methods: {
        renderData: function(data) {
            if (data.series) {

                //区域图分析
                if (data.series.length) {
                    if (Object.is(data.series[0].type, 'area')) {
    
                        //如果series是数组的话
                        for (let i in data.series) {
                            data.series[i].type = 'line';
                            data.series[i].areaStyle = {};
                            Object.assign(data.series[i].areaStyle, { normal: {} });
                        }
                        return data;
                    }
                } else {
                    if (Object.is(data.series.type, 'area')) {
                        data.series.type = 'line';
                        data.series.areaStyle = {};
                        Object.assign(data.series.areaStyle, { normal: {} });
                        return data;
                    }
                }
    
                //雷达图分析
                if (data.series.length) {
                    if (Object.is(data.series[0].type, 'radar')) {
    
                        //1.找到每个角的最大值
                        let max: number = 0;
    
                        //获得每个角的数据
                        let arrs: Array<any> = [];
                        for (let i in data.series) {
                            arrs.push(data.series[i].data);
                        }
                        const lastarrs: Array<any> = arrs[0].map(function (col, i) {
                            return arrs.map(function (row) {
                                return row[i];
                            })
                        });
                        let maxs = [];
                        for (let j in lastarrs) {
                            max = lastarrs[j][0];
                            for (let k in lastarrs[j]) {
                                if (max < lastarrs[j][k]) {
                                    max = lastarrs[j][k];
                                }
                            }
                            maxs.push(max);
                        }
    
                        // x轴数据转化成indicator数据
                        let indicatorArr: Array<any> = [];
                        for (let i in data.xAxis.data) {
                            for (let j in maxs) {
                                if (i === j) {
                                    indicatorArr.push({ name: data.xAxis.data[i], max: maxs[i] });
                                }
                            }
                        }
                        data.radar.indicator = [];
                        if (Array.isArray(indicatorArr)) {
                            data.radar.indicator = [...indicatorArr];
                        }
                        data.xAxis = null;
    
                        // 设置series的data数据
                        for (let i in data.series) {
                            const valueArray = data.series[i].data;
                            const name = data.series.name;
                            data.series[i].data = [];
                            data.series[i].data.push({ name: name, value: valueArray });
                        }
                    }
                }
                
            }
            return data;
        }
    }
});