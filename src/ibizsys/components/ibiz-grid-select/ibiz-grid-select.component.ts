Vue.component('ibiz-grid-select', {
    template: `
    <i-select v-model="value" style="width:100%" transfer label-in-value @on-change="onValueChange">
        <template v-for="(item, index) of items">
            <i-option :value="item.value" :key="index">{{ item.text }}</i-option>
        </template>
    </i-select>
    `,
    data: function() {
        return {
            value: '',
            text: '',
            items: [],
            http: IBizHttp.getInstance(),
            rowData: null,
            column: null,
            grid: null
        }
    },
    created: function() {
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        if(Object.is(this.params.selectType, 'STATIC')) {
            this.value = this.params.value;
            let viewController = this.grid.getViewController();
            this.items = viewController.getCodeList(this.params.codelistId).data;
        } else if(Object.is(this.params.selectType, 'DYNAMIC')) {
            this.value = this.rowData[this.column.name];
            this.text = this.params.value;
            let param: any = {};
            param.srfreferdata = JSON.stringify(this.rowData);
            this.http.post(this.params.url, param).subscribe((success: any) => {
                if (success.ret === 0) {
                    this.items = [...success.items];
                }
            }, (error: any) => {
                console.log(error);
            });
        }
    },
    methods: {
        getValue: function() {
            if(Object.is(this.params.selectType, 'DYNAMIC')) {
                return this.text;
            } else {
                return this.value;
            }
        },
        onValueChange: function(item) {
            if(Object.is(this.params.selectType, 'DYNAMIC')) {
                this.text = item.label;
            }
            this.grid.colValueChange(this.column.name, item.value, this.rowData);
        }
    }
});