Vue.component('ibiz-grid-input', {
    template: `
        <i-input v-model="value" placeholder="请输入" style="width: 100%" @on-change="onChange"/>
    `,
    data: function() {
        return {
            value: '',
            rowData: null,
            column: null,
            grid: null
        }
    },
    created: function() {
        this.value = this.params.value;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.grid = this.params.grid;
    },
    methods: {
        getValue: function() {
            return this.value;
        },
        onChange: function() {
            this.grid.colValueChange(this.column.name, this.value, this.rowData);
        }
    }
});