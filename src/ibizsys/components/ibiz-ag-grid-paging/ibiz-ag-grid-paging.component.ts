Vue.component('ibiz-ag-gird-paging', {
	template: `
		<page @on-change="onChange" @on-page-size-change="onPageSizeChange" :transfer="true" :total="grid.totalrow"
            show-sizer :page-size="20" :page-size-opts="[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]" show-elevator show-total>
            <span><span class="page-refresh"><i-button icon="md-refresh" title="刷新" @click="onRefresh"></i-button></span>显示 {{ (grid.curPage - 1)*grid.limit + 1}} - {{grid.totalrow > grid.curPage*grid.limit ? grid.curPage*grid.limit : grid.totalrow}} 条，共 {{grid.totalrow}} 条</span>
        </page>
	`,
	props: ['agGrid', 'grid'],
	data: function() {
		return {}
	},
	methods: {
		'onPageSizeChange': function(data) {
			this.$emit('on-page-size-change', data);
		},
		'onChange': function(data) {
			this.$emit('on-change', data);
		},
		'onRefresh': function() {
			this.$emit('refresh');
		}
	}
})