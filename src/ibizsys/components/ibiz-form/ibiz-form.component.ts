Vue.component('ibiz-form', {
    template: `
        <div style="height: 100%;">
            <i-form :model="form" style="height: 100%;">
                <row style="height: 100%;">
                    <slot :scope="fields"></slot>
                </row>
            </i-form>
        </div>
    `,
    props: ['form'],
    data: function () {
        let data: any = { fields: this.form.fields };
        return data;
    }
});