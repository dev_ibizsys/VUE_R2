Vue.component('ibiz-grid-picker', {
    template: `
    <div class="ibiz-picker">
        <el-autocomplete class="text-value" v-if="editorType != 'dropdown' && editorType != 'pickup-no-ac'" value-key="text" v-model="value" size="small" :fetch-suggestions="onSearch" @select="onACSelect" @blur="onBlur" style="width:100%;">
            <template slot="suffix">
                <i v-if="value != ''" class="el-icon-circle-close" @click="onClear"></i>
                <i v-if="editorType != 'ac'" class="el-icon-search"  @click="openView"></i>
            </template>
        </el-autocomplete>
        <el-input class="text-value" :value="value" v-if="editorType == 'pickup-no-ac'" readonly size="small">
            <template slot="suffix">
                <i v-if="value != ''" class="el-icon-circle-close" @click="onClear"></i>
                <i class="el-icon-search"  @click="openView"></i>
            </template>
        </el-input>
        <el-select v-if="editorType == 'dropdown'" remote :remote-method="onSearch" :value="value" size="small" filterable @change="onSelect" style="width:100%;" clearable @clear="onClear" @visible-change="onSelectOpen">
            <el-option v-for="(item, index) of items" :value="item.value" :label="item.text" :disabled="item.disabled"></el-option>
        </el-select>
        <span v-if="editorType == 'dropdown'" style="position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;">
            <i v-if="!open" class="el-icon-arrow-down"></i>
            <i v-if="open" class="el-icon-arrow-up"></i>
        </span>
    </div>
    `,
    data: function() {
        return {
            value: '',
            editorType: '',
            iBizHttp: IBizHttp.getInstance(),
            iBizNotification: IBizNotification.getInstance(),
            items: [],
            editorParams: null,
            rowData: null,
            column: null,
            grid: null
        }
    },
    created: function() {
        this.value = this.params.value;
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.editorType = this.params.editorType;
        this.editorParams = this.grid.getEditItem(this.column.name).editorParams;
    },
    methods: {
        getValue: function() {
            return this.value;
        },
        onSelectOpen(flag) {
            this.open = flag;
        },
        onBlur() {
            if (this.value != this.rowData[this.column.name]) {
                this.value = this.rowData[this.column.name];
            }
        },
        // 选中项设置
        onACSelect(item: any = {}) {
            if (this.grid) {
                if (this.editorParams && !Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                    this.grid.colValueChange(this.editorParams.valueItem, item.value, this.rowData);
                }
                this.grid.colValueChange(this.column.name, item.text, this.rowData);
            }
        },
        onSearch(query: any, func: any) {
            if (this.editorParams && (!this.editorParams.url || Object.is(this.editorParams.url, ''))) {
                return;
            }
            let param: any = { srfaction: 'itemfetch', query: query };
            let bcancel = this.fillPickupCondition(param);

            if (!bcancel) {
                this.iBizNotification.warning({ title: '异常', desc: '条件不满足' });
                return;
            }
            this.iBizHttp.post(this.editorParams.url, param).subscribe((data) => {
                this.items = data.items;
                if (typeof func == 'function') {
                    func(data.items);
                }
            })
        },
        onSelect(value: any) {
            let index = this.items.findIndex((item) => Object.is(item.value, value));
            if (index >= 0) {
                const item = this.items[index];
                this.onACSelect(item);
            }
        },
        // 清空选中数据
        onClear() {
            if (this.grid) {
                if (this.editorParams && !Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                    this.grid.colValueChange(this.editorParams.valueItem, '', this.rowData);
                }
                this.grid.colValueChange(this.column.name, '', this.rowData);
            }
        },
        //  填充条件
        fillPickupCondition(arg: any): boolean {
            if (this.grid) {
                if (this.editorParams && this.editorParams.itemParam && this.editorParams.itemParam.fetchcond) {
                    let fetchparam = {};
                    let fetchCond = this.editorParams.itemParam.fetchcond;
                    if (fetchCond) {
                        for (let cond in fetchCond) {
                            let value = this.rowData[fetchCond[cond]];
                            if (!value) {
                                this.iBizNotification.error('操作失败', '未能找到当前表格数据项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                Object.assign(arg, { srfreferitem: this.column.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.rowData) });
                return true;
            } else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        // 打开选择视图
        openView() {
            let view = { viewparam: {} };
            let viewController: any = null;

            if (this.grid) {
                viewController = this.grid.getViewController();
            }

            if (this.rowData && Object.keys(this.rowData).length > 0) {
                Object.assign(view.viewparam, { srfkey: this.rowData['srfkey'] });
            }

            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }

            const bcancel: boolean = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }

            if (this.editorParams &&  this.editorParams.pickupView && Object.keys(this.editorParams.pickupView).length > 0) {
                const subject: Subject<any> = new rxjs.Subject();
                Object.assign(view, this.editorParams.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe((result: any) => {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    let item: any = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }

                    if (this.grid) {
                        if (!Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                            this.grid.colValueChange(this.editorParams.valueItem, item.srfkey, this.rowData);
                        }
                        this.value = item.srfmajortext;
                        this.grid.colValueChange(this.column.name, item.srfmajortext, this.rowData);
                    }
                })
            }
        }
    }
});