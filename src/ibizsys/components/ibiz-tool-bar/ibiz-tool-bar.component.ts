Vue.component('ibiz-tool-bar', {
    template: `
    <div>
        <template v-for="item in toolbar.items">
            <template v-if="item.type == 'SEPERATOR'">
                <span class="toolbar-seperator">|</span>
            </template>
            <template v-else>
                <!--  带分组按钮：BEGIN  -->
                <template v-if="item.items && item.items.length > 0">
                    <template v-if="item._dataaccaction">
                        <dropdown trigger="click" transfer="true" @on-click="menuOnClick">
                            <i-button :title="!item.showCaption ? item.caption : ''" :disabled="item.disabled"
                                :class="item.class">
                                <i v-if="item.showIcon" :class="item.iconClass"></i>
                                <span v-if="item.showCaption">{{item.caption}}</span>
                                <icon type="ios-arrow-down"></icon>
                            </i-button>

                            <dropdown-menu slot="list">
                                <template v-for="submenu1 in item.items">
                                    <!--  带分组按钮：BEGIN  -->
                                    <template v-if="submenu1.items && submenu1.items.length > 0">
                                        <template v-if="submenu1._dataaccaction">
                                            <dropdown transfer="true">

                                                <dropdown-item :title="!submenu1.showCaption ? submenu1.caption : ''"
                                                    :disabled="submenu1.disabled" :class="submenu1.class">
                                                    <i v-if="submenu1.showIcon" :class="submenu1.iconClass"></i>
                                                    <span v-if="submenu1.showCaption">{{submenu1.caption}}</span>
                                                    <icon type="ios-arrow-forward"></icon>
                                                </dropdown-item>

                                                <dropdown-menu slot="list">
                                                    <template v-for="submenu2 in submenu1.items">
                                                        <template v-if="submenu2._dataaccaction">
                                                            <dropdown-item
                                                                :title="!submenu2.showCaption ? submenu2.caption : ''"
                                                                :disabled="submenu2.disabled" :class="submenu2.class"
                                                                :name="submenu2.name">
                                                                <i v-if="submenu2.showIcon" :class="submenu2.iconClass"></i>
                                                                <span
                                                                    v-if="submenu2.showCaption">{{submenu2.caption}}</span>
                                                            </dropdown-item>
                                                        </template>
                                                    </template>
                                                </dropdown-menu>
                                            </dropdown>
                                        </template>
                                    </template>
                                    <!--  带分组按钮：END  -->
                                    <!--  不带分组按钮：BEGIN  -->
                                    <template v-else>
                                        <template v-if="submenu1._dataaccaction">

                                            <template v-if="submenu1.uiaction.tag == 'ExportExcel'">
                                                <dropdown trigger="custom" transfer="true"
                                                    :visible="toolbar.exportMenuState" @on-clickoutside="onClickoutside"
                                                    placement="bottom-start" transfer="true"
                                                    transfer-class-name="second-transter-exportexcel-content">
                                                    <dropdown-item>
                                                        <span @click="toolbar.exportMenuState = !toolbar.exportMenuState">
                                                            <i v-if="submenu1.showIcon" :class="submenu1.iconClass"></i>
                                                            <span v-if="submenu1.showCaption">{{submenu1.caption}}</span>
                                                        </span>
                                                    </dropdown-item>

                                                    <dropdown-menu slot="list">
                                                        <dropdown-item>
                                                            <p @click="toolbar.itemExportExcel('ExportExcel', 'all')">
                                                                {{submenu1.caption}}全部(最大导出{{submenu1.MaxRowCount}}行)</p>
                                                        </dropdown-item>
                                                        <dropdown-item>
                                                            <p @click="toolbar.itemExportExcel('ExportExcel')">
                                                                {{submenu1.caption}}当前页</p>
                                                        </dropdown-item>
                                                        <dropdown-item>
                                                            {{submenu1.caption}}第
                                                            <i-input class="exportStartPage"
                                                                v-model="toolbar.exportStartPage" style="width: 30px;">
                                                            </i-input>
                                                            <i-input class="exportEndPage" v-model="toolbar.exportEndPage"
                                                                style="width: 30px;"></i-input>
                                                            <i-button
                                                                @click="toolbar.itemExportExcel('ExportExcel', 'custom')">
                                                                Go!</i-button>
                                                        </dropdown-item>
                                                    </dropdown-menu>
                                                </dropdown>

                                            </template>

                                            <template v-else>

                                                <dropdown-item :title="!submenu1.showCaption ? submenu1.caption : ''"
                                                    :disabled="submenu1.disabled" :class="submenu1.class"
                                                    :name="submenu1.name">
                                                    <i v-if="submenu1.showIcon" :class="submenu1.iconClass"></i>
                                                    <span v-if="submenu1.showCaption">{{submenu1.caption}}</span>
                                                </dropdown-item>
                                            </template>

                                        </template>
                                    </template>
                                    <!--  不带分组按钮：END  -->
                                </template>
                            </dropdown-menu>
                        </dropdown>
                    </template>
                </template>
                <!--  带分组按钮：END  -->
                <!--  不带分组按钮：BEGIN  -->
                <template v-else>
                    <template v-if="item._dataaccaction">

                        <template v-if="item.uiaction.tag == 'ExportExcel'">

                            <dropdown trigger="custom" transfer="true" :visible="toolbar.exportMenuState"
                                @on-clickoutside="onClickoutside">
                                <i-button :title="!item.showCaption ? item.caption : ''" :disabled="item.disabled"
                                    :class="item.class" @click="toolbar.exportMenuState = !toolbar.exportMenuState">
                                    <i v-if="item.showIcon" :class="item.iconClass"></i>
                                    <span v-if="item.showCaption">{{item.caption}}</span>
                                    <icon type="ios-arrow-down"></icon>
                                </i-button>

                                <dropdown-menu slot="list">
                                    <dropdown-item>
                                        <p @click="toolbar.itemExportExcel('ExportExcel', 'all')">
                                            {{item.caption}}全部(最大导出{{item.MaxRowCount}}行)</p>
                                    </dropdown-item>
                                    <dropdown-item>
                                        <p @click="toolbar.itemExportExcel('ExportExcel')">{{item.caption}}当前页</p>
                                    </dropdown-item>
                                    <dropdown-item>
                                        {{item.caption}}第
                                        <i-input class="exportStartPage" v-model="toolbar.exportStartPage"
                                            style="width: 30px;"></i-input>
                                        <i-input class="exportEndPage" v-model="toolbar.exportEndPage" style="width: 30px;">
                                        </i-input>
                                        <i-button @click="toolbar.itemExportExcel('ExportExcel', 'custom')">Go!</i-button>
                                    </dropdown-item>
                                </dropdown-menu>
                            </dropdown>

                        </template>

                        <template v-else>
                            <i-button :title="!item.showCaption ? item.caption : ''" :disabled="item.disabled"
                                :class="item.class" @click="toolbar.itemclick(item)">
                                <i v-if="item.showIcon" :class="item.iconClass"></i>
                                <span v-if="item.showCaption">{{item.caption}}</span>
                            </i-button>
                        </template>
                    </template>
                </template>
                <!--  不带分组按钮：END  -->
            </template>
        </template>
    </div>
    `,
    props: ['toolbar', 'viewController'],
    data: function () {
        var data = {};
        return data;
    },
    methods: {
        getItem(items: Array<any>, name: string) {
            let item: any = {};
            let _this = this;
            items.some((_item: any) => {
                if (Object.is(_item.name, name)) {
                    Object.assign(item, _item);
                    return true;
                }
                if (_item.items && _item.items.length > 0) {
                    let subItem = _this.getItem(_item.items, name);
                    if (Object.keys(subItem).length > 0) {
                        Object.assign(item, subItem);
                        return true;
                    }
                }
            });
            return item;
        },
        menuOnClick(name) {
            if (!this.toolbar) {
                return;
            }
            let item = this.getItem(this.toolbar.getItems(), name);
            if (Object.keys(item).length === 0) {
                return;
            }
            this.toolbar.itemclick(item);
        },
        onClickoutside($event) {
            if ($event && $event.target && $event.target.className.indexOf('ivu-input') !== -1) {
                this.toolbar.exportMenuState = true;
            } else {
                this.toolbar.exportMenuState = false;
            }
        }
    }
});