Vue.component('ibiz-exp-bar', {
    template: `
        <el-menu class="ibiz-exp-bar" @open="subMenuSelect" @close="subMenuSelect" @select="onSelect" :default-active="ctrl.selectItem.id" :default-openeds="ctrl.expandItems">
            <template v-for="(item0, index0) in ctrl.items">

                <!---  一级菜单有子项 begin  --->
                <template v-if="item0.items && item0.items.length > 0">
                    <el-submenu :index="item0.id" v-show="item0.show" :disabled="item0.disabled">
                        <template slot="title">
                            <img v-if="item0.icon && item0.icon != ''" :src="item0.icon"/>
                            <i v-else class="ibiz-menu-icon" :class="[item0.iconcls == '' ? 'el-icon-share' : item0.iconcls ]" aria-hidden="true"></i>
                            <span :class="ctrl.selectItem.id == item0.id ? 'active-subemnu': ''">{{ item0.text }}</span>
                            <span>&nbsp;&nbsp;<badge :count="item0.counterdata" :class-name="item0.class"></badge></span>
                        </template>
                        <template v-for="(item1, index1) in item0.items">

                            <!---  二级菜单有子项 begin  --->
                            <template v-if="item1.items && item1.items.length > 0">
                                <el-submenu :index="item1.id" v-show="item1.show" :disabled="item1.disabled">
                                    <template slot="title">
                                        <img v-if="item1.icon && item1.icon != ''" :src="item1.icon"/>
                                        <i v-else class="ibiz-menu-icon" v-if="item1.iconcls != ''" :class="item1.iconcls" aria-hidden="true"></i>
                                        <span :class="ctrl.selectItem.id == item1.id ? 'active-subemnu': ''">{{ item1.text }}</span>
                                        <span>&nbsp;&nbsp;<badge :count="item1.counterdata" :class-name="item1.class"></badge></span>
                                    </template>

                                    <!---  三级菜单 begin  --->
                                    <template v-for="(item2, index2) in item1.items">
                                        <el-menu-item :index="item2.id" v-show="item2.show" :disabled="item2.disabled">
                                            <img v-if="item2.icon && item2.icon != ''" :src="item2.icon"/>
                                            <i v-else class="ibiz-menu-icon" v-if="item2.iconcls != ''" :class="item2.iconcls" aria-hidden="true"></i>
                                            <span>{{ item2.text }}</span>
                                            <span>&nbsp;&nbsp;<badge :count="item2.counterdata" :class-name="item2.class"></badge></span>
                                        </el-menu-item>
                                    </template>
                                    <!---  三级菜单有 begin  --->

                                </el-submenu>
                            </template>
                            <!---  二级菜单有子项 end  --->

                            <!---  二级菜单无子项 begin  --->
                            <template v-else>
                                <el-menu-item :index="item1.id" v-show="item1.show" :disabled="item1.disabled">
                                    <img v-if="item1.icon && item1.icon != ''" :src="item1.icon"/>
                                    <i v-else class="ibiz-menu-icon" v-if="item1.iconcls != ''" :class="item1.iconcls" aria-hidden="true"></i>
                                    <span>{{ item1.text }}</span>
                                    <span>&nbsp;&nbsp;<badge :count="item1.counterdata" :class-name="item1.class"></badge></span>
                                </el-menu-item>
                            </template>
                            <!---  二级菜单无子项 end  --->

                        </template>
                    </el-submenu>
                </template>
                <!---  一级菜单有子项 end  --->

                <!---  一级菜单无子项 begin  --->
                <template v-else>
                    <el-menu-item :index="item0.id" v-show="item0.show" :disabled="item0.disabled">
                        <img v-if="item0.icon && item0.icon != ''" :src="item0.icon"/>
                        <i v-else class="ibiz-menu-icon" :class="[item0.iconcls == '' ? 'el-icon-share' : item0.iconcls ]" aria-hidden="true"></i>
                        <span>{{ item0.text }}</span>
                        <span>&nbsp;&nbsp;<badge :count="item0.counterdata" :class-name="item0.class"></badge></span>
                    </el-menu-item>
                </template>
                <!---  一级菜单无子项 end  --->

            </template>
        </el-menu>
    `,
    props: ['ctrl', 'viewController'],
    data: function () {
        let data: any = { opendata: [] };
        return data;
    },
    mounted: function () {
    },
    methods: {
        // 获取选中项数据
        getItem(items: Array<any>, id) {
            let _this = this;
            let data: any = {};
            items.some(_item => {
                if (Object.is(id, _item.id)) {
                    Object.assign(data, _item);
                    return true;
                }
                if (_item.items && _item.items.length > 0) {
                    let subItem = _this.getItem(_item.items, id);
                    if (Object.keys(subItem).length > 0) {
                        Object.assign(data, subItem);
                        return true;
                    }
                }
            });
            return data;
        },
        // 菜单项选中
        onSelect(name: string) {
            let _this = this;
            let _data = _this.getItem(_this.ctrl.items, name);
            _this.ctrl.selection(_data);
        },
        // 菜单节点选中
        subMenuSelect(index, indexpath) {
            let _this = this;
            let _data = _this.getItem(_this.ctrl.items, index);
            _this.ctrl.expandedAndSelectSubMenu(_data);
        }
    }
});