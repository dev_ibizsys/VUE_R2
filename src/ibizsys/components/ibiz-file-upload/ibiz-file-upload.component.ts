Vue.component("ibiz-file-upload", {
    template: `
    <el-upload :disabled="field.disabled" :file-list="files" :action="uploadUrl" :before-upload="beforeUpload" :on-success="onSuccess" :on-error="onError" :before-remove="onRemove" :on-preview="onDownload">
        <el-button size="small" icon="el-icon-upload">上传</el-button>
    </el-upload>
    `,
    props: ['field', 'name'],
    data: function () {
        let data: any = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        let form: any = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        let editorParams: any = {};
        let uploadparams: string = '';
        let exportparams: string = '';

        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }

        let upload_keys: Array<string> = uploadparams.split(';');
        let export_keys: Array<string> = exportparams.split(';');
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach((name: string) => {
                if (editorParams.customparams[name]) {
                    this.custom_arr.push(`${name}=${editorParams.customparams[name]}`);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe((data: any) => {
            this.upload_arr = [];
            this.export_arr = [];
            upload_keys.forEach((key: string) => {
                let _field = form.findField(key);
                if (!_field) {
                    return;
                }
                let _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                this.upload_arr.push(`${key}=${_value}`);
            });
            export_keys.forEach((key: string) => {
                let _field = form.findField(key);
                if (!_field) {
                    return;
                }
                let _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                this.export_arr.push(`${key}=${_value}`);
            });

            let uploadUrl: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            let downloadUrl: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;

            if (this.upload_arr.length > 0 || this.custom_arr.length > 0) {
                uploadUrl = `${uploadUrl}?${this.upload_arr.join('&')}${this.upload_arr.length > 0 ? '&' : ''}${this.custom_arr.join('&')}`;
            }
            this.uploadUrl = uploadUrl;

            if (this.export_arr.length > 0 || this.custom_arr.length > 0) {
                downloadUrl = `${downloadUrl}?${this.export_arr.join('&')}${this.export_arr.length > 0 ? '&' : ''}${this.custom_arr.join('&')}`;
            }
            this.downloadUrl = downloadUrl;

            this.files.forEach((file: any) => {
                let _url: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = `${_url}?fileid=${file.id}`;
                if (this.export_arr.length > 0) {
                    _url = `${_url}&${this.export_arr.join('&')}`;
                }
                if (this.custom_arr.length > 0) {
                    _url = `${_url}&${this.custom_arr.join('&')}`;
                }
                file.url = _url;
            });
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach((file: any) => {
                    let _url: string = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = `${_url}?fileid=${file.id}`;
                    if (this.export_arr.length > 0) {
                        _url = `${_url}&${this.export_arr.join('&')}`;
                    }
                    if (this.custom_arr.length > 0) {
                        _url = `${_url}&${this.custom_arr.join('&')}`;
                    }
                    file.url = _url;
                });
            } else {
                this.files = [];
            }
        }
    },
    methods: {
        beforeUpload(file) {
            console.log('上传之前');
        },
        onSuccess(response, file, fileList) {
            let arr: Array<any> = [];
            arr = [...response.files];
            this.files.forEach(f => {
                arr.push({ name: f.name, id: f.id });
            });
            let value: String = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onError(error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove(file, fileList) {
            let arr: Array<any> = [];
            fileList.forEach(f => {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            let value: String = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onDownload(file) {
            window.open(file.url);
        }
    }
});