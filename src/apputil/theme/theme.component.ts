Vue.component("ibiz-theme", {
    template: `
        <div>
            <Poptip title="主题" popper-class="ibiz-theme" placement="bottom-end">
                <a>
                    <Icon class="ibiz-theme-icon" type="md-settings"/>
                </a>
                <template slot="content">
                    <div class="ibiz-theme-color">
                        <template v-for="(theme,index) of defaultThemes">
                            <Tooltip :content="theme.title">
                                <div class="ibiz-theme-item" :class="{active: selectTheme==theme.tag}" :style="{background: theme.color}" @click="themeChange(theme.tag)"></div>
                            </Tooltip>
                        </template>
                        <template v-for="(theme,index) of themes">
                            <Tooltip :content="theme.title">
                                <div :key="index" class="ibiz-theme-item" :class="{active: selectTheme==theme.tag}" :style="{background: theme.color}" @click="themeChange(theme.tag)"></div>
                            </Tooltip>
                        </template>
                    </div>
                    <div>
                        <Form :label-width="60" label-position="left">
                            <FormItem label="字体">
                                <Select :value="selectFont" size="small" style="width:100px" @on-change="fontChange" transfer>
                                    <Option v-for="font in fontFamilys" :value="font.value" :key="font.value">{{ font.label }}</Option>
                                </Select>
                            </FormItem>
                        </Form>
                    </div>
                </template>
            </Poptip>
        </div>
    `,
    data: function(){
        let data: any = {
            selectTheme: '',
            defaultThemes: [
                {
                    tag: 'ibiz-default-theme',
                    title: 'light',
                    color: '#f6f6f6',
                },
                {
                    title: 'Blue',
                    tag: 'ibiz_theme_blue',
                    color: '#6ba1d1'
                },
                {
                    title: 'Dark Blue',
                    tag: 'ibiz_theme_darkblue',
                    color: '#606d80'
                }
            ],
            themes: IBizTheme,
            selectFont: '',
            fontFamilys: [
                {
                    label: '微软雅黑',
                    value: 'Microsoft YaHei',
                },
                {
                    label: '黑体',
                    value: 'SimHei',
                },
                {
                    label: '幼圆',
                    value: 'YouYuan',
                },
            ]
        };
        return data;
    },
    mounted: function() {
        let win: any = window;
        if(win.iBizApp) {
            this.selectTheme = win.iBizApp.getThemeClass();
            this.selectFont = win.iBizApp.getFontFamily();
        }
    },
    methods: {
        'themeChange': function(val) {
            if(!Object.is(this.activeTheme, val)) {
                this.selectTheme = val;
                this.$root.setThemeClass(val);
            }
        },
        'fontChange': function(val) {
            if(!Object.is(this.selectFont, val)) {
                this.selectFont = val;
                this.$root.setFontFamily(val);
            }
        }
    }
})