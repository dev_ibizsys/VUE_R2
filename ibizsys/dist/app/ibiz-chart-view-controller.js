"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据图表视图控制器
 *
 * @class IBizChartViewController
 * @extends {IBizSearchViewController}
 */
var IBizChartViewController = /** @class */ (function (_super) {
    __extends(IBizChartViewController, _super);
    /**
     * Creates an instance of IBizChartViewController.
     * 创建 IBizChartViewController 对象
     *
     * @param {*} [opts={}]
     * @memberof IBizChartViewController
     */
    function IBizChartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图部件初始化
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var chart = this.getChart();
        if (chart) {
            chart.on(IBizChart.LOADED).subscribe(function (data) {
                _this.onStoreLoad(data);
            });
            chart.on(IBizChart.BEFORELOAD).subscribe(function (data) {
                _this.onStoreBeforeLoad(data);
            });
            chart.on(IBizChart.DBLCLICK).subscribe(function (data) {
                _this.onDataActivated();
            });
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        if (!this.getSearchForm() && this.isLoadDefault()) {
            if (!this.isFreeLayout()) {
                if (this.getChart()) {
                    this.getChart().load();
                }
            }
            else {
                this.otherLoad();
            }
        }
    };
    /**
     * 获取图表部件
     *
     * @returns {*}
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.getChart = function () {
        return this.getControl('chart');
    };
    /**
     * 搜索表单触发加载
     *
     * @param {boolean} isload 是否加载
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onSearchFormSearched = function () {
        if (!this.isFreeLayout()) {
            if (this.getChart()) {
                this.getChart().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 表单重置完成
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onSearchFormReseted = function () {
        if (!this.isFreeLayout()) {
            if (this.getChart()) {
                this.getChart().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 视图部件刷新
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        if (!this.isFreeLayout()) {
            if (this.getChart()) {
                this.getChart().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 附加额外部件加载
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.otherLoad = function () {
        if (this.controls) {
            this.controls.forEach(function (obj, key) {
                if (obj instanceof IBizForm || obj instanceof IBizToolbar) {
                    return;
                }
                if (obj.load) {
                    obj.load();
                }
            });
        }
    };
    return IBizChartViewController;
}(IBizSearchViewController));
