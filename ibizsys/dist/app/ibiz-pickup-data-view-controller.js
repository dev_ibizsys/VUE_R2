"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据选择索引关系数据选择视图（部件视图）
 *
 * @class IBizPickupDataViewController
 * @extends {IBizDataViewController}
 */
var IBizPickupDataViewController = /** @class */ (function (_super) {
    __extends(IBizPickupDataViewController, _super);
    /**
     * Creates an instance of IBizPickupDataViewController.
     * 创建 IBizPickupDataViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupDataViewController
     */
    function IBizPickupDataViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizPickupDataViewController;
}(IBizDataViewController));
