"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 流程图
 *
 * @class IBizWFStepDataTraceChartViewController
 * @extends {IBizMDViewController}
 */
var IBizWFStepDataTraceChartViewController = /** @class */ (function (_super) {
    __extends(IBizWFStepDataTraceChartViewController, _super);
    /**
     *Creates an instance of IBizWFStepDataTraceChartViewController.
     * @param {*} [opts={}]
     * @memberof IBizWFStepDataTraceChartViewController
     */
    function IBizWFStepDataTraceChartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 实体名称
         *
         * @type {string}
         * @memberof IBizWFStepDataTraceChartViewController
         */
        _this.srfdeid = '';
        return _this;
    }
    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    IBizWFStepDataTraceChartViewController.prototype.getMDCtrl = function () {
        return undefined;
    };
    /**
     * 获取图片路径
     *
     * @returns {string}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    IBizWFStepDataTraceChartViewController.prototype.getImgPath = function () {
        return '';
    };
    return IBizWFStepDataTraceChartViewController;
}(IBizMDViewController));
