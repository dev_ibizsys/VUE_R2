"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 实体重定向视图
 *
 * @class IBizRedirectViewController
 * @extends {IBizMainViewController}
 */
var IBizRedirectViewController = /** @class */ (function (_super) {
    __extends(IBizRedirectViewController, _super);
    /**
     * Creates an instance of IBizRedirectViewController.
     * 创建 IBizRedirectViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizRedirectViewController
     */
    function IBizRedirectViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图加载
     *
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.loadRedirectData();
    };
    /**
     * 加载重新向数据
     *
     * @private
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.loadRedirectData = function () {
        var _this = this;
        var params = { srfaction: 'GETRDVIEWURL' };
        Object.assign(params, this.getViewParam());
        this.beforeLoadRedirectData(params);
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (success) {
            _this.afterLoadRedirectData(success);
            if (success.ret === 0) {
                if (success.rdview && !Object.is(success.rdview, '')) {
                    window.location = success.rdview;
                }
            }
        }, function (error) {
            _this.afterLoadRedirectData(error);
        });
    };
    /**
     * 加载重定向数据之前
     *
     * @param {*} [data={}]
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.beforeLoadRedirectData = function (data) {
        if (data === void 0) { data = {}; }
    };
    /**
     * 加载重定向数据之后
     *
     * @param {*} [data={}]
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.afterLoadRedirectData = function (data) {
        if (data === void 0) { data = {}; }
    };
    return IBizRedirectViewController;
}(IBizMainViewController));
