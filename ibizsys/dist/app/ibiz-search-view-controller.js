"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 搜索视图控制器
 *
 * @class IBizSearchViewController
 * @extends {IBizMainViewController}
 */
var IBizSearchViewController = /** @class */ (function (_super) {
    __extends(IBizSearchViewController, _super);
    /**
     * Creates an instance of IBizSearchViewController.
     * 创建 IBizSearchViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizSearchViewController
     */
    function IBizSearchViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 父数据改变
         *
         * @memberof IBizSearchViewController
         */
        _this.parentDataChanged = true;
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var searchform = this.getSearchForm();
        if (searchform) {
            // 表单加载完成
            searchform.on(IBizForm.FORMLOADED).subscribe(function (data) {
                if (_this.isLoadDefault()) {
                    _this.onSearchFormSearched();
                }
            });
            // 表单搜索，手动触发
            searchform.on(IBizSearchForm.FORMSEARCHED).subscribe(function (data) {
                _this.onSearchFormSearched();
            });
            // 表单重置
            searchform.on(IBizSearchForm.FORMRESETED).subscribe(function (data) {
                _this.onSearchFormReseted();
            });
            // 表单值变化
            searchform.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                if (!data) {
                    return;
                }
                var fieldname = data.name;
                _this.onSearchFormFieldChanged(fieldname, data.value, data);
            });
            // 设置表单是否开启
            searchform.setOpen(!this.isEnableQuickSearch());
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        var searchform = this.getSearchForm();
        if (searchform) {
            searchform.autoLoad({});
        }
    };
    /**
     * 获取搜索表单对象
     *
     * @returns {*}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.getSearchForm = function () {
        return this.getControl('searchform');
    };
    /**
     * 搜索表单属性值发生变化
     *
     * @param {string} fieldname
     * @param {string} field
     * @param {string} value
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSearchFormFieldChanged = function (fieldname, field, value) {
    };
    /**
     * 搜索表单重置
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSearchFormReseted = function () {
    };
    /**
     * 视图是否默认加载
     *
     * @returns {boolean}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.isLoadDefault = function () {
        return true;
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.isEnableQuickSearch = function () {
        return true;
    };
    /**
     * 获取搜索表单值
     *
     * @returns {*}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.getSearchCond = function () {
        if (this.getSearchForm()) {
            return this.getSearchForm().getValues();
        }
        return {};
    };
    /**
     * 搜索表单搜索执行
     *
     * @param {boolean} isload 是否加载
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSearchFormSearched = function () {
    };
    /**
     * 数据加载之前
     *
     * @param {*} [args={}]
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onStoreBeforeLoad = function (args) {
        if (args === void 0) { args = {}; }
        var fetchParam = {};
        if (this.getViewParam()) {
            Object.assign(fetchParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(fetchParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(fetchParam, this.getParentData());
        }
        if ((this.getSearchCond() && this.getSearchForm().isOpen()) || !this.isEnableQuickSearch()) {
            Object.assign(fetchParam, this.getSearchCond());
        }
        // 获取快速搜索里的搜索参数
        if (this.isEnableQuickSearch() && this.searchValue !== undefined) {
            args.search = this.searchValue;
        }
        Object.assign(args, fetchParam);
    };
    /**
     * 数据加载完成
     *
     * @param {*} data
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onStoreLoad = function (data) {
        this.parentDataChanged = false;
        this.reloadUICounters();
    };
    /**
     *设置父数据
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSetParentData = function () {
        _super.prototype.onSetParentData.call(this);
        this.parentDataChanged = true;
    };
    /**
     * 数据被激活<最典型的情况就是行双击>
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onDataActivated = function () {
    };
    /**
     * 搜索表单打开
     *
     * @returns {void}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.openSearchForm = function () {
        if (!this.isEnableQuickSearch()) {
            return;
        }
        var searchForm = this.getSearchForm();
        if (searchForm) {
            searchForm.setOpen(!searchForm.opened);
        }
    };
    /**
     * 搜索按钮执行搜索
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.btnSearch = function () {
        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched();
        }
    };
    /**
     * 清空快速搜索值
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.clearQuickSearchValue = function () {
        this.searchValue = undefined;
        this.onRefresh();
    };
    /**
     * 执行快速搜索
     *
     * @param {*} event
     * @returns {void}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onQuickSearch = function (event) {
        if (!event || event.keyCode !== 13) {
            return;
        }
        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched();
        }
    };
    return IBizSearchViewController;
}(IBizMainViewController));
