"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 实体分页导航视图控制器
 *
 * @export
 * @class IBizEntityIndexViewController
 * @extends {IBizEditViewController}
 */
var IBizEntityTabExpViewController = /** @class */ (function (_super) {
    __extends(IBizEntityTabExpViewController, _super);
    /**
     * Creates an instance of IBizEntityTabExpViewController.
     * 创建 IBizEntityTabExpViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEntityTabExpViewController
     */
    function IBizEntityTabExpViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 默认选中项下标
         *
         * @memberof IBizEditView2Controller
         */
        _this.selectIndex = 0;
        /**
         * 默认激活下标页
         *
         * @memberof IBizEntityTabExpViewController
         */
        _this.defaultActiveTab = 0;
        /**
         * 所有tab选中项
         *
         * @type {Array<any>}
         * @memberof IBizEntityTabExpViewController
         */
        _this.tabs = [];
        /**
         * tab选中项
         *
         * @type {*}
         * @memberof IBizEntityTabExpViewController
         */
        _this.tabItem = {};
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.onInitComponents = function () {
        _super.prototype.onInitComponents.call(this);
        this.regTabs();
    };
    /**
     * 视图加载
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.loadDefaultTab();
    };
    /**
     * 加载默认数据项
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.loadDefaultTab = function () {
        if (this.tabs.length > 0) {
            var tab = this.tabs[0];
            var viewParams = this.getExpItemView({ viewid: tab.name });
            this.openView(viewParams.routepath, Object.assign(tab.viewParams, viewParams));
        }
    };
    /**
     * 选择卡变化
     *
     * @param {*} [item={}]
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.changeTab = function (name) {
        if (name === void 0) { name = {}; }
        if (name) {
            var viewParams = this.getExpItemView({ viewid: name });
            if (viewParams) {
                var index = this.tabs.findIndex(function (tab) { return Object.is(tab.name, name); });
                var param = {};
                if (index >= 0) {
                    param = this.tabs[index].viewParams;
                }
                this.openView(viewParams.routepath, Object.assign(param, viewParams));
            }
        }
    };
    /**
     * 注册Tab选择卡
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.regTabs = function () {
    };
    /**
     * 保存Tab选项卡
     *
     * @param {*} [tab={}]
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.regTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        this.tabs.push(tab);
    };
    return IBizEntityTabExpViewController;
}(IBizExpViewController));
