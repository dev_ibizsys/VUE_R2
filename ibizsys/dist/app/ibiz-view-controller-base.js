"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 视图控制器基类
 *
 * @class IBizViewControllerBase
 * @extends {IBizObject}
 */
var IBizViewControllerBase = /** @class */ (function (_super) {
    __extends(IBizViewControllerBase, _super);
    /**
     * Creates an instance of IBizViewControllerBase.
     * 创建 IBizViewControllerBase 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizViewControllerBase
     */
    function IBizViewControllerBase(opts) {
        if (opts === void 0) { opts = {}; }
        var _this_1 = _super.call(this, opts) || this;
        /**
         * 计数器
         *
         * @type {Map<string, any>}
         * @memberof IBizViewControllerBase
         */
        _this_1.uicounters = new Map();
        /**
         * 关系数据
         *
         * @type {*}
         * @memberof IBizViewControllerBase
         */
        _this_1.srfReferData = {};
        /**
         * 视图控制器父对象数据
         *
         * @type {*}implements OnInit, OnDestroy, OnChanges
         * @memberof IBizViewController
         */
        _this_1.srfParentData = {};
        /**
         * 视图控制器父对象模型
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this_1.srfParentMode = {};
        /**
         * 视图控制器是否初始化
         *
         * @type {boolean}
         * @memberof IBizViewControllerBase
         */
        _this_1.bInited = false;
        /**
         * 视图使用模式
         *
         * @private
         * @type {number}
         * @memberof IBizViewControllerBase
         */
        _this_1.viewUsage = 1;
        /**
         * 视图控制器参数
         *
         * @type {*}
         * @memberof IBizViewControllerBase
         */
        _this_1.viewParam = {};
        /**
         * vue 路由对象
         *
         * @type {*}
         * @memberof IBizViewControllerBase
         */
        _this_1.$router = null;
        /**
         * vue 实例对象
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this_1.$vue = null;
        /**
         * vue 当前路由对象
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this_1.$route = null;
        /**
         * 当前路由所在位置下标
         *
         * @type {number}
         * @memberof IBizViewController
         */
        _this_1.route_index = -1;
        /**
         * 当前路由url
         *
         * @type {string}
         * @memberof IBizViewController
         */
        _this_1.route_url = '';
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.regSRFController(_this_1);
        }
        return _this_1;
    }
    ;
    /**
    * 初始化
    * 模拟vue生命周期
    *
    * @memberof IBizViewController
    */
    IBizViewControllerBase.prototype.VueOnInit = function (vue) {
        this.beforeVueOnInit(vue);
    };
    /**
     * 视图初始化之前
     *
     * @param {*} vue
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.beforeVueOnInit = function (vue) {
        this.$route = vue.$route;
        this.$router = vue.$router;
        this.$vue = vue;
        this.setViewUsage(this.$vue.viewUsage);
    };
    /**
     * 视图组件销毁时调用
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.onDestroy = function () {
        this.unRegUICounters();
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.unRegSRFController(this);
        }
    };
    /**
     * 注销计数器
     *
     * @returns {void}
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.unRegUICounters = function () {
        var _this_1 = this;
        if (Object.keys(this.uicounters).length == 0) {
            return;
        }
        var _nameArr = Object.keys(this.uicounters);
        _nameArr.forEach(function (name) {
            var _counter = _this_1.getUICounter(name);
            if (_counter) {
                _counter.close();
            }
        });
    };
    /**
     * 获取界面计数器
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.getUICounter = function (name) {
    };
    /**
     * 设置父模式
     *
     * @param {*} [data={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setParentMode = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentMode = {};
        var _data = {};
        Object.keys(data).forEach(function (name) {
            _data[name.toLowerCase()] = data[name];
        });
        Object.assign(this.srfParentMode, _data);
    };
    /**
     *  设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setParentData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentData = {};
        var _data = {};
        Object.keys(data).forEach(function (name) {
            _data[name.toLowerCase()] = data[name];
        });
        Object.assign(this.srfParentData, _data);
        this.onSetParentData();
        this.reloadUpdatePanels();
    };
    /**
     * 刷新全部界面更新面板
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.reloadUpdatePanels = function () {
    };
    /**
     * 设置父数据
     *
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.onSetParentData = function () {
    };
    /**
     * 设置关系数据
     *
     * @param {*} [data={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setReferData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfReferData = {};
        var _data = {};
        Object.keys(data).forEach(function (name) {
            _data[name.toLowerCase()] = data[name];
        });
        Object.assign(this.srfReferData, _data);
    };
    /**
     * 解析视图参数，初始化调用
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.parseViewParams = function () {
        var _this_1 = this;
        var parsms = {};
        if (this.getViewUsage() === IBizViewController.VIEWUSAGE_DEFAULT) {
            var _parsms = {};
            var iBizApp = IBizApp.getInstance();
            if (iBizApp) {
                var _index_1 = 0;
                var views = iBizApp.viewControllers;
                views.some(function (_view) {
                    if (Object.is(_view.getId(), _this_1.getId()) && Object.is(_view.getViewUsage(), _this_1.getViewUsage())) {
                        return true;
                    }
                    if (Object.is(_view.getViewUsage(), IBizViewControllerBase.VIEWUSAGE_DEFAULT)) {
                        _index_1 = _index_1 + 1;
                    }
                });
                this.route_index = _index_1;
            }
            var route_arr = this.$route.fullPath.split('/');
            var matched = this.$route.matched;
            var cur_route_name_1 = matched[this.route_index].name;
            var cur_route_index = route_arr.findIndex(function (_name) { return Object.is(_name, cur_route_name_1); });
            var route_url_index = cur_route_index + 1;
            if (matched[this.route_index + 1]) {
                var next_route_name_1 = matched[this.route_index + 1].name;
                var next_route_index = route_arr.findIndex(function (_name) { return Object.is(_name, next_route_name_1); });
                if (cur_route_index + 2 === next_route_index) {
                    var datas = decodeURIComponent(route_arr[cur_route_index + 1]);
                    Object.assign(_parsms, IBizUtil.matrixURLToJson(datas));
                    route_url_index = route_url_index + 1;
                }
            }
            else if (route_arr[cur_route_index + 1]) {
                var datas = decodeURIComponent(route_arr[cur_route_index + 1]);
                Object.assign(_parsms, IBizUtil.matrixURLToJson(datas));
                route_url_index = route_url_index + 1;
            }
            this.route_url = route_arr.slice(0, route_url_index).join('/');
            if (Object.keys(_parsms).length > 0) {
                Object.assign(parsms, _parsms);
            }
        }
        else if (this.getViewUsage() === IBizViewController.VIEWUSAGE_MODAL) {
            Object.assign(parsms, this.$vue.params);
            if (this.$vue.srfParentMode) {
                this.setParentMode(this.$vue.srfParentMode);
            }
            if (this.$vue.srfParentData) {
                this.setParentData(this.$vue.srfParentData);
            }
            if (this.$vue.srfReferData) {
                this.setReferData(this.$vue.srfReferData);
            }
        }
        else if (this.getViewUsage() === IBizViewController.VIEWUSAGE_EMBEDED) {
            Object.assign(parsms, this.$vue.params);
            if (this.$vue.srfParentMode) {
                this.setParentMode(this.$vue.srfParentMode);
            }
            if (this.$vue.srfParentData) {
                this.setParentData(this.$vue.srfParentData);
            }
            if (this.$vue.srfReferData) {
                this.setReferData(this.$vue.srfReferData);
            }
        }
        this.addViewParam(parsms);
    };
    /**
     * 处理组件参数
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.fullComponentParam = function (param) {
        if (param === void 0) { param = {}; }
        var _data = {};
        Object.keys(param).forEach(function (name) {
            _data[name.toLowerCase()] = param[name];
        });
        Object.assign(this.viewParam, _data);
    };
    /**
     * 添加视图参数, 处理视图刷新操作
     *
     * @param {*} [param={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.addViewParam = function (param) {
        if (param === void 0) { param = {}; }
        var _data = {};
        Object.keys(param).forEach(function (name) {
            _data[name.toLowerCase()] = param[name];
        });
        Object.assign(this.viewParam, _data);
    };
    /**
     * 数据加载
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.onLoad = function () {
    };
    /**
     * 是否初始化完毕
     *
     * @returns {boolean}
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.isInited = function () {
        return this.bInited;
    };
    /**
     * 设置视图的使用模式
     *
     * @private
     * @param {number} [viewUsage=0]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setViewUsage = function (viewUsage) {
        if (viewUsage === void 0) { viewUsage = 0; }
        this.viewUsage = viewUsage;
    };
    /**
     * 获取视图的使用模式
     *
     * @returns {number}
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.getViewUsage = function () {
        return this.viewUsage;
    };
    /**
     * 打开数据视图,模态框打开
     *
     * @param {*} [view={}]
     * @returns {Subject<any>}
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.openModal = function (view) {
        if (view === void 0) { view = {}; }
        var subject = new rxjs.Subject();
        var _view = {};
        Object.assign(_view, JSON.parse(JSON.stringify(view)), { subject: subject });
        this.$vue.$root.addModal(_view);
        return subject;
    };
    /**
     * 关闭模态框
     *
     * @param {*} [result]
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.closeModal = function (result) {
        var _this = this;
        _this.$vue.$emit('close', result);
    };
    /**
     * 打开视图;打开方式,路由打开
     *
     * @param {string} routeString 相对路由地址
     * @param {*} [routeParam={}] 激活路由参数
     * @param {*} [queryParams] 路由全局查询参数
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.openView = function (routeString, routeParam, queryParams) {
        if (routeParam === void 0) { routeParam = {}; }
        if (this.getViewUsage() !== IBizViewController.VIEWUSAGE_DEFAULT) {
            return;
        }
        var param_arr = [];
        Object.keys(routeParam).forEach(function (name) {
            if (routeParam[name] && !Object.is(routeParam[name], '')) {
                param_arr.push(name + "=" + routeParam[name]);
            }
        });
        var url = this.route_url + "/" + routeString;
        if (param_arr.length > 0) {
            url = url + "/" + param_arr.join(';');
        }
        this.$router.push({ path: url, query: queryParams });
    };
    /**
     * 打开新窗口
     *
     * @param {string} viewurl
     * @param {*} [parsms={}]
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.openWindow = function (viewurl, parsms) {
        if (parsms === void 0) { parsms = {}; }
        var url = "/" + IBizEnvironment.BaseUrl + "/" + IBizEnvironment.AppName.toLowerCase() + viewurl;
        var parsms_arr = [];
        Object.keys(parsms).forEach(function (name) {
            if (parsms[name] && !Object.is(parsms[name], '')) {
                parsms_arr.push(name + "=" + parsms[name]);
            }
        });
        if (parsms_arr.length > 0) {
            url = url + "/" + parsms_arr.join(';');
        }
        var win = window;
        win.open(url, '_blank');
    };
    /**
     * 数据变化
     *
     * @param {*} [data]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.dataChange = function (data) {
        var _this = this;
        _this.$vue.$emit('dataChange', data);
    };
    /**
     * 视图使用模式，默认
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewControllerBase.VIEWUSAGE_DEFAULT = 1;
    /**
     * 视图使用模式，模式弹出
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewControllerBase.VIEWUSAGE_MODAL = 2;
    /**
     * 视图使用模式，嵌入
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewControllerBase.VIEWUSAGE_EMBEDED = 4;
    return IBizViewControllerBase;
}(IBizObject));
