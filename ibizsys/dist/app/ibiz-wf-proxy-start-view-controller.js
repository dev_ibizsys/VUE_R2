"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流启动视图
 *
 * @class IBizWFProxyStartViewController
 * @extends {IBizMainViewController}
 */
var IBizWFProxyStartViewController = /** @class */ (function (_super) {
    __extends(IBizWFProxyStartViewController, _super);
    /**
     * Creates an instance of IBizWFProxyStartViewController.
     * 创建 IBizWFProxyStartViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFProxyStartViewController
     */
    function IBizWFProxyStartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 代理应用启动视图url
         *
         * @type {string}
         * @memberof IBizWFProxyStartViewController
         */
        _this.startviewurl = '';
        return _this;
    }
    /**
     * 视图模型加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyStartViewController
     */
    IBizWFProxyStartViewController.prototype.afterLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.afterLoadMode.call(this, data);
        this.startviewurl = data.startviewurl;
    };
    return IBizWFProxyStartViewController;
}(IBizMainViewController));
