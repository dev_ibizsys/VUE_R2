"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 实体选项操作视图
 *
 * @class IBizOptionViewController
 * @extends {IBizEditViewController}
 */
var IBizOptionViewController = /** @class */ (function (_super) {
    __extends(IBizOptionViewController, _super);
    /**
     * Creates an instance of IBizOptionViewController.
     * 创建 IBizOptionViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizOptionViewController
     */
    function IBizOptionViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 确定
     *
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onClickOkButton = function () {
        this.doSaveAndExit();
    };
    /**
     * 关闭串口
     *
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onClickCancelButton = function () {
        // var me = this;
        // var window = me.window;
        // if(window){
        // 	window.dialogResult = 'cancel';
        // }
        // me.closeWindow();
        // this.closeView();
        this.viewParam = {};
        this.closeWindow();
    };
    /**
     * 视图加载
     *
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onLoad = function () {
        var editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    };
    /**
     * 表单保存完成
     *
     * @returns {void}
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onFormSaved = function (data) {
        // var me = this;
        // var window = me.window;
        // if (window) {
        // 	window.dialogResult = 'ok';
        // 	window.activeData = me.getForm().getValues();
        // 	window.selectedData = window.activeData
        // }
        // me.closeWindow();
        this.refreshReferView();
        // this.closeView();
        this.viewParam = {};
        if (data.url && !Object.is(data.url, '')) {
            this.addViewParam({ ru: data.url });
        }
        this.closeWindow();
        return;
    };
    return IBizOptionViewController;
}(IBizEditViewController));
