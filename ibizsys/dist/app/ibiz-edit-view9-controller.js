"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 嵌入编辑视图控制器
 *
 * @class IBizEditView9Controller
 * @extends {IBizEditViewController}
 */
var IBizEditView9Controller = /** @class */ (function (_super) {
    __extends(IBizEditView9Controller, _super);
    /**
     * Creates an instance of IBizEditView9Controller.
     * 创建 IBizEditView9Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditView9Controller
     */
    function IBizEditView9Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图刷新
     *
     * @memberof IBizEditView9Controller
     */
    IBizEditView9Controller.prototype.onRefresh = function () {
        var editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    };
    /**
     * 视图参数变化，嵌入表单，手动刷新数据
     *
     * @param {*} change
     * @memberof IBizEditView9Controller
     */
    IBizEditView9Controller.prototype.viewParamChange = function (change) {
        if (change.srfparentkey && !Object.is(change.srfparentkey, '')) {
            this.addViewParam({ srfkey: change.srfparentkey });
            this.onRefresh();
        }
    };
    return IBizEditView9Controller;
}(IBizEditViewController));
