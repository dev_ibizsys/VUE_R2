"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格视图控制器 嵌入
 *
 * @class IBizGridView2Controller
 * @extends {IBizGridViewController}
 */
var IBizGridView2Controller = /** @class */ (function (_super) {
    __extends(IBizGridView2Controller, _super);
    /**
     * Creates an instance of IBizGridView2Controller.
     * 创建 IBizGridView2Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGridView2Controller
     */
    function IBizGridView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 选中行数据
         *
         * @type {*}
         * @memberof IBizGridView2Controller
         */
        _this.selectItem = {};
        /**
         * 排序字段名称
         *
         * @memberof IBizGridView2Controller
         */
        _this.sortFieldName = '关键字';
        /**
         * 排序字段属性文本值
         *
         * @memberof IBizGridView2Controller
         */
        _this.sortField = '';
        /**
         * 属性字段排序方向
         *
         * @type {string}
         * @memberof IBizGridView2Controller
         */
        _this.sortDirection = 'desc';
        /**
         * 视图显示数据类型
         *
         * @memberof IBizGridView2Controller
         */
        _this.gridType = 'listgrid';
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.onInitComponents = function () {
        _super.prototype.onInitComponents.call(this);
        var listgrid = this.getControl('listgrid');
        if (listgrid) {
            // listgrid.on(IBizEvent.IBizMDControl_SELECTIONCHANGE, (args) => {
            //     this.onSelectionChange(args);
            // });
            // listgrid.on(IBizEvent.IBizMDControl_BEFORELOAD, (args) => {
            //     this.onStoreBeforeLoad(args);
            // });
            // listgrid.on(IBizEvent.IBizMDControl_LOADED, (args) => {
            //     this.onStoreLoad(args);
            // });
            // listgrid.on(IBizEvent.IBizMDControl_CHANGEEDITSTATE, (args) => {
            //     this.onGridRowEditChange(undefined, args, undefined);
            // });
            // listgrid.on(IBizEvent.IBizDataGrid_ROWDBLCLICK, (args) => {
            //     this.onDataActivated(args);
            // });
        }
    };
    /**
     * 视图部件加载
     *
     * @param {*} [opt={}]
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.load = function (opt) {
        if (opt === void 0) { opt = {}; }
        _super.prototype.load.call(this, opt);
        var listgrid = this.getControl('listgrid');
        if (listgrid) {
            listgrid.load(opt);
        }
    };
    /**
     * 部件加载完成
     *
     * @param {Array<any>} args
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.onStoreLoad = function (args) {
        var _this = this;
        _super.prototype.onStoreLoad.call(this, args);
        if (args.length > 0) {
            setTimeout(function () {
                _this.selectItem = {};
                Object.assign(_this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
            }, 1);
        }
    };
    /**
     * 选中值变化
     *
     * @param {any} selection
     * @memberof IBizMDViewController
     */
    IBizGridView2Controller.prototype.onSelectionChange = function (args) {
        if (args.length > 0) {
            this.selectItem = {};
            Object.assign(this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
        }
        _super.prototype.onSelectionChange.call(this, args);
    };
    /**
     * 数据列表排序
     *
     * @param {string} field
     * @param {string} name
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.dataListSort = function (field, name) {
        if (Object.is(this.sortField, field)) {
            return;
        }
        this.sortField = field;
        this.sortFieldName = name;
        if (Object.is(this.sortField, '')) {
            return;
        }
        var sort = [];
        sort.push({ property: this.sortField, direction: this.sortDirection });
        var listgrid = this.getControl('listgrid');
        listgrid.refresh({ sort: JSON.stringify(sort) });
    };
    /**
     * 视图类型切换
     *
     * @param {string} type
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.viewTypeChange = function (type) {
        this.gridType = type;
    };
    /**
     * 数据列表字段排序
     *
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.dataListFiledSort = function () {
        if (Object.is(this.sortField, '')) {
            return;
        }
        if (Object.is(this.sortDirection, 'desc')) {
            this.sortDirection = 'asc';
        }
        else {
            this.sortDirection = 'desc';
        }
        var sort = [];
        sort.push({ property: this.sortField, direction: this.sortDirection });
        var listgrid = this.getControl('listgrid');
        listgrid.refresh({ sort: JSON.stringify(sort) });
    };
    return IBizGridView2Controller;
}(IBizGridViewController));
