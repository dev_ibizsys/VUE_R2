"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流左右关系编辑视图
 *
 * @class IBizWFEditView2Controller
 * @extends {IBizEditView2Controller}
 */
var IBizWFEditView2Controller = /** @class */ (function (_super) {
    __extends(IBizWFEditView2Controller, _super);
    /**
     * Creates an instance of IBizWFEditView2Controller.
     * 创建 IBizWFEditView2Controller  实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFEditView2Controller
     */
    function IBizWFEditView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizWFEditView2Controller;
}(IBizEditView2Controller));
