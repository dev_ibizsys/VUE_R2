"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 自定义视图控制器对象
 *
 * @class IBizCustomViewController
 * @extends {IBizMainViewController}
 */
var IBizCustomViewController = /** @class */ (function (_super) {
    __extends(IBizCustomViewController, _super);
    /**
     * Creates an instance of IBizCustomViewController.
     * 创建 IBizCustomViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizCustomViewController
     */
    function IBizCustomViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 加载部件
     *
     * @param {*} [opt={}]
     * @memberof IBizMDViewController
     */
    IBizCustomViewController.prototype.onLoad = function (opt) {
        if (opt === void 0) { opt = {}; }
        if (this.controls) {
            this.controls.forEach(function (obj, key) {
                if (obj instanceof IBizToolbar) {
                    return;
                }
                if (obj.load) {
                    obj.load(opt);
                }
            });
        }
    };
    return IBizCustomViewController;
}(IBizMainViewController));
