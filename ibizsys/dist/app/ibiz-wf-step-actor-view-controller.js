"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 流程步骤视图
 *
 * @class IBizWFStepActorViewController
 * @extends {IBizMDViewController}
 */
var IBizWFStepActorViewController = /** @class */ (function (_super) {
    __extends(IBizWFStepActorViewController, _super);
    /**
     *Creates an instance of IBizWFStepActorViewController.
     * @param {*} [opts={}]
     * @memberof IBizWFStepActorViewController
     */
    function IBizWFStepActorViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 流程步骤对象
         *
         * @type {*}
         * @memberof IBizWFStepActorViewController
         */
        _this.wfstepactor = null;
        _this.wfstepactor = new IBizTimeline({
            name: 'grid',
            url: opts.backendurl,
            viewController: _this,
            $route: opts.$route,
            $router: opts.$router,
            $vue: opts.$vue
        });
        _this.controls.set('wfstepactor', _this.wfstepactor);
        return _this;
    }
    /**
     * 初始化面板
     *
     * @memberof IBizWFStepActorViewController
     */
    IBizWFStepActorViewController.prototype.onInitComponents = function () {
        _super.prototype.onInitComponents.call(this);
    };
    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof IBizWFStepActorViewController
     */
    IBizWFStepActorViewController.prototype.getMDCtrl = function () {
        return this.getControl('wfstepactor');
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizWFStepActorViewController
     */
    IBizWFStepActorViewController.prototype.isEnableQuickSearch = function () {
        return false;
    };
    return IBizWFStepActorViewController;
}(IBizMDViewController));
