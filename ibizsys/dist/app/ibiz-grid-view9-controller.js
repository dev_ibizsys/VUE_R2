"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格视图控制器 (部件视图)
 *
 * @class IBizGridView9Controller
 * @extends {IBizGridViewController}
 */
var IBizGridView9Controller = /** @class */ (function (_super) {
    __extends(IBizGridView9Controller, _super);
    /**
     * Creates an instance of IBizGridView9Controller.
     * 创建 IBizGridView9Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGridView9Controller
     */
    function IBizGridView9Controller(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 选中行数据
         *
         * @type {*}
         * @memberof IBizGridView9Controller
         */
        _this.selectItem = {};
        return _this;
    }
    /**
     * 视图初始化，处理视图父数据，清除其他参数
     *
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.onInited = function () {
        _super.prototype.onInited.call(this);
        if (this.viewParam && this.viewParam.srfkey) {
            delete this.viewParam.srfkey;
        }
    };
    /**
     * 视图部件加载,不提供预加载方法
     *
     * @param {*} [opt={}]
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.load = function (opt) {
        if (opt === void 0) { opt = {}; }
    };
    /**
     * 部件加载完成
     *
     * @param {Array<any>} args
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.onStoreLoad = function (args) {
        var _this = this;
        _super.prototype.onStoreLoad.call(this, args);
        if (args.length > 0) {
            setTimeout(function () {
                _this.selectItem = {};
                Object.assign(_this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
            }, 1);
        }
    };
    /**
     * 选中值变化
     *
     * @param {Array<any>} args
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.onSelectionChange = function (args) {
        if (args.length > 0) {
            this.selectItem = {};
            Object.assign(this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
        }
        _super.prototype.onSelectionChange.call(this, args);
    };
    /**
     * 获取UI操作参数
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.getFrontUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        var front_arg = _super.prototype.getFrontUIActionParam.call(this, uiaction, params);
        Object.assign(arg, front_arg);
        Object.assign(arg, this.getViewParam());
        return arg;
    };
    /**
     * 实体界面行为参数
     *
     * @param {*} [uiaction={}]
     * @returns {*}
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.getBackendUIActionParam = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        var arg = {};
        var back_arg = _super.prototype.getBackendUIActionParam.call(this, uiaction);
        Object.assign(arg, back_arg);
        Object.assign(arg, this.getViewParam());
        return arg;
    };
    return IBizGridView9Controller;
}(IBizGridViewController));
