"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 左右编辑视图控制器
 *
 * @export
 * @class IBizEditView2Controller
 * @extends {IBizEditViewController}
 */
var IBizEditView2Controller = /** @class */ (function (_super) {
    __extends(IBizEditView2Controller, _super);
    /**
     * Creates an instance of IBizEditView2Controller.
     * 创建IBizEditView2Controller实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditView2Controller
     */
    function IBizEditView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化部件
     *
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var drBar = this.getDRBar();
        if (drBar) {
            // 关系数据部件加载完成
            drBar.on(IBizDRBar.DRBARLOADED).subscribe(function (item) {
                _this.onDRBarLoaded(item);
            });
            // 关系部件选中变化
            drBar.on(IBizDRBar.DRBARSELECTCHANGE).subscribe(function (item) {
                _this.onDRBarItemChangeSelect(item);
            });
        }
    };
    /**
     * 视图加载,只触发关系栏加载
     *
     * @returns {void}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onLoad = function () {
        var drBar = this.getDRBar();
        if (drBar) {
            drBar.load();
        }
    };
    /**
     * 关系数据项加载完成，触发视图加载
     *
     * @private
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onDRBarLoaded = function (items) {
        if (!this.getForm()) {
            this.iBizNotification.error('错误', '表单不存在!');
        }
        _super.prototype.onLoad.call(this);
    };
    /**
     * 表单部件加载完成
     *
     * @param {any} params
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onFormLoaded = function () {
        _super.prototype.onFormLoaded.call(this);
        var drBar = this.getDRBar();
        var form = this.getForm();
        var _field = form.findField('srfkey');
        var _srfuf = form.findField('srfuf');
        var matched = this.$route.matched;
        var formState = (Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '')) ? 'new' : 'edit';
        drBar.setDRBarItemState(drBar.getItems(), Object.is(formState, 'new') ? true : false);
        drBar.setSelectItem({ id: 'form' });
        if (this.isHideEditForm()) {
            if (Object.is(formState, 'new')) {
                this.iBizNotification.warning('警告', '新建模式，表单主数据不存在，该表单已被配置隐藏');
                return;
            }
        }
        if (matched[this.route_index + 1]) {
            var nextItem = drBar.getItem(drBar.getItems(), { routepath: matched[this.route_index + 1].name });
            if (nextItem && Object.keys(nextItem).length) {
                this.isShowToolBar = false;
                drBar.setSelectItem(nextItem);
            }
        }
    };
    /**
     * 表单保存完成
     *
     * @param {*} result
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onFormSaved = function (result) {
        _super.prototype.onFormSaved.call(this, result);
        var drBar = this.getDRBar();
        drBar.setDRBarItemState(drBar.getItems(), false);
    };
    /**
     * 是否影藏编辑表单
     *
     * @returns {boolean}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.isHideEditForm = function () {
        return false;
    };
    /**
     * 关联数据更新
     *
     * @returns {void}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.updateViewInfo = function () {
        _super.prototype.updateViewInfo.call(this);
        var form = this.getForm();
        if (!form) {
            return;
        }
        var field = form.findField('srfkey');
        if (!field) {
            return;
        }
        var keyvalue = field.getValue();
        var srforikey = form.findField('srforikey');
        if (field) {
            var keyvalue2 = field.getValue();
            if (keyvalue2 && !Object.is(keyvalue2, '')) {
                keyvalue = keyvalue2;
            }
        }
        var deid = '';
        var deidfield = form.findField('srfdeid');
        if (deidfield) {
            deid = deidfield.getValue();
        }
        var parentData = { srfparentkey: keyvalue };
        if (!Object.is(deid, '')) {
            parentData.srfparentdeid = deid;
        }
        if (this.getDRBar()) {
            this.getDRBar().setParentData(parentData);
        }
    };
    /**
     * 数据项选中变化
     *
     * @private
     * @param {*} [item={}]
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onDRBarItemChangeSelect = function (item) {
        if (item === void 0) { item = {}; }
        var _isShowToolBar = Object.is(item.id, 'form') ? true : false;
        this.isShowToolBar = _isShowToolBar;
        this.setDRBarSelect(item);
        if (_isShowToolBar) {
            this.$router.push({ path: this.route_url });
        }
        else {
            this.openView(item.routepath, item.viewparam);
        }
    };
    /**
     * 获取数据关系部件
     *
     * @returns {any}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.getDRBar = function () {
        return this.getControl('drbar');
    };
    /**
     * 设置数据关系部件选中
     *
     * @param {*} [item={}]
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.setDRBarSelect = function (item) {
        if (item === void 0) { item = {}; }
        if (this.getDRBar()) {
            this.getDRBar().setSelectItem(item);
        }
    };
    /**
     * 获取关系视图
     *
     * @param {*} [view={}]
     * @returns {*}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.getDRItemView = function (view) {
        if (view === void 0) { view = {}; }
    };
    return IBizEditView2Controller;
}(IBizEditViewController));
