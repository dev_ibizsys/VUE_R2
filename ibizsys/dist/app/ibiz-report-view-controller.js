"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 报表视图控制器
 *
 * @export
 * @class IBizReportViewController
 * @extends {IBizSearchViewController}
 */
var IBizReportViewController = /** @class */ (function (_super) {
    __extends(IBizReportViewController, _super);
    /**
     * Creates an instance of IBizReportViewController.
     * 创建 IBizReportViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizReportViewController
     */
    function IBizReportViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var reportPanel = this.getReportPanel();
        if (reportPanel) {
            reportPanel.on(IBizReportPanel.BEFORELOAD, function (data) {
                _this.onStoreBeforeLoad(data);
            });
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        if (!this.getSearchForm() && this.getReportPanel() && this.isLoadDefault()) {
            this.getReportPanel().load();
        }
    };
    /**
     * 获取报表部件
     *
     * @returns {*}
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.getReportPanel = function () {
        return this.getControl('reportpanel');
    };
    /**
     * 搜索搜索表单搜索
     *
     * @param {boolean} isload 是否加载
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onSearchFormSearched = function () {
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    };
    /**
     * 搜搜表单重置
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onSearchFormReseted = function () {
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    };
    /**
     * 视图刷新
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    };
    return IBizReportViewController;
}(IBizSearchViewController));
