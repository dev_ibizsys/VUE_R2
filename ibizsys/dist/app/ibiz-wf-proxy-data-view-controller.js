"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流代理数据视图
 *
 * @class IBizWFProxyDataViewController
 * @extends {IBizMainViewController}
 */
var IBizWFProxyDataViewController = /** @class */ (function (_super) {
    __extends(IBizWFProxyDataViewController, _super);
    /**
     * Creates an instance of IBizWFProxyDataViewController.
     * 创建 IBizWFProxyDataViewController 实例
     * @param {*} [opts={}]
     * @memberof IBizWFProxyDataViewController
     */
    function IBizWFProxyDataViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 代理数据视图url
         *
         * @type {string}
         * @memberof IBizWFProxyDataViewController
         */
        _this.viewurl = '';
        return _this;
    }
    /**
     * 刷新数据 （处理关系数据变更）
     *
     * @memberof IBizWFProxyDataViewController
     */
    IBizWFProxyDataViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        this.loadModel();
    };
    /**
     * 模型加载之前
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyDataViewController
     */
    IBizWFProxyDataViewController.prototype.beforeLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.beforeLoadMode.call(this, data);
        if (this.getParentMode()) {
            Object.assign(data, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(data, this.getParentData());
        }
        if (this.getReferData()) {
            Object.assign(data, this.getReferData());
        }
    };
    /**
     * 工作流加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyDataViewController
     */
    IBizWFProxyDataViewController.prototype.afterLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.afterLoadMode.call(this, data);
        this.viewurl = data.viewurl;
    };
    return IBizWFProxyDataViewController;
}(IBizMainViewController));
