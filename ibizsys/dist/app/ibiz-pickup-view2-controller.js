"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 左右关系单项选择视图控制器
 *
 * @class IBizPickupView2Controller
 * @extends {IBizPickupViewController}
 */
var IBizPickupView2Controller = /** @class */ (function (_super) {
    __extends(IBizPickupView2Controller, _super);
    /**
     * Creates an instance of IBizPickupView2Controller.
     * 创建 IBizPickupView2Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupView2Controller
     */
    function IBizPickupView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizPickupView2Controller;
}(IBizPickupViewController));
