"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 索引关系数据选择视图（部件视图）
 *
 * @class IBizDataViewController
 * @extends {IBizMDViewController}
 */
var IBizDataViewController = /** @class */ (function (_super) {
    __extends(IBizDataViewController, _super);
    /**
     * Creates an instance of IBizDataViewController.
     * 创建 IBizDataViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDataViewController
     */
    function IBizDataViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否支持多项数据选择  <Input>
         *
         * @private
         * @type {boolean}
         * @memberof IBizDataViewController
         */
        _this.multiselect = true;
        /**
         * 当前选择数据 <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizDataViewController
         */
        _this.currentValue = null;
        /**
         * 删除数据 <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizDataViewController
         */
        _this.deleteData = null;
        /**
         * 数据选中事件 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizDataViewController
         */
        _this.selectionChange = 'selectionChange';
        /**
         * 行数据激活事件 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizDataViewController
         */
        _this.dataActivated = 'dataActivated';
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onInitComponents = function () {
        var _this = this;
        var dataViewControl = this.getMDCtrl();
        if (dataViewControl) {
            // 数据视图部件行激活
            dataViewControl.on(IBizDataView.DATAACTIVATED).subscribe(function (data) {
                _this.onDataActivated(data);
            });
            // 数据视图部件行选中
            dataViewControl.on(IBizDataView.SELECTIONCHANGE).subscribe(function (data) {
                _this.onSelectionChange(data);
            });
        }
    };
    /**
     * 部件加载
     *
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onLoad = function () {
        var dataViewControl = this.getMDCtrl();
        if (dataViewControl) {
            dataViewControl.load();
        }
    };
    /**
     * 部件数据激活
     *
     * @param {*} data
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onDataActivated = function (data) {
        _super.prototype.onDataActivated.call(this, data);
        this.$vue.$emit(this.dataActivated, data);
    };
    /**
     * 部件数据行选中
     *
     * @param {*} data
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onSelectionChange = function (data) {
        _super.prototype.onSelectionChange.call(this, data);
        this.$vue.$emit(this.selectionChange, data);
    };
    /**
     * 获取部件
     *
     * @returns {*}
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.getMDCtrl = function () {
        return this.getDataView();
    };
    /**
     * 获取数据视图部件
     *
     * @returns {*}
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.getDataView = function () {
        return this.getControl('dataview');
    };
    return IBizDataViewController;
}(IBizMDViewController));
