"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多数据编辑视图控制器对象
 *
 * @class IBizMEditView9Controller
 * @extends {IBizMDViewController}
 */
var IBizMEditView9Controller = /** @class */ (function (_super) {
    __extends(IBizMEditView9Controller, _super);
    /**
     * Creates an instance of IBizMEditView9Controller.
     * 创建 IBizMEditView9Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMEditView9Controller
     */
    function IBizMEditView9Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化部件对象
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var meditviewpanel = this.getMDCtrl();
        if (meditviewpanel) {
            meditviewpanel.on(IBizMultiEditViewPanel.FINDEDITVIEWCONTROLLER).subscribe(function (data) {
            });
            //数据发生改变
            meditviewpanel.on(IBizMultiEditViewPanel.EDITVIEWCHANGED).subscribe(function (data) {
                _this.onDataChanged(data);
            });
            //数据保存完成
            meditviewpanel.on(IBizMultiEditViewPanel.DATASAVED).subscribe(function (data) {
                _this.onDataSaved();
            });
        }
    };
    /**
     * 刷新
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.load = function () {
    };
    /**
     * 加载
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onRefresh = function () {
        if (this.getMDCtrl) {
            this.getMDCtrl().refresh();
        }
    };
    /**
     * 获取多数据部件对象
     *
     * @returns {*}
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.getMDCtrl = function () {
        return this.getControl('meditviewpanel');
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.isEnableQuickSearch = function () {
        return false;
    };
    /**
     * 父页面触发保存
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.saveData = function () {
        if (this.getMDCtrl() && this.getMDCtrl().isDirty()) {
            this.getMDCtrl().doSave();
            return;
        }
        this.onDataSaved();
    };
    /**
     * 数据发生改变
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onDataChanged = function (data) {
        this.$vue.$emit('drDataChanged', data);
    };
    /**
     * 数据保存完成
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onDataSaved = function () {
        this.$vue.$emit('drDataSaved');
    };
    return IBizMEditView9Controller;
}(IBizMDViewController));
