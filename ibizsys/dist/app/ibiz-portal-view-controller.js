"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Portal视图控制器
 *
 * @class IBizPortalViewController
 * @extends {IBizMainViewController}
 */
var IBizPortalViewController = /** @class */ (function (_super) {
    __extends(IBizPortalViewController, _super);
    /**
     * Creates an instance of IBizPortalViewController.
     * 创建 IBizPortalViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPortalViewController
     */
    function IBizPortalViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 门户部件
         *
         * @private
         * @type {Map<string, any>}
         * @memberof IBizPortalViewController
         */
        _this.portalCtrls = new Map();
        return _this;
    }
    /**
     * 视图初始化
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onInit = function () {
        this.regPortalCtrls();
        _super.prototype.onInit.call(this);
    };
    /**
     * 部件初始化
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        this.portalCtrls.forEach(function (ctrl) {
            // 应用菜单选中
            ctrl.on(IBizAppMenu.MENUSELECTION).subscribe(function (data) {
                _this.onMenuSelection(data);
            });
        });
    };
    /**
     * 视图加载
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onLoad = function () {
        var _this = this;
        _super.prototype.onLoad.call(this);
        this.portalCtrls.forEach(function (ctrl) {
            if (ctrl.load instanceof Function) {
                ctrl.load(_this.viewParam);
            }
        });
    };
    /**
     * 注册所有Portal部件
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.regPortalCtrls = function () {
    };
    /**
     * 注册所有Portal部件
     *
     * @param {string} name
     * @param {*} ctrl
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.regPortalCtrl = function (name, ctrl) {
        this.portalCtrls.set(name, ctrl);
    };
    /**
     * 菜单选中跳转路由
     *
     * @param {Array<any>} data
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onMenuSelection = function (data) {
        var item = {};
        Object.assign(item, data[0]);
        this.openView(item.routepath, item.openviewparam);
    };
    return IBizPortalViewController;
}(IBizMainViewController));
