"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 视图主控制器
 *
 * @class IBizMainViewController
 * @extends {IBizViewController}
 */
var IBizMainViewController = /** @class */ (function (_super) {
    __extends(IBizMainViewController, _super);
    /**
     * Creates an instance of IBizMainViewController.
     * 创建 IBizMainViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMainViewController
     */
    function IBizMainViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否显示工具栏，默认显示
         *
         * @type {boolean}
         * @memberof IBizMainViewController
         */
        _this.isShowToolBar = true;
        /**
         * 视图控制器标题
         *
         * @type {string}
         * @memberof IBizMainViewController
         */
        _this.caption = '';
        /**
         * 实体数据权限
         *
         * @type {*}
         * @memberof IBizMainViewController
         */
        _this.dataaccaction = {};
        /**
         * 视图模型
         *
         * @type {*}
         * @memberof IBizMainViewController
         */
        _this.viewModel = {};
        return _this;
    }
    /**
     * 视图处初始化部件
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var toolbar = this.getToolBar();
        if (toolbar) {
            // 工具栏点击
            toolbar.on(IBizToolbar.ITEMCLICK).subscribe(function (params) {
                _this.onClickTBItem(params);
            });
        }
        var toolbar2 = this.getToolBar2();
        if (toolbar2) {
            // 工具栏点击
            toolbar2.on(IBizToolbar.ITEMCLICK).subscribe(function (params) {
                _this.onClickTBItem(params);
            });
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.loadModel();
    };
    /**
     * 加载视图模型之前
     *
     * @param {*} [params={}]
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.beforeLoadMode = function (params) {
        Object.assign(params, this.getViewParam());
    };
    /**
     * 视图模型加载完毕
     *
     * @protected
     * @param {*} data
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.afterLoadMode = function (data) { };
    /**
     * 加载视图模型
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.loadModel = function () {
        var _this = this;
        var params = { srfaction: 'loadmodel' };
        this.beforeLoadMode(params);
        var url = this.isDynamicView() ? this.addOptionsForUrl(this.getBackendUrl(), this.getViewParam()) : this.getBackendUrl();
        this.iBizHttp.post(url, params).subscribe(function (data) {
            if (data.ret !== 0) {
                console.log(data.info);
                return;
            }
            if (data.dataaccaction && Object.keys(data.dataaccaction).length > 0) {
                Object.assign(_this.dataaccaction, data.dataaccaction);
                _this.onDataAccActionChange(data.dataaccaction);
            }
            _this.afterLoadMode(data);
            Object.assign(_this.viewModel, data);
        }, function (error) {
            console.log(error.info);
        });
    };
    /**
     * 添加参数到指定的url中
     *
     * @private
     * @param {string} url
     * @param {*} [opt={}]
     * @returns {string}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.addOptionsForUrl = function (url, opt) {
        if (opt === void 0) { opt = {}; }
        var keys = Object.keys(opt);
        var isOpt = url.indexOf('?');
        keys.forEach(function (key, index) {
            if (index === 0 && isOpt === -1) {
                url += "?" + key + "=" + opt[key];
            }
            else {
                url += "&" + key + "=" + opt[key];
            }
        });
        return url;
    };
    /**
     * 是否是动态视图
     *
     * @returns {boolean}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.isDynamicView = function () {
        return false;
    };
    /**
     * 点击按钮
     *
     * @param {*} [params={}]  tag 事件源
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onClickTBItem = function (params) {
        if (params === void 0) { params = {}; }
        var uiaction = this.getUIAction(params.tag);
        this.doUIAction(uiaction, params);
    };
    /**
     * 处理实体界面行为
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (uiaction && (typeof uiaction === 'string')) {
            uiaction = this.getUIAction(uiaction);
        }
        if (uiaction) {
            if (Object.is(uiaction.type, 'DEUIACTION')) {
                this.doDEUIAction(uiaction, params);
                return;
            }
            if (Object.is(uiaction.type, 'WFUIACTION')) {
                this.doWFUIAction(uiaction, params);
                return;
            }
        }
    };
    /**
     * 获取前台行为参数
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getFrontUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        if (uiaction.refreshview) {
            arg.callback = function (win) {
                this.refresh();
            };
        }
        return arg;
    };
    /**
     * 获取后台行为参数
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getBackendUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        return arg;
    };
    /**
     * 打开界面行为视图，前端实体界面行为
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [viewparam={}]  视图参数
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.openUIActionView = function (uiaction, viewparam) {
        var _this = this;
        if (uiaction === void 0) { uiaction = {}; }
        if (viewparam === void 0) { viewparam = {}; }
        var frontview = uiaction.frontview;
        frontview.viewparam = viewparam;
        if (Object.is(uiaction.fronttype, 'TOP')) {
            Object.assign(viewparam, { openerid: this.getId(), pviewusage: this.getViewUsage() });
            this.openWindow(frontview.viewurl, viewparam);
            return;
        }
        this.openModal(frontview).subscribe(function (result) {
            if (result && Object.is(result.ret, 'OK')) {
                if (uiaction.reload) {
                    _this.onRefresh(uiaction);
                }
                if (uiaction.colseeditview) {
                    _this.closeWindow();
                }
            }
        });
        return;
    };
    /**
     * 执行实体行为
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doDEUIAction = function (uiaction, params) {
        var _this = this;
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.actionmode, 'FRONT')) {
            if ((Object.is(uiaction.fronttype, 'WIZARD')) || (Object.is(uiaction.fronttype, 'SHOWPAGE')) || (Object.is(uiaction.fronttype, 'TOP'))) {
                var viewparam = this.getFrontUIActionParam(uiaction, params);
                if (!viewparam) {
                    viewparam = {};
                }
                var frontview = uiaction.frontview;
                if (params.srfmstag) {
                    Object.assign(viewparam, { srfeditmode2: params.srfmstag });
                }
                if (frontview.redirectview) {
                    this.redirectOpenView({ redirectUrl: frontview.backendurl, viewParam: viewparam });
                    return;
                }
                // 携带所有的实体界面行为参数
                this.openUIActionView(uiaction, viewparam);
                return;
            }
            if (Object.is(uiaction.fronttype, 'OPENHTMLPAGE')) {
                var viewparam_1 = this.getFrontUIActionParam(uiaction, params);
                var _names = Object.keys(viewparam_1);
                var viewparam_arr_1 = [];
                _names.forEach(function (name) {
                    if (viewparam_1[name]) {
                        viewparam_arr_1.push(name + "=" + viewparam_1[name]);
                    }
                });
                var url = uiaction.htmlpageurl.indexOf('?') !== -1 ? "" + uiaction.htmlpageurl + viewparam_arr_1.join('&') : uiaction.htmlpageurl + "?" + viewparam_arr_1.join('&');
                window.open(url, '_blank');
                return;
            }
        }
        if (Object.is(uiaction.actionmode, 'BACKEND')) {
            var param_1 = this.getBackendUIActionParam(uiaction, params);
            if (!param_1) {
                return;
            }
            param_1.srfuiactionid = uiaction.tag;
            if (uiaction.confirmmsg) {
                this.iBizNotification.confirm('提示', uiaction.confirmmsg).subscribe(function (result) {
                    if (result && Object.is(result, 'OK')) {
                        _this.doBackendUIAction(param_1);
                    }
                });
            }
            else {
                this.doBackendUIAction(param_1);
            }
            return;
        }
        this.iBizNotification.error('错误', '未处理的实体行为[' + uiaction.tag + ']');
    };
    /**
     * 执行工作流行为
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doWFUIAction = function (uiaction, params) {
        var _this = this;
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.actionmode, 'WFFRONT')) {
            if (Object.is(uiaction.fronttype, 'WIZARD') || Object.is(uiaction.fronttype, 'SHOWPAGE')) {
                // let className: string;
                // if (uiaction.frontview.className) {
                //     className = uiaction.frontview.className;
                // } else {
                //     className = uiaction.frontview.classname;
                // }
                var opt = { viewparam: {} };
                var data = this.getFrontUIActionParam(uiaction, params);
                // opt.modalZIndex = this.modalZIndex;
                // opt.viewparam = {};
                if (data) {
                    Object.assign(opt.viewparam, data);
                }
                if (uiaction.frontview.viewParam) {
                    Object.assign(opt.viewparam, uiaction.frontview.viewParam);
                }
                else {
                    Object.assign(opt.viewparam, uiaction.frontview.viewparam);
                }
                Object.assign(opt, {
                    modalviewname: uiaction.frontview.modalviewname,
                    title: uiaction.frontview.title,
                    height: uiaction.frontview.height,
                    width: uiaction.frontview.width,
                });
                // 打开模态框
                var modal = this.openModal(opt);
                modal.subscribe(function (result) {
                    if (result && Object.is(result.ret, 'OK')) {
                        _this.onWFUIFrontWindowClosed(result);
                    }
                });
                return;
            }
        }
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.ERROR','错误'),IGM('MAINVIEWCONTROLLER.DOWFUIACTION.INFO','未处理的实体工作流行为['+uiaction.tag+']',[uiaction.tag]), 2);
        this.iBizNotification.warning('错误', '未处理的实体工作流行为[' + uiaction.tag + ']');
    };
    /**
     * 关系工作流窗口对象
     *
     * @param {*} win
     * @param {*} [data={}]
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onWFUIFrontWindowClosed = function (win, data) {
        if (data === void 0) { data = {}; }
    };
    /**
     * 执行后台行为
     *
     * @param {*} [uiaction={}] 行为
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doBackendUIAction = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.ERROR','错误'),IGM('MAINVIEWCONTROLLER.DOBACKENDUIACTION.INFO','未处理的后台界面行为['+uiaction.tag+']',[uiaction.tag]), 2);
        this.iBizNotification.error('错误', '未处理的后台界面行为[' + uiaction.tag + ']');
    };
    /**
     * 是否-模式框显示
     *
     * @returns {boolean}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.isShowModal = function () {
        return false;
    };
    /**
     * 关闭窗口
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.closeWindow = function () {
        if (this.isModal()) {
            this.closeModal({});
            return;
        }
        var win = this.getWindow();
        var viewParams = this.getViewParam();
        if (viewParams.ru && !Object.is(viewParams.ru, '') && (viewParams.ru.startsWith('https://') || viewParams.ru.startsWith('http://'))) {
            win.location.href = viewParams.ru;
            return;
        }
        win.close();
    };
    /**
     * 获取窗口对象
     *
     * @returns {Window}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getWindow = function () {
        return window;
    };
    /**
     * 获取标题
     *
     * @returns {string} 标题
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getCaption = function () {
        return this.caption;
    };
    /**
     * 设置标题
     *
     * @param {string} caption 标题
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.setCaption = function (caption) {
        if (!Object.is(this.caption, caption)) {
            this.caption = caption;
            this.fire(IBizMainViewController.CAPTIONCHANGED, this);
        }
    };
    /**
     * 获取工具栏服务对象
     *
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getToolBar = function () {
        return this.getControl('toolbar');
    };
    /**
     * 获取工具栏服务对象
     *
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getToolBar2 = function () {
        return this.getControl('toolbar2');
    };
    /**
     * 计算工具栏项状态-<例如 根据是否有选中数据,设置 工具栏按钮是否可点击>
     *
     * @param {boolean} hasdata 是否有数据
     * @param {*} dataaccaction
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.calcToolbarItemState = function (hasdata, dataaccaction) {
        var toolbar = this.getToolBar();
        if (toolbar && toolbar.getItems().length > 0) {
            var items = toolbar.getItems();
            toolbar.setItemDisabled(items, !hasdata);
            toolbar.updateAccAction(items, Object.assign({}, this.dataaccaction, dataaccaction));
        }
        var toolbar2 = this.getToolBar2();
        if (toolbar2 && toolbar2.getItems().length > 0) {
            var items = toolbar2.getItems();
            toolbar2.setItemDisabled(items, !hasdata);
            toolbar2.updateAccAction(items, Object.assign({}, this.dataaccaction, dataaccaction));
        }
    };
    /**
     * 获取引用视图
     *
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getReferView = function () {
        return undefined;
    };
    /**
     * 打开重定向视图
     *
     * @param {{ redirectUrl: string, viewParam: any }} view 打开视图的参数
     * @param {*} [uiaction] 实体界面行为
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.redirectOpenView = function (view, uiaction) {
        var _this = this;
        var viewParam = {};
        viewParam.srfviewparam = JSON.stringify(view.viewParam);
        Object.assign(viewParam, { 'srfaction': 'GETRDVIEW' });
        this.iBizHttp.post(view.redirectUrl, viewParam).subscribe(function (response) {
            if (!response.rdview || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.info('错误', response.errorMessage);
                }
                return;
            }
            if (!response.rdview || Object.keys(response.rdview).length === 0) {
                _this.iBizNotification.info('错误', '重定向视图信息获取错误,无法打开!');
            }
            var data = {};
            if (response.rdview.srfkey || Object.is(response.rdview.srfkey, '')) {
                Object.assign(data, { srfkey: response.rdview.srfkey, srfkeys: response.rdview.srfkey });
            }
            if (response.rdview.srfviewparam) {
                Object.assign(data, response.rdview.srfviewparam);
            }
            var rdview = response.rdview;
            Object.assign(data, { openerid: _this.getId(), pviewusage: _this.getViewUsage() });
            if (Object.is(rdview.openmode, 'POPUPMODAL')) {
                var formatname = IBizUtil.srfFilePath2(rdview.viewname);
                var modelviewname = formatname + "_modalview";
                var view_1 = {
                    height: rdview.height,
                    modalviewname: modelviewname,
                    title: rdview.title,
                    viewparam: data,
                    width: rdview.width,
                };
                var modal = _this.openModal(view_1);
                modal.subscribe(function (result) {
                    if (result && Object.is(result.ret, 'OK')) {
                        if (!uiaction) {
                            return;
                        }
                        if (uiaction.reload) {
                            _this.onRefresh(uiaction);
                        }
                        if (uiaction.colseeditview) {
                            _this.closeWindow();
                        }
                    }
                });
            }
            else {
                var _modulename = IBizUtil.srfFilePath2(rdview.modulename);
                var _viewname = IBizUtil.srfFilePath2(rdview.viewname);
                var url = "/pages/" + _modulename + "/" + _viewname + "/" + _viewname + ".html#/" + _viewname;
                _this.openWindow(url, data);
                // const routeUrl = `${rdview.modulename.toLowerCase()}_${rdview.viewname.toLowerCase()}`;
                // this.openView(routeUrl, data);
            }
        }, function (error) {
            _this.iBizNotification.info('错误', error.info);
        });
    };
    /**
     * 实体数据发生变化
     *
     * @param {*} [dataaccaction={}]
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onDataAccActionChange = function (dataaccaction) {
        if (dataaccaction === void 0) { dataaccaction = {}; }
    };
    /**
     * 是否自由布局
     *
     * @returns {boolean}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.isFreeLayout = function () {
        return false;
    };
    /**
     * 设置视图标题
     *
     * @static
     * @memberof IBizMainViewController
     */
    IBizMainViewController.CAPTIONCHANGED = 'CAPTIONCHANGED';
    /**
     * 视图模型加载完成
     *
     * @static
     * @memberof IBizMainViewController
     */
    IBizMainViewController.VIEWMODELLOADED = 'VIEWMODELLOADED';
    return IBizMainViewController;
}(IBizViewController));
