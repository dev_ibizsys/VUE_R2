"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流启动视图
 *
 * @class IBizHtmlViewController
 * @extends {IBizMainViewController}
 */
var IBizHtmlViewController = /** @class */ (function (_super) {
    __extends(IBizHtmlViewController, _super);
    /**
     * Creates an instance of IBizHtmlViewController.
     * 创建 IBizHtmlViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizHtmlViewController
     */
    function IBizHtmlViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 代理应用启动视图url
         *
         * @type {string}
         * @memberof IBizHtmlViewController
         */
        _this.viewurl = '';
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.onPostMessage().subscribe(function (data) {
                if (data === void 0) { data = {}; }
                if (!Object.is(_this.getId(), data.openerid) && !Object.is(data.type, 'SETSELECTIONS')) {
                    return;
                }
                _this.setViewResult(data);
            });
        }
        return _this;
    }
    /**
     * 刷新数据 （处理关系数据变更）
     *
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        this.loadModel();
    };
    /**
     * 模型加载之前
     *
     * @param {*} [data={}]
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.beforeLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.beforeLoadMode.call(this, data);
        if (this.getParentMode()) {
            Object.assign(data, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(data, this.getParentData());
        }
        if (this.getReferData()) {
            Object.assign(data, this.getReferData());
        }
    };
    /**
     * 视图模型加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.afterLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.afterLoadMode.call(this, data);
        var url = data.viewurl;
        this.viewurl = url.replace('-OPENERID-', this.getId());
    };
    /**
     * 设置视图结果数据
     *
     * @param {*} data
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.setViewResult = function (data) {
        if (data.selections && Array.isArray(data.selections)) {
            this.closeModal({ ret: 'OK', selections: data.selections });
        }
        else {
            this.closeModal();
        }
    };
    return IBizHtmlViewController;
}(IBizMainViewController));
