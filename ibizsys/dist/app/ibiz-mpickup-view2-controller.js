"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 左右关系多项数据选择视图控制器
 *
 * @class IBizMPickupView2Controller
 * @extends {IBizMPickupViewController}
 */
var IBizMPickupView2Controller = /** @class */ (function (_super) {
    __extends(IBizMPickupView2Controller, _super);
    /**
     * Creates an instance of IBizMPickupView2Controller.
     * 创建 IBizMPickupView2Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMPickupView2Controller
     */
    function IBizMPickupView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizMPickupView2Controller;
}(IBizMPickupViewController));
