"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 向导视图控制器
 *
 * @class IBizWizardViewController
 * @extends {IBizMainViewController}
 */
var IBizWizardViewController = /** @class */ (function (_super) {
    __extends(IBizWizardViewController, _super);
    /**
     *Creates an instance of IBizWizardViewController.
     * @param {*} [opts={}]
     * @memberof IBizWizardViewController
     */
    function IBizWizardViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化面板
     *
     * @memberof IBizEditViewController
     */
    IBizWizardViewController.prototype.onInitComponents = function () {
        var _this_1 = this;
        _super.prototype.onInitComponents.call(this);
        var wizardpanel = this.getWizardPanel();
        if (wizardpanel) {
            // 向导面板初始化完成
            wizardpanel.on(IBizWizardPanel.WIZARDPANEL_INITED).subscribe(function (data) {
                _this_1.onWizardInited(data);
            });
            // 向导面板完成
            wizardpanel.on(IBizWizardPanel.WIZARDPANEL_FINISH).subscribe(function (data) {
                _this_1.onWizardFinesh(data);
            });
            // 向导表单保存完成
            wizardpanel.on(IBizWizardPanel.WIZARDFORMSAVED).subscribe(function (data) {
                _this_1.onWizardFormSaved(data);
            });
            // 向导表单切换
            wizardpanel.on(IBizWizardPanel.WIZARDFORMCHANGED).subscribe(function (data) {
                _this_1.onWizardFormChanged(data);
            });
            // 向导表单项更新
            wizardpanel.on(IBizWizardPanel.WIZARDFORMFIELDCHANGED).subscribe(function (data) {
                if (data) {
                    _this_1.onWizardFormFieldChanged(data.formName, data.name, data.field, data.value);
                }
            });
        }
    };
    /**
     * 加载面板数据
     *
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onLoad = function () {
        if (this.getWizardPanel()) {
            this.getWizardPanel().autoLoad(this.getViewParam());
        }
    };
    /**
     * 获取向导面板对象
     *
     * @returns {*}
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.getWizardPanel = function () {
        return this.getControl('wizardpanel');
    };
    /**
     * 向导表单保存完成
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFormSaved = function (data) {
    };
    /**
     * 向导面板初始化完成
     *
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardInited = function (data) {
    };
    /**
     * 向导面板完成
     *
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFinesh = function (data) {
        var _this = this;
        _this.$vue.$emit('dataChange', data);
        _this.refreshReferView(data);
    };
    /**
     * 向导表单切换
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFormChanged = function (data) {
    };
    /**
     * 向导表单项更新
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFormFieldChanged = function (formName, fieldname, field, value) {
    };
    /**
     * 刷新关联数据
     *
     * @memberof IBizEditViewController
     */
    IBizWizardViewController.prototype.refreshReferView = function (data) {
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            var parentWindow = iBizApp.getParentWindow();
            var viewparam = this.getViewParam();
            if (parentWindow && viewparam.openerid && !Object.is(viewparam.openerid, '')) {
                try {
                    var pWinIBizApp = parentWindow.getIBizApp();
                    pWinIBizApp.fireRefreshView({ openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) });
                }
                catch (error) {
                    parentWindow.postMessage({ ret: 'OK', type: 'REFERVIEW', openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) }, '*');
                    // if (window && window.parent) {
                    //     let win = window;
                    // }
                }
            }
        }
        if (this.isModal()) {
            var result = { ret: 'OK', activeData: data };
            this.dataChange(result);
        }
    };
    return IBizWizardViewController;
}(IBizMainViewController));
