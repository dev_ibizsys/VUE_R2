"use strict";
/**
 * IBizApp 应用
 * 调用 getInstance() 获取实列
 *
 * @class IBizApp
 */
var IBizApp = /** @class */ (function () {
    /**
     * Creates an instance of IBizApp.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof IBizApp
     */
    function IBizApp() {
        var _this = this;
        /**
         * 当前窗口所有视图控制器
         *
         * @type {Array<any>}
         * @memberof IBizApp
         */
        this.viewControllers = [];
        /**
         * 父窗口window对象
         *
         * @type {*}
         * @memberof IBizApp
         */
        this.parentWindow = null;
        /**
         * rxjs 流观察对象
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizApp
         */
        this.subject = new rxjs.Subject();
        /**
         * postMessage 流观察对象
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizApp
         */
        this.postMessage = new rxjs.Subject();
        /**
         * 主题颜色
         *
         * @type {String}
         * @memberof IBizApp
         */
        this.themeClass = '';
        /**
         * 字体
         *
         * @private
         * @type {String}
         * @memberof IBizApp
         */
        this.fontFamily = '';
        /**
         * app应用功能数据
         *
         * @private
         * @type {string}
         * @memberof IBizApp
         */
        this.appData = '';
        var win = window;
        win.addEventListener('message', function (message) {
            if (message.data && Object.is(message.data.ret, 'OK') && !Object.is(message.data.type, '')) {
                _this.firePostMessage(message.data);
            }
        }, false);
    }
    /**
     * 获取 IBizApp 单列对象
     *
     * @static
     * @returns {IBizApp}
     * @memberof IBizApp
     */
    IBizApp.getInstance = function () {
        if (!IBizApp.iBizApp) {
            IBizApp.iBizApp = new IBizApp();
        }
        return IBizApp.iBizApp;
    };
    /**
     * 注册视图控制
     *
     * @param {*} ctrler
     * @memberof IBizApp
     */
    IBizApp.prototype.regSRFController = function (ctrler) {
        this.viewControllers.push(ctrler);
    };
    /**
     * 注销视图控制器
     *
     * @param {*} ctrler
     * @memberof IBizApp
     */
    IBizApp.prototype.unRegSRFController = function (ctrler) {
        var id = ctrler.getId();
        var viewUsage = ctrler.getViewUsage();
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            this.viewControllers[index] = null;
            this.viewControllers.splice(index, 1);
        }
    };
    /**
     * 注销视图控制器
     *
     * @param {string} id 视图id
     * @param {number} viewUsage 视图使用模式
     * @memberof IBizApp
     */
    IBizApp.prototype.unRegSRFController2 = function (id, viewUsage) {
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            this.viewControllers[index] = null;
            this.viewControllers.splice(index, 1);
        }
    };
    /**
     * 获取视图控制器
     *
     * @param {string} id 视图id
     * @param {number} viewUsage 视图使用模式
     * @returns {*}
     * @memberof IBizApp
     */
    IBizApp.prototype.getSRFController = function (id, viewUsage) {
        var viewController = null;
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            viewController = this.viewControllers[index];
        }
        return viewController;
    };
    /**
     * 获取父视图控制器
     *
     * @param {string} id 视图id
     * @param {number} viewUsage 视图使用模式
     * @returns {*}
     * @memberof IBizApp
     */
    IBizApp.prototype.getParentController = function (id, viewUsage) {
        var viewController = null;
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            viewController = this.viewControllers[index - 1];
        }
        return viewController;
    };
    /**
     * 注册父窗口window 对象
     *
     * @param {Window} win
     * @memberof IBizApp
     */
    IBizApp.prototype.regParentWindow = function (win) {
        this.parentWindow = win;
    };
    /**
     * 获取父窗口window 对象
     *
     * @returns {Window}
     * @memberof IBizApp
     */
    IBizApp.prototype.getParentWindow = function () {
        return this.parentWindow;
    };
    /**
     * 订阅刷新视图事件
     *
     * @returns {Subject<any>}
     * @memberof IBizApp
     */
    IBizApp.prototype.onRefreshView = function () {
        return this.subject;
    };
    /**
     * 通知视图刷新事件
     *
     * @param {*} data
     * @memberof IBizApp
     */
    IBizApp.prototype.fireRefreshView = function (data) {
        if (data === void 0) { data = {}; }
        this.subject.next(data);
    };
    /**
     * 订阅postMessage对象
     *
     * @returns {Subject<any>}
     * @memberof IBizApp
     */
    IBizApp.prototype.onPostMessage = function () {
        return this.postMessage;
    };
    /**
     * 处理postMessage对象
     *
     * @param {*} [data={}]
     * @memberof IBizApp
     */
    IBizApp.prototype.firePostMessage = function (data) {
        if (data === void 0) { data = {}; }
        this.postMessage.next(data);
    };
    /**
     * 设置主题颜色
     *
     * @param {String} name
     * @memberof IBizApp
     */
    IBizApp.prototype.setThemeClass = function (name) {
        this.themeClass = name;
        Cookies.set('theme-class', name);
    };
    /**
     * 获取主题颜色
     *
     * @returns {String}
     * @memberof IBizApp
     */
    IBizApp.prototype.getThemeClass = function () {
        if (Object.is(this.themeClass, '')) {
            var name_1 = Cookies.get('theme-class');
            if (name_1) {
                return name_1;
            }
            return 'ibiz-default-theme';
        }
        else {
            return this.themeClass;
        }
    };
    /**
     * 设置字体
     *
     * @param {String} font
     * @memberof IBizApp
     */
    IBizApp.prototype.setFontFamily = function (font) {
        this.fontFamily = font;
        Cookies.set('font-family', font);
    };
    /**
     * 获取字体
     *
     * @returns {String}
     * @memberof IBizApp
     */
    IBizApp.prototype.getFontFamily = function () {
        if (Object.is(this.fontFamily, '')) {
            var font = Cookies.get('font-family');
            if (font) {
                return font;
            }
            return '';
        }
        else {
            return this.fontFamily;
        }
    };
    /**
     * 获取应用功能数据
     *
     * @returns {string}
     * @memberof IBizApp
     */
    IBizApp.prototype.getAppData = function () {
        return this.appData;
    };
    /**
     * 设置应用功能数据
     *
     * @param {string} appdata
     * @memberof IBizApp
     */
    IBizApp.prototype.setAppData = function (appdata) {
        this.appData = appdata;
    };
    /**
     * 单列变量声明
     *
     * @private
     * @static
     * @type {IBizApp}
     * @memberof IBizApp
     */
    IBizApp.iBizApp = null;
    return IBizApp;
}());
// 初始化IBizApp 对象， 挂载在window对象下
(function () {
    var win = window;
    if (!win.iBizApp) {
        win.iBizApp = IBizApp.getInstance();
    }
    win.getIBizApp = function () {
        if (!win.iBizApp) {
            win.iBizApp = IBizApp.getInstance();
        }
        return win.iBizApp;
    };
    if (window.opener && window.opener.window) {
        IBizApp.getInstance().regParentWindow(window.opener.window);
    }
})();

"use strict";
// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});
// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    var data = null;
    if (response.data) {
        data = response.data;
    }
    // no login
    if (data && data.ret === 2 && data.notlogin) {
        var curUrl = decodeURIComponent(window.location.href);
        if (IBizEnvironment.CustomLoginRedirect) {
            if (window.location.href.indexOf(IBizEnvironment.LoginRedirect) === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
        else {
            if (window.location.href.indexOf('/ibizutil/login.html') === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
    }
    return data;
}, function (error) {
    // Do something with response error
    error = error ? error : { response: {} };
    var res = error.response;
    var _data = res.data;
    // no login
    if (_data && _data.ret === 2 && _data.notlogin) {
        var curUrl = decodeURIComponent(window.location.href);
        if (IBizEnvironment.CustomLoginRedirect) {
            if (window.location.href.indexOf(IBizEnvironment.LoginRedirect) === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
        else {
            if (window.location.href.indexOf('/ibizutil/login.html') === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
    }
    // 其他状态码信息
    if (!_data) {
        _data = IBizHandResponseData.doErrorResponseData(res);
    }
    return Promise.reject(_data);
});

"use strict";
/**
 * IBizSys平台工具类
 *
 * @class IBizUtil
 */
var IBizUtil = /** @class */ (function () {
    function IBizUtil() {
    }
    /**
     * 创建 UUID
     *
     * @static
     * @returns {string}
     * @memberof IBizUtil
     */
    IBizUtil.createUUID = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    };
    /**
     * 判断条件是否成立
     *
     * @static
     * @param {*} value
     * @param {*} op
     * @param {*} value2
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.testCond = function (value, op, value2) {
        // 等于操作
        if (Object.is(op, 'EQ')) {
            return value === value2;
        }
        // 大于操作
        if (Object.is(op, 'GT')) {
            var result = this.compare(value, value2);
            if (result !== undefined && result > 0) {
                return true;
            }
            else {
                return false;
            }
        }
        // 大于等于操作
        if (Object.is(op, 'GTANDEQ')) {
            var result = this.compare(value, value2);
            if (result !== undefined && result >= 0) {
                return true;
            }
            else {
                return false;
            }
        }
        // 值包含在给定的范围中
        if (Object.is(op, 'IN')) {
            return this.contains(value, value2);
        }
        // 不为空判断操作
        if (Object.is(op, 'ISNOTNULL')) {
            return (value != null && value !== '');
        }
        // 为空判断操作
        if (Object.is(op, 'ISNULL')) {
            return (value == null || value === '');
        }
        // 文本左包含
        if (Object.is(op, 'LEFTLIKE')) {
            return (value && value2 && (value.toUpperCase().indexOf(value2.toUpperCase()) === 0));
        }
        // 文本包含
        if (Object.is(op, 'LIKE')) {
            return (value && value2 && (value.toUpperCase().indexOf(value2.toUpperCase()) !== -1));
        }
        // 小于操作
        if (Object.is(op, 'LT')) {
            var result = this.compare(value, value2);
            if (result !== undefined && result < 0) {
                return true;
            }
            else {
                return false;
            }
        }
        // 小于等于操作
        if (Object.is(op, 'LTANDEQ')) {
            var result = this.compare(value, value2);
            if (result !== undefined && result <= 0) {
                return true;
            }
            else {
                return false;
            }
        }
        // 不等于操作
        if (Object.is(op, 'NOTEQ')) {
            return value !== value2;
        }
        // 值不包含在给定的范围中
        if (Object.is(op, 'NOTIN')) {
            return !this.contains(value, value2);
        }
        // 文本右包含
        if (Object.is(op, 'RIGHTLIKE')) {
            if (!(value && value2)) {
                return false;
            }
            var nPos = value.toUpperCase().indexOf(value2.toUpperCase());
            if (nPos === -1) {
                return false;
            }
            return nPos + value2.length === value.length;
        }
        // 空判断
        if (Object.is(op, 'TESTNULL')) {
        }
        // 自定义包含
        if (Object.is(op, 'USERLIKE')) {
        }
        return false;
    };
    /**
     * 文本包含
     *
     * @static
     * @param {any} value
     * @param {any} value2
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.contains = function (value, value2) {
        if (value && value2) {
            // 定义一数组
            var arr = new Array();
            arr = value2.split(',');
            // 定义正则表达式的连接符
            var S = String.fromCharCode(2);
            var reg = new RegExp(S + value + S);
            return (reg.test(S + arr.join(S) + S));
        }
        return false;
    };
    /**
     * 值比较
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {number}
     * @memberof IBizUtil
     */
    IBizUtil.compare = function (value, value2) {
        var result = null;
        if (!Object.is(value, '') && !Object.is(value2, '') && !isNaN(value) && !isNaN(value2)) {
            result = this.compareNumber(parseFloat(value), parseFloat(value2));
        }
        else if (this.isParseDate(value) && this.isParseDate(value2)) {
            result = this.compareDate((new Date(value)).getTime(), (new Date(value2)).getTime());
        }
        else if (value && (typeof (value) === 'boolean' || value instanceof Boolean)) {
            result = this.compareBoolean(value, value2);
        }
        else if (value && (typeof (value) === 'string' || value instanceof String)) {
            result = this.compareString(value, value2);
        }
        return result;
    };
    /**
     * 是否是时间
     *
     * @static
     * @param {string} value
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.isParseDate = function (value) {
        var time = new Date(value);
        if (isNaN(time.getTime())) {
            return false;
        }
        return true;
    };
    /**
     * 时间值比较（毫秒数）
     *
     * @static
     * @param {number} value
     * @param {number} value2
     * @returns {number}
     * @memberof IBizUtil
     */
    IBizUtil.compareDate = function (value, value2) {
        if (value > value2) {
            return 1;
        }
        else if (value < value2) {
            return -1;
        }
        else {
            return 0;
        }
    };
    /**
     * 数值比较
     *
     * @static
     * @param {number} value
     * @param {number} value2
     * @returns {number}
     * @memberof IBizUtil
     */
    IBizUtil.compareNumber = function (value, value2) {
        if (value > value2) {
            return 1;
        }
        else if (value < value2) {
            return -1;
        }
        else {
            return 0;
        }
    };
    /**
     * 字符串比较
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {number}
     * @memberof IBizUtil
     */
    IBizUtil.compareString = function (value, value2) {
        return value.localeCompare(value2);
    };
    /**
     * boolean 值比较
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {number}
     * @memberof IBizUtil
     */
    IBizUtil.compareBoolean = function (value, value2) {
        if (value === value2) {
            return 0;
        }
        else {
            return -1;
        }
    };
    /**
    *
    *
    * @param {*} [o={}]
    * @memberof IBizUtil
    */
    IBizUtil.processResult = function (o) {
        if (o === void 0) { o = {}; }
        if (o.url != null && o.url !== '') {
            window.location.href = o.url;
        }
        if (o.code != null && o.code !== '') {
            eval(o.code);
        }
        if (o.downloadurl != null && o.downloadurl !== '') {
            var downloadurl = this.parseURL2('', o.downloadurl, '');
            this.download(downloadurl);
        }
    };
    /**
     * 下载文件
     *
     * @static
     * @param {string} url
     * @memberof IBizUtil
     */
    IBizUtil.download = function (url) {
        window.open(url, '_blank');
    };
    /**
     *
     *
     * @static
     * @param {any} subapp
     * @param {any} url
     * @param {any} params
     * @returns {string}
     * @memberof IBizUtil
     */
    IBizUtil.parseURL2 = function (subapp, url, params) {
        var tmpURL;
        // let root;
        // if (subapp) {
        //     root = WEBROOTURL;
        // } else {
        //     root = BASEURL;
        // }
        // if (url.toLowerCase().indexOf('http') !== -1) {
        //     tmpURL = url;
        // } else {
        //     tmpURL = root + '/jsp' + url;
        // }
        // if (url.indexOf('../../') !== -1) {
        //     tmpURL = url.substring(url.indexOf('../../') + 6, url.length);
        // } else if (url.indexOf('/') === 0) {
        //     tmpURL = url.substring(url.indexOf('/') + 1, url.length);
        // } else {
        //     tmpURL = url;
        // }
        tmpURL = "/" + IBizEnvironment.AppName.toLowerCase() + url + " ";
        tmpURL = (IBizEnvironment.LocalDeve ? '' : "/" + IBizEnvironment.BaseUrl) + tmpURL;
        if (params) {
            return tmpURL + (tmpURL.indexOf('?') === -1 ? '?' : '&'); // + $.param(params);
        }
        else {
            return tmpURL;
        }
    };
    /**
     * 是否是数字
     *
     * @param {*} num
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.isNumberNaN = function (num) {
        return Number.isNaN(num) || num !== num;
    };
    /**
     * 是否未定义
     *
     * @static
     * @param {*} value
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.isUndefined = function (value) {
        return typeof value === 'undefined';
    };
    /**
     * 是否为空
     *
     * @static
     * @param {*} value
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.isEmpty = function (value) {
        return this.isUndefined(value) || value === '' || value === null || value !== value;
    };
    /**
     * 检查属性常规条件
     *
     * @static
     * @param {string} defname 属性名称
     * @param {string} op 条件
     * @param {string} paramValue 值项
     * @param {string} errorInfo 错误信息
     * @param {string} paramType 参数类型
     * @param {boolean} primaryModel 是否必须条件
     * @param {*} form 表单
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.checkFieldSimpleRule = function (defname, op, paramValue, errorInfo, paramType, primaryModel, form) {
        var value = null;
        var value2 = null;
        var _deField = form.findField(defname);
        if (_deField) {
            value = _deField.getValue();
        }
        if (Object.is(paramType, 'CURTIME')) {
            value2 = "" + new Date();
        }
        if (Object.is(paramType, 'ENTITYFIELD')) {
            value2 = paramValue;
        }
        if (this.isEmpty(errorInfo)) {
            errorInfo = '内容必须符合值规则';
        }
        this.errorInfo = errorInfo;
        var reault = this.testCond(value, op, value2);
        if (!reault) {
            if (primaryModel) {
                throw new Error(this.errorInfo);
            }
            return false;
        }
        this.errorInfo = '';
        return true;
    };
    /**
     * 检查属性字符长度规则
     *
     * @static
     * @param {string} defname 属性名称
     * @param {number} minLength 最小长度
     * @param {boolean} indexOfMin 是否包含最小
     * @param {number} maxLength 最大长度
     * @param {boolean} indexOfMax 是否包含最大
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键值
     * @param {*} form 表单
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.checkFieldStringLengthRule = function (defname, minLength, indexOfMin, maxLength, indexOfMax, errorInfo, primaryModel, form) {
        var value = null;
        var _deField = form.findField(defname);
        if (_deField) {
            value = _deField.getValue();
        }
        if (this.isEmpty(errorInfo)) {
            this.errorInfo = '内容长度必须符合范围规则';
        }
        else {
            this.errorInfo = errorInfo;
        }
        var isEmpty = IBizUtil.isEmpty(value);
        // 值为空或者未定义
        if (isEmpty) {
            if (primaryModel) {
                throw new Error(this.errorInfo);
            }
            return false;
        }
        var viewValueLength = value.length;
        // 小于等于
        if (minLength !== null) {
            if (indexOfMin) {
                if (viewValueLength < minLength) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
            else {
                if (viewValueLength <= minLength) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
        }
        //  大于等于
        if (maxLength !== null) {
            if (indexOfMax) {
                if (viewValueLength > maxLength) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
            else {
                if (viewValueLength >= maxLength) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
        }
        this.errorInfo = '';
        return true;
    };
    /**
     * 检查属性值正则式规则
     *
     * @static
     * @param {string} defname 属性名称
     * @param {*} strReg 正则表达式
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @param {*} form
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.checkFieldRegExRule = function (defname, strReg, errorInfo, primaryModel, form) {
        var value = null;
        var _deField = form.findField(defname);
        if (_deField) {
            value = _deField.getValue();
        }
        if (this.isEmpty(errorInfo)) {
            this.errorInfo = '值必须符合正则规则';
        }
        else {
            this.errorInfo = errorInfo;
        }
        var isEmpty = IBizUtil.isEmpty(value);
        if (isEmpty) {
            if (primaryModel) {
                throw new Error(this.errorInfo);
            }
            return false;
        }
        var regExp = new RegExp(strReg);
        var result = regExp.test(value);
        if (!result) {
            if (primaryModel) {
                throw new Error(this.errorInfo);
            }
            return false;
        }
        this.errorInfo = '';
        return true;
    };
    /**
     * 检查属性值范围规则
     *
     * @static
     * @param {string} defname 属性名称
     * @param {*} minNumber 最小值
     * @param {boolean} indexOfMin 是否包含最小值
     * @param {*} maxNumber 最大值
     * @param {boolean} indexOfMax 是否包含最大值
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @param {*} form 表单
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.checkFieldValueRangeRule = function (defname, minNumber, indexOfMin, maxNumber, indexOfMax, errorInfo, primaryModel, form) {
        var value = null;
        var _deField = form.findField(defname);
        if (_deField) {
            value = _deField.getValue();
        }
        if (this.isEmpty(errorInfo)) {
            this.errorInfo = '值必须符合值范围规则';
        }
        else {
            this.errorInfo = errorInfo;
        }
        var isEmpty = IBizUtil.isEmpty(value);
        if (isEmpty) {
            if (primaryModel) {
                throw new Error(this.errorInfo);
            }
            return false;
        }
        var valueFormat = this.checkFieldRegExRule(value, /^-?\d*\.?\d+$/, null, primaryModel, form);
        if (!valueFormat) {
            return false;
        }
        else {
            this.errorInfo = errorInfo;
        }
        var data = Number.parseFloat(value);
        // 小于等于
        if (minNumber !== null) {
            if (indexOfMin) {
                if (data < minNumber) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
            else {
                if (data <= minNumber) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
        }
        // //大于等于
        if (maxNumber != null) {
            if (indexOfMax) {
                if (data > maxNumber) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
            else {
                if (data >= maxNumber) {
                    if (primaryModel) {
                        throw new Error(this.errorInfo);
                    }
                    return false;
                }
            }
        }
        this.errorInfo = '';
        return true;
    };
    /**
     * 检查属性值系统值范围规则  暂时支持正则表达式
     *
     * @static
     * @param {string} defname 属性名称
     * @param {*} strReg 正则表达式
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @param {*} form 表单
     * @returns {boolean}
     * @memberof IBizUtil
     */
    IBizUtil.checkFieldSysValueRule = function (defname, strReg, errorInfo, primaryModel, form) {
        return this.checkFieldRegExRule(defname, strReg, errorInfo, primaryModel, form);
    };
    /**
     * 将文本格式的xml转换为dom模式
     *
     * @static
     * @param {string} strXml
     * @memberof IBizUtil
     */
    IBizUtil.parseXML = function (strXml) {
        if (strXml) {
            return new DOMParser().parseFromString(strXml, 'text/xml');
        }
    };
    /**
     * 将xml转换为object对象
     *
     * @static
     * @param {*} node
     * @param {*} [obj={}]
     * @memberof IBizUtil
     */
    IBizUtil.loadXMLNode = function (node, obj) {
        if (obj === void 0) { obj = {}; }
        if (node && node.attributes) {
            var arr = node.attributes;
            for (var i = 0; i < arr.length; i++) {
                var A = arr.item(i).name;
                var B = arr.item(i).value;
                A = A.toLowerCase();
                obj[A] = B;
            }
        }
    };
    /**
     * 将object转换为xml对象
     *
     * @static
     * @param {any} XML
     * @param {any} obj
     * @memberof IBizUtil
     */
    IBizUtil.saveXMLNode = function (XML, obj) {
        var proName = '';
        for (proName in obj) {
            var value = obj[proName];
            if (!value || value instanceof Object || typeof (value) === 'function') {
                continue;
            }
            var proValue = obj[proName].toString();
            if (proValue !== '') {
                XML.attrib(proName, proValue);
            }
        }
    };
    /**
     *  矩阵 URLJson 格式化
     *
     * @static
     * @param {string} params
     * @returns {*}
     * @memberof IBizUtil
     */
    IBizUtil.matrixURLToJson = function (params) {
        if (params === void 0) { params = ''; }
        var data = {};
        var params_arr = params.split(';');
        params_arr.forEach(function (_data, index) {
            if ((index === params_arr.length - 1) && _data.indexOf('?') !== -1) {
                _data = _data.substr(0, _data.indexOf('?'));
            }
            var _data_arr = _data.split('=').slice();
            data[_data_arr[0]] = _data_arr[1];
        });
        return data;
    };
    /**
     * 名称格式化
     *
     * @static
     * @param {string} name
     * @returns {string}
     * @memberof IBizUtil
     */
    IBizUtil.srfFilePath2 = function (name) {
        if (!name || (name && Object.is(name, ''))) {
            throw new Error('名称异常');
        }
        name = name.replace(/[_]/g, '-');
        var state = 0;
        var _str = '';
        var uPattern = /^[A-Z]{1}$/;
        var str1 = name.substring(0, 1);
        var str2 = name.substring(1);
        state = uPattern.test(str1) ? 1 : 0;
        _str = "" + _str + str1.toLowerCase();
        for (var _i = 0, str2_1 = str2; _i < str2_1.length; _i++) {
            var chr = str2_1[_i];
            if (uPattern.test(chr)) {
                if (state === 1) {
                    _str = "" + _str + chr.toLowerCase();
                }
                else {
                    _str = _str + "-" + chr.toLowerCase();
                }
                state = 1;
            }
            else {
                _str = "" + _str + chr.toLowerCase();
                state = 0;
            }
        }
        _str = _str.replace(/---/g, '-').replace(/--/g, '-');
        return _str;
    };
    /**
     * 错误提示信息
     *
     * @static
     * @type {string}
     * @memberof IBizUtil
     */
    IBizUtil.errorInfo = '';
    return IBizUtil;
}());
Vue.prototype.IBizUtil = IBizUtil;

"use strict";
/**
 * IBizHttp net 对象
 * 调用 getInstance() 获取实例
 *
 * @class IBizHttp
 */
var IBizHttp = /** @class */ (function () {
    /**
     * Creates an instance of IBizHttp.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof IBizHttp
     */
    function IBizHttp() {
        /**
         * 是否正在加载中
         *
         * @type {boolean}
         * @memberof IBizHttp
         */
        this.isLoading = false;
        /**
         * 统计加载
         *
         * @type {number}
         * @memberof IBizHttp
         */
        this.loadingCount = 0;
    }
    /**
     * 获取 IBizHttp 单例对象
     *
     * @static
     * @returns {IBizHttp}
     * @memberof IBizHttp
     */
    IBizHttp.getInstance = function () {
        if (!IBizHttp.iBizHttp) {
            IBizHttp.iBizHttp = new IBizHttp();
        }
        return this.iBizHttp;
    };
    /**
     * post请求
     *
     * @param {string} url 请求路径
     * @param {*} [params={}] 请求参数
     * @returns {Subject<any>} 可订阅请求对象
     * @memberof IBizHttp
     */
    IBizHttp.prototype.post = function (url, params) {
        if (params === void 0) { params = {}; }
        var _this = this;
        var subject = new rxjs.Subject();
        var win = window;
        var iBizApp = win.getIBizApp();
        if (!Object.is(iBizApp.getAppData(), '')) {
            Object.assign(params, { srfappdata: iBizApp.getAppData() });
        }
        var _strParams = _this.transformationOpt(params);
        var _url = '';
        var BaseUrl = IBizEnvironment.BaseUrl;
        _url = "/" + BaseUrl + url;
        _this.beginLoading();
        axios({
            method: 'post',
            url: _url,
            data: _strParams,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8', 'Accept': 'application/json' },
            transformResponse: [function (data) {
                    var _data = null;
                    try {
                        _data = JSON.parse(JSON.parse(JSON.stringify(data)));
                    }
                    catch (error) {
                    }
                    return _data;
                }],
        }).then(function (data) {
            _this.endLoading();
            if (data && data.notlogin) {
                return;
            }
            if (data.ret !== 0) {
                data.failureType = 'CLIENT_INVALID';
                data.info = data.info ? data.info : '本地网络异常，请重试';
                data.info = data.errorMessage ? data.errorMessage : '本地网络异常，请重试';
            }
            IBizUtil.processResult(data);
            subject.next(data);
        }).catch(function (error) {
            _this.endLoading();
            subject.error(error);
        });
        return subject;
    };
    /**
     * post请求,不处理loading加载
     *
     * @param {string} url
     * @param {*} [params={}]
     * @returns {Subject<any>}
     * @memberof IBizHttp
     */
    IBizHttp.prototype.post2 = function (url, params) {
        if (params === void 0) { params = {}; }
        var subject = new rxjs.Subject();
        var iBizApp = IBizApp.getInstance();
        if (!Object.is(iBizApp.getAppData(), '')) {
            Object.assign(params, { srfappdata: iBizApp.getAppData() });
        }
        var _strParams = this.transformationOpt(params);
        var _url = '';
        var BaseUrl = IBizEnvironment.BaseUrl;
        _url = "/" + BaseUrl + url;
        axios({
            method: 'post',
            url: _url,
            data: _strParams,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8', 'Accept': 'application/json' },
            transformResponse: [function (data) {
                    var _data = null;
                    try {
                        _data = JSON.parse(JSON.parse(JSON.stringify(data)));
                    }
                    catch (error) {
                    }
                    return _data;
                }],
        }).then(function (data) {
            if (data && data.notlogin) {
                return;
            }
            if (data.ret !== 0) {
                data.failureType = 'CLIENT_INVALID';
                data.info = data.info ? data.info : '本地网络异常，请重试';
                data.info = data.errorMessage ? data.errorMessage : '本地网络异常，请重试';
            }
            IBizUtil.processResult(data);
            subject.next(data);
        }).catch(function (error) {
            subject.error(error);
        });
        return subject;
    };
    /**
     * get请求
     *
     * @param {string} url 请求路径
     * @param {*} [params={}] 请求参数
     * @returns {Subject<any>} 可订阅请求对象
     * @memberof IBizHttp
     */
    IBizHttp.prototype.get = function (url, params) {
        if (params === void 0) { params = {}; }
        var _this = this;
        var subject = new rxjs.Subject();
        var iBizApp = IBizApp.getInstance();
        if (!Object.is(iBizApp.getAppData(), '')) {
            Object.assign(params, { srfappdata: iBizApp.getAppData() });
        }
        if (Object.keys(params).length > 0) {
            var _strParams = _this.transformationOpt(params);
            url = url.indexOf('?') ? url + "&" + _strParams : url + "?&" + _strParams;
        }
        var _url = '';
        var BaseUrl = IBizEnvironment.BaseUrl;
        _url = "/" + BaseUrl + url;
        _this.beginLoading();
        axios.get(_url).then(function (response) {
            _this.endLoading();
            subject.next(response);
        }).catch(function (error) {
            _this.endLoading();
            subject.error(error);
        });
        return subject;
    };
    /**
     * 请求参数转义处理
     *
     * @private
     * @param {*} [opt={}]
     * @returns {string}
     * @memberof IBizHttp
     */
    IBizHttp.prototype.transformationOpt = function (opt) {
        if (opt === void 0) { opt = {}; }
        var params = {};
        var postData = [];
        Object.assign(params, opt);
        var keys = Object.keys(params);
        keys.forEach(function (key) {
            var val = params[key];
            if (val instanceof Array || val instanceof Object) {
                postData.push(key + "=" + encodeURIComponent(JSON.stringify(val)));
            }
            else {
                postData.push(key + "=" + encodeURIComponent(val));
            }
        });
        return postData.join('&');
    };
    /**
     * 开始加载
     *
     * @private
     * @memberof IBizHttp
     */
    IBizHttp.prototype.beginLoading = function () {
        var _this_1 = this;
        if (this.loadingCount === 0) {
            setTimeout(function () {
                _this_1.isLoading = true;
            });
        }
        this.loadingCount++;
    };
    /**
     * 加载结束
     *
     * @private
     * @memberof IBizHttp
     */
    IBizHttp.prototype.endLoading = function () {
        var _this_1 = this;
        if (this.loadingCount > 0) {
            this.loadingCount--;
        }
        if (this.loadingCount === 0) {
            setTimeout(function () {
                _this_1.isLoading = false;
            });
        }
    };
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {IBizHttp}
     * @memberof IBizHttp
     */
    IBizHttp.iBizHttp = null;
    return IBizHttp;
}());

"use strict";
/**
 * 守卫认证
 *
 * @class IBizAuthguard
 */
var IBizAuthguard = /** @class */ (function () {
    /**
     * Creates an instance of IBizAuthguard.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof IBizAuthguard
     */
    function IBizAuthguard() {
    }
    /**
     * 获取单例
     *
     * @static
     * @returns {IBizAuthguard}
     * @memberof IBizAuthguard
     */
    IBizAuthguard.getInstance = function () {
        if (!IBizAuthguard.iBizAuthguard) {
            IBizAuthguard.iBizAuthguard = new IBizAuthguard();
        }
        return this.iBizAuthguard;
    };
    /**
     * 激活守卫处理
     *
     * @param {string} url
     * @returns {Subject<boolean>}
     * @memberof IBizAuthguard
     */
    IBizAuthguard.prototype.canActivate = function (url, params) {
        if (params === void 0) { params = {}; }
        var subject = new rxjs.Subject();
        var http = IBizHttp.getInstance();
        var opt = { srfaction: 'loadappdata' };
        Object.assign(opt, params);
        var _params = [];
        Object.keys(params).forEach(function (name) {
            if (params[name] && !Object.is(params[name], '')) {
                try {
                    JSON.parse(params[name]);
                }
                catch (e) {
                    _params.push(name + "=" + params[name]);
                }
            }
        });
        url = url.indexOf('?') !== -1 ? url + "&" + _params.join('&') : url + "?" + _params.join('&');
        http.post(url, opt).subscribe(function (success) {
            if (success && success.ret === 0) {
                if (success.remotetag) {
                    var win = window;
                    var iBizApp = win.getIBizApp();
                    iBizApp.setAppData(success.remotetag);
                }
            }
            subject.next(true);
        }, function (error) {
            subject.next(true);
        });
        return subject;
    };
    /**
     * 单列变量
     *
     * @private
     * @static
     * @type {IBizAuthguard}
     * @memberof IBizAuthguard
     */
    IBizAuthguard.iBizAuthguard = null;
    return IBizAuthguard;
}());

"use strict";
/**
 * UI信息提示与交互确认类
 *
 * @class IBizNotification
 */
var IBizNotification = /** @class */ (function () {
    /**
     * Creates an instance of IBizNotification.
     * 创建 IBizNotification 实例 私有构造，拒绝通过 new 创建对象
     * @param {*} [opts={}]
     * @memberof IBizNotification
     */
    function IBizNotification(opts) {
        if (opts === void 0) { opts = {}; }
    }
    /**
     * 获取 IBizNotification 单例对象
     *
     * @static
     * @returns {IBizNotification}
     * @memberof IBizNotification
     */
    IBizNotification.getInstance = function () {
        if (!IBizNotification.iBizNotification) {
            IBizNotification.iBizNotification = new IBizNotification();
        }
        return this.iBizNotification;
    };
    /**
     * 信息
     *
     * @param {string} title 标题
     * @param {string} desc 内容
     * @param {number} [duration] 关闭时间，默认4.5秒 （可选）
     * @memberof IBizNotification
     */
    IBizNotification.prototype.info = function (title, desc, duration) {
        iview.Notice.info({ title: title, desc: desc, duration: duration ? duration : 4.5 });
    };
    /**
     * 成功
     *
     * @param {string} title 标题
     * @param {string} desc 内容
     * @param {number} [duration] 关闭时间，默认4.5秒 （可选）
     * @memberof IBizNotification
     */
    IBizNotification.prototype.success = function (title, desc, duration) {
        iview.Notice.success({ title: title, desc: desc, duration: duration ? duration : 4.5 });
    };
    /**
     * 警告
     *
     * @param {string} title 标题
     * @param {string} desc 内容
     * @param {number} [duration] 关闭时间，默认4.5秒 （可选）
     * @memberof IBizNotification
     */
    IBizNotification.prototype.warning = function (title, desc, duration) {
        iview.Notice.warning({ title: title, desc: desc, duration: duration ? duration : 4.5 });
    };
    /**
     * 错误
     *
     * @param {string} title 标题
     * @param {string} desc 内容
     * @param {number} [duration] 关闭时间，默认4.5秒 （可选）
     * @memberof IBizNotification
     */
    IBizNotification.prototype.error = function (title, desc, duration) {
        iview.Notice.error({ title: title, desc: desc, duration: duration ? duration : 4.5 });
    };
    /**
     * 确认对话框
     *
     * @param {string} title 标题
     * @param {string} contant 内容
     * @returns {Subject<any>} 可订阅对象
     * @memberof IBizNotification
     */
    IBizNotification.prototype.confirm = function (title, contant) {
        var subject = new rxjs.Subject();
        iview.Modal.confirm({
            title: title,
            content: contant,
            onOk: function () {
                subject.next('OK');
            },
            onCancel: function () {
                subject.next('CANCEL');
            }
        });
        return subject;
    };
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {IBizNotification}
     * @memberof IBizNotification
     */
    IBizNotification.iBizNotification = null;
    return IBizNotification;
}());

"use strict";
/**
 * IBizSys控制器借口对象
 *
 * @abstract
 * @class IBizObject
 */
var IBizObject = /** @class */ (function () {
    /**
     * Creates an instance of IBizObject.
     * 创建 IBizObject 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizObject
     */
    function IBizObject(opts) {
        if (opts === void 0) { opts = {}; }
        /**
         * 对象事件集合
         *
         * @private
         * @type {Map<string, any>}
         * @memberof IBizObject
         */
        this.events = new Map();
        /**
         * IBizHttp 服务对象
         *
         * @type {IBizHttp}
         * @memberof IBizObject
         */
        this.iBizHttp = IBizHttp.getInstance();
        /**
         * IBiz 通知对象
         *
         * @type {IBizNotification}
         * @memberof IBizObject
         */
        this.iBizNotification = IBizNotification.getInstance();
        /**
         * 对象id
         *
         * @private
         * @type {string}
         * @memberof IBizObject
         */
        this.id = '';
        /**
         * 对象name
         *
         * @private
         * @type {string}
         * @memberof IBizObject
         */
        this.name = '';
        this.Id = opts.id;
        this.Name = opts.name;
    }
    Object.defineProperty(IBizObject.prototype, "Id", {
        /**
         * 获取id
         *
         * @type {string}
         * @memberof IBizObject
         */
        get: function () {
            return this.id;
        },
        /**
         * 设置id
         *
         * @memberof IBizObject
         */
        set: function (id) {
            this.id = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IBizObject.prototype, "Name", {
        /**
         * 获取name
         *
         * @type {string}
         * @memberof IBizObject
         */
        get: function () {
            return this.name;
        },
        /**
         * 设置name
         *
         * @memberof IBizObject
         */
        set: function (name) {
            this.name = name;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * 获取id
     *
     * @returns {string}
     * @memberof IBizObject
     */
    IBizObject.prototype.getId = function () {
        return this.id;
    };
    /**
     * 获取名称
     *
     * @returns {string}
     * @memberof IBizObject
     */
    IBizObject.prototype.getName = function () {
        return this.name;
    };
    /**
     * 注册Rx订阅事件
     *
     * @param {string} eventName
     * @returns {Subject<any>}
     * @memberof IBizObject
     */
    IBizObject.prototype.on = function (eventName) {
        var subject;
        if (eventName && !Object.is(eventName, '')) {
            if (!this.events.get(eventName)) {
                subject = new rxjs.Subject();
                this.events.set(eventName, subject);
            }
            else {
                subject = this.events.get(eventName);
            }
            return subject;
        }
    };
    /**
     * Rx事件流触发
     *
     * @param {string} eventName
     * @param {*} data
     * @memberof IBizObject
     */
    IBizObject.prototype.fire = function (eventName, data) {
        var subject = this.events.get(eventName);
        if (subject) {
            subject.next(data);
        }
    };
    return IBizObject;
}());

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
/**
 * 代码表对象
 *
 * @class IBizCodeList
 * @extends {IBizObject}
 */
var IBizCodeList = /** @class */ (function (_super) {
    __extends(IBizCodeList, _super);
    /**
     * Creates an instance of IBizCodeList.
     * 创建 IBizCodeList 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizCodeList
     */
    function IBizCodeList(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 静态代码表数据
         *
         * @type {Array<any>}
         * @memberof IBizCodeList
         */
        _this.data = [];
        _this.data = opts.datas.slice();
        return _this;
    }
    /**
     * 获取代码表数据
     *
     * @returns {Array<any>}
     * @memberof IBizCodeList
     */
    IBizCodeList.prototype.getData = function () {
        return this.data;
    };
    /**
     * 获取数据项的值
     *
     * @param {string} value
     * @param {*} cascade
     * @returns {*}
     * @memberof IBizCodeList
     */
    IBizCodeList.prototype.getItemByValue = function (value, cascade) {
        var result = {};
        var arr = this.data.filter(function (item) { return Object.is(item.value, value); });
        if (arr.length !== 1) {
            return undefined;
        }
        result = __assign({}, arr[0]);
        return result;
    };
    /**
     * 获取代码表文本值
     *
     * @param {*} [item={}]
     * @returns {string}
     * @memberof IBizCodeList
     */
    IBizCodeList.prototype.getCodeItemText = function (item) {
        if (item === void 0) { item = {}; }
        var color = item.color;
        var textCls = item.textcls;
        var iconCls = item.iconcls;
        var realText = item.text;
        var ret = '';
        if (iconCls) {
            ret = "<i class='" + iconCls + "'></i>&nbsp;";
        }
        if (textCls || color) {
            ret = ret + "<span class='" + (textCls ? textCls : '') + "' style='color: " + (color ? color : '') + "'>" + realText + "</span>";
        }
        else {
            ret += realText;
        }
        return ret;
    };
    return IBizCodeList;
}(IBizObject));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 计数器
 *
 * @class IBizUICounter
 * @extends {IBizControl}
 */
var IBizUICounter = /** @class */ (function (_super) {
    __extends(IBizUICounter, _super);
    /**
     * Creates an instance of IBizUICounter.
     * 创建 IBizUICounter 服务对象
     *
     * @param {*} [config={}]
     * @memberof IBizUICounter
     */
    function IBizUICounter(config) {
        if (config === void 0) { config = {}; }
        var _this = _super.call(this, config) || this;
        /**
         * 定时器时间
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.timer = null;
        /**
         * 定时器
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.timerTag = null;
        /**
         * 计数器id
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.counterId = '';
        /**
         * 计数器参数
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.counterParam = {};
        /**
         * 最后加载数据
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.lastReloadArg = {};
        /**
         * 计数器结果
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.result = {};
        /**
         * 计数器交互数据
         *
         * @private
         * @type {*}
         * @memberof IBizUICounter
         */
        _this.data = {};
        /**
         * url
         *
         * @type {string}
         * @memberof IBizUICounter
         */
        _this.url = '';
        _this.counterId = config.counterId;
        Object.assign(_this.counterParam, config.counterParam);
        _this.timer = config.timer;
        _this.url = config.url;
        _this.load();
        return _this;
    }
    /**
     * 加载定时器
     *
     * @memberof IBizUICounter
     */
    IBizUICounter.prototype.load = function () {
        var _this = this;
        if (this.timer > 1000) {
            this.timerTag = setInterval(function () {
                _this.reload();
            }, this.timer);
        }
        this.reload();
    };
    /**
     * 刷新计数器
     *
     * @private
     * @param {*} [arg={}]
     * @memberof IBizUICounter
     */
    IBizUICounter.prototype.reload = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        Object.assign(this.lastReloadArg, arg);
        Object.assign(params, this.lastReloadArg);
        Object.assign(params, { srfcounterid: this.counterId, srfaction: 'FETCH', srfcounterparam: JSON.stringify(this.counterParam) });
        this.iBizHttp.post2(this.url, params).subscribe(function (res) {
            if (res.ret === 0) {
                _this.setData(res);
            }
        }, function (error) {
            console.log('加载计数器异常');
        });
    };
    /**
     * 处理数据
     *
     * @private
     * @param {*} result
     * @memberof IBizUICounter
     */
    IBizUICounter.prototype.setData = function (result) {
        this.result = result;
        this.data = result.data;
        this.fire(IBizUICounter.COUNTERCHANGED, this.data);
    };
    /**
     * 获取结果
     *
     * @returns {*}
     * @memberof IBizUICounter
     */
    IBizUICounter.prototype.getResult = function () {
        return this.result;
    };
    /**
     * 获取数据
     *
     * @returns {*}
     * @memberof IBizUICounter
     */
    IBizUICounter.prototype.getData = function () {
        return this.data;
    };
    /**
     * 关闭计数器
     *
     * @memberof IBizUICounter
     */
    IBizUICounter.prototype.close = function () {
        if (this.timerTag !== undefined) {
            clearInterval(this.timerTag);
            delete this.timer;
        }
    };
    /**
     * 计数发生变化
     *
     * @static
     * @memberof IBizUICounter
     */
    IBizUICounter.COUNTERCHANGED = "COUNTERCHANGED";
    return IBizUICounter;
}(IBizObject));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单属性对象<主要管理属性及其标签的值、可用、显示、必填等操作>
 *
 * @class IBizFormItem
 * @extends {IBizObject}
 */
var IBizFormItem = /** @class */ (function (_super) {
    __extends(IBizFormItem, _super);
    /**
     * Creates an instance of IBizFormItem.
     * 创建 IBizFormItem 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormItem
     */
    function IBizFormItem(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否是必填
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.allowEmpty = false;
        /**
         * 属性动态配置值<代码表>
         *
         * @type {Array<any>}
         * @memberof IBizFormItem
         */
        _this.config = [];
        /**
         * 标题
         *
         * @type {string}
         * @memberof IBizFormItem
         */
        _this.caption = '';
        /**
         * 属性动态配置值<用户字典>
         *
         * @type {Array<any>}
         * @memberof IBizFormItem
         */
        _this.dictitems = [];
        /**
         * 表单项是否禁用
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.disabled = false;
        /**
         * 标签是否为空
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.emptyCaption = false;
        /**
         * 表达校验错误信息
         *
         * @type {string}
         * @memberof IBizFormItem
         */
        _this.errorInfo = '';
        /**
         *表单项类型
         *
         * @private
         * @type {string}
         * @memberof IBizFormItem
         */
        _this.fieldType = '';
        /**
         * 错误类型
         *
         * @private
         * @type {('FRONTEND' | 'BACKEND' | '')}
         * @memberof IBizFormItem
         */
        _this.errorType = '';
        /**
         * 表单对象
         *
         * @private
         * @type {*}
         * @memberof IBizFormItem
         */
        _this.form = null;
        /**
         * 隐藏表单项
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.hidden = false;
        /**
         * 是否有错误信息
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.hasError = false;
        /**
         * 是否显示标题
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.showCaption = true;
        /**
         * 表单项校验状态
         *
         * @type {string}
         * @memberof IBizFormItem
         */
        _this.validateStatus = 'success';
        /**
         * 是否可见
         *
         * @type {boolean}
         * @memberof IBizFormItem
         */
        _this.visible = true;
        /**
         * 表单项的值
         *
         * @private
         * @type {string}
         * @memberof IBizFormItem
         */
        _this._value = '';
        /**
         * 表单项当前权限模式
         *
         * @private
         * @type {number}
         * @memberof IBizFormItem
         */
        _this.jurisdiction = IBizEnvironment.formItemPrivTag ? 2 : 1;
        /**
         * 表单项无权限默认显示模式
         *
         * @private
         * @type {number}
         * @memberof IBizFormItem
         */
        _this.noPrivDisplayMode = 1;
        _this.allowEmpty = opts.allowEmpty ? true : false;
        _this.caption = opts.caption;
        _this.disabled = opts.disabled ? true : false;
        _this.emptyCaption = opts.emptyCaption ? true : false;
        _this.fieldType = opts.fieldType;
        _this.form = opts.form;
        _this.hidden = opts.hidden ? true : false;
        _this.showCaption = opts.showCaption ? true : false;
        _this.visible = opts.visible ? true : false;
        _this.noPrivDisplayMode = opts.noPrivDisplayMode;
        return _this;
    }
    Object.defineProperty(IBizFormItem.prototype, "value", {
        /**
         * 获取值
         *
         * @type {string}
         * @memberof IBizFormItem
         */
        get: function () {
            return this._value ? this._value : '';
        },
        /**
         * 设置值
         *
         * @memberof IBizFormItem
         */
        set: function (val) {
            var oldVal = this._value;
            this._value = val;
            if (oldVal !== this._value) {
                this.onValueChanged(oldVal);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * 获取表单项类型
     *
     * @returns {string}
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.getFieldType = function () {
        return this.fieldType;
    };
    /**
     * 设置表单对象
     *
     * @param {*} form
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setForm = function (form) {
        this.form = form;
    };
    /**
     * 获取表单对象
     *
     * @returns {*}
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.getForm = function () {
        return this.form;
    };
    /**
     * 获取值
     *
     * @returns {*}
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.getValue = function () {
        return this.value;
    };
    /**
     * 设置值
     *
     * @param {string} value
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setValue = function (value) {
        this.value = value;
    };
    /**
     * 是否启用
     *
     * @returns {boolean}
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.isDisabled = function () {
        return this.disabled;
    };
    /**
     * 设置是否启用
     *
     * @param {boolean} disabled
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setDisabled = function (disabled) {
        if (IBizEnvironment.formItemPrivTag) {
            this.disabled = (this.jurisdiction === 0 || this.jurisdiction === 1) ? true : disabled;
        }
        else {
            this.disabled = this.jurisdiction === 0 ? true : disabled;
        }
    };
    /**
     * 设置是否拥有权限
     *
     * @param {string} jurisdiction
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setJurisdiction = function (jurisdiction) {
        try {
            this.jurisdiction = parseInt(jurisdiction, 10);
        }
        catch (error) {
            console.warn(error);
        }
        this.visible = (this.jurisdiction !== 0 || this.noPrivDisplayMode === 1) ? true : false;
        if (IBizEnvironment.formItemPrivTag) {
            this.disabled = (this.jurisdiction === 0 || this.jurisdiction === 1) ? true : false;
        }
        else {
            this.disabled = this.jurisdiction === 0 ? true : false;
        }
    };
    /**
     * 隐藏控件
     *
     * @param {boolean} hidden
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setHidden = function (hidden) {
        this.hidden = hidden;
    };
    /**
     * 设置可见性
     *
     * @param {boolean} visible
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setVisible = function (visible) {
        this.visible = (this.jurisdiction !== 0 || this.noPrivDisplayMode === 1) ? visible : false;
    };
    /**
     * 设置属性动态配置
     *
     * @param {Array<any>} config 代码表
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setAsyncConfig = function (config) {
        if (Array.isArray(config)) {
            this.config = config.slice();
        }
    };
    /**
     * 设置用户字典
     *
     * @param {Array<any>} item
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setDictItems = function (item) {
        if (Array.isArray(item)) {
            this.dictitems = item.slice();
        }
    };
    /**
     * 设置只读<Ext版本方法禁止使用>
     *
     * @param {boolean} readonly
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setReadOnly = function (readonly) {
        this.setDisabled(readonly);
    };
    /**
     * 编辑是否必须输入
     *
     * @param {boolean} allowblank
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setAllowBlank = function (allowblank) {
        this.allowEmpty = allowblank;
    };
    /**
     * 标记表单项值无效提示
     *
     * @param {*} info
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.markInvalid = function (info) {
    };
    /**
     * 设置表单项错误
     *
     * @param {*} info
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setActiveError = function (info) {
        this.markInvalid(info);
    };
    /**
     * 表单项是否有错误
     *
     * @returns {boolean}
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.hasActiveError = function () {
        return this.hasError;
    };
    /**
     * 重置表单项错误
     *
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.unsetActiveError = function () {
        this.hasError = false;
    };
    /**
     * 值变化
     *
     * @param {string} oldValue
     * @param {string} newValue
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.onValueChanged = function (oldValue) {
        this.fire(IBizFormItem.VALUECHANGED, { name: this.getName(), value: oldValue, field: this });
    };
    /**
     * 设置表单项错误信息
     *
     * @param {*} [info={}]
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.setErrorInfo = function (info) {
        if (info === void 0) { info = {}; }
        this.validateStatus = info.validateStatus;
        this.hasError = info.hasError;
        this.errorInfo = info.errorInfo;
        this.errorType = info.errorType;
    };
    /**
     * 获取表单项错误类型
     *
     * @returns {string}
     * @memberof IBizFormItem
     */
    IBizFormItem.prototype.getErrorType = function () {
        return this.errorType;
    };
    /**
     * 值变化
     *
     * @static
     * @memberof IBizFormItem
     */
    IBizFormItem.VALUECHANGED = 'VALUECHANGED';
    return IBizFormItem;
}(IBizObject));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单属性项
 *
 * @class IBizFormField
 * @extends {IBizFormItem}
 */
var IBizFormField = /** @class */ (function (_super) {
    __extends(IBizFormField, _super);
    /**
     * Creates an instance of IBizFormField.
     * 创建 IBizFormField 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormField
     */
    function IBizFormField(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * label 宽度
         *
         * @type {number}
         * @memberof IBizFormField
         */
        _this.labelWidth = 130;
        /**
         * 实体属性输入旧值
         *
         * @private
         * @type {string}
         * @memberof IBizFormField
         */
        _this.oldVal = '';
        /**
         * 编辑器参数
         *
         * @type {*}
         * @memberof IBizFormField
         */
        _this.editorParams = {};
        /**
         * 输入防抖
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizFormField
         */
        _this.subject = new rxjs.Subject();
        _this.labelWidth = opts.labelWidth;
        if (opts.editorParams) {
            Object.assign(_this.editorParams, opts.editorParams);
        }
        _this.subject
            .pipe(rxjs.operators.debounceTime(300), rxjs.operators.distinctUntilChanged()).subscribe(function (data) {
            _this.value = data;
        });
        return _this;
    }
    /**
     * 值设定
     *
     * @memberof IBizFormField
     */
    IBizFormField.prototype.setData = function ($event) {
        if (!event) {
            return;
        }
        if (!$event.target) {
            return;
        }
        this.subject.next($event.target.value);
    };
    /**
     * 设置旧值
     *
     * @param {string} val
     * @memberof IBizFormField
     */
    IBizFormField.prototype.setOldValue = function (val) {
        this.oldVal = val;
    };
    /**
     * 获取旧值
     *
     * @returns {string}
     * @memberof IBizFormField
     */
    IBizFormField.prototype.getOldValue = function () {
        return this.oldVal;
    };
    /**
     * 属性值变化
     *
     * @param {*} event
     * @memberof IBizFormField
     */
    IBizFormField.prototype.valueChange = function (event) {
        if (Object.is(event, this.value)) {
            return;
        }
        this.value = event;
    };
    /**
     * 输入框失去焦点触发
     *
     * @param {*} _value
     * @returns {void}
     * @memberof IBizFormField
     */
    IBizFormField.prototype.onBlur = function (_value) {
        if (!_value || Object.is(_value, this.value)) {
            return;
        }
        this.value = _value;
    };
    /**
     * 键盘事件
     *
     * @param {*} $event
     * @returns {void}
     * @memberof IBizFormField
     */
    IBizFormField.prototype.onKeydown = function ($event) {
        if (!$event) {
            return;
        }
        if ($event.keyCode !== 13) {
            return;
        }
        if (Object.is($event.target.value, this.value)) {
            return;
        }
        this.value = $event.target.value;
    };
    return IBizFormField;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单关系部件
 *
 * @class IBizFormDRPanel
 * @extends {IBizFormItem}
 */
var IBizFormDRPanel = /** @class */ (function (_super) {
    __extends(IBizFormDRPanel, _super);
    /**
     * Creates an instance of IBizFormDRPanel.
     * 创建 IBizFormDRPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormDRPanel
     */
    function IBizFormDRPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否开启遮罩
         *
         * @type {boolean}
         * @memberof IBizFormDRPanel
         */
        _this.blockUI = false;
        /**
         * 遮罩提示信息
         *
         * @type {string}
         * @memberof IBizFormDRPanel
         */
        _this.blockUITipInfo = '请先保存主数据';
        /**
         * 刷新数据
         *
         * @private
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.hookItems = {};
        /**
         * 父数据
         *
         * @type {string}
         * @memberof IBizFormDRPanel
         */
        _this.srfParentData = {};
        /**
         * 父模式
         *
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.srfParentMode = {};
        /**
         * 关联数据
         *
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.srfReferData = {};
        /**
         * 刷新关联视图计数
         *
         * @type {number}
         * @memberof IBizFormDRPanel
         */
        _this.refreshRefView = 0;
        /**
         * 关系表单项值变化，刷新关联视图
         *
         * @type {string}
         * @memberof IBizFormDRPanel
         */
        _this.refreshItems = '';
        /**
         * 关系页触发保存
         *
         * @type {Number}
         * @memberof IBizFormDRPanel
         */
        _this.saveRefView = 0;
        /**
         * 关系页是否值变更
         *
         * @type {boolean}
         * @memberof IBizFormDRPanel
         */
        _this.refViewDirty = false;
        /**
         * 关系页视图类型
         *
         * @type {String}
         * @memberof IBizFormDRPanel
         */
        _this.refViewType = '';
        /**
         * 视图参数
         *
         * @private
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.viewParamJO = {};
        /**
         * 是否刷新关系数据
         *
         * @type {boolean}
         * @memberof IBizFormDRPanel
         */
        _this.isRelationalData = true;
        if (opts.dritem.parentmode) {
            Object.assign(_this.srfParentMode, opts.dritem.parentmode);
        }
        if (opts.dritem.paramjo) {
            Object.assign(_this.viewParamJO, opts.dritem.paramjo);
        }
        if (opts.dritem.refviewtype) {
            _this.refViewType = opts.dritem.refviewtype;
        }
        if (opts.refreshitems && !Object.is(opts.refreshitems, '')) {
            _this.refreshItems = opts.refreshitems;
        }
        Object.assign(_this.srfParentMode, _this.viewParamJO);
        _this.initDRPanel();
        return _this;
    }
    /**
     * 初始化关系面板
     *
     * @private
     * @memberof IBizFormDRPanel
     */
    IBizFormDRPanel.prototype.initDRPanel = function () {
        var _this = this;
        if (this.form) {
            var form_1 = this.form;
            form_1.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                _this.srfReferData = {};
                var srfReferData = form_1.getActiveData();
                Object.assign(_this.srfReferData, { srfreferdata: JSON.stringify(srfReferData) });
            });
            form_1.on(IBizForm.FORMLOADED).subscribe(function (data) {
                _this.refreshDRUIPart();
            });
            // 表单保存之前
            form_1.on(IBizEditForm.FORMBEFORESAVE).subscribe(function (data) {
                return _this.saveDRData(data);
            });
            // 表单保存完成
            form_1.on(IBizForm.FORMSAVED).subscribe(function (data) {
                _this.refreshDRUIPart();
            });
            // 表单项更新
            form_1.on(IBizForm.UPDATEFORMITEMS).subscribe(function (data) {
                if (!data.data) {
                    return;
                }
                var refreshRefview = false;
                Object.keys(data.data).some(function (name) {
                    if (_this.isRefreshItem(name)) {
                        refreshRefview = true;
                        return true;
                    }
                });
                if (refreshRefview) {
                    _this.refreshDRUIPart();
                }
            });
            var items = this.refreshItems.split(';');
            if (items.length > 0) {
                items.forEach(function (item) {
                    _this.hookItems[item.toLowerCase()] = true;
                });
                form_1.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                    if (form_1.$ignoreformfieldchange) {
                        return;
                    }
                    var fieldname = '';
                    if (data != null) {
                        fieldname = data.name;
                    }
                    else {
                        return;
                    }
                    if (_this.isRefreshItem(fieldname)) {
                        _this.refreshDRUIPart();
                    }
                });
            }
        }
    };
    /**
     * 保存关系数据，继续父数据保存，返回true,否则返回false
     *
     * @returns {boolean}
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.saveDRData = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (Object.is(this.refViewType, 'DEMEDITVIEW9') && this.refViewDirty) {
            this.saveRefView++;
            arg.srfcancel = true;
            return;
        }
        arg.srfcancel = false;
    };
    /**
     * 是否是刷新数据项
     *
     * @param {string} name
     * @returns {boolean}
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.isRefreshItem = function (name) {
        if (!name || name === '') {
            return false;
        }
        if (this.hookItems[name.toLowerCase()]) {
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * 刷新关系数据
     *
     * @returns {void}
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.refreshDRUIPart = function () {
        var _this = this;
        if (Object.is(this.srfParentMode.srfparenttype, 'CUSTOM')) {
            this.isRelationalData = false;
        }
        var form = this.form;
        var _field = form.findField('srfkey');
        if (!_field) {
            return;
        }
        var _value = _field.getValue();
        if (this.isRelationalData) {
            if (!_value || _value == null || Object.is(_value, '')) {
                this.blockUIStart();
                return;
            }
            else {
                this.blockUIStop();
            }
        }
        var srfReferData = form.getActiveData();
        this.srfParentData = {};
        this.srfReferData = {};
        Object.assign(this.srfParentData, { srfparentkey: _value });
        Object.assign(this.srfReferData, { srfreferdata: JSON.stringify(srfReferData) });
        setTimeout(function () {
            _this.refreshRefView += 1;
        }, 10);
    };
    /**
     * 开启遮罩
     *
     * @private
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.blockUIStart = function () {
        this.blockUI = true;
    };
    /**
     * 关闭遮罩
     *
     * @private
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.blockUIStop = function () {
        this.blockUI = false;
    };
    /**
     * 关系页数据保存完成
     *
     * @memberof IBizFormDRPanel
     */
    IBizFormDRPanel.prototype.onDrDataSaved = function () {
        this.refViewDirty = false;
        if (this.getForm()) {
            this.getForm().save2();
        }
    };
    /**
     * 关系页发生值变更
     *
     * @memberof IBizFormDRPanel
     */
    IBizFormDRPanel.prototype.onDrDataChange = function (flag) {
        this.refViewDirty = flag;
    };
    return IBizFormDRPanel;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单分组
 *
 * @class IBizFormGroup
 * @extends {IBizFormItem}
 */
var IBizFormGroup = /** @class */ (function (_super) {
    __extends(IBizFormGroup, _super);
    /**
     * Creates an instance of IBizFormGroup.
     * 创建 IBizFormGroup 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormGroup
     */
    function IBizFormGroup(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 部件集合
         *
         * @type {*}
         * @memberof IBizFormGroup
         */
        _this.$editor = {};
        _this.titleBarCloseMode = opts.titleBarCloseMode;
        return _this;
    }
    /**
     * 注册部件
     *
     * @param {string} name
     * @param {*} editor
     * @memberof IBizFormGroup
     */
    IBizFormGroup.prototype.regEditor = function (name, editor) {
        if (name) {
            this.$editor[name] = editor;
        }
    };
    /**
     * 获取指定部件
     *
     * @param {string} name
     * @memberof IBizFormGroup
     */
    IBizFormGroup.prototype.getEditor = function (name) {
        if (name) {
            return this.$editor[name];
        }
        return null;
    };
    /**
     * 设置是否启用
     *
     * @param {boolean} disabled
     * @memberof IBizFormGroup
     */
    IBizFormGroup.prototype.setDisabled = function (disabled) {
        this.disabled = disabled;
    };
    /**
     * 隐藏控件
     *
     * @param {boolean} hidden
     * @memberof IBizFormGroup
     */
    IBizFormGroup.prototype.setHidden = function (hidden) {
        this.hidden = hidden;
    };
    /**
     * 设置可见性
     *
     * @param {boolean} visible
     * @memberof IBizFormGroup
     */
    IBizFormGroup.prototype.setVisible = function (visible) {
        this.visible = visible;
    };
    return IBizFormGroup;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 *  表单IFrame部件
 *
 * @class IBizFormIFrame
 * @extends {IBizFormItem}
 */
var IBizFormIFrame = /** @class */ (function (_super) {
    __extends(IBizFormIFrame, _super);
    /**
     * Creates an instance of IBizFormIFrame.
     * 创建 IBizFormIFrame 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormIFrame
     */
    function IBizFormIFrame(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizFormIFrame;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单直接内容
 *
 * @class IBizFormRawItem
 * @extends {IBizFormItem}
 */
var IBizFormRawItem = /** @class */ (function (_super) {
    __extends(IBizFormRawItem, _super);
    /**
     * Creates an instance of IBizFormRawItem.
     * 创建 IBizFormRawItem 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormRawItem
     */
    function IBizFormRawItem(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizFormRawItem;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单分页部件
 *
 * @class IBizFormTabPage
 * @extends {IBizFormItem}
 */
var IBizFormTabPage = /** @class */ (function (_super) {
    __extends(IBizFormTabPage, _super);
    /**
     * Creates an instance of IBizFormTabPage.
     * 创建 IBizFormTabPage 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormTabPage
     */
    function IBizFormTabPage(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizFormTabPage;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单分页面板
 *
 * @class IBizFormTabPanel
 * @extends {IBizFormItem}
 */
var IBizFormTabPanel = /** @class */ (function (_super) {
    __extends(IBizFormTabPanel, _super);
    /**
     * Creates an instance of IBizFormTabPanel.
     * 创建 IBizFormTabPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormTabPanel
     */
    function IBizFormTabPanel(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizFormTabPanel;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单成员按钮
 *
 * @class IBizFormButton
 * @extends {IBizFormItem}
 */
var IBizFormButton = /** @class */ (function (_super) {
    __extends(IBizFormButton, _super);
    /**
     * Creates an instance of IBizFormButton.
     * 创建 IBizFormButton 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormButton
     */
    function IBizFormButton(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 实体界面行为
         *
         * @private
         * @type {*}
         * @memberof IBizFormButton
         */
        _this.uiaction = {};
        /**
         * 表单项更新
         *
         * @private
         * @type {*}
         * @memberof IBizFormButton
         */
        _this.fiupdate = {};
        _this.actiontype = opts.actiontype;
        if (opts.uiaction) {
            Object.assign(_this.uiaction, opts.uiaction);
        }
        if (opts.fiupdate) {
            Object.assign(_this.fiupdate, opts.fiupdate);
        }
        return _this;
    }
    /**
     * 表单成员按钮事件
     *
     * @returns {void}
     * @memberof IBizFormButton
     */
    IBizFormButton.prototype.onClick = function () {
        var form = this.getForm();
        if (!form) {
            return;
        }
        var viewController = form.getViewController();
        if (Object.is(this.actiontype, 'UIACTION') && viewController && Object.keys(this.uiaction).length > 0) {
            var uiaction = viewController.getUIAction(this.uiaction.tag);
            viewController.doUIAction(uiaction);
        }
        if (Object.is(this.actiontype, 'FIUPDATE') && Object.keys(this.fiupdate).length > 0) {
            form.updateFormItems(this.fiupdate.tag);
        }
    };
    return IBizFormButton;
}(IBizFormItem));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 部件对象
 *
 * @class IBizControl
 * @extends {IBizObject}
 */
var IBizControl = /** @class */ (function (_super) {
    __extends(IBizControl, _super);
    /**
     * Creates an instance of IBizControl.
     * 创建 IBizControl 实例。
     *
     * @param {*} [opts={}]
     * @memberof IBizControl
     */
    function IBizControl(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 后台交互URL
         *
         * @private
         * @type {string}
         * @memberof IBizControl
         */
        _this.url = '';
        /**
         * 视图控制器对象
         *
         * @private
         * @type {*}
         * @memberof IBizControl
         */
        _this.viewController = null;
        /**
         * 部件http请求状态
         *
         * @type {boolean}
         * @memberof IBizControl
         */
        _this.isLoading = false;
        /**
         * vue 路由对象
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.$router = null;
        /**
         * vue 实例对象
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.$vue = null;
        /**
         * vue 当前路由对象
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.$route = null;
        /**
         * 部件模型
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.model = {};
        _this.url = opts.url;
        _this.viewController = opts.viewController;
        _this.$route = opts.$route;
        _this.$router = opts.$router;
        _this.$vue = opts.$vue;
        return _this;
    }
    ;
    ;
    /**
     * 获取后台路径
     *
     * @returns {*}
     * @memberof IBizControl
     */
    IBizControl.prototype.getBackendUrl = function () {
        var url = '';
        var viewController = this.getViewController();
        if (this.url) {
            url = this.url;
        }
        if (Object.is(url, '') && viewController) {
            url = viewController.getBackendUrl();
        }
        return url;
    };
    /**
     * 获取视图控制器
     *
     * @returns {*}
     * @memberof IBizControl
     */
    IBizControl.prototype.getViewController = function () {
        if (this.viewController) {
            return this.viewController;
        }
        return undefined;
    };
    /**
     * 部件http请求
     *
     * @memberof IBizControl
     */
    IBizControl.prototype.beginLoading = function () {
        this.isLoading = true;
    };
    /**
     * 部件结束http请求
     *
     * @memberof IBizControl
     */
    IBizControl.prototype.endLoading = function () {
        this.isLoading = false;
    };
    /**
     * 设置部件模型
     *
     * @param {*} model
     * @memberof IBizControl
     */
    IBizControl.prototype.setModel = function (model) {
        if (model === void 0) { model = {}; }
        this.model = model;
    };
    /**
     * 获取部件模型
     *
     * @returns {*}
     * @memberof IBizControl
     */
    IBizControl.prototype.getModel = function () {
        return this.model;
    };
    /**
     * 部件模型变化
     *
     * @static
     * @memberof IBizControl
     */
    IBizControl.CONTROLMODELCHANGE = 'CONTROLMODELCHANGE';
    return IBizControl;
}(IBizObject));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 应用菜单
 *
 * @class IBizAppMenu
 * @extends {IBizControl}
 */
var IBizAppMenu = /** @class */ (function (_super) {
    __extends(IBizAppMenu, _super);
    /**
     * Creates an instance of IBizAppMenu.
     * 创建 IBizAppMenu 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizAppMenu
     */
    function IBizAppMenu(opts) {
        if (opts === void 0) { opts = {}; }
        var _this_1 = _super.call(this, opts) || this;
        /**
         * 应用功能数据
         *
         * @type {Array<any>}
         * @memberof IBizAppMenu
         */
        _this_1.appFunctions = [];
        /**
         * 展开数据项
         *
         * @type {string[]}
         * @memberof IBizAppMenu
         */
        _this_1.defaultOpeneds = [];
        /**
         * 菜单数据项
         *
         * @type {any[]}
         * @memberof IBizAppMenu
         */
        _this_1.items = [];
        /**
         * 导航树部件是否收缩，默认展开
         *
         * @type {boolean}
         * @memberof IBizAppMenu
         */
        _this_1.isCollapsed = true;
        /**
         * 菜单宽度
         *
         * @type {number}
         * @memberof IBizAppMenu
         */
        _this_1.width = 240;
        /**
         * 选中项
         *
         * @type {*}
         * @memberof IBizAppMenu
         */
        _this_1.selectItem = {};
        _this_1.setAppFunctions();
        return _this_1;
    }
    /**
     * 设置应用功能参数
     *
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.setAppFunctions = function () {
    };
    /**
     *
     *
     * @param {string} [appfuncid]
     * @param {string} [routepath]
     * @returns {*}
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.getAppFunction = function (appfuncid, routepath) {
        var appfunc = {};
        this.appFunctions.some(function (_appFunction) {
            if (_appFunction === void 0) { _appFunction = {}; }
            if (appfuncid && Object.is(appfuncid, _appFunction.appfuncid)) {
                Object.assign(appfunc, _appFunction);
                return true;
            }
            if (routepath && Object.is(routepath, _appFunction.routepath)) {
                Object.assign(appfunc, _appFunction);
                return true;
            }
        });
        return appfunc;
    };
    /**
     * 部件加载
     *
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.load = function () {
        var _this_1 = this;
        var params = { srfctrlid: this.getName(), srfaction: 'FETCH' };
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (success) {
            if (success.ret === 0) {
                _this_1.items = success.items;
                _this_1.dataProcess(_this_1.items);
                _this_1.fire(IBizAppMenu.LOADED, _this_1.items);
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 处理数据
     *
     * @private
     * @param {any[]} items
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.dataProcess = function (items) {
        var _this_1 = this;
        items.forEach(function (_item) {
            if (_item.expanded) {
                _this_1.defaultOpeneds.push(_item.id);
            }
            if (_item.items && _item.items.length > 0) {
                _this_1.dataProcess(_item.items);
            }
        });
    };
    /**
     * 菜单选中
     *
     * @param {*} [item={}]
     * @returns {*}
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.onSelectChange = function (item) {
        if (item === void 0) { item = {}; }
        // tslint:disable-next-line:prefer-const
        var _item = {};
        Object.assign(_item, item);
        var _appFunction = this.appFunctions.find(function (appfunction) { return Object.is(appfunction.appfuncid, item.appfuncid); });
        if (!_appFunction) {
            return;
        }
        Object.assign(_item, _appFunction);
        this.fire(IBizAppMenu.MENUSELECTION, [_item]);
    };
    /**
     * 设置选中菜单
     *
     * @param {*} [item={}]
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.setAppMenuSelected = function (item) {
        if (item === void 0) { item = {}; }
        if (item && Object.keys(item).length > 0) {
            Object.assign(this.selectItem, item);
        }
    };
    /**
     * 根据应用功能数据获取菜单数据项
     *
     * @param {Array<any>} items
     * @param {*} [appfunction={}]
     * @returns {*}
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.getSelectMenuItem = function (items, appfunction) {
        var _this_1 = this;
        if (appfunction === void 0) { appfunction = {}; }
        // tslint:disable-next-line:prefer-const
        var item = {};
        items.some(function (_item) {
            if (Object.is(_item.appfuncid, appfunction.appfuncid)) {
                Object.assign(item, _item);
                return true;
            }
            if (_item.items) {
                var subItem = _this_1.getSelectMenuItem(_item.items, appfunction);
                if (subItem && Object.keys(subItem).length > 0) {
                    Object.assign(item, subItem);
                    return true;
                }
            }
        });
        return item;
    };
    /**
    * 获取菜单数据
    *
    * @returns {Array<any>}
    * @memberof IBizAppMenu
    */
    IBizAppMenu.prototype.getItems = function () {
        return this.items;
    };
    /**
     * 根据菜单节点获取菜单数据项
     *
     * @param {Array<any>} items 菜单数据项
     * @param {*} [data={}]
     * @returns {*}
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.getItem = function (items, data) {
        if (data === void 0) { data = {}; }
        var _this = this;
        var _item = {};
        items.some(function (item) {
            if (Object.is(item.id, data.id)) {
                Object.assign(_item, item);
                return true;
            }
            if (item.items && item.items.length > 0 && Array.isArray(item.items)) {
                var _subItem = _this.getItem(item.items, data);
                if (_subItem && Object.keys(_subItem).length > 0) {
                    Object.assign(_item, _subItem);
                    return true;
                }
            }
        });
        return _item;
    };
    /**
     * 菜单收缩
     *
     * @memberof IBizAppMenu
     */
    IBizAppMenu.prototype.collapseChange = function () {
        this.isCollapsed = !this.isCollapsed;
        this.width = (this.isCollapsed ? 240 : 64);
    };
    /**
     * 菜单加载
     *
     * @static
     * @memberof IBizAppMenu
     */
    IBizAppMenu.LOADED = 'LOADED';
    /**
     * 菜单选中
     *
     * @static
     * @memberof IBizAppMenu
     */
    IBizAppMenu.MENUSELECTION = 'MENUSELECTION';
    return IBizAppMenu;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多项数据部件服务对象
 *
 * @export
 * @class IBizMDControl
 * @extends {IBizControl}
 */
var IBizMDControl = /** @class */ (function (_super) {
    __extends(IBizMDControl, _super);
    /**
     * Creates an instance of IBizMDControl.
     * 创建 IBizMDControl 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMDControl
     */
    function IBizMDControl(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 多数据列头
         *
         * @type {*}
         * @memberof IBizMDControl
         */
        _this.columns = [];
        /**
         * 默认排序
         *
         * @type {*}
         * @memberof IBizMDControl
         */
        _this.defaultSort = {};
        /**
         * 所有数据项
         *
         * @type {Array<any>}
         * @memberof IBizMDControl
         */
        _this.items = [];
        /**
         * 选中数据项
         *
         * @type {Array<any>}
         * @memberof IBizMDControl
         */
        _this.selection = [];
        _this.regColumns();
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.load = function (arg) {
        if (arg === void 0) { arg = {}; }
    };
    /**
     * 刷新数据
     * @param arg
     */
    IBizMDControl.prototype.refresh = function (arg) {
        if (arg === void 0) { arg = {}; }
    };
    /**
     * 设置选中项
     *
     * @param {Array<any>} selection
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.setSelection = function (selection) {
        this.selection = selection;
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
    };
    /**
     * 选中对象
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.clickItem = function (item) {
        if (item === void 0) { item = {}; }
        if (this.isLoading) {
            return;
        }
        this.setSelection([item]);
    };
    /**
     *
     *
     * @param {any} item
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.activeItem = function (item) {
    };
    /**
     * 获取列表中某条数据
     *
     * @param {string} name 字段
     * @param {string} value 名称
     * @returns {*}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.findItem = function (name, value) {
        var item;
        this.items.forEach(function (element) {
            if (Object.is(element[name], value)) {
                item = element;
                return;
            }
        });
        return item;
    };
    /**
     * 删除数据
     *
     * @param {*} [arg={}]
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.remove = function (arg) {
        if (arg === void 0) { arg = {}; }
    };
    /**
     * 单选
     *
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.onItemSelect = function (value, item) {
    };
    /**
     * 全选
     *
     * @param {boolean} value
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.selectAll = function (value) {
    };
    /**
     * 获取选中行
     *
     * @returns {Array<any>}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.getSelection = function () {
        return this.selection;
    };
    /**
     * 工作流提交
     *
     * @param {*} [params={}]
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.wfsubmit = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (!params) {
            params = {};
        }
        Object.assign(params, { srfaction: 'wfsubmit', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (data) {
            _this.endLoading();
            if (data.ret === 0) {
                _this.refresh();
            }
            else {
                _this.iBizNotification.error('', '执行工作流操作失败,' + data.info);
            }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', '执行工作流操作失败,' + error.info);
        });
    };
    /**
     * 实体界面行为
     *
     * @param {*} [params={}]
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.doUIAction = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        if (arg) {
            Object.assign(params, arg);
        }
        Object.assign(params, { srfaction: 'uiaction', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (data) {
            _this.endLoading();
            if (data.ret === 0) {
                if (data.reloadData) {
                    _this.refresh();
                }
                if (data.info && !Object.is(data.info, '')) {
                    _this.iBizNotification.success('', '操作成功');
                }
                // IBizUtil.processResult(data);
            }
            else {
                _this.iBizNotification.error('操作失败', '操作失败,执行操作发生错误,' + data.info);
            }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('操作失败', '操作失败,执行操作发生错误,' + error.info);
        });
    };
    /**
     * 批量添加
     *
     * @param {*} [arg={}]
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.addBatch = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        if (arg) {
            Object.assign(params, arg);
        }
        Object.assign(params, { srfaction: 'addbatch', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (data) {
            _this.endLoading();
            if (data.ret === 0) {
                _this.refresh();
                _this.fire(IBizMDControl.ADDBATCHED, data);
            }
            else {
                _this.iBizNotification.error('添加失败', '执行批量添加失败,' + data.info);
            }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('添加失败', '执行批量添加失败,' + error.info);
        });
    };
    /**
     * 获取所有数据项
     *
     * @returns {Array<any>}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.getItems = function () {
        return this.items;
    };
    /**
     * 注册多数据列头
     *
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.regColumns = function () {
    };
    /**
     * 获取多数据列头
     *
     * @returns {*}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.getColumns = function () {
        return this.columns;
    };
    /**
     * 设置多数据列头
     *
     * @param {*} [column={}]
     * @returns {void}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.regColumn = function (column) {
        if (column === void 0) { column = {}; }
        if (Object.keys(column).length === 0) {
            return;
        }
        if (!this.columns) {
            this.columns = [];
        }
        this.columns.push(column);
    };
    /**
     * 多数据项界面_数据导入栏
     *
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.doImportData = function (name) {
        if (Object.is(name, '')) {
            return;
        }
        // this.nzModalService.open({
        //     content: IBizImportdataViewComponent,
        //     wrapClassName: 'ibiz_wrap_modal',
        //     componentParams: { dename: name },
        //     footer: false,
        //     maskClosable: false,
        //     width: 500,
        // }).subscribe((result) => {
        //     if (result && result.ret) {
        //         this.refresh();
        //     }
        // });
    };
    /**
     * 界面行为
     *
     * @param {string} tag
     * @param {*} [data={}]
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.uiAction = function (tag, data) {
        if (data === void 0) { data = {}; }
    };
    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizMDControl
     */
    IBizMDControl.prototype.rendererDatas = function (items) {
        return items;
    };
    /*****************事件声明************************/
    /**
     * 添加数据
     *
     * @static
     * @memberof IBizMDControl
     */
    IBizMDControl.ADDBATCHED = 'ADDBATCHED';
    /**
     * 加载之前
     *
     * @static
     * @memberof IBizMDControl
     */
    IBizMDControl.BEFORELOAD = 'BEFORELOAD';
    /**
     * 加载完成
     *
     * @static
     * @memberof IBizMDControl
     */
    IBizMDControl.LOADED = 'LOADED';
    /**
     * 行数据选中
     *
     * @static
     * @memberof IBizMDControl
     */
    IBizMDControl.SELECTIONCHANGE = 'SELECTIONCHANGE';
    /**
     * 实体界面行为
     *
     * @static
     * @memberof IBizMDControl
     */
    IBizMDControl.UIACTION = 'UIACTION';
    return IBizMDControl;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据视图部件控制器
 *
 * @class IBizDataView
 * @extends {IBizMDControl}
 */
var IBizDataView = /** @class */ (function (_super) {
    __extends(IBizDataView, _super);
    /**
     * Creates an instance of IBizDataView.
     * 创建 IBizDataView 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDataView
     */
    function IBizDataView(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 双击选中项
         *
         * @private
         * @type {Array<any>}
         * @memberof IBizDataView
         */
        _this.dblselection = [];
        /**
         * 选中项
         *
         * @type {*}
         * @memberof IBizDataView
         */
        _this.selectItem = {};
        return _this;
    }
    /**
     * 数据加载
     *
     * @param {*} [arg={}]
     * @memberof IBizDataView
     */
    IBizDataView.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            _this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = response.items;
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            _this.endLoading();
            console.log(error.info);
        });
    };
    /**
     * 选择变化
     *
     * @param {*} [item={}]
     * @memberof IBizDataView
     */
    IBizDataView.prototype.selectChange = function (data) {
        if (data === void 0) { data = {}; }
        var arr = this.selection.filter(function (item) { return Object.is(data.srfkey, item.srfkey); });
        this.selection = [];
        this.selectItem = {};
        if (arr.length !== 1) {
            this.selection.push(data);
            Object.assign(this.selectItem, data);
        }
        this.fire(IBizDataView.SELECTIONCHANGE, this.selection);
    };
    /**
     * 双击选择变化
     *
     * @param {*} [data={}]
     * @memberof IBizDataView
     */
    IBizDataView.prototype.DBClickSelectChange = function (data) {
        if (data === void 0) { data = {}; }
        var arr = this.dblselection.filter(function (item) { return Object.is(data.srfkey, item.srfkey); });
        this.dblselection = [];
        this.selectItem = {};
        if (arr.length !== 1) {
            this.dblselection.push(data);
            Object.assign(this.selectItem, data);
        }
        this.fire(IBizDataView.DATAACTIVATED, this.dblselection);
    };
    /**
     * 数据项选中
     *
     * @static
     * @memberof IBizDataView
     */
    IBizDataView.SELECTIONCHANGE = 'SELECTIONCHANGE';
    /**
     * 数据项双击激活
     *
     * @static
     * @memberof IBizDataView
     */
    IBizDataView.DATAACTIVATED = 'DATAACTIVATED';
    return IBizDataView;
}(IBizMDControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格
 *
 * @class IBizDataGrid
 * @extends {IBizMDControl}
 */
var IBizDataGrid = /** @class */ (function (_super) {
    __extends(IBizDataGrid, _super);
    /**
     * Creates an instance of IBizGrid.
     * 创建 IBizGrid 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGrid
     */
    function IBizDataGrid(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 查询开始条数
         *
         * @memberof IBizGrid
         */
        _this.start = 0;
        /**
         * 每次加载条数
         *
         * @memberof IBizGrid
         */
        _this.limit = 20;
        /**
         * 总条数
         *
         * @memberof IBizGrid
         */
        _this.totalrow = 0;
        /**
         * 当前显示页码
         *
         * @memberof IBizGrid
         */
        _this.curPage = 1;
        /**
         * 是否全选
         *
         * @memberof IBizGrid
         */
        _this.allChecked = false;
        /**
         * 表格行选中动画
         *
         * @memberof IBizGrid
         */
        _this.indeterminate = false;
        /**
         * 表格全部排序字段
         *
         * @type {Array<any>}
         * @memberof IBizGrid
         */
        _this.gridSortField = [];
        /**
         * 行多项选中设置，用于阻塞多次触发选中效果
         *
         * @private
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.rowsSelection = false;
        /**
         * 是否支持多项
         *
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.multiSelect = true;
        /**
         * 是否启用行编辑
         *
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.isEnableRowEdit = false;
        /**
         * 打开行编辑
         *
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.openRowEdit = false;
        /**
         * 表格编辑项集合
         *
         * @type {*}
         * @memberof IBizGrid
         */
        _this.editItems = {};
        /**
         * 编辑行数据处理
         *
         * @type {*}
         * @memberof IBizGrid
         */
        _this.state = {};
        /**
         * 备份数据
         *
         * @type {Array<any>}
         * @memberof IBizGrid
         */
        _this.backupDatas = [];
        /**
         * 编辑行保存结果
         *
         * @type {Array<any>}
         * @memberof IBizDataGrid
         */
        _this.editorRowsSaveResult = [];
        /**
         * 最大导出行数
         *
         * @type {number}
         * @memberof IBizGrid
         */
        _this.maxExportRow = 1000;
        _this.regEditItems();
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        if (!opt.start) {
            Object.assign(opt, { start: (this.curPage - 1) * this.limit });
        }
        if (!opt.limit) {
            Object.assign(opt, { limit: this.limit });
        }
        Object.assign(opt, { sort: JSON.stringify(this.gridSortField) });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.allChecked = false;
        this.indeterminate = false;
        this.selection = [];
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            _this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = _this.rendererDatas(response.items);
            _this.totalrow = response.totalrow;
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', error.errorMessage);
        });
    };
    /**
     * 刷新数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.refresh = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.load(arg);
    };
    /**
     * 删除数据
     *
     * @param {*} [arg={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.remove = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        Object.assign(params, arg);
        Object.assign(params, { srfaction: 'remove', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (response) {
            _this.endLoading();
            if (response.ret !== 0) {
                _this.iBizNotification.error('', '删除数据失败,' + response.info);
                return;
            }
            if (_this.allChecked) {
                var rows = _this.curPage * _this.limit;
                if (_this.totalrow <= rows) {
                    _this.curPage = _this.curPage - 1;
                    if (_this.curPage === 0) {
                        _this.curPage = 1;
                    }
                }
            }
            _this.load({});
            _this.fire(IBizDataGrid.REMOVED, {});
            if (response.info && response.info !== '') {
                _this.iBizNotification.success('', '删除成功!');
            }
            _this.selection = [];
            // IBizUtil.processResult(response);
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', '删除数据失败');
        });
    };
    /**
     * 行数据复选框单选
     *
     * @param {boolean} value
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.onItemSelect = function (value, item) {
        if (item === void 0) { item = {}; }
        if (item.disabled) {
            return;
        }
        // if (this.isEnableRowEdit && this.openRowEdit) {
        //     return;
        // }
        var index = this.selection.findIndex(function (data) { return Object.is(data.srfkey, item.srfkey); });
        if (index === -1) {
            this.selection.push(item);
        }
        else {
            this.selection.splice(index, 1);
        }
        if (!this.multiSelect) {
            this.selection.forEach(function (data) {
                data.checked = false;
            });
            this.selection = [];
            if (index === -1) {
                this.selection.push(item);
            }
        }
        this.rowsSelection = true;
        this.allChecked = this.selection.length === this.items.length ? true : false;
        this.indeterminate = (!this.allChecked) && (this.selection.length > 0);
        item.checked = value;
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
    };
    /**
     * 行数据复选框全选
     *
     * @param {boolean} value
     * @memberof IBizMDControl
     */
    IBizDataGrid.prototype.selectAll = function (value) {
        var _this = this;
        if (this.isEnableRowEdit && this.openRowEdit) {
            return;
        }
        this.allChecked = value;
        if (!this.multiSelect) {
            setTimeout(function () {
                _this.allChecked = false;
            });
            return;
        }
        this.items.forEach(function (item) {
            if (!item.disabled) {
                item.checked = value;
            }
        });
        this.selection = [];
        if (value) {
            this.selection = this.items.slice();
        }
        this.indeterminate = (!value) && (this.selection.length > 0);
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
    };
    /**
     * 导出数据
     *
     * @param {any} params
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.exportData = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var params = {};
        this.fire(IBizMDControl.BEFORELOAD, params);
        if (params.search) {
            Object.assign(params, { query: params.search });
        }
        Object.assign(params, { srfaction: 'exportdata', srfctrlid: this.getName() });
        if (Object.is(arg.itemTag, 'all')) {
            Object.assign(params, { start: 0, limit: this.maxExportRow });
        }
        else if (Object.is(arg.itemTag, 'custom')) {
            var nStart = arg.exportPageStart;
            var nEnd = arg.exportPageEnd;
            if (nStart < 1 || nEnd < 1 || nStart > nEnd) {
                this.iBizNotification.warning('警告', '请输入有效的起始页');
                return;
            }
            Object.assign(params, { start: (nStart - 1) * this.limit, limit: (nEnd - nStart + 1) * this.limit });
        }
        else {
            Object.assign(params, { start: (this.curPage * this.limit) - this.limit, limit: this.curPage * this.limit });
        }
        if (this.gridSortField.length > 0) {
            Object.assign(params, { sort: this.gridSortField[0].property, sortdir: this.gridSortField[0].direction });
        }
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (res) {
            _this.endLoading();
            if (res.ret !== 0) {
                _this.iBizNotification.warning('警告', res.info);
                return;
            }
            // if (res.downloadurl) {
            //     let downloadurl = `/${IBizEnvironment.AppName.toLowerCase()}${res.downloadurl} `;
            //     downloadurl = (IBizEnvironment.LocalDeve ? '' : `/${IBizEnvironment.BaseUrl}`) + downloadurl;
            //     IBizUtil.download(downloadurl);
            // }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', error.errorMessage);
        });
    };
    /**
     * 导出模型
     *
     * @param {*} [arg={}]
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.exportModel = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        Object.assign(params, arg);
        Object.assign(params, { srfaction: 'exportmodel', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (res) {
            _this.endLoading();
            if (res.ret === 0) {
                if (res.downloadurl) {
                    var downloadurl = "/" + IBizEnvironment.AppName.toLowerCase() + res.downloadurl + " ";
                    downloadurl = (IBizEnvironment.LocalDeve ? '' : "/" + IBizEnvironment.BaseUrl) + downloadurl;
                    IBizUtil.download(downloadurl);
                }
            }
            else {
                _this.iBizNotification.warning('警告', res.info);
            }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', error.errorMessage);
        });
    };
    /**
     * 重置分页
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.resetStart = function () {
        this.start = 0;
    };
    /**
     * 分页页数改变
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.pageIndexChange = function () {
        this.refresh();
    };
    /**
     * 每页显示条数
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.pageSizeChange = function () {
        this.curPage = 1;
        this.refresh();
    };
    /**
     * 单击行选中
     *
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.clickRowSelect = function (data) {
        if (data === void 0) { data = {}; }
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizDataGrid.ROWCLICK, this.selection);
    };
    /**
     * 双击行选中
     *
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.dblClickRowSelection = function (data) {
        if (data === void 0) { data = {}; }
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizDataGrid.ROWDBLCLICK, this.selection);
    };
    /**
     * 表格排序
     *
     * @param {string} name 字段明显
     * @param {string} type 排序类型
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.sort = function (name, type) {
        if (!name || !type) {
            return;
        }
        var _name = name;
        this.columns.some(function (solumn) {
            if (solumn.dataItemName && Object.is(solumn.dataItemName, name)) {
                _name = solumn.name;
                return true;
            }
            return false;
        });
        var _item = this.gridSortField[0];
        if (_item && Object.is(_item.property, _name) && Object.is(_item.direction, type)) {
            return;
        }
        var sort = { property: _name };
        Object.assign(sort, { direction: type });
        this.gridSortField = [];
        this.gridSortField.push(sort);
        this.refresh({});
    };
    /**
     * 设置表格数据当前页
     *
     * @param {number} page 分页数量
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setCurPage = function (page) {
        this.curPage = page;
    };
    /**
     * 设置是否支持多选
     *
     * @param {boolean} state 是否支持多选
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setMultiSelect = function (state) {
        this.multiSelect = state;
    };
    /**
     * 界面行为
     *
     * @param {string} tag
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.uiAction = function (tag, data) {
        if (data === void 0) { data = {}; }
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizMDControl.UIACTION, { tag: tag, data: data });
    };
    /**
     * 处理非复选框行数据选中,并处理是否激活数据
     *
     * @private
     * @param {*} [data={}]
     * @returns {boolean} 是否激活
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.doRowDataSelect = function (data) {
        // if (this.isEnableRowEdit && this.openRowEdit) {
        //     return;
        // }
        if (data === void 0) { data = {}; }
        this.selection.forEach(function (item) {
            item.checked = false;
        });
        this.selection = [];
        data.checked = true;
        this.selection.push(data);
        this.indeterminate = (!this.allChecked) && (this.selection.length > 0);
        // if (this.rowsSelection) {
        //     this.rowsSelection = false;
        //     return true;
        // }
        return false;
    };
    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.rendererDatas = function (items) {
        var _this = this;
        _super.prototype.rendererDatas.call(this, items);
        items.forEach(function (item) {
            var names = Object.keys(item);
            names.forEach(function (name) { item[name] = item[name] ? item[name] : ''; });
            item.checked = false;
        });
        if (this.isEnableRowEdit) {
            items.forEach(function (item) { item.openeditrow = (_this.openRowEdit) ? false : true; });
            if (this.openRowEdit) {
                this.selection = [];
                this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
                this.backupDatas = JSON.parse(JSON.stringify(items)).slice();
                items.forEach(function (item, index) {
                    _this.setEditItemState(item.srfkey);
                    _this.editRow(item, index, false);
                });
            }
            else {
                this.backupDatas = [];
                this.state = {};
            }
        }
        return items;
    };
    /**
     * 注册表格所有编辑项
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.regEditItems = function () {
    };
    /**
     * 注册表格编辑项
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.regEditItem = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.keys(item).length === 0) {
            return;
        }
        this.editItems[item.name] = item;
    };
    /**
     * 获取编辑项
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.getEditItem = function (name) {
        if (name) {
            return this.editItems[name];
        }
    };
    /**
     * 设置编辑项状态
     *
     * @param {string} srfkey
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setEditItemState = function (srfkey) {
        var _this = this;
        if (!this.state) {
            return;
        }
        if (!srfkey) {
            this.iBizNotification.warning('警告', '数据异常');
        }
        // tslint:disable-next-line:prefer-const
        var editItems = {};
        var itemsName = Object.keys(this.editItems);
        itemsName.forEach(function (name) {
            // tslint:disable-next-line:prefer-const
            var item = {};
            var _editor = JSON.stringify(_this.editItems[name]);
            Object.assign(item, JSON.parse(_editor));
            editItems[name] = item;
        });
        this.state[srfkey] = editItems;
    };
    /**
     * 删除信息编辑项状态
     *
     * @param {string} srfkey
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.deleteEditItemState = function (srfkey) {
        if (srfkey && this.state.hasOwnProperty(srfkey)) {
            delete this.state.srfkey;
        }
    };
    /**
     * 设置编辑项是否启用
     *
     * @param {string} srfkey
     * @param {number} type
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setEditItemDisabled = function (srfkey, type) {
        if (this.state && this.state.hasOwnProperty(srfkey)) {
            // tslint:disable-next-line:prefer-const
            var item_1 = this.state[srfkey];
            var itemsName = Object.keys(item_1);
            itemsName.forEach(function (name) {
                var isReadOk = true;
                var state = true;
                if (isReadOk) {
                    if (type === 1) {
                        if ((item_1[name].enabledcond & 2) === 2) {
                            state = false;
                        }
                    }
                    else {
                        if ((item_1[name].enabledcond & 1) === 1) {
                            state = false;
                        }
                    }
                }
                item_1[name].disabled = state;
            });
            Object.assign(this.state[srfkey], item_1);
        }
    };
    /**
     * 获取行编辑状态
     *
     * @returns {boolean}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.getOpenEdit = function () {
        return this.openRowEdit;
    };
    /**
     * 保存表格所有编辑行 <在插件模板中提供重写>
     *
     * @param {*} [params={}]
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.saveAllEditRow = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        var saveCount = 0;
        this.items.forEach(function (_item, index) {
            var data = _this.backupDatas.find(function (_data) { return Object.is(_data.srfkey, _item.srfkey); });
            if (!data) {
                return;
            }
            Object.keys(data).some(function (name) {
                var fieldIsSave = !Object.is(name, 'openeditrow') && (typeof _item[name] === 'string' && typeof data[name] === 'string') && !Object.is(_item[name], data[name]);
                if (!fieldIsSave) {
                    return false;
                }
                var templData = {};
                Object.assign(templData, params, _item);
                var subject = _this.editRowSave(templData, index);
                subject.subscribe(function (result) {
                    _this.editorRowsSaveResult.push(result);
                    if (_this.editorRowsSaveResult.length === saveCount) {
                        _this.showSaveResultInfo();
                        _this.editorRowsSaveResult = [];
                        _this.fire(IBizMDControl.LOADED, _this.items);
                    }
                });
                saveCount++;
                return true;
            });
        });
    };
    /**
     * 显示保存结果信息
     *
     * @private
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.showSaveResultInfo = function () {
        var successInfo = '';
        var errorInfo = '';
        var info = '';
        this.editorRowsSaveResult.sort(function (a, b) {
            return a.index - b.index;
        });
        this.editorRowsSaveResult.forEach(function (data) {
            if (Object.is(data.state, 'success')) {
                if (!Object.is(successInfo, '')) {
                    successInfo = successInfo + "\u3001";
                }
                successInfo = "" + successInfo + (data.index + 1);
            }
            if (Object.is(data.state, 'error')) {
                var _info_1 = "\u7B2C " + (data.index + 1) + " \u6761\u4FDD\u5B58\u5931\u8D25\uFF0C\u9519\u8BEF\u4FE1\u606F\u5982\u4E0B\uFF1A";
                if (data.result && Array.isArray(data.result) && data.result.length > 0) {
                    var items = data.result;
                    items.forEach(function (item, index) {
                        if (index > 0) {
                            _info_1 = _info_1 + "\u3001";
                        }
                        _info_1 = "" + _info_1 + item.info;
                    });
                }
                else if (!Object.is(data.errorMessage, '')) {
                    // _info = !Object.is(_info, '') ? `${_info}、` : _info;
                    _info_1 = "" + _info_1 + data.errorMessage;
                }
                errorInfo = errorInfo + "<div>" + _info_1 + "</div>";
            }
        });
        if (!Object.is(successInfo, '')) {
            info = info + "<div>\u7B2C " + successInfo + " \u6761\u4FDD\u5B58\u6210\u529F\uFF01</div>";
        }
        if (!Object.is(errorInfo, '')) {
            info = info + "<div style='color: red;'>" + errorInfo + "</div>";
        }
        this.iBizNotification.info('', info);
    };
    /**
     * 启用行编辑
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.openEdit = function (params) {
        var _this = this;
        if (!this.isEnableRowEdit) {
            this.iBizNotification.info('提示', '未启用行编辑');
            return;
        }
        if (!this.openRowEdit) {
            this.openRowEdit = true;
            this.items.forEach(function (item) { item.openeditrow = true; });
            this.selection.forEach(function (data) {
                data.checked = false;
            });
            this.selection = [];
            this.indeterminate = false;
            this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
            this.backupDatas = JSON.parse(JSON.stringify(this.items)).slice();
            this.items.forEach(function (item, index) {
                _this.setEditItemState(item.srfkey);
                _this.editRow(item, index, false);
            });
        }
    };
    /**
     * 关闭行编辑
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.closeEdit = function (params) {
        if (!this.isEnableRowEdit) {
            this.iBizNotification.info('提示', '未启用行编辑');
            return;
        }
        if (this.openRowEdit) {
            this.openRowEdit = false;
            this.backupDatas = [];
            this.items = [];
            this.state = {};
            this.refresh({});
        }
    };
    /**
     * 编辑行数据
     *
     * @param {*} [data={}]
     * @param {number} rowindex
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.editRow = function (data, rowindex, state) {
        if (data === void 0) { data = {}; }
        data.openeditrow = state;
        this.setEditItemState(data.srfkey);
        if (data.openeditrow) {
            var index = this.backupDatas.findIndex(function (item) { return Object.is(item.srfkey, data.srfkey); });
            if (index !== -1) {
                Object.assign(data, this.backupDatas[index]);
            }
            if (Object.is(data.srfkey, '')) {
                this.items.splice(rowindex, 1);
            }
        }
        else {
            this.setEditItemDisabled(data.srfkey, 1);
        }
    };
    /**
     * 保存编辑行数据
     *
     * @param {*} [data={}]
     * @param {number} rowindex
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.editRowSave = function (data, rowindex) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var subject = new rxjs.Subject();
        var srfaction = data.rowdatamodal && Object.is(data.rowdatamodal, 'gridloaddraft') ? 'create' : 'update';
        var params = { srfaction: srfaction, srfctrlid: 'grid' };
        var _names = Object.keys(data);
        _names.forEach(function (name) {
            data[name] = data[name] ? data[name] : '';
        });
        Object.assign(params, data);
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (responce) {
            if (responce.ret === 0) {
                var index = _this.backupDatas.findIndex(function (item) { return Object.is(data.srfkey, item.srfkey); });
                if (index !== -1) {
                    Object.assign(_this.backupDatas[index], responce.data);
                    if (_this.backupDatas[index].hasOwnProperty('srfisnewdata')) {
                        delete _this.backupDatas[index].srfisnewdata;
                    }
                    if (_this.backupDatas[index].hasOwnProperty('rowdatamodal')) {
                        delete _this.backupDatas[index].rowdatamodal;
                    }
                    _this.deleteEditItemState(data.srfkey);
                    _this.setEditItemState(responce.data.srfkey);
                    _this.setEditItemDisabled(responce.data.srfkey, 1);
                }
                if (_this.items[rowindex].hasOwnProperty('srfisnewdata')) {
                    delete _this.items[rowindex].srfisnewdata;
                }
                if (_this.items[rowindex].hasOwnProperty('rowdatamodal')) {
                    delete _this.items[rowindex].rowdatamodal;
                }
                Object.assign(_this.items[rowindex], responce.data);
                subject.next({ state: 'success', index: rowindex, result: [] });
            }
            else {
                var state = responce;
                _this.doSaveErrorResult({ subject: subject, state: state, data: data, rowindex: rowindex });
            }
        }, function (error) {
            var state = error;
            _this.doSaveErrorResult({ subject: subject, state: state, data: data, rowindex: rowindex });
        });
        return subject;
    };
    /**
     * 保存错误处理
     *
     * @private
     * @param {{ subject: Subject<any>, state: any, data: any, rowindex: number }} { subject, state, data, rowindex }
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.doSaveErrorResult = function (_a) {
        var _this = this;
        var subject = _a.subject, state = _a.state, data = _a.data, rowindex = _a.rowindex;
        var _result = [];
        if (state.error && (state.error.items && Array.isArray(state.error.items) && state.error.items.length > 0)) {
            var items = state.error.items;
            items.forEach(function (item, index) {
                Object.assign(_this.state[data.srfkey][item.id].styleCss, { 'border': '1px solid #f04134', 'border-radius': '4px' });
            });
            _result = items.slice();
        }
        var message = state.errorMessage ? state.errorMessage : '保存异常!';
        subject.next({ state: 'error', index: rowindex, result: _result, errorMessage: message });
    };
    /**
     * 行编辑文本框光标移出事件
     *
     * @param {*} event
     * @param {string} name
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.onBlur = function (event, name, data) {
        if (data === void 0) { data = {}; }
        if ((!event) || Object.keys(data).length === 0) {
            return;
        }
        if (Object.is(event.target.value, data[name])) {
            return;
        }
        this.colValueChange(name, event.target.value, data);
    };
    /**
     * 行编辑文本框键盘事件
     *
     * @param {*} event
     * @param {string} name
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.onKeydown = function (event, name, data) {
        if (data === void 0) { data = {}; }
        if ((!event) || Object.keys(data).length === 0) {
            return;
        }
        if (event.keyCode !== 13) {
            return;
        }
        if (Object.is(event.target.value, data[name])) {
            return;
        }
        this.colValueChange(name, event.target.value, data);
    };
    /**
     * 行编辑单元格值变化
     *
     * @param {string} name
     * @param {*} data
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.colValueChange = function (name, value, data) {
        var srfkey = data.srfkey;
        var _data = this.items.find(function (back) { return Object.is(back.srfkey, srfkey); });
        if (this.state[srfkey] && this.state[srfkey][name]) {
            if (_data && !Object.is(_data[name], value)) {
                Object.assign(this.state[srfkey][name].styleCss, { 'border': '1px solid #49a9ee', 'border-radius': '4px' });
            }
            else {
                Object.assign(this.state[srfkey][name].styleCss, { 'border': '0px', 'border-radius': '0px' });
            }
            data[name] = value;
            this.fire(IBizDataGrid.UPDATEGRIDITEMCHANGE, { name: name, data: data });
        }
    };
    /**
     * 更新表格编辑列值
     *
     * @param {string} srfufimode
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.updateGridEditItems = function (srfufimode, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = { srfaction: 'updategridedititem', srfufimode: srfufimode, srfctrlid: 'grid' };
        var _names = Object.keys(data);
        _names.forEach(function (name) {
            data[name] = data[name] ? data[name] : '';
        });
        Object.assign(opt, { srfactivedata: JSON.stringify(data) });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (success) {
            if (success.ret === 0) {
                var index = _this.items.findIndex(function (item) { return Object.is(item.srfkey, data.srfkey); });
                if (index !== -1) {
                    Object.assign(_this.items[index], success.data);
                }
            }
            else {
                _this.iBizNotification.error('错误', success.info);
            }
        }, function (error) {
            _this.iBizNotification.error('错误', error.info);
        });
    };
    /**
     * 新建编辑行
     *
     * @param {*} [param={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.newRowAjax = function (param) {
        var _this = this;
        if (param === void 0) { param = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        Object.assign(opt, param);
        this.fire(IBizMDControl.BEFORELOAD, opt);
        Object.assign(opt, { srfaction: 'loaddraft', srfctrlid: 'grid' });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (success) {
            if (success.ret === 0) {
                var srfkey = (Object.is(success.data.srfkey, '')) ? IBizUtil.createUUID() : success.data.srfkey;
                success.data.srfkey = srfkey;
                _this.setEditItemState(srfkey);
                _this.setEditItemDisabled(srfkey, 0);
                _this.items.splice(0, 0, Object.assign(success.data, { openeditrow: false, rowdatamodal: 'gridloaddraft', srfisnewdata: true }));
                var backdata = JSON.parse(JSON.stringify(success.data));
                _this.backupDatas.push(backdata);
            }
            else {
                _this.iBizNotification.error('错误', "\u83B7\u53D6\u9ED8\u8BA4\u6570\u636E\u5931\u8D25, " + success.info);
            }
        }, function (error) {
            _this.iBizNotification.error('错误', "\u83B7\u53D6\u9ED8\u8BA4\u6570\u636E\u5931\u8D25, " + error.info);
        });
    };
    // BEGIN：Element UI 表格部件内置方法
    /**
     * 复选框行选中 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSelect = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var selections = argu[0], row = argu[1];
        var index = selections.findIndex(function (data) { return Object.is(data.srfkey, row.srfkey); });
        this.onItemSelect(index !== -1 ? false : true, row);
    };
    /**
     * 复选框全选 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSelectAll = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var selections = argu[0];
        if (Array.isArray(selections)) {
            this.selectAll(selections.length !== 0 ? true : false);
        }
    };
    /**
     * 行单击 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementRowClick = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var row = argu[0];
        if (!this.multiSelect) {
            var selections = this.$vue.$refs.multipleTable.selection;
            var index = selections.findIndex(function (data) { return Object.is(data.srfkey, row.srfkey); });
            row = index !== -1 ? {} : row;
        }
        this.$vue.$refs.multipleTable.clearSelection();
        this.$vue.$refs.multipleTable.toggleRowSelection(row);
        this.clickRowSelect(row);
    };
    /**
     * 行双击 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementRowDblclick = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var row = argu[0];
        this.$vue.$refs.multipleTable.clearSelection();
        this.$vue.$refs.multipleTable.toggleRowSelection(row);
        if (this.openRowEdit) {
            return;
        }
        this.dblClickRowSelection(row);
    };
    /**
     * 改变页码
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementOnChange = function (argu) {
        this.curPage = argu[0];
        this.pageIndexChange();
    };
    /**
     * 分页数量改变
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementOnPageSizeChange = function (argu) {
        this.limit = argu[0];
        this.pageSizeChange();
    };
    /**
     * 排序
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSortChange = function (argu) {
        var event = argu[0];
        var type = event.order.indexOf('descending') >= 0 ? 'desc' : 'asc';
        this.sort(event.prop, type);
    };
    /**
     * 是否支持选中
     *
     * @param {*} argu
     * @returns {boolean}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSelectable = function () {
        return function (row, index) {
            // return this.openRowEdit ? false : true;
            return true;
        };
    };
    /**
     * 绘制表格行选中样式
     *
     * @returns
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.renderElementTabelRowSelectClass = function () {
        var _this = this;
        return function (_a) {
            var row = _a.row, rowIndex = _a.rowIndex;
            var rowClass = '';
            if (row) {
                var index = _this.selection.findIndex(function (_select) { return Object.is(_select.srfkey, row.srfkey); });
                if (index !== -1) {
                    rowClass = 'ibiz-data-grid-row-select';
                }
            }
            return rowClass;
        };
    };
    // END：Element UI 表格部件内置方法
    /*****************事件声明************************/
    /**
     * 改变启用行编辑按钮信息
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.CHANGEEDITSTATE = 'CHANGEEDITSTATE';
    /**
     * 表格行数据变化
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.UPDATEGRIDITEMCHANGE = 'UPDATEGRIDITEMCHANGE';
    /**
     * 数据删除完成
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.REMOVED = 'REMOVED';
    /**
     * 行单击选中
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.ROWCLICK = 'ROWCLICK';
    /**
     * 行数据双击选中
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.ROWDBLCLICK = 'ROWDBLCLICK';
    return IBizDataGrid;
}(IBizMDControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 列表部件
 *
 * @class IBizList
 * @extends {IBizMDControl}
 */
var IBizList = /** @class */ (function (_super) {
    __extends(IBizList, _super);
    /**
     * Creates an instance of IBizList.
     * 创建 IBizList 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizList
     */
    function IBizList(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     *
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizList
     */
    IBizList.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = response.items;
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            console.log(error.info);
        });
    };
    /**
     * 删除单条数据
     *
     * @param {*} item
     * @memberof IBizList
     */
    IBizList.prototype.doRemove = function (item) {
        if (item) {
        }
    };
    /**
     * 删除所选数据
     *
     * @memberof IBizList
     */
    IBizList.prototype.doRemoveAll = function () {
    };
    return IBizList;
}(IBizMDControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工具栏
 *
 * @class IBizToolbar
 * @extends {IBizControl}
 */
var IBizToolbar = /** @class */ (function (_super) {
    __extends(IBizToolbar, _super);
    /**
     * Creates an instance of IBizToolbar.
     * 创建IBizToolbar的一个实例。
     *
     * @param {*} [opts={}]
     * @memberof IBizToolbar
     */
    function IBizToolbar(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 数据导出下拉菜单状态
         *
         * @type {boolean}
         * @memberof IBizToolbar
         */
        _this.exportMenuState = false;
        /**
         * 工具栏按钮
         *
         * @type {Array<any>}
         * @memberof IBizToolbar
         */
        _this.items = [];
        _this.regToolBarItems();
        return _this;
    }
    /**
     * 注册所有工具栏按钮
     *
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.regToolBarItems = function () {
    };
    /**
     * 注册工具栏按钮
     *
     * @param {*} [item={}]
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.regToolBarItem = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.keys(item).length === 0) {
            return;
        }
        this.items.push(item);
    };
    /**
     * 获取工具栏按钮
     *
     * @returns {Array<any>}
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.getItems = function () {
        if (!this.items) {
            this.items = [];
        }
        return this.items;
    };
    /**
     * 获取工具栏项
     *
     * @param {Array<any>} items
     * @param {string} name
     * @returns {*}
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.getItem = function (items, name) {
        var _this = this;
        var data = {};
        items.some(function (_item) {
            if (Object.is(_item.name, name)) {
                data = _item;
                return true;
            }
            if (_item.items && _item.items.length > 0) {
                var subItem = _this.getItem(_item.items, name);
                if (Object.keys(subItem).length > 0) {
                    data = subItem;
                    return true;
                }
            }
        });
        return data;
    };
    /**
     * 设置工具栏按钮项是否启用
     *
     * @param {Array<any>} items
     * @param {boolean} disabled
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.setItemDisabled = function (items, disabled) {
        var _this = this;
        items.forEach(function (_item) {
            if (_item.uiaction && (Object.is(_item.uiaction.target, 'SINGLEKEY') || Object.is(_item.uiaction.target, 'MULTIKEY'))) {
                _item.disabled = disabled;
            }
            if (_item.uiaction && Object.is(_item.uiaction.tag, 'NewRow')) {
                _item.disabled = false;
            }
            if (_item.items && _item.items.length > 0) {
                _this.setItemDisabled(_item.items, disabled);
            }
        });
    };
    /**
     * 更新权限
     *
     * @param {Array<any>} items
     * @param {*} [action={}]
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.updateAccAction = function (items, action) {
        var _this = this;
        if (action === void 0) { action = {}; }
        items.forEach(function (_item) {
            var dataaccaction = _item.dataaccaction;
            var state = (dataaccaction && !Object.is(dataaccaction, '')) && (action && Object.keys(action).length > 0 && action[dataaccaction] !== 1);
            _item._dataaccaction = state ? false : true;
            if (_item.items && _item.items.length > 0) {
                _this.updateAccAction(_item.items, action);
            }
        });
    };
    /**
     * 工具栏导出功能设置
     *
     * @param {string} type
     * @param {string} [itemTag]
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.itemExportExcel = function (type, itemTag) {
        // tslint:disable-next-line:prefer-const
        var params = { tag: type };
        if (itemTag && Object.is(itemTag, 'all')) {
            Object.assign(params, { itemTag: 'all' });
        }
        else if (itemTag && Object.is(itemTag, 'custom')) {
            if (!this.exportStartPage || !this.exportEndPage) {
                this.iBizNotification.warning('警告', '请输入起始页');
                return;
            }
            var startPage = Number.parseInt(this.exportStartPage, 10);
            var endPage = Number.parseInt(this.exportEndPage, 10);
            if (Number.isNaN(startPage) || Number.isNaN(endPage)) {
                this.iBizNotification.warning('警告', '请输入有效的起始页');
                return;
            }
            if (startPage < 1 || endPage < 1 || startPage > endPage) {
                this.iBizNotification.warning('警告', '请输入有效的起始页');
                return;
            }
            Object.assign(params, { exportPageStart: startPage, exportPageEnd: endPage, itemTag: 'custom' });
        }
        this.exportMenuState = false;
        this.fire(IBizToolbar.ITEMCLICK, params);
    };
    /**
     * 点击按钮
     *
     * @param {string} type  界面行为类型
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.itemclick = function (_item) {
        if (_item === void 0) { _item = {}; }
        var item = this.getItem(this.items, _item.name);
        if (Object.is(item.tag, 'ToggleRowEdit')) {
            item.rowedit = !item.rowedit;
            item.caption = item.rowedit ? '开启行编辑' : '关闭行编辑';
        }
        if (Object.is(item.tag, 'ToggleFilter')) {
            item.opensearchform = !item.opensearchform;
            item.caption = item.opensearchform ? '收起' : '过滤';
        }
        if (!_item.uiaction) {
            return;
        }
        this.fire(IBizToolbar.ITEMCLICK, { tag: _item.uiaction.tag });
    };
    /**
     * 点击按钮事件
     *
     * @static
     * @memberof IBizToolbar
     */
    IBizToolbar.ITEMCLICK = 'ITEMCLICK';
    return IBizToolbar;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单
 *
 * @export
 * @class IBizForm
 * @extends {IBizControl}
 */
var IBizForm = /** @class */ (function (_super) {
    __extends(IBizForm, _super);
    /**
     * Creates an instance of IBizForm.
     * 创建IBizForm的一个实例。
     *
     * @param {*} [opts={}]
     * @memberof IBizForm
     */
    function IBizForm(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否忽略表单变化
         *
         * @type {boolean}
         * @memberof IBizForm
         */
        _this.ignoreformfieldchange = false;
        /**
         * 是否忽略表单项更新
         *
         * @type {boolean}
         * @memberof IBizForm
         */
        _this.ignoreUFI = false;
        /**
         * 当前表单权限
         *
         * @type {*}
         * @memberof IBizForm
         */
        _this.dataaccaction = {};
        /**
         * 表单是否改变
         *
         * @type {boolean}
         * @memberof IBizForm
         */
        _this.formDirty = false;
        /**
         * 表单表单项
         *
         * @type {*}
         * @memberof IBizForm
         */
        _this.fields = {};
        _this.regFields();
        return _this;
    }
    /**
     * 注册表单项
     *
     * @memberof IBizForm
     */
    IBizForm.prototype.regFields = function () {
    };
    /**
     * 表单加载
     *
     * @param {*} [arg={}] 参数
     * @returns {void}
     * @memberof IBizForm
     */
    IBizForm.prototype.autoLoad = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (!arg) {
            arg = {};
        }
        if (arg.srfkey && !Object.is(arg.srfkey, '')) {
            this.load2(arg);
            return;
        }
        if (arg.srfkeys && !Object.is(arg.srfkeys, '')) {
            Object.assign(arg, { srfkey: arg.srfkeys });
            this.load2(arg);
            return;
        }
        this.loadDraft(arg);
    };
    /**
     * 加载
     *
     * @param {*} [opt={}] 参数
     * @memberof IBizForm
     */
    IBizForm.prototype.load2 = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        Object.assign(arg, opt);
        Object.assign(arg, { srfaction: 'load', srfctrlid: this.getName() });
        this.fire(IBizForm.BEFORELOAD, arg);
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;
        this.load(arg).subscribe(function (action) {
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            _this.setDataAccAction(action.dataaccaction);
            _this.fillForm(action.data);
            _this.formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED, this);
            _this.fire(IBizForm.FORMLOADED, _this);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            _this.fire(IBizForm.FORMFIELDCHANGED, null);
            _this.onLoaded();
        }, function (action) {
            action.failureType = 'SERVER_INVALID';
            _this.iBizNotification.error('加载失败', '加载数据发生错误, ' + _this.getActionErrorInfo(action));
            // IBiz.alert(IGM('IBIZFORM.LOAD.TITLE', '加载失败'), IGM('IBIZFORM.LOAD2.INFO', '加载数据发生错误,' + this.getActionErrorInfo(action), [this.getActionErrorInfo(action)]), 2);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
        });
    };
    /**
     * 加载草稿
     *
     * @param {*} [opt={}]
     * @memberof IBizForm
     */
    IBizForm.prototype.loadDraft = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        Object.assign(arg, opt);
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;
        if (!arg.srfsourcekey || Object.is(arg.srfsourcekey, '')) {
            // .extend(arg, { srfaction: 'loaddraft' });
            Object.assign(arg, { srfaction: 'loaddraft', srfctrlid: this.getName() });
        }
        else {
            // .extend(arg, { srfaction: 'loaddraftfrom' });
            Object.assign(arg, { srfaction: 'loaddraftfrom', srfctrlid: this.getName() });
        }
        this.fire(IBizForm.BEFORELOAD, arg);
        this.load(arg).subscribe(function (action) {
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            _this.setDataAccAction(action.dataaccaction);
            _this.fillForm(action.data);
            _this.formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED, this);
            _this.fire(IBizForm.FORMLOADED, _this);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            _this.fire(IBizForm.FORMFIELDCHANGED, null);
            _this.onDraftLoaded();
        }, function (action) {
            action.failureType = 'SERVER_INVALID';
            // IBiz.alert(IGM('IBIZFORM.LOAD.TITLE', '加载失败'), IGM('IBIZFORM.LOADDRAFT.INFO', '加载草稿发生错误,' + this.getActionErrorInfo(action), [this.getActionErrorInfo(action)]), 2);
            _this.iBizNotification.error('加载失败', '加载草稿发生错误, ' + _this.getActionErrorInfo(action));
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
        });
    };
    /**
     *
     *
     * @memberof IBizForm
     */
    IBizForm.prototype.onDraftLoaded = function () {
    };
    /**
     *
     *
     * @memberof IBizForm
     */
    IBizForm.prototype.onLoaded = function () {
    };
    /**
     * 设置表单动态配置
     *
     * @param {*} [config={}]
     * @memberof IBizForm
     */
    IBizForm.prototype.setFieldAsyncConfig = function (config) {
        var _this = this;
        if (config === void 0) { config = {}; }
        if (!config) {
            return;
        }
        var _names = Object.keys(config);
        _names.forEach(function (name) {
            var field = _this.findField(name);
            if (!field) {
                return;
            }
            if (config[name].hasOwnProperty('items') && Array.isArray(config[name].items)) {
                field.setAsyncConfig(config[name].items);
            }
            if (config[name].hasOwnProperty('dictitems') && Array.isArray(config[name].dictitems)) {
                field.setDictItems(config[name].dictitems);
            }
        });
    };
    /**
     * 设置当前表单权限信息
     *
     * @param {*} [dataaccaction={}] 权限数据
     * @memberof IBizForm
     */
    IBizForm.prototype.setDataAccAction = function (dataaccaction) {
        if (dataaccaction === void 0) { dataaccaction = {}; }
        this.dataaccaction = dataaccaction;
        this.fire(IBizForm.DATAACCACTIONCHANGE, this.dataaccaction);
    };
    /**
     * 获取当前表单权限信息
     *
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.getdataaccaction = function () {
        return this.dataaccaction;
    };
    /**
     * 设置属性状态
     *
     * @param {*} [state={}]
     * @memberof IBizForm
     */
    IBizForm.prototype.setFieldState = function (state) {
        var _this = this;
        if (state === void 0) { state = {}; }
        if (!state) {
            return;
        }
        var stateDats = Object.keys(state);
        stateDats.forEach(function (name) {
            var field = _this.findField(name);
            if (field) {
                // tslint:disable-next-line:no-bitwise
                var disabled = ((state[name] & 1) === 0);
                if (field.isDisabled() !== disabled) {
                    field.setDisabled(disabled);
                }
            }
        });
    };
    /**
     * 设置表单项权限
     *
     * @param {*} [data={}]
     * @memberof IBizForm
     */
    IBizForm.prototype.setFieldJurisdiction = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var fillData = Object.keys(data);
        fillData.forEach(function (name) {
            var item = _this.findField(name);
            if (item) {
                // 设置当前表单项权限
                var jurisdictionValue = data['srfip_' + name];
                if (jurisdictionValue !== undefined) {
                    item.setJurisdiction(jurisdictionValue);
                }
            }
        });
    };
    /**
     * 表单是否改变
     *
     * @returns {boolean}
     * @memberof IBizForm
     */
    IBizForm.prototype.isDirty = function () {
        return this.formDirty;
    };
    /**
     * 注册表单属性
     *
     * @param {*} field 表单项
     * @memberof IBizForm
     */
    IBizForm.prototype.regField = function (field) {
        var _this = this;
        if (!this.fields) {
            this.fields = {};
        }
        if (field) {
            field.on(IBizFormItem.VALUECHANGED).subscribe(function (data) {
                if (data === void 0) { data = {}; }
                if (_this.ignoreformfieldchange) {
                    return;
                }
                _this.formDirty = true;
                _this.fire(IBizForm.FORMFIELDCHANGED, data);
            });
            this.fields[field.getName()] = field;
        }
    };
    /**
     * 注销表单属性
     *
     * @param {*} field 属性
     * @memberof IBizForm
     */
    IBizForm.prototype.unRegFiled = function (field) {
        delete this.fields[field.getName()];
    };
    /**
     * 获取控件标识
     *
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.getSRFCtrlId = function () {
        // return this.srfctrlid;
    };
    /**
     * 根据名称获取属性
     *
     * @param {string} name 属性名称
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.findField = function (name) {
        if (this.fields[name]) {
            return this.fields[name];
        }
        return undefined;
    };
    /**
     * 根据唯一标识获取属性
     *
     * @param {string} id 表单项id
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.getFieldById = function (id) {
        // return this.fieldIdMap[id];
    };
    /**
     * 加载数据
     *
     * @param {*} [opt={}] 参数
     * @returns {Observable<any>}  事件回调
     * @memberof IBizForm
     */
    IBizForm.prototype.load = function (opt) {
        if (opt === void 0) { opt = {}; }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        Object.assign(arg, opt);
        var subject = new rxjs.Subject();
        this.iBizHttp.post(this.getBackendUrl(), arg).subscribe(function (data) {
            if (data.ret === 0) {
                subject.next(data);
            }
            else {
                subject.error(data);
            }
        }, function (data) {
            subject.error(data);
        });
        return subject;
    };
    /**
     * 数据提交
     *
     * @param {*} [opt={}] 参数
     * @returns {Observable<any>} 事件回调
     * @memberof IBizForm
     */
    IBizForm.prototype.submit = function (opt) {
        if (opt === void 0) { opt = {}; }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        Object.assign(arg, opt);
        var subject = new rxjs.Subject();
        this.iBizHttp.post(this.getBackendUrl(), arg).subscribe(function (data) {
            if (data.ret === 0) {
                subject.next(data);
            }
            else {
                subject.error(data);
            }
        }, function (data) {
            subject.error(data);
        });
        return subject;
    };
    /**
     * 返回错误提示信息
     *
     * @param {*} [action={}]
     * @returns {string}
     * @memberof IBizForm
     */
    IBizForm.prototype.getActionErrorInfo = function (action) {
        if (action === void 0) { action = {}; }
        if (action.failureType === 'CONNECT_FAILURE') {
            return 'Status:' + action.response.status + ': ' + action.response.statusText;
        }
        if (action.failureType === 'SERVER_INVALID') {
            var msg_1;
            if (action.errorMessage) {
                msg_1 = action.errorMessage;
            }
            if (action.error && action.error.items) {
                var items = action.error.items;
                items.forEach(function (item, index) {
                    if (index >= 5) {
                        msg_1 += ('...... ');
                        return false;
                    }
                    if (item.info && !Object.is(item.info, '') && msg_1.indexOf(item.info) < 0) {
                        msg_1 += item.info;
                    }
                });
            }
            return msg_1;
        }
        if (action.failureType === 'CLIENT_INVALID') {
            return '';
        }
        if (action.failureType === 'LOAD_FAILURE') {
            return '';
        }
    };
    /**
     * 填充表单
     *
     * @param {*} [data={}]
     * @memberof IBizForm
     */
    IBizForm.prototype.fillForm = function (data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var fillDatas = Object.keys(data);
        fillDatas.forEach(function (name) {
            var field = _this.findField(name);
            if (field) {
                var _value = data[name];
                if (_value instanceof Array || _value instanceof Object) {
                    _value = JSON.stringify(_value);
                }
                field.setValue(_value);
            }
        });
    };
    /**
     * 设置表单项值
     *
     * @param {string} name
     * @param {*} value
     * @memberof IBizForm
     */
    IBizForm.prototype.setFieldValue = function (name, value) {
        var field = this.findField(name);
        if (field) {
            field.setValue(value);
        }
    };
    /**
     * 获取表单项值
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.getFieldValue = function (name) {
        var field = this.findField(name);
        if (!field) {
            // IBiz.alert(IGM('IBIZFORM.GETFIELDVALUE.TITLE', '获取失败'), IGM('IBIZFORM.GETFIELDVALUE.INFO', '无法获取表单项[' + name + ']', [name]), 2);
            this.iBizNotification.error('获取失败', '无法获取表单项[' + name + ']');
            return '';
        }
        return field.getValue();
    };
    /**
     * 设置表单项允许为空
     *
     * @param {string} name
     * @param {boolean} allowblank
     * @memberof IBizForm
     */
    IBizForm.prototype.setFieldAllowBlank = function (name, allowblank) {
        var field = this.findField(name);
        if (field) {
            field.setAllowBlank(allowblank);
        }
    };
    /**
     * 设置表单项属性是否禁用
     *
     * @param {string} name
     * @param {boolean} disabled
     * @memberof IBizForm
     */
    IBizForm.prototype.setFieldDisabled = function (name, disabled) {
        var field = this.findField(name);
        if (field) {
            field.setDisabled(disabled);
        }
    };
    /**
     * 设置表单错误
     *
     * @param {*} formerror
     * @param {string} errorType
     * @memberof IBizForm
     */
    IBizForm.prototype.setFormError = function (formerror, errorType) {
        var _this = this;
        this.resetFormError();
        if (formerror && formerror.items) {
            var errorItems = formerror.items;
            errorItems.forEach(function (item) {
                var name = item.id;
                if (name) {
                    var _item = _this.fields[name];
                    _item.setErrorInfo({ validateStatus: 'error', hasError: true, errorInfo: item.info, errorType: errorType });
                }
            });
        }
    };
    /**
     *
     *
     * @memberof IBizForm
     */
    IBizForm.prototype.resetFormError = function () {
        var _this = this;
        var itemsData = Object.keys(this.fields);
        itemsData.forEach(function (name) {
            var item = _this.fields[name];
            item.setErrorInfo({ validateStatus: 'success', hasError: false, errorInfo: '', errorType: '' });
        });
    };
    /**
     * 设置面板,表单项<分组、分页面板>隐藏
     *
     * @param {string} name
     * @param {boolean} visible
     * @memberof IBizForm
     */
    IBizForm.prototype.setPanelVisible = function (name, visible) {
        var field = this.findField(name);
        if (field) {
            field.setVisible(visible);
        }
    };
    /**
     * 获取当前表单项值
     *
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.getActiveData = function () {
        var _this = this;
        // tslint:disable-next-line:prefer-const
        var values = {};
        var items = Object.keys(this.fields);
        items.forEach(function (name) {
            var field = _this.findField(name);
            if (field && (Object.is(field.fieldType, 'FORMITEM') || Object.is(field.fieldType, 'HIDDENFORMITEM'))) {
                var value = field.getValue();
                if (Object.keys(values).length <= 1000) {
                    values[name] = value;
                }
            }
        });
        return values;
    };
    /**
     * 获取全部表单项值
     *
     * @returns {*}
     * @memberof IBizForm
     */
    IBizForm.prototype.getValues = function () {
        var _this = this;
        // tslint:disable-next-line:prefer-const
        var values = {};
        var items = Object.keys(this.fields);
        items.forEach(function (name) {
            var field = _this.findField(name);
            if (field && (Object.is(field.fieldType, 'FORMITEM') || Object.is(field.fieldType, 'HIDDENFORMITEM'))) {
                var value = field.getValue();
                values[name] = value;
            }
        });
        return values;
    };
    /**
     *
     *
     * @param {*} value
     * @returns {boolean}
     * @memberof IBizForm
     */
    IBizForm.prototype.testFieldEnableReadonly = function (value) {
        return false;
    };
    /**
     * 更新表单项
     *
     * @param {string} mode 更新模式
     * @returns {void}
     * @memberof IBizForm
     */
    IBizForm.prototype.updateFormItems = function (mode) {
        var _this = this;
        if (this.ignoreUFI) {
            return;
        }
        var activeData = this.getActiveData();
        // tslint:disable-next-line:prefer-const
        var arg = {};
        this.fire(IBizForm.UPDATEFORMITEMBEFORE, activeData);
        Object.assign(arg, { srfaction: 'updateformitem', srfufimode: mode, srfactivedata: JSON.stringify(activeData), srfctrlid: this.getName() });
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;
        this.load(arg).subscribe(function (action) {
            _this.fire(IBizForm.UPDATEFORMITEMED, action.data);
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            if (action.dataaccaction) {
                _this.setDataAccAction(action.dataaccaction);
            }
            _this.fillForm(action.data);
            _this.fire(IBizForm.UPDATEFORMITEMS, { ufimode: arg.srfufimode, data: action.data });
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            _this.fire(IBizForm.FORMFIELDCHANGED, null);
        }, function (action) {
            action.failureType = 'SERVER_INVALID';
            // IBiz.alert(IGM('IBIZFORM.UPDATEFORMITEMS.TITLE', '更新失败'), IGM('IBIZFORM.UPDATEFORMITEMS.INFO', '更新表单项发生错误,' + action.info, [action.info]), 2);
            _this.iBizNotification.error('更新失败', '更新表单项发生错误, ' + action.info);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
        });
    };
    /**
     * 重置表单
     *
     * @memberof IBizForm
     */
    IBizForm.prototype.reset = function () {
        this.autoLoad();
    };
    /**
     * 获取表单类型
     *
     * @returns {string}
     * @memberof IBizForm
     */
    IBizForm.prototype.getFormType = function () {
        return undefined;
    };
    /**
     *
     *
     * @param {string} fieldName
     * @param {boolean} state
     * @param {string} errorInfo
     * @memberof IBizForm
     */
    IBizForm.prototype.setFormFieldChecked = function (fieldName, state, errorInfo) {
        var field = this.findField(fieldName);
        if (field) {
            field.setErrorInfo({ validateStatus: state ? 'error' : 'success', hasError: state ? true : false, errorInfo: state ? errorInfo : '', errorType: 'FRONTEND' });
        }
    };
    /**
     * 表单加载完成事件
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.FORMLOADED = 'FORMLOADED';
    /**
     * 表单属性值变化事件
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.FORMFIELDCHANGED = 'FORMFIELDCHANGED';
    /**
     * 表单保存完成
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.FORMSAVED = 'FORMSAVED';
    /**
     * 表单删除完成
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.FORMREMOVED = 'FORMREMOVED';
    /**
     * 表单工作流启动完成
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.FORMWFSTARTED = 'FORMWFSTARTED';
    /**
     * 表单工作流提交完成
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.FORMWFSUBMITTED = 'FORMWFSUBMITTED';
    /**
     * 表单权限发生变化
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.DATAACCACTIONCHANGE = 'DATAACCACTIONCHANGE';
    /**
     * 表单项更新
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.UPDATEFORMITEMS = 'UPDATEFORMITEMS';
    /**
     * 表单项更新之前
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.UPDATEFORMITEMBEFORE = 'UPDATEFORMITEMBEFORE';
    /**
     * 表单项更新完成
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.UPDATEFORMITEMED = 'UPDATEFORMITEMED';
    /**
     * 表单加载之前
     *
     * @static
     * @memberof IBizForm
     */
    IBizForm.BEFORELOAD = 'BEFORELOAD';
    return IBizForm;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 编辑表单
 *
 * @class IBizEditForm
 * @extends {IBizForm}
 */
var IBizEditForm = /** @class */ (function (_super) {
    __extends(IBizEditForm, _super);
    /**
     * Creates an instance of IBizEditForm.
     * 创建 IBizEditForm 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditForm
     */
    function IBizEditForm(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 数据保存
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.save2 = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        if (!this.isAllowSave()) {
            this.iBizNotification.error('错误', '表单项值异常');
            return;
        }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        if (opt) {
            Object.assign(arg, opt);
        }
        var data = this.getValues();
        Object.assign(arg, data);
        if (Object.is(data.srfuf, '1')) {
            Object.assign(arg, { srfaction: 'update', srfctrlid: this.getName() });
        }
        else {
            Object.assign(arg, { srfaction: 'create', srfctrlid: this.getName() });
        }
        arg.srfcancel = false;
        this.fire(IBizEditForm.FORMBEFORESAVE, arg);
        if (arg.srfcancel) {
            return;
        }
        delete arg.srfcancel;
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;
        this.submit(arg).subscribe(function (action) {
            _this.resetFormError();
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            _this.setDataAccAction(action.dataaccaction);
            _this.fillForm(action.data);
            _this.formDirty = false;
            // 判断是否有提示
            if (action.info && !Object.is(action.info, '')) {
                // IBiz.alert('', action.info, 1);
                _this.iBizNotification.info('', action.info);
            }
            // this.fireEvent('formsaved', this, action);
            _this.fire(IBizForm.FORMSAVED, action);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            // this.fireEvent('formfieldchanged', null);
            _this.fire(IBizForm.FORMFIELDCHANGED, null);
            _this.onSaved();
        }, function (action) {
            if (action.error) {
                _this.setFormError(action.error, 'BACKEND');
            }
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            // this.fireEvent(IBizEditForm.FORMSAVEERROR, this);
            _this.fire(IBizEditForm.FORMSAVEERROR, _this);
            action.failureType = 'SERVER_INVALID';
            if (action.ret === 10) {
                _this.iBizNotification.confirm('保存错误信息', '保存数据发生错误, ' + _this.getActionErrorInfo(action) + ', 是否要重新加载数据？').subscribe(function (result) {
                    if (result && Object.is(result, 'OK')) {
                        _this.reload();
                    }
                });
            }
            else if (action.ret === 19) {
                var calcel_1 = {};
                var ok_1 = {};
                if (action.confirmoptions && Array.isArray(action.confirmoptions)) {
                    action.confirmoptions.forEach(function (confirmoption) {
                        var _a, _b;
                        if (Object.is(confirmoption.confirmvalue, 'ok')) {
                            Object.assign(ok_1, (_a = {}, _a[action.confirmkey] = confirmoption.confirmvalue, _a));
                        }
                        if (Object.is(confirmoption.confirmvalue, 'calcel')) {
                            Object.assign(calcel_1, (_b = {}, _b[action.confirmkey] = confirmoption.confirmvalue, _b));
                        }
                    });
                }
                _this.iBizNotification.confirm(action.confirmtitle, action.confirmmsg).subscribe(function (result) {
                    if (Object.is(result, 'OK')) {
                        _this.save2(ok_1);
                    }
                });
            }
            else {
                _this.iBizNotification.error('保存错误信息', '保存数据发生错误,' + _this.getActionErrorInfo(action));
            }
        });
    };
    /**
     *
     *
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.onSaved = function () {
    };
    /**
     * 表单数据刷新
     *
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.reload = function () {
        var field = this.findField('srfkey');
        // tslint:disable-next-line:prefer-const
        var loadarg = {};
        if (field) {
            loadarg.srfkey = field.getValue();
            if (loadarg.srfkey.indexOf('SRFTEMPKEY:') === 0) {
                field = this.findField('srforikey');
                if (field) {
                    loadarg.srfkey = field.getValue();
                }
            }
            var viewController = this.getViewController();
            if (viewController) {
                var viewParmams = viewController.getViewParam();
                if (!Object.is(loadarg.srfkey, viewParmams.srfkey)) {
                    loadarg.srfkey = viewParmams.srfkey;
                }
            }
        }
        this.autoLoad(loadarg);
    };
    /**
     * 删除
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.remove = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        if (opt) {
            Object.assign(arg, opt);
        }
        if (!arg.srfkey) {
            var field = this.findField('srfkey');
            if (field) {
                arg.srfkey = field.getValue();
            }
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert(IGM('IBIZEDITFORM.REMOVEFAILED.TITLE', '删除错误信息'), IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.iBizNotification.error('删除错误信息', '当前表单未加载数据！');
            return;
        }
        Object.assign(arg, { srfaction: 'remove', srfctrlid: this.getName() });
        this.ignoreUFI = true;
        this.load(arg).subscribe(function (action) {
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            // this.fireEvent(IBizForm.FORMREMOVED);
            _this.fire(IBizForm.FORMREMOVED, _this);
        }, function (action) {
            action.failureType = 'SERVER_INVALID';
            _this.iBizNotification.error('删除错误信息', '删除数据发生错误, ' + _this.getActionErrorInfo(action));
            _this.ignoreUFI = false;
        });
    };
    /**
     * 工作流启动
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.wfstart = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        if (!this.isAllowSave()) {
            this.iBizNotification.error('错误', '表单项值异常');
            return;
        }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        if (opt) {
            Object.assign(arg, opt);
        }
        if (!arg.srfkey) {
            var field = this.findField('srfkey');
            if (field) {
                arg.srfkey = field.getValue();
            }
            field = this.findField('srforikey');
            if (field) {
                // tslint:disable-next-line:prefer-const
                var v = field.getValue();
                if (v && !Object.is(v, '')) {
                    arg.srfkey = v;
                }
            }
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert(IGM('IBIZEDITFORM.WFSTARTFAILED.TITLE', '启动流程错误信息'), IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.iBizNotification.error('启动流程错误信息', '当前表单未加载数据！');
            return;
        }
        Object.assign(arg, { srfaction: 'wfstart', srfctrlid: this.getName() });
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;
        this.iBizHttp.post(this.getBackendUrl(), arg).subscribe(function (action) {
            if (action.ret !== 0) {
                action.failureType = 'SERVER_INVALID';
                _this.iBizNotification.error('启动流程错误信息', '启动流程发生错误,' + _this.getActionErrorInfo(action));
                _this.ignoreUFI = false;
                _this.ignoreformfieldchange = false;
                return;
            }
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            _this.setDataAccAction(action.dataaccaction);
            _this.fillForm(action.data);
            _this.formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED);
            // this.fireEvent(IBizForm.FORMWFSTARTED);
            _this.fire(IBizForm.FORMWFSTARTED, _this);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            _this.fire(IBizForm.FORMFIELDCHANGED, null);
        }, function (action) {
            if (action.error) {
                _this.setFormError(action.error, 'BACKEND');
            }
            action.failureType = 'SERVER_INVALID';
            _this.iBizNotification.error('启动流程错误信息', '启动流程发生错误,' + _this.getActionErrorInfo(action));
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
        });
    };
    /**
     * 工作流提交
     *
     * @param {*} [opt={}]
     * @returns {void}
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.wfsubmit = function (opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        if (!this.isAllowSave()) {
            this.iBizNotification.error('错误', '表单项值异常');
            return;
        }
        // tslint:disable-next-line:prefer-const
        var arg = {};
        if (opt) {
            Object.assign(arg, opt);
        }
        var data = this.getValues();
        Object.assign(arg, data);
        Object.assign(arg, { srfaction: 'wfsubmit', srfctrlid: this.getName() });
        //        if (!arg.srfkey) {
        //            var field = this.findField('srfkey');
        //            if (field) {
        //                arg.srfkey = field.getValue();
        //            }
        //        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            // IBiz.alert(IGM('IBIZEDITFORM.WFSUBMITFAILED.TITLE', '提交流程错误信息'), IGM('IBIZEDITFORM.UNLOADDATA', '当前表单未加载数据！'), 2);
            this.iBizNotification.error('提交流程错误信息', '当前表单未加载数据！');
            return;
        }
        this.ignoreUFI = true;
        this.ignoreformfieldchange = true;
        this.load(arg).subscribe(function (action) {
            _this.setFieldAsyncConfig(action.config);
            _this.setFieldJurisdiction(action.data);
            _this.setFieldState(action.state);
            _this.setDataAccAction(action.dataaccaction);
            _this.fillForm(action.data);
            _this.formDirty = false;
            // this.fireEvent(IBizForm.FORMLOADED);
            // this.fireEvent(IBizForm.FORMWFSUBMITTED);
            _this.fire(IBizForm.FORMWFSUBMITTED, _this);
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
            // this.fireEvent(IBizForm.FORMFIELDCHANGED, null);
            _this.fire(IBizForm.FORMFIELDCHANGED, null);
        }, function (action) {
            if (action.error) {
                _this.setFormError(action.error, 'BACKEND');
            }
            action.failureType = 'SERVER_INVALID';
            _this.iBizNotification.error('提交流程错误信息', '工作流提交发生错误,' + _this.getActionErrorInfo(action));
            _this.ignoreUFI = false;
            _this.ignoreformfieldchange = false;
        });
    };
    /**
     * 界面行为
     *
     * @param {*} [arg={}]
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.doUIAction = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        if (arg) {
            Object.assign(opt, arg);
        }
        Object.assign(opt, { srfaction: 'uiaction', srfctrlid: this.getName() });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (data) {
            if (data.ret === 0) {
                // IBiz.processResultBefore(data);
                _this.fire(IBizEditForm.UIACTIONFINISHED, data);
                if (data.reloadData) {
                    _this.reload();
                }
                if (data.info && !Object.is(data.info, '')) {
                    _this.iBizNotification.info('', data.info);
                }
                // IBiz.processResult(data);
            }
            else {
                _this.iBizNotification.error('界面操作错误信息', '操作失败,' + data.errorMessage);
            }
        }, function (error) {
            _this.iBizNotification.error('界面操作错误信息', '操作失败,' + error.info);
        });
    };
    /**
     * 表单类型
     *
     * @returns {string}
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.getFormType = function () {
        return 'EDITFORM';
    };
    /**
     * 是否允许保存
     *
     * @returns {boolean}
     * @memberof IBizEditForm
     */
    IBizEditForm.prototype.isAllowSave = function () {
        var _this = this;
        var allow = true;
        var fieldNames = Object.keys(this.fields);
        fieldNames.some(function (name) {
            var field = _this.findField(name);
            if (field && field.hasActiveError() && Object.is(field.getErrorType(), 'FRONTEND')) {
                allow = false;
                return true;
            }
        });
        return allow;
    };
    /**
     * 表单权限发生变化
     */
    IBizEditForm.UIACTIONFINISHED = 'UIACTIONFINISHED';
    /**
     * 表单保存之前触发
     */
    IBizEditForm.FORMBEFORESAVE = 'FORMBEFORESAVE';
    /**
     * 表单保存错误触发
     */
    IBizEditForm.FORMSAVEERROR = 'FORMSAVEERROR';
    return IBizEditForm;
}(IBizForm));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 搜索表单
 *
 * @class IBizSearchForm
 * @extends {IBizForm}
 */
var IBizSearchForm = /** @class */ (function (_super) {
    __extends(IBizSearchForm, _super);
    /**
     * Creates an instance of IBizSearchForm.
     * 创建 IBizSearchForm 实例
     *
     * @param {*} [opt={}]
     * @memberof IBizSearchForm
     */
    function IBizSearchForm(opt) {
        if (opt === void 0) { opt = {}; }
        var _this = _super.call(this, opt) || this;
        /**
         * 是搜重置搜索
         *
         * @type {boolean}
         * @memberof IBizSearchForm
         */
        _this.bResetting = false;
        /**
         * 是否有更多搜索
         *
         * @memberof IBizSearchForm
         */
        _this.searchMore = false;
        /**
         * 搜索表单是否打开
         *
         * @type {boolean}
         * @memberof IBizSearchForm
         */
        _this.opened = false;
        return _this;
    }
    /**
     * 表单类型
     *
     * @returns {string}
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.getFormType = function () {
        return 'SEARCHFORM';
    };
    /**
     * 更多搜索
     *
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.toggleSearchMore = function () {
        this.searchMore = !this.searchMore;
    };
    /**
     * 执行搜索功能
     *
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.onSearch = function () {
        this.fire(IBizSearchForm.FORMSEARCHED, null);
    };
    /**
     * 重置表单
     *
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.onReset = function () {
        this.bResetting = true;
        this.reset();
    };
    /**
     * 搜索表单草稿加载完成
     *
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.onDraftLoaded = function () {
        _super.prototype.onDraftLoaded.call(this);
        if (this.bResetting) {
            this.bResetting = false;
            this.fire(IBizSearchForm.FORMRESETED, null);
        }
    };
    /**
     * 搜索表单加载完成
     *
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.onLoaded = function () {
        _super.prototype.onLoaded.call(this);
        if (this.bResetting) {
            this.bResetting = false;
            this.fire(IBizSearchForm.FORMRESETED, null);
        }
    };
    /**
     * 搜索功能是否支持,全支持
     *
     * @returns {boolean}
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.isOpen = function () {
        return this.opened;
    };
    /**
     * 设置搜索表单是否展开
     *
     * @param {boolean} open
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.setOpen = function (open) {
        this.opened = open;
    };
    /**
     * 关闭搜索功能
     *
     * @memberof IBizSearchForm
     */
    IBizSearchForm.prototype.close = function () {
        this.opened = false;
    };
    /*****************事件声明************************/
    /**
     * 搜索表单重置事件
     */
    IBizSearchForm.FORMRESETED = 'FORMRESETED';
    /**
     * 搜索表单搜索事件
     */
    IBizSearchForm.FORMSEARCHED = 'FORMSEARCHED';
    /**
     * 搜索表单收缩事件
     */
    IBizSearchForm.FORMCONTRACT = 'FORMCONTRACT';
    return IBizSearchForm;
}(IBizForm));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 分页
 *
 * @class IBizTab
 * @extends {IBizControl}
 */
var IBizTab = /** @class */ (function (_super) {
    __extends(IBizTab, _super);
    /**
     * Creates an instance of IBizTab.
     * 创建 IBizTab 实例
     * @param {*} [opts={}]
     * @memberof IBizTab
     */
    function IBizTab(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 激活分页
         *
         * @type {*}
         * @memberof IBizTab
         */
        _this.activeTab = {};
        /**
         * 分页部件对象
         *
         * @type {*}
         * @memberof IBizTab
         */
        _this.tabs = {};
        _this.regTabs();
        return _this;
    }
    /**
     * 注册所有分页部件对象
     *
     * @memberof IBizTab
     */
    IBizTab.prototype.regTabs = function () {
    };
    /**
     * 注册分页部件对象
     *
     * @param {*} [tab={}]
     * @memberof IBizTab
     */
    IBizTab.prototype.regTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        if (Object.keys(tab).length > 0 && tab.name) {
            var viewController = this.getViewController();
            var view = viewController.getDRItemView({ viewid: tab.name });
            if (view) {
                Object.assign(tab, view);
            }
            this.tabs[tab.name] = tab;
        }
    };
    /**
     * 获取分页部件对象
     *
     * @param {string} name
     * @param {string} [routepath]
     * @returns {*}
     * @memberof IBizTab
     */
    IBizTab.prototype.getTab = function (name, routepath) {
        var tab_arr = Object.values(this.tabs);
        var tab = {};
        tab_arr.some(function (_tab) {
            if (Object.is(_tab.name, name) || (_tab.routepath && Object.is(_tab.routepath, routepath))) {
                Object.assign(tab, _tab);
                return true;
            }
        });
        return tab;
    };
    /**
     * 设置激活分页
     *
     * @param {*} [tab={}]
     * @memberof IBizTab
     */
    IBizTab.prototype.setActiveTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        if (!Object.is(tab.name, this.activeTab.name)) {
            this.activeTab = {};
            Object.assign(this.activeTab, tab);
        }
    };
    /**
     * 设置分页状态
     *
     * @param {boolean} state
     * @memberof IBizTab
     */
    IBizTab.prototype.setTabState = function (state) {
    };
    return IBizTab;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 导航分页
 *
 * @class IBizExpTab
 * @extends {IBizTab}
 */
var IBizExpTab = /** @class */ (function (_super) {
    __extends(IBizExpTab, _super);
    /**
     * Creates an instance of IBizExpTab.
     * 创建 IBizExpTab 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizExpTab
     */
    function IBizExpTab(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizExpTab;
}(IBizTab));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 关系分页
 *
 * @class IBizDRTab
 * @extends {IBizTab}
 */
var IBizDRTab = /** @class */ (function (_super) {
    __extends(IBizDRTab, _super);
    /**
     * Creates an instance of IBizDRTab.
     * 创建 IBizDRTab 实例
     * @param {*} [opts={}]
     * @memberof IBizDRTab
     */
    function IBizDRTab(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 父数据对象
         *
         * @type {*}
         * @memberof IBizDRTab
         */
        _this.srfParentData = {};
        return _this;
    }
    /**
     * 设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizDRTab
     */
    IBizDRTab.prototype.setParentData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentData = {};
        Object.assign(this.srfParentData, data);
    };
    /**
     * 获取父数据
     *
     * @returns {*}
     * @memberof IBizDRTab
     */
    IBizDRTab.prototype.getParentData = function () {
        return this.srfParentData;
    };
    /**
     * 分页部件选中变化
     *
     * @param {string} name
     * @returns {void}
     * @memberof IBizDRTab
     */
    IBizDRTab.prototype.onTabSelectionChange = function (name) {
        var viewid = name;
        var controller = this.getViewController();
        var parentKey = '';
        if (this.srfParentData.srfparentkey) {
            parentKey = this.srfParentData.srfparentkey;
        }
        if (!parentKey || Object.is(parentKey, '')) {
            this.iBizNotification.warning('警告', '请先建立主数据');
            var tab_1 = this.getTab('form');
            this.setActiveTab(tab_1);
            return;
        }
        if (Object.is(viewid, 'form')) {
            this.fire(IBizDRTab.SELECTCHANGE, { parentMode: {}, parentData: {}, viewid: 'form' });
            var tab_2 = this.getTab('form');
            this.setActiveTab(tab_2);
            return;
        }
        var dritem = { viewid: viewid.toLocaleUpperCase() };
        if (!dritem.viewid || Object.is(dritem.viewid, '')) {
            return;
        }
        var viewItem = controller.getDRItemView(dritem);
        if (viewItem == null || !viewItem.viewparam) {
            return;
        }
        var item = { viewparam: {} };
        Object.assign(item, viewItem);
        Object.assign(item.viewparam, this.getParentData());
        var tab = this.getTab(viewid);
        Object.assign(item, { index: tab.index, name: tab.name });
        this.fire(IBizDRTab.SELECTCHANGE, item);
    };
    /**
     * 设置分页状态
     *
     * @param {boolean} state
     * @memberof IBizDRTab
     */
    IBizDRTab.prototype.setTabState = function (state) {
        var _this = this;
        var tab_names = Object.keys(this.tabs);
        tab_names.forEach(function (name) {
            if (Object.is(name, 'form')) {
                _this.tabs[name].disabled = false;
            }
            else {
                _this.tabs[name].disabled = state;
            }
        });
    };
    /**
     * 关系分页选中
     *
     * @static
     * @memberof IBizDRTab
     */
    IBizDRTab.SELECTCHANGE = 'SELECTCHANGE';
    return IBizDRTab;
}(IBizTab));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多视图面板
 *
 * @class IBizViewPanel
 * @extends {IBizTab}
 */
var IBizViewPanel = /** @class */ (function (_super) {
    __extends(IBizViewPanel, _super);
    /**
     * Creates an instance of IBizViewPanel.
     * 创建 IBizViewPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizViewPanel
     */
    function IBizViewPanel(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizViewPanel;
}(IBizTab));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 拾取数据视图面板
 *
 * @class IBizPickupViewPanel
 * @extends {IBizViewPanel}
 */
var IBizPickupViewPanel = /** @class */ (function (_super) {
    __extends(IBizPickupViewPanel, _super);
    /**
     * Creates an instance of IBizPickupViewPanel.
     * 创建 IBizPickupViewPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupViewPanel
     */
    function IBizPickupViewPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 父数据
         *
         * @type {*}
         * @memberof IBizPickupViewPanel
         */
        _this.srfParentData = {};
        /**
         * 刷新关联视图计数
         *
         * @type {number}
         * @memberof IBizPickupViewPanel
         */
        _this.refreshRefView = 0;
        /**
         * 选中数据
         *
         * @type {Array<any>}
         * @memberof IBizPickupViewPanel
         */
        _this.selections = [];
        /**
         * 所有数据
         *
         * @type {Array<any>}
         * @memberof IBizPickupViewPanel
         */
        _this.allData = [];
        return _this;
    }
    /**
     * 获取所有数据
     *
     * @returns {Array<any>}
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.getAllData = function () {
        return this.allData;
    };
    /**
     * 获取所有选中数据
     *
     * @returns {Array<any>}
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.getSelections = function () {
        return this.selections;
    };
    /**
     * 数据选中
     *
     * @param {Array<any>} event
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.onSelectionChange = function (event) {
        this.selections = event;
        this.fire(IBizPickupViewPanel.SELECTIONCHANGE, this.selections);
    };
    /**
     * 数据激活
     *
     * @param {Array<any>} event
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.onDataActivated = function (event) {
        this.selections = event;
        this.fire(IBizPickupViewPanel.DATAACTIVATED, this.selections);
    };
    /**
     * 全部数据
     *
     * @param {Array<any>} event
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.onAllData = function (event) {
        this.allData = event;
        this.fire(IBizPickupViewPanel.ALLDATA, this.allData);
    };
    /**
     * 设置父数据
     *
     * @param {*} [parentData={}]
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.setParentData = function (parentData) {
        if (parentData === void 0) { parentData = {}; }
        this.srfParentData = {};
        Object.assign(this.srfParentData, parentData);
    };
    /**
     * 刷新面板
     *
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.prototype.refreshViewPanel = function () {
        var _this = this;
        setTimeout(function () {
            _this.refreshRefView += 1;
        }, 10);
    };
    /*****************事件声明************************/
    /**
     * 数据选中
     *
     * @static
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.SELECTIONCHANGE = 'SELECTIONCHANGE';
    /**
     * 数据激活
     *
     * @static
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.DATAACTIVATED = 'DATAACTIVATED';
    /**
     * 数据全选
     *
     * @static
     * @memberof IBizPickupViewPanel
     */
    IBizPickupViewPanel.ALLDATA = 'ALLDATA';
    return IBizPickupViewPanel;
}(IBizViewPanel));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 *  树部件
 *
 * @class IBizTree
 * @extends {IBizControl}
 */
var IBizTree = /** @class */ (function (_super) {
    __extends(IBizTree, _super);
    /**
     * Creates an instance of IBizTree.
     * 创建 IBizTree 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizTree
     */
    function IBizTree(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 默认展开节点id数组
         *
         * @type {Array<any>}
         * @memberof IBizTree
         */
        _this.expandedKeys = [];
        /**
         * 树部件是否收缩，默认展开
         *
         * @type {boolean}
         * @memberof IBizTree
         */
        _this.isCollapsed = true;
        /**
         * 数据项节点集合
         *
         * @type {Array<any>}
         * @memberof IBizTree
         */
        _this.items = [];
        /**
         * 默认节点
         *
         * @private
         * @type {*}
         * @memberof IBizTree
         */
        _this.node = {};
        /**
         *
         *
         * @type {*}
         * @memberof IBizTree
         */
        _this.selectNode = {};
        /**
         * 搜索值
         *
         * @type {string}
         * @memberof IBizTree
         */
        _this.searchValue = '';
        /**
         * 根节点渲染方法
         *
         * @type {*}
         * @memberof IBizTree
         */
        _this.rootResolve = null;
        /**
         * 根节点
         *
         * @type {*}
         * @memberof IBizTree
         */
        _this.rootNode = null;
        /**
         * 树初始化
         *
         * @type {boolean}
         * @memberof IBizTree
         */
        _this.initTree = true;
        return _this;
    }
    /**
     * 加载节点数据
     *
     * @param {*} [treeCfg={}]
     * @memberof IBizTree
     */
    IBizTree.prototype.load = function (treeCfg) {
        var _this = this;
        if (treeCfg === void 0) { treeCfg = {}; }
        this.expandedKeys = [];
        this.isCollapsed = true;
        this.items = [];
        this.node = {};
        this.selectNode = {};
        this.searchValue = '';
        this.rootResolve = null;
        this.rootNode = null;
        this.initTree = false;
        setTimeout(function () {
            _this.initTree = true;
        }, 200);
    };
    /**
     * 获取选择节点数据
     *
     * @param {any} bFull true：返回的数据包含节点全部数据，false：返回的数据仅包含节点ID
     * @returns {*}
     * @memberof IBizTree
     */
    IBizTree.prototype.getSelected = function (bFull) {
    };
    /**
     * 获取所有节点数据
     *
     * @returns {Array<any>}
     * @memberof IBizTree
     */
    IBizTree.prototype.getNodes = function () {
        return this.items;
    };
    /**
     * 节点重新加载
     *
     * @param {*} [node={}]
     * @memberof IBizTree
     */
    IBizTree.prototype.reload = function (node) {
        if (node === void 0) { node = {}; }
        if (Object.keys(node).length === 0) {
            return;
        }
        var treeObj = this.getViewController().$vue.$refs[this.getName()];
        treeObj.updateKeyChildren(node.id, []);
        this.load2({ data: node }, this.rootResolve);
    };
    /**
     * 删除节点
     *
     * @param {any} node
     * @memberof IBizTree
     */
    IBizTree.prototype.remove = function (node) {
    };
    /**
     * 实体界面行为
     *
     * @param {any} params
     * @memberof IBizTree
     */
    IBizTree.prototype.doUIAction = function (params) {
    };
    /**
     * 格式化树数据
     *
     * @private
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizTree
     */
    IBizTree.prototype.formatTreeData = function (items) {
        var _this = this;
        var data = [];
        items.forEach(function (item) {
            var tempData = {};
            Object.assign(tempData, item);
            tempData.name = tempData.text;
            if (item.expanded) {
                _this.expandedKeys.push(item.id);
            }
            data.push(tempData);
        });
        return data;
    };
    /**
     * 树节点激活加载子数据
     *
     * @param {*} node
     * @param {*} resolve
     * @memberof IBizTree
     */
    IBizTree.prototype.loadChildren = function (node, resolve) {
        var _this = this;
        if (!this.rootNode && node.id > 0 && !Object.is(node.data.id, '#')) {
            this.rootNode = node;
            this.rootResolve = resolve;
        }
        if (this.searchValue) {
            this.load2(node, resolve);
        }
        var param = {
            srfnodeid: node.data && node.data.id ? node.data.id : '#', srfaction: 'fetch', srfrender: 'JSTREE',
            srfviewparam: JSON.stringify(this.getViewController().getViewParam()),
            srfctrlid: this.getName()
        };
        this.fire(IBizMDControl.BEFORELOAD, param);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (result) {
            if (result.ret !== 0) {
                _this.iBizNotification.error('错误', result.info);
                resolve([]);
                return;
            }
            var _items = _this.formatTreeData(result.items).slice();
            if (node.level === 0) {
                _this.items = _items.slice();
                _this.fire(IBizTree.CONTEXTMENU, _this.items);
            }
            resolve(_items);
        }, function (error) {
            _this.iBizNotification.error('错误', error.info);
            resolve([]);
        });
    };
    /**
     * 树节点激活选中数据
     *
     * @param {*} [data={}]
     * @memberof IBizTree
     */
    IBizTree.prototype.nodeSelect = function (data) {
        if (data === void 0) { data = {}; }
        this.fire(IBizTree.SELECTIONCHANGE, [data]);
    };
    /**
     *
     *
     * @param {*} [item={}]
     * @memberof IBizTree
     */
    IBizTree.prototype.setSelectTreeItem = function (item) {
        if (item === void 0) { item = {}; }
        Object.assign(this.selectNode, item);
    };
    /**
     *
     *
     * @param {string} val
     * @memberof IBizTree
     */
    IBizTree.prototype.onSearch = function (val) {
        this.load2(this.rootNode, this.rootResolve);
    };
    /**
     * 带搜索条件的加载
     *
     * @param {*} resolve
     * @memberof IBizTree
     */
    IBizTree.prototype.load2 = function (node, resolve) {
        var _this = this;
        var param = {
            srfnodeid: node.data && node.data.id ? node.data.id : '#', srfaction: 'fetch',
            srfviewparam: JSON.stringify(this.getViewController().getViewParam()),
            srfctrlid: this.getName(),
            query: this.searchValue,
            srfnodefilter: this.searchValue,
            node: node.data.id
        };
        this.fire(IBizMDControl.BEFORELOAD, param);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (result) {
            if (result.ret !== 0) {
                _this.iBizNotification.error('错误', result.info);
                resolve([]);
                return;
            }
            var _items = _this.formatTreeData(result.items).slice();
            if (node.level === 0) {
                _this.items = _items.slice();
                _this.fire(IBizTree.CONTEXTMENU, _this.items);
            }
            var treeObj = _this.getViewController().$vue.$refs[_this.getName()];
            treeObj.updateKeyChildren(node.data.id, _items);
        }, function (error) {
            _this.iBizNotification.error('错误', error.info);
            resolve([]);
        });
    };
    /*****************事件声明************************/
    /**
     * 选择变化
     *
     * @static
     * @memberof IBizTree
     */
    IBizTree.SELECTIONCHANGE = "SELECTIONCHANGE";
    /**
     * 上下文菜单
     *
     * @static
     * @memberof IBizTree
     */
    IBizTree.CONTEXTMENU = "CONTEXTMENU";
    return IBizTree;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 树导航
 *
 * @class IBizTreeExpBar
 * @extends {IBizControl}
 */
var IBizTreeExpBar = /** @class */ (function (_super) {
    __extends(IBizTreeExpBar, _super);
    /**
     * Creates an instance of IBizTreeExpBar.
     * 创建 IBizTreeExpBar 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizTreeExpBar
     */
    function IBizTreeExpBar(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 选中项
         *
         * @type {*}
         * @memberof IBizTreeExpBar
         */
        _this.selectItem = {};
        var viewController = _this.getViewController();
        if (viewController) {
            viewController.on(IBizViewController.INITED).subscribe(function () {
                var tree = viewController.controls.get(_this.getName() + '_tree');
                _this.tree = tree;
                if (_this.tree) {
                    _this.tree.on(IBizTree.SELECTIONCHANGE).subscribe(function (args) {
                        _this.onTreeSelectionChange(args);
                    });
                    _this.tree.on(IBizTree.CONTEXTMENU).subscribe(function (args) {
                        _this.onTreeContextMenu(args);
                    });
                    // this.tree.load({});
                }
            });
        }
        return _this;
    }
    /**
     * 获取树控件
     *
     * @returns {*}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.getTree = function () {
        var viewController = this.getViewController();
        if (viewController) {
            return viewController.controls.get(this.getName() + '_tree');
        }
        return undefined;
    };
    /**
     * 获取导航分页部件服务对象
     *
     * @returns {*}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.getExpTab = function () {
        var viewController = this.getViewController();
        if (viewController) {
            return viewController.controls.get('exptab');
        }
        return undefined;
    };
    /**
     * 获取树配置信息
     *
     * @returns {*}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.getTreeCfg = function () {
        return undefined || {};
    };
    /**
     * 获取到导航嵌入
     *
     * @returns {*}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.getExpFrame = function () {
        return undefined;
    };
    /**
     * 获取  PickupviewpanelService （选择视图面板部件服务对象）
     * 判断视图视图类型
     *
     * @returns {*}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.getPVPanel = function () {
        var viewController = this.getViewController();
        if (viewController) {
            return viewController.controls.get('pickupviewpanel');
        }
        return undefined;
    };
    /**
     * 节点选中变化
     *
     * @param {Array<any>} records
     * @returns {void}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.onTreeSelectionChange = function (records) {
        if (!records || records.length === 0) {
            return;
        }
        var record = records[0];
        this.setTreeSelect(record);
        // 替换键值
        var nodeids = record.id.split(';');
        var nodetext = record.text;
        var nodetag = record.nodetag;
        var nodetag2 = record.nodetag2;
        var nodetag3 = record.nodetag3;
        var nodetag4 = record.nodetag4;
        var controller = this.getViewController();
        if (this.getExpTab()) {
            var viewarg = { viewid: record.srfnodetype };
            var viewItem = controller.getExpItemView(viewarg);
            if (!viewItem) {
                this.fire(IBizTreeExpBar.SELECTIONCHANGE, { viewid: record.srfnodetype, viewParam: {} });
                return;
            }
            // tslint:disable-next-line:prefer-const
            var viewParam = {};
            if (viewItem.viewparam) {
                Object.assign(viewParam, viewItem.viewparam);
            }
            for (var key in viewParam) {
                if (viewParam.hasOwnProperty(key)) {
                    var value = viewParam[key];
                    if (value) {
                        value = value.replace(new RegExp('%NODETEXT%', 'g'), nodetext);
                        if (nodetag && !Object.is(nodetag, '')) {
                            value = value.replace(new RegExp('%NODETAG%', 'g'), nodetag);
                        }
                        if (nodetag2 && !Object.is(nodetag2, '')) {
                            value = value.replace(new RegExp('%NODETAG2%', 'g'), nodetag2);
                        }
                        if (nodetag3 && !Object.is(nodetag3, '')) {
                            value = value.replace(new RegExp('%NODETAG3%', 'g'), nodetag3);
                        }
                        if (nodetag4 && !Object.is(nodetag4, '')) {
                            value = value.replace(new RegExp('%NODETAG4%', 'g'), nodetag4);
                        }
                        // 进行替换
                        for (var i = 1; i < nodeids.length; i++) {
                            value = value.replace(new RegExp('%NODEID' + ((i === 1) ? '' : i.toString()) + '%', 'g'), nodeids[i]);
                        }
                        viewParam[key] = value;
                    }
                }
            }
            this.fire(IBizTreeExpBar.SELECTIONCHANGE, { viewItem: viewItem, viewParam: viewParam });
            return;
        }
        if (this.getPVPanel()) {
            // tslint:disable-next-line:prefer-const
            var viewarg = { nodetype: record.srfnodetype };
            // tslint:disable-next-line:prefer-const
            var viewParam = controller.getNavViewParam(viewarg);
            if (!viewParam) {
                return;
            }
            for (var key in viewParam) {
                if (viewParam.hasOwnProperty(key)) {
                    var value = viewParam[key];
                    if (value) {
                        value = value.replace(new RegExp('%NODETEXT%', 'g'), nodetext);
                        if (nodetag && !Object.is(nodetag, '')) {
                            value = value.replace(new RegExp('%NODETAG%', 'g'), nodetag);
                        }
                        if (nodetag2 && !Object.is(nodetag2, '')) {
                            value = value.replace(new RegExp('%NODETAG2%', 'g'), nodetag2);
                        }
                        if (nodetag3 && !Object.is(nodetag3, '')) {
                            value = value.replace(new RegExp('%NODETAG3%', 'g'), nodetag3);
                        }
                        if (nodetag4 && !Object.is(nodetag4, '')) {
                            value = value.replace(new RegExp('%NODETAG4%', 'g'), nodetag4);
                        }
                        // 进行替换
                        for (var i = 1; i < nodeids.length; i++) {
                            value = value.replace(new RegExp('%NODEID' + ((i === 1) ? '' : i.toString()) + '%', 'g'), nodeids[i]);
                        }
                        viewParam[key] = value;
                    }
                }
            }
            this.getPVPanel().setParentData(viewParam);
            this.getPVPanel().refreshViewPanel();
            // this.fire(IBizEvent.IBizTreeExpBar_SELECTIONCHANGE, { viewid: record.srfnodetype, viewParam: viewParam });
            return;
        }
        if (this.getExpFrame()) {
            // var viewarg = { viewid: tag.srfnodetype };
            // var viewItem = controller.getExpItemView(viewarg);
            // if (viewItem == null)
            //     return;
            // var viewParam = {};
            // if (viewItem.viewparam) {
            //     $.extend(viewParam, viewItem.viewparam);
            // }
            // for (var key in viewParam) {
            //     var value = viewParam[key];
            //     if (value) {
            //         value = value.replace(new RegExp('%NODETEXT%', 'g'), nodetext);
            //         //进行替换
            //         for (var i = 1; i < nodeids.length; i++) {
            //             value = value.replace(new RegExp('%NODEID' + ((i == 1) ? '' : i.toString()) + '%', 'g'), nodeids[i]);
            //         }
            //         viewParam[key] = value;
            //     }
            // }
            // var url = $.getIBizApp().parseURL(BASEURL, viewItem.viewurl, {});
            // url += '&' + $.param({ 'srfifchild': true, 'srfparentdata': JSON.stringify(viewParam) });
            // this.getExpFrame().attr('src', url);
            // return;
        }
    };
    /**
     * 树内容菜单
     *
     * @param {Array<any>} nodes
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.onTreeContextMenu = function (nodes) {
        this.node = {};
        if (nodes.length > 0) {
            Object.assign(this.node, nodes[0]);
            this.onTreeSelectionChange([this.node]);
        }
    };
    /**
     * 设置树选中数据
     *
     * @private
     * @param {*} [item={}]
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.setTreeSelect = function (item) {
        if (item === void 0) { item = {}; }
        var viewController = this.getViewController();
        if (viewController) {
            var tree = viewController.controls.get(this.getName() + '_tree');
            this.tree = tree;
            if (this.tree) {
                this.tree.setSelectTreeItem(item);
            }
        }
    };
    /**
     * 获取计数器名称，在发布器中重写
     *
     * @returns {string}
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.getUICounterName = function () {
        return undefined;
    };
    /**
     * 设置树导航选中项
     *
     * @param {*} [_item={}]
     * @memberof IBizTreeExpBar
     */
    IBizTreeExpBar.prototype.setSelectItem = function (_item) {
        if (_item === void 0) { _item = {}; }
        if (_item) {
            this.selectItem = {};
            Object.assign(this.selectItem, _item);
        }
    };
    IBizTreeExpBar.SELECTIONCHANGE = 'SELECTIONCHANGE';
    IBizTreeExpBar.LOADED = 'LOADED';
    return IBizTreeExpBar;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流树导航部件
 *
 * @class IBizWFExpBar
 * @extends {IBizControl}
 */
var IBizWFExpBar = /** @class */ (function (_super) {
    __extends(IBizWFExpBar, _super);
    /**
     * Creates an instance of IBizWFExpBar.
     * 创建 IBizWFExpBar 实例
     *
     * @param {*} [otps={}]
     * @memberof IBizWFExpBar
     */
    function IBizWFExpBar(otps) {
        if (otps === void 0) { otps = {}; }
        var _this = _super.call(this, otps) || this;
        /**
         * 展开数据项
         *
         * @type {Array<string>}
         * @memberof IBizWFExpBar
         */
        _this.expandItems = [];
        /**
         * 导航树部件是否收缩，默认展开
         *
         * @type {boolean}
         * @memberof IBizWFExpBar
         */
        _this.isCollapsed = true;
        /**
         * 导航菜单数据项
         *
         * @type {Array<any>}
         * @memberof IBizWFExpBarService
         */
        _this.items = [];
        /**
         * 选中菜单项
         *
         * @type {*}
         * @memberof IBizWFExpBarService
         */
        _this.selectItem = {};
        /**
         * 计数器
         *
         * @type {IBizUICounterService}
         * @memberof IBizWFExpBarService
         */
        _this.UICounter = null;
        if (_this.getViewController()) {
            var viewController_1 = _this.getViewController();
            viewController_1.on(IBizViewController.INITED).subscribe(function () {
                _this.UICounter = viewController_1.uicounters.get(_this.getUICounterName());
                _this.onCounterChanged(_this.items);
                _this.UICounter.on(IBizUICounter.COUNTERCHANGED).subscribe(function (data) {
                    _this.onCounterChanged(_this.items);
                });
            });
        }
        return _this;
    }
    /**
     * 加载导航树数据
     *
     * @param {*} _opt
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.load = function (_opt) {
        var _this = this;
        var opts = {};
        Object.assign(opts, _opt);
        Object.assign(opts, { srfaction: 'fetch', srfctrlid: this.getName() });
        this.iBizHttp.post(this.getBackendUrl(), opts).subscribe(function (result) {
            if (result.ret === 0) {
                // this.items = result.items;
                _this.onCounterChanged(result.items);
                _this.formarItems(result.items);
                _this.items = result.items.slice();
                _this.fire(IBizWFExpBar.LOADED, _this.items[0]);
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 格式化数据项
     *
     * @private
     * @param {*} _items
     * @returns {*}
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.formarItems = function (_items) {
        var _this = this;
        _items.forEach(function (item) {
            item.disabled = false;
            item.class = 'wfbadge';
            if (item.items) {
                _this.expandItems.push(item.id);
                _this.formarItems(item.items);
            }
        });
    };
    /**
     * 菜单项选中处理
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.selection = function (item) {
        if (item === void 0) { item = {}; }
        if (item.items && item.items.length > 0) {
            return;
        }
        if (Object.is(item.id, this.selectItem.id)) {
            return;
        }
        this.selectItem = {};
        Object.assign(this.selectItem, item);
        this.fire(IBizWFExpBar.SELECTIONCHANGE, this.selectItem);
    };
    /**
     * 菜单节点选中处理
     *
     * @param {*} [item={}]
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.expandedAndSelectSubMenu = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.is(item.id, this.selectItem.id)) {
            return;
        }
        this.selectItem = {};
        Object.assign(this.selectItem, item);
        this.fire(IBizWFExpBar.SELECTIONCHANGE, this.selectItem);
    };
    /**
     * 获取计数器名称
     * 在发布器中重写
     *
     * @returns {string}
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.getUICounterName = function () {
        return undefined;
    };
    /**
     * 设置选中项
     *
     * @param {*} [item={}]
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.setSelectItem = function (item) {
        if (item === void 0) { item = {}; }
        if (item && !Object.is(item.id, this.selectItem.id)) {
            this.selectItem = {};
            Object.assign(this.selectItem, item);
        }
    };
    /**
     * 计数器值变化
     *
     * @private
     * @returns {void}
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.onCounterChanged = function (items) {
        if (!this.UICounter) {
            return;
        }
        var data = this.UICounter.getData();
        if (!data) {
            return;
        }
        var bNeedReSelect = this.itemSelect(items, data);
        if (bNeedReSelect) {
            this.selectItem = {};
            Object.assign(this.selectItem, this.items[0]);
            this.fire(IBizWFExpBar.SELECTIONCHANGE, this.selectItem);
        }
    };
    /**
     * 选中项
     *
     * @private
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {boolean}
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.itemSelect = function (items, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var bNeedReSelect = false;
        items.forEach(function (item) {
            var counterid = item.counterid;
            var countermode = item.countermode;
            item.show = true;
            var count = data[counterid];
            if (!count) {
                count = 0;
            }
            if (count === 0 && countermode && countermode === 1) {
                item.show = false;
                // 判断是否选中列，如果是则重置选中
                if (_this.selectItem && Object.is(_this.selectItem.id, item.id)) {
                    bNeedReSelect = true;
                }
            }
            item.counterdata = count;
            if (item.items) {
                bNeedReSelect = _this.itemSelect(item.items, data);
            }
        });
        return bNeedReSelect;
    };
    /**
     * 获取数据项
     *
     * @returns {Array<any>}
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.prototype.getItems = function () {
        return this.items;
    };
    /*****************事件声明************************/
    /**
     * 选择变化
     *
     * @static
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.SELECTIONCHANGE = "SELECTIONCHANGE";
    /**
     * 加载完成
     *
     * @static
     * @memberof IBizWFExpBar
     */
    IBizWFExpBar.LOADED = 'LOADED';
    return IBizWFExpBar;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多项选择结果集合控件服务对象
 *
 * @class IBizMPickupResult
 * @extends {IBizControl}
 */
var IBizMPickupResult = /** @class */ (function (_super) {
    __extends(IBizMPickupResult, _super);
    /**
     * Creates an instance of IBizMPickupResult.
     * 创建 IBizMPickupResult 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMPickupResult
     */
    function IBizMPickupResult(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 按钮文本--数据选中
         *
         * @type {string}
         * @memberof IBizMPickupResult
         */
        _this.onRightText = '选中';
        /**
         * 按钮文本--取消选中
         *
         * @type {string}
         * @memberof IBizMPickupResult
         */
        _this.onLeftText = '取消';
        /**
         * 按钮文本--全部选中
         *
         * @type {string}
         * @memberof IBizMPickupResult
         */
        _this.onAllRightText = '全部选中';
        /**
         * 按钮文本--取消全部选中
         *
         * @type {string}
         * @memberof IBizMPickupResult
         */
        _this.onAllLeftText = '全部取消';
        /**
         * 当前结果数据中选中数据
         *
         * @type {Array<any>}
         * @memberof IBizMPickupResult
         */
        _this.resSelecttions = [];
        /**
         * 多项数据结果集中所有数据
         *
         * @type {Array<any>}
         * @memberof IBizMPickupResult
         */
        _this.selections = [];
        /**
         * 当前表格选中数据
         *
         * @type {Array<any>}
         * @memberof IBizMPickupResult
         */
        _this.curSelecttions = [];
        /**
         * 当前表格所有数据
         *
         * @private
         * @type {Array<any>}
         * @memberof IBizMPickupResult
         */
        _this.allData = [];
        return _this;
    }
    /**
     * 结果集数据选中
     *
     * @param {*} [item={}]
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.resultSelect = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.keys(item).length === 0) {
            return;
        }
        var index = this.resSelecttions.findIndex(function (select) { return Object.is(item.srfkey, select.srfkey); });
        if (index === -1) {
            this.resSelecttions.push(item);
            item.select = true;
        }
        else {
            this.resSelecttions.splice(index, 1);
            item.select = false;
        }
    };
    /**
     * 结果数据选中激活
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.dataActivated = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.keys(item).length === 0) {
            return;
        }
        var index = this.selections.findIndex(function (select) { return Object.is(item.srfkey, select.srfkey); });
        this.selections.splice(index, 1);
        var _index = this.resSelecttions.findIndex(function (select) { return Object.is(item.srfkey, select.srfkey); });
        if (_index !== -1) {
            this.resSelecttions.splice(_index, 1);
        }
        item.select = false;
    };
    /**
     * 移除结果数据中已选中数据
     *
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.onLeftClick = function () {
        var _this = this;
        this.resSelecttions.forEach(function (item) {
            var index = _this.selections.findIndex(function (select) { return Object.is(item.srfkey, select.srfkey); });
            if (index !== -1) {
                _this.selections.splice(index, 1);
            }
        });
        this.resSelecttions = [];
    };
    /**
     * 添加表格选中数据至结果数据中
     *
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.onRightClick = function () {
        var _this = this;
        this.curSelecttions.forEach(function (item) {
            var index = _this.selections.findIndex(function (select) { return Object.is(item.srfkey, select.srfkey); });
            if (index === -1) {
                item.select = false;
                _this.selections.push(item);
            }
        });
    };
    /**
     * 将所有表格数据添加到结果数据中
     *
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.onRightAllClick = function () {
        var _this = this;
        this.allData.forEach(function (item) {
            var index = _this.selections.findIndex(function (select) { return Object.is(item.srfkey, select.srfkey); });
            if (index === -1) {
                item.select = false;
                _this.selections.push(item);
            }
        });
    };
    /**
     * 移除所有结果数据
     *
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.onLeftAllClick = function () {
        this.selections = [];
        this.resSelecttions = [];
    };
    /**
     * 获取选中值
     *
     * @returns {Array<any>}
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.getSelections = function () {
        var sele = [];
        sele = this.selections.slice();
        return sele;
    };
    /**
     * 添加结果数据中的选中数据
     *
     * @param {Array<any>} items
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.appendDatas = function (items) {
        var _this = this;
        items.forEach(function (item) {
            var index = _this.selections.findIndex(function (data) { return Object.is(data.srfkey, item.srfkey); });
            if (index === -1) {
                item.select = false;
                _this.selections.push(item);
            }
        });
    };
    /**
     * 设置设置当前选中数据
     *
     * @param {Array<any>} data
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.setCurSelections = function (data) {
        this.curSelecttions = [];
        this.curSelecttions = data.slice();
    };
    /**
     * 设置当前表格所有数据
     *
     * @param {Array<any>} data
     * @memberof IBizMPickupResult
     */
    IBizMPickupResult.prototype.setAllData = function (data) {
        this.curSelecttions = [];
        this.allData = [];
        this.allData = data.slice();
    };
    return IBizMPickupResult;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 图表
 *
 * @class IBizChart
 * @extends {IBizControl}
 */
var IBizChart = /** @class */ (function (_super) {
    __extends(IBizChart, _super);
    /**
     * Creates an instance of IBizChart.
     * 创建 IBizChart 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizChart
     */
    function IBizChart(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 图表数据
         *
         * @type {Array<any>}
         * @memberof IBizChart
         */
        _this.data = [];
        return _this;
    }
    /**
     * 加载报表数据
     *
     * @memberof IBizChart
     */
    IBizChart.prototype.load = function () {
        this.buildChart();
    };
    /**
     * 处理图表内容
     *
     * @memberof IBizChart
     */
    IBizChart.prototype.buildChart = function () {
        var _this = this;
        var params = {};
        this.fire(IBizChart.BEFORELOAD, params);
        Object.assign(params, { srfrender: 'ECHARTS3', srfaction: 'FETCH', srfctrlid: this.getName() });
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (result) {
            if (result.ret === 0) {
                _this.data = result.data;
                var data = _this.getChartConfig();
                var target = {};
                Object.assign(target, data, _this.data);
                _this.fire(IBizChart.LOADED, target);
            }
            else {
                _this.iBizNotification.error('系统异常', result.info);
            }
        }, function (error) {
            _this.iBizNotification.error('系统异常', error.info);
        });
    };
    /**
     * 获取图表基础配置数据
     */
    IBizChart.prototype.getChartConfig = function () {
        var opts = {};
        return opts;
    };
    /**
     * 加载之前
     *
     * @static
     * @memberof IBizChart
     */
    IBizChart.BEFORELOAD = 'BEFORELOAD';
    /**
     * 加载完成
     *
     * @static
     * @memberof IBizChart
     */
    IBizChart.LOADED = 'LOADED';
    /**
     * 点击
     *
     * @static
     * @memberof IBizChart
     */
    IBizChart.DBLCLICK = 'DBLCLICK';
    return IBizChart;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据关系栏
 *
 * @class IBizDRBar
 * @extends {IBizControl}
 */
var IBizDRBar = /** @class */ (function (_super) {
    __extends(IBizDRBar, _super);
    /**
     * Creates an instance of IBizDRBar.
     * 创建IBizDRBar实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDRBar
     */
    function IBizDRBar(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 展开数据项
         *
         * @type {Array<string>}
         * @memberof IBizDRBar
         */
        _this.expandItems = [];
        /**
         * 关系部件是否收缩，默认展开
         *
         * @type {boolean}
         * @memberof IBizDRBar
         */
        _this.isCollapsed = true;
        /**
         * 关系数据项
         *
         * @type {Array<any>}
         * @memberof IBizDRBar
         */
        _this.items = [];
        /**
         * 选中项
         *
         * @type {*}
         * @memberof IBizDRBar
         */
        _this.selectItem = {};
        /**
         * 父数据对象
         *
         * @type {*}
         * @memberof IBizDRBar
         */
        _this.srfParentData = {};
        return _this;
    }
    /**
     * 加载关系数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var param = { srfaction: 'fetch', srfctrlid: this.getName() };
        Object.assign(param, arg);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (result) {
            if (result.ret === 0) {
                _this.formarItems(result.items);
                _this.itemSelect(result.items, {});
                _this.items = result.items.slice();
                _this.fire(IBizDRBar.DRBARLOADED, _this.items);
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 选中项
     *
     * @private
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {boolean}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.itemSelect = function (items, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var bNeedReSelect = false;
        items.forEach(function (item) {
            var counterid = item.counterid;
            var countermode = item.countermode;
            item.show = true;
            var count = data[counterid];
            if (!count) {
                count = 0;
            }
            if (count === 0 && countermode && countermode === 1) {
                item.show = false;
                // 判断是否选中列，如果是则重置选中
                if (_this.selectItem && Object.is(_this.selectItem.id, item.id)) {
                    bNeedReSelect = true;
                }
            }
            item.counterdata = count;
            if (item.items) {
                bNeedReSelect = _this.itemSelect(item.items, data);
            }
        });
        return bNeedReSelect;
    };
    /**
     * 格式化数据项
     *
     * @private
     * @param {*} _items
     * @returns {*}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.formarItems = function (_items) {
        var _this = this;
        _items.forEach(function (item) {
            item.disabled = true;
            item.class = 'drbadge';
            if (item.items) {
                _this.expandItems.push(item.id);
                _this.formarItems(item.items);
            }
        });
    };
    /**
     * 菜单节点选中处理
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.expandedAndSelectSubMenu = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.is(item.id, 'form')) {
            this.setSelectItem(item);
            this.fire(IBizDRBar.DRBARSELECTCHANGE, item);
            return;
        }
        var viewController = this.getViewController();
        var refitem = viewController.getDRItemView(item.dritem);
        if (!refitem || Object.keys(refitem).length == 0) {
            return;
        }
        var view = { viewparam: {} };
        if (item.dritem.viewparam) {
            Object.assign(view.viewparam, item.dritem.viewparam);
        }
        Object.assign(view.viewparam, this.srfParentData);
        Object.assign(view, refitem, item);
        this.fire(IBizDRBar.DRBARSELECTCHANGE, view);
    };
    /**
     * 菜单项选中事件
     *
     * @param {*} item
     * @returns {void}
     * @memberof IBizTreeExpBarService
     */
    IBizDRBar.prototype.selection = function (item) {
        if (item.items && item.items.length > 0) {
            return;
        }
        if (Object.is(item.id, 'form')) {
            this.fire(IBizDRBar.DRBARSELECTCHANGE, item);
            return;
        }
        var viewController = this.getViewController();
        var refitem = viewController.getDRItemView(item.dritem);
        if (!refitem && Object.keys(refitem).length == 0) {
            return;
        }
        var view = { viewparam: {} };
        if (item.dritem.viewparam) {
            Object.assign(view.viewparam, item.dritem.viewparam);
        }
        Object.assign(view.viewparam, this.srfParentData);
        Object.assign(view, refitem, item);
        this.fire(IBizDRBar.DRBARSELECTCHANGE, view);
    };
    /**
     * 重新加载关系数据
     *
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.reLoad = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.load(arg);
    };
    /**
     * 获取所有关系数据项
     *
     * @returns {Array<any>}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.getItems = function () {
        return this.items;
    };
    /**
     * 获取关系数据项
     *
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {*}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.getItem = function (items, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var _item = {};
        var viewController = this.getViewController();
        items.some(function (item) {
            var drItem = viewController.getDRItemView({ viewid: item.id });
            if (drItem && Object.is(drItem.routepath, data.routepath)) {
                Object.assign(_item, drItem, item);
                return true;
            }
            if (item.items && item.items.length > 0) {
                var subItem = _this.getItem(item.items, data);
                if (subItem && Object.keys(subItem).length > 0) {
                    Object.assign(_item, subItem);
                    return;
                }
            }
        });
        return _item;
    };
    /**
     * 设置关系数据选中项
     *
     * @param {*} [_item={}]
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.setSelectItem = function (_item) {
        if (_item === void 0) { _item = {}; }
        if (!Object.is(this.selectItem.id, _item.id)) {
            this.selectItem = {};
            Object.assign(this.selectItem, _item);
        }
    };
    /**
     * 设置关系数据项状态
     *
     * @param {Array<any>} items
     * @param {boolean} state
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.setDRBarItemState = function (items, state) {
        var _this = this;
        items.forEach(function (item) {
            item.disabled = Object.is(item.id, 'form') ? false : state;
            if (item.items && item.items.length > 0) {
                _this.setDRBarItemState(item.items, state);
            }
        });
    };
    /**
     * 设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.setParentData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentData = {};
        Object.assign(this.srfParentData, data);
    };
    /**
     * 数据关系项加载完成
     *
     * @static
     * @memberof IBizDRBar
     */
    IBizDRBar.DRBARLOADED = 'DRBARLOADED';
    /**
     * 数据关系项选中
     *
     * @static
     * @memberof IBizDRBar
     */
    IBizDRBar.DRBARSELECTCHANGE = 'DRBARSELECTCHANGE';
    return IBizDRBar;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 自定义部件
 *
 * @class IBizCustom
 * @extends {IBizControl}
 */
var IBizCustom = /** @class */ (function (_super) {
    __extends(IBizCustom, _super);
    /**
     * Creates an instance of IBizCustom.
     * 创建 IBizCustom 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizCustom
     */
    function IBizCustom(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizCustom;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据看板
 *
 * @class IBizDashboard
 * @extends {IBizControl}
 */
var IBizDashboard = /** @class */ (function (_super) {
    __extends(IBizDashboard, _super);
    /**
     * Creates an instance of IBizDashboard.
     * 创建 IBizDashboard 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDashboard
     */
    function IBizDashboard(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizDashboard;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 报表
 *
 * @class IBizReportPanel
 * @extends {IBizControl}
 */
var IBizReportPanel = /** @class */ (function (_super) {
    __extends(IBizReportPanel, _super);
    /**
     * Creates an instance of IBizReportPanel.
     * 创建 IBizReportPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizReportPanel
     */
    function IBizReportPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否显示报表，本地开发不显示报表
         *
         * @type {boolean}
         * @memberof IBizReportPanel
         */
        _this.showReport = false;
        /**
         * 报表ID
         *
         * @type {string}
         * @memberof IBizReportPanel
         */
        _this.reportid = '';
        return _this;
    }
    /**
     * 数据加载
     *
     * @memberof IBizReportPanel
     */
    IBizReportPanel.prototype.load = function () {
        this.buildReport();
    };
    /**
     * 处理报表内容
     *
     * @memberof IBizReportPanel
     */
    IBizReportPanel.prototype.buildReport = function () {
        var params = {};
        this.fire(IBizReportPanel.BEFORELOAD, params);
        if (params.srfaction) {
            delete params.srfaction;
        }
        if (Object.is(this.reportid, '')) {
            return;
        }
        Object.assign(params, { srfreportid: this.reportid });
        if (!IBizEnvironment.LocalDeve) {
            if (Object.keys(params).length !== 0) {
                this.showReport = true;
                var _data_1 = [];
                var _paramsArr = Object.keys(params);
                _paramsArr.forEach(function (key) {
                    _data_1.push("{key}={params[key]}");
                });
                var urlData = _data_1.join('&');
                this.viewurl = "..{IBizEnvironment.PDFReport}?{urlData}";
            }
        }
    };
    /**
     * 加载之前
     *
     * @static
     * @memberof IBizReportPanel
     */
    IBizReportPanel.BEFORELOAD = 'BEFORELOAD';
    return IBizReportPanel;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 向导面板
 *
 * @class IBizWizardPanel
 * @extends {IBizControl}
 */
var IBizWizardPanel = /** @class */ (function (_super) {
    __extends(IBizWizardPanel, _super);
    /**
     * Creates an instance of IBizWizardPanel.
     * 创建 IBizWizardPanel 实例
     *
     * @param {*} [otps={}]
     * @memberof IBizWizardPanel
     */
    function IBizWizardPanel(opt) {
        if (opt === void 0) { opt = {}; }
        var _this = _super.call(this, opt) || this;
        /**
         * 向导表单集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.forms = {};
        /**
         * 向导表单名称集合
         *
         * @type {Array<string>}
         * @memberof IBizWizardPanel
         */
        _this.formNames = [];
        /**
         * 向导表单行为集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.acitons = {};
        /**
         * 当前激活表单名称
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.curForm = {};
        /**
         * 当前激活表单名称
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.curFormName = null;
        /**
         * 向导面板参数
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.params = {};
        /**
         * 执行过的表单集合
         *
         * @type {Array<String>}
         * @memberof IBizWizardPanel
         */
        _this.prevForms = [];
        /**
         * 步骤集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.steps = [];
        /**
         * 当前步骤
         *
         * @type {String}
         * @memberof IBizWizardPanel
         */
        _this.curStep = '';
        /**
         * 表单步骤集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.formSteps = {};
        /**
         * 下一步后续操作行为
         *
         * @type {String}
         * @memberof IBizWizardPanel
         */
        _this.afterformsaveaction = '';
        _this.regWizardForms();
        _this.regFormActions();
        _this.regSteps();
        _this.regFormSteps();
        _this.setDefFrom();
        return _this;
    }
    /**
     * 注册向导表单
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regWizardForms = function () {
    };
    /**
     * 注册表单面板行为
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regFormActions = function () {
    };
    /**
 * 注册向导表单项
 *
 * @param {*} form
 * @memberof IBizWizardPanel
 */
    IBizWizardPanel.prototype.regForm = function (form) {
        var _this = this;
        if (form) {
            // 表单保存之前
            form.on(IBizEditForm.FORMBEFORESAVE).subscribe(function (data) {
                // this.onFormBeforeSaved(data);
            });
            // 表单保存完成
            form.on(IBizForm.FORMSAVED).subscribe(function (data) {
                _this.onFormSaved(data);
            });
            // 表单加载完成
            form.on(IBizForm.FORMLOADED).subscribe(function (data) {
                _this.fire(IBizWizardPanel.WIZARDFORMLOADED, data);
            });
            // 表单属性值变化
            form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                if (data) {
                    Object.assign(data, { formName: form.getName() });
                    _this.fire(IBizWizardPanel.WIZARDFORMFIELDCHANGED, data);
                }
                else {
                    _this.fire(IBizWizardPanel.WIZARDFORMFIELDCHANGED, { formName: form.getName(), name: '' });
                }
            });
            this.formNames.push(form.getName());
            this.forms[form.getName()] = form;
        }
    };
    /**
     * 注册表单行为项
     *
     * @param {*} name
     * @param {*} action
     * @returns {void}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regAction = function (name, action) {
        if (name) {
            this.acitons[name] = action;
        }
    };
    /**
     * 注册步骤
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regSteps = function () {
    };
    /**
     * 注册表单步骤集合
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regFormSteps = function () {
    };
    /**
     * 注册表单步骤项
     *
     * @param {*} name
     * @param {*} item
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regFormStep = function (name, item) {
        if (name) {
            this.formSteps[name] = item;
        }
    };
    /**
     * 设置默认表单
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.setDefFrom = function () {
    };
    /**
     * 获取表单
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getForm = function (name) {
        return this.forms[name];
    };
    /**
     * 获取表单对应步骤
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getStep = function (name) {
        if (name) {
            return this.formSteps[name];
        }
    };
    /**
     * 获取步骤对应下标
     *
     * @param {*} tag
     * @returns {Number}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getStepIndex = function (tag) {
        if (tag) {
            var index = this.steps.findIndex(function (step) { return Object.is(step.tag, tag); });
            if (index >= 0) {
                return index;
            }
            else if (Object.is(tag, 'finish')) {
                return this.steps.length;
            }
        }
        return 0;
    };
    /**
     * 向导面板加载
     *
     * @param {*} arg
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.autoLoad = function (arg) {
        var _this = this;
        var param = { srfaction: 'init', srfctrlid: this.getName() };
        Object.assign(param, arg);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (data) {
            if (data.ret === 0) {
                _this.params = data.data;
                if (_this.curForm) {
                    _this.curForm.autoLoad(_this.params);
                }
                _this.fire(IBizWizardPanel.WIZARDPANEL_INITED, data);
            }
            else {
            }
        }, function (data) {
            console.log(data);
        });
    };
    /**
     * 表单保存完成
     *
     * @param {*} data
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.onFormSaved = function (data) {
        var field = this.curForm.findField("srfnextform");
        if (Object.is(this.afterformsaveaction, 'NEXT') && field && !Object.is(field.getValue(), '')) {
            this.changeForm(field.getValue());
        }
        else if (Object.is(this.afterformsaveaction, 'NEXT') && this.getNext()) {
            this.changeForm(this.getNext());
        }
        else {
            this.curFormName = "finish";
            this.curForm = null;
            this.curStep = 'finish';
            this.finish();
        }
        this.fire(IBizWizardPanel.WIZARDFORMSAVED, data);
    };
    /**
     * 执行上一步
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.goPrev = function () {
        var length = this.prevForms.length;
        if (length > 0) {
            var name_1 = this.prevForms[length - 1];
            this.changeForm(name_1);
            this.prevForms.splice(length - 1, 1);
        }
    };
    /**
     * 执行下一步
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.goNext = function () {
        if (this.curForm) {
            this.afterformsaveaction = 'NEXT';
            this.prevForms.push(this.curFormName);
            this.curForm.save2();
        }
    };
    /**
     * 执行完成
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.goFinish = function () {
        if (this.curForm) {
            this.afterformsaveaction = 'FINISH';
            this.curForm.save2();
        }
    };
    /**
     * 切换表单
     *
     * @param {*} name
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.changeForm = function (name) {
        if (name && this.getForm(name)) {
            this.curFormName = name;
            this.curForm = this.getForm(name);
            this.curStep = this.getStep(name);
            this.curForm.autoLoad(this.params);
            this.fire(IBizWizardPanel.WIZARDFORMCHANGED, this);
        }
    };
    /**
     * 获取下一个表单名称
     *
     * @returns {String}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getNext = function () {
        var index = this.formNames.indexOf(this.curFormName);
        if (index >= 0) {
            if (this.formNames[index + 1]) {
                return this.formNames[index + 1];
            }
        }
        return undefined;
    };
    /**
     * 完成
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.finish = function () {
        var _this = this;
        var param = { srfaction: "finish", srfctrlid: this.getName() };
        Object.assign(param, this.params);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (data) {
            if (data.ret === 0) {
                _this.fire(IBizWizardPanel.WIZARDPANEL_FINISH, data);
            }
        }, function (data) {
            console.log(data);
        });
    };
    /**
     * 是否显示该行为按钮
     *
     * @param {*} form
     * @param {*} action
     * @returns
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.isVisible = function (name, action) {
        if (name) {
            var formActions = this.acitons[name];
            if (formActions) {
                var index = formActions.findIndex(function (item) { return Object.is(item, action); });
                if (index >= 0) {
                    return true;
                }
            }
        }
        return false;
    };
    /**
     * 向导表单项值改变
     */
    IBizWizardPanel.WIZARDFORMFIELDCHANGED = 'WIZARDFORMFIELDCHANGE';
    /**
     * 向导表单保存完成
     */
    IBizWizardPanel.WIZARDFORMSAVED = 'WIZARDFORMSAVED';
    /**
     * 向导表单加载完成
     */
    IBizWizardPanel.WIZARDFORMLOADED = 'WIZARDFORMLOADED';
    /**
     * 向导面板初始化完成
     */
    IBizWizardPanel.WIZARDPANEL_INITED = 'WIZARDPANEL_INITED';
    /**
     * 向导面板完成
     */
    IBizWizardPanel.WIZARDPANEL_FINISH = 'WIZARDPANEL_FINISH';
    /**
     * 向导表单切换
     */
    IBizWizardPanel.WIZARDFORMCHANGED = 'WIZARDFORMCHANGED';
    return IBizWizardPanel;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多编辑视图面板部件服务对象
 *
 * @class IBizMultiEditViewPanel
 * @extends {IBizControl}
 */
var IBizMultiEditViewPanel = /** @class */ (function (_super) {
    __extends(IBizMultiEditViewPanel, _super);
    /**
     * Creates an instance of IBizMultiEditViewPanel.
     * 创建 IBizMultiEditViewPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMultiEditViewPanel
     */
    function IBizMultiEditViewPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 编辑数据集合
         *
         * @type {Array<any>}
         * @memberof IBizMultiEditViewPanel
         */
        _this.editCtrls = [];
        /**
         * 当前激活tab
         *
         * @type {Number}
         * @memberof IBizMultiEditViewPanel
         */
        _this.activeTab = 0;
        /**
         * 待删除主键集合
         *
         * @type {Array<String>}
         * @memberof IBizMultiEditViewPanel
         */
        _this.removeKeys = [];
        /**
         * 提交数量
         *
         * @memberof IBizMultiEditViewPanel
         */
        _this.submitCount = 0;
        return _this;
    }
    /**
     * 数据记载
     *
     * @param {*} [params={}]
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.load = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        var opt = { srfctrlid: this.getName(), srfaction: 'fetch' };
        if (params) {
            Object.assign(opt, params);
        }
        this.fire(IBizMDControl.BEFORELOAD, opt);
        Object.assign(opt, { start: 0, limit: 500 });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (success) {
            if (success.ret == 0) {
                success.items.forEach(function (item) {
                    item.saveRefView = 0;
                    item.isDirty = false;
                });
                _this.editCtrls = success.items;
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 刷新加载数据
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.refresh = function () {
        this.load();
    };
    /**
     * 添加编辑页
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.addEditView = function () {
        this.editCtrls.push({ srfmajortext: 'New', isDirty: true, saveRefView: 0 });
        this.activeTab = this.editCtrls.length - 1;
        this.onDataChanged();
    };
    /**
     * 表单保存完成后回调
     *
     * @param {*} [data={}]
     * @param {*} [item={}]
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onEditFormSaved = function (data, item) {
        if (data === void 0) { data = {}; }
        if (item === void 0) { item = {}; }
        Object.assign(item, data);
        item.isDirty = false;
        item.saveRefView = 0;
        if (this.submitCount > 0) {
            this.submitCount--;
            if (this.submitCount === 0) {
                if (this.removeKeys.length > 0) {
                    this.doRemove();
                }
                else {
                    this.fire(IBizMultiEditViewPanel.DATASAVED, this);
                }
            }
        }
        else {
            this.onDataChanged();
        }
    };
    IBizMultiEditViewPanel.prototype.onBeforeRemove = function () {
        var _this = this;
        return function (num) {
            var item = _this.editCtrls[num];
            if (item) {
                if (item.srfkey && _this.removeKeys.indexOf(item.srfkey) < 0) {
                    _this.removeKeys.push(item.srfkey);
                }
                _this.editCtrls.splice(num, 1);
            }
            if (num == _this.activeTab) {
                if (num >= _this.editCtrls.length) {
                    _this.activeTab = _this.editCtrls.length - 1;
                }
            }
            _this.onDataChanged();
            return new Promise(function (resolve, reject) { });
        };
    };
    /**
     * 删除编辑数据
     *
     * @param {*} item
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onRemoveEditView = function (num) {
        var item = this.editCtrls[num];
        if (item) {
            if (item.srfkey && this.removeKeys.indexOf(item.srfkey) < 0) {
                this.removeKeys.push(item.srfkey);
            }
            this.editCtrls.splice(num, 1);
        }
        this.onDataChanged();
    };
    /**
     * 是否发生值变更
     *
     * @returns {boolean}
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.isDirty = function () {
        if (this.removeKeys.length > 0) {
            return true;
        }
        var items = this.editCtrls.filter(function (item) { return item.isDirty; });
        if (items.length > 0) {
            return true;
        }
        return false;
    };
    /**
     * 执行保存
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.doSave = function () {
        var _this = this;
        var items = this.editCtrls.filter(function (item) { return item.isDirty; });
        if (items && items.length > 0) {
            items.forEach(function (item) {
                _this.submitCount++;
                item.saveRefView++;
            });
        }
        else {
            if (this.removeKeys.length > 0) {
                this.doRemove();
            }
        }
    };
    /**
     * 删除数据
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.doRemove = function () {
        var _this = this;
        var params = { srfaction: 'remove', srfctrlid: 'meditviewpanel', 'srfkeys': this.removeKeys.join(';') };
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (success) {
            if (success.ret == 0) {
                _this.fire(IBizMultiEditViewPanel.DATASAVED, _this);
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 数据发生改变
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onDataChanged = function () {
        var isDirty = false;
        var items = this.editCtrls.filter(function (item) { return item.isDirty; });
        if ((items && items.length > 0) || this.removeKeys.length > 0) {
            isDirty = true;
        }
        this.fire(IBizMultiEditViewPanel.EDITVIEWCHANGED, isDirty);
    };
    /**
     * 表单项改变
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onEditFormFieldChanged = function (param, item) {
        if (!param) {
            return;
        }
        item[param.name] = param.value;
        item.isDirty = true;
        this.onDataChanged();
    };
    /**
     * 获取选项编辑页控制对象事件
     */
    IBizMultiEditViewPanel.FINDEDITVIEWCONTROLLER = 'FINDEDITVIEWCONTROLLER';
    /**
     * 编辑页变化
     */
    IBizMultiEditViewPanel.EDITVIEWCHANGED = 'EDITVIEWCHANGED';
    /**
     * 保存完成
     */
    IBizMultiEditViewPanel.DATASAVED = 'DATASAVED';
    return IBizMultiEditViewPanel;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 时间轴
 *
 * @class IBizTimeline
 * @extends {IBizControl}
 */
var IBizTimeline = /** @class */ (function (_super) {
    __extends(IBizTimeline, _super);
    function IBizTimeline() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * 数据集合
         *
         * @type {Array<any>}
         * @memberof IBizTimeline
         */
        _this.items = [];
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @memberof IBizTimeline
     */
    IBizTimeline.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            if (response.ret === 0) {
                _this.items = response.items;
            }
            else {
                _this.iBizNotification.error('', response.errorMessage);
            }
        }, function (error) {
            console.log(error.info);
        });
    };
    /**
     * 刷新数据
     *
     * @param {*} [arg={}]
     * @memberof IBizTimeline
     */
    IBizTimeline.prototype.refresh = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.load(arg);
    };
    return IBizTimeline;
}(IBizControl));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 视图控制器基类
 *
 * @class IBizViewControllerBase
 * @extends {IBizObject}
 */
var IBizViewControllerBase = /** @class */ (function (_super) {
    __extends(IBizViewControllerBase, _super);
    /**
     * Creates an instance of IBizViewControllerBase.
     * 创建 IBizViewControllerBase 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizViewControllerBase
     */
    function IBizViewControllerBase(opts) {
        if (opts === void 0) { opts = {}; }
        var _this_1 = _super.call(this, opts) || this;
        /**
         * 计数器
         *
         * @type {Map<string, any>}
         * @memberof IBizViewControllerBase
         */
        _this_1.uicounters = new Map();
        /**
         * 关系数据
         *
         * @type {*}
         * @memberof IBizViewControllerBase
         */
        _this_1.srfReferData = {};
        /**
         * 视图控制器父对象数据
         *
         * @type {*}implements OnInit, OnDestroy, OnChanges
         * @memberof IBizViewController
         */
        _this_1.srfParentData = {};
        /**
         * 视图控制器父对象模型
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this_1.srfParentMode = {};
        /**
         * 视图控制器是否初始化
         *
         * @type {boolean}
         * @memberof IBizViewControllerBase
         */
        _this_1.bInited = false;
        /**
         * 视图使用模式
         *
         * @private
         * @type {number}
         * @memberof IBizViewControllerBase
         */
        _this_1.viewUsage = 1;
        /**
         * 视图控制器参数
         *
         * @type {*}
         * @memberof IBizViewControllerBase
         */
        _this_1.viewParam = {};
        /**
         * vue 路由对象
         *
         * @type {*}
         * @memberof IBizViewControllerBase
         */
        _this_1.$router = null;
        /**
         * vue 实例对象
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this_1.$vue = null;
        /**
         * vue 当前路由对象
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this_1.$route = null;
        /**
         * 当前路由所在位置下标
         *
         * @type {number}
         * @memberof IBizViewController
         */
        _this_1.route_index = -1;
        /**
         * 当前路由url
         *
         * @type {string}
         * @memberof IBizViewController
         */
        _this_1.route_url = '';
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.regSRFController(_this_1);
        }
        return _this_1;
    }
    ;
    /**
    * 初始化
    * 模拟vue生命周期
    *
    * @memberof IBizViewController
    */
    IBizViewControllerBase.prototype.VueOnInit = function (vue) {
        this.beforeVueOnInit(vue);
    };
    /**
     * 视图初始化之前
     *
     * @param {*} vue
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.beforeVueOnInit = function (vue) {
        this.$route = vue.$route;
        this.$router = vue.$router;
        this.$vue = vue;
        this.setViewUsage(this.$vue.viewUsage);
    };
    /**
     * 视图组件销毁时调用
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.onDestroy = function () {
        this.unRegUICounters();
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.unRegSRFController(this);
        }
    };
    /**
     * 注销计数器
     *
     * @returns {void}
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.unRegUICounters = function () {
        var _this_1 = this;
        if (Object.keys(this.uicounters).length == 0) {
            return;
        }
        var _nameArr = Object.keys(this.uicounters);
        _nameArr.forEach(function (name) {
            var _counter = _this_1.getUICounter(name);
            if (_counter) {
                _counter.close();
            }
        });
    };
    /**
     * 获取界面计数器
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.getUICounter = function (name) {
    };
    /**
     * 设置父模式
     *
     * @param {*} [data={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setParentMode = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentMode = {};
        var _data = {};
        Object.keys(data).forEach(function (name) {
            _data[name.toLowerCase()] = data[name];
        });
        Object.assign(this.srfParentMode, _data);
    };
    /**
     *  设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setParentData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentData = {};
        var _data = {};
        Object.keys(data).forEach(function (name) {
            _data[name.toLowerCase()] = data[name];
        });
        Object.assign(this.srfParentData, _data);
        this.onSetParentData();
        this.reloadUpdatePanels();
    };
    /**
     * 刷新全部界面更新面板
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.reloadUpdatePanels = function () {
    };
    /**
     * 设置父数据
     *
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.onSetParentData = function () {
    };
    /**
     * 设置关系数据
     *
     * @param {*} [data={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setReferData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfReferData = {};
        var _data = {};
        Object.keys(data).forEach(function (name) {
            _data[name.toLowerCase()] = data[name];
        });
        Object.assign(this.srfReferData, _data);
    };
    /**
     * 解析视图参数，初始化调用
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.parseViewParams = function () {
        var _this_1 = this;
        var parsms = {};
        if (this.getViewUsage() === IBizViewController.VIEWUSAGE_DEFAULT) {
            var _parsms = {};
            var iBizApp = IBizApp.getInstance();
            if (iBizApp) {
                var _index_1 = 0;
                var views = iBizApp.viewControllers;
                views.some(function (_view) {
                    if (Object.is(_view.getId(), _this_1.getId()) && Object.is(_view.getViewUsage(), _this_1.getViewUsage())) {
                        return true;
                    }
                    if (Object.is(_view.getViewUsage(), IBizViewControllerBase.VIEWUSAGE_DEFAULT)) {
                        _index_1 = _index_1 + 1;
                    }
                });
                this.route_index = _index_1;
            }
            var route_arr = this.$route.fullPath.split('/');
            var matched = this.$route.matched;
            var cur_route_name_1 = matched[this.route_index].name;
            var cur_route_index = route_arr.findIndex(function (_name) { return Object.is(_name, cur_route_name_1); });
            var route_url_index = cur_route_index + 1;
            if (matched[this.route_index + 1]) {
                var next_route_name_1 = matched[this.route_index + 1].name;
                var next_route_index = route_arr.findIndex(function (_name) { return Object.is(_name, next_route_name_1); });
                if (cur_route_index + 2 === next_route_index) {
                    var datas = decodeURIComponent(route_arr[cur_route_index + 1]);
                    Object.assign(_parsms, IBizUtil.matrixURLToJson(datas));
                    route_url_index = route_url_index + 1;
                }
            }
            else if (route_arr[cur_route_index + 1]) {
                var datas = decodeURIComponent(route_arr[cur_route_index + 1]);
                Object.assign(_parsms, IBizUtil.matrixURLToJson(datas));
                route_url_index = route_url_index + 1;
            }
            this.route_url = route_arr.slice(0, route_url_index).join('/');
            if (Object.keys(_parsms).length > 0) {
                Object.assign(parsms, _parsms);
            }
        }
        else if (this.getViewUsage() === IBizViewController.VIEWUSAGE_MODAL) {
            Object.assign(parsms, this.$vue.params);
            if (this.$vue.srfParentMode) {
                this.setParentMode(this.$vue.srfParentMode);
            }
            if (this.$vue.srfParentData) {
                this.setParentData(this.$vue.srfParentData);
            }
            if (this.$vue.srfReferData) {
                this.setReferData(this.$vue.srfReferData);
            }
        }
        else if (this.getViewUsage() === IBizViewController.VIEWUSAGE_EMBEDED) {
            Object.assign(parsms, this.$vue.params);
            if (this.$vue.srfParentMode) {
                this.setParentMode(this.$vue.srfParentMode);
            }
            if (this.$vue.srfParentData) {
                this.setParentData(this.$vue.srfParentData);
            }
            if (this.$vue.srfReferData) {
                this.setReferData(this.$vue.srfReferData);
            }
        }
        this.addViewParam(parsms);
    };
    /**
     * 处理组件参数
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.fullComponentParam = function (param) {
        if (param === void 0) { param = {}; }
        var _data = {};
        Object.keys(param).forEach(function (name) {
            _data[name.toLowerCase()] = param[name];
        });
        Object.assign(this.viewParam, _data);
    };
    /**
     * 添加视图参数, 处理视图刷新操作
     *
     * @param {*} [param={}]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.addViewParam = function (param) {
        if (param === void 0) { param = {}; }
        var _data = {};
        Object.keys(param).forEach(function (name) {
            _data[name.toLowerCase()] = param[name];
        });
        Object.assign(this.viewParam, _data);
    };
    /**
     * 数据加载
     *
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.onLoad = function () {
    };
    /**
     * 是否初始化完毕
     *
     * @returns {boolean}
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.isInited = function () {
        return this.bInited;
    };
    /**
     * 设置视图的使用模式
     *
     * @private
     * @param {number} [viewUsage=0]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.setViewUsage = function (viewUsage) {
        if (viewUsage === void 0) { viewUsage = 0; }
        this.viewUsage = viewUsage;
    };
    /**
     * 获取视图的使用模式
     *
     * @returns {number}
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.getViewUsage = function () {
        return this.viewUsage;
    };
    /**
     * 打开数据视图,模态框打开
     *
     * @param {*} [view={}]
     * @returns {Subject<any>}
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.openModal = function (view) {
        if (view === void 0) { view = {}; }
        var subject = new rxjs.Subject();
        var _view = {};
        Object.assign(_view, JSON.parse(JSON.stringify(view)), { subject: subject });
        this.$vue.$root.addModal(_view);
        return subject;
    };
    /**
     * 关闭模态框
     *
     * @param {*} [result]
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.closeModal = function (result) {
        var _this = this;
        _this.$vue.$emit('close', result);
    };
    /**
     * 打开视图;打开方式,路由打开
     *
     * @param {string} routeString 相对路由地址
     * @param {*} [routeParam={}] 激活路由参数
     * @param {*} [queryParams] 路由全局查询参数
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.openView = function (routeString, routeParam, queryParams) {
        if (routeParam === void 0) { routeParam = {}; }
        if (this.getViewUsage() !== IBizViewController.VIEWUSAGE_DEFAULT) {
            return;
        }
        var param_arr = [];
        Object.keys(routeParam).forEach(function (name) {
            if (routeParam[name] && !Object.is(routeParam[name], '')) {
                param_arr.push(name + "=" + routeParam[name]);
            }
        });
        var url = this.route_url + "/" + routeString;
        if (param_arr.length > 0) {
            url = url + "/" + param_arr.join(';');
        }
        this.$router.push({ path: url, query: queryParams });
    };
    /**
     * 打开新窗口
     *
     * @param {string} viewurl
     * @param {*} [parsms={}]
     * @memberof IBizViewController
     */
    IBizViewControllerBase.prototype.openWindow = function (viewurl, parsms) {
        if (parsms === void 0) { parsms = {}; }
        var url = "/" + IBizEnvironment.BaseUrl + "/" + IBizEnvironment.AppName.toLowerCase() + viewurl;
        var parsms_arr = [];
        Object.keys(parsms).forEach(function (name) {
            if (parsms[name] && !Object.is(parsms[name], '')) {
                parsms_arr.push(name + "=" + parsms[name]);
            }
        });
        if (parsms_arr.length > 0) {
            url = url + "/" + parsms_arr.join(';');
        }
        var win = window;
        win.open(url, '_blank');
    };
    /**
     * 数据变化
     *
     * @param {*} [data]
     * @memberof IBizViewControllerBase
     */
    IBizViewControllerBase.prototype.dataChange = function (data) {
        var _this = this;
        _this.$vue.$emit('dataChange', data);
    };
    /**
     * 视图使用模式，默认
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewControllerBase.VIEWUSAGE_DEFAULT = 1;
    /**
     * 视图使用模式，模式弹出
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewControllerBase.VIEWUSAGE_MODAL = 2;
    /**
     * 视图使用模式，嵌入
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewControllerBase.VIEWUSAGE_EMBEDED = 4;
    return IBizViewControllerBase;
}(IBizObject));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 视图控制器
 *
 * @class IBizViewController
 * @extends {IBizViewControllerBase}
 */
var IBizViewController = /** @class */ (function (_super) {
    __extends(IBizViewController, _super);
    /**
     *Creates an instance of IBizViewController.
     * 创建 IBizViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizViewController
     */
    function IBizViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 视图模型对象
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this.model = {};
        /**
         * 模态框打开视图注入参数
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this.modalViewParam = {};
        /**
         * 模态框打开视图注入视图层级参数
         *
         * @memberof IBizViewController
         */
        _this.modalZIndex = 300;
        /**
         * 暂时废弃
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this.itemMap = {};
        /**
         * 视图控制器代码表
         *
         * @type {*}
         * @memberof IBizViewController
         */
        _this.codelists = {};
        /**
         * 部件控制器
         *
         * @type {Map<string, any>}
         * @memberof IBizViewController
         */
        _this.controls = new Map();
        /**
         * 视图模型
         *
         * @private
         * @type {*}
         * @memberof IBizViewController
         */
        _this.viewModal = {};
        /**
         * 实体界面行为
         *
         * @type {Map<string, any>}
         * @memberof IBizViewController
         */
        _this.uiactions = new Map();
        /**
         * 视图控制器url
         *
         * @private
         * @type {string}
         * @memberof IBizViewController
         */
        _this.url = '';
        _this.url = opts.url;
        Object.assign(_this.model, { title: opts.title, caption: opts.caption, icon: opts.icon, iconcls: opts.iconcls });
        return _this;
    }
    /**
     * 初始化
     * 模拟vue生命周期
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.VueOnInit = function (vue) {
        _super.prototype.VueOnInit.call(this, vue);
        this.parseViewParams();
        this.onInit();
        this.onInited();
    };
    /**
     * 销毁
     * 模拟Vue生命周期
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.VueOnDestroy = function () {
        this.onDestroy();
    };
    /**
     * 视图组件销毁时调用
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onDestroy = function () {
        _super.prototype.onDestroy.call(this);
    };
    /**
     * 视图参数变化，嵌入表单，手动刷新数据
     *
     * @param {*} change
     * @memberof IBizViewController
     */
    IBizViewController.prototype.viewParamChange = function (change) {
        if (change && change.srfparentkey && !Object.is(change.srfparentkey, '')) {
            this.addViewParam(change);
            this.refresh();
        }
    };
    /**
     * 视图初始化
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onInit = function () {
        this.regUIActions();
        this.regUICounters();
        this.regCodeLists();
        this.onInitComponents();
        this.onLoad();
        this.fire(IBizViewController.INITED, this);
    };
    /**
     * 部件初始化
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onInitComponents = function () {
    };
    /**
     * 数据加载
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onLoad = function () {
    };
    /**
     * 视图控制器初始化完成
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onInited = function () {
        this.bInited = true;
    };
    /**
     * 开始触发界面行为
     *
     * @param {*} id
     * @memberof IBizViewController
     */
    IBizViewController.prototype.clickButton = function (id) {
        this.onClickTBItem({ tag: id });
    };
    /**
     *
     *
     * @param {any} params
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onClickTBItem = function (params) {
    };
    /**
     * 设置部件
     *
     * @param {string} name
     * @param {*} control
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regControl = function (name, control) {
        this.controls.set(name, control);
    };
    /**
     * 获取部件
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getControl = function (name) {
        return this.controls.get(name);
    };
    /**
     * 关闭
     *
     * @returns {boolean}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.isClosed = function () {
        return true;
    };
    /**
     *
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.quit = function () {
    };
    /**
     *
     *
     * @param {string} itemId
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getItem = function (itemId) {
        if (this.itemMap[itemId]) {
            return this.itemMap[itemId];
        }
        return undefined;
    };
    /**
     *
     *
     * @param {string} itemId
     * @param {*} item
     * @memberof IBizViewController
     */
    IBizViewController.prototype.registerItem = function (itemId, item) {
        this.itemMap[itemId] = item;
    };
    /**
     *
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.unloaded = function () {
        return null;
    };
    /**
     *
     *
     * @returns {string}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getAppCtx = function () {
        return '';
    };
    /**
     * 注册子控制器对象
     *
     * @param {*} ctrler
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regController = function (ctrler) {
    };
    /**
     * 获取子控制器对象
     *
     * @param {string} id
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getController = function (id) {
        return undefined;
    };
    /**
     * 获取父控件
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getPController = function () {
        var win = window;
        var iBizApp = win.getIBizApp();
        var PController = null;
        ;
        if (iBizApp) {
            PController = iBizApp.getParentController(this.getId(), this.getViewUsage());
        }
        return PController;
    };
    /**
     * 注销子控制器对象
     *
     * @param {*} ctrler
     * @memberof IBizViewController
     */
    IBizViewController.prototype.unRegController = function (ctrler) {
    };
    /**
     * 注册代码表
     *
     * @param {*} codelist
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regCodeList = function (codelist) {
        if (!this.codelists) {
            this.codelists = {};
        }
        this.codelists[codelist.getId()] = codelist;
    };
    /**
     * 获取代码表
     *
     * @param {string} codelistId
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getCodeList = function (codelistId) {
        if (!this.codelists) {
            return undefined;
        }
        if (this.codelists[codelistId]) {
            return this.codelists[codelistId];
        }
        return undefined;
    };
    /**
     * 注册界面行为
     *
     * @param {*} [uiaction={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regUIAction = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        if (uiaction) {
            this.uiactions.set(uiaction.tag, uiaction);
        }
    };
    /**
     * 获取界面行为
     *
     * @param {string} uiactionId
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getUIAction = function (uiactionId) {
        return this.uiactions.get(uiactionId);
    };
    /**
     * 注册界面计数器
     *
     * @param {string} name
     * @param {*} uicounter
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regUICounter = function (name, uicounter) {
        this.uicounters.set(name, uicounter);
    };
    /**
     * 获取界面计数器
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getUICounter = function (name) {
        return this.uicounters.get(name);
    };
    /**
     * 刷新全部界面计数器
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.reloadUICounters = function () {
        this.uicounters.forEach(function (uicounter) {
            uicounter.reload();
        });
        var pController = this.getPController();
        if (pController) {
            pController.reloadUICounters();
        }
    };
    /**
     * 获取窗口对象
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getWindow = function () {
        return window;
    };
    /**
     * 是否支持视图模型
     *
     * @returns {boolean}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.isEnableViewModel = function () {
        return false;
    };
    /**
     * 获取后台地址
     *
     * @returns {string}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getBackendUrl = function () {
        if (this.url) {
            return this.url;
        }
        return undefined;
    };
    /**
     * 获取动态视图参数
     *
     * @returns {(any | undefined)}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getDynamicParams = function () {
        return {};
    };
    /**
     * 刷新
     *
     * @private
     * @memberof IBizViewController
     */
    IBizViewController.prototype.refresh = function () {
        this.onRefresh();
    };
    /**
     * 视图刷新方法，继承视图控制器重写
     *
     * @param {*} [uiaction]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onRefresh = function (uiaction) {
    };
    /**
     * 刷新子项
     *
     * @param {string} name
     * @memberof IBizViewController
     */
    IBizViewController.prototype.refreshItem = function (name) {
    };
    /**
     * 设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.setParentData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentData = {};
        Object.assign(this.srfParentData, data);
        this.onSetParentData();
        this.reloadUpdatePanels();
    };
    /**
     * 设置父数据
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onSetParentData = function () {
    };
    /**
     * 获取父数据
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getParentData = function () {
        return this.srfParentData;
    };
    /**
     * 设置父模式
     *
     * @param {*} [data={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.setParentMode = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentMode = {};
        Object.assign(this.srfParentMode, data);
    };
    /**
     * 获取父模式
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getParentMode = function () {
        return this.srfParentMode;
    };
    /**
     * 获取引用数据
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getViewParam = function () {
        return this.viewParam;
    };
    /**
     * 设置关系数据
     *
     * @param {*} [data={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.setReferData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfReferData = {};
        Object.assign(this.srfReferData, data);
    };
    /**
     * 获取关系数据
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getReferData = function () {
        return this.srfReferData;
    };
    /**
     * 正常代码表模式
     *
     * @param {string} codeListId 代码表ID
     * @param {string} value 数据值
     * @param {string} emtpytext 空值显示数据
     * @returns {string}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.renderCodeList_Normal = function (codeListId, value, emtpytext) {
        if (!value) {
            return emtpytext;
        }
        var codelist = this.getCodeList(codeListId);
        if (codelist) {
            var result_1 = '';
            var values = value.split(';');
            values.forEach(function (value) {
                var item = codelist.getItemByValue(value);
                if (item) {
                    result_1 += '、' + codelist.getCodeItemText(item);
                }
            });
            if (result_1.length > 1) {
                result_1 = result_1.substring(1);
            }
            return result_1;
        }
        return '';
    };
    /**
     * 代码表数字或处理
     *
     * @param {string} codeListId 代码表ID
     * @param {string} value 数据值
     * @param {string} emtpytext 空值显示信息
     * @param {string} textSeparator 文本拼接方式
     * @returns {string}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.renderCodeList_NumOr = function (codeListId, value, emtpytext, textSeparator) {
        if (!textSeparator || Object.is(textSeparator, '')) {
            textSeparator = '、';
        }
        var strTextOr = '';
        if (!value) {
            return emtpytext;
        }
        var nValue = parseInt(value, 10);
        var codelist = this.getCodeList(codeListId);
        if (codelist) {
            codelist.data.forEach(function (ele) {
                var codevalue = ele.value;
                if ((parseInt(codevalue, 10) & nValue) > 0) {
                    if (strTextOr.length > 0) {
                        strTextOr += (textSeparator);
                    }
                    strTextOr += codelist.getCodeItemText(ele);
                }
            });
        }
        return strTextOr;
    };
    /**
     * 代码表文本或处理
     *
     * @param {string} codeListId 代码表ID
     * @param {*} value 数据值
     * @param {*} emtpytext 空值显示信息
     * @param {*} textSeparator 文本凭借方式
     * @param {*} valueSeparator 数据值分割方式
     * @returns {string}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.renderCodeList_StrOr = function (codeListId, value, emtpytext, textSeparator, valueSeparator) {
        var _this = this;
        if (!textSeparator || Object.is(textSeparator, '')) {
            textSeparator = '、';
        }
        if (!value) {
            return emtpytext;
        }
        var strTextOr = '';
        var codelist = this.getCodeList(codeListId);
        var arrayValue = value.split(valueSeparator);
        arrayValue.forEach(function (value) {
            var strText = '';
            strText = _this.renderCodeList_Normal(codeListId, value, emtpytext);
            if (strTextOr.length > 0) {
                strTextOr += (textSeparator);
            }
            strTextOr += strText;
        });
        return strTextOr;
    };
    /**
     *
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.initViewLogic = function () {
    };
    /**
     *
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onPrepareViewLogics = function () {
    };
    /**
     *
     *
     * @param {*} logic
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regViewLogic = function (logic) {
    };
    /**
     *
     *
     * @param {*} tag
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getViewLogic = function (tag) {
        return undefined;
    };
    /**
     *
     *
     * @param {any} ctrlid
     * @param {any} command
     * @param {any} arg
     * @memberof IBizViewController
     */
    IBizViewController.prototype.invokeCtrl = function (ctrlid, command, arg) {
    };
    /**
     * 注册界面更新面板
     *
     * @param {*} updatepanel
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regUpdatePanel = function (updatepanel) {
    };
    /**
     * 获取界面更新面板
     *
     * @param {string} updatepanelId
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getUpdatePanel = function (updatepanelId) {
        return undefined;
    };
    /**
     * 刷新全部界面更新面板
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.reloadUpdatePanels = function () {
    };
    /**
     * 填充更新面板调用参数
     *
     * @param {*} [params={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.onFillUpdatePanelParam = function (params) {
        if (params === void 0) { params = {}; }
    };
    /**
     * 初始化注册界面行为
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regUIActions = function () {
    };
    /**
     * 初始化注册计数器
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regUICounters = function () {
    };
    /**
     * 初始化代码表
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.regCodeLists = function () {
    };
    /**
     * 解析视图参数，初始化调用
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.parseViewParams = function () {
        _super.prototype.parseViewParams.call(this);
    };
    /**
     * 添加视图参数, 处理视图刷新操作
     *
     * @param {*} [param={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.addViewParam = function (param) {
        if (param === void 0) { param = {}; }
        _super.prototype.addViewParam.call(this, param);
    };
    /**
     * 打开数据视图,模态框打开
     *
     * @param {*} [view={}]
     * @returns {Subject<any>}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.openModal = function (view) {
        if (view === void 0) { view = {}; }
        return _super.prototype.openModal.call(this, view);
    };
    /**
     * 关闭模态框
     *
     * @param {*} [result]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.closeModal = function (result) {
        _super.prototype.closeModal.call(this, result);
    };
    /**
     * 打开视图;打开方式,路由打开
     *
     * @param {string} routeString 相对路由地址
     * @param {*} [routeParam={}] 激活路由参数
     * @param {*} [queryParams] 路由全局查询参数
     * @memberof IBizViewController
     */
    IBizViewController.prototype.openView = function (routeString, routeParam, queryParams) {
        if (routeParam === void 0) { routeParam = {}; }
        _super.prototype.openView.call(this, routeString, routeParam, queryParams);
    };
    /**
     * 打开新窗口
     *
     * @param {string} viewurl
     * @param {*} [parsms={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.openWindow = function (viewurl, parsms) {
        if (parsms === void 0) { parsms = {}; }
        _super.prototype.openWindow.call(this, viewurl, parsms);
    };
    /**
    * 视图是否是模态框对象
    *
    * @returns {boolean}
    * @memberof IBizViewController
    */
    IBizViewController.prototype.isModal = function () {
        var type = false;
        if (this.getViewUsage() === IBizViewController.VIEWUSAGE_MODAL) {
            type = true;
        }
        return type;
    };
    /**
     * 获取实体名称
     *
     * @returns {string}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getDEName = function () {
        return '';
    };
    /**
     * 返回历史记录
     *
     * @memberof IBizViewController
     */
    IBizViewController.prototype.goBack = function () {
    };
    /**
     * 数据变化
     *
     * @param {*} [data]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.dataChange = function (data) {
        _super.prototype.dataChange.call(this, data);
    };
    /**
     * 设置视图模型
     *
     * @param {*} [viewMoal={}]
     * @memberof IBizViewController
     */
    IBizViewController.prototype.setViewModal = function (viewMoal) {
        if (viewMoal === void 0) { viewMoal = {}; }
        Object.assign(this.viewModal, JSON.parse(JSON.stringify(viewMoal)));
    };
    /**
     * 获取视图模型
     *
     * @returns {*}
     * @memberof IBizViewController
     */
    IBizViewController.prototype.getViewModal = function () {
        return this.viewModal;
    };
    /**
     * 视图初始化完成
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewController.INITED = 'INITED';
    /**
     * 顶级容器分页
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewController.OPENMODE_INDEXVIEWTAB = 'INDEXVIEWTAB';
    /**
     * 非模式弹出
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewController.OPENMODE_POPUP = 'POPUP';
    /**
     * 模式弹出
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewController.OPENMODE_POPUPMODAL = 'POPUPMODAL';
    /**
     * 独立程序弹出
     *
     * @static
     * @memberof IBizViewController
     */
    IBizViewController.OPENMODE_POPUPAPP = 'POPUPAPP';
    return IBizViewController;
}(IBizViewControllerBase));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 视图主控制器
 *
 * @class IBizMainViewController
 * @extends {IBizViewController}
 */
var IBizMainViewController = /** @class */ (function (_super) {
    __extends(IBizMainViewController, _super);
    /**
     * Creates an instance of IBizMainViewController.
     * 创建 IBizMainViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMainViewController
     */
    function IBizMainViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否显示工具栏，默认显示
         *
         * @type {boolean}
         * @memberof IBizMainViewController
         */
        _this.isShowToolBar = true;
        /**
         * 视图控制器标题
         *
         * @type {string}
         * @memberof IBizMainViewController
         */
        _this.caption = '';
        /**
         * 实体数据权限
         *
         * @type {*}
         * @memberof IBizMainViewController
         */
        _this.dataaccaction = {};
        /**
         * 视图模型
         *
         * @type {*}
         * @memberof IBizMainViewController
         */
        _this.viewModel = {};
        return _this;
    }
    /**
     * 视图处初始化部件
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var toolbar = this.getToolBar();
        if (toolbar) {
            // 工具栏点击
            toolbar.on(IBizToolbar.ITEMCLICK).subscribe(function (params) {
                _this.onClickTBItem(params);
            });
        }
        var toolbar2 = this.getToolBar2();
        if (toolbar2) {
            // 工具栏点击
            toolbar2.on(IBizToolbar.ITEMCLICK).subscribe(function (params) {
                _this.onClickTBItem(params);
            });
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.loadModel();
    };
    /**
     * 加载视图模型之前
     *
     * @param {*} [params={}]
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.beforeLoadMode = function (params) {
        Object.assign(params, this.getViewParam());
    };
    /**
     * 视图模型加载完毕
     *
     * @protected
     * @param {*} data
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.afterLoadMode = function (data) { };
    /**
     * 加载视图模型
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.loadModel = function () {
        var _this = this;
        var params = { srfaction: 'loadmodel' };
        this.beforeLoadMode(params);
        var url = this.isDynamicView() ? this.addOptionsForUrl(this.getBackendUrl(), this.getViewParam()) : this.getBackendUrl();
        this.iBizHttp.post(url, params).subscribe(function (data) {
            if (data.ret !== 0) {
                console.log(data.info);
                return;
            }
            if (data.dataaccaction && Object.keys(data.dataaccaction).length > 0) {
                Object.assign(_this.dataaccaction, data.dataaccaction);
                _this.onDataAccActionChange(data.dataaccaction);
            }
            _this.afterLoadMode(data);
            Object.assign(_this.viewModel, data);
        }, function (error) {
            console.log(error.info);
        });
    };
    /**
     * 添加参数到指定的url中
     *
     * @private
     * @param {string} url
     * @param {*} [opt={}]
     * @returns {string}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.addOptionsForUrl = function (url, opt) {
        if (opt === void 0) { opt = {}; }
        var keys = Object.keys(opt);
        var isOpt = url.indexOf('?');
        keys.forEach(function (key, index) {
            if (index === 0 && isOpt === -1) {
                url += "?" + key + "=" + opt[key];
            }
            else {
                url += "&" + key + "=" + opt[key];
            }
        });
        return url;
    };
    /**
     * 是否是动态视图
     *
     * @returns {boolean}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.isDynamicView = function () {
        return false;
    };
    /**
     * 点击按钮
     *
     * @param {*} [params={}]  tag 事件源
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onClickTBItem = function (params) {
        if (params === void 0) { params = {}; }
        var uiaction = this.getUIAction(params.tag);
        this.doUIAction(uiaction, params);
    };
    /**
     * 处理实体界面行为
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (uiaction && (typeof uiaction === 'string')) {
            uiaction = this.getUIAction(uiaction);
        }
        if (uiaction) {
            if (Object.is(uiaction.type, 'DEUIACTION')) {
                this.doDEUIAction(uiaction, params);
                return;
            }
            if (Object.is(uiaction.type, 'WFUIACTION')) {
                this.doWFUIAction(uiaction, params);
                return;
            }
        }
    };
    /**
     * 获取前台行为参数
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getFrontUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        if (uiaction.refreshview) {
            arg.callback = function (win) {
                this.refresh();
            };
        }
        return arg;
    };
    /**
     * 获取后台行为参数
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getBackendUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        return arg;
    };
    /**
     * 打开界面行为视图，前端实体界面行为
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [viewparam={}]  视图参数
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.openUIActionView = function (uiaction, viewparam) {
        var _this = this;
        if (uiaction === void 0) { uiaction = {}; }
        if (viewparam === void 0) { viewparam = {}; }
        var frontview = uiaction.frontview;
        frontview.viewparam = viewparam;
        if (Object.is(uiaction.fronttype, 'TOP')) {
            Object.assign(viewparam, { openerid: this.getId(), pviewusage: this.getViewUsage() });
            this.openWindow(frontview.viewurl, viewparam);
            return;
        }
        this.openModal(frontview).subscribe(function (result) {
            if (result && Object.is(result.ret, 'OK')) {
                if (uiaction.reload) {
                    _this.onRefresh(uiaction);
                }
                if (uiaction.colseeditview) {
                    _this.closeWindow();
                }
            }
        });
        return;
    };
    /**
     * 执行实体行为
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doDEUIAction = function (uiaction, params) {
        var _this = this;
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.actionmode, 'FRONT')) {
            if ((Object.is(uiaction.fronttype, 'WIZARD')) || (Object.is(uiaction.fronttype, 'SHOWPAGE')) || (Object.is(uiaction.fronttype, 'TOP'))) {
                var viewparam = this.getFrontUIActionParam(uiaction, params);
                if (!viewparam) {
                    viewparam = {};
                }
                var frontview = uiaction.frontview;
                if (params.srfmstag) {
                    Object.assign(viewparam, { srfeditmode2: params.srfmstag });
                }
                if (frontview.redirectview) {
                    this.redirectOpenView({ redirectUrl: frontview.backendurl, viewParam: viewparam });
                    return;
                }
                // 携带所有的实体界面行为参数
                this.openUIActionView(uiaction, viewparam);
                return;
            }
            if (Object.is(uiaction.fronttype, 'OPENHTMLPAGE')) {
                var viewparam_1 = this.getFrontUIActionParam(uiaction, params);
                var _names = Object.keys(viewparam_1);
                var viewparam_arr_1 = [];
                _names.forEach(function (name) {
                    if (viewparam_1[name]) {
                        viewparam_arr_1.push(name + "=" + viewparam_1[name]);
                    }
                });
                var url = uiaction.htmlpageurl.indexOf('?') !== -1 ? "" + uiaction.htmlpageurl + viewparam_arr_1.join('&') : uiaction.htmlpageurl + "?" + viewparam_arr_1.join('&');
                window.open(url, '_blank');
                return;
            }
        }
        if (Object.is(uiaction.actionmode, 'BACKEND')) {
            var param_1 = this.getBackendUIActionParam(uiaction, params);
            if (!param_1) {
                return;
            }
            param_1.srfuiactionid = uiaction.tag;
            if (uiaction.confirmmsg) {
                this.iBizNotification.confirm('提示', uiaction.confirmmsg).subscribe(function (result) {
                    if (result && Object.is(result, 'OK')) {
                        _this.doBackendUIAction(param_1);
                    }
                });
            }
            else {
                this.doBackendUIAction(param_1);
            }
            return;
        }
        this.iBizNotification.error('错误', '未处理的实体行为[' + uiaction.tag + ']');
    };
    /**
     * 执行工作流行为
     *
     * @param {*} [uiaction={}] 行为
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doWFUIAction = function (uiaction, params) {
        var _this = this;
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.actionmode, 'WFFRONT')) {
            if (Object.is(uiaction.fronttype, 'WIZARD') || Object.is(uiaction.fronttype, 'SHOWPAGE')) {
                // let className: string;
                // if (uiaction.frontview.className) {
                //     className = uiaction.frontview.className;
                // } else {
                //     className = uiaction.frontview.classname;
                // }
                var opt = { viewparam: {} };
                var data = this.getFrontUIActionParam(uiaction, params);
                // opt.modalZIndex = this.modalZIndex;
                // opt.viewparam = {};
                if (data) {
                    Object.assign(opt.viewparam, data);
                }
                if (uiaction.frontview.viewParam) {
                    Object.assign(opt.viewparam, uiaction.frontview.viewParam);
                }
                else {
                    Object.assign(opt.viewparam, uiaction.frontview.viewparam);
                }
                Object.assign(opt, {
                    modalviewname: uiaction.frontview.modalviewname,
                    title: uiaction.frontview.title,
                    height: uiaction.frontview.height,
                    width: uiaction.frontview.width,
                });
                // 打开模态框
                var modal = this.openModal(opt);
                modal.subscribe(function (result) {
                    if (result && Object.is(result.ret, 'OK')) {
                        _this.onWFUIFrontWindowClosed(result);
                    }
                });
                return;
            }
        }
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.ERROR','错误'),IGM('MAINVIEWCONTROLLER.DOWFUIACTION.INFO','未处理的实体工作流行为['+uiaction.tag+']',[uiaction.tag]), 2);
        this.iBizNotification.warning('错误', '未处理的实体工作流行为[' + uiaction.tag + ']');
    };
    /**
     * 关系工作流窗口对象
     *
     * @param {*} win
     * @param {*} [data={}]
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onWFUIFrontWindowClosed = function (win, data) {
        if (data === void 0) { data = {}; }
    };
    /**
     * 执行后台行为
     *
     * @param {*} [uiaction={}] 行为
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.doBackendUIAction = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.ERROR','错误'),IGM('MAINVIEWCONTROLLER.DOBACKENDUIACTION.INFO','未处理的后台界面行为['+uiaction.tag+']',[uiaction.tag]), 2);
        this.iBizNotification.error('错误', '未处理的后台界面行为[' + uiaction.tag + ']');
    };
    /**
     * 是否-模式框显示
     *
     * @returns {boolean}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.isShowModal = function () {
        return false;
    };
    /**
     * 关闭窗口
     *
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.closeWindow = function () {
        if (this.isModal()) {
            this.closeModal({});
            return;
        }
        var win = this.getWindow();
        var viewParams = this.getViewParam();
        if (viewParams.ru && !Object.is(viewParams.ru, '') && (viewParams.ru.startsWith('https://') || viewParams.ru.startsWith('http://'))) {
            win.location.href = viewParams.ru;
            return;
        }
        win.close();
    };
    /**
     * 获取窗口对象
     *
     * @returns {Window}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getWindow = function () {
        return window;
    };
    /**
     * 获取标题
     *
     * @returns {string} 标题
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getCaption = function () {
        return this.caption;
    };
    /**
     * 设置标题
     *
     * @param {string} caption 标题
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.setCaption = function (caption) {
        if (!Object.is(this.caption, caption)) {
            this.caption = caption;
            this.fire(IBizMainViewController.CAPTIONCHANGED, this);
        }
    };
    /**
     * 获取工具栏服务对象
     *
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getToolBar = function () {
        return this.getControl('toolbar');
    };
    /**
     * 获取工具栏服务对象
     *
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getToolBar2 = function () {
        return this.getControl('toolbar2');
    };
    /**
     * 计算工具栏项状态-<例如 根据是否有选中数据,设置 工具栏按钮是否可点击>
     *
     * @param {boolean} hasdata 是否有数据
     * @param {*} dataaccaction
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.calcToolbarItemState = function (hasdata, dataaccaction) {
        var toolbar = this.getToolBar();
        if (toolbar && toolbar.getItems().length > 0) {
            var items = toolbar.getItems();
            toolbar.setItemDisabled(items, !hasdata);
            toolbar.updateAccAction(items, Object.assign({}, this.dataaccaction, dataaccaction));
        }
        var toolbar2 = this.getToolBar2();
        if (toolbar2 && toolbar2.getItems().length > 0) {
            var items = toolbar2.getItems();
            toolbar2.setItemDisabled(items, !hasdata);
            toolbar2.updateAccAction(items, Object.assign({}, this.dataaccaction, dataaccaction));
        }
    };
    /**
     * 获取引用视图
     *
     * @returns {*}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.getReferView = function () {
        return undefined;
    };
    /**
     * 打开重定向视图
     *
     * @param {{ redirectUrl: string, viewParam: any }} view 打开视图的参数
     * @param {*} [uiaction] 实体界面行为
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.redirectOpenView = function (view, uiaction) {
        var _this = this;
        var viewParam = {};
        viewParam.srfviewparam = JSON.stringify(view.viewParam);
        Object.assign(viewParam, { 'srfaction': 'GETRDVIEW' });
        this.iBizHttp.post(view.redirectUrl, viewParam).subscribe(function (response) {
            if (!response.rdview || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.info('错误', response.errorMessage);
                }
                return;
            }
            if (!response.rdview || Object.keys(response.rdview).length === 0) {
                _this.iBizNotification.info('错误', '重定向视图信息获取错误,无法打开!');
            }
            var data = {};
            if (response.rdview.srfkey || Object.is(response.rdview.srfkey, '')) {
                Object.assign(data, { srfkey: response.rdview.srfkey, srfkeys: response.rdview.srfkey });
            }
            if (response.rdview.srfviewparam) {
                Object.assign(data, response.rdview.srfviewparam);
            }
            var rdview = response.rdview;
            Object.assign(data, { openerid: _this.getId(), pviewusage: _this.getViewUsage() });
            if (Object.is(rdview.openmode, 'POPUPMODAL')) {
                var formatname = IBizUtil.srfFilePath2(rdview.viewname);
                var modelviewname = formatname + "_modalview";
                var view_1 = {
                    height: rdview.height,
                    modalviewname: modelviewname,
                    title: rdview.title,
                    viewparam: data,
                    width: rdview.width,
                };
                var modal = _this.openModal(view_1);
                modal.subscribe(function (result) {
                    if (result && Object.is(result.ret, 'OK')) {
                        if (!uiaction) {
                            return;
                        }
                        if (uiaction.reload) {
                            _this.onRefresh(uiaction);
                        }
                        if (uiaction.colseeditview) {
                            _this.closeWindow();
                        }
                    }
                });
            }
            else {
                var _modulename = IBizUtil.srfFilePath2(rdview.modulename);
                var _viewname = IBizUtil.srfFilePath2(rdview.viewname);
                var url = "/pages/" + _modulename + "/" + _viewname + "/" + _viewname + ".html#/" + _viewname;
                _this.openWindow(url, data);
                // const routeUrl = `${rdview.modulename.toLowerCase()}_${rdview.viewname.toLowerCase()}`;
                // this.openView(routeUrl, data);
            }
        }, function (error) {
            _this.iBizNotification.info('错误', error.info);
        });
    };
    /**
     * 实体数据发生变化
     *
     * @param {*} [dataaccaction={}]
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.onDataAccActionChange = function (dataaccaction) {
        if (dataaccaction === void 0) { dataaccaction = {}; }
    };
    /**
     * 是否自由布局
     *
     * @returns {boolean}
     * @memberof IBizMainViewController
     */
    IBizMainViewController.prototype.isFreeLayout = function () {
        return false;
    };
    /**
     * 设置视图标题
     *
     * @static
     * @memberof IBizMainViewController
     */
    IBizMainViewController.CAPTIONCHANGED = 'CAPTIONCHANGED';
    /**
     * 视图模型加载完成
     *
     * @static
     * @memberof IBizMainViewController
     */
    IBizMainViewController.VIEWMODELLOADED = 'VIEWMODELLOADED';
    return IBizMainViewController;
}(IBizViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流启动视图
 *
 * @class IBizHtmlViewController
 * @extends {IBizMainViewController}
 */
var IBizHtmlViewController = /** @class */ (function (_super) {
    __extends(IBizHtmlViewController, _super);
    /**
     * Creates an instance of IBizHtmlViewController.
     * 创建 IBizHtmlViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizHtmlViewController
     */
    function IBizHtmlViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 代理应用启动视图url
         *
         * @type {string}
         * @memberof IBizHtmlViewController
         */
        _this.viewurl = '';
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.onPostMessage().subscribe(function (data) {
                if (data === void 0) { data = {}; }
                if (!Object.is(_this.getId(), data.openerid) && !Object.is(data.type, 'SETSELECTIONS')) {
                    return;
                }
                _this.setViewResult(data);
            });
        }
        return _this;
    }
    /**
     * 刷新数据 （处理关系数据变更）
     *
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        this.loadModel();
    };
    /**
     * 模型加载之前
     *
     * @param {*} [data={}]
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.beforeLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.beforeLoadMode.call(this, data);
        if (this.getParentMode()) {
            Object.assign(data, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(data, this.getParentData());
        }
        if (this.getReferData()) {
            Object.assign(data, this.getReferData());
        }
    };
    /**
     * 视图模型加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.afterLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.afterLoadMode.call(this, data);
        var url = data.viewurl;
        this.viewurl = url.replace('-OPENERID-', this.getId());
    };
    /**
     * 设置视图结果数据
     *
     * @param {*} data
     * @memberof IBizHtmlViewController
     */
    IBizHtmlViewController.prototype.setViewResult = function (data) {
        if (data.selections && Array.isArray(data.selections)) {
            this.closeModal({ ret: 'OK', selections: data.selections });
        }
        else {
            this.closeModal();
        }
    };
    return IBizHtmlViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 实体重定向视图
 *
 * @class IBizRedirectViewController
 * @extends {IBizMainViewController}
 */
var IBizRedirectViewController = /** @class */ (function (_super) {
    __extends(IBizRedirectViewController, _super);
    /**
     * Creates an instance of IBizRedirectViewController.
     * 创建 IBizRedirectViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizRedirectViewController
     */
    function IBizRedirectViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图加载
     *
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.loadRedirectData();
    };
    /**
     * 加载重新向数据
     *
     * @private
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.loadRedirectData = function () {
        var _this = this;
        var params = { srfaction: 'GETRDVIEWURL' };
        Object.assign(params, this.getViewParam());
        this.beforeLoadRedirectData(params);
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (success) {
            _this.afterLoadRedirectData(success);
            if (success.ret === 0) {
                if (success.rdview && !Object.is(success.rdview, '')) {
                    window.location = success.rdview;
                }
            }
        }, function (error) {
            _this.afterLoadRedirectData(error);
        });
    };
    /**
     * 加载重定向数据之前
     *
     * @param {*} [data={}]
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.beforeLoadRedirectData = function (data) {
        if (data === void 0) { data = {}; }
    };
    /**
     * 加载重定向数据之后
     *
     * @param {*} [data={}]
     * @memberof IBizRedirectViewController
     */
    IBizRedirectViewController.prototype.afterLoadRedirectData = function (data) {
        if (data === void 0) { data = {}; }
    };
    return IBizRedirectViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 自定义视图控制器对象
 *
 * @class IBizCustomViewController
 * @extends {IBizMainViewController}
 */
var IBizCustomViewController = /** @class */ (function (_super) {
    __extends(IBizCustomViewController, _super);
    /**
     * Creates an instance of IBizCustomViewController.
     * 创建 IBizCustomViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizCustomViewController
     */
    function IBizCustomViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 加载部件
     *
     * @param {*} [opt={}]
     * @memberof IBizMDViewController
     */
    IBizCustomViewController.prototype.onLoad = function (opt) {
        if (opt === void 0) { opt = {}; }
        if (this.controls) {
            this.controls.forEach(function (obj, key) {
                if (obj instanceof IBizToolbar) {
                    return;
                }
                if (obj.load) {
                    obj.load(opt);
                }
            });
        }
    };
    return IBizCustomViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 首页应用视图
 *
 * @class IBizIndexViewController
 * @extends {IBizMainViewController}
 */
var IBizIndexViewController = /** @class */ (function (_super) {
    __extends(IBizIndexViewController, _super);
    /**
     * Creates an instance of IBizIndexViewController.
     * 创建 IBizIndexViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizIndexViewController
     */
    function IBizIndexViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 默认打开视图
         *
         * @type {*}
         * @memberof IBizIndexViewController
         */
        _this.defOpenView = {};
        _this.regDefOpenView();
        return _this;
    }
    /**
     * 应用菜单部件初始化
     *
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var appMenu = this.getAppMenu();
        if (appMenu) {
            // 菜单加载完成
            appMenu.on(IBizAppMenu.LOADED).subscribe(function (items) {
                _this.appMenuLoaded(items);
            });
            // 菜单选中
            appMenu.on(IBizAppMenu.MENUSELECTION).subscribe(function (items) {
                _this.appMenuSelection(items);
            });
        }
    };
    /**
     * 部件加载
     *
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        var appMenu = this.getAppMenu();
        if (appMenu) {
            appMenu.load();
        }
        this.setMianMenuState();
    };
    /**
     * 应用菜单部件加载完成,调用基类处理
     *
     * @private
     * @param {any[]} items
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.appMenuLoaded = function (items) {
        var matched = this.$route.matched;
        var appMenu = this.getAppMenu();
        if (matched[this.route_index + 1]) {
            var next_route_name = matched[this.route_index + 1].name;
            var _app = appMenu.getAppFunction('', next_route_name);
            var _item = {};
            if (_app && Object.keys(_app).length > 0) {
                Object.assign(_item, appMenu.getSelectMenuItem(items, _app));
            }
            if (Object.keys(_item).length > 0) {
                appMenu.setAppMenuSelected(_item);
            }
        }
        else if (this.defOpenView && Object.keys(this.defOpenView).length > 0) {
            var defView = {};
            var _app = appMenu.getAppFunction('', this.defOpenView.routepath);
            if (_app) {
                Object.assign(defView, appMenu.getSelectMenuItem(items, _app), _app);
            }
            if (Object.keys(defView).length > 0) {
                appMenu.setAppMenuSelected(defView);
                this.appMenuSelection([defView]);
            }
        }
        else {
            var firstItem = {};
            Object.assign(firstItem, this.getFirstMenuItem(items));
            if (firstItem && Object.keys(firstItem).length > 0) {
                appMenu.setAppMenuSelected(firstItem);
                this.appMenuSelection([firstItem]);
            }
        }
    };
    /**
     * 应用菜单选中
     *
     * @param {Array<any>} items
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.appMenuSelection = function (items) {
        var item = {};
        Object.assign(item, items[0]);
        this.getAppMenu().setAppMenuSelected(items[0]);
        if (Object.is(item.functype, 'APPVIEW')) {
            // 打开应用视图
            this.openView(item.routepath, item.openviewparam);
        }
        else if (Object.is(item.functype, 'OPENHTMLPAGE')) {
            // 打开HTML页面
            window.open(item.htmlPageUrl, '_blank');
        }
    };
    /**
     * 获取表单项
     *
     * @returns {*}
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.getAppMenu = function () {
        return this.getControl('appmenu');
    };
    /**
     * 设置主菜单状态
     *
     * @param {string} [align]
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.setMianMenuState = function (align) {
    };
    /**
     * 注册默认打开视图
     *
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.regDefOpenView = function () {
    };
    /**
     * 获取第一个带导航内容的菜单数据
     *
     * @private
     * @param {Array<any>} items
     * @returns {*}
     * @memberof IBizIndexViewController
     */
    IBizIndexViewController.prototype.getFirstMenuItem = function (items) {
        var _this = this;
        var app = {};
        var appMenu = this.getAppMenu();
        items.some(function (_item) {
            var _app = appMenu.getAppFunction(_item.appfuncid, '');
            if (_app && Object.keys(_app).length > 0) {
                Object.assign(app, _item, _app);
                return true;
            }
            if (_item.items && _item.items.length > 0) {
                var subapp = _this.getFirstMenuItem(_item.items);
                if (subapp && Object.keys(subapp).length > 0) {
                    Object.assign(app, _item, subapp);
                    return true;
                }
            }
        });
        return app;
    };
    return IBizIndexViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多项数据视图控制器
 *
 * @class IBizMDViewController
 * @extends {IBizMainViewController}
 */
var IBizMDViewController = /** @class */ (function (_super) {
    __extends(IBizMDViewController, _super);
    /**
     * Creates an instance of IBizMDViewController.
     * 创建 IBizMDViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMDViewController
     */
    function IBizMDViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 当前数据主键
         *
         * @type {string}
         * @memberof IBizMDViewController
         */
        _this.currentDataKey = '';
        /**
         * 是否支持多选
         *
         * @type {boolean}
         * @memberof IBizMDViewController
         */
        _this.multiSelect = false;
        /**
         * 快速搜索值
         *
         * @type {string}
         * @memberof IBizMDViewController
         */
        _this.searchValue = '';
        /**
         * 父数据改变
         *
         * @type {boolean}
         * @memberof IBizMDViewController
         */
        _this.parentDataChanged = false;
        /**
         * 表格是否支持行编辑
         *
         * @type {boolean}
         * @memberof IBizMDViewController
         */
        _this.isInGridRowEdit = false;
        /**
         * 是否打开行编辑
         *
         * @type {boolean}
         * @memberof IBizMDViewController
         */
        _this.isOpenRowEdit = false;
        /**
         * 默认进入行编辑是否完成
         *
         * @type {boolean}
         * @memberof IBizMDViewController
         */
        _this.isRowEditDefaulted = false;
        /**
         * 实体支持快速搜索属性
         *
         * @type {Array<any>}
         * @memberof IBizMDViewController
         */
        _this.quickSearchEntityDEFields = [];
        /**
         * 快速搜索提示信息
         *
         * @type {string}
         * @memberof IBizMDViewController
         */
        _this.quickSearchTipInfo = '';
        /**
         * 数据部件数据加载完成 <Output>
         *
         * @type {string}
         * @memberof IBizMDViewController
         */
        _this.onMDItemsLoad = 'onMDItemsLoad';
        _this.regQuickSearchDEFileds();
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            iBizApp.onRefreshView().subscribe(function (data) {
                if (data === void 0) { data = {}; }
                if (!Object.is(_this.getId(), data.openerid)) {
                    return;
                }
                var controller = iBizApp.getSRFController(data.openerid, data.viewUsage);
                if (controller) {
                    _this.onRefresh();
                }
            });
            iBizApp.onPostMessage().subscribe(function (data) {
                if (data === void 0) { data = {}; }
                if (!Object.is(_this.getId(), data.openerid) && !Object.is(data.type, 'REFERVIEW')) {
                    return;
                }
                var controller = iBizApp.getSRFController(data.openerid, data.viewUsage);
                if (controller) {
                    _this.onRefresh();
                }
            });
        }
        return _this;
    }
    /**
     * 初始化部件对象
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        this.parentDataChanged = false;
        var mdctrl = this.getMDCtrl();
        if (mdctrl) {
            // 多数据部件选中
            mdctrl.on(IBizMDControl.SELECTIONCHANGE).subscribe(function (args) {
                _this.onSelectionChange(args);
            });
            // 多数据部件加载之前
            mdctrl.on(IBizMDControl.BEFORELOAD).subscribe(function (args) {
                _this.onStoreBeforeLoad(args);
            });
            // 多数据部件加载完成
            mdctrl.on(IBizMDControl.LOADED).subscribe(function (args) {
                _this.onStoreLoad(args);
            });
            // 多数据部件状态改变
            mdctrl.on(IBizDataGrid.CHANGEEDITSTATE).subscribe(function (args) {
                _this.onGridRowEditChange(undefined, args, undefined);
            });
            // 多数据界面行为
            mdctrl.on(IBizMDControl.UIACTION).subscribe(function (args) {
                if (args.tag) {
                    _this.doUIAction(args.tag, args.data);
                }
            });
            if (this.isEnableQuickSearch()) {
                var columns = mdctrl.getColumns();
                var _quickFields_1 = [];
                columns.forEach(function (column) {
                    var index = _this.quickSearchEntityDEFields.findIndex(function (item) { return Object.is(item.name, column.name); });
                    if (index !== -1) {
                        _quickFields_1.push(column.caption);
                    }
                });
                this.quickSearchTipInfo = _quickFields_1.join('/');
            }
        }
        var searchform = this.getSearchForm();
        if (searchform) {
            // 搜索表单加载完成
            searchform.on(IBizForm.FORMLOADED).subscribe(function (args) {
                if (_this.isLoadDefault()) {
                    _this.onSearchFormSearched();
                }
            });
            // 搜索表单搜索触发，手动触发
            searchform.on(IBizSearchForm.FORMSEARCHED).subscribe(function (args) {
                _this.onSearchFormSearched();
            });
            // 搜索表单重置
            searchform.on(IBizSearchForm.FORMRESETED).subscribe(function (args) {
                _this.onSearchFormReseted();
            });
            // 搜索表单值变化
            searchform.on(IBizForm.FORMFIELDCHANGED).subscribe(function (args) {
                if (args == null) {
                    _this.onSearchFormFieldChanged('', null, null);
                }
                else {
                    _this.onSearchFormFieldChanged(args.name, args.field, args.value);
                    _this.onSearchFormFieldValueCheck(args.name, args.field.getValue());
                }
            });
            searchform.setOpen(!this.isEnableQuickSearch());
        }
    };
    /**
     * 多数据视图加载，加载部件
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        if (this.getSearchForm()) {
            var viewparams = {};
            Object.assign(viewparams, this.getViewParam());
            this.getSearchForm().autoLoad(viewparams);
        }
        else if (this.isLoadDefault()) {
            this.load();
        }
    };
    /**
     * 加载多视图部件
     *
     * @param {*} [opt={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.load = function (opt) {
        if (opt === void 0) { opt = {}; }
        if (!this.isFreeLayout()) {
            if (this.getMDCtrl()) {
                this.getMDCtrl().load(opt);
            }
        }
        else {
            this.otherLoad(opt);
        }
    };
    /**
     * 执行快速搜索
     *
     * @param {*} event
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onQuickSearch = function (event) {
        if (!Object.is(this.searchValue, event)) {
            this.searchValue = event;
        }
        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched();
        }
    };
    /**
     * 清空快速搜索值
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.clearQuickSearchValue = function () {
        this.searchValue = undefined;
        this.onRefresh();
    };
    /**
     * 搜索表单打开
     *
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.openSearchForm = function () {
        if (!this.isEnableQuickSearch()) {
            return;
        }
        var searchForm = this.getSearchForm();
        if (searchForm) {
            searchForm.setOpen(!searchForm.opened);
        }
    };
    /**
     * 获取搜索表单对象
     *
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getSearchForm = function () {
        return undefined;
    };
    /**
     * 获取所有多项数据
     *
     * @returns {Array<any>}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getAllData = function () {
        if (this.getMDCtrl()) {
            return this.getMDCtrl().getAllData();
        }
        return [];
    };
    /**
     * 搜索表单属性值发生变化
     *
     * @param {string} fieldname
     * @param {*} field
     * @param {*} value
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onSearchFormFieldChanged = function (fieldname, field, value) {
    };
    /**
     * 搜索表单属性值检测
     *
     * @param {string} fieldname
     * @param {string} value
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onSearchFormFieldValueCheck = function (fieldname, value) {
    };
    /**
     * 数据加载之前
     *
     * @param {*} [args={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onStoreBeforeLoad = function (args) {
        if (args === void 0) { args = {}; }
        var fetchParam = {};
        // 获取视图参数
        if (this.getViewParam() && Object.keys(this.getViewParam()).length > 0) {
            Object.assign(fetchParam, this.getViewParam());
        }
        // 获取父模型
        if (this.getParentMode() && Object.keys(this.getParentMode()).length > 0) {
            Object.assign(fetchParam, this.getParentMode());
        }
        // 获取父数据
        if (this.getParentData() && Object.keys(this.getParentData()).length > 0) {
            Object.assign(fetchParam, this.getParentData());
        }
        // 获取搜索表单值参数，与快速搜索互斥
        if ((this.getSearchForm() && this.getSearchCond() && this.getSearchForm().isOpen()) || !this.isEnableQuickSearch()) {
            Object.assign(fetchParam, this.getSearchCond());
        }
        // //是否有自定义查询
        // if (this.searchform && this.searchform.isCustomSearch()) {
        // 	Object.assign(fetchParam, this.searchform.getCustomSearchVal());
        // }
        // 获取关系数据
        if (this.getReferData()) {
            Object.assign(fetchParam, this.getReferData());
        }
        // 获取快速搜索里的搜索参数，与搜索表单互斥
        if (this.isEnableQuickSearch() && this.searchValue !== undefined && (this.getSearchForm() && !this.getSearchForm().isOpen())) {
            Object.assign(fetchParam, { query: this.searchValue });
        }
        Object.assign(args, fetchParam);
    };
    /**
     * 数据加载完成
     *
     * @param {any} sender
     * @param {any} args
     * @param {any} e
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onStoreLoad = function (args) {
        var _this = this;
        this.$vue.$emit(this.onMDItemsLoad, args);
        var lastActive = null;
        if (this.currentDataKey != null && !Object.is(this.currentDataKey, '') && args && args.items) {
            args.items.forEach(function (element) {
                if (Object.is(element.srfkey, _this.currentDataKey)) {
                    lastActive = element;
                    return false;
                }
            });
        }
        if (lastActive) {
            this.getMDCtrl().setSelection(lastActive);
            this.calcToolbarItemState(true, lastActive.srfdataaccaction);
        }
        else {
            this.currentDataKey = null;
            // this.fireEvent(MDViewControllerBase.SELECTIONCHANGE, this, []);
            this.fire(IBizMDViewController.SELECTIONCHANGE, []);
            this.calcToolbarItemState(false);
        }
        this.parentDataChanged = false;
        this.reloadUICounters();
        if (this.isRowEditDefault() && !this.isRowEditDefaulted) {
            this.doOpenRowEdit({});
            this.isRowEditDefaulted = true;
        }
    };
    /**
     * 数据被激活<最典型的情况就是行双击>
     *
     * @param {*} [record={}] 行记录
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onDataActivated = function (record) {
        if (record === void 0) { record = {}; }
        if (!record || !record.srfkey) {
            return;
        }
        this.fire(IBizMDViewController.DATAACTIVATED, [record]);
        this.currentDataKey = record.srfkey;
        this.calcToolbarItemState(true, record.srfdataaccaction);
        this.onEditData({ data: record });
    };
    /**
     * 行选择变化
     *
     * @param {Array<any>} selected
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onSelectionChange = function (selected) {
        if (selected == null || selected.length == 0) {
            this.currentDataKey = null;
        }
        else {
            this.currentDataKey = selected[0].srfkey;
        }
        this.fire(IBizMDViewController.SELECTIONCHANGE, selected[0]);
        this.calcToolbarItemState(this.currentDataKey != null, (this.currentDataKey != null) ? selected[0].srfdataaccaction : null);
    };
    /**
     * 改变工具栏启用编辑按钮信息
     *
     * @param {any} sender
     * @param {any} args
     * @param {any} e
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onGridRowEditChange = function (sender, args, e) {
        // var editButton = null;
        // var submitButton = null;
        // if (this.toolbar && this.toolbar.items) {
        //     .each(this.toolbar.items, function (index, ele) {
        //         if (ele.attr('data-ibiz-tag') == 'NewRow')
        //             submitButton = ele;
        //         if (ele.attr('data-ibiz-tag') == 'ToggleRowEdit')
        //             editButton = ele;
        //     });
        // }
        // this.isInGridRowEdit = args.state;
        // if (editButton) {
        //     if (!args.state) {
        //         editButton.find('span').html(IGM('MDVIEWCONTROLLER.ONGRIDROWEDITCHANGE.ENABLE', '启用编辑'));
        //     } else {
        //         editButton.find('span').html(IGM('MDVIEWCONTROLLER.ONGRIDROWEDITCHANGE.ENABLE2', '关闭编辑'));
        //     }
        // }
        // if (submitButton)
        //     submitButton[0].disabled = !args.state;
    };
    /**
     * 计算工具栏项状态-<例如 根据是否有选中数据,设置 工具栏按钮是否可点击>
     *
     * @param {boolean} hasdata 是否有数据
     * @param {*} [dataaccaction]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.calcToolbarItemState = function (hasdata, dataaccaction) {
        _super.prototype.calcToolbarItemState.call(this, hasdata, dataaccaction);
    };
    /**
     * 实体数据发生变化
     *
     * @param {*} [dataaccaction={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onDataAccActionChange = function (dataaccaction) {
        if (dataaccaction === void 0) { dataaccaction = {}; }
        var toolBar = this.getToolBar();
        if (toolbar) {
            toolBar.updateAccAction(toolBar.getItems(), Object.assign({}, this.dataaccaction, dataaccaction));
        }
        var toolBar2 = this.getToolBar2();
        if (toolBar2) {
            toolBar2.updateAccAction(toolBar2.getItems(), Object.assign({}, this.dataaccaction, dataaccaction));
        }
        // if (this.getToolbar())
        //     this.getToolbar().updateAccAction(dataaccaction);
        // if (this.mintoolbar)
        //     this.mintoolbar.updateAccAction(dataaccaction);
        // if (this.floattoolbar)
        //     this.floattoolbar.updateAccAction(dataaccaction);
    };
    /**
     * 新建数据
     *
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onNewData = function () {
        var loadParam = {};
        if (this.getViewParam()) {
            Object.assign(loadParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(loadParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(loadParam, this.getParentData());
        }
        if (this.isEnableRowEdit() && (this.getMDCtrl() && this.getMDCtrl().getOpenEdit())) {
            this.doNewRow(loadParam);
            return;
        }
        if (this.isEnableBatchAdd()) {
            this.doNewDataBatch(loadParam);
            return;
        }
        if (this.doNewDataWizard(loadParam)) {
            return;
        }
        Object.assign(loadParam, { openerid: this.getId(), pviewusage: this.getViewUsage() });
        this.doNewDataNormal(loadParam);
    };
    /**
     * 批量新建
     *
     * @param {*} [arg={}]
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doNewDataBatch = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var mpickupview = this.getMPickupView(arg);
        if (mpickupview) {
            this.openModal(mpickupview).subscribe(function (data) {
                if (data && Object.is(data.ret, 'OK')) {
                    _this.onMPickupWindowClose(data.selections);
                }
            });
            return true;
        }
        return false;
    };
    /**
     * 批量新建关闭
     *
     * @param {Array<any>} selection
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onMPickupWindowClose = function (selection) {
        if (selection) {
            this.addDataBatch(selection);
        }
        return;
    };
    /**
     * 批量添加数据
     *
     * @param {Array<any>} selectedDatas
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.addDataBatch = function (selectedDatas) {
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.WARN', '警告'), IGM('MDVIEWCONTROLLER.ADDDATABATCH.INFO', '[addDataBatch]方法必须重写！'), 2);
        this.iBizNotification.warning('警告', '[addDataBatch]方法必须重写！');
    };
    /**
     * 向导新建数据
     *
     * @param {any} arg
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doNewDataWizard = function (arg) {
        var _this = this;
        var hasWizardView = false;
        var wizardView = this.getNewDataWizardView(arg);
        if (wizardView) {
            // 打开模态框
            this.openModal(wizardView).subscribe(function (result) {
                if (result && Object.is(result.ret, 'OK')) {
                    var data = result.selections[0];
                    var _arg = { srfnewmode: data.srfkey, openerid: _this.getId(), pviewusage: _this.getViewUsage() };
                    Object.assign(_arg, arg);
                    _this.doNewDataNormal(_arg);
                }
            });
            hasWizardView = true;
        }
        return hasWizardView;
    };
    /**
     * 向导新建数据窗口关闭
     *
     * @param {any} win
     * @param {any} eOpts
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onNewDataWizardWindowClose = function (win, eOpts) {
        // var this = win.scope;
        // var loadParam = {};//win.userData;
        // var dialogResult = win.dialogResult;
        // if (!dialogResult) return;
        // if (dialogResult == 'ok') {
        //     var selectedData = win.selectedData;
        //     if (selectedData) {
        //         var newMode = selectedData.srfkey;
        //         loadParam.srfnewmode = newMode;
        //         var view = this.getNewDataView(loadParam);
        //         if (view == null) {
        //             return;
        //         }
        //         this.openDataView(view);
        //     }
        // }
        // return;
    };
    /**
     * 常规新建数据
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doNewDataNormal = function (arg) {
        var view = this.getNewDataView(arg);
        if (view == null) {
            return false;
        }
        var openMode = view.openMode;
        if (!openMode || Object.is(openMode, '')) {
            view.openMode = 'INDEXVIEWTAB';
        }
        if (!view.state) {
            view.state = 'new';
            var viewParam = {};
            Object.assign(viewParam, view.viewParam);
            if (viewParam && viewParam.srfnewmode && !Object.is(viewParam.srfnewmode, '')) {
                var srfnewmode = viewParam.srfnewmode.split('@').join('__');
                view.state = view.state + '_' + srfnewmode.toLowerCase();
            }
        }
        return this.openDataView(view);
    };
    /**
     * 编辑数据
     *
     * @param {any} arg
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onEditData = function (arg) {
        var loadParam = {};
        if (this.getViewParam()) {
            Object.assign(loadParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(loadParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(loadParam, this.getParentData());
        }
        if (arg.srfcopymode) {
            Object.assign(loadParam, {
                srfsourcekey: arg.data.srfkey
            });
        }
        else {
            Object.assign(loadParam, { srfkey: arg.data.srfkey, srfdeid: arg.data.srfdeid });
        }
        var editMode = this.getEditMode(arg.data);
        if (editMode) {
            loadParam.srfeditmode = editMode;
            loadParam.srfviewmode = editMode;
        }
        if (arg.data.srfmstag) {
            loadParam.srfeditmode2 = arg.data.srfmstag;
        }
        Object.assign(loadParam, { openerid: this.getId(), pviewusage: this.getViewUsage() });
        this.doEditDataNormal(loadParam);
    };
    /**
     * 执行常规编辑数据
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doEditDataNormal = function (arg) {
        if (arg === void 0) { arg = {}; }
        var view = this.getEditDataView(arg);
        if (view == null) {
            return false;
        }
        if (view.redirectview) {
            this.redirectOpenView({ redirectUrl: view.backendurl, viewParam: view.viewparam });
            return false;
        }
        var openMode = view.openMode;
        if (!openMode || Object.is(openMode, '')) {
            view.openMode = 'INDEXVIEWTAB';
        }
        if (!view.state) {
            view.state = 'edit';
            var viewParam = {};
            Object.assign(viewParam, view.viewParam);
            if (Object.keys(viewParam).length > 0) {
                var srfeditmode = '';
                if (viewParam.srfeditmode && !Object.is(viewParam.srfeditmode, '')) {
                    srfeditmode = viewParam.srfeditmode.split('@').join('__');
                }
                // 实体主状态
                if (viewParam.srfeditmode2 && !Object.is(viewParam.srfeditmode2, '') && !Object.is(viewParam.srfeditmode2, 'MSTAG:null')) {
                    srfeditmode = viewParam.srfeditmode2.split(':').join('__');
                }
                if (!Object.is(srfeditmode, '')) {
                    view.state = view.state + "_" + srfeditmode.toLowerCase();
                }
            }
        }
        return this.openDataView(view);
    };
    /**
     * 打开数据视图
     *
     * @param {*} [view={}]
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.openDataView = function (view) {
        var _this = this;
        if (view === void 0) { view = {}; }
        var openMode = view.openMode;
        if (view.redirect) {
            this.redirectOpenView(view);
            return false;
        }
        if (openMode != undefined) {
            if (openMode == 'POPUPMODAL') {
                view.modal = true;
            }
            else if (openMode == 'POPUP') {
                view.modal = true;
            }
            else if (openMode == '' || openMode == 'INDEXVIEWTAB') {
                view.modal = false;
            }
        }
        if (view.modal) {
            var modalview = this.openModal(view);
            modalview.subscribe(function (result) {
                if (result && Object.is(result.ret, 'OK')) {
                    _this.onRefresh();
                }
            });
        }
        else {
            this.openWindow(view.viewurl || view.url, view.viewparam);
        }
        return true;
    };
    /**
     * 获取编辑模式
     *
     * @param {any} data
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getEditMode = function (data) {
        return data.srfdatatype;
    };
    /**
     * 获取编辑视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getEditDataView = function (arg) {
        return undefined;
    };
    /**
     * 获取新建视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getNewDataView = function (arg) {
        if (arg === void 0) { arg = {}; }
        var viewModal = this.getViewModal();
        if (viewModal && Array.isArray(viewModal.refviews)) {
            var refview_1 = {};
            viewModal.refviews.some(function (_refview) {
                if (Object.is(_refview.mode, 'NEWDATA') && Object.keys(_refview.view).length > 0) {
                    Object.assign(refview_1, JSON.parse(JSON.stringify(_refview.view)));
                    return true;
                }
            });
            if (Object.keys(refview_1).length > 0) {
                return refview_1;
            }
        }
        return undefined;
    };
    /**
     * 获取新建向导视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getNewDataWizardView = function (arg) {
        return undefined;
    };
    /**
     * 获取多选视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getMPickupView = function (arg) {
        return undefined;
    };
    /**
     * 获取多数据对象
     *
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getMDCtrl = function () {
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.WARN', '警告'), IGM('MDVIEWCONTROLLER.GETMDCTRL.INFO', '[getMDCtrl]方法必须重写！'), 2);
        this.iBizNotification.warning('警告', '[getMDCtrl]方法必须重写！');
    };
    /**
     * 视图刷新
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        if (!this.isFreeLayout()) {
            if (this.getMDCtrl()) {
                this.getMDCtrl().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     *
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onSetParentData = function () {
        _super.prototype.onSetParentData.call(this);
        this.parentDataChanged = true;
    };
    /**
     * 获取搜索条件
     *
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getSearchCond = function () {
        if (this.getSearchForm()) {
            return this.getSearchForm().getValues();
        }
        return undefined;
    };
    /**
     * 搜索表单搜索执行
     *
     * @param {boolean} isload 是否加载数据
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onSearchFormSearched = function () {
        if (!this.isFreeLayout()) {
            if (this.getMDCtrl()) {
                this.getMDCtrl().setCurPage(1);
                this.getMDCtrl().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 搜索表单重置完成
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onSearchFormReseted = function () {
        if (!this.isFreeLayout()) {
            if (this.getMDCtrl()) {
                this.getMDCtrl().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 处理实体界面行为
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doDEUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.tag, 'Help')) {
            this.doHelp(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Edit')) {
            this.doEdit(params);
            return;
        }
        if (Object.is(uiaction.tag, 'View')) {
            this.doView(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Print')) {
            this.doPrint(params);
            return;
        }
        if (Object.is(uiaction.tag, 'ExportExcel')) {
            this.doExportExcel(params);
            return;
        }
        if (Object.is(uiaction.tag, 'ExportModel')) {
            this.doExportModel(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Copy')) {
            this.doCopy(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Remove')) {
            this.doRemove(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Import')) {
            this.doImport(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Refresh')) {
            this.doRefresh(params);
            return;
        }
        if (Object.is(uiaction.tag, 'NewRow')) {
            this.doCheck(params);
            return;
        }
        if (Object.is(uiaction.tag, 'SaveAllEditRow')) {
            this.doSaveAllEditRow(params);
            return;
        }
        if (Object.is(uiaction.tag, 'New')) {
            this.doNew(params);
            return;
        }
        if (Object.is(uiaction.tag, 'OpenRowEdit')) {
            this.doOpenRowEdit(params);
            return;
        }
        if (Object.is(uiaction.tag, 'CloseRowEdit')) {
            this.doCloseRowEdit(params);
            return;
        }
        if (Object.is(uiaction.tag, 'ToggleRowEdit')) {
            this.doToggleRowEdit(params);
            return;
        }
        if (Object.is(uiaction.tag, 'ToggleFilter')) {
            this.doToggleFilter(params);
            return;
        }
        _super.prototype.doDEUIAction.call(this, uiaction, params);
    };
    /**
     * 多数据项界面_切换过滤面板
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doToggleFilter = function (params) {
        if (params === void 0) { params = {}; }
        this.openSearchForm();
        var searchForm = this.getSearchForm();
        if (searchForm && searchForm.isOpen()) {
            this.load();
        }
    };
    /**
     * 多数据项界面_行编辑操作
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doToggleRowEdit = function (params) {
        if (params === void 0) { params = {}; }
    };
    /**
     * 打开行编辑
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doOpenRowEdit = function (params) {
        if (params === void 0) { params = {}; }
        this.isOpenRowEdit = true;
        if (this.getMDCtrl() && typeof (this.getMDCtrl().openEdit) === 'function') {
            this.getMDCtrl().openEdit(params);
        }
    };
    /**
     * 关闭行编辑
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doCloseRowEdit = function (params) {
        if (params === void 0) { params = {}; }
        this.isOpenRowEdit = false;
        if (this.getMDCtrl() && typeof (this.getMDCtrl().closeEdit) === 'function') {
            this.getMDCtrl().closeEdit(params);
        }
    };
    /**
     * 多数据项界面_新建行操作
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doNewRow = function (params) {
        if (params === void 0) { params = {}; }
        if (this.getMDCtrl() && typeof (this.getMDCtrl().newRowAjax) === 'function') {
            this.getMDCtrl().newRowAjax(params);
        }
    };
    /**
     * 多数据项界面_检测行操作
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doCheck = function (params) {
        if (params === void 0) { params = {}; }
    };
    /**
     * 保存所有行
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doSaveAllEditRow = function (params) {
        if (params === void 0) { params = {}; }
        if (this.getMDCtrl() && typeof (this.getMDCtrl().saveAllEditRow) === 'function') {
            var loadParam = {};
            if (this.getViewParam()) {
                Object.assign(loadParam, this.getViewParam());
            }
            if (this.getParentMode()) {
                Object.assign(loadParam, this.getParentMode());
            }
            if (this.getParentData()) {
                Object.assign(loadParam, this.getParentData());
            }
            this.getMDCtrl().saveAllEditRow(loadParam);
        }
    };
    /**
     * 多数据项界面_帮助操作
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doHelp = function (params) {
        if (params === void 0) { params = {}; }
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.WARN', '警告'), IGM('MDVIEWCONTROLLER.DOHELP.INFO', '帮助操作'), 0);
        this.iBizNotification.warning('警告', '帮助操作');
    };
    /**
     * 多数据项界面_编辑操作
     *
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doEdit = function (params) {
        if (params === void 0) { params = {}; }
        // 获取要编辑的数据集合
        if (params && params.srfkey) {
            // if (.isFunction(this.getMDCtrl().findItem)) {
            //     params = this.getMDCtrl().findItem('srfkey', params.srfkey);
            // }
            var arg = { data: params };
            this.onEditData(arg);
            return;
        }
        var selectedData = this.getMDCtrl().getSelection();
        if (selectedData == null || selectedData.length === 0) {
            return;
        }
        this.onEditData({ data: selectedData[0] });
    };
    /**
     * 多数据项界面_行编辑操作
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doView = function (params) {
        this.doEdit(params);
    };
    /**
     * 多数据项界面_打印操作
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doPrint = function (params) {
        if (params === void 0) { params = {}; }
        var arg = {};
        var selectedData = this.getMDCtrl().getSelection();
        if (!selectedData || selectedData.length === 0) {
            return;
        }
        Object.assign(arg, { srfkeys: selectedData[0].srfkey });
        this.onPrintData(arg);
    };
    /**
     * 多数据项界面_导出操作（Excel）
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doExportExcel = function (params) {
        // if (params.itemtag == '') {
        // }
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.WARN', '警告'), IGM('MDVIEWCONTROLLER.DOEXPORTEXCEL.INFO', '导出操作（Excel）'), 0);
        this.iBizNotification.warning('警告', '导出操作（Excel）');
    };
    /**
     * 多数据项界面_导出数据模型
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doExportModel = function (params) {
        this.iBizNotification.warning('警告', '导出数据模型');
    };
    /**
     * 多数据项界面_拷贝操作
     *
     * @param {any} params
     * @returns {void}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doCopy = function (params) {
        // 获取要拷贝的数据集合
        if (!this.getMDCtrl()) {
            return;
        }
        var selectedData = this.getMDCtrl().getSelection();
        if (selectedData == null || selectedData.length == 0) {
            return;
        }
        var arg = { data: selectedData[0], srfcopymode: true };
        this.onEditData(arg);
    };
    /**
     * 多数据项界面_删除操作
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doRemove = function (params) {
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.WARN', '警告'), IGM('MDVIEWCONTROLLER.DOREMOVE.INFO', '删除操作'), 0);
        this.iBizNotification.warning('警告', '删除操作');
    };
    /**
     * 多数据项界面_数据导入栏
     *
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doImport = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (this.getMDCtrl() && !Object.is(this.getDEName(), '')) {
            var view = { modalviewname: 'ibiz-importdata-view', title: "导入数据", width: 500, viewparam: { dename: this.getDEName() } };
            var subject = this.openModal(view);
            subject.subscribe(function (result) {
                if (result && Object.is(result.ret, 'OK')) {
                    _this.onRefresh();
                }
            });
        }
    };
    /**
     * 多数据项界面_刷新操作
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doRefresh = function (params) {
        this.onRefresh();
    };
    /**
     * 多数据项界面_新建操作
     *
     * @param {any} params
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doNew = function (params) {
        this.onNewData();
    };
    /**
     *
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doWFUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.actionmode, 'WFBACKEND')) {
            var selectedData = this.getMDCtrl().getSelection();
            if (selectedData == null || selectedData.length === 0) {
                return;
            }
            var keys_1 = '';
            selectedData.forEach(function (element, index) {
                var key = element.srfkey;
                if (!Object.is(keys_1, '')) {
                    keys_1 += ';';
                }
                keys_1 += key;
            });
            if (this.getMDCtrl()) {
                this.getMDCtrl().wfsubmit({ srfwfiatag: uiaction.tag, srfkeys: keys_1 });
                return;
            }
        }
        _super.prototype.doWFUIAction.call(this, uiaction, params);
    };
    /**
     *
     *
     * @param {any} win
     * @param {any} data
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onWFUIFrontWindowClosed = function (win, data) {
        // this.load();
        this.onRefresh();
    };
    /**
     * 获取UI操作参数
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getFrontUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        var front_arg = _super.prototype.getFrontUIActionParam.call(this, uiaction, params);
        if (front_arg) {
            Object.assign(arg, front_arg);
        }
        if (this.getParentMode()) {
            Object.assign(arg, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(arg, this.getParentData());
        }
        var target = 'NONE';
        if (uiaction.actiontarget) {
            target = uiaction.actiontarget;
        }
        if (!Object.is(target, 'NONE')) {
            var selectedData = this.getMDCtrl().getSelection();
            if (!(selectedData == null || selectedData.length === 0)) {
                var valueitem_1 = 'srfkey';
                var paramkey = 'srfkeys';
                var paramjo = null;
                if (uiaction.actionparams) {
                    var actionparams = uiaction.actionparams;
                    valueitem_1 = (actionparams.valueitem && !Object.is(actionparams.valueitem, '')) ? actionparams.valueitem.toLowerCase() : valueitem_1;
                    paramkey = (actionparams.paramitem && !Object.is(actionparams.paramitem, '')) ? actionparams.paramitem.toLowerCase() : paramkey;
                    paramjo = actionparams.paramjo ? actionparams.paramjo : {};
                }
                if (Object.is(target, 'SINGLEKEY')) {
                    arg[paramkey] = selectedData[0][valueitem_1];
                }
                else if (Object.is(target, 'SINGLEDATA')) {
                    Object.assign(arg, selectedData[0]);
                }
                else if (Object.is(target, 'MULTIKEY')) {
                    var keys_2 = '';
                    selectedData.forEach(function (item) {
                        var key = item[valueitem_1];
                        if (!Object.is(keys_2, '')) {
                            keys_2 += ';';
                        }
                        keys_2 += key;
                    });
                    arg[paramkey] = keys_2;
                }
                if (paramjo) {
                    Object.assign(arg, paramjo);
                }
            }
        }
        return arg;
    };
    /**
     * 获取后天界面行为参数
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getBackendUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        if (this.getParentMode()) {
            Object.assign(arg, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(arg, this.getParentData());
        }
        var bSingle = false;
        if (Object.is(uiaction.actiontarget, 'SINGLEKEY')) {
            bSingle = true;
        }
        var selectedData = this.getMDCtrl().getSelection();
        if (!(selectedData == null || selectedData.length === 0)) {
            var valueitem_2 = 'srfkey';
            var paramkey = 'srfkeys';
            var paramitems_1 = '';
            var paramjo = null;
            var infoitem = 'srfmajortext';
            if (uiaction.actionparams) {
                var actionparams = uiaction.actionparams;
                valueitem_2 = (actionparams.valueitem && !Object.is(actionparams.valueitem, '')) ? actionparams.valueitem.toLowerCase() : valueitem_2;
                paramkey = (actionparams.paramitem && !Object.is(actionparams.paramitem, '')) ? actionparams.paramitem.toLowerCase() : paramkey;
                infoitem = (actionparams.textitem && !Object.is(actionparams.textitem, '')) ? actionparams.textitem.toLowerCase() : infoitem;
                paramjo = actionparams.paramjo ? actionparams.paramjo : {};
            }
            if (bSingle) {
                paramitems_1 = selectedData[0][valueitem_2];
            }
            else {
                selectedData.forEach(function (item) {
                    var key = item[valueitem_2];
                    if (!Object.is(paramitems_1, '')) {
                        paramitems_1 += ';';
                    }
                    paramitems_1 += key;
                });
            }
            arg[paramkey] = paramitems_1;
            if (paramjo) {
                Object.assign(arg, paramjo);
            }
        }
        return arg;
    };
    /**
     * 移动记录
     *
     * @param {any} target
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.moveRecord = function (target) {
    };
    /**
     *
     *
     * @param {any} arg
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doBackendUIAction = function (arg) {
        if (this.getMDCtrl()) {
            this.getMDCtrl().doUIAction(arg);
        }
    };
    /**
     * 隐藏关系列
     *
     * @param {any} parentMode
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doHideParentColumns = function (parentMode) {
    };
    /**
     * 执行数据打印
     *
     * @param {*} [arg={}]
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.onPrintData = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.doPrintDataNormal(arg);
    };
    /**
     * 常规数据打印
     *
     * @param {*} [arg={}]
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.doPrintDataNormal = function (arg) {
        if (arg === void 0) { arg = {}; }
        var view = this.getPrintDataView(arg);
        if (!view) {
            return false;
        }
        var viewurl = view.viewurl;
        if (!viewurl || Object.is(viewurl, '')) {
            viewurl = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.PDFPrint;
        }
        var viewparams_keys = Object.keys(view.viewparam);
        var _viewparam = [];
        viewparams_keys.forEach(function (key) {
            if (view.viewparam[key]) {
                _viewparam.push(key + "=" + view.viewparam[key]);
            }
        });
        viewurl = viewurl + (viewurl.indexOf('?') == -1 ? "?" + _viewparam.join('&') : _viewparam.join('&'));
        window.open(viewurl, '_blank');
        return true;
    };
    /**
     * 获取大厅数据视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.getPrintDataView = function (arg) {
        if (arg === void 0) { arg = {}; }
        return undefined;
    };
    /**
     * 是否默认加载
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isLoadDefault = function () {
        return true;
    };
    /**
     * 是否默认开启行编辑
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isRowEditDefault = function () {
        return false;
    };
    /**
     * 支持批量添加
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isEnableBatchAdd = function () {
        return false;
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isEnableQuickSearch = function () {
        return true;
    };
    /**
     * 只支持批量添加
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isBatchAddOnly = function () {
        return false;
    };
    /**
     * 是否支持行编辑
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isEnableRowEdit = function () {
        return false;
    };
    /**
     * 是否支持多选
     *
     * @returns {boolean}
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.isEnableMultiSelect = function () {
        return this.multiSelect;
    };
    /**
     * 设置支持多选
     *
     * @param {boolean} multiSelect
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.setEnableMultiSelect = function (multiSelect) {
        this.multiSelect = multiSelect;
    };
    /**
     * 注册快速搜索实体属性
     *
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.regQuickSearchDEFileds = function () {
    };
    /**
     * 附加部件加载
     *
     * @param {*} opt
     * @memberof IBizMDViewController
     */
    IBizMDViewController.prototype.otherLoad = function (opt) {
        if (opt === void 0) { opt = {}; }
        if (this.controls) {
            this.controls.forEach(function (obj, key) {
                if (obj instanceof IBizSearchForm || obj instanceof IBizToolbar) {
                    return;
                }
                if (obj.load) {
                    obj.load(opt);
                }
            });
        }
    };
    /*****************事件声明************************/
    /**
     * 数据激活<例如：表格行双击>
     *
     * @static
     * @memberof IBizMDViewController
     */
    IBizMDViewController.DATAACTIVATED = 'DATAACTIVATED';
    /**
     * 数据选择变化
     *
     * @static
     * @memberof IBizMDViewController
     */
    IBizMDViewController.SELECTIONCHANGE = 'SELECTIONCHANGE';
    return IBizMDViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 索引关系数据选择视图（部件视图）
 *
 * @class IBizDataViewController
 * @extends {IBizMDViewController}
 */
var IBizDataViewController = /** @class */ (function (_super) {
    __extends(IBizDataViewController, _super);
    /**
     * Creates an instance of IBizDataViewController.
     * 创建 IBizDataViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDataViewController
     */
    function IBizDataViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否支持多项数据选择  <Input>
         *
         * @private
         * @type {boolean}
         * @memberof IBizDataViewController
         */
        _this.multiselect = true;
        /**
         * 当前选择数据 <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizDataViewController
         */
        _this.currentValue = null;
        /**
         * 删除数据 <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizDataViewController
         */
        _this.deleteData = null;
        /**
         * 数据选中事件 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizDataViewController
         */
        _this.selectionChange = 'selectionChange';
        /**
         * 行数据激活事件 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizDataViewController
         */
        _this.dataActivated = 'dataActivated';
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onInitComponents = function () {
        var _this = this;
        var dataViewControl = this.getMDCtrl();
        if (dataViewControl) {
            // 数据视图部件行激活
            dataViewControl.on(IBizDataView.DATAACTIVATED).subscribe(function (data) {
                _this.onDataActivated(data);
            });
            // 数据视图部件行选中
            dataViewControl.on(IBizDataView.SELECTIONCHANGE).subscribe(function (data) {
                _this.onSelectionChange(data);
            });
        }
    };
    /**
     * 部件加载
     *
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onLoad = function () {
        var dataViewControl = this.getMDCtrl();
        if (dataViewControl) {
            dataViewControl.load();
        }
    };
    /**
     * 部件数据激活
     *
     * @param {*} data
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onDataActivated = function (data) {
        _super.prototype.onDataActivated.call(this, data);
        this.$vue.$emit(this.dataActivated, data);
    };
    /**
     * 部件数据行选中
     *
     * @param {*} data
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.onSelectionChange = function (data) {
        _super.prototype.onSelectionChange.call(this, data);
        this.$vue.$emit(this.selectionChange, data);
    };
    /**
     * 获取部件
     *
     * @returns {*}
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.getMDCtrl = function () {
        return this.getDataView();
    };
    /**
     * 获取数据视图部件
     *
     * @returns {*}
     * @memberof IBizDataViewController
     */
    IBizDataViewController.prototype.getDataView = function () {
        return this.getControl('dataview');
    };
    return IBizDataViewController;
}(IBizMDViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据选择索引关系数据选择视图（部件视图）
 *
 * @class IBizPickupDataViewController
 * @extends {IBizDataViewController}
 */
var IBizPickupDataViewController = /** @class */ (function (_super) {
    __extends(IBizPickupDataViewController, _super);
    /**
     * Creates an instance of IBizPickupDataViewController.
     * 创建 IBizPickupDataViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupDataViewController
     */
    function IBizPickupDataViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizPickupDataViewController;
}(IBizDataViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格视图控制
 *
 * @class IBizGridViewController
 * @extends {IBizMDViewController}
 */
var IBizGridViewController = /** @class */ (function (_super) {
    __extends(IBizGridViewController, _super);
    /**
     * Creates an instance of IBizGridViewController.
     * 创建 IBizGridViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGridViewController
     */
    function IBizGridViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var grid = this.getMDCtrl();
        if (grid) {
            // 双击行数据
            grid.on(IBizDataGrid.ROWDBLCLICK).subscribe(function (args) {
                _this.onSelectionChange(args);
                if (_this.getGridRowActiveMode() === 0) {
                    return;
                }
                _this.onDataActivated(args[0]);
            });
            // 单击行数据
            grid.on(IBizDataGrid.ROWCLICK).subscribe(function (args) {
                _this.onSelectionChange(args);
                if (_this.getGridRowActiveMode() === 1) {
                    _this.onDataActivated(args[0]);
                }
            });
            // 表格行数据变化
            grid.on(IBizDataGrid.UPDATEGRIDITEMCHANGE).subscribe(function (param) {
                if (!_this.isEnableRowEdit()) {
                    return;
                }
                _this.onGridRowChanged(param.name, param.data);
            });
        }
    };
    /**
     * 获取多项数据控件对象
     *
     * @returns {*}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.getMDCtrl = function () {
        return this.getGrid();
    };
    /**
     * 获取表格部件对象
     *
     * @returns {*}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.getGrid = function () {
        return this.getControl('grid');
    };
    /**
     * 获取搜索表单对象
     *
     * @returns {*}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.getSearchForm = function () {
        return this.getControl('searchform');
    };
    /**
     * 表格行数据激活模式，默认支持双击激活行数据
     *
     * @returns {number}  0--不激活，1--单击激活模式，2--双击激活行数据
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.getGridRowActiveMode = function () {
        return 2;
    };
    /**
     * 隐藏关系列
     *
     * @param {any} parentMode
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.doHideParentColumns = function (parentMode) {
    };
    /**
     * 隐藏列
     *
     * @param {any} columnname
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.hideGridColumn = function (columnname) {
    };
    /**
     * 删除操作
     *
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.doRemove = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        if (params && params.srfkey) {
            // if ($.isFunction(this.getMDCtrl().findItem)) {
            //     params = this.getMDCtrl().findItem('srfkey', params.srfkey);
            // }
            // //询问框
            // IBiz.confirm($IGM('GRIDVIEWCONTROLLER.DOREMOVE.INFO', '确认要删除数据，删除操作将不可恢复？'), function (result) {
            //     if (result) {
            //         this.removeData({ srfkeys: params.srfkey });
            //     }
            // });
        }
        else {
            var selectedData = this.getGrid().getSelection();
            if (!selectedData || selectedData == null || selectedData.length === 0) {
                return;
            }
            var dataInfo_1 = '';
            selectedData.forEach(function (record, index) {
                var srfmajortext = record.srfmajortext;
                if (index < 5) {
                    if (!Object.is(dataInfo_1, '')) {
                        dataInfo_1 += '、';
                    }
                    dataInfo_1 += srfmajortext;
                }
                else {
                    return false;
                }
            });
            if (selectedData.length < 5) {
                dataInfo_1 = dataInfo_1 + '共' + selectedData.length + '条数据';
            }
            else {
                dataInfo_1 = dataInfo_1 + '...' + '共' + selectedData.length + '条数据';
            }
            dataInfo_1 = dataInfo_1.replace(/[null]/g, '').replace(/[undefined]/g, '').replace(/[ ]/g, '');
            // 询问框
            this.iBizNotification.confirm('警告', '确认要删除 ' + dataInfo_1 + '，删除操作将不可恢复？').subscribe(function (result) {
                if (result && Object.is(result, 'OK')) {
                    _this.removeData(null);
                }
            });
        }
    };
    /**
     * 删除数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.removeData = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (!arg) {
            arg = {};
        }
        if (this.getParentMode()) {
            Object.assign(arg, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(arg, this.getParentData());
        }
        if (!arg.srfkeys) {
            // 获取要删除的数据集合
            var selectedData = this.getGrid().getSelection();
            if (!selectedData || selectedData == null || selectedData.length === 0) {
                return;
            }
            var keys_1 = '';
            selectedData.forEach(function (record) {
                var key = record.srfkey;
                if (!Object.is(keys_1, '')) {
                    keys_1 += ';';
                }
                keys_1 += key;
            });
            arg.srfkeys = keys_1;
        }
        var grid = this.getGrid();
        if (grid) {
            grid.remove(arg);
        }
    };
    /**
     * 批量添加数据
     *
     * @param {Array<any>} selectedDatas
     * @returns {void}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.addDataBatch = function (selectedDatas) {
        var arg = {};
        if (!selectedDatas || selectedDatas == null || selectedDatas.length === 0) {
            return;
        }
        Object.assign(arg, this.viewParam);
        if (this.getParentMode()) {
            Object.assign(arg, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(arg, this.getParentData());
        }
        var keys = '';
        selectedDatas.forEach(function (record) {
            var key = record.srfkey;
            if (!Object.is(keys, '')) {
                keys += ';';
            }
            keys += key;
        });
        arg.srfkeys = keys;
        var grid = this.getGrid();
        if (grid) {
            grid.addBatch(arg);
        }
    };
    /**
     * 编辑操作
     *
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.doEdit = function (params) {
        if (params === void 0) { params = {}; }
        var gridCtrl = this.getGrid();
        if (!gridCtrl) {
            return;
        }
        // 获取要编辑的数据集合
        if (params && params.srfkey) {
            var param = gridCtrl.findItem('srfkey', params.srfkey);
            if (!param) {
                return;
            }
            var arg_1 = { data: Object.assign(params, param) };
            this.onEditData(arg_1);
            return;
        }
        var selectedData = gridCtrl.getSelection();
        if (!selectedData || selectedData == null || selectedData.length === 0) {
            return;
        }
        var arg = { data: selectedData[0] };
        this.onEditData(arg);
    };
    /**
     * 实体界面行为参数
     *
     * @param {*} [uiaction={}] 实体界面行为
     * @returns {*}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.getBackendUIActionParam = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        if (Object.is(uiaction.actiontarget, 'SINGLEKEY') || Object.is(uiaction.actiontarget, 'MULTIKEY')) {
            var selectedData = this.getGrid().getSelection();
            if (!selectedData && selectedData == null || selectedData.length === 0) {
                return null;
            }
            var valueitem_1 = 'srfkey';
            var paramkey = 'srfkeys';
            var paramitems_1 = '';
            var paramjo = null;
            var infoitem_1 = 'srfmajortext';
            if (uiaction.actionparams) {
                var actionparams = uiaction.actionparams;
                valueitem_1 = (actionparams.valueitem && !Object.is(actionparams.valueitem, '')) ? actionparams.valueitem.toLowerCase() : valueitem_1;
                paramkey = (actionparams.paramitem && !Object.is(actionparams.paramitem, '')) ? actionparams.paramitem.toLowerCase() : paramkey;
                infoitem_1 = (actionparams.textitem && !Object.is(actionparams.textitem, '')) ? actionparams.textitem.toLowerCase() : infoitem_1;
                paramjo = actionparams.paramjo ? actionparams.paramjo : {};
            }
            var dataInfo_2 = '';
            var firstOnly_1 = (Object.is(uiaction.actiontarget, 'SINGLEKEY'));
            selectedData.some(function (record, index) {
                if (record === void 0) { record = {}; }
                var srfmajortext = record[infoitem_1];
                if (valueitem_1) {
                    var temp = record[valueitem_1];
                    if (!Object.is(paramitems_1, '')) {
                        paramitems_1 += ';';
                    }
                    paramitems_1 += temp ? temp : '';
                }
                if (index < 5) {
                    if (!Object.is(dataInfo_2, '')) {
                        dataInfo_2 += '、';
                    }
                    dataInfo_2 += srfmajortext;
                }
                if (firstOnly_1) {
                    return false;
                }
            });
            var data = { dataInfo: dataInfo_2 };
            data[paramkey] = paramitems_1;
            if (paramjo) {
                Object.assign(data, paramjo);
            }
            return data;
        }
        return {};
    };
    /**
     * 导出操作（Excel）
     *
     * @param {*} params
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.doExportExcel = function (params) {
        if (this.getMDCtrl()) {
            this.getMDCtrl().exportData(params);
        }
    };
    /**
     * 表格行数据变化
     *
     * @param {string} name
     * @param {*} [data={}]
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.onGridRowChanged = function (name, data) {
        if (data === void 0) { data = {}; }
    };
    /**
     * 多数据项界面_导出数据模型
     *
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizGridViewController
     */
    IBizGridViewController.prototype.doExportModel = function (params) {
        if (params === void 0) { params = {}; }
        var arg = {};
        if (this.getParentMode()) {
            Object.assign(arg, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(arg, this.getParentData());
        }
        if (!arg.srfkeys) {
            // 获取要删除的数据集合
            var selectedData = this.getGrid().getSelection();
            if (!selectedData || selectedData == null || selectedData.length === 0) {
                return;
            }
            var keys_2 = '';
            selectedData.forEach(function (record) {
                var key = record.srfkey;
                if (!Object.is(keys_2, '')) {
                    keys_2 += ';';
                }
                keys_2 += key;
            });
            arg.srfkeys = keys_2;
        }
        if (this.getMDCtrl()) {
            this.getMDCtrl().exportModel(arg);
        }
    };
    return IBizGridViewController;
}(IBizMDViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格视图控制器 嵌入
 *
 * @class IBizGridView2Controller
 * @extends {IBizGridViewController}
 */
var IBizGridView2Controller = /** @class */ (function (_super) {
    __extends(IBizGridView2Controller, _super);
    /**
     * Creates an instance of IBizGridView2Controller.
     * 创建 IBizGridView2Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGridView2Controller
     */
    function IBizGridView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 选中行数据
         *
         * @type {*}
         * @memberof IBizGridView2Controller
         */
        _this.selectItem = {};
        /**
         * 排序字段名称
         *
         * @memberof IBizGridView2Controller
         */
        _this.sortFieldName = '关键字';
        /**
         * 排序字段属性文本值
         *
         * @memberof IBizGridView2Controller
         */
        _this.sortField = '';
        /**
         * 属性字段排序方向
         *
         * @type {string}
         * @memberof IBizGridView2Controller
         */
        _this.sortDirection = 'desc';
        /**
         * 视图显示数据类型
         *
         * @memberof IBizGridView2Controller
         */
        _this.gridType = 'listgrid';
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.onInitComponents = function () {
        _super.prototype.onInitComponents.call(this);
        var listgrid = this.getControl('listgrid');
        if (listgrid) {
            // listgrid.on(IBizEvent.IBizMDControl_SELECTIONCHANGE, (args) => {
            //     this.onSelectionChange(args);
            // });
            // listgrid.on(IBizEvent.IBizMDControl_BEFORELOAD, (args) => {
            //     this.onStoreBeforeLoad(args);
            // });
            // listgrid.on(IBizEvent.IBizMDControl_LOADED, (args) => {
            //     this.onStoreLoad(args);
            // });
            // listgrid.on(IBizEvent.IBizMDControl_CHANGEEDITSTATE, (args) => {
            //     this.onGridRowEditChange(undefined, args, undefined);
            // });
            // listgrid.on(IBizEvent.IBizDataGrid_ROWDBLCLICK, (args) => {
            //     this.onDataActivated(args);
            // });
        }
    };
    /**
     * 视图部件加载
     *
     * @param {*} [opt={}]
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.load = function (opt) {
        if (opt === void 0) { opt = {}; }
        _super.prototype.load.call(this, opt);
        var listgrid = this.getControl('listgrid');
        if (listgrid) {
            listgrid.load(opt);
        }
    };
    /**
     * 部件加载完成
     *
     * @param {Array<any>} args
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.onStoreLoad = function (args) {
        var _this = this;
        _super.prototype.onStoreLoad.call(this, args);
        if (args.length > 0) {
            setTimeout(function () {
                _this.selectItem = {};
                Object.assign(_this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
            }, 1);
        }
    };
    /**
     * 选中值变化
     *
     * @param {any} selection
     * @memberof IBizMDViewController
     */
    IBizGridView2Controller.prototype.onSelectionChange = function (args) {
        if (args.length > 0) {
            this.selectItem = {};
            Object.assign(this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
        }
        _super.prototype.onSelectionChange.call(this, args);
    };
    /**
     * 数据列表排序
     *
     * @param {string} field
     * @param {string} name
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.dataListSort = function (field, name) {
        if (Object.is(this.sortField, field)) {
            return;
        }
        this.sortField = field;
        this.sortFieldName = name;
        if (Object.is(this.sortField, '')) {
            return;
        }
        var sort = [];
        sort.push({ property: this.sortField, direction: this.sortDirection });
        var listgrid = this.getControl('listgrid');
        listgrid.refresh({ sort: JSON.stringify(sort) });
    };
    /**
     * 视图类型切换
     *
     * @param {string} type
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.viewTypeChange = function (type) {
        this.gridType = type;
    };
    /**
     * 数据列表字段排序
     *
     * @memberof IBizGridView2Controller
     */
    IBizGridView2Controller.prototype.dataListFiledSort = function () {
        if (Object.is(this.sortField, '')) {
            return;
        }
        if (Object.is(this.sortDirection, 'desc')) {
            this.sortDirection = 'asc';
        }
        else {
            this.sortDirection = 'desc';
        }
        var sort = [];
        sort.push({ property: this.sortField, direction: this.sortDirection });
        var listgrid = this.getControl('listgrid');
        listgrid.refresh({ sort: JSON.stringify(sort) });
    };
    return IBizGridView2Controller;
}(IBizGridViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格视图控制器 (部件视图)
 *
 * @class IBizGridView9Controller
 * @extends {IBizGridViewController}
 */
var IBizGridView9Controller = /** @class */ (function (_super) {
    __extends(IBizGridView9Controller, _super);
    /**
     * Creates an instance of IBizGridView9Controller.
     * 创建 IBizGridView9Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGridView9Controller
     */
    function IBizGridView9Controller(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 选中行数据
         *
         * @type {*}
         * @memberof IBizGridView9Controller
         */
        _this.selectItem = {};
        return _this;
    }
    /**
     * 视图初始化，处理视图父数据，清除其他参数
     *
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.onInited = function () {
        _super.prototype.onInited.call(this);
        if (this.viewParam && this.viewParam.srfkey) {
            delete this.viewParam.srfkey;
        }
    };
    /**
     * 视图部件加载,不提供预加载方法
     *
     * @param {*} [opt={}]
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.load = function (opt) {
        if (opt === void 0) { opt = {}; }
    };
    /**
     * 部件加载完成
     *
     * @param {Array<any>} args
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.onStoreLoad = function (args) {
        var _this = this;
        _super.prototype.onStoreLoad.call(this, args);
        if (args.length > 0) {
            setTimeout(function () {
                _this.selectItem = {};
                Object.assign(_this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
            }, 1);
        }
    };
    /**
     * 选中值变化
     *
     * @param {Array<any>} args
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.onSelectionChange = function (args) {
        if (args.length > 0) {
            this.selectItem = {};
            Object.assign(this.selectItem, { srfparentkey: args[args.length - 1].srfkey });
        }
        _super.prototype.onSelectionChange.call(this, args);
    };
    /**
     * 获取UI操作参数
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.getFrontUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        var arg = {};
        var front_arg = _super.prototype.getFrontUIActionParam.call(this, uiaction, params);
        Object.assign(arg, front_arg);
        Object.assign(arg, this.getViewParam());
        return arg;
    };
    /**
     * 实体界面行为参数
     *
     * @param {*} [uiaction={}]
     * @returns {*}
     * @memberof IBizGridView9Controller
     */
    IBizGridView9Controller.prototype.getBackendUIActionParam = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        var arg = {};
        var back_arg = _super.prototype.getBackendUIActionParam.call(this, uiaction);
        Object.assign(arg, back_arg);
        Object.assign(arg, this.getViewParam());
        return arg;
    };
    return IBizGridView9Controller;
}(IBizGridViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多数据编辑视图控制器对象
 *
 * @class IBizMEditView9Controller
 * @extends {IBizMDViewController}
 */
var IBizMEditView9Controller = /** @class */ (function (_super) {
    __extends(IBizMEditView9Controller, _super);
    /**
     * Creates an instance of IBizMEditView9Controller.
     * 创建 IBizMEditView9Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMEditView9Controller
     */
    function IBizMEditView9Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化部件对象
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var meditviewpanel = this.getMDCtrl();
        if (meditviewpanel) {
            meditviewpanel.on(IBizMultiEditViewPanel.FINDEDITVIEWCONTROLLER).subscribe(function (data) {
            });
            //数据发生改变
            meditviewpanel.on(IBizMultiEditViewPanel.EDITVIEWCHANGED).subscribe(function (data) {
                _this.onDataChanged(data);
            });
            //数据保存完成
            meditviewpanel.on(IBizMultiEditViewPanel.DATASAVED).subscribe(function (data) {
                _this.onDataSaved();
            });
        }
    };
    /**
     * 刷新
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.load = function () {
    };
    /**
     * 加载
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onRefresh = function () {
        if (this.getMDCtrl) {
            this.getMDCtrl().refresh();
        }
    };
    /**
     * 获取多数据部件对象
     *
     * @returns {*}
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.getMDCtrl = function () {
        return this.getControl('meditviewpanel');
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.isEnableQuickSearch = function () {
        return false;
    };
    /**
     * 父页面触发保存
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.saveData = function () {
        if (this.getMDCtrl() && this.getMDCtrl().isDirty()) {
            this.getMDCtrl().doSave();
            return;
        }
        this.onDataSaved();
    };
    /**
     * 数据发生改变
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onDataChanged = function (data) {
        this.$vue.$emit('drDataChanged', data);
    };
    /**
     * 数据保存完成
     *
     * @memberof IBizMEditView9Controller
     */
    IBizMEditView9Controller.prototype.onDataSaved = function () {
        this.$vue.$emit('drDataSaved');
    };
    return IBizMEditView9Controller;
}(IBizMDViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 选择表格视图控制器（部件视图）
 *
 * @class IBizPickupGridViewController
 * @extends {IBizGridViewController}
 */
var IBizPickupGridViewController = /** @class */ (function (_super) {
    __extends(IBizPickupGridViewController, _super);
    /**
     * Creates an instance of IBizPickupGridViewController.
     * 创建 IBizPickupGridViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupGridViewController
     */
    function IBizPickupGridViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 父数据  <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizPickupGridViewController
         */
        _this.parentData = null;
        /**
         * 是否支持多项数据选择  <Input>
         *
         * @private
         * @type {boolean}
         * @memberof IBizPickupGridViewController
         */
        _this.multiselect = true;
        /**
         *  当前选择数据 <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizPickupGridViewController
         */
        _this.currentValue = null;
        /**
         * 删除数据 <Input>
         *
         * @private
         * @type {*}
         * @memberof IBizPickupGridViewController
         */
        _this.deleteData = null;
        /**
         * 数据选中事件 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizPickupGridViewController
         */
        _this.selectionChange = 'selectionChange';
        /**
         * 行数据激活事件 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizPickupGridViewController
         */
        _this.dataActivated = 'dataActivated';
        /**
         * 多数据部件加载所有数据 <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizPickupGridViewController
         */
        _this.allData = 'allData';
        return _this;
    }
    /**
     * 部件初始化完成
     *
     * @param {*} opt
     * @memberof IBizPickupGridViewController
     */
    IBizPickupGridViewController.prototype.onStoreLoad = function (opt) {
        _super.prototype.onStoreLoad.call(this, opt);
        if (this.multiselect && Array.isArray(opt)) {
            this.$vue.$emit(this.allData, opt);
        }
    };
    /**
     * 视图部件初始化完成
     *
     * @memberof IBizPickupGridViewController
     */
    IBizPickupGridViewController.prototype.onInited = function () {
        _super.prototype.onInited.call(this);
        var grid = this.getGrid();
        if (grid) {
            grid.setMultiSelect(this.multiselect);
        }
        var searchForm = this.getSearchForm();
        if (searchForm) {
            searchForm.setOpen(true);
        }
    };
    /**
     * 获取多数据对象
     *
     * @returns {*}
     * @memberof IBizGridViewController
     */
    IBizPickupGridViewController.prototype.getMDCtrl = function () {
        return this.getControl('grid');
    };
    /**
     * 数据选择事件触发，提交选中数据
     *
     * @param {Array<any>} selection
     * @memberof IBizPickupGridViewController
     */
    IBizPickupGridViewController.prototype.onSelectionChange = function (selection) {
        this.$vue.$emit(this.selectionChange, selection);
    };
    /**
     * 数据被激活<最典型的情况就是行双击>
     *
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizPickupGridViewController
     */
    IBizPickupGridViewController.prototype.onDataActivated = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.onDataActivated.call(this, data);
        if (Object.keys(data).length === 0) {
            return;
        }
        this.$vue.$emit(this.dataActivated, [data]);
    };
    return IBizPickupGridViewController;
}(IBizGridViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 编辑视图控制器
 *
 * @class IBizEditViewController
 * @extends {IBizMainViewController}
 */
var IBizEditViewController = /** @class */ (function (_super) {
    __extends(IBizEditViewController, _super);
    /**
     * Creates an instance of IBizEditViewController.
     * 创建IBizEditViewController实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditViewController
     */
    function IBizEditViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 表单视图数据标题信息
         *
         * @type {string}
         * @memberof IBizEditViewController
         */
        _this.dataInfo = '';
        /**
         * 表单保存后操作行为
         *
         * @private
         * @type {string}
         * @memberof IBizEditViewController
         */
        _this.afterformsaveaction = '';
        /**
         * 最后的工作流实体界面行为
         *
         * @private
         * @type {*}
         * @memberof IBizEditViewController
         */
        _this.lastwfuiaction = {};
        /**
         * 最后工作流操作参数
         *
         * @private
         * @type {*}
         * @memberof IBizEditViewController
         */
        _this.lastwfuaparam = {};
        /**
         * 当表单保存后抛出
         *
         * @private
         * @type {String}
         * @memberof IBizEditViewController
         */
        _this.onEditFormSaved = 'onEditFormSaved';
        /**
         * 当表单保存后抛出
         *
         * @private
         * @type {String}
         * @memberof IBizEditViewController
         */
        _this.onEditFormFieldChanged = 'onEditFormFieldChanged';
        return _this;
    }
    /**
     * 初始化表单
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var form = this.getForm();
        if (form) {
            // 表单加载之前
            form.on(IBizEditForm.BEFORELOAD).subscribe(function (data) {
                _this.onFormBeforeLoad(data);
            });
            // 表单保存之前
            form.on(IBizEditForm.FORMBEFORESAVE).subscribe(function (data) {
                _this.onFormBeforeSaved(data);
            });
            // 表单保存完成
            form.on(IBizForm.FORMSAVED).subscribe(function (data) {
                _this.onFormSaved(data);
            });
            // 表单加载完成
            form.on(IBizForm.FORMLOADED).subscribe(function (data) {
                _this.onFormLoaded();
            });
            // 表单删除完成
            form.on(IBizForm.FORMREMOVED).subscribe(function (data) {
                _this.onFormRemoved();
            });
            // 工作流启动完成
            form.on(IBizForm.FORMWFSTARTED).subscribe(function (data) {
                _this.onFormWFStarted();
            });
            // 工作流提交完成
            form.on(IBizForm.FORMWFSUBMITTED).subscribe(function (data) {
                _this.onFormWFSubmitted();
            });
            // 编辑表单实体界面行为
            form.on(IBizEditForm.UIACTIONFINISHED).subscribe(function (data) {
                if (data.reloadData) {
                    _this.refreshReferView();
                }
                if (data.closeEditview) {
                    _this.closeWindow();
                }
            });
            // 表单属性值变化
            form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                if (data == null) {
                    _this.onFormFieldChanged('', null, null);
                }
                else {
                    _this.$vue.$emit(_this.onEditFormFieldChanged, { name: data.name, value: data.value });
                    _this.onFormFieldChanged(data.name, data.field, data.value);
                    _this.onFormFieldValueCheck(data.name, data.field.getValue());
                }
            });
            // 表单权限发生变化
            form.on(IBizForm.DATAACCACTIONCHANGE).subscribe(function (data) {
                _this.onDataAccActionChange(data);
            });
        }
    };
    /**
     * 加载数据
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        var editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    };
    /**
     *
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.addEditMenu = function () {
    };
    /**
     * 判断表单是否修改了
     *
     * @returns {boolean}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.unloaded = function () {
        // 判断表单是否修改了
        // if (this.form.isDirty()) {
        //     return IGM('EDITVIEWCONTROLLER.UNLOADED.INFO', '表单已经被修改是否关闭');
        // }
        return false;
    };
    /**
     * 表单权限发生变化
     *
     * @param {*} dataaccaction
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onDataAccActionChange = function (dataaccaction) {
        if (this.getToolBar()) {
            this.getToolBar().updateAccAction(this.getToolBar().getItems(), dataaccaction);
        }
        // if (this.getToolbar())
        //     this.getToolbar().updateAccAction(dataaccaction);
        // if (this.mintoolbar)
        //     this.mintoolbar.updateAccAction(dataaccaction);
        // if (this.floattoolbar)
        //     this.floattoolbar.updateAccAction(dataaccaction);
    };
    /**
     * 设置父数据
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onSetParentData = function () {
        // if (this.isInited() == true) {
        //     if (this.parentData) {
        //         var params = .extend(this.viewparam, this.parentData);
        //         this.form.autoLoad(params);
        //     }
        // }
    };
    /**
     * 获取表单对象
     *
     * @returns {*}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.getForm = function () {
        return this.getControl('form');
    };
    /**
     * 获取数据信息区对象
     *
     * @returns {*}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.getDataInfoBar = function () {
        // return this.datainfobar;
        return;
    };
    /**
     * 表单加载之前，处理视图数据
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormBeforeLoad = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (this.getViewParam()) {
            Object.assign(arg, this.getViewParam());
        }
        if (this.getParentData()) {
            Object.assign(arg, this.getParentData());
        }
        if (this.getParentMode()) {
            Object.assign(arg, this.getParentMode());
        }
    };
    /**
     * 表单保存之前，处理视图数据
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormBeforeSaved = function (arg) {
        if (arg === void 0) { arg = {}; }
        var data = {};
        Object.assign(data, this.getViewParam(), arg);
        Object.assign(arg, data);
    };
    /**
     * 表单保存完成
     *
     * @param {*} [result={}]
     * @returns {void}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormSaved = function (result) {
        if (result === void 0) { result = {}; }
        this.refreshReferView();
        if (Object.is(this.afterformsaveaction, 'exit')) {
            // var window = this.getWindow();
            // if (window) {
            //     window.dialogResult = 'ok';
            //     window.activeData = this.getForm().getValues();
            // }
            if (this.isModal()) {
                var result_1 = { ret: 'OK', activeData: this.getForm().getValues() };
                this.closeModal(result_1);
                return;
            }
            this.closeWindow();
            return;
        }
        if (Object.is(this.afterformsaveaction, 'new')) {
            var arg = this.getViewParam();
            if (!arg) {
                arg = {};
            }
            this.getForm().loadDraft(arg);
            return;
        }
        if (Object.is(this.afterformsaveaction, 'dowfuiaction')) {
            this.afterformsaveaction = 'dowfuiactionok';
            this.doWFUIAction(this.lastwfuiaction, this.lastwfuaparam);
            return;
        }
        if (Object.is(this.afterformsaveaction, 'startwf')) {
            this.startWF();
        }
        else {
            // 判断是否已经出现过提示
            if (!result || !result.info) {
                // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.INFO', '信息'), IGM('EDITVIEWCONTROLLER.ONFORMSAVED.INFO', '数据保存成功！'), 1);
                this.iBizNotification.success('信息', '数据保存成功！');
            }
        }
        this.updateViewInfo();
        this.$vue.$emit(this.onEditFormSaved, result.data);
    };
    /**
     * 表单加载完成
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormLoaded = function () {
        this.updateViewInfo();
    };
    /**
     * 工作流表单启动完成
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormWFStarted = function () {
        this.refreshReferView();
        this.closeWindow();
    };
    /**
     * 工作流表单提交完成
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormWFSubmitted = function () {
        this.refreshReferView();
        if (this.isModal()) {
            var result = { ret: 'OK', activeData: this.getForm().getValues() };
            this.dataChange();
            this.closeModal(result);
        }
        else {
            this.closeWindow();
        }
    };
    /**
     * 更细视图caption信息
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.updateViewInfo = function () {
        var form = this.getForm();
        if (!form) {
            return;
        }
        var _srfuf = form.findField('srfuf');
        if (!_srfuf) {
            return;
        }
        var newdata = !Object.is(_srfuf.getValue(), '1');
        var dataAccAction = form.getdataaccaction();
        this.calcToolbarItemState(!newdata, dataAccAction);
        var info = '';
        if (newdata) {
            info = '新建';
        }
        else {
            var _srfmajortext = form.findField('srfmajortext');
            if (_srfmajortext) {
                info = _srfmajortext.getValue();
            }
        }
        var _StrInfo = info.replace(/[null]/g, '').replace(/[undefined]/g, '').replace(/[ ]/g, '');
        if (_StrInfo.length > (IBizEnvironment.STRINFOLENGTH > 0 ? IBizEnvironment.STRINFOLENGTH : 10)) {
            info = _StrInfo.substring(0, 10) + "...";
        }
        this.dataInfo = Object.is(info, '') ? '' : info;
    };
    /**
     * 表单删除完成
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormRemoved = function () {
        this.refreshReferView();
        this.closeWindow();
    };
    /**
     * 表单项更新
     *
     * @param {string} fieldname
     * @param {*} field
     * @param {string} value
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormFieldChanged = function (fieldname, field, value) {
    };
    /**
     * 表单项值检测
     *
     * @param {string} fieldname
     * @param {string} value
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onFormFieldValueCheck = function (fieldname, value) {
    };
    /**
     * 处理实体界面行为
     *
     * @param {*} [uiaction={}] 界面行为
     * @param {*} [params={}]  参数
     * @returns {void}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doDEUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.tag, 'Help')) {
            this.doHelp();
            return;
        }
        if (Object.is(uiaction.tag, 'SaveAndStart')) {
            this.doSaveAndStart();
            return;
        }
        if (Object.is(uiaction.tag, 'SaveAndExit')) {
            this.doSaveAndExit();
            return;
        }
        if (Object.is(uiaction.tag, 'SaveAndNew')) {
            this.doSaveAndNew();
            return;
        }
        if (Object.is(uiaction.tag, 'Save')) {
            this.doSave();
            return;
        }
        if (Object.is(uiaction.tag, 'Print')) {
            this.doPrint();
            return;
        }
        if (Object.is(uiaction.tag, 'Copy')) {
            this.doCopy();
            return;
        }
        if (Object.is(uiaction.tag, 'RemoveAndExit')) {
            this.doRemoveAndExit();
            return;
        }
        if (Object.is(uiaction.tag, 'Refresh')) {
            this.doRefresh();
            return;
        }
        if (Object.is(uiaction.tag, 'New')) {
            this.doNew();
            return;
        }
        if (Object.is(uiaction.tag, 'FirstRecord')) {
            this.doMoveToRecord('first');
            return;
        }
        if (Object.is(uiaction.tag, 'PrevRecord')) {
            this.doMoveToRecord('prev');
            return;
        }
        if (Object.is(uiaction.tag, 'NextRecord')) {
            this.doMoveToRecord('next');
            return;
        }
        if (Object.is(uiaction.tag, 'LastRecord')) {
            this.doMoveToRecord('last');
            return;
        }
        if (Object.is(uiaction.tag, 'Exit') || Object.is(uiaction.tag, 'Close')) {
            this.doExit();
            return;
        }
        _super.prototype.doDEUIAction.call(this, uiaction, params);
    };
    /**
     * 编辑界面_实体帮助界面操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doHelp = function () {
        // IBiz.alert(IGM('IBIZAPP.CONFIRM.TITLE.INFO', '信息'), IGM('EDITVIEWCONTROLLER.DOHELP.INFO', '编辑界面_帮助操作！'), 5);
        this.iBizNotification.info('信息', '编辑界面_帮助操作！');
    };
    /**
     * 编辑界面_保存并启动工作流操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doSaveAndStart = function () {
        this.afterformsaveaction = 'startwf';
        this.saveData({ 'postType': 'startwf' });
    };
    /**
     * 编辑界面_保存并退出操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doSaveAndExit = function () {
        this.afterformsaveaction = 'exit';
        var window = this.getWindow();
        // if (window) {
        //     window.dialogResult = 'cancel';
        // }
        this.saveData({});
    };
    /**
     * 编辑界面_保存并新建操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doSaveAndNew = function () {
        this.afterformsaveaction = 'new';
        this.saveData({});
    };
    /**
     * 编辑界面_保存操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doSave = function () {
        this.afterformsaveaction = '';
        this.saveData({});
    };
    /**
     * 编辑界面_打印操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doPrint = function () {
        var arg = {};
        var field = this.getForm().findField('srforikey');
        if (field) {
            Object.assign(arg, { srfkey: field.getValue() });
        }
        if (!arg.srfkey || Object.is(arg.srfkey, '')) {
            field = this.getForm().findField('srfkey');
            if (field) {
                Object.assign(arg, { srfkey: field.getValue() });
            }
        }
        if (Object.is(arg.srfkey, '')) {
            return;
        }
        this.onPrintData(arg);
    };
    /**
     * 编辑界面_拷贝操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doCopy = function () {
        var arg = {};
        Object.assign(arg, this.getViewParam());
        arg.srfkey = '';
        if (!this.getForm()) {
            return;
        }
        var srforikey = this.getForm().findField('srforikey');
        if (srforikey) {
            arg.srfsourcekey = srforikey.getValue();
        }
        if (!arg.srfsourcekey || Object.is(arg.srfsourcekey, '')) {
            var srfkey = this.getForm().findField('srfkey');
            if (srfkey) {
                arg.srfsourcekey = srfkey.getValue();
            }
        }
        if (!arg.srfsourcekey || Object.is(arg.srfsourcekey, '')) {
            this.iBizNotification.warning('警告', '当前表单未加载数据，不能拷贝');
            return;
        }
        this.getForm().autoLoad(arg);
    };
    /**
     * 编辑界面_删除并退出操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doRemoveAndExit = function () {
        var _this = this;
        this.iBizNotification.confirm('删除提示', '确认要删除当前数据，删除操作将不可恢复？').subscribe(function (result) {
            if (result && Object.is(result, 'OK')) {
                _this.removeData();
            }
        });
    };
    /**
     * 编辑界面_刷新操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doRefresh = function () {
        this.iBizNotification.info('信息', '编辑界面_刷新操作！');
    };
    /**
     * 编辑界面_新建操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doNew = function () {
        this.iBizNotification.info('信息', '编辑界面_新建操作！');
    };
    /**
     * 编辑界面_退出操作
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doExit = function () {
        this.closeWindow();
    };
    /**
     * 保存视图数据
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.saveData = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (!arg) {
            arg = {};
        }
        this.getForm().save2(arg);
    };
    /**
     * 删除视图数据
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.removeData = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (!arg) {
            arg = {};
        }
        this.getForm().remove(arg);
    };
    /**
     * 刷新关联数据
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.refreshReferView = function () {
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            var parentWindow = iBizApp.getParentWindow();
            var viewparam = this.getViewParam();
            if (parentWindow && viewparam.openerid && !Object.is(viewparam.openerid, '')) {
                try {
                    var pWinIBizApp = parentWindow.getIBizApp();
                    pWinIBizApp.fireRefreshView({ openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) });
                }
                catch (error) {
                    parentWindow.postMessage({ ret: 'OK', type: 'REFERVIEW', openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) }, '*');
                    // if (window && window.parent) {
                    //     let win = window;
                    // }
                }
            }
        }
        if (this.isModal()) {
            var result = { ret: 'OK', activeData: this.getForm().getValues() };
            this.dataChange(result);
        }
    };
    /**
     * 更新表单项
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.updateFormItems = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.getForm().updateFormItems(arg);
    };
    /**
     *
     *
     * @param {boolean} bShow
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.showCommandBar = function (bShow) {
        // var toolbar = this.getToolbar();
        // if (toolbar && (toolbar.isHidden() == bShow)) {
        //     if (bShow) {
        //         toolbar.show();
        //     } else toolbar.hide();
        // }
    };
    /**
     * 工作流实体界面行为
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doWFUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (this.isEnableEditData()) {
            if (!Object.is(this.afterformsaveaction, 'dowfuiactionok')) {
                this.afterformsaveaction = 'dowfuiaction';
                this.lastwfuiaction = uiaction;
                this.lastwfuaparam = params;
                this.saveData({});
                return;
            }
            this.afterformsaveaction = '';
            this.lastwfuiaction = null;
            this.lastwfuaparam = null;
        }
        if (Object.is(uiaction.actionmode, 'WFBACKEND')) {
            var arg = {
                srfwfiatag: uiaction.tag
            };
            this.getForm().wfsubmit(arg);
            return;
        }
        _super.prototype.doWFUIAction.call(this, uiaction, params);
    };
    /**
     * 启动工作流
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.startWF = function (arg) {
        if (arg === void 0) { arg = {}; }
        var startuiaction = this.getUIAction('WFStartWizard');
        if (startuiaction) {
            this.doUIAction(startuiaction, {});
        }
        else {
            this.getForm().wfstart(arg);
        }
    };
    /**
     *
     *
     * @param {*} target
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doMoveToRecord = function (target) {
        // var referView = this.getReferView();
        // if (referView && referView.moveRecord) {
        //     var record = referView.moveRecord(target);
        //     if (record) {
        //         var loadParam = {};
        //         .extend(loadParam, {
        //             srfkey: record.get('srfkey')
        //         });
        //         this.getForm().autoLoad(loadParam);
        //     }
        // }
    };
    /**
     * 执行后台界面行为
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doBackendUIAction = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.getForm().doUIAction(arg);
    };
    /**
     * 获取前台行为参数
     *
     * @param {*} [uiaction={}] 行为
     * @returns {*}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.getFrontUIActionParam = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        var arg = _super.prototype.getFrontUIActionParam.call(this, uiaction);
        if (Object.is(uiaction.actiontarget, 'SINGLEKEY') || Object.is(uiaction.actiontarget, 'MULTIKEY')) {
            var valueitem = 'srfkey';
            var paramkey = 'srfkeys';
            var paramjo = null;
            var paramitems = null;
            if (uiaction.actionparams) {
                var actionparams = uiaction.actionparams;
                valueitem = (actionparams.valueitem && !Object.is(actionparams.valueitem, '')) ? actionparams.valueitem.toLowerCase() : valueitem;
                paramkey = (actionparams.paramitem && !Object.is(actionparams.paramitem, '')) ? actionparams.paramitem.toLowerCase() : paramkey;
                paramjo = actionparams.paramjo ? actionparams.paramjo : {};
            }
            var field = this.getForm().findField('srforikey');
            if (field) {
                paramitems = field.getValue();
            }
            if (!paramitems || Object.is(paramitems, '')) {
                field = this.getForm().findField(valueitem);
                if (field) {
                    paramitems = field.getValue();
                }
            }
            var data = {};
            data[paramkey] = paramitems;
            if (paramjo) {
                Object.assign(data, paramjo);
            }
            return Object.assign(arg, data);
        }
        return arg;
    };
    /**
     * 获取后台行为参数
     *
     * @param {*} [uiaction={}] 行为
     * @returns {*}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.getBackendUIActionParam = function (uiaction) {
        if (uiaction === void 0) { uiaction = {}; }
        if (Object.is(uiaction.actiontarget, 'SINGLEKEY') || Object.is(uiaction.actiontarget, 'MULTIKEY')) {
            var valueitem = 'srfkey';
            var paramkey = 'srfkeys';
            var paramjo = null;
            var infoitem = 'srfmajortext';
            if (uiaction.actionparams) {
                var actionparams = uiaction.actionparams;
                valueitem = (actionparams.valueitem && !Object.is(actionparams.valueitem, '')) ? actionparams.valueitem.toLowerCase() : valueitem;
                paramkey = (actionparams.paramitem && !Object.is(actionparams.paramitem, '')) ? actionparams.paramitem.toLowerCase() : paramkey;
                infoitem = (actionparams.textitem && !Object.is(actionparams.textitem, '')) ? actionparams.textitem.toLowerCase() : infoitem;
                paramjo = actionparams.paramjo ? actionparams.paramjo : {};
            }
            var dataInfo = '';
            var keys = '';
            var field = this.getForm().findField(valueitem);
            if (field) {
                keys = field.getValue();
            }
            field = this.getForm().findField(infoitem);
            if (field) {
                dataInfo = field.getValue();
            }
            var data = { dataInfo: dataInfo };
            data[paramkey] = keys;
            if (paramjo) {
                Object.assign(data, paramjo);
            }
            var formData = this.getForm().getValues();
            if (formData.srfkey) {
                delete formData.srfkey;
            }
            return Object.assign(data, this.getForm().getValues());
        }
        return {};
    };
    /**
     * 初始化浮动工具栏
     *
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.initFloatToolbar = function () {
        // var offset = 60;
        // var duration = 300;
        // if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {  // ios supported
        //     (window).bind('touchend touchcancel touchleave', function (e) {
        //         if ((this).scrollTop() > offset) {
        //             ('.scroll-to-top').fadeIn(duration);
        //         } else {
        //             ('.scroll-to-top').fadeOut(duration);
        //         }
        //     });
        // } else {
        //     (window).scroll(function () {
        //         if ((this).scrollTop() > offset) {
        //             ('.scroll-to-top').fadeIn(duration);
        //         } else {
        //             ('.scroll-to-top').fadeOut(duration);
        //         }
        //     });
        // }
        // ('.scroll-to-top').click(function (e) {
        //     e.preventDefault();
        //     return false;
        // });
    };
    /**
     * 工作流前端实体界面后窗口关闭
     *
     * @param {*} win
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onWFUIFrontWindowClosed = function (win, data) {
        // if (win.dialogResult == 'ok') {
        //     var window = this.getWindow();
        //     if (window) {
        //         window.dialogResult = 'ok';
        //         window.activeData = this.getForm().getValues();
        //     }
        // this.refreshReferView();
        // this.closeWindow();
        //     return;
        // }
        if (data === void 0) { data = {}; }
        // if (win) {
        //     if (Object.is(win.ret, 'OK')) {
        //         // this.closeView();
        //     }
        // }
        this.refreshReferView();
        this.closeWindow();
    };
    /**
     * 是否启用新建数据
     *
     * @returns {boolean}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.isEnableNewData = function () {
        return true;
    };
    /**
     * 是否启用编辑数据
     *
     * @returns {boolean}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.isEnableEditData = function () {
        return true;
    };
    /**
     * 是否启用删除数据
     *
     * @returns {boolean}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.isEnableRemoveData = function () {
        return true;
    };
    /**
     * 打印数据
     *
     * @param {*} [arg={}]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onPrintData = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.doPrintDataNormal(arg);
    };
    /**
     * 打印常规数据
     *
     * @param {*} [arg={}]
     * @returns {boolean}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.doPrintDataNormal = function (arg) {
        if (arg === void 0) { arg = {}; }
        var view = this.getPrintDataView(arg);
        if (!view) {
            return false;
        }
        var viewurl = view.viewurl;
        if (!viewurl || Object.is(viewurl, '')) {
            viewurl = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.PDFPrint;
        }
        var viewparams_keys = Object.keys(view.viewparam);
        var _viewparam = [];
        viewparams_keys.forEach(function (key) {
            if (view.viewparam[key]) {
                _viewparam.push(key + "=" + view.viewparam[key]);
            }
        });
        viewurl = viewurl + (viewurl.indexOf('?') == -1 ? "?" + _viewparam.join('&') : _viewparam.join('&'));
        window.open(viewurl, '_blank');
        return true;
    };
    /**
     * 获取打印数据
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.getPrintDataView = function (arg) {
        // return null;
        if (arg === void 0) { arg = {}; }
        return undefined;
    };
    /**
     * 视图数据刷新
     *
     * @param {*} [uiaction]
     * @memberof IBizEditViewController
     */
    IBizEditViewController.prototype.onRefresh = function (uiaction) {
        var form = this.getForm();
        if (form) {
            form.reload();
        }
        if (uiaction && uiaction.reload) {
            this.refreshReferView();
        }
    };
    return IBizEditViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 左右编辑视图控制器
 *
 * @export
 * @class IBizEditView2Controller
 * @extends {IBizEditViewController}
 */
var IBizEditView2Controller = /** @class */ (function (_super) {
    __extends(IBizEditView2Controller, _super);
    /**
     * Creates an instance of IBizEditView2Controller.
     * 创建IBizEditView2Controller实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditView2Controller
     */
    function IBizEditView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化部件
     *
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var drBar = this.getDRBar();
        if (drBar) {
            // 关系数据部件加载完成
            drBar.on(IBizDRBar.DRBARLOADED).subscribe(function (item) {
                _this.onDRBarLoaded(item);
            });
            // 关系部件选中变化
            drBar.on(IBizDRBar.DRBARSELECTCHANGE).subscribe(function (item) {
                _this.onDRBarItemChangeSelect(item);
            });
        }
    };
    /**
     * 视图加载,只触发关系栏加载
     *
     * @returns {void}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onLoad = function () {
        var drBar = this.getDRBar();
        if (drBar) {
            drBar.load();
        }
    };
    /**
     * 关系数据项加载完成，触发视图加载
     *
     * @private
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onDRBarLoaded = function (items) {
        if (!this.getForm()) {
            this.iBizNotification.error('错误', '表单不存在!');
        }
        _super.prototype.onLoad.call(this);
    };
    /**
     * 表单部件加载完成
     *
     * @param {any} params
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onFormLoaded = function () {
        _super.prototype.onFormLoaded.call(this);
        var drBar = this.getDRBar();
        var form = this.getForm();
        var _field = form.findField('srfkey');
        var _srfuf = form.findField('srfuf');
        var matched = this.$route.matched;
        var formState = (Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '')) ? 'new' : 'edit';
        drBar.setDRBarItemState(drBar.getItems(), Object.is(formState, 'new') ? true : false);
        drBar.setSelectItem({ id: 'form' });
        if (this.isHideEditForm()) {
            if (Object.is(formState, 'new')) {
                this.iBizNotification.warning('警告', '新建模式，表单主数据不存在，该表单已被配置隐藏');
                return;
            }
        }
        if (matched[this.route_index + 1]) {
            var nextItem = drBar.getItem(drBar.getItems(), { routepath: matched[this.route_index + 1].name });
            if (nextItem && Object.keys(nextItem).length) {
                this.isShowToolBar = false;
                drBar.setSelectItem(nextItem);
            }
        }
    };
    /**
     * 表单保存完成
     *
     * @param {*} result
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onFormSaved = function (result) {
        _super.prototype.onFormSaved.call(this, result);
        var drBar = this.getDRBar();
        drBar.setDRBarItemState(drBar.getItems(), false);
    };
    /**
     * 是否影藏编辑表单
     *
     * @returns {boolean}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.isHideEditForm = function () {
        return false;
    };
    /**
     * 关联数据更新
     *
     * @returns {void}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.updateViewInfo = function () {
        _super.prototype.updateViewInfo.call(this);
        var form = this.getForm();
        if (!form) {
            return;
        }
        var field = form.findField('srfkey');
        if (!field) {
            return;
        }
        var keyvalue = field.getValue();
        var srforikey = form.findField('srforikey');
        if (field) {
            var keyvalue2 = field.getValue();
            if (keyvalue2 && !Object.is(keyvalue2, '')) {
                keyvalue = keyvalue2;
            }
        }
        var deid = '';
        var deidfield = form.findField('srfdeid');
        if (deidfield) {
            deid = deidfield.getValue();
        }
        var parentData = { srfparentkey: keyvalue };
        if (!Object.is(deid, '')) {
            parentData.srfparentdeid = deid;
        }
        if (this.getDRBar()) {
            this.getDRBar().setParentData(parentData);
        }
    };
    /**
     * 数据项选中变化
     *
     * @private
     * @param {*} [item={}]
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.onDRBarItemChangeSelect = function (item) {
        if (item === void 0) { item = {}; }
        var _isShowToolBar = Object.is(item.id, 'form') ? true : false;
        this.isShowToolBar = _isShowToolBar;
        this.setDRBarSelect(item);
        if (_isShowToolBar) {
            this.$router.push({ path: this.route_url });
        }
        else {
            this.openView(item.routepath, item.viewparam);
        }
    };
    /**
     * 获取数据关系部件
     *
     * @returns {any}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.getDRBar = function () {
        return this.getControl('drbar');
    };
    /**
     * 设置数据关系部件选中
     *
     * @param {*} [item={}]
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.setDRBarSelect = function (item) {
        if (item === void 0) { item = {}; }
        if (this.getDRBar()) {
            this.getDRBar().setSelectItem(item);
        }
    };
    /**
     * 获取关系视图
     *
     * @param {*} [view={}]
     * @returns {*}
     * @memberof IBizEditView2Controller
     */
    IBizEditView2Controller.prototype.getDRItemView = function (view) {
        if (view === void 0) { view = {}; }
    };
    return IBizEditView2Controller;
}(IBizEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 分页编辑视图
 *
 * @class IBizEditView3Controller
 * @extends {IBizEditViewController}
 */
var IBizEditView3Controller = /** @class */ (function (_super) {
    __extends(IBizEditView3Controller, _super);
    /**
     * Creates an instance of IBizEditView3Controller.
     * 创建 IBizEditView3Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditView3Controller
     */
    function IBizEditView3Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图部件初始化，注册所有事件
     *
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var drTab = this.getDRTab();
        if (drTab) {
            // 分页导航选中
            drTab.on(IBizDRTab.SELECTCHANGE).subscribe(function (data) {
                _this.doDRTabSelectChange(data);
            });
        }
    };
    /**
     * 表单加载完成
     *
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.onFormLoaded = function () {
        _super.prototype.onFormLoaded.call(this);
        this.setDrTabState();
        var drtab = this.getDRTab();
        var form = this.getForm();
        var _field = form.findField('srfkey');
        var _srfuf = form.findField('srfuf');
        var tab = {};
        if (this.isHideEditForm()) {
            if (Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '')) {
                this.iBizNotification.warning('警告', '新建模式，表单主数据不存在，该表单已被配置隐藏');
                return;
            }
        }
        if (form.findField('srfkey') && !Object.is(form.findField('srfkey').getValue(), '')) {
            Object.assign(tab, this.getActivatedDRTab());
        }
        if (Object.keys(tab).length) {
            drtab.setActiveTab(tab);
        }
    };
    /**
     * 表单保存完成
     *
     * @param {*} result
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.onFormSaved = function (result) {
        _super.prototype.onFormSaved.call(this, result);
        this.setDrTabState();
    };
    /**
     * 是否隐藏编辑表单
     *
     * @returns {boolean}
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.isHideEditForm = function () {
        return false;
    };
    /**
     * 视图信息更新
     *
     * @returns {void}
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.updateViewInfo = function () {
        _super.prototype.updateViewInfo.call(this);
        var form = this.getForm();
        if (!form) {
            return;
        }
        var field = form.findField('srfkey');
        if (!field) {
            return;
        }
        var keyvalue = field.getValue();
        var srforikey = form.findField('srforikey');
        if (field) {
            var keyvalue2 = field.getValue();
            if (keyvalue2 && !Object.is(keyvalue2, '')) {
                keyvalue = keyvalue2;
            }
        }
        var deid = '';
        var deidfield = form.findField('srfdeid');
        if (deidfield) {
            deid = deidfield.getValue();
        }
        var parentData = { srfparentkey: keyvalue };
        if (!Object.is(deid, '')) {
            parentData.srfparentdeid = deid;
        }
        if (this.getDRTab()) {
            this.getDRTab().setParentData(parentData);
        }
    };
    /**
     * 关系分页部件选择变化处理
     *
     * @param {*} [data={}]
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.doDRTabSelectChange = function (data) {
        if (data === void 0) { data = {}; }
        var _isShowToolBar = Object.is(data.viewid, 'form') ? true : false;
        this.isShowToolBar = _isShowToolBar;
        ;
        this.setActiveTab(data);
        if (_isShowToolBar) {
            this.$router.push({ path: this.route_url });
        }
        else {
            this.openView(data.routepath, data.viewparam);
        }
    };
    /**
     * 获取关系视图参数
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.getDRItemView = function (arg) {
        if (arg === void 0) { arg = {}; }
    };
    /**
     * 刷新视图
     *
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.onRefresh = function () {
        if (this.getDRTab() && this.getDRTab().refresh && this.getDRTab().refresh instanceof Function) {
            this.getDRTab().refresh();
        }
    };
    /**
     * 获取关系分页部件
     *
     * @returns {*}
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.getDRTab = function () {
        return this.getControl('drtab');
    };
    /**
     * 获取激活关系分页
     *
     * @private
     * @returns {*}
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.getActivatedDRTab = function () {
        var _tab = {};
        var matched = this.$route.matched;
        var drTab = this.getDRTab();
        if (matched[1]) {
            var next_route_name = matched[1].name;
            var tab = drTab.getTab(null, next_route_name);
            if (tab) {
                this.isShowToolBar = false;
                Object.assign(_tab, tab);
            }
        }
        else {
            var tab = drTab.getTab('form');
            if (tab) {
                this.isShowToolBar = true;
                Object.assign(_tab, tab);
            }
        }
        return _tab;
    };
    /**
     * 设置关系分页状态
     *
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.setDrTabState = function () {
        var form = this.getForm();
        var _field = form.findField('srfkey');
        var _srfuf = form.findField('srfuf');
        var state = Object.is(_srfuf.getValue(), '0') && Object.is(_field.getValue(), '');
        if (this.getDRTab()) {
            this.getDRTab().setTabState(state);
        }
    };
    /**
     * 设置激活分页
     *
     * @param {*} [tab={}]
     * @memberof IBizEditView3Controller
     */
    IBizEditView3Controller.prototype.setActiveTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        if (this.getDRTab()) {
            this.getDRTab().setActiveTab(tab);
        }
    };
    return IBizEditView3Controller;
}(IBizEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 嵌入编辑视图控制器
 *
 * @class IBizEditView9Controller
 * @extends {IBizEditViewController}
 */
var IBizEditView9Controller = /** @class */ (function (_super) {
    __extends(IBizEditView9Controller, _super);
    /**
     * Creates an instance of IBizEditView9Controller.
     * 创建 IBizEditView9Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEditView9Controller
     */
    function IBizEditView9Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图刷新
     *
     * @memberof IBizEditView9Controller
     */
    IBizEditView9Controller.prototype.onRefresh = function () {
        var editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    };
    /**
     * 视图参数变化，嵌入表单，手动刷新数据
     *
     * @param {*} change
     * @memberof IBizEditView9Controller
     */
    IBizEditView9Controller.prototype.viewParamChange = function (change) {
        if (change.srfparentkey && !Object.is(change.srfparentkey, '')) {
            this.addViewParam({ srfkey: change.srfparentkey });
            this.onRefresh();
        }
    };
    return IBizEditView9Controller;
}(IBizEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 实体选项操作视图
 *
 * @class IBizOptionViewController
 * @extends {IBizEditViewController}
 */
var IBizOptionViewController = /** @class */ (function (_super) {
    __extends(IBizOptionViewController, _super);
    /**
     * Creates an instance of IBizOptionViewController.
     * 创建 IBizOptionViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizOptionViewController
     */
    function IBizOptionViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 确定
     *
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onClickOkButton = function () {
        this.doSaveAndExit();
    };
    /**
     * 关闭串口
     *
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onClickCancelButton = function () {
        // var me = this;
        // var window = me.window;
        // if(window){
        // 	window.dialogResult = 'cancel';
        // }
        // me.closeWindow();
        // this.closeView();
        this.viewParam = {};
        this.closeWindow();
    };
    /**
     * 视图加载
     *
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onLoad = function () {
        var editForm = this.getForm();
        if (editForm) {
            editForm.autoLoad(this.getViewParam());
        }
    };
    /**
     * 表单保存完成
     *
     * @returns {void}
     * @memberof IBizOptionViewController
     */
    IBizOptionViewController.prototype.onFormSaved = function (data) {
        // var me = this;
        // var window = me.window;
        // if (window) {
        // 	window.dialogResult = 'ok';
        // 	window.activeData = me.getForm().getValues();
        // 	window.selectedData = window.activeData
        // }
        // me.closeWindow();
        this.refreshReferView();
        // this.closeView();
        this.viewParam = {};
        if (data.url && !Object.is(data.url, '')) {
            this.addViewParam({ ru: data.url });
        }
        this.closeWindow();
        return;
    };
    return IBizOptionViewController;
}(IBizEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 单项选择视图控制器
 *
 * @class IBizPickupViewController
 * @extends {IBizMainViewController}
 */
var IBizPickupViewController = /** @class */ (function (_super) {
    __extends(IBizPickupViewController, _super);
    /**
     * Creates an instance of IBizPickupViewController.
     * 创建 IBizPickupViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupViewController
     */
    function IBizPickupViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 按钮文本--确定
         *
         * @type {string}
         * @memberof IBizPickupViewController
         */
        _this.okBtnText = '确定';
        /**
         * 按钮文本--取消
         *
         * @type {string}
         * @memberof IBizPickupViewController
         */
        _this.cancelBtnText = '取消';
        /**
         * 是否是iframe嵌入视图
         *
         * @private
         * @type {boolean}
         * @memberof IBizPickupViewController
         */
        _this.srfembed = false;
        /**
         * 父视图id
         *
         * @private
         * @type {string}
         * @memberof IBizPickupViewController
         */
        _this.parentOpenerid = '';
        /**
         * 是否选中
         *
         * @type {boolean}
         * @memberof IBizPickupViewController
         */
        _this.isSelect = false;
        return _this;
    }
    /**
     * 视图部件初始化
     *
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var pickupViewPanel = this.getPickupViewPanel();
        if (pickupViewPanel) {
            // 选择视图面板数据选中
            pickupViewPanel.on(IBizPickupViewPanel.SELECTIONCHANGE).subscribe(function (args) {
                _this.onSelectionChange(args);
            });
            // 选择视图面板数据激活
            pickupViewPanel.on(IBizPickupViewPanel.DATAACTIVATED).subscribe(function (args) {
                _this.onDataActivated(args);
            });
        }
    };
    /**
     * 准备视图参数
     *
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.parseViewParams = function () {
        _super.prototype.parseViewParams.call(this);
        var viewparams = this.getViewParam();
        if (viewparams.hasOwnProperty('srfembed')) {
            this.srfembed = Object.is(viewparams.srfembed, 'true') ? true : false;
        }
        if (viewparams.hasOwnProperty('openerid')) {
            this.parentOpenerid = viewparams.openerid;
        }
    };
    /**
     * 数据选择，确定功能
     *
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.onClickOkButton = function () {
        var pickupViewPanel = this.getPickupViewPanel();
        if (!pickupViewPanel) {
            return;
        }
        if (pickupViewPanel.getSelections().length !== 1) {
            return;
        }
        // this.nzModalSubject.next({ ret: 'OK', selection: pickupViewPanel.getSelections() });
        // this.nzModalSubject.next('DATACHANGE');
        // this.closeWindow();
        if (this.srfembed && window && window.parent && !Object.is(this.parentOpenerid, '')) {
            var win = window;
            win.parent.postMessage({ ret: 'OK', type: 'SETSELECTIONS', selections: pickupViewPanel.getSelections(), openerid: this.parentOpenerid }, '*');
            return;
        }
        this.closeModal({ ret: 'OK', selections: pickupViewPanel.getSelections() });
    };
    /**
     * 取消显示选择视图
     *
     * @param {string} type
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.onClickCancelButton = function (type) {
        // this.nzModalSubject.destroy(type);
        if (this.srfembed && window && window.parent && !Object.is(this.parentOpenerid, '')) {
            var win = window;
            win.parent.postMessage({ ret: 'OK', type: 'SETSELECTIONS', selections: null, openerid: this.parentOpenerid }, '*');
            return;
        }
        this.closeModal();
    };
    /**
     * 接收选择视图数据传递
     *
     * @param {Array<any>} args
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.onSelectionChange = function (args) {
        this.isSelect = args.length > 0 ? true : false;
    };
    /**
     * 数据选中激活
     *
     * @param {Array<any>} args
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.onDataActivated = function (args) {
        this.onSelectionChange(args);
        this.onClickOkButton();
    };
    /**
     * 获取选择视图面板
     *
     * @returns {*}
     * @memberof IBizPickupViewController
     */
    IBizPickupViewController.prototype.getPickupViewPanel = function () {
        return this.getControl('pickupviewpanel');
    };
    return IBizPickupViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 左右关系单项选择视图控制器
 *
 * @class IBizPickupView2Controller
 * @extends {IBizPickupViewController}
 */
var IBizPickupView2Controller = /** @class */ (function (_super) {
    __extends(IBizPickupView2Controller, _super);
    /**
     * Creates an instance of IBizPickupView2Controller.
     * 创建 IBizPickupView2Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupView2Controller
     */
    function IBizPickupView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizPickupView2Controller;
}(IBizPickupViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多项数据选择视图控制器
 *
 * @class IBizMPickupViewController
 * @extends {IBizMainViewController}
 */
var IBizMPickupViewController = /** @class */ (function (_super) {
    __extends(IBizMPickupViewController, _super);
    /**
     * Creates an instance of IBizMPickupViewController.
     * 创建 IBizMPickupViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMPickupViewController
     */
    function IBizMPickupViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 按钮文本--确定
         *
         * @type {string}
         * @memberof IBizMPickupViewController
         */
        _this.okBtnText = '确定';
        /**
         * 按钮文本--取消
         *
         * @type {string}
         * @memberof IBizMPickupViewController
         */
        _this.cancelBtnText = '取消';
        /**
         * 是否是iframe嵌入视图
         *
         * @private
         * @type {boolean}
         * @memberof IBizMPickupViewController
         */
        _this.srfembed = false;
        /**
         * 父视图id
         *
         * @private
         * @type {string}
         * @memberof IBizMPickupViewController
         */
        _this.parentOpenerid = '';
        /**
         * 多项选择数据集服务对象
         *
         * @type {IBizMPickupResult}
         * @memberof IBizMPickupViewController
         */
        _this.MPickupResult = null;
        _this.MPickupResult = new IBizMPickupResult({
            name: 'mpickupresult',
            viewController: _this,
            url: opts.url,
        });
        _this.regControl('mpickupresult', _this.MPickupResult);
        return _this;
    }
    /**
     * 视图部件初始化
     *
     * @memberof IBizMPickupViewController
     */
    IBizMPickupViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var pickupViewPanel = this.getPickupViewPanel();
        if (pickupViewPanel && this.MPickupResult) {
            // 选择视图面板数据选中
            pickupViewPanel.on(IBizPickupViewPanel.SELECTIONCHANGE).subscribe(function (args) {
                _this.MPickupResult.setCurSelections(args);
            });
            // 选择视图面板数据激活
            pickupViewPanel.on(IBizPickupViewPanel.DATAACTIVATED).subscribe(function (args) {
                _this.MPickupResult.appendDatas(args);
            });
            // 选择视图面板所有数据
            pickupViewPanel.on(IBizPickupViewPanel.ALLDATA).subscribe(function (args) {
                _this.MPickupResult.setAllData(args);
            });
        }
    };
    /**
     * 准备视图参数
     *
     * @memberof IBizMPickupViewController
     */
    IBizMPickupViewController.prototype.parseViewParams = function () {
        _super.prototype.parseViewParams.call(this);
        var viewparams = this.getViewParam();
        if (viewparams.hasOwnProperty('srfembed')) {
            this.srfembed = Object.is(viewparams.srfembed, 'true') ? true : false;
        }
        if (viewparams.hasOwnProperty('openerid')) {
            this.parentOpenerid = viewparams.openerid;
        }
    };
    /**
     * 处理视图参数
     *
     * @memberof IBizMPickupViewController
     */
    IBizMPickupViewController.prototype.onInit = function () {
        _super.prototype.onInit.call(this);
        if (this.getViewParam() && Array.isArray(this.getViewParam().selectedData)) {
            if (this.MPickupResult) {
                this.MPickupResult.appendDatas(this.getViewParam().selectedData);
            }
        }
    };
    /**
     * 数据选择，确定功能
     *
     * @memberof IBizPickupViewController
     */
    IBizMPickupViewController.prototype.onClickOkButton = function () {
        if (this.MPickupResult.selections.length === 0) {
            return;
        }
        // this.nzModalSubject.next({ ret: 'OK', selection: this.MPickupResult.selections });
        // this.nzModalSubject.next('DATACHANGE');
        // this.closeWindow();
        if (this.srfembed && window && window.parent && !Object.is(this.parentOpenerid, '')) {
            var win = window;
            win.parent.postMessage({ ret: 'OK', type: 'SETSELECTIONS', selections: this.MPickupResult.selections, openerid: this.parentOpenerid }, '*');
            return;
        }
        this.closeModal({ ret: 'OK', selections: this.MPickupResult.selections });
    };
    /**
     * 关闭显示选择视图
     *
     * @param {*} type
     * @memberof IBizMPickupViewController
     */
    IBizMPickupViewController.prototype.onClickCancelButton = function (type) {
        // this.nzModalSubject.destroy(type);
        if (this.srfembed && window && window.parent && !Object.is(this.parentOpenerid, '')) {
            var win = window;
            win.parent.postMessage({ ret: 'OK', type: 'SETSELECTIONS', selections: null, openerid: this.parentOpenerid }, '*');
            return;
        }
        this.closeModal();
    };
    /**
     * 获取选中视图面板
     *
     * @returns {*}
     * @memberof IBizMPickupViewController
     */
    IBizMPickupViewController.prototype.getPickupViewPanel = function () {
        return this.getControl('pickupviewpanel');
    };
    return IBizMPickupViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 左右关系多项数据选择视图控制器
 *
 * @class IBizMPickupView2Controller
 * @extends {IBizMPickupViewController}
 */
var IBizMPickupView2Controller = /** @class */ (function (_super) {
    __extends(IBizMPickupView2Controller, _super);
    /**
     * Creates an instance of IBizMPickupView2Controller.
     * 创建 IBizMPickupView2Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMPickupView2Controller
     */
    function IBizMPickupView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizMPickupView2Controller;
}(IBizMPickupViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 树导航视图控制器
 *
 * @class IBizTreeExpViewController
 * @extends {IBizMainViewController}
 */
var IBizTreeExpViewController = /** @class */ (function (_super) {
    __extends(IBizTreeExpViewController, _super);
    /**
     * Creates an instance of IBizTreeExpViewController.
     * 创建 IBizTreeExpViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizTreeExpViewController
     */
    function IBizTreeExpViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         *
         *
         * @type {string}
         * @memberof IBizTreeExpViewController
         */
        _this.treeReloadMode = '';
        /**
         * 导航分页对象
         *
         * @type {IBizExpTabService}
         * @memberof IBizTreeExpViewController
         */
        _this.exptab = null;
        _this.exptab = new IBizExpTab({
            name: 'exptab',
            viewController: _this,
            url: opts.url,
        });
        _this.regControl('exptab', _this.exptab);
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var treeExpBar = this.getTreeExpBar();
        if (treeExpBar) {
            //  树导航选中
            treeExpBar.on(IBizTreeExpBar.SELECTIONCHANGE).subscribe(function (data) {
                _this.treeExpBarSelectionChange(data);
            });
        }
    };
    /**
     * 获取导航部件服务对象
     *
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getTreeExpBar = function () {
        return this.getControl('treeexpbar');
    };
    /**
     * 获取导航分页部件服务对象
     *
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getExpTab = function () {
        return this.getControl('exptab');
    };
    /**
     *
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doDEUIAction = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        this.treeReloadMode = '';
        if (Object.is(uiaction.tag, 'Remove')) {
            this.doRemove(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Refresh')) {
            this.doTreeRefresh(params);
            return;
        }
        if (Object.is(uiaction.tag, 'New')) {
            this.doNew(params);
            return;
        }
        if (Object.is(uiaction.tag, 'EDITDATA')) {
            this.doEdit(params);
            return;
        }
        if (Object.is(uiaction.tag, 'Copy')) {
            this.doCopy(params);
            return;
        }
        _super.prototype.doDEUIAction.call(this, uiaction, params);
    };
    /**
     * 新建操作
     *
     * @param {any} params
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doNew = function (params) {
        this.onNewData(params);
    };
    /**
     * 拷贝操作
     *
     * @param {any} params
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doCopy = function (params) {
        var arg = {
            data: params,
            srfcopymode: true
        };
        this.onEditData(arg);
    };
    /**
     * 编辑操作
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doEdit = function (params) {
        // 获取要编辑的数据集合
        if (params && params.srfkey) {
            var arg = { data: params };
            this.onEditData(arg);
            return;
        }
    };
    /**
     * 查看操作
     *
     * @param {any} params
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doView = function (params) {
        this.doEdit(params);
    };
    /**
     * 删除操作
     *
     * @param {*} params
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doRemove = function (params) {
        this.onRemove(params);
    };
    /**
     * 刷新操作
     *
     * @param {*} params
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doTreeRefresh = function (params) {
        this.onTreeRefresh(params);
    };
    /**
     * 新建数据
     *
     * @param {*} arg
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onNewData = function (arg) {
        this.treeReloadMode = IBizTreeExpViewController.REFRESHMODE_CURRENTNODE;
        var loadParam = {};
        if (this.getViewParam()) {
            Object.assign(loadParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(loadParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(loadParam, this.getParentData());
        }
        if (this.isEnableBatchAdd()) {
            this.doNewDataBatch(loadParam);
            return;
        }
        if (this.doNewDataWizard(loadParam)) {
            return;
        }
        var newMode = this.getNewMode(arg);
        if (newMode) {
            loadParam.srfnewmode = newMode;
        }
        this.doNewDataNormal(loadParam, arg);
    };
    /**
     * 批量新建
     *
     * @param {*} arg
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doNewDataBatch = function (arg) {
        return false;
    };
    /**
     * 批量新建关闭
     *
     * @param {*} win
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onMPickupWindowClose = function (win) {
    };
    /**
     * 批量添加数据
     *
     * @param {*} selectedDatas
     * @returns {string}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.addDataBatch = function (selectedDatas) {
        return '';
    };
    /**
     * 向导新建数据
     *
     * @param {*} arg
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doNewDataWizard = function (arg) {
        return false;
    };
    /**
     * 向导新建数据窗口关闭
     *
     * @param {any} win
     * @param {any} eOpts
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onNewDataWizardWindowClose = function (win, eOpts) {
        return;
    };
    /**
     * 常规新建数据
     *
     * @param {any} arg
     * @param {any} params
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doNewDataNormal = function (arg, params) {
        var view = this.getNewDataView(arg);
        if (view == null) {
            return false;
        }
        if (params && view.viewparam && view.viewparam.srfparenttype) {
            var parentType = view.viewparam.srfparenttype;
            if (Object.is(parentType, 'DER1N')) {
                view.viewparam.srfparentkey = params.srfkey;
            }
        }
        return this.openDataView(view);
    };
    /**
     * 编辑数据
     *
     * @param {any} arg
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onEditData = function (arg) {
        this.treeReloadMode = IBizTreeExpViewController.REFRESHMODE_PARENTNODE;
        var loadParam = {};
        if (this.getViewParam()) {
            Object.assign(loadParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(loadParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(loadParam, this.getParentData());
        }
        if (arg.srfcopymode) {
            Object.assign(loadParam, {
                srfsourcekey: arg.data.srfkey
            });
        }
        else {
            Object.assign(loadParam, { srfkey: arg.data.srfkey });
        }
        var editMode = this.getEditMode(arg.data);
        if (editMode) {
            loadParam.srfeditmode = editMode;
        }
        if (arg.data.srfmstag) {
            loadParam.srfeditmode2 = arg.data.srfmstag;
        }
        this.doEditDataNormal(loadParam);
    };
    /**
     * 执行常规编辑数据
     *
     * @param {any} arg
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doEditDataNormal = function (arg) {
        var view = this.getEditDataView(arg);
        if (view == null) {
            return false;
        }
        return this.openDataView(view);
    };
    /**
     * 打开数据视图
     *
     * @param {any} view
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.openDataView = function (view) {
        return true;
    };
    /**
     *
     *
     * @param {any} params
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onRemove = function (params) {
    };
    /**
     * 界面操作树节点刷新
     *
     * @param {any} params
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onTreeRefresh = function (params) {
    };
    /**
     * 视图刷新操作
     *
     * @returns {void}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.onRefresh = function () {
        var node;
        if (Object.is(this.treeReloadMode, IBizTreeExpViewController.REFRESHMODE_NONE)) {
            return;
        }
        else if (Object.is(this.treeReloadMode, IBizTreeExpViewController.REFRESHMODE_CURRENTNODE)) {
            var nodes = this.getSelected(true);
            if (nodes && nodes.length > 0) {
                node = nodes[0];
            }
        }
        else if (Object.is(this.treeReloadMode, IBizTreeExpViewController.REFRESHMODE_PARENTNODE)) {
            var nodes = this.getSelected(true);
            if (nodes && nodes.length > 0) {
                node = nodes[0].parent;
            }
        }
        // 刷新树节点
        // this.getTreeExpBar().getTree().reload(node);
    };
    /**
     *
     *
     * @param {any} bFull
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getSelected = function (bFull) {
        var nodes = this.getTreeExpBar().getTree().getSelected(bFull);
        return nodes;
    };
    /**
     * 获取新建模式
     *
     * @param {*} data
     * @returns {string}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getNewMode = function (data) {
        return 'NEWDATA@' + data.srfnodetype.toUpperCase();
    };
    /**
     * 获取编辑模式
     *
     * @param {*} data
     * @returns {string}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getEditMode = function (data) {
        return 'EDITDATA@' + data.srfnodetype.toUpperCase();
    };
    /**
     * 获取编辑视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getEditDataView = function (arg) {
        return this.getEditDataView(arg);
    };
    /**
     * 获取新建视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getNewDataView = function (arg) {
        return this.getNewDataView(arg);
    };
    /**
     * 获取新建向导视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getNewDataWizardView = function (arg) {
        return null;
    };
    /**
     * 获取多选视图
     *
     * @param {any} arg
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getMPickupView = function (arg) {
        return null;
    };
    /**
     *
     *
     * @param {any} arg
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.doBackendUIAction = function (arg) {
    };
    /**
     *
     *
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.isEnableBatchAdd = function () {
        return false;
    };
    /**
     *
     *
     * @returns {boolean}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.isBatchAddOnly = function () {
        return false;
    };
    /**
     *
     *
     * @param {*} [uiaction={}]
     * @param {*} [params={}]
     * @returns {*}
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.getBackendUIActionParam = function (uiaction, params) {
        if (uiaction === void 0) { uiaction = {}; }
        if (params === void 0) { params = {}; }
        if (Object.is(uiaction.actiontarget, 'SINGLEKEY') || Object.is(uiaction.actiontarget, 'MULTIKEY')) {
            var node = null;
            var keys = params.srfkey;
            var dataInfo = params.srfmajortext;
            var nodeType = params.srfnodetype;
            return { srfkeys: keys, srfkey: keys, dataInfo: dataInfo, srfnodetype: nodeType };
        }
        return {};
    };
    /**
     * 树导航部件选中变化
     *
     * @param {*} [data={}]
     * @memberof IBizTreeExpViewController
     */
    IBizTreeExpViewController.prototype.treeExpBarSelectionChange = function (data) {
        if (data === void 0) { data = {}; }
        if (!data || Object.keys(data).length === 0 || !data.viewItem) {
            return;
        }
        var viewParam = data.viewParam;
        this.openView(data.viewItem.routepath, viewParam);
    };
    IBizTreeExpViewController.REFRESHMODE_CURRENTNODE = 'CURRENTNODE';
    IBizTreeExpViewController.REFRESHMODE_PARENTNODE = 'PARENTNODE';
    IBizTreeExpViewController.REFRESHMODE_ALLNODE = 'ALLNODE';
    IBizTreeExpViewController.REFRESHMODE_NONE = 'NONE';
    return IBizTreeExpViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 导航视图控制器
 *
 * @class IBizExpViewController
 * @extends {IBizMainViewController}
 */
var IBizExpViewController = /** @class */ (function (_super) {
    __extends(IBizExpViewController, _super);
    /**
     * Creates an instance of IBizExpViewController.
     * 创建 IBizExpViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizExpViewController
     */
    function IBizExpViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化导航部件
     *
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var expCtrl = this.getExpCtrl();
        if (expCtrl) {
            // 导航节点选中
            expCtrl.on(IBizTreeExpBar.SELECTIONCHANGE).subscribe(function (item) {
                _this.onExpCtrlSelectionChange(item);
            });
            // 导航节点加载完成
            expCtrl.on(IBizTreeExpBar.LOADED).subscribe(function (item) {
                _this.onExpCtrlLoaded(item);
            });
        }
    };
    /**
     * 导航部件加载
     *
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        var expCtrl = this.getExpCtrl();
        if (expCtrl) {
            expCtrl.load({});
        }
    };
    /**
     * 获取导航部件
     *
     * @returns {*}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.getExpCtrl = function () {
        var expctrl = this.getExpBar();
        if (expctrl) {
            return expctrl;
        }
        expctrl = this.getExpTab();
        if (expctrl) {
            return expctrl;
        }
        return undefined;
    };
    /**
     * 获取导航部件
     *
     * @returns {*}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.getExpBar = function () {
        return this.getControl('expbar');
    };
    /**
     * 获取导航分页部件
     *
     * @returns {*}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.getExpTab = function () {
        return this.getControl('exptab');
    };
    /**
     * 导航部件值选中变化
     *
     * @param {*} [item={}]
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.onExpCtrlSelectionChange = function (item) {
        if (item === void 0) { item = {}; }
    };
    /**
     * 导航树部件加载完成
     *
     * @param {*} [item={}]
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.onExpCtrlLoaded = function (item) {
        if (item === void 0) { item = {}; }
    };
    /**
     * 获取导航项视图参数，在发布视图控制器内重写
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.getExpItemView = function (arg) {
        if (arg === void 0) { arg = {}; }
        return undefined;
    };
    /**
     * 获取新建导航视图参数，在发布视图控制器中重写
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.getNewDataView = function (arg) {
        if (arg === void 0) { arg = {}; }
        return undefined;
    };
    /**
     * 获取编辑导航视图参数，在发布视图控制器中重写
     *
     * @param {*} [arg={}]
     * @returns {*}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.getEditDataView = function (arg) {
        if (arg === void 0) { arg = {}; }
        return undefined;
    };
    /**
     * 打开导航子视图
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizExpViewController
     */
    IBizExpViewController.prototype.openExpChildView = function (item) {
        if (item === void 0) { item = {}; }
        if (!item || Object.keys(item).length === 0) {
            return;
        }
        var view = this.getExpItemView(item.expitem);
        if (!view) {
            return;
        }
        var data = {};
        Object.assign(data, item.expitem.viewparam);
        var exp = this.getExpBar();
        if (exp) {
            exp.setSelectItem(item);
        }
        this.openView(view.routepath, data);
    };
    return IBizExpViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 实体分页导航视图控制器
 *
 * @export
 * @class IBizEntityIndexViewController
 * @extends {IBizEditViewController}
 */
var IBizEntityTabExpViewController = /** @class */ (function (_super) {
    __extends(IBizEntityTabExpViewController, _super);
    /**
     * Creates an instance of IBizEntityTabExpViewController.
     * 创建 IBizEntityTabExpViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizEntityTabExpViewController
     */
    function IBizEntityTabExpViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 默认选中项下标
         *
         * @memberof IBizEditView2Controller
         */
        _this.selectIndex = 0;
        /**
         * 默认激活下标页
         *
         * @memberof IBizEntityTabExpViewController
         */
        _this.defaultActiveTab = 0;
        /**
         * 所有tab选中项
         *
         * @type {Array<any>}
         * @memberof IBizEntityTabExpViewController
         */
        _this.tabs = [];
        /**
         * tab选中项
         *
         * @type {*}
         * @memberof IBizEntityTabExpViewController
         */
        _this.tabItem = {};
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.onInitComponents = function () {
        _super.prototype.onInitComponents.call(this);
        this.regTabs();
    };
    /**
     * 视图加载
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        this.loadDefaultTab();
    };
    /**
     * 加载默认数据项
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.loadDefaultTab = function () {
        if (this.tabs.length > 0) {
            var tab = this.tabs[0];
            var viewParams = this.getExpItemView({ viewid: tab.name });
            this.openView(viewParams.routepath, Object.assign(tab.viewParams, viewParams));
        }
    };
    /**
     * 选择卡变化
     *
     * @param {*} [item={}]
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.changeTab = function (name) {
        if (name === void 0) { name = {}; }
        if (name) {
            var viewParams = this.getExpItemView({ viewid: name });
            if (viewParams) {
                var index = this.tabs.findIndex(function (tab) { return Object.is(tab.name, name); });
                var param = {};
                if (index >= 0) {
                    param = this.tabs[index].viewParams;
                }
                this.openView(viewParams.routepath, Object.assign(param, viewParams));
            }
        }
    };
    /**
     * 注册Tab选择卡
     *
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.regTabs = function () {
    };
    /**
     * 保存Tab选项卡
     *
     * @param {*} [tab={}]
     * @memberof IBizEntityTabExpViewController
     */
    IBizEntityTabExpViewController.prototype.regTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        this.tabs.push(tab);
    };
    return IBizEntityTabExpViewController;
}(IBizExpViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 树视图视图控制器
 *
 * @class IBizTreeViewController
 * @extends {IBizMDViewController}
 */
var IBizTreeViewController = /** @class */ (function (_super) {
    __extends(IBizTreeViewController, _super);
    /**
     * Creates an instance of IBizTreeViewController.
     * 创建 IBizTreeViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizTreeViewController
     */
    function IBizTreeViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 所有选中树数据
         *
         * @type {*}
         * @memberof IBizTreeViewController
         */
        _this.selectedDatas = [];
        /**
         * 当前选中树数据
         *
         * @type {*}
         * @memberof IBizTreeViewController
         */
        _this.selectedData = {};
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizTreeViewController
     */
    IBizTreeViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var tree = this.getTree();
        if (tree) {
            // 树加载完成
            tree.on(IBizTree.CONTEXTMENU).subscribe(function (datas) {
                _this.onTreeLoad(datas);
            });
            // 数据选中
            tree.on(IBizTree.SELECTIONCHANGE).subscribe(function (datas) {
                _this.onSelectionChange(datas);
            });
            // 数据激活
            // tree.on(IBizTree.DATAACTIVATED, (datas) => {
            //     this.onDataActivated(datas);
            // });
        }
    };
    /**
     * 获取多数据部件
     *
     * @returns {*}
     * @memberof IBizTreeViewController
     */
    IBizTreeViewController.prototype.getMDCtrl = function () {
        return this.getTree();
    };
    /**
     * 获取数部件
     *
     * @returns {*}
     * @memberof IBizTreeViewController
     */
    IBizTreeViewController.prototype.getTree = function () {
        return undefined;
    };
    /**
     * 数据部件数据加载完成
     *
     * @param {Array<any>} args
     * @memberof IBizTreeViewController
     */
    IBizTreeViewController.prototype.onTreeLoad = function (args) {
    };
    /**
     * 值选中变化
     *
     * @param {Array<any>} args
     * @memberof IBizTreeViewController
     */
    IBizTreeViewController.prototype.onSelectionChange = function (args) {
        var _this = this;
        if (args.length > 0) {
            var record = args[0];
            var selectedData = { srfkey: record.srfkey, srfmajortext: record.srfmajortext };
            this.selectedData = selectedData;
            this.selectedDatas = [];
            args.forEach(function (item, index) {
                var record = item;
                var selectedData = { srfkey: record.srfkey, srfmajortext: record.srfmajortext };
                if (index == 0) {
                    _this.selectedData = selectedData;
                }
                _this.selectedDatas.push(selectedData);
            });
        }
        else {
            this.selectedData = {};
            this.selectedDatas = [];
        }
        _super.prototype.onSelectionChange.call(this, args);
    };
    return IBizTreeViewController;
}(IBizMDViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 选择树视图控制器（部件视图）
 *
 * @class IBizPickupTreeViewController
 * @extends {IBizTreeViewController}
 */
var IBizPickupTreeViewController = /** @class */ (function (_super) {
    __extends(IBizPickupTreeViewController, _super);
    /**
     * Creates an instance of IBizPickupTreeViewController.
     * 创建 IBizPickupTreeViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPickupTreeViewController
     */
    function IBizPickupTreeViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否支持多项数据选择 <Input>
         *
         * @private
         * @type {boolean}
         * @memberof IBizPickupTreeViewController
         */
        _this.multiselect = true;
        /**
         * 多数据部件加载所有数据  <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizPickupTreeViewController
         */
        _this.allData = 'allData';
        /**
         * 数据选中事件  <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizPickupTreeViewController
         */
        _this.selectionChange = 'selectionChange';
        /**
         * 数据激活事件  <Output>
         *
         * @private
         * @type {string}
         * @memberof IBizPickupTreeViewController
         */
        _this.dataActivated = 'dataActivated';
        return _this;
    }
    /**
     * 获取树部件
     *
     * @returns {*}
     * @memberof IBizPickupTreeViewController
     */
    IBizPickupTreeViewController.prototype.getTree = function () {
        return this.getControl('tree');
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizPickupTreeViewController
     */
    IBizPickupTreeViewController.prototype.isEnableQuickSearch = function () {
        return false;
    };
    /**
     * 树部件数据选中
     *
     * @param {Array<any>} datas
     * @memberof IBizPickupTreeViewController
     */
    IBizPickupTreeViewController.prototype.onSelectionChange = function (datas) {
        _super.prototype.onSelectionChange.call(this, datas);
        this.$vue.$emit(this.selectionChange, datas);
    };
    /**
     * 树部件数据激活
     *
     * @param {Array<any>} datas
     * @memberof IBizPickupTreeViewController
     */
    IBizPickupTreeViewController.prototype.onDataActivated = function (datas) {
        _super.prototype.onDataActivated.call(this, datas);
        this.$vue.$emit(this.dataActivated, datas);
    };
    /**
     * 树部件数据加载完成
     *
     * @param {Array<any>} datas
     * @memberof IBizPickupTreeViewController
     */
    IBizPickupTreeViewController.prototype.onTreeLoad = function (datas) {
        _super.prototype.onTreeLoad.call(this, datas);
        var _datas = this.doTreeDatas(datas);
        this.$vue.$emit(this.allData, _datas);
    };
    /**
     * 处理所有树数据
     *
     * @private
     * @param {Array<any>} datas
     * @returns {Array<any>}
     * @memberof IBizPickupTreeViewController
     */
    IBizPickupTreeViewController.prototype.doTreeDatas = function (datas) {
        var _this = this;
        var _datas = [];
        datas.forEach(function (data) {
            var _data = {};
            Object.assign(_data, data);
            if (data.items && data.items.length > 0) {
                var _items = _this.doTreeDatas(data.items).slice();
                delete _data.items;
                _datas.push.apply(_datas, _items);
            }
            _datas.push(_data);
        });
        return _datas;
    };
    return IBizPickupTreeViewController;
}(IBizTreeViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 树导航视图控制器
 *
 * @class IBizWFExpViewController
 * @extends {IBizExpViewController}
 */
var IBizWFExpViewController = /** @class */ (function (_super) {
    __extends(IBizWFExpViewController, _super);
    /**
     * Creates an instance of IBizWFExpViewController.
     * 创建 IBizWFExpViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFExpViewController
     */
    function IBizWFExpViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 获取树导航部件
     *
     * @memberof IBizTreeExpViewController
     */
    IBizWFExpViewController.prototype.getExpBar = function () {
        return this.getControl('expbar');
    };
    /**
     * 导航视图部件加载完成
     *
     * @param {*} [item={}]
     * @memberof IBizWFExpViewController
     */
    IBizWFExpViewController.prototype.onExpCtrlLoaded = function (item) {
        if (item === void 0) { item = {}; }
        this.openExpChildView(item);
    };
    /**
     * 导航树选中导航变化
     *
     * @param {*} [item={}]
     * @memberof IBizWFExpViewController
     */
    IBizWFExpViewController.prototype.onExpCtrlSelectionChange = function (item) {
        if (item === void 0) { item = {}; }
        this.openExpChildView(item);
    };
    return IBizWFExpViewController;
}(IBizExpViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流编辑视图控制器
 *
 * @class IBizWFEditViewController
 * @extends {IBizEditViewController}
 */
var IBizWFEditViewController = /** @class */ (function (_super) {
    __extends(IBizWFEditViewController, _super);
    /**
     * Creates an instance of IBizWFEditViewController.
     * 创建 IBizWFEditViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFEditViewController
     */
    function IBizWFEditViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizWFEditViewController;
}(IBizEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流启动编辑视图
 *
 * @class IBizWFStartViewController
 * @extends {IBizWFEditViewController}
 */
var IBizWFStartViewController = /** @class */ (function (_super) {
    __extends(IBizWFStartViewController, _super);
    /**
     * Creates an instance of IBizWFStartViewController.
     * 创建 IBizWFStartViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFStartViewController
     */
    function IBizWFStartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     *工作流视图启动完成
     *
     * @memberof IBizWFStartViewController
     */
    IBizWFStartViewController.prototype.onFormWFStarted = function () {
        _super.prototype.onFormWFStarted.call(this);
    };
    /**
     * 保存启动表单内容并启动
     *
     * @memberof IBizWFStartViewController
     */
    IBizWFStartViewController.prototype.onClickOkButton = function () {
        this.doSaveAndStart();
    };
    /**
     * 取消启动工作流
     *
     * @memberof IBizWFStartViewController
     */
    IBizWFStartViewController.prototype.onClickCancelButton = function () {
        this.closeWindow();
    };
    return IBizWFStartViewController;
}(IBizWFEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流交互视图控制器
 *
 * @class IBizWFActionViewController
 * @extends {IBizWFEditViewController}
 */
var IBizWFActionViewController = /** @class */ (function (_super) {
    __extends(IBizWFActionViewController, _super);
    /**
     * Creates an instance of IBizWFActionViewController.
     * 创建 IBizWFActionViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFActionViewController
     */
    function IBizWFActionViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 工作流提交
     *
     * @memberof IBizWFActionViewController
     */
    IBizWFActionViewController.prototype.onFormWFSubmitted = function () {
        _super.prototype.onFormWFSubmitted.call(this);
    };
    /**
     * 工作流提交
     *
     * @memberof IBizWFActionViewController
     */
    IBizWFActionViewController.prototype.onClickOkButton = function () {
        var form = this.getForm();
        if (form) {
            form.wfsubmit(this.getViewParam());
        }
    };
    /**
     * 关闭工作流操作界面
     *
     * @memberof IBizWFActionViewController
     */
    IBizWFActionViewController.prototype.onClickCancelButton = function () {
        if (this.isModal()) {
            this.closeModal();
        }
        else {
            this.closeWindow();
        }
    };
    return IBizWFActionViewController;
}(IBizWFEditViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流左右关系编辑视图
 *
 * @class IBizWFEditView2Controller
 * @extends {IBizEditView2Controller}
 */
var IBizWFEditView2Controller = /** @class */ (function (_super) {
    __extends(IBizWFEditView2Controller, _super);
    /**
     * Creates an instance of IBizWFEditView2Controller.
     * 创建 IBizWFEditView2Controller  实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFEditView2Controller
     */
    function IBizWFEditView2Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizWFEditView2Controller;
}(IBizEditView2Controller));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流分页编辑视图
 *
 * @class IBizWFEditView3Controller
 * @extends {IBizEditView3Controller}
 */
var IBizWFEditView3Controller = /** @class */ (function (_super) {
    __extends(IBizWFEditView3Controller, _super);
    /**
     * Creates an instance of IBizWFEditView3Controller.
     * 创建 IBizWFEditView3Controller 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFEditView3Controller
     */
    function IBizWFEditView3Controller(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizWFEditView3Controller;
}(IBizEditView3Controller));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 搜索视图控制器
 *
 * @class IBizSearchViewController
 * @extends {IBizMainViewController}
 */
var IBizSearchViewController = /** @class */ (function (_super) {
    __extends(IBizSearchViewController, _super);
    /**
     * Creates an instance of IBizSearchViewController.
     * 创建 IBizSearchViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizSearchViewController
     */
    function IBizSearchViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 父数据改变
         *
         * @memberof IBizSearchViewController
         */
        _this.parentDataChanged = true;
        return _this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var searchform = this.getSearchForm();
        if (searchform) {
            // 表单加载完成
            searchform.on(IBizForm.FORMLOADED).subscribe(function (data) {
                if (_this.isLoadDefault()) {
                    _this.onSearchFormSearched();
                }
            });
            // 表单搜索，手动触发
            searchform.on(IBizSearchForm.FORMSEARCHED).subscribe(function (data) {
                _this.onSearchFormSearched();
            });
            // 表单重置
            searchform.on(IBizSearchForm.FORMRESETED).subscribe(function (data) {
                _this.onSearchFormReseted();
            });
            // 表单值变化
            searchform.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                if (!data) {
                    return;
                }
                var fieldname = data.name;
                _this.onSearchFormFieldChanged(fieldname, data.value, data);
            });
            // 设置表单是否开启
            searchform.setOpen(!this.isEnableQuickSearch());
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        var searchform = this.getSearchForm();
        if (searchform) {
            searchform.autoLoad({});
        }
    };
    /**
     * 获取搜索表单对象
     *
     * @returns {*}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.getSearchForm = function () {
        return this.getControl('searchform');
    };
    /**
     * 搜索表单属性值发生变化
     *
     * @param {string} fieldname
     * @param {string} field
     * @param {string} value
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSearchFormFieldChanged = function (fieldname, field, value) {
    };
    /**
     * 搜索表单重置
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSearchFormReseted = function () {
    };
    /**
     * 视图是否默认加载
     *
     * @returns {boolean}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.isLoadDefault = function () {
        return true;
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.isEnableQuickSearch = function () {
        return true;
    };
    /**
     * 获取搜索表单值
     *
     * @returns {*}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.getSearchCond = function () {
        if (this.getSearchForm()) {
            return this.getSearchForm().getValues();
        }
        return {};
    };
    /**
     * 搜索表单搜索执行
     *
     * @param {boolean} isload 是否加载
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSearchFormSearched = function () {
    };
    /**
     * 数据加载之前
     *
     * @param {*} [args={}]
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onStoreBeforeLoad = function (args) {
        if (args === void 0) { args = {}; }
        var fetchParam = {};
        if (this.getViewParam()) {
            Object.assign(fetchParam, this.getViewParam());
        }
        if (this.getParentMode()) {
            Object.assign(fetchParam, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(fetchParam, this.getParentData());
        }
        if ((this.getSearchCond() && this.getSearchForm().isOpen()) || !this.isEnableQuickSearch()) {
            Object.assign(fetchParam, this.getSearchCond());
        }
        // 获取快速搜索里的搜索参数
        if (this.isEnableQuickSearch() && this.searchValue !== undefined) {
            args.search = this.searchValue;
        }
        Object.assign(args, fetchParam);
    };
    /**
     * 数据加载完成
     *
     * @param {*} data
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onStoreLoad = function (data) {
        this.parentDataChanged = false;
        this.reloadUICounters();
    };
    /**
     *设置父数据
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onSetParentData = function () {
        _super.prototype.onSetParentData.call(this);
        this.parentDataChanged = true;
    };
    /**
     * 数据被激活<最典型的情况就是行双击>
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onDataActivated = function () {
    };
    /**
     * 搜索表单打开
     *
     * @returns {void}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.openSearchForm = function () {
        if (!this.isEnableQuickSearch()) {
            return;
        }
        var searchForm = this.getSearchForm();
        if (searchForm) {
            searchForm.setOpen(!searchForm.opened);
        }
    };
    /**
     * 搜索按钮执行搜索
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.btnSearch = function () {
        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched();
        }
    };
    /**
     * 清空快速搜索值
     *
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.clearQuickSearchValue = function () {
        this.searchValue = undefined;
        this.onRefresh();
    };
    /**
     * 执行快速搜索
     *
     * @param {*} event
     * @returns {void}
     * @memberof IBizSearchViewController
     */
    IBizSearchViewController.prototype.onQuickSearch = function (event) {
        if (!event || event.keyCode !== 13) {
            return;
        }
        if (this.isEnableQuickSearch()) {
            this.onSearchFormSearched();
        }
    };
    return IBizSearchViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据图表视图控制器
 *
 * @class IBizChartViewController
 * @extends {IBizSearchViewController}
 */
var IBizChartViewController = /** @class */ (function (_super) {
    __extends(IBizChartViewController, _super);
    /**
     * Creates an instance of IBizChartViewController.
     * 创建 IBizChartViewController 对象
     *
     * @param {*} [opts={}]
     * @memberof IBizChartViewController
     */
    function IBizChartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 视图部件初始化
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var chart = this.getChart();
        if (chart) {
            chart.on(IBizChart.LOADED).subscribe(function (data) {
                _this.onStoreLoad(data);
            });
            chart.on(IBizChart.BEFORELOAD).subscribe(function (data) {
                _this.onStoreBeforeLoad(data);
            });
            chart.on(IBizChart.DBLCLICK).subscribe(function (data) {
                _this.onDataActivated();
            });
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        if (!this.getSearchForm() && this.isLoadDefault()) {
            if (!this.isFreeLayout()) {
                if (this.getChart()) {
                    this.getChart().load();
                }
            }
            else {
                this.otherLoad();
            }
        }
    };
    /**
     * 获取图表部件
     *
     * @returns {*}
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.getChart = function () {
        return this.getControl('chart');
    };
    /**
     * 搜索表单触发加载
     *
     * @param {boolean} isload 是否加载
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onSearchFormSearched = function () {
        if (!this.isFreeLayout()) {
            if (this.getChart()) {
                this.getChart().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 表单重置完成
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onSearchFormReseted = function () {
        if (!this.isFreeLayout()) {
            if (this.getChart()) {
                this.getChart().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 视图部件刷新
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        if (!this.isFreeLayout()) {
            if (this.getChart()) {
                this.getChart().load();
            }
        }
        else {
            this.otherLoad();
        }
    };
    /**
     * 附加额外部件加载
     *
     * @memberof IBizChartViewController
     */
    IBizChartViewController.prototype.otherLoad = function () {
        if (this.controls) {
            this.controls.forEach(function (obj, key) {
                if (obj instanceof IBizForm || obj instanceof IBizToolbar) {
                    return;
                }
                if (obj.load) {
                    obj.load();
                }
            });
        }
    };
    return IBizChartViewController;
}(IBizSearchViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * Portal视图控制器
 *
 * @class IBizPortalViewController
 * @extends {IBizMainViewController}
 */
var IBizPortalViewController = /** @class */ (function (_super) {
    __extends(IBizPortalViewController, _super);
    /**
     * Creates an instance of IBizPortalViewController.
     * 创建 IBizPortalViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizPortalViewController
     */
    function IBizPortalViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 门户部件
         *
         * @private
         * @type {Map<string, any>}
         * @memberof IBizPortalViewController
         */
        _this.portalCtrls = new Map();
        return _this;
    }
    /**
     * 视图初始化
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onInit = function () {
        this.regPortalCtrls();
        _super.prototype.onInit.call(this);
    };
    /**
     * 部件初始化
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        this.portalCtrls.forEach(function (ctrl) {
            // 应用菜单选中
            ctrl.on(IBizAppMenu.MENUSELECTION).subscribe(function (data) {
                _this.onMenuSelection(data);
            });
        });
    };
    /**
     * 视图加载
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onLoad = function () {
        var _this = this;
        _super.prototype.onLoad.call(this);
        this.portalCtrls.forEach(function (ctrl) {
            if (ctrl.load instanceof Function) {
                ctrl.load(_this.viewParam);
            }
        });
    };
    /**
     * 注册所有Portal部件
     *
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.regPortalCtrls = function () {
    };
    /**
     * 注册所有Portal部件
     *
     * @param {string} name
     * @param {*} ctrl
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.regPortalCtrl = function (name, ctrl) {
        this.portalCtrls.set(name, ctrl);
    };
    /**
     * 菜单选中跳转路由
     *
     * @param {Array<any>} data
     * @memberof IBizPortalViewController
     */
    IBizPortalViewController.prototype.onMenuSelection = function (data) {
        var item = {};
        Object.assign(item, data[0]);
        this.openView(item.routepath, item.openviewparam);
    };
    return IBizPortalViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 报表视图控制器
 *
 * @export
 * @class IBizReportViewController
 * @extends {IBizSearchViewController}
 */
var IBizReportViewController = /** @class */ (function (_super) {
    __extends(IBizReportViewController, _super);
    /**
     * Creates an instance of IBizReportViewController.
     * 创建 IBizReportViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizReportViewController
     */
    function IBizReportViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 部件初始化
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onInitComponents = function () {
        var _this = this;
        _super.prototype.onInitComponents.call(this);
        var reportPanel = this.getReportPanel();
        if (reportPanel) {
            reportPanel.on(IBizReportPanel.BEFORELOAD, function (data) {
                _this.onStoreBeforeLoad(data);
            });
        }
    };
    /**
     * 视图加载
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onLoad = function () {
        _super.prototype.onLoad.call(this);
        if (!this.getSearchForm() && this.getReportPanel() && this.isLoadDefault()) {
            this.getReportPanel().load();
        }
    };
    /**
     * 获取报表部件
     *
     * @returns {*}
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.getReportPanel = function () {
        return this.getControl('reportpanel');
    };
    /**
     * 搜索搜索表单搜索
     *
     * @param {boolean} isload 是否加载
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onSearchFormSearched = function () {
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    };
    /**
     * 搜搜表单重置
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onSearchFormReseted = function () {
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    };
    /**
     * 视图刷新
     *
     * @memberof IBizReportViewController
     */
    IBizReportViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        if (this.getReportPanel()) {
            this.getReportPanel().load();
        }
    };
    return IBizReportViewController;
}(IBizSearchViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 向导视图控制器
 *
 * @class IBizWizardViewController
 * @extends {IBizMainViewController}
 */
var IBizWizardViewController = /** @class */ (function (_super) {
    __extends(IBizWizardViewController, _super);
    /**
     *Creates an instance of IBizWizardViewController.
     * @param {*} [opts={}]
     * @memberof IBizWizardViewController
     */
    function IBizWizardViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     * 初始化面板
     *
     * @memberof IBizEditViewController
     */
    IBizWizardViewController.prototype.onInitComponents = function () {
        var _this_1 = this;
        _super.prototype.onInitComponents.call(this);
        var wizardpanel = this.getWizardPanel();
        if (wizardpanel) {
            // 向导面板初始化完成
            wizardpanel.on(IBizWizardPanel.WIZARDPANEL_INITED).subscribe(function (data) {
                _this_1.onWizardInited(data);
            });
            // 向导面板完成
            wizardpanel.on(IBizWizardPanel.WIZARDPANEL_FINISH).subscribe(function (data) {
                _this_1.onWizardFinesh(data);
            });
            // 向导表单保存完成
            wizardpanel.on(IBizWizardPanel.WIZARDFORMSAVED).subscribe(function (data) {
                _this_1.onWizardFormSaved(data);
            });
            // 向导表单切换
            wizardpanel.on(IBizWizardPanel.WIZARDFORMCHANGED).subscribe(function (data) {
                _this_1.onWizardFormChanged(data);
            });
            // 向导表单项更新
            wizardpanel.on(IBizWizardPanel.WIZARDFORMFIELDCHANGED).subscribe(function (data) {
                if (data) {
                    _this_1.onWizardFormFieldChanged(data.formName, data.name, data.field, data.value);
                }
            });
        }
    };
    /**
     * 加载面板数据
     *
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onLoad = function () {
        if (this.getWizardPanel()) {
            this.getWizardPanel().autoLoad(this.getViewParam());
        }
    };
    /**
     * 获取向导面板对象
     *
     * @returns {*}
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.getWizardPanel = function () {
        return this.getControl('wizardpanel');
    };
    /**
     * 向导表单保存完成
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFormSaved = function (data) {
    };
    /**
     * 向导面板初始化完成
     *
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardInited = function (data) {
    };
    /**
     * 向导面板完成
     *
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFinesh = function (data) {
        var _this = this;
        _this.$vue.$emit('dataChange', data);
        _this.refreshReferView(data);
    };
    /**
     * 向导表单切换
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFormChanged = function (data) {
    };
    /**
     * 向导表单项更新
     *
     * @param {*} data
     * @memberof IBizWizardViewController
     */
    IBizWizardViewController.prototype.onWizardFormFieldChanged = function (formName, fieldname, field, value) {
    };
    /**
     * 刷新关联数据
     *
     * @memberof IBizEditViewController
     */
    IBizWizardViewController.prototype.refreshReferView = function (data) {
        var iBizApp = IBizApp.getInstance();
        if (iBizApp) {
            var parentWindow = iBizApp.getParentWindow();
            var viewparam = this.getViewParam();
            if (parentWindow && viewparam.openerid && !Object.is(viewparam.openerid, '')) {
                try {
                    var pWinIBizApp = parentWindow.getIBizApp();
                    pWinIBizApp.fireRefreshView({ openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) });
                }
                catch (error) {
                    parentWindow.postMessage({ ret: 'OK', type: 'REFERVIEW', openerid: viewparam.openerid, viewUsage: parseInt(viewparam.pviewusage) }, '*');
                    // if (window && window.parent) {
                    //     let win = window;
                    // }
                }
            }
        }
        if (this.isModal()) {
            var result = { ret: 'OK', activeData: data };
            this.dataChange(result);
        }
    };
    return IBizWizardViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 流程步骤视图
 *
 * @class IBizWFStepActorViewController
 * @extends {IBizMDViewController}
 */
var IBizWFStepActorViewController = /** @class */ (function (_super) {
    __extends(IBizWFStepActorViewController, _super);
    /**
     *Creates an instance of IBizWFStepActorViewController.
     * @param {*} [opts={}]
     * @memberof IBizWFStepActorViewController
     */
    function IBizWFStepActorViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 流程步骤对象
         *
         * @type {*}
         * @memberof IBizWFStepActorViewController
         */
        _this.wfstepactor = null;
        _this.wfstepactor = new IBizTimeline({
            name: 'grid',
            url: opts.backendurl,
            viewController: _this,
            $route: opts.$route,
            $router: opts.$router,
            $vue: opts.$vue
        });
        _this.controls.set('wfstepactor', _this.wfstepactor);
        return _this;
    }
    /**
     * 初始化面板
     *
     * @memberof IBizWFStepActorViewController
     */
    IBizWFStepActorViewController.prototype.onInitComponents = function () {
        _super.prototype.onInitComponents.call(this);
    };
    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof IBizWFStepActorViewController
     */
    IBizWFStepActorViewController.prototype.getMDCtrl = function () {
        return this.getControl('wfstepactor');
    };
    /**
     * 是否支持快速搜索
     *
     * @returns {boolean}
     * @memberof IBizWFStepActorViewController
     */
    IBizWFStepActorViewController.prototype.isEnableQuickSearch = function () {
        return false;
    };
    return IBizWFStepActorViewController;
}(IBizMDViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 流程图
 *
 * @class IBizWFStepDataTraceChartViewController
 * @extends {IBizMDViewController}
 */
var IBizWFStepDataTraceChartViewController = /** @class */ (function (_super) {
    __extends(IBizWFStepDataTraceChartViewController, _super);
    /**
     *Creates an instance of IBizWFStepDataTraceChartViewController.
     * @param {*} [opts={}]
     * @memberof IBizWFStepDataTraceChartViewController
     */
    function IBizWFStepDataTraceChartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 实体名称
         *
         * @type {string}
         * @memberof IBizWFStepDataTraceChartViewController
         */
        _this.srfdeid = '';
        return _this;
    }
    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    IBizWFStepDataTraceChartViewController.prototype.getMDCtrl = function () {
        return undefined;
    };
    /**
     * 获取图片路径
     *
     * @returns {string}
     * @memberof IBizWFStepDataTraceChartViewController
     */
    IBizWFStepDataTraceChartViewController.prototype.getImgPath = function () {
        return '';
    };
    return IBizWFStepDataTraceChartViewController;
}(IBizMDViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流启动视图
 *
 * @class IBizWFProxyStartViewController
 * @extends {IBizMainViewController}
 */
var IBizWFProxyStartViewController = /** @class */ (function (_super) {
    __extends(IBizWFProxyStartViewController, _super);
    /**
     * Creates an instance of IBizWFProxyStartViewController.
     * 创建 IBizWFProxyStartViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFProxyStartViewController
     */
    function IBizWFProxyStartViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 代理应用启动视图url
         *
         * @type {string}
         * @memberof IBizWFProxyStartViewController
         */
        _this.startviewurl = '';
        return _this;
    }
    /**
     * 视图模型加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyStartViewController
     */
    IBizWFProxyStartViewController.prototype.afterLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.afterLoadMode.call(this, data);
        this.startviewurl = data.startviewurl;
    };
    return IBizWFProxyStartViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流结果视图
 *
 * @class IBizWFProxyResultViewController
 * @extends {IBizMainViewController}
 */
var IBizWFProxyResultViewController = /** @class */ (function (_super) {
    __extends(IBizWFProxyResultViewController, _super);
    /**
     * Creates an instance of IBizWFProxyResultViewController.
     * 创建 IBizWFProxyResultViewController 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizWFProxyResultViewController
     */
    function IBizWFProxyResultViewController(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizWFProxyResultViewController;
}(IBizMainViewController));

"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工作流代理数据视图
 *
 * @class IBizWFProxyDataViewController
 * @extends {IBizMainViewController}
 */
var IBizWFProxyDataViewController = /** @class */ (function (_super) {
    __extends(IBizWFProxyDataViewController, _super);
    /**
     * Creates an instance of IBizWFProxyDataViewController.
     * 创建 IBizWFProxyDataViewController 实例
     * @param {*} [opts={}]
     * @memberof IBizWFProxyDataViewController
     */
    function IBizWFProxyDataViewController(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 代理数据视图url
         *
         * @type {string}
         * @memberof IBizWFProxyDataViewController
         */
        _this.viewurl = '';
        return _this;
    }
    /**
     * 刷新数据 （处理关系数据变更）
     *
     * @memberof IBizWFProxyDataViewController
     */
    IBizWFProxyDataViewController.prototype.onRefresh = function () {
        _super.prototype.onRefresh.call(this);
        this.loadModel();
    };
    /**
     * 模型加载之前
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyDataViewController
     */
    IBizWFProxyDataViewController.prototype.beforeLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.beforeLoadMode.call(this, data);
        if (this.getParentMode()) {
            Object.assign(data, this.getParentMode());
        }
        if (this.getParentData()) {
            Object.assign(data, this.getParentData());
        }
        if (this.getReferData()) {
            Object.assign(data, this.getReferData());
        }
    };
    /**
     * 工作流加载完成
     *
     * @param {*} [data={}]
     * @memberof IBizWFProxyDataViewController
     */
    IBizWFProxyDataViewController.prototype.afterLoadMode = function (data) {
        if (data === void 0) { data = {}; }
        _super.prototype.afterLoadMode.call(this, data);
        this.viewurl = data.viewurl;
    };
    return IBizWFProxyDataViewController;
}(IBizMainViewController));

"use strict";
Vue.component('ibiz-app-menu', {
    template: "\n        <div class=\"ibiz-app-menu\">\n            <el-menu class=\"ibiz-menu\" :default-openeds=\"ctrl.defaultOpeneds\" :mode=\"mode\" :menu-trigger=\"trigger\" @select=\"onSelect\" :default-active=\"ctrl.selectItem.id\" :collapse=\"isCollapse\">\n                <template v-for=\"(item0, index0) in ctrl.items\">\n\n                    <!---  \u4E00\u7EA7\u83DC\u5355\u6709\u5B50\u9879 begin  --->\n                    <template v-if=\"item0.items && item0.items.length > 0\">\n                        <el-submenu v-show=\"!item0.hidden\" :index=\"item0.id\" :popper-class=\"['ibiz-popper-menu', $root.themeClass]\">\n                            <template slot=\"title\">\n                                <img v-if=\"item0.icon != ''\" :src=\"item0.icon\"/>\n                                <i v-else class=\"ibiz-menu-icon\" :class=\"[item0.iconcls == '' ? 'fa fa-cogs' : item0.iconcls ]\" aria-hidden=\"true\"></i>\n                                <span slot=\"title\">{{ item0.text }}</span>\n                                <Icon class=\"active-icon\" type=\"md-arrow-dropleft\" />\n                            </template>\n                            <template v-for=\"(item1, index1) in item0.items\">\n\n                                <!---  \u4E8C\u7EA7\u83DC\u5355\u6709\u5B50\u9879 begin  --->\n                                <template v-if=\"item1.items && item1.items.length > 0\">\n                                    <el-submenu v-show=\"!item1.hidden\" :index=\"item1.id\">\n                                        <template slot=\"title\">\n                                            <img v-if=\"item1.icon != ''\" :src=\"item1.icon\"/>\n                                            <i v-else class=\"ibiz-menu-icon\" v-if=\"item1.iconcls != ''\" :class=\"item1.iconcls\" aria-hidden=\"true\"></i>\n                                            <span slot=\"title\">{{ item1.text }}</span>\n                                        </template>\n\n                                        <!---  \u4E09\u7EA7\u83DC\u5355 begin  --->\n                                        <template v-for=\"(item2, index2) in item1.items\">\n                                            <el-menu-item v-show=\"!item2.hidden\" :index=\"item2.id\">\n                                                <img v-if=\"item2.icon != ''\" :src=\"item2.icon\"/>\n                                                <i v-else class=\"ibiz-menu-icon\" v-if=\"item2.iconcls != ''\" :class=\"item2.iconcls\" aria-hidden=\"true\"></i>\n                                                <span slot=\"title\">{{ item2.text }}</span>\n                                            </el-menu-item>\n                                        </template>\n                                        <!---  \u4E09\u7EA7\u83DC\u5355\u6709 begin  --->\n\n                                    </el-submenu>\n                                </template>\n                                <!---  \u4E8C\u7EA7\u83DC\u5355\u6709\u5B50\u9879 end  --->\n\n                                <!---  \u4E8C\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 begin  --->\n                                <template v-else>\n                                    <el-menu-item v-show=\"!item1.hidden\" :index=\"item1.id\">\n                                        <img v-if=\"item1.icon != ''\" :src=\"item1.icon\"/>\n                                        <i v-else class=\"ibiz-menu-icon\" v-if=\"item1.iconcls != ''\" :class=\"item1.iconcls\" aria-hidden=\"true\"></i>\n                                        <span slot=\"title\">{{ item1.text }}</span>\n                                    </el-menu-item>\n                                </template>\n                                <!---  \u4E8C\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 end  --->\n\n                            </template>\n                        </el-submenu>\n                    </template>\n                    <!---  \u4E00\u7EA7\u83DC\u5355\u6709\u5B50\u9879 end  --->\n\n                    <!---  \u4E00\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 begin  --->\n                    <template v-else>\n                        <el-menu-item v-show=\"!item0.hidden\" :index=\"item0.id\">\n                            <img v-if=\"item0.icon != ''\" :src=\"item0.icon\"/>\n                            <i v-else class=\"ibiz-menu-icon\" :class=\"[item0.iconcls == '' ? 'fa fa-cogs' : item0.iconcls ]\" aria-hidden=\"true\"></i>\n                            <span slot=\"title\">{{ item0.text }}</span>\n                        </el-menu-item>\n                    </template>\n                    <!---  \u4E00\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 end  --->\n\n                </template>\n            </el-menu>\n        </div>\n    ",
    props: ['ctrl', 'viewController', 'isCollapse', 'mode'],
    data: function () {
        var data = {
            trigger: 'click'
        };
        return data;
    },
    created: function () {
        if (Object.is(this.mode, 'horizontal')) {
            this.trigger = 'hover';
        }
    },
    methods: {
        onSelect: function (name) {
            if (this.ctrl && !Object.is(name, '')) {
                var item = this.ctrl.getItem(this.ctrl.getItems(), { id: name });
                this.ctrl.onSelectChange(item);
            }
        }
    }
});

"use strict";
Vue.component('ibiz-form', {
    template: "\n        <div style=\"height: 100%;\">\n            <i-form :model=\"form\" style=\"height: 100%;\">\n                <row style=\"height: 100%;\">\n                    <slot :scope=\"fields\"></slot>\n                </row>\n            </i-form>\n        </div>\n    ",
    props: ['form'],
    data: function () {
        var data = { fields: this.form.fields };
        return data;
    }
});

"use strict";
Vue.component('ibiz-form-group', {
    template: "\n        <div>\n            <template v-if=\"group.showCaption\">\n                <card :bordered=\"false\" :dis-hover=\"true\">\n                    <p class=\"\" slot=\"title\"> {{ group.caption }}</p>\n                    <row>\n                        <slot></slot>\n                    </row>\n                </card>\n            </template>\n            <template v-else>\n                <row>\n                    <slot></slot>\n                </row>\n            </template>\n        </div>\n    ",
    props: ['form', 'group', 'name'],
    data: function () {
        var data = {};
        return data;
    }
});

"use strict";
Vue.component('ibiz-form-item', {
    template: "\n        <div>\n            <form-item :class=\"item.hasError ? 'ivu-form-item-error' :''\" :label-width=\"item.showCaption ? item.labelWidth : 0\" :required=\"!item.allowEmpty\">\n                <span slot=\"label\" class=\"\" v-if=\"item.labelWidth === 0 ? false : item.showCaption\">{{ item.emptyCaption ? '' : item.caption }}</span>\n                <slot></slot>\n            </form-item>\n        </div>\n    ",
    props: ['form', 'item', 'name'],
    data: function () {
        var data = {};
        return data;
    }
});

"use strict";
Vue.component('ibiz-form-editor', {
    template: "\n        <div class=\"ibiz-form-editor\">\n            <slot></slot>\n            <div class=\"editor-tipinfo\" :class=\"item.hasError ? 'show-tip-info': 'hide-tip-into'\" >\n                <tooltip max-width=\"200\" transfer=\"true\" theme=\"light\">\n                    <icon type=\"ios-warning-outline\"></icon>\n                    <div slot=\"content\">\n                        <p style=\"color:red;\">{{item.errorInfo}}</p>\n                    </div>\n                </tooltip>\n            </div>\n        </div>\n    ",
    props: ['item'],
    data: function () {
        var data = {};
        return data;
    }
});

"use strict";
Vue.component('ibiz-exp-bar', {
    template: "\n        <el-menu class=\"ibiz-exp-bar\" @open=\"subMenuSelect\" @close=\"subMenuSelect\" @select=\"onSelect\" :default-active=\"ctrl.selectItem.id\" :default-openeds=\"ctrl.expandItems\">\n            <template v-for=\"(item0, index0) in ctrl.items\">\n\n                <!---  \u4E00\u7EA7\u83DC\u5355\u6709\u5B50\u9879 begin  --->\n                <template v-if=\"item0.items && item0.items.length > 0\">\n                    <el-submenu :index=\"item0.id\" v-show=\"item0.show\" :disabled=\"item0.disabled\">\n                        <template slot=\"title\">\n                            <img v-if=\"item0.icon && item0.icon != ''\" :src=\"item0.icon\"/>\n                            <i v-else class=\"ibiz-menu-icon\" :class=\"[item0.iconcls == '' ? 'el-icon-share' : item0.iconcls ]\" aria-hidden=\"true\"></i>\n                            <span :class=\"ctrl.selectItem.id == item0.id ? 'active-subemnu': ''\">{{ item0.text }}</span>\n                            <span>&nbsp;&nbsp;<badge :count=\"item0.counterdata\" :class-name=\"item0.class\"></badge></span>\n                        </template>\n                        <template v-for=\"(item1, index1) in item0.items\">\n\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u6709\u5B50\u9879 begin  --->\n                            <template v-if=\"item1.items && item1.items.length > 0\">\n                                <el-submenu :index=\"item1.id\" v-show=\"item1.show\" :disabled=\"item1.disabled\">\n                                    <template slot=\"title\">\n                                        <img v-if=\"item1.icon && item1.icon != ''\" :src=\"item1.icon\"/>\n                                        <i v-else class=\"ibiz-menu-icon\" v-if=\"item1.iconcls != ''\" :class=\"item1.iconcls\" aria-hidden=\"true\"></i>\n                                        <span :class=\"ctrl.selectItem.id == item1.id ? 'active-subemnu': ''\">{{ item1.text }}</span>\n                                        <span>&nbsp;&nbsp;<badge :count=\"item1.counterdata\" :class-name=\"item1.class\"></badge></span>\n                                    </template>\n\n                                    <!---  \u4E09\u7EA7\u83DC\u5355 begin  --->\n                                    <template v-for=\"(item2, index2) in item1.items\">\n                                        <el-menu-item :index=\"item2.id\" v-show=\"item2.show\" :disabled=\"item2.disabled\">\n                                            <img v-if=\"item2.icon && item2.icon != ''\" :src=\"item2.icon\"/>\n                                            <i v-else class=\"ibiz-menu-icon\" v-if=\"item2.iconcls != ''\" :class=\"item2.iconcls\" aria-hidden=\"true\"></i>\n                                            <span>{{ item2.text }}</span>\n                                            <span>&nbsp;&nbsp;<badge :count=\"item2.counterdata\" :class-name=\"item2.class\"></badge></span>\n                                        </el-menu-item>\n                                    </template>\n                                    <!---  \u4E09\u7EA7\u83DC\u5355\u6709 begin  --->\n\n                                </el-submenu>\n                            </template>\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u6709\u5B50\u9879 end  --->\n\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 begin  --->\n                            <template v-else>\n                                <el-menu-item :index=\"item1.id\" v-show=\"item1.show\" :disabled=\"item1.disabled\">\n                                    <img v-if=\"item1.icon && item1.icon != ''\" :src=\"item1.icon\"/>\n                                    <i v-else class=\"ibiz-menu-icon\" v-if=\"item1.iconcls != ''\" :class=\"item1.iconcls\" aria-hidden=\"true\"></i>\n                                    <span>{{ item1.text }}</span>\n                                    <span>&nbsp;&nbsp;<badge :count=\"item1.counterdata\" :class-name=\"item1.class\"></badge></span>\n                                </el-menu-item>\n                            </template>\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 end  --->\n\n                        </template>\n                    </el-submenu>\n                </template>\n                <!---  \u4E00\u7EA7\u83DC\u5355\u6709\u5B50\u9879 end  --->\n\n                <!---  \u4E00\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 begin  --->\n                <template v-else>\n                    <el-menu-item :index=\"item0.id\" v-show=\"item0.show\" :disabled=\"item0.disabled\">\n                        <img v-if=\"item0.icon && item0.icon != ''\" :src=\"item0.icon\"/>\n                        <i v-else class=\"ibiz-menu-icon\" :class=\"[item0.iconcls == '' ? 'el-icon-share' : item0.iconcls ]\" aria-hidden=\"true\"></i>\n                        <span>{{ item0.text }}</span>\n                        <span>&nbsp;&nbsp;<badge :count=\"item0.counterdata\" :class-name=\"item0.class\"></badge></span>\n                    </el-menu-item>\n                </template>\n                <!---  \u4E00\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 end  --->\n\n            </template>\n        </el-menu>\n    ",
    props: ['ctrl', 'viewController'],
    data: function () {
        var data = { opendata: [] };
        return data;
    },
    mounted: function () {
    },
    methods: {
        // 获取选中项数据
        getItem: function (items, id) {
            var _this = this;
            var data = {};
            items.some(function (_item) {
                if (Object.is(id, _item.id)) {
                    Object.assign(data, _item);
                    return true;
                }
                if (_item.items && _item.items.length > 0) {
                    var subItem = _this.getItem(_item.items, id);
                    if (Object.keys(subItem).length > 0) {
                        Object.assign(data, subItem);
                        return true;
                    }
                }
            });
            return data;
        },
        // 菜单项选中
        onSelect: function (name) {
            var _this = this;
            var _data = _this.getItem(_this.ctrl.items, name);
            _this.ctrl.selection(_data);
        },
        // 菜单节点选中
        subMenuSelect: function (index, indexpath) {
            var _this = this;
            var _data = _this.getItem(_this.ctrl.items, index);
            _this.ctrl.expandedAndSelectSubMenu(_data);
        }
    }
});

"use strict";
Vue.component('ibiz-modal', {
    template: "\n        <modal class=\"ibiz-model\" :class-name=\"modalviewname\" v-model=\"showmodal\" @on-visible-change=\"onVisibleChange($event)\" :title=\"title\" :footer-hide=\"true\" :mask-closable=\"false\" :width=\"width\" :styles=\"{height: height}\">\n            <component v-if=\"showmodal\" :is=\"modalviewname\" :params=\"viewparam\" :viewType=\"'modalview'\" @close=\"close\" @dataChange=\"dataChange\"></component>\n        </modal>\n    ",
    props: ['key', 'params', 'index'],
    data: function () {
        var data = {
            showmodal: true,
            width: 0,
            title: '',
            height: '',
            modalviewname: '',
            subject: null,
            viewparam: {},
            tempResult: {},
        };
        var width = 600;
        if (window && window.innerWidth > 100) {
            if (window.innerWidth > 100) {
                width = window.innerWidth - 100;
            }
            else {
                width = window.innerWidth;
            }
        }
        Object.assign(data, { width: width });
        return data;
    },
    mounted: function () {
        this.modalviewname = this.params.modalviewname;
        if (this.params.subject) {
            this.subject = this.params.subject;
        }
        if (this.params.width && this.params.width !== 0) {
            this.width = this.params.width;
        }
        if (this.params.height && this.params.height !== 0) {
            this.height = this.params.height + "px";
        }
        if (this.params.title) {
            this.title = this.params.title;
        }
        if (this.params.viewparam) {
            Object.assign(this.viewparam, this.params.viewparam);
        }
    },
    methods: {
        close: function (result) {
            console.log(result);
            if (result && Object.keys(result).length > 0) {
                Object.assign(this.tempResult, JSON.parse(JSON.stringify(result)));
            }
            if (this.subject) {
                if (this.tempResult && Object.is(this.tempResult.ret, 'OK')) {
                    this.subject.next(this.tempResult);
                    this.subject = null;
                }
            }
            this.showmodal = false;
            // this.$emit("on-close", this.index)
        },
        dataChange: function (result) {
            console.log(result);
            this.tempResult = {};
            if (result && Object.keys(result).length > 0) {
                Object.assign(this.tempResult, JSON.parse(JSON.stringify(result)));
            }
        },
        onVisibleChange: function ($event) {
            console.log($event);
            if ($event) {
                return;
            }
            if (this.subject) {
                if (this.tempResult && Object.is(this.tempResult.ret, 'OK')) {
                    this.subject.next(this.tempResult);
                    this.subject = null;
                }
            }
        }
    }
});

"use strict";
Vue.component('ibiz-picker', {
    template: "\n    <div class=\"ibiz-picker\">\n        <el-autocomplete class=\"text-value\" v-if=\"editorType != 'dropdown' && editorType != 'pickup-no-ac'\" value-key=\"text\" :disabled=\"field.disabled\" v-model=\"value\" size=\"small\" :fetch-suggestions=\"onSearch\" @select=\"onACSelect\" @blur=\"onBlur\" style=\"width:100%;\">\n            <template slot=\"suffix\">\n                <i v-if=\"value != '' && !field.disabled\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                <i v-if=\"editorType != 'ac'\" class=\"el-icon-search\"  @click=\"openView\"></i>\n            </template>\n        </el-autocomplete>\n        <el-input class=\"text-value\" :value=\"value\" v-if=\"editorType == 'pickup-no-ac'\" readonly size=\"small\" :disabled=\"field.disabled\">\n            <template slot=\"suffix\">\n                <i v-if=\"value != '' && !field.disabled\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                <i class=\"el-icon-search\"  @click=\"openView\"></i>\n            </template>\n        </el-input>\n        <el-select v-if=\"editorType == 'dropdown'\" remote :remote-method=\"onSearch\" :value=\"value\" size=\"small\" filterable @change=\"onSelect\" :disabled=\"field.disabled\" style=\"width:100%;\" clearable @clear=\"onClear\" @visible-change=\"onSelectOpen\">\n            <el-option v-for=\"(item, index) of items\" :value=\"item.value\" :label=\"item.text\" :disabled=\"item.disabled\"></el-option>\n        </el-select>\n        <span v-if=\"editorType == 'dropdown'\" style=\"position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;\">\n            <i v-if=\"!open\" class=\"el-icon-arrow-down\"></i>\n            <i v-if=\"open\" class=\"el-icon-arrow-up\"></i>\n        </span>\n    </div>\n    ",
    props: ['field', 'name', 'modalviewname', 'editorType'],
    data: function () {
        var data = {
            http: IBizHttp.getInstance(),
            value: '',
            open: false,
            items: []
        };
        Object.assign(data, this.field.editorParams);
        Object.assign(data, { form: this.field.getForm() });
        data.value = this.field.value;
        return data;
    },
    mounted: function () {
        if (this.editorType == 'dropdown') {
            this.onSearch('');
        }
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            this.value = newVal;
        }
    },
    methods: {
        onSelectOpen: function (flag) {
            this.open = flag;
            if (this.open) {
                this.onSearch(this.value);
            }
        },
        onBlur: function () {
            if (this.field && this.value != this.field.value) {
                if (this.forceSelection) {
                    this.value = this.field.value;
                }
                else {
                    this.onACSelect({ text: this.value, value: '' });
                }
            }
        },
        //  填充条件
        fillPickupCondition: function (arg) {
            if (this.form) {
                if (this.itemParam && this.itemParam.fetchcond) {
                    var fetchparam = {};
                    var fetchCond = this.itemParam.fetchcond;
                    if (fetchCond) {
                        for (var cond in fetchCond) {
                            var field = this.form.findField(fetchCond[cond]);
                            if (!field) {
                                this.iBizNotification.error('操作失败', '未能找到当前表单项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            var value = field.getValue();
                            if (!value == null || Object.is(value, '')) {
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                if (this.itemParam && this.itemParam.temprs) {
                    // if (form.tempMode) {
                    // 	arg.srftempmode = true;
                    // }
                }
                Object.assign(arg, { srfreferitem: this.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.form.getActiveData()) });
                return true;
            }
            else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        onSearch: function (query, func) {
            var _this = this;
            if (this.url) {
                var param = {
                    srfaction: 'itemfetch',
                    query: query
                };
                var bcancel = this.fillPickupCondition(param);
                if (!bcancel) {
                    this.$Notice.warning({ title: '异常', desc: '条件不满足' });
                    return;
                }
                this.http.post(this.url, param).subscribe(function (data) {
                    _this.items = data.items;
                    if (typeof func == 'function') {
                        func(data.items);
                    }
                });
            }
        },
        onSelect: function (value) {
            var index = this.items.findIndex(function (item) { return Object.is(item.value, value); });
            if (index >= 0) {
                var item = this.items[index];
                this.onACSelect(item);
            }
        },
        onClear: function () {
            if (this.form) {
                var valueField = this.form.findField(this.valueItem);
                if (valueField) {
                    valueField.setValue('');
                }
                var itemField = this.form.findField(this.name);
                if (itemField) {
                    itemField.setValue('');
                }
            }
        },
        onACSelect: function (item) {
            if (this.form) {
                var valueField = this.form.findField(this.valueItem);
                if (valueField) {
                    valueField.setValue(item.value);
                }
                var itemField = this.form.findField(this.name);
                if (itemField) {
                    itemField.setValue(item.text);
                }
            }
        },
        openView: function () {
            var _this = this;
            if (this.field && this.field.disabled) {
                return;
            }
            var view = { viewparam: {} };
            var viewController;
            if (this.form) {
                viewController = this.form.getViewController();
                // let _srfkey = this.form.findField('srfkey');
                // if (_srfkey) {
                //     Object.assign(view.viewparam, { srfkey: _srfkey.getValue() });
                // }
            }
            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
                // Object.assign(view, { modalZIndex: viewController.modalZIndex });
            }
            var bcancel = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }
            if (this.pickupView && Object.keys(this.pickupView).length > 0) {
                var subject = new rxjs.Subject();
                Object.assign(view, this.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe(function (result) {
                    console.log(result);
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    var item = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }
                    if (_this.form) {
                        var valueField = _this.form.findField(_this.valueItem);
                        if (valueField) {
                            valueField.setValue(item.srfkey);
                        }
                        var itemField = _this.form.findField(_this.name);
                        if (itemField) {
                            itemField.setValue(item.srfmajortext);
                        }
                    }
                });
            }
        }
    }
});

"use strict";
Vue.component("ibiz-check-list", {
    template: "\n    <CheckboxGroup v-model=\"selects\" @on-change=\"onSelectChange\">\n        <template v-for=\"(item, index) of field.config\">\n            <Checkbox :label=\"item.value\" :disabled=\"field.disabled || item.disabled\">{{ item.text }}</Checkbox>\n        </template>\n    </CheckboxGroup>\n    ",
    props: ['field', 'name', 'mode', 'separator'],
    data: function () {
        var data = {
            selects: []
        };
        return data;
    },
    mounted: function () {
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            if (!this.mode) {
                this.mode = 'str';
            }
            if (!this.separator) {
                this.separator = ';';
            }
            if (Object.is(this.mode, 'num') && this.field) {
                this.selects = [];
                var num_1 = parseInt(newVal, 10);
                this.field.config.forEach(function (item) {
                    if ((num_1 & item.value) == item.value) {
                        _this.selects.push(item.value);
                    }
                });
            }
            else if (Object.is(this.mode, 'str')) {
                this.selects = newVal.split(this.separator);
            }
        }
    },
    methods: {
        'onSelectChange': function (select) {
            if (!this.mode) {
                this.mode = 'str';
            }
            if (!this.separator) {
                this.separator = ';';
            }
            var value = '';
            if (Object.is(this.mode, 'num')) {
                var temp_1 = 0;
                select.forEach(function (item) {
                    temp_1 = temp_1 | parseInt(item, 10);
                });
                value = temp_1 !== 0 ? temp_1.toString() : '';
            }
            else if (Object.is(this.mode, 'str')) {
                value = select.join(this.separator);
            }
            if (this.field) {
                this.field.setValue(value);
            }
        }
    },
});

"use strict";
Vue.component("ibiz-file-upload", {
    template: "\n    <el-upload :disabled=\"field.disabled\" :file-list=\"files\" :action=\"uploadUrl\" :before-upload=\"beforeUpload\" :on-success=\"onSuccess\" :on-error=\"onError\" :before-remove=\"onRemove\" :on-preview=\"onDownload\">\n        <el-button size=\"small\" icon=\"el-icon-upload\">\u4E0A\u4F20</el-button>\n    </el-upload>\n    ",
    props: ['field', 'name'],
    data: function () {
        var data = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        var _this = this;
        var form = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        var editorParams = {};
        var uploadparams = '';
        var exportparams = '';
        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }
        var upload_keys = uploadparams.split(';');
        var export_keys = exportparams.split(';');
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach(function (name) {
                if (editorParams.customparams[name]) {
                    _this.custom_arr.push(name + "=" + editorParams.customparams[name]);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
            _this.upload_arr = [];
            _this.export_arr = [];
            upload_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.upload_arr.push(key + "=" + _value);
            });
            export_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.export_arr.push(key + "=" + _value);
            });
            var uploadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            var downloadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
            if (_this.upload_arr.length > 0 || _this.custom_arr.length > 0) {
                uploadUrl = uploadUrl + "?" + _this.upload_arr.join('&') + (_this.upload_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.uploadUrl = uploadUrl;
            if (_this.export_arr.length > 0 || _this.custom_arr.length > 0) {
                downloadUrl = downloadUrl + "?" + _this.export_arr.join('&') + (_this.export_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.downloadUrl = downloadUrl;
            _this.files.forEach(function (file) {
                var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = _url + "?fileid=" + file.id;
                if (_this.export_arr.length > 0) {
                    _url = _url + "&" + _this.export_arr.join('&');
                }
                if (_this.custom_arr.length > 0) {
                    _url = _url + "&" + _this.custom_arr.join('&');
                }
                file.url = _url;
            });
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach(function (file) {
                    var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = _url + "?fileid=" + file.id;
                    if (_this.export_arr.length > 0) {
                        _url = _url + "&" + _this.export_arr.join('&');
                    }
                    if (_this.custom_arr.length > 0) {
                        _url = _url + "&" + _this.custom_arr.join('&');
                    }
                    file.url = _url;
                });
            }
            else {
                this.files = [];
            }
        }
    },
    methods: {
        beforeUpload: function (file) {
            console.log('上传之前');
        },
        onSuccess: function (response, file, fileList) {
            var arr = [];
            arr = response.files.slice();
            this.files.forEach(function (f) {
                arr.push({ name: f.name, id: f.id });
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onError: function (error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove: function (file, fileList) {
            var arr = [];
            fileList.forEach(function (f) {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onDownload: function (file) {
            window.open(file.url);
        }
    }
});

"use strict";
Vue.component("ibiz-mpicker", {
    template: "\n    <div style=\"position: relative;width: 100%;\">\n        <el-select :value=\"value\" multiple filterable remote :remote-method=\"onSearch\" size=\"small\" style=\"width:100%;\" @change=\"onSelect\" @remove-tag=\"onRemove\" :disabled=\"field.disabled\">\n            <el-option v-for=\"item in items\" :label=\"item.text\" :value=\"item.value\"></el-option>\n        </el-select>\n        <span style=\"position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;\">\n            <i class=\"el-icon-search\"  @click=\"openView\"></i>\n        </span>\n    </div>\n    ",
    props: ['field', 'name'],
    data: function () {
        var data = {
            http: IBizHttp.getInstance(),
            items: [],
            value: [],
            selectItems: []
        };
        Object.assign(data, this.field.editorParams);
        Object.assign(data, { form: this.field.getForm() });
        return data;
    },
    mounted: function () {
        // this.onSearch('');
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            this.value = [];
            this.selectItems = [];
            if (newVal) {
                this.selectItems = JSON.parse(newVal);
                this.selectItems.forEach(function (item) {
                    _this.value.push(item.srfkey);
                    var index = _this.items.findIndex(function (i) { return Object.is(i.value, item.srfkey); });
                    if (index < 0) {
                        _this.items.push({ text: item.srfmajortext, value: item.srfkey });
                    }
                });
            }
        }
    },
    methods: {
        'onSelect': function (selects) {
            var _this = this;
            var val = [];
            selects.forEach(function (select) {
                var index = _this.items.findIndex(function (item) { return Object.is(item.value, select); });
                if (index >= 0) {
                    var item = _this.items[index];
                    val.push({ srfkey: item.value, srfmajortext: item.text });
                }
                else {
                    index = _this.selectItems.findIndex(function (item) { return Object.is(item.srfkey, select); });
                    if (index >= 0) {
                        var item = _this.selectItems[index];
                        val.push(item);
                    }
                }
            });
            if (this.field) {
                var value = val.length > 0 ? JSON.stringify(val) : '';
                this.field.setValue(value);
            }
        },
        'onRemove': function (tag) {
            var index = this.selectItems.findIndex(function (item) { return Object.is(item.value, tag); });
            if (index >= 0) {
                this.selectItems.splice(index, 1);
                var value = this.selectItems.length > 0 ? JSON.stringify(this.selectItems) : '';
                if (this.field) {
                    this.field.setValue(value);
                }
            }
        },
        'onSearch': function (query) {
            var _this = this;
            if (this.url) {
                var param = {
                    srfaction: 'itemfetch',
                    query: query
                };
                if (this.form) {
                    Object.assign(param, { srfreferdata: JSON.stringify(this.form.getActiveData()) });
                }
                this.http.post(this.url, param).subscribe(function (data) {
                    _this.items = data.items;
                });
            }
        },
        'openView': function () {
            var _this = this;
            if (this.field && this.field.disabled) {
                return;
            }
            var view = { viewparam: {} };
            var viewController;
            if (this.form) {
                viewController = this.form.getViewController();
                Object.assign(view.viewparam, {
                    srfreferdata: JSON.stringify(this.form.getActiveData()),
                    selectedData: this.selectItems.slice()
                });
            }
            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }
            if (this.pickupView && Object.keys(this.pickupView).length > 0) {
                var subject = new rxjs.Subject();
                Object.assign(view, this.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe(function (result) {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    var selects = [];
                    if (result.selections && Array.isArray(result.selections)) {
                        result.selections.forEach(function (select) {
                            selects.push({ srfkey: select.srfkey, srfmajortext: select.srfmajortext });
                            var index = _this.items.findIndex(function (item) { return Object.is(item.value, select.srfkey); });
                            if (index < 0) {
                                _this.items.push({ text: select.srfmajortext, value: select.srfkey });
                            }
                        });
                    }
                    if (_this.field) {
                        var value = selects.length > 0 ? JSON.stringify(selects) : '';
                        _this.field.setValue(value);
                    }
                });
            }
        }
    }
});

"use strict";
Vue.component("ibiz-picture-upload", {
    template: "\n    <div class=\"ibiz-picture-upload\">\n        <ul class=\"el-upload-list el-upload-list--picture-card\">\n            <template v-for=\"(file,index) of files\">\n                <li :key=\"index\" class=\"el-upload-list__item is-success\">\n                    <img :src=\"file.url\" class=\"el-upload-list__item-thumbnail\" style=\"min-height:100px;min-width:100px;\">\n                    <a class=\"el-upload-list__item-name\">\n                        <i class=\"el-icon-document\"></i> {{ file.name }}\n                    </a>\n                    <i class=\"el-icon-close\"></i>\n                    <label class=\"el-upload-list__item-status-label\">\n                        <i class=\"el-icon-upload-success el-icon-check\"></i>\n                    </label>\n                    <span class=\"el-upload-list__item-actions\">\n                        <span class=\"el-upload-list__item-preview\">\n                            <i class=\"el-icon-zoom-in\" @click=\"onPreview(file)\"></i>\n                        </span>\n                        <span class=\"el-upload-list__item-download\">\n                            <i class=\"el-icon-download\" @click=\"onDownload(file)\"></i>\n                        </span>\n                        <span v-if=\"!field.disabled\" class=\"el-upload-list__item-delete\">\n                            <i class=\"el-icon-delete\" @click=\"onRemove(file)\"></i>\n                        </span>\n                    </span>\n                </li>\n            </template>\n        </ul>\n        <el-upload v-if=\"!field.disabled\" :show-file-list=\"false\" list-type=\"picture-card\" :file-list=\"files\" :action=\"uploadUrl\" :before-upload=\"beforeUpload\" :on-success=\"onSuccess\" :on-error=\"onError\">\n            <i class=\"el-icon-plus\"></i>\n        </el-upload>\n        <Modal v-model=\"dialogVisible\" footer-hide>\n            <img width=\"100%\" :src=\"dialogImageUrl\" alt=\"\">\n        </Modal>\n    <div>\n    ",
    props: ['field', 'name'],
    data: function () {
        var data = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            dialogVisible: false,
            dialogImageUrl: '',
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        var _this = this;
        var form = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        var editorParams = {};
        var uploadparams = '';
        var exportparams = '';
        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }
        var upload_keys = uploadparams.split(';');
        var export_keys = exportparams.split(';');
        var custom_arr = [];
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach(function (name) {
                if (editorParams.customparams[name]) {
                    custom_arr.push(name + "=" + editorParams.customparams[name]);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
            _this.upload_arr = [];
            _this.export_arr = [];
            upload_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.upload_arr.push(key + "=" + _value);
            });
            export_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.export_arr.push(key + "=" + _value);
            });
            var uploadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            var downloadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
            if (_this.upload_arr.length > 0 || _this.custom_arr.length > 0) {
                uploadUrl = uploadUrl + "?" + _this.upload_arr.join('&') + (_this.upload_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.uploadUrl = uploadUrl;
            if (_this.export_arr.length > 0 || _this.custom_arr.length > 0) {
                downloadUrl = downloadUrl + "?" + _this.export_arr.join('&') + (_this.export_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.downloadUrl = downloadUrl;
            _this.files.forEach(function (file) {
                var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = _url + "?fileid=" + file.id;
                if (_this.export_arr.length > 0) {
                    _url = _url + "&" + _this.export_arr.join('&');
                }
                if (_this.custom_arr.length > 0) {
                    _url = _url + "&" + _this.custom_arr.join('&');
                }
                file.url = _url;
            });
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach(function (file) {
                    var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = _url + "?fileid=" + file.id;
                    if (_this.export_arr.length > 0) {
                        _url = _url + "&" + _this.export_arr.join('&');
                    }
                    if (_this.custom_arr.length > 0) {
                        _url = _url + "&" + _this.custom_arr.join('&');
                    }
                    file.url = _url;
                });
            }
            else {
                this.files = [];
            }
        }
    },
    methods: {
        onPreview: function (file) {
            this.dialogImageUrl = file.url;
            this.dialogVisible = true;
        },
        beforeUpload: function (file) {
            console.log('上传之前');
        },
        onSuccess: function (response, file, fileList) {
            var arr = [];
            arr = response.files.slice();
            this.files.forEach(function (f) {
                arr.push({ name: f.name, id: f.id });
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onError: function (error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove: function (file) {
            if (this.field && this.field.disabled) {
                return;
            }
            var arr = [];
            this.files.forEach(function (f) {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onDownload: function (file) {
            window.open(file.url);
        }
    }
});

"use strict";
Vue.component("ibiz-rich-text-editor", {
    template: "\n    <textarea :id=\"id\"></textarea>\n    ",
    props: ['field', 'name', 'height'],
    data: function () {
        var data = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            editor: null,
            id: IBizUtil.createUUID()
        };
        return data;
    },
    mounted: function () {
        this.init('');
    },
    watch: {
        'field.value': function (newVal) {
            if (newVal) {
                tinymce.remove('#' + this.id);
                this.init(newVal);
            }
        },
        'field.disabled': function (disabled) {
            tinymce.remove('#' + this.id);
            this.init(this.field.value);
        }
    },
    destroyed: function () {
        tinymce.remove(this.editor);
    },
    methods: {
        init: function (val) {
            var _this_1 = this;
            var richtexteditor = this;
            tinymce.init({
                selector: '#' + this.id,
                language: 'zh_CN',
                branding: false,
                height: this.height,
                min_height: 400,
                plugins: ['link', 'paste', 'table', 'image', 'codesample', 'code', 'fullscreen', 'preview'],
                codesample_languages: [
                    { text: 'HTML/XML', value: 'markup' },
                    { text: 'JavaScript', value: 'javascript' },
                    { text: 'CSS', value: 'css' },
                    { text: 'PHP', value: 'php' },
                    { text: 'Ruby', value: 'ruby' },
                    { text: 'Python', value: 'python' },
                    { text: 'Java', value: 'java' },
                    { text: 'C', value: 'c' },
                    { text: 'C#', value: 'csharp' },
                    { text: 'C++', value: 'cpp' }
                ],
                setup: function (editor) {
                    _this_1.editor = editor;
                    editor.on('blur', function () {
                        var content = editor.getContent();
                        if (richtexteditor.field) {
                            richtexteditor.field.setValue(content);
                        }
                    });
                },
                images_upload_handler: function (bolbinfo, success, failure) {
                    var formData = new FormData();
                    formData.append('file', bolbinfo.blob(), bolbinfo.filename());
                    var _url = richtexteditor.uploadUrl;
                    richtexteditor.uploadFile(_url, formData).subscribe(function (response) {
                        if (response.ret === 0 && response.files.length > 0) {
                            var id = response.files[0].id;
                            var url = richtexteditor.downloadUrl + "?fileid=" + id;
                            success(url);
                        }
                    }, function (error) {
                        console.log(error);
                    });
                },
                init_instance_callback: function (editor) {
                    _this_1.editor = editor;
                    var value = (val && val.length > 0) ? val : '';
                    if (_this_1.editor) {
                        _this_1.editor.setContent(value);
                    }
                    if (_this_1.field.disabled) {
                        _this_1.editor.setMode('readonly');
                    }
                }
            });
        },
        uploadFile: function (url, formData) {
            var _this = this;
            var subject = new rxjs.Subject();
            axios({
                method: 'post',
                url: url,
                data: formData,
                headers: { 'Content-Type': 'image/png', 'Accept': 'application/json' },
            }).then(function (response) {
                if (response.ret === 0) {
                    subject.next(response);
                }
                else {
                    subject.error(response);
                }
            }).catch(function (response) {
                subject.error(response);
            });
            return subject;
        }
    }
});

"use strict";
Vue.component("ibiz-autocomplete", {
    template: "\n    <el-autocomplete size=\"small\" :value=\"field.value\" value-key=\"text\" clearable :fetch-suggestions=\"onSearch\" @select=\"onSelect\" @blur=\"onBlur\" @clear=\"onClear\" :disabled=\"field.disabled\" :trigger-on-focus=\"false\" style=\"width: 100%;\"></el-autocomplete>\n    ",
    props: ['field', 'name', 'width'],
    data: function () {
        var data = {
            http: IBizHttp.getInstance()
        };
        Object.assign(data, this.field.editorParams);
        Object.assign(data, { form: this.field.getForm() });
        return data;
    },
    methods: {
        onClear: function () {
            if (this.form) {
                var valueField = this.form.findField(this.valueItem);
                if (valueField) {
                    valueField.setValue('');
                }
                var itemField = this.form.findField(this.name);
                if (itemField) {
                    itemField.setValue('');
                }
            }
        },
        onBlur: function (e) {
            var val = e.target.value;
            if (!Object.is(val, this.field.value)) {
                this.onSelect({ text: val, value: '' });
            }
        },
        onSelect: function (item) {
            if (this.form) {
                var valueField = this.form.findField(this.valueItem);
                if (valueField) {
                    valueField.setValue(item.value);
                }
                var itemField = this.form.findField(this.name);
                if (itemField) {
                    itemField.setValue(item.text);
                }
            }
        },
        onSearch: function (query, callback) {
            var _this = this;
            var param = {
                srfaction: 'itemfetch',
                query: query,
            };
            if (_this.form) {
                Object.assign(param, { srfreferdata: JSON.stringify(_this.form.getActiveData()) });
            }
            _this.http.post(_this.url, param).subscribe(function (response) {
                var items = [];
                if (response.ret == 0) {
                    items = response.items;
                }
                callback(items);
            }, function () {
                callback([]);
            });
        }
    }
});

"use strict";
Vue.component("ibiz-echarts", {
    template: "\n    ",
    props: ['elementId', 'control'],
    data: function () {
        var data = {
            echartsObj: null
        };
        return data;
    },
    mounted: function () {
        var _this = this;
        _this.echartsObj = echarts.init(_this.$el);
        if (_this.control) {
            _this.control.on(IBizChart.LOADED).subscribe(function (data) {
                var opt = _this.renderData(data);
                _this.echartsObj.setOption(opt);
                _this.echartsObj.resize();
            });
        }
        else {
            _this.echartsObj.setOption({});
            _this.echartsObj.resize();
        }
        window.onresize = function () {
            setTimeout(function () {
                _this.echartsObj.resize();
            }, 1);
        };
    },
    watch: {},
    methods: {
        renderData: function (data) {
            if (data.series) {
                //区域图分析
                if (data.series.length) {
                    if (Object.is(data.series[0].type, 'area')) {
                        //如果series是数组的话
                        for (var i in data.series) {
                            data.series[i].type = 'line';
                            data.series[i].areaStyle = {};
                            Object.assign(data.series[i].areaStyle, { normal: {} });
                        }
                        return data;
                    }
                }
                else {
                    if (Object.is(data.series.type, 'area')) {
                        data.series.type = 'line';
                        data.series.areaStyle = {};
                        Object.assign(data.series.areaStyle, { normal: {} });
                        return data;
                    }
                }
                //雷达图分析
                if (data.series.length) {
                    if (Object.is(data.series[0].type, 'radar')) {
                        //1.找到每个角的最大值
                        var max = 0;
                        //获得每个角的数据
                        var arrs_1 = [];
                        for (var i in data.series) {
                            arrs_1.push(data.series[i].data);
                        }
                        var lastarrs = arrs_1[0].map(function (col, i) {
                            return arrs_1.map(function (row) {
                                return row[i];
                            });
                        });
                        var maxs = [];
                        for (var j in lastarrs) {
                            max = lastarrs[j][0];
                            for (var k in lastarrs[j]) {
                                if (max < lastarrs[j][k]) {
                                    max = lastarrs[j][k];
                                }
                            }
                            maxs.push(max);
                        }
                        // x轴数据转化成indicator数据
                        var indicatorArr = [];
                        for (var i in data.xAxis.data) {
                            for (var j in maxs) {
                                if (i === j) {
                                    indicatorArr.push({ name: data.xAxis.data[i], max: maxs[i] });
                                }
                            }
                        }
                        data.radar.indicator = [];
                        if (Array.isArray(indicatorArr)) {
                            data.radar.indicator = indicatorArr.slice();
                        }
                        data.xAxis = null;
                        // 设置series的data数据
                        for (var i in data.series) {
                            var valueArray = data.series[i].data;
                            var name_1 = data.series.name;
                            data.series[i].data = [];
                            data.series[i].data.push({ name: name_1, value: valueArray });
                        }
                    }
                }
            }
            return data;
        }
    }
});

"use strict";
Vue.component('ibiz-importdata-view', {
    template: "\n        <div class=\"ibiz-import-data\">\n            <el-upload accept=\"application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" ref=\"upload\"\n                :action=\"url\" :on-change=\"handleChange\" :file-list=\"fileList\" :auto-upload=\"false\" :show-file-list=\"false\">\n                <el-button slot=\"trigger\" size=\"small\" type=\"primary\">\u9009\u53D6\u6587\u4EF6</el-button>\n                <el-button style=\"margin-left: 10px;\" size=\"small\" type=\"primary\" @click=\"submitUpload\">\u4E0A\u4F20\u5230\u670D\u52A1\u5668</el-button>\n                <div class=\"down-data-template\"><a :href=\"downUrl\">\u4E0B\u8F7D\u5BFC\u5165\u6570\u636E\u6A21\u677F</a></div>\n            </el-upload>\n            <div class=\"import-files\">\n                <ul class=\"el-upload-list el-upload-list--text\">\n                    <li class=\"el-upload-list__item is-ready\" v-for=\"file in result\">\n                        <div>\n                            <a class=\"el-upload-list__item-name\">\n                                <i class=\"el-icon-document\"></i>{{file.name}}\n                            </a>\n                        </div>\n                        <div class=\"file-info\">\n                            <a v-if=\"file.response.errorfileid && file.response.errorfileid != ''\" class=\"errorLink\">\u4E0B\u8F7D\u5BFC\u5165\u5931\u8D25\u6570\u636E\u6587\u4EF6</a>\n                            <div v-if=\"file.response.processinfo && file.response.processinfo != ''\" v-html=\"file.response.processinfo\"></div>\n                        </div>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    ",
    props: ['params'],
    data: function () {
        var data = {
            fileList: [],
            url: "/" + IBizEnvironment.BaseUrl + IBizEnvironment.UploadDEData,
            downUrl: "/" + IBizEnvironment.BaseUrl + IBizEnvironment.ExportExcel,
            result: []
        };
        if (this.params) {
            Object.assign(data, this.params);
        }
        data.url = !Object.is(data.dename, '') ? data.url + "?srfdeid=" + data.dename : data.url;
        data.downUrl = !Object.is(data.dename, '') ? data.downUrl + "?srfdeid=" + data.dename : data.downUrl;
        return data;
    },
    methods: {
        submitUpload: function () {
            this.$refs.upload.submit();
        },
        handleChange: function (file, fileList) {
            var _this = this;
            if (!file.response) {
                Object.assign(file, { response: {} });
            }
            this.result = [];
            setTimeout(function () {
                _this.result = fileList.slice();
            });
            fileList.some(function (item) {
                if (item.response && item.response.ret === 0) {
                    _this.$emit('dataChange', { ret: 'OK' });
                    return true;
                }
            });
        }
    }
});

"use strict";
Vue.component("ibiz-card", {
    template: "\n    <div :class=\"classes\">\n        <div :class=\"headClasses\" v-if=\"showHead\">\n            <slot name=\"title\">\n                <p v-if=\"title\">\n                    <Icon v-if=\"icon\" :type=\"icon\"></Icon>\n                    <span>{{title}}</span>\n                </p>\n            </slot>\n            <div :class=\"extraClasses\" v-if=\"showExtra\"><slot name=\"extra\"></slot></div>\n            <div style=\"clear: both;\"></div>\n        </div>\n        <div :class=\"bodyClasses\" :style=\"bodyStyles\"><slot></slot></div>\n    </div>\n    ",
    props: {
        bordered: {
            type: Boolean,
            default: true
        },
        disHover: {
            type: Boolean,
            default: false
        },
        shadow: {
            type: Boolean,
            default: false
        },
        padding: {
            type: Number,
            default: 16
        },
        title: {
            type: String,
        },
        icon: {
            type: String,
        }
    },
    data: function () {
        return {
            showHead: true,
            showExtra: true,
            prefixCls: 'ivu-card'
        };
    },
    computed: {
        classes: function () {
            var _a;
            return [
                "" + this.prefixCls,
                (_a = {},
                    _a[this.prefixCls + "-bordered"] = this.bordered && !this.shadow,
                    _a[this.prefixCls + "-dis-hover"] = this.disHover || this.shadow,
                    _a[this.prefixCls + "-shadow"] = this.shadow,
                    _a)
            ];
        },
        headClasses: function () {
            return "ibiz-card-head";
        },
        extraClasses: function () {
            return this.prefixCls + "-extra";
        },
        bodyClasses: function () {
            return this.prefixCls + "-body";
        },
        bodyStyles: function () {
            if (this.padding !== 16) {
                return {
                    padding: this.padding + "px"
                };
            }
            else {
                return '';
            }
        }
    },
    mounted: function () {
        this.showHead = this.title || this.$slots.title !== undefined;
        this.showExtra = this.$slots.extra !== undefined;
    }
});

"use strict";
Vue.component('ibiz-tool-bar', {
    template: "\n    <div>\n        <template v-for=\"item in toolbar.items\">\n            <template v-if=\"item.type == 'SEPERATOR'\">\n                <span class=\"toolbar-seperator\">|</span>\n            </template>\n            <template v-else>\n                <!--  \u5E26\u5206\u7EC4\u6309\u94AE\uFF1ABEGIN  -->\n                <template v-if=\"item.items && item.items.length > 0\">\n                    <template v-if=\"item._dataaccaction\">\n                        <dropdown trigger=\"click\" transfer=\"true\" @on-click=\"menuOnClick\">\n                            <i-button :title=\"!item.showCaption ? item.caption : ''\" :disabled=\"item.disabled\"\n                                :class=\"item.class\">\n                                <i v-if=\"item.showIcon\" :class=\"item.iconClass\"></i>\n                                <span v-if=\"item.showCaption\">{{item.caption}}</span>\n                                <icon type=\"ios-arrow-down\"></icon>\n                            </i-button>\n\n                            <dropdown-menu slot=\"list\">\n                                <template v-for=\"submenu1 in item.items\">\n                                    <!--  \u5E26\u5206\u7EC4\u6309\u94AE\uFF1ABEGIN  -->\n                                    <template v-if=\"submenu1.items && submenu1.items.length > 0\">\n                                        <template v-if=\"submenu1._dataaccaction\">\n                                            <dropdown transfer=\"true\">\n\n                                                <dropdown-item :title=\"!submenu1.showCaption ? submenu1.caption : ''\"\n                                                    :disabled=\"submenu1.disabled\" :class=\"submenu1.class\">\n                                                    <i v-if=\"submenu1.showIcon\" :class=\"submenu1.iconClass\"></i>\n                                                    <span v-if=\"submenu1.showCaption\">{{submenu1.caption}}</span>\n                                                    <icon type=\"ios-arrow-forward\"></icon>\n                                                </dropdown-item>\n\n                                                <dropdown-menu slot=\"list\">\n                                                    <template v-for=\"submenu2 in submenu1.items\">\n                                                        <template v-if=\"submenu2._dataaccaction\">\n                                                            <dropdown-item\n                                                                :title=\"!submenu2.showCaption ? submenu2.caption : ''\"\n                                                                :disabled=\"submenu2.disabled\" :class=\"submenu2.class\"\n                                                                :name=\"submenu2.name\">\n                                                                <i v-if=\"submenu2.showIcon\" :class=\"submenu2.iconClass\"></i>\n                                                                <span\n                                                                    v-if=\"submenu2.showCaption\">{{submenu2.caption}}</span>\n                                                            </dropdown-item>\n                                                        </template>\n                                                    </template>\n                                                </dropdown-menu>\n                                            </dropdown>\n                                        </template>\n                                    </template>\n                                    <!--  \u5E26\u5206\u7EC4\u6309\u94AE\uFF1AEND  -->\n                                    <!--  \u4E0D\u5E26\u5206\u7EC4\u6309\u94AE\uFF1ABEGIN  -->\n                                    <template v-else>\n                                        <template v-if=\"submenu1._dataaccaction\">\n\n                                            <template v-if=\"submenu1.uiaction.tag == 'ExportExcel'\">\n                                                <dropdown trigger=\"custom\" transfer=\"true\"\n                                                    :visible=\"toolbar.exportMenuState\" @on-clickoutside=\"onClickoutside\"\n                                                    placement=\"bottom-start\" transfer=\"true\"\n                                                    transfer-class-name=\"second-transter-exportexcel-content\">\n                                                    <dropdown-item>\n                                                        <span @click=\"toolbar.exportMenuState = !toolbar.exportMenuState\">\n                                                            <i v-if=\"submenu1.showIcon\" :class=\"submenu1.iconClass\"></i>\n                                                            <span v-if=\"submenu1.showCaption\">{{submenu1.caption}}</span>\n                                                        </span>\n                                                    </dropdown-item>\n\n                                                    <dropdown-menu slot=\"list\">\n                                                        <dropdown-item>\n                                                            <p @click=\"toolbar.itemExportExcel('ExportExcel', 'all')\">\n                                                                {{submenu1.caption}}\u5168\u90E8(\u6700\u5927\u5BFC\u51FA{{submenu1.MaxRowCount}}\u884C)</p>\n                                                        </dropdown-item>\n                                                        <dropdown-item>\n                                                            <p @click=\"toolbar.itemExportExcel('ExportExcel')\">\n                                                                {{submenu1.caption}}\u5F53\u524D\u9875</p>\n                                                        </dropdown-item>\n                                                        <dropdown-item>\n                                                            {{submenu1.caption}}\u7B2C\n                                                            <i-input class=\"exportStartPage\"\n                                                                v-model=\"toolbar.exportStartPage\" style=\"width: 30px;\">\n                                                            </i-input>\n                                                            <i-input class=\"exportEndPage\" v-model=\"toolbar.exportEndPage\"\n                                                                style=\"width: 30px;\"></i-input>\n                                                            <i-button\n                                                                @click=\"toolbar.itemExportExcel('ExportExcel', 'custom')\">\n                                                                Go!</i-button>\n                                                        </dropdown-item>\n                                                    </dropdown-menu>\n                                                </dropdown>\n\n                                            </template>\n\n                                            <template v-else>\n\n                                                <dropdown-item :title=\"!submenu1.showCaption ? submenu1.caption : ''\"\n                                                    :disabled=\"submenu1.disabled\" :class=\"submenu1.class\"\n                                                    :name=\"submenu1.name\">\n                                                    <i v-if=\"submenu1.showIcon\" :class=\"submenu1.iconClass\"></i>\n                                                    <span v-if=\"submenu1.showCaption\">{{submenu1.caption}}</span>\n                                                </dropdown-item>\n                                            </template>\n\n                                        </template>\n                                    </template>\n                                    <!--  \u4E0D\u5E26\u5206\u7EC4\u6309\u94AE\uFF1AEND  -->\n                                </template>\n                            </dropdown-menu>\n                        </dropdown>\n                    </template>\n                </template>\n                <!--  \u5E26\u5206\u7EC4\u6309\u94AE\uFF1AEND  -->\n                <!--  \u4E0D\u5E26\u5206\u7EC4\u6309\u94AE\uFF1ABEGIN  -->\n                <template v-else>\n                    <template v-if=\"item._dataaccaction\">\n\n                        <template v-if=\"item.uiaction.tag == 'ExportExcel'\">\n\n                            <dropdown trigger=\"custom\" transfer=\"true\" :visible=\"toolbar.exportMenuState\"\n                                @on-clickoutside=\"onClickoutside\">\n                                <i-button :title=\"!item.showCaption ? item.caption : ''\" :disabled=\"item.disabled\"\n                                    :class=\"item.class\" @click=\"toolbar.exportMenuState = !toolbar.exportMenuState\">\n                                    <i v-if=\"item.showIcon\" :class=\"item.iconClass\"></i>\n                                    <span v-if=\"item.showCaption\">{{item.caption}}</span>\n                                    <icon type=\"ios-arrow-down\"></icon>\n                                </i-button>\n\n                                <dropdown-menu slot=\"list\">\n                                    <dropdown-item>\n                                        <p @click=\"toolbar.itemExportExcel('ExportExcel', 'all')\">\n                                            {{item.caption}}\u5168\u90E8(\u6700\u5927\u5BFC\u51FA{{item.MaxRowCount}}\u884C)</p>\n                                    </dropdown-item>\n                                    <dropdown-item>\n                                        <p @click=\"toolbar.itemExportExcel('ExportExcel')\">{{item.caption}}\u5F53\u524D\u9875</p>\n                                    </dropdown-item>\n                                    <dropdown-item>\n                                        {{item.caption}}\u7B2C\n                                        <i-input class=\"exportStartPage\" v-model=\"toolbar.exportStartPage\"\n                                            style=\"width: 30px;\"></i-input>\n                                        <i-input class=\"exportEndPage\" v-model=\"toolbar.exportEndPage\" style=\"width: 30px;\">\n                                        </i-input>\n                                        <i-button @click=\"toolbar.itemExportExcel('ExportExcel', 'custom')\">Go!</i-button>\n                                    </dropdown-item>\n                                </dropdown-menu>\n                            </dropdown>\n\n                        </template>\n\n                        <template v-else>\n                            <i-button :title=\"!item.showCaption ? item.caption : ''\" :disabled=\"item.disabled\"\n                                :class=\"item.class\" @click=\"toolbar.itemclick(item)\">\n                                <i v-if=\"item.showIcon\" :class=\"item.iconClass\"></i>\n                                <span v-if=\"item.showCaption\">{{item.caption}}</span>\n                            </i-button>\n                        </template>\n                    </template>\n                </template>\n                <!--  \u4E0D\u5E26\u5206\u7EC4\u6309\u94AE\uFF1AEND  -->\n            </template>\n        </template>\n    </div>\n    ",
    props: ['toolbar', 'viewController'],
    data: function () {
        var data = {};
        return data;
    },
    methods: {
        getItem: function (items, name) {
            var item = {};
            var _this = this;
            items.some(function (_item) {
                if (Object.is(_item.name, name)) {
                    Object.assign(item, _item);
                    return true;
                }
                if (_item.items && _item.items.length > 0) {
                    var subItem = _this.getItem(_item.items, name);
                    if (Object.keys(subItem).length > 0) {
                        Object.assign(item, subItem);
                        return true;
                    }
                }
            });
            return item;
        },
        menuOnClick: function (name) {
            if (!this.toolbar) {
                return;
            }
            var item = this.getItem(this.toolbar.getItems(), name);
            if (Object.keys(item).length === 0) {
                return;
            }
            this.toolbar.itemclick(item);
        },
        onClickoutside: function ($event) {
            if ($event && $event.target && $event.target.className.indexOf('ivu-input') !== -1) {
                this.toolbar.exportMenuState = true;
            }
            else {
                this.toolbar.exportMenuState = false;
            }
        }
    }
});

"use strict";
Vue.component('ibiz-grid-select-editor', {
    template: "\n            <i-select :value=\"itemvalue\" :placeholder=\"placeHolder\" :disabled=\"disabled\" clearable :transfer=\"true\" @on-change=\"onChange($event)\">\n                <i-option v-for=\"(item, index) of codelist\" :value=\"item.value\" :key=\"item.value\">{{ item.text }}</i-option>\n            </i-select>\n        ",
    props: {
        itemvalue: {
            type: String
        },
        codelist: {
            type: Array,
            default: []
        },
        codelisttype: {
            type: String,
        },
        url: {
            type: String,
        },
        name: {
            type: String,
        },
        text: {
            type: String,
        },
        disabled: {
            type: Boolean
        },
        placeHolder: {
            type: String
        },
        grid: {
            type: Object
        },
        data: {
            type: Object
        }
    },
    data: function () {
        var data = {};
        return data;
    },
    mounted: function () {
        var _this = this;
        if (Object.is(this.codelisttype, 'DYNAMIC')) {
            var iBizHttp = IBizHttp.getInstance();
            iBizHttp.post(this.url, {}).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.codelist = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    methods: {
        onChange: function ($event) {
            if (!this.grid) {
                return;
            }
            this.grid.colValueChange(this.name, $event, this.data);
            if (Object.is(this.codelisttype, 'DYNAMIC')) {
                var code = this.codelist.filter(function (item) { return Object.is(item.value, $event); });
                var text = (code && code.length === 1) ? code[0].text : '';
                this.grid.colValueChange(this.text, text, this.data);
            }
        }
    }
});

"use strict";
Vue.component('ibiz-grid-picker-editor', {
    template: "\n            <div class=\"ibiz-picker ibiz-grid-picker-editor\">\n            <el-autocomplete class=\"text-value\" v-if=\"editorType != 'dropdown' && editorType != 'pickup-no-ac'\" value-key=\"text\" :disabled=\"disabled\" v-model=\"value\" size=\"small\" :fetch-suggestions=\"onSearch\" @select=\"onACSelect\" @blur=\"onBlur\" style=\"width:100%;\">\n                <template slot=\"suffix\">\n                    <i v-if=\"value != '' && !disabled\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                    <i v-if=\"editorType != 'ac'\" class=\"el-icon-search\"  @click=\"openView\"></i>\n                </template>\n            </el-autocomplete>\n            <el-input class=\"text-value\" :value=\"value\" v-if=\"editorType == 'pickup-no-ac'\" readonly size=\"small\" :disabled=\"disabled\">\n                <template slot=\"suffix\">\n                    <i v-if=\"value != '' && !disabled\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                    <i class=\"el-icon-search\"  @click=\"openView\"></i>\n                </template>\n            </el-input>\n            <el-select v-if=\"editorType == 'dropdown'\" remote :remote-method=\"onSearch\" :value=\"value\" size=\"small\" filterable @change=\"onSelect\" :disabled=\"disabled\" style=\"width:100%;\" clearable @clear=\"onClear\" @visible-change=\"onSelectOpen\">\n                <el-option v-for=\"(item, index) of items\" :value=\"item.value\" :label=\"item.text\" :disabled=\"item.disabled\"></el-option>\n            </el-select>\n            <span v-if=\"editorType == 'dropdown'\" style=\"position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;\">\n                <i v-if=\"!open\" class=\"el-icon-arrow-down\"></i>\n                <i v-if=\"open\" class=\"el-icon-arrow-up\"></i>\n            </span>\n        </div>\n    ",
    props: {
        itemvalue: {
            type: String
        },
        editorType: {
            type: String,
        },
        editorParams: {
            type: Object
        },
        name: {
            type: String,
        },
        text: {
            type: String,
        },
        disabled: {
            type: Boolean
        },
        placeHolder: {
            type: String
        },
        grid: {
            type: Object
        },
        data: {
            type: Object
        }
    },
    data: function () {
        var data = {
            iBizHttp: IBizHttp.getInstance(),
            iBizNotification: IBizNotification.getInstance(),
            value: '',
            open: false,
            items: []
        };
        data.value = this.itemvalue;
        Object.assign(data, this.editorParams);
        return data;
    },
    mounted: function () {
        if (this.editorType == 'dropdown') {
            this.onSearch('');
        }
    },
    watch: {
        'itemvalue': function (newVal, oldVal) {
            this.value = newVal;
        }
    },
    methods: {
        onSelectOpen: function (flag) {
            this.open = flag;
        },
        onBlur: function () {
            if (this.field && this.value != this.field.value) {
                if (this.forceSelection) {
                    this.value = this.field.value;
                }
                else {
                    this.onACSelect({ text: this.value, value: '' });
                }
            }
        },
        //  填充条件
        fillPickupCondition: function (arg) {
            if (this.grid) {
                if (!this.data) {
                    this.iBizNotification.error('操作失败', '未能找到当前数据，无法继续操作');
                    return false;
                }
                if (this.itemParam && this.itemParam.fetchcond) {
                    var fetchparam = {};
                    var fetchCond = this.itemParam.fetchcond;
                    if (fetchCond) {
                        for (var cond in fetchCond) {
                            var value = this.data[fetchCond[cond]];
                            if (!value) {
                                this.iBizNotification.error('操作失败', '未能找到当前表格数据项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                Object.assign(arg, { srfreferitem: this.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.data) });
                return true;
            }
            else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        onSearch: function (query, func) {
            var _this = this;
            if (!this.url || Object.is(this.url, '')) {
                return;
            }
            var param = { srfaction: 'itemfetch', query: query };
            var bcancel = this.fillPickupCondition(param);
            if (!bcancel) {
                this.iBizNotification.warning({ title: '异常', desc: '条件不满足' });
                return;
            }
            this.iBizHttp.post(this.url, param).subscribe(function (data) {
                _this.items = data.items;
                if (typeof func == 'function') {
                    func(data.items);
                }
            });
        },
        onSelect: function (value) {
            var index = this.items.findIndex(function (item) { return Object.is(item.value, value); });
            if (index >= 0) {
                var item = this.items[index];
                this.onACSelect(item);
            }
        },
        // 清空选中数据
        onClear: function () {
            if (this.grid) {
                if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                    this.grid.colValueChange(this.valueItem, '', this.data);
                }
                this.grid.colValueChange(this.name, '', this.data);
            }
        },
        // 选中项设置
        onACSelect: function (item) {
            if (item === void 0) { item = {}; }
            if (this.grid) {
                if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                    this.grid.colValueChange(this.valueItem, item.value, this.data);
                }
                this.grid.colValueChange(this.name, item.text, this.data);
            }
        },
        // 打开选择视图
        openView: function () {
            var _this = this;
            if (this.disabled) {
                return;
            }
            var view = { viewparam: {} };
            var viewController = null;
            if (this.grid) {
                viewController = this.grid.getViewController();
            }
            if (this.data && Object.keys(this.data).length > 0) {
                Object.assign(view.viewparam, { srfkey: this.data['srfkey'] });
            }
            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }
            var bcancel = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }
            if (this.pickupView && Object.keys(this.pickupView).length > 0) {
                var subject = new rxjs.Subject();
                Object.assign(view, this.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe(function (result) {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    var item = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }
                    if (_this.grid) {
                        if (!Object.is(_this.valueItem, '') && _this.data.hasOwnProperty(_this.valueItem)) {
                            _this.grid.colValueChange(_this.valueItem, item.srfkey, _this.data);
                        }
                        _this.grid.colValueChange(_this.name, item.srfmajortext, _this.data);
                    }
                });
            }
        }
    }
});

"use strict";
Vue.component('ibiz-group-menu', {
    template: "\n        <div class=\"ibiz-group-menu\">\n            <row>\n                <i-col span=\"24\" class=\"first-level\" v-for=\"first in menu.items\" v-if=\"first.items && first.items.length > 0\">\n                    <div class=\"first-level-title\">\n                        <h2>\n                            <span class=\"first-level-title-icon\">\n                                <i v-if=\"first.icon == ''\" :class=\"[first.iconcls == '' ? '' : first.iconcls ]\"></i>\n                                <img v-else :src=\"first.icon\"/>\n                            </span>\n                            <span class=\"first-level-title-text\">{{first.text}}</span>\n                        </h2>\n                    </div>\n                    <row>\n\t                    <i-col :xs=\"6\" v-for=\"second in first.items\" class=\"second-level\">\n\t\t                    <div @click=\"openMenu(second)\">\n                                <div class=\"second-level-icon\">\n                                    <i v-if=\"second.icon == ''\" :class=\"[second.iconcls == '' ? 'fa fa-cogs' : second.iconcls ]\"></i>\n                                    <img v-else :src=\"second.icon\"/>\n                                </div>\n\t\t                        <h4 class=\"second-level-text\">{{second.text}}</h4>\n\t\t                    </div>\n\t\t                </i-col>\n                    </row>\n                </i-col>\n            </row>\n        </div>\n    ",
    props: {
        menu: {
            type: Object
        },
        viewController: {
            type: Object
        }
    },
    data: function () {
        var data = {};
        return data;
    },
    mounted: function () {
    },
    methods: {
        // 打开菜单节点
        openMenu: function (second) {
            if (second === void 0) { second = {}; }
            if (this.menu) {
                this.menu.onSelectChange(second);
            }
        }
    }
});

"use strict";
Vue.component("ibiz-picture-one-upload", {
    template: "\n    <div class=\"ibiz-picture-upload\">\n        <ul class=\"el-upload-list el-upload-list--picture-card\">\n            <template v-for=\"(file,index) of files\">\n                <li :key=\"index\" class=\"el-upload-list__item is-success\" :style=\"sytleObj\">\n                    <img :src=\"file.url\" class=\"el-upload-list__item-thumbnail\" :style=\"sytleObj\" style=\"min-height:100px;min-width:100px;\">\n                    <a class=\"el-upload-list__item-name\">\n                        <i class=\"el-icon-document\"></i> {{ file.name }}\n                    </a>\n                    <i class=\"el-icon-close\"></i>\n                    <label class=\"el-upload-list__item-status-label\">\n                        <i class=\"el-icon-upload-success el-icon-check\"></i>\n                    </label>\n                    <span class=\"el-upload-list__item-actions\">\n                        <span class=\"el-upload-list__item-preview\">\n                            <i class=\"el-icon-zoom-in\" @click=\"onPreview(file)\"></i>\n                        </span>\n                        <span class=\"el-upload-list__item-download\">\n                            <i class=\"el-icon-download\" @click=\"onDownload(file)\"></i>\n                        </span>\n                        <span v-if=\"!field.disabled\" class=\"el-upload-list__item-delete\">\n                            <i class=\"el-icon-delete\" @click=\"onRemove(file)\"></i>\n                        </span>\n                    </span>\n                </li>\n            </template>\n        </ul>\n        <el-upload v-if=\"showUpload\" :show-file-list=\"false\" list-type=\"picture-card\" :file-list=\"files\" :action=\"uploadUrl\" :before-upload=\"beforeUpload\" :on-success=\"onSuccess\" :on-error=\"onError\">\n            <i class=\"el-icon-plus\"></i>\n        </el-upload>\n        <Modal v-model=\"dialogVisible\" footer-hide>\n            <img width=\"100%\" :src=\"dialogImageUrl\" alt=\"\">\n        </Modal>\n    <div>\n    ",
    props: ['field', 'name', 'width', 'height'],
    data: function () {
        var data = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            showUpload: true,
            dialogVisible: false,
            dialogImageUrl: '',
            sytleObj: {
                width: this.width ? this.width + "px !important" : '100%',
                height: this.height ? this.height + "px !important" : '100%',
            },
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        var _this = this;
        var form = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        var editorParams = {};
        var uploadparams = '';
        var exportparams = '';
        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }
        var upload_keys = uploadparams.split(';');
        var export_keys = exportparams.split(';');
        var custom_arr = [];
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach(function (name) {
                if (editorParams.customparams[name]) {
                    custom_arr.push(name + "=" + editorParams.customparams[name]);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
            _this.upload_arr = [];
            _this.export_arr = [];
            upload_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.upload_arr.push(key + "=" + _value);
            });
            export_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.export_arr.push(key + "=" + _value);
            });
            var uploadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            var downloadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
            if (_this.upload_arr.length > 0 || _this.custom_arr.length > 0) {
                uploadUrl = uploadUrl + "?" + _this.upload_arr.join('&') + (_this.upload_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.uploadUrl = uploadUrl;
            if (_this.export_arr.length > 0 || _this.custom_arr.length > 0) {
                downloadUrl = downloadUrl + "?" + _this.export_arr.join('&') + (_this.export_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.downloadUrl = downloadUrl;
            _this.files.forEach(function (file) {
                var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = _url + "?fileid=" + file.id;
                if (_this.export_arr.length > 0) {
                    _url = _url + "&" + _this.export_arr.join('&');
                }
                if (_this.custom_arr.length > 0) {
                    _url = _url + "&" + _this.custom_arr.join('&');
                }
                file.url = _url;
            });
            _this.showUpload = !_this.field.disabled && _this.files.length !== 1;
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach(function (file) {
                    var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = _url + "?fileid=" + file.id;
                    if (_this.export_arr.length > 0) {
                        _url = _url + "&" + _this.export_arr.join('&');
                    }
                    if (_this.custom_arr.length > 0) {
                        _url = _url + "&" + _this.custom_arr.join('&');
                    }
                    file.url = _url;
                });
            }
            else {
                this.files = [];
            }
            this.showUpload = !this.field.disabled && this.files.length !== 1;
        }
    },
    methods: {
        onPreview: function (file) {
            this.dialogImageUrl = file.url;
            this.dialogVisible = true;
        },
        beforeUpload: function (file) {
            console.log('上传之前');
        },
        onSuccess: function (response, file, fileList) {
            var arr = [];
            arr = response.files.slice();
            this.files.forEach(function (f) {
                arr.push({ name: f.name, id: f.id });
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
            this.showUpload = !this.field.disabled && this.files.length !== 1;
        },
        onError: function (error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove: function (file) {
            if (this.field && this.field.disabled) {
                return;
            }
            var arr = [];
            this.files.forEach(function (f) {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
            this.showUpload = !this.field.disabled && this.files.length !== 1;
        },
        onDownload: function (file) {
            window.open(file.url);
        }
    }
});

"use strict";
Vue.component('ibiz-range-picker', {
    template: "\n\t<div style=\"display: flex;\">\n\t\t<template v-for=\"(formitem,index) of formItems\">\n\t\t\t<div v-if=\"index > 0\" style=\"text-align: center;width: 50px;\">\u2014</div>\n\t\t\t<template v-if=\"format\">\n\t\t\t\t<date-picker v-if=\"format == 'yyyy-MM-dd HH:mm:ss'\" type=\"datetime\" :format=\"format\"\n\t\t\t\t\t:value=\"formitem.value\"\n\t\t\t\t\t:disabled=\"field.disabled\" transfer\n\t\t\t\t\t@on-change=\"formitem.setValue($event)\" style=\"flex-grow: 1;\">\n\t\t\t\t</date-picker>\n\t\t\t\t<date-picker v-else-if=\"format == 'yyyy-MM-dd' || format == 'YYYY-MM-DD'\" type=\"date\" format=\"yyyy-MM-dd\"\n\t\t\t\t\t:value=\"formitem.value\"\n\t\t\t\t\t:disabled=\"field.disabled\" transfer\n\t\t\t\t\t@on-change=\"formitem.setValue($event)\" style=\"flex-grow: 1;\">\n\t\t\t\t</date-picker>\n\t\t\t\t<time-picker v-else type=\"time\" :format=\"format\" :value=\"formitem.value\" \n\t\t\t\t\t:disabled=\"field.disabled\" transfer \n\t\t\t\t\t@on-change=\"formitem.setValue($event)\">\n\t\t\t\t</time-picker>\n\t\t\t</template>\n\t\t\t<template v-else>\n\t\t\t\t<i-input v-model=\"formitem.value\" :disabled=\"field.disabled\"></i-input>\n\t\t\t</template>\n\t\t</template>\n\t</div>\n\t",
    props: ['field', 'format', 'editorType', 'refFormItem'],
    data: function () {
        var data = {
            formItems: []
        };
        var formItems = [];
        Object.assign(data, { form: this.field.getForm() });
        formItems = this.refFormItem.split(';');
        formItems.forEach(function (item) {
            var field = data.form.findField(item);
            if (field) {
                data.formItems.push(field);
            }
        });
        return data;
    }
});
