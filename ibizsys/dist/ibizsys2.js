"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * agGrid控件
 *
 * @class IBizDataGridPro
 * @extends {IBizDataGrid}
 */
var IBizDataGrid2 = /** @class */ (function (_super) {
    __extends(IBizDataGrid2, _super);
    /**
     *Creates an instance of IBizDataGrid2.
     * @param {*} [opts={}]
     * @memberof IBizDataGrid2
     */
    function IBizDataGrid2(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * agGrid控件api对象
         *
         * @type {*}
         * @memberof IBizDataGridPro
         */
        _this.agGridApi = null;
        /**
         * agGrid列api对象
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.agGridColumnApi = null;
        /**
         * agGrid列数组
         *
         * @type {Array<any>}
         * @memberof IBizDataGrid2
         */
        _this.columnDefs = [];
        /**
         * 树表格列模型
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.treeColMode = {};
        /**
         * 自定义分组列
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.autoGroupColumn = null;
        /**
         * 列过滤参数
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.colFilterParams = {};
        /**
         * 是否开启列过滤
         *
         * @type {boolean}
         * @memberof IBizDataGrid2
         */
        _this.isOpenFilter = false;
        if (Object.is(_this.getGridStyle(), 'TREEGRID')) {
            _this.setTreeColMode();
        }
        _this.setIsOpenFilter();
        _this.regColumnDefs();
        if (!_this.autoGroupColumn) {
            _this.setCheckboxColumn();
        }
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        if (!opt.start) {
            Object.assign(opt, { start: (this.curPage - 1) * this.limit });
        }
        if (!opt.limit) {
            Object.assign(opt, { limit: this.limit });
        }
        Object.assign(opt, { sort: JSON.stringify(this.gridSortField) });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        if (this.setIsOpenFilter) {
            Object.assign(opt, this.colFilterParams);
        }
        this.allChecked = false;
        this.indeterminate = false;
        this.selection = [];
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            _this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = _this.rendererDatas(response.items);
            _this.totalrow = response.totalrow;
            if (response.summaryitem) {
                _this.setPinnedBottomRow(response.summaryitem);
            }
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            _this.endLoading();
            console.log(error.info);
        });
    };
    /**
     * 表格准备完成
     *
     * @param {*} obj
     * @memberof IBizDataGridPro
     */
    IBizDataGrid2.prototype.onGridReady = function () {
        var _this = this;
        return function (params) {
            _this.agGridApi = params.api;
            _this.agGridColumnApi = params.columnApi;
        };
    };
    /**
     * 排序
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid2.prototype.elementSortChange = function () {
        var _this = this;
        return function () {
            var sorts = _this.agGridApi.getSortModel();
            if (sorts.length > 0) {
                if (Object.is(sorts[0].colId, 'ag-Grid-AutoColumn')) {
                    _this.sort(_this.autoGroupColumn.field, sorts[0].sort);
                }
                else {
                    _this.sort(sorts[0].colId, sorts[0].sort);
                }
            }
        };
    };
    /**
     * 选中值变化
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.selectionChanged = function () {
        var _this = this;
        return function () {
            if (_this.openRowEdit) {
                _this.agGridApi.deselectAll();
                return;
            }
            _this.selection = _this.agGridApi.getSelectedRows();
            _this.fire(IBizMDControl.SELECTIONCHANGE, _this.selection);
        };
    };
    IBizDataGrid2.prototype.isRowSelectable = function () {
        var _this = this;
        return function () {
            return !_this.openRowEdit;
        };
    };
    /**
     * 行双击
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.rowDbClicked = function () {
        var _this = this;
        return function (param) {
            if (!_this.openRowEdit && !Object.is(param.rowPinned, 'bottom') && !Object.is(param.rowPinned, 'top')) {
                _this.fire(IBizDataGrid.ROWDBLCLICK, _this.selection);
            }
        };
    };
    /**
     * 设置是否支持多选
     *
     * @param {boolean} state 是否支持多选
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.setMultiSelect = function (state) {
        _super.prototype.setMultiSelect.call(this, state);
        this.autoGroupColumn.headerCheckboxSelection = state;
        if (this.agGridApi) {
            this.agGridApi.refreshHeader();
        }
    };
    /**
     * 注册多数据列
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.regColumnDefs = function () {
    };
    /**
     * 注册多数据列
     *
     * @param {*} column
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.regColumnDef = function (column) {
        if (Object.keys(column).length === 0) {
            return;
        }
        this.columnDefs.push(column);
        this.regColumn(column);
    };
    /**
     * 设置多数据列头
     *
     * @param {*} [column={}]
     * @returns {void}
     * @memberof IBizMDControl
     */
    IBizDataGrid2.prototype.regColumn = function (column) {
        var _this = this;
        if (column === void 0) { column = {}; }
        if (Object.keys(column).length === 0) {
            return;
        }
        if (!this.columns) {
            this.columns = [];
        }
        if (Object.is(column.colType, 'GROUPGRIDCOLUMN') && column.children) {
            column.children.forEach(function (col) {
                _this.regColumn(col);
            });
        }
        else {
            if (Object.is(this.getGridStyle(), 'TREEGRID') && this.treeColMode.parentText && Object.is(this.treeColMode.text, column.field)) {
                this.setAutoTreeColumn(column);
                column.hide = true;
            }
            else if (column.rowGroupIndex === 1) {
                this.setAutoGroupColumn(column);
                column.hide = true;
            }
            if (column.filter) {
                this.colFilterParams[column.searchName] = '';
            }
            this.columns.push(column);
        }
    };
    /**
     * 默认展开分组层级
     * {-1: 全部展开, 0: 不展开, 1: 展开一级}
     * @returns {number}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getGroupDefExpand = function () {
        return -1;
    };
    /**
     * 分组列
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getAutoGroupCol = function () {
        return this.autoGroupColumn;
    };
    /**
     * 获取树数据层级结构
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getTreePath = function () {
        return function (data) {
            return data.paths;
        };
    };
    /**
     * 获取表格样式类型
     *
     * @returns {string}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getGridStyle = function () {
        return '';
    };
    /**
     * 设置树模型
     *
     * @param {*} [column={}]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setTreeColMode = function () {
    };
    /**
     * 设置自定义树列
     *
     * @param {*} [column={}]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setAutoTreeColumn = function (column) {
        if (column === void 0) { column = {}; }
        if (!this.autoGroupColumn) {
            this.autoGroupColumn = {};
        }
        Object.assign(this.autoGroupColumn, column);
        delete this.autoGroupColumn.hide;
        this.autoGroupColumn.cellRendererParams = {
            checkbox: true,
            suppressCount: true,
        };
    };
    /**
     * 设置自定义分组列
     *
     * @param {*} [column={}]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setAutoGroupColumn = function (column) {
        if (column === void 0) { column = {}; }
        if (!this.autoGroupColumn) {
            this.autoGroupColumn = {};
        }
        Object.assign(this.autoGroupColumn, column);
        delete this.autoGroupColumn.hide;
        this.autoGroupColumn.cellRendererParams = {
            checkbox: true,
            suppressCount: true,
        };
    };
    /**
     * 创建复选框列
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setCheckboxColumn = function () {
        if (!this.autoGroupColumn) {
            this.autoGroupColumn = {};
        }
        Object.assign(this.autoGroupColumn, {
            width: 42,
            maxWidth: 42,
            pinned: 'left',
            field: 'srf_grid_checkbox',
            suppressMovable: true,
            suppressResize: true,
            suppressMenu: true,
            suppressSorting: true,
            suppressFilter: true,
            checkboxSelection: true,
            suppressSizeToFit: true,
            headerCheckboxSelection: true
        });
        if (this.columnDefs) {
            this.columnDefs.push(this.autoGroupColumn);
        }
    };
    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.rendererDatas = function (items) {
        var _this = this;
        if (Object.is(this.getGridStyle(), 'TREEGRID') && this.treeColMode.parentId && this.treeColMode.id) {
            var arr = items.slice();
            arr.forEach(function (item) {
                item.paths = [];
                _this.setTreePath(items, item, item.paths);
            });
        }
        return items;
    };
    /**
     * 设置树路径
     *
     * @param {Array<any>} items
     * @param {*} [item={}]
     * @param {Array<string>} [paths=[]]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setTreePath = function (items, item, paths) {
        var _this = this;
        if (item === void 0) { item = {}; }
        if (paths === void 0) { paths = []; }
        var id = this.treeColMode.id;
        var text = this.treeColMode.text ? this.treeColMode.text : 'srfmajortext';
        var parentId = this.treeColMode.parentId;
        var parentText = this.treeColMode.parentText;
        if (item[parentId] && !Object.is(item[parentId], '')) {
            var hesParent_1 = true;
            items.forEach(function (data) {
                if (Object.is(data[id], item[parentId])) {
                    _this.setTreePath(items, data, paths);
                    hesParent_1 = false;
                }
            });
            if (hesParent_1 && parentText) {
                var index = items.indexOf(item);
                var data = {};
                data[id] = item[parentId];
                data[text] = item[parentText];
                data.paths = [item[parentText]];
                items.splice(index, 0, data);
                paths.push(item[parentText]);
            }
        }
        paths.push(item[text]);
    };
    /**
     * 是否开启行编辑
     *
     * @returns {boolean}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getIsOpenEdit = function () {
        var _this = this;
        return function (param) {
            return (_this.openRowEdit && param.node && !param.node.rowPinned);
        };
    };
    /**
     * 启用行编辑
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid2.prototype.openEdit = function (params) {
        var _this = this;
        if (!this.isEnableRowEdit) {
            this.iBizNotification.info('提示', '未启用行编辑');
            return;
        }
        this.agGridApi.deselectAll();
        if (!this.openRowEdit) {
            this.openRowEdit = true;
            this.items.forEach(function (item) { item.openeditrow = true; });
            this.backupDatas = JSON.parse(JSON.stringify(this.items)).slice();
            this.items.forEach(function (item, index) {
                _this.setEditItemState(item.srfkey);
                _this.editRow(item, index, false);
            });
        }
    };
    /**
     * 行编辑单元格值变化
     *
     * @param {string} name
     * @param {*} data
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.colValueChange = function (name, value, data) {
        var srfkey = data.srfkey;
        var _data = this.items.find(function (back) { return Object.is(back.srfkey, srfkey); });
        if (this.state[srfkey] && this.state[srfkey][name]) {
            data[name] = value;
            this.fire(IBizDataGrid.UPDATEGRIDITEMCHANGE, { name: name, data: data });
        }
    };
    /**
     * 获取语言资源
     *
     * @returns {*}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getLocaleText = function () {
        return IBizDataGrid2.LOCALETEXT;
    };
    /**
     * 设置是否开启列过滤
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setIsOpenFilter = function () {
    };
    /**
     * 获取列过滤字段值
     *
     * @param {*} param
     * @returns {*}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getColFilterValue = function (param) {
        if (!param) {
            return;
        }
        var filterModel = this.agGridApi.getFilterModel();
        var defCol = param.colDef;
        if (defCol && filterModel[defCol.field]) {
            return filterModel[defCol.field].filter;
        }
        return '';
    };
    /**
     * 过滤值改变
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.filterChanged = function () {
        var _this = this;
        return function () {
            var filterModel = _this.agGridApi.getFilterModel();
            var keys = Object.keys(_this.colFilterParams);
            var isChange = false;
            keys.forEach(function (key) {
                if (filterModel) {
                    var index = _this.columns.findIndex(function (column) { return Object.is(column.searchName, key); });
                    var column = _this.columns[index];
                    if (filterModel.hasOwnProperty(column.field)) {
                        if (Object.is(_this.colFilterParams[key], filterModel[column.field].filter)) {
                            return;
                        }
                        else {
                            _this.colFilterParams[key] = filterModel[column.field].filter;
                            isChange = true;
                        }
                    }
                    else if (Object.is(_this.colFilterParams[key], '')) {
                        return;
                    }
                    else {
                        _this.colFilterParams[key] = '';
                        isChange = true;
                    }
                }
                else if (Object.is(_this.colFilterParams[key], '')) {
                    return;
                }
                else {
                    _this.colFilterParams[key] = '';
                    isChange = true;
                }
            });
            if (isChange) {
                _this.refresh();
            }
        };
    };
    /**
     * 获取预置组件
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getFrameworkComponents = function () {
        return {
            customTextColumnFilter: Vue.options.components['costom-text-column-filter'],
            customSelectColumnFilter: Vue.options.components['costom-select-column-filter'],
        };
    };
    /**
     * 获取列默认设置
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getDefColOption = function () {
        var _this = this;
        return {
            menuTabs: ['generalMenuTab', 'columnsMenuTab'],
            pinnedRowCellRenderer: function (data) {
                return _this.getPinnedRowCellRender(data);
            }
        };
    };
    /**
     * 设置底部固定行
     *
     * @param {*} item
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setPinnedBottomRow = function (item) {
        if (item) {
            if (Array.isArray(item)) {
                this.agGridApi.setPinnedBottomRowData(item);
            }
            else {
                this.agGridApi.setPinnedBottomRowData([item]);
            }
        }
    };
    /**
     * 获取固定行列绘制
     *
     * @param {*} data
     * @returns {string}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getPinnedRowCellRender = function (data) {
        return data.value;
    };
    /**
     * 表格模型变化
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.onGridModelChange = function () {
        var _this = this;
        var subject = new rxjs.Subject();
        subject.pipe(rxjs.operators.debounceTime(2000)).subscribe(function () {
            _this.setModel(_this.agGridColumnApi.getColumnState());
        });
        return function () {
            subject.next();
        };
    };
    /**
     * 多语言设置
     *
     * @static
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.LOCALETEXT = {
        pinColumn: '锁定列',
        autosizeThiscolumn: '自动调整此列的大小',
        autosizeAllColumns: '自动调整所有列的大小',
        resetColumns: '重置所有列',
        pinLeft: '锁定左侧',
        pinRight: '锁定右侧',
        noPin: '不锁定',
        noRowsToShow: '暂无数据'
    };
    return IBizDataGrid2;
}(IBizDataGrid));

"use strict";
Vue.component('ibiz-ag-gird-paging', {
    template: "\n\t\t<page @on-change=\"onChange\" @on-page-size-change=\"onPageSizeChange\" :transfer=\"true\" :total=\"grid.totalrow\"\n            show-sizer :page-size=\"20\" :page-size-opts=\"[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]\" show-elevator show-total>\n            <span><span class=\"page-refresh\"><i-button icon=\"md-refresh\" title=\"\u5237\u65B0\" @click=\"onRefresh\"></i-button></span>\u663E\u793A {{ (grid.curPage - 1)*grid.limit + 1}} - {{grid.totalrow > grid.curPage*grid.limit ? grid.curPage*grid.limit : grid.totalrow}} \u6761\uFF0C\u5171 {{grid.totalrow}} \u6761</span>\n        </page>\n\t",
    props: ['agGrid', 'grid'],
    data: function () {
        return {};
    },
    methods: {
        'onPageSizeChange': function (data) {
            this.$emit('on-page-size-change', data);
        },
        'onChange': function (data) {
            this.$emit('on-change', data);
        },
        'onRefresh': function () {
            this.$emit('refresh');
        }
    }
});

"use strict";
Vue.component('ibiz-grid-datepicker', {
    template: "\n    <date-picker style=\"width: 100%;\" type=\"datetime\" :format=\"fmt\" v-model=\"value\" transfer @on-change=\"setValue\"></date-picker>\n    ",
    data: function () {
        return {
            value: '',
            fmt: '',
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.value = this.params.value;
        this.fmt = this.params.fmt;
    },
    methods: {
        getValue: function () {
            return this.value;
        },
        setValue: function (val) {
            this.grid.colValueChange(this.column.name, val, this.rowData);
        }
    }
});

"use strict";
Vue.component('ibiz-grid-timepicker', {
    template: "\n    <time-picker style=\"width: 100%;\" type=\"time\" :format=\"fmt\" v-model=\"value\" transfer @on-change=\"setValue\"></time-picker>\n    ",
    data: function () {
        return {
            value: '',
            fmt: '',
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        this.value = this.params.value;
        this.fmt = this.params.fmt;
    },
    methods: {
        getValue: function () {
            return this.value;
        },
        setValue: function (val) {
            this.grid.colValueChange(this.column.name, val, this.rowData);
        }
    }
});

"use strict";
Vue.component('ibiz-grid-span', {
    template: "\n    <i-input :value=\"value\" style=\"width: 100%\" disabled readonly/>\n    ",
    data: function () {
        return {
            value: ''
        };
    },
    created: function () {
        this.value = this.params.value;
    },
    methods: {
        getValue: function () {
            return this.value;
        }
    }
});

"use strict";
Vue.component('ibiz-grid-input', {
    template: "\n        <i-input v-model=\"value\" placeholder=\"\u8BF7\u8F93\u5165\" style=\"width: 100%\" @on-change=\"onChange\"/>\n    ",
    data: function () {
        return {
            value: '',
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        this.value = this.params.value;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.grid = this.params.grid;
    },
    methods: {
        getValue: function () {
            return this.value;
        },
        onChange: function () {
            this.grid.colValueChange(this.column.name, this.value, this.rowData);
        }
    }
});

"use strict";
Vue.component('ibiz-grid-select', {
    template: "\n    <i-select v-model=\"value\" style=\"width:100%\" transfer label-in-value @on-change=\"onValueChange\">\n        <template v-for=\"(item, index) of items\">\n            <i-option :value=\"item.value\" :key=\"index\">{{ item.text }}</i-option>\n        </template>\n    </i-select>\n    ",
    data: function () {
        return {
            value: '',
            text: '',
            items: [],
            http: IBizHttp.getInstance(),
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        var _this = this;
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        if (Object.is(this.params.selectType, 'STATIC')) {
            this.value = this.params.value;
            var viewController = this.grid.getViewController();
            this.items = viewController.getCodeList(this.params.codelistId).data;
        }
        else if (Object.is(this.params.selectType, 'DYNAMIC')) {
            this.value = this.rowData[this.column.name];
            this.text = this.params.value;
            var param = {};
            param.srfreferdata = JSON.stringify(this.rowData);
            this.http.post(this.params.url, param).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.items = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    methods: {
        getValue: function () {
            if (Object.is(this.params.selectType, 'DYNAMIC')) {
                return this.text;
            }
            else {
                return this.value;
            }
        },
        onValueChange: function (item) {
            if (Object.is(this.params.selectType, 'DYNAMIC')) {
                this.text = item.label;
            }
            this.grid.colValueChange(this.column.name, item.value, this.rowData);
        }
    }
});

"use strict";
Vue.component('ibiz-grid-picker', {
    template: "\n    <div class=\"ibiz-picker\">\n        <el-autocomplete class=\"text-value\" v-if=\"editorType != 'dropdown' && editorType != 'pickup-no-ac'\" value-key=\"text\" v-model=\"value\" size=\"small\" :fetch-suggestions=\"onSearch\" @select=\"onACSelect\" @blur=\"onBlur\" style=\"width:100%;\">\n            <template slot=\"suffix\">\n                <i v-if=\"value != ''\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                <i v-if=\"editorType != 'ac'\" class=\"el-icon-search\"  @click=\"openView\"></i>\n            </template>\n        </el-autocomplete>\n        <el-input class=\"text-value\" :value=\"value\" v-if=\"editorType == 'pickup-no-ac'\" readonly size=\"small\">\n            <template slot=\"suffix\">\n                <i v-if=\"value != ''\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                <i class=\"el-icon-search\"  @click=\"openView\"></i>\n            </template>\n        </el-input>\n        <el-select v-if=\"editorType == 'dropdown'\" remote :remote-method=\"onSearch\" :value=\"value\" size=\"small\" filterable @change=\"onSelect\" style=\"width:100%;\" clearable @clear=\"onClear\" @visible-change=\"onSelectOpen\">\n            <el-option v-for=\"(item, index) of items\" :value=\"item.value\" :label=\"item.text\" :disabled=\"item.disabled\"></el-option>\n        </el-select>\n        <span v-if=\"editorType == 'dropdown'\" style=\"position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;\">\n            <i v-if=\"!open\" class=\"el-icon-arrow-down\"></i>\n            <i v-if=\"open\" class=\"el-icon-arrow-up\"></i>\n        </span>\n    </div>\n    ",
    data: function () {
        return {
            value: '',
            editorType: '',
            iBizHttp: IBizHttp.getInstance(),
            iBizNotification: IBizNotification.getInstance(),
            items: [],
            editorParams: null,
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        this.value = this.params.value;
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.editorType = this.params.editorType;
        this.editorParams = this.grid.getEditItem(this.column.name).editorParams;
    },
    methods: {
        getValue: function () {
            return this.value;
        },
        onSelectOpen: function (flag) {
            this.open = flag;
        },
        onBlur: function () {
            if (this.value != this.rowData[this.column.name]) {
                this.value = this.rowData[this.column.name];
            }
        },
        // 选中项设置
        onACSelect: function (item) {
            if (item === void 0) { item = {}; }
            if (this.grid) {
                if (this.editorParams && !Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                    this.grid.colValueChange(this.editorParams.valueItem, item.value, this.rowData);
                }
                this.grid.colValueChange(this.column.name, item.text, this.rowData);
            }
        },
        onSearch: function (query, func) {
            var _this = this;
            if (this.editorParams && (!this.editorParams.url || Object.is(this.editorParams.url, ''))) {
                return;
            }
            var param = { srfaction: 'itemfetch', query: query };
            var bcancel = this.fillPickupCondition(param);
            if (!bcancel) {
                this.iBizNotification.warning({ title: '异常', desc: '条件不满足' });
                return;
            }
            this.iBizHttp.post(this.editorParams.url, param).subscribe(function (data) {
                _this.items = data.items;
                if (typeof func == 'function') {
                    func(data.items);
                }
            });
        },
        onSelect: function (value) {
            var index = this.items.findIndex(function (item) { return Object.is(item.value, value); });
            if (index >= 0) {
                var item = this.items[index];
                this.onACSelect(item);
            }
        },
        // 清空选中数据
        onClear: function () {
            if (this.grid) {
                if (this.editorParams && !Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                    this.grid.colValueChange(this.editorParams.valueItem, '', this.rowData);
                }
                this.grid.colValueChange(this.column.name, '', this.rowData);
            }
        },
        //  填充条件
        fillPickupCondition: function (arg) {
            if (this.grid) {
                if (this.editorParams && this.editorParams.itemParam && this.editorParams.itemParam.fetchcond) {
                    var fetchparam = {};
                    var fetchCond = this.editorParams.itemParam.fetchcond;
                    if (fetchCond) {
                        for (var cond in fetchCond) {
                            var value = this.rowData[fetchCond[cond]];
                            if (!value) {
                                this.iBizNotification.error('操作失败', '未能找到当前表格数据项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                Object.assign(arg, { srfreferitem: this.column.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.rowData) });
                return true;
            }
            else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        // 打开选择视图
        openView: function () {
            var _this = this;
            var view = { viewparam: {} };
            var viewController = null;
            if (this.grid) {
                viewController = this.grid.getViewController();
            }
            if (this.rowData && Object.keys(this.rowData).length > 0) {
                Object.assign(view.viewparam, { srfkey: this.rowData['srfkey'] });
            }
            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }
            var bcancel = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }
            if (this.editorParams && this.editorParams.pickupView && Object.keys(this.editorParams.pickupView).length > 0) {
                var subject = new rxjs.Subject();
                Object.assign(view, this.editorParams.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe(function (result) {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    var item = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }
                    if (_this.grid) {
                        if (!Object.is(_this.editorParams.valueItem, '') && _this.rowData.hasOwnProperty(_this.editorParams.valueItem)) {
                            _this.grid.colValueChange(_this.editorParams.valueItem, item.srfkey, _this.rowData);
                        }
                        _this.value = item.srfmajortext;
                        _this.grid.colValueChange(_this.column.name, item.srfmajortext, _this.rowData);
                    }
                });
            }
        }
    }
});

"use strict";
Vue.component("costom-text-column-filter", {
    template: "\n        <div class=\"ibiz-ag-filter text-filter\">\n            <input type=\"text\" :value=\"value\" @change=\"valueChanged($event)\"/>\n            <Icon :class=\"{selectClear: value != ''}\" type=\"ios-close-circle\" @click=\"clear\"/>\n        </div>\n    ",
    data: function () {
        return {
            value: ""
        };
    },
    beforeMount: function () {
    },
    mounted: function () { },
    methods: {
        valueChanged: function (event) {
            this.value = event.target.value;
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        onParentModelChanged: function (parentModel) {
            var _this = this;
            if (parentModel) {
                this.value = !parentModel ? "" : parentModel.filter;
            }
            else {
                if (this.value) {
                    setTimeout(function () {
                        _this.params.onFloatingFilterChanged({
                            model: _this.buildModel()
                        });
                    }, 50);
                }
            }
        },
        clear: function () {
            this.value = "";
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        buildModel: function () {
            return {
                filterType: 'text',
                type: 'contains',
                filter: this.value
            };
        }
    }
});

"use strict";
Vue.component("costom-select-column-filter", {
    template: "\n    \t<div class=\"ibiz-ag-filter select-filter\">\n\t        <select ref=\"select\" @change=\"valueChanged\">\n\t            <option style='display: none'></option>\n\t            <template v-for=\"item of items\">\n\t            \t<option :value=\"item.value\">{{ item.text }}</option>\n\t            </template>\n\t        </select>\n\t        <Icon :class=\"{selectClear: value != ''}\" type=\"ios-close-circle\" @click=\"clear\"/>\n        </div>\n    ",
    data: function () {
        return {
            value: '',
            items: [],
            http: IBizHttp.getInstance(),
            grid: null
        };
    },
    beforeMount: function () {
        var _this = this;
        this.grid = this.params.grid;
        if (Object.is(this.params.selectType, 'STATIC')) {
            var viewController = this.grid.getViewController();
            this.items = viewController.getCodeList(this.params.codelistId).data;
        }
        else if (Object.is(this.params.selectType, 'DYNAMIC')) {
            var param = {};
            this.http.post(this.params.url, param).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.items = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    mounted: function () {
    },
    methods: {
        valueChanged: function (event) {
            this.value = event.target.value;
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        clear: function () {
            this.value = "";
            this.$refs.select.value = "";
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        onParentModelChanged: function (parentModel) {
            var _this_1 = this;
            if (parentModel) {
                this.value = !parentModel ? "" : parentModel.filter;
            }
            else {
                if (this.value) {
                    setTimeout(function () {
                        _this_1.params.onFloatingFilterChanged({
                            model: _this_1.buildModel()
                        });
                    }, 50);
                }
            }
        },
        buildModel: function () {
            return {
                filterType: 'text',
                type: 'contains',
                filter: this.value
            };
        }
    }
});
