"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单关系部件
 *
 * @class IBizFormDRPanel
 * @extends {IBizFormItem}
 */
var IBizFormDRPanel = /** @class */ (function (_super) {
    __extends(IBizFormDRPanel, _super);
    /**
     * Creates an instance of IBizFormDRPanel.
     * 创建 IBizFormDRPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormDRPanel
     */
    function IBizFormDRPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否开启遮罩
         *
         * @type {boolean}
         * @memberof IBizFormDRPanel
         */
        _this.blockUI = false;
        /**
         * 遮罩提示信息
         *
         * @type {string}
         * @memberof IBizFormDRPanel
         */
        _this.blockUITipInfo = '请先保存主数据';
        /**
         * 刷新数据
         *
         * @private
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.hookItems = {};
        /**
         * 父数据
         *
         * @type {string}
         * @memberof IBizFormDRPanel
         */
        _this.srfParentData = {};
        /**
         * 父模式
         *
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.srfParentMode = {};
        /**
         * 关联数据
         *
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.srfReferData = {};
        /**
         * 刷新关联视图计数
         *
         * @type {number}
         * @memberof IBizFormDRPanel
         */
        _this.refreshRefView = 0;
        /**
         * 关系表单项值变化，刷新关联视图
         *
         * @type {string}
         * @memberof IBizFormDRPanel
         */
        _this.refreshItems = '';
        /**
         * 关系页触发保存
         *
         * @type {Number}
         * @memberof IBizFormDRPanel
         */
        _this.saveRefView = 0;
        /**
         * 关系页是否值变更
         *
         * @type {boolean}
         * @memberof IBizFormDRPanel
         */
        _this.refViewDirty = false;
        /**
         * 关系页视图类型
         *
         * @type {String}
         * @memberof IBizFormDRPanel
         */
        _this.refViewType = '';
        /**
         * 视图参数
         *
         * @private
         * @type {*}
         * @memberof IBizFormDRPanel
         */
        _this.viewParamJO = {};
        /**
         * 是否刷新关系数据
         *
         * @type {boolean}
         * @memberof IBizFormDRPanel
         */
        _this.isRelationalData = true;
        if (opts.dritem.parentmode) {
            Object.assign(_this.srfParentMode, opts.dritem.parentmode);
        }
        if (opts.dritem.paramjo) {
            Object.assign(_this.viewParamJO, opts.dritem.paramjo);
        }
        if (opts.dritem.refviewtype) {
            _this.refViewType = opts.dritem.refviewtype;
        }
        if (opts.refreshitems && !Object.is(opts.refreshitems, '')) {
            _this.refreshItems = opts.refreshitems;
        }
        Object.assign(_this.srfParentMode, _this.viewParamJO);
        _this.initDRPanel();
        return _this;
    }
    /**
     * 初始化关系面板
     *
     * @private
     * @memberof IBizFormDRPanel
     */
    IBizFormDRPanel.prototype.initDRPanel = function () {
        var _this = this;
        if (this.form) {
            var form_1 = this.form;
            form_1.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                _this.srfReferData = {};
                var srfReferData = form_1.getActiveData();
                Object.assign(_this.srfReferData, { srfreferdata: JSON.stringify(srfReferData) });
            });
            form_1.on(IBizForm.FORMLOADED).subscribe(function (data) {
                _this.refreshDRUIPart();
            });
            // 表单保存之前
            form_1.on(IBizEditForm.FORMBEFORESAVE).subscribe(function (data) {
                return _this.saveDRData(data);
            });
            // 表单保存完成
            form_1.on(IBizForm.FORMSAVED).subscribe(function (data) {
                _this.refreshDRUIPart();
            });
            // 表单项更新
            form_1.on(IBizForm.UPDATEFORMITEMS).subscribe(function (data) {
                if (!data.data) {
                    return;
                }
                var refreshRefview = false;
                Object.keys(data.data).some(function (name) {
                    if (_this.isRefreshItem(name)) {
                        refreshRefview = true;
                        return true;
                    }
                });
                if (refreshRefview) {
                    _this.refreshDRUIPart();
                }
            });
            var items = this.refreshItems.split(';');
            if (items.length > 0) {
                items.forEach(function (item) {
                    _this.hookItems[item.toLowerCase()] = true;
                });
                form_1.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                    if (form_1.$ignoreformfieldchange) {
                        return;
                    }
                    var fieldname = '';
                    if (data != null) {
                        fieldname = data.name;
                    }
                    else {
                        return;
                    }
                    if (_this.isRefreshItem(fieldname)) {
                        _this.refreshDRUIPart();
                    }
                });
            }
        }
    };
    /**
     * 保存关系数据，继续父数据保存，返回true,否则返回false
     *
     * @returns {boolean}
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.saveDRData = function (arg) {
        if (arg === void 0) { arg = {}; }
        if (Object.is(this.refViewType, 'DEMEDITVIEW9') && this.refViewDirty) {
            this.saveRefView++;
            arg.srfcancel = true;
            return;
        }
        arg.srfcancel = false;
    };
    /**
     * 是否是刷新数据项
     *
     * @param {string} name
     * @returns {boolean}
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.isRefreshItem = function (name) {
        if (!name || name === '') {
            return false;
        }
        if (this.hookItems[name.toLowerCase()]) {
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * 刷新关系数据
     *
     * @returns {void}
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.refreshDRUIPart = function () {
        var _this = this;
        if (Object.is(this.srfParentMode.srfparenttype, 'CUSTOM')) {
            this.isRelationalData = false;
        }
        var form = this.form;
        var _field = form.findField('srfkey');
        if (!_field) {
            return;
        }
        var _value = _field.getValue();
        if (this.isRelationalData) {
            if (!_value || _value == null || Object.is(_value, '')) {
                this.blockUIStart();
                return;
            }
            else {
                this.blockUIStop();
            }
        }
        var srfReferData = form.getActiveData();
        this.srfParentData = {};
        this.srfReferData = {};
        Object.assign(this.srfParentData, { srfparentkey: _value });
        Object.assign(this.srfReferData, { srfreferdata: JSON.stringify(srfReferData) });
        setTimeout(function () {
            _this.refreshRefView += 1;
        }, 10);
    };
    /**
     * 开启遮罩
     *
     * @private
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.blockUIStart = function () {
        this.blockUI = true;
    };
    /**
     * 关闭遮罩
     *
     * @private
     * @memberof IBizDRPanelComponent
     */
    IBizFormDRPanel.prototype.blockUIStop = function () {
        this.blockUI = false;
    };
    /**
     * 关系页数据保存完成
     *
     * @memberof IBizFormDRPanel
     */
    IBizFormDRPanel.prototype.onDrDataSaved = function () {
        this.refViewDirty = false;
        if (this.getForm()) {
            this.getForm().save2();
        }
    };
    /**
     * 关系页发生值变更
     *
     * @memberof IBizFormDRPanel
     */
    IBizFormDRPanel.prototype.onDrDataChange = function (flag) {
        this.refViewDirty = flag;
    };
    return IBizFormDRPanel;
}(IBizFormItem));
