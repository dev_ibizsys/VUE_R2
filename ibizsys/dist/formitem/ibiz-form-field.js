"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表单属性项
 *
 * @class IBizFormField
 * @extends {IBizFormItem}
 */
var IBizFormField = /** @class */ (function (_super) {
    __extends(IBizFormField, _super);
    /**
     * Creates an instance of IBizFormField.
     * 创建 IBizFormField 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizFormField
     */
    function IBizFormField(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * label 宽度
         *
         * @type {number}
         * @memberof IBizFormField
         */
        _this.labelWidth = 130;
        /**
         * 实体属性输入旧值
         *
         * @private
         * @type {string}
         * @memberof IBizFormField
         */
        _this.oldVal = '';
        /**
         * 编辑器参数
         *
         * @type {*}
         * @memberof IBizFormField
         */
        _this.editorParams = {};
        /**
         * 输入防抖
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizFormField
         */
        _this.subject = new rxjs.Subject();
        _this.labelWidth = opts.labelWidth;
        if (opts.editorParams) {
            Object.assign(_this.editorParams, opts.editorParams);
        }
        _this.subject
            .pipe(rxjs.operators.debounceTime(300), rxjs.operators.distinctUntilChanged()).subscribe(function (data) {
            _this.value = data;
        });
        return _this;
    }
    /**
     * 值设定
     *
     * @memberof IBizFormField
     */
    IBizFormField.prototype.setData = function ($event) {
        if (!event) {
            return;
        }
        if (!$event.target) {
            return;
        }
        this.subject.next($event.target.value);
    };
    /**
     * 设置旧值
     *
     * @param {string} val
     * @memberof IBizFormField
     */
    IBizFormField.prototype.setOldValue = function (val) {
        this.oldVal = val;
    };
    /**
     * 获取旧值
     *
     * @returns {string}
     * @memberof IBizFormField
     */
    IBizFormField.prototype.getOldValue = function () {
        return this.oldVal;
    };
    /**
     * 属性值变化
     *
     * @param {*} event
     * @memberof IBizFormField
     */
    IBizFormField.prototype.valueChange = function (event) {
        if (Object.is(event, this.value)) {
            return;
        }
        this.value = event;
    };
    /**
     * 输入框失去焦点触发
     *
     * @param {*} _value
     * @returns {void}
     * @memberof IBizFormField
     */
    IBizFormField.prototype.onBlur = function (_value) {
        if (!_value || Object.is(_value, this.value)) {
            return;
        }
        this.value = _value;
    };
    /**
     * 键盘事件
     *
     * @param {*} $event
     * @returns {void}
     * @memberof IBizFormField
     */
    IBizFormField.prototype.onKeydown = function ($event) {
        if (!$event) {
            return;
        }
        if ($event.keyCode !== 13) {
            return;
        }
        if (Object.is($event.target.value, this.value)) {
            return;
        }
        this.value = $event.target.value;
    };
    return IBizFormField;
}(IBizFormItem));
