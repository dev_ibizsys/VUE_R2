"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 图表
 *
 * @class IBizChart
 * @extends {IBizControl}
 */
var IBizChart = /** @class */ (function (_super) {
    __extends(IBizChart, _super);
    /**
     * Creates an instance of IBizChart.
     * 创建 IBizChart 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizChart
     */
    function IBizChart(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 图表数据
         *
         * @type {Array<any>}
         * @memberof IBizChart
         */
        _this.data = [];
        return _this;
    }
    /**
     * 加载报表数据
     *
     * @memberof IBizChart
     */
    IBizChart.prototype.load = function () {
        this.buildChart();
    };
    /**
     * 处理图表内容
     *
     * @memberof IBizChart
     */
    IBizChart.prototype.buildChart = function () {
        var _this = this;
        var params = {};
        this.fire(IBizChart.BEFORELOAD, params);
        Object.assign(params, { srfrender: 'ECHARTS3', srfaction: 'FETCH', srfctrlid: this.getName() });
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (result) {
            if (result.ret === 0) {
                _this.data = result.data;
                var data = _this.getChartConfig();
                var target = {};
                Object.assign(target, data, _this.data);
                _this.fire(IBizChart.LOADED, target);
            }
            else {
                _this.iBizNotification.error('系统异常', result.info);
            }
        }, function (error) {
            _this.iBizNotification.error('系统异常', error.info);
        });
    };
    /**
     * 获取图表基础配置数据
     */
    IBizChart.prototype.getChartConfig = function () {
        var opts = {};
        return opts;
    };
    /**
     * 加载之前
     *
     * @static
     * @memberof IBizChart
     */
    IBizChart.BEFORELOAD = 'BEFORELOAD';
    /**
     * 加载完成
     *
     * @static
     * @memberof IBizChart
     */
    IBizChart.LOADED = 'LOADED';
    /**
     * 点击
     *
     * @static
     * @memberof IBizChart
     */
    IBizChart.DBLCLICK = 'DBLCLICK';
    return IBizChart;
}(IBizControl));
