"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据关系栏
 *
 * @class IBizDRBar
 * @extends {IBizControl}
 */
var IBizDRBar = /** @class */ (function (_super) {
    __extends(IBizDRBar, _super);
    /**
     * Creates an instance of IBizDRBar.
     * 创建IBizDRBar实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDRBar
     */
    function IBizDRBar(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 展开数据项
         *
         * @type {Array<string>}
         * @memberof IBizDRBar
         */
        _this.expandItems = [];
        /**
         * 关系部件是否收缩，默认展开
         *
         * @type {boolean}
         * @memberof IBizDRBar
         */
        _this.isCollapsed = true;
        /**
         * 关系数据项
         *
         * @type {Array<any>}
         * @memberof IBizDRBar
         */
        _this.items = [];
        /**
         * 选中项
         *
         * @type {*}
         * @memberof IBizDRBar
         */
        _this.selectItem = {};
        /**
         * 父数据对象
         *
         * @type {*}
         * @memberof IBizDRBar
         */
        _this.srfParentData = {};
        return _this;
    }
    /**
     * 加载关系数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var param = { srfaction: 'fetch', srfctrlid: this.getName() };
        Object.assign(param, arg);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (result) {
            if (result.ret === 0) {
                _this.formarItems(result.items);
                _this.itemSelect(result.items, {});
                _this.items = result.items.slice();
                _this.fire(IBizDRBar.DRBARLOADED, _this.items);
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 选中项
     *
     * @private
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {boolean}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.itemSelect = function (items, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var bNeedReSelect = false;
        items.forEach(function (item) {
            var counterid = item.counterid;
            var countermode = item.countermode;
            item.show = true;
            var count = data[counterid];
            if (!count) {
                count = 0;
            }
            if (count === 0 && countermode && countermode === 1) {
                item.show = false;
                // 判断是否选中列，如果是则重置选中
                if (_this.selectItem && Object.is(_this.selectItem.id, item.id)) {
                    bNeedReSelect = true;
                }
            }
            item.counterdata = count;
            if (item.items) {
                bNeedReSelect = _this.itemSelect(item.items, data);
            }
        });
        return bNeedReSelect;
    };
    /**
     * 格式化数据项
     *
     * @private
     * @param {*} _items
     * @returns {*}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.formarItems = function (_items) {
        var _this = this;
        _items.forEach(function (item) {
            item.disabled = true;
            item.class = 'drbadge';
            if (item.items) {
                _this.expandItems.push(item.id);
                _this.formarItems(item.items);
            }
        });
    };
    /**
     * 菜单节点选中处理
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.expandedAndSelectSubMenu = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.is(item.id, 'form')) {
            this.setSelectItem(item);
            this.fire(IBizDRBar.DRBARSELECTCHANGE, item);
            return;
        }
        var viewController = this.getViewController();
        var refitem = viewController.getDRItemView(item.dritem);
        if (!refitem || Object.keys(refitem).length == 0) {
            return;
        }
        var view = { viewparam: {} };
        if (item.dritem.viewparam) {
            Object.assign(view.viewparam, item.dritem.viewparam);
        }
        Object.assign(view.viewparam, this.srfParentData);
        Object.assign(view, refitem, item);
        this.fire(IBizDRBar.DRBARSELECTCHANGE, view);
    };
    /**
     * 菜单项选中事件
     *
     * @param {*} item
     * @returns {void}
     * @memberof IBizTreeExpBarService
     */
    IBizDRBar.prototype.selection = function (item) {
        if (item.items && item.items.length > 0) {
            return;
        }
        if (Object.is(item.id, 'form')) {
            this.fire(IBizDRBar.DRBARSELECTCHANGE, item);
            return;
        }
        var viewController = this.getViewController();
        var refitem = viewController.getDRItemView(item.dritem);
        if (!refitem && Object.keys(refitem).length == 0) {
            return;
        }
        var view = { viewparam: {} };
        if (item.dritem.viewparam) {
            Object.assign(view.viewparam, item.dritem.viewparam);
        }
        Object.assign(view.viewparam, this.srfParentData);
        Object.assign(view, refitem, item);
        this.fire(IBizDRBar.DRBARSELECTCHANGE, view);
    };
    /**
     * 重新加载关系数据
     *
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.reLoad = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.load(arg);
    };
    /**
     * 获取所有关系数据项
     *
     * @returns {Array<any>}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.getItems = function () {
        return this.items;
    };
    /**
     * 获取关系数据项
     *
     * @param {Array<any>} items
     * @param {*} [data={}]
     * @returns {*}
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.getItem = function (items, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var _item = {};
        var viewController = this.getViewController();
        items.some(function (item) {
            var drItem = viewController.getDRItemView({ viewid: item.id });
            if (drItem && Object.is(drItem.routepath, data.routepath)) {
                Object.assign(_item, drItem, item);
                return true;
            }
            if (item.items && item.items.length > 0) {
                var subItem = _this.getItem(item.items, data);
                if (subItem && Object.keys(subItem).length > 0) {
                    Object.assign(_item, subItem);
                    return;
                }
            }
        });
        return _item;
    };
    /**
     * 设置关系数据选中项
     *
     * @param {*} [_item={}]
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.setSelectItem = function (_item) {
        if (_item === void 0) { _item = {}; }
        if (!Object.is(this.selectItem.id, _item.id)) {
            this.selectItem = {};
            Object.assign(this.selectItem, _item);
        }
    };
    /**
     * 设置关系数据项状态
     *
     * @param {Array<any>} items
     * @param {boolean} state
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.setDRBarItemState = function (items, state) {
        var _this = this;
        items.forEach(function (item) {
            item.disabled = Object.is(item.id, 'form') ? false : state;
            if (item.items && item.items.length > 0) {
                _this.setDRBarItemState(item.items, state);
            }
        });
    };
    /**
     * 设置父数据
     *
     * @param {*} [data={}]
     * @memberof IBizDRBar
     */
    IBizDRBar.prototype.setParentData = function (data) {
        if (data === void 0) { data = {}; }
        this.srfParentData = {};
        Object.assign(this.srfParentData, data);
    };
    /**
     * 数据关系项加载完成
     *
     * @static
     * @memberof IBizDRBar
     */
    IBizDRBar.DRBARLOADED = 'DRBARLOADED';
    /**
     * 数据关系项选中
     *
     * @static
     * @memberof IBizDRBar
     */
    IBizDRBar.DRBARSELECTCHANGE = 'DRBARSELECTCHANGE';
    return IBizDRBar;
}(IBizControl));
