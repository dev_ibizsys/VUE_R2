"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 表格
 *
 * @class IBizDataGrid
 * @extends {IBizMDControl}
 */
var IBizDataGrid = /** @class */ (function (_super) {
    __extends(IBizDataGrid, _super);
    /**
     * Creates an instance of IBizGrid.
     * 创建 IBizGrid 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizGrid
     */
    function IBizDataGrid(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 查询开始条数
         *
         * @memberof IBizGrid
         */
        _this.start = 0;
        /**
         * 每次加载条数
         *
         * @memberof IBizGrid
         */
        _this.limit = 20;
        /**
         * 总条数
         *
         * @memberof IBizGrid
         */
        _this.totalrow = 0;
        /**
         * 当前显示页码
         *
         * @memberof IBizGrid
         */
        _this.curPage = 1;
        /**
         * 是否全选
         *
         * @memberof IBizGrid
         */
        _this.allChecked = false;
        /**
         * 表格行选中动画
         *
         * @memberof IBizGrid
         */
        _this.indeterminate = false;
        /**
         * 表格全部排序字段
         *
         * @type {Array<any>}
         * @memberof IBizGrid
         */
        _this.gridSortField = [];
        /**
         * 行多项选中设置，用于阻塞多次触发选中效果
         *
         * @private
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.rowsSelection = false;
        /**
         * 是否支持多项
         *
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.multiSelect = true;
        /**
         * 是否启用行编辑
         *
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.isEnableRowEdit = false;
        /**
         * 打开行编辑
         *
         * @type {boolean}
         * @memberof IBizGrid
         */
        _this.openRowEdit = false;
        /**
         * 表格编辑项集合
         *
         * @type {*}
         * @memberof IBizGrid
         */
        _this.editItems = {};
        /**
         * 编辑行数据处理
         *
         * @type {*}
         * @memberof IBizGrid
         */
        _this.state = {};
        /**
         * 备份数据
         *
         * @type {Array<any>}
         * @memberof IBizGrid
         */
        _this.backupDatas = [];
        /**
         * 编辑行保存结果
         *
         * @type {Array<any>}
         * @memberof IBizDataGrid
         */
        _this.editorRowsSaveResult = [];
        /**
         * 最大导出行数
         *
         * @type {number}
         * @memberof IBizGrid
         */
        _this.maxExportRow = 1000;
        _this.regEditItems();
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        if (!opt.start) {
            Object.assign(opt, { start: (this.curPage - 1) * this.limit });
        }
        if (!opt.limit) {
            Object.assign(opt, { limit: this.limit });
        }
        Object.assign(opt, { sort: JSON.stringify(this.gridSortField) });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.allChecked = false;
        this.indeterminate = false;
        this.selection = [];
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            _this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = _this.rendererDatas(response.items);
            _this.totalrow = response.totalrow;
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', error.errorMessage);
        });
    };
    /**
     * 刷新数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.refresh = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.load(arg);
    };
    /**
     * 删除数据
     *
     * @param {*} [arg={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.remove = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        Object.assign(params, arg);
        Object.assign(params, { srfaction: 'remove', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (response) {
            _this.endLoading();
            if (response.ret !== 0) {
                _this.iBizNotification.error('', '删除数据失败,' + response.info);
                return;
            }
            if (_this.allChecked) {
                var rows = _this.curPage * _this.limit;
                if (_this.totalrow <= rows) {
                    _this.curPage = _this.curPage - 1;
                    if (_this.curPage === 0) {
                        _this.curPage = 1;
                    }
                }
            }
            _this.load({});
            _this.fire(IBizDataGrid.REMOVED, {});
            if (response.info && response.info !== '') {
                _this.iBizNotification.success('', '删除成功!');
            }
            _this.selection = [];
            // IBizUtil.processResult(response);
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', '删除数据失败');
        });
    };
    /**
     * 行数据复选框单选
     *
     * @param {boolean} value
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.onItemSelect = function (value, item) {
        if (item === void 0) { item = {}; }
        if (item.disabled) {
            return;
        }
        // if (this.isEnableRowEdit && this.openRowEdit) {
        //     return;
        // }
        var index = this.selection.findIndex(function (data) { return Object.is(data.srfkey, item.srfkey); });
        if (index === -1) {
            this.selection.push(item);
        }
        else {
            this.selection.splice(index, 1);
        }
        if (!this.multiSelect) {
            this.selection.forEach(function (data) {
                data.checked = false;
            });
            this.selection = [];
            if (index === -1) {
                this.selection.push(item);
            }
        }
        this.rowsSelection = true;
        this.allChecked = this.selection.length === this.items.length ? true : false;
        this.indeterminate = (!this.allChecked) && (this.selection.length > 0);
        item.checked = value;
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
    };
    /**
     * 行数据复选框全选
     *
     * @param {boolean} value
     * @memberof IBizMDControl
     */
    IBizDataGrid.prototype.selectAll = function (value) {
        var _this = this;
        if (this.isEnableRowEdit && this.openRowEdit) {
            return;
        }
        this.allChecked = value;
        if (!this.multiSelect) {
            setTimeout(function () {
                _this.allChecked = false;
            });
            return;
        }
        this.items.forEach(function (item) {
            if (!item.disabled) {
                item.checked = value;
            }
        });
        this.selection = [];
        if (value) {
            this.selection = this.items.slice();
        }
        this.indeterminate = (!value) && (this.selection.length > 0);
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
    };
    /**
     * 导出数据
     *
     * @param {any} params
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.exportData = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var params = {};
        this.fire(IBizMDControl.BEFORELOAD, params);
        if (params.search) {
            Object.assign(params, { query: params.search });
        }
        Object.assign(params, { srfaction: 'exportdata', srfctrlid: this.getName() });
        if (Object.is(arg.itemTag, 'all')) {
            Object.assign(params, { start: 0, limit: this.maxExportRow });
        }
        else if (Object.is(arg.itemTag, 'custom')) {
            var nStart = arg.exportPageStart;
            var nEnd = arg.exportPageEnd;
            if (nStart < 1 || nEnd < 1 || nStart > nEnd) {
                this.iBizNotification.warning('警告', '请输入有效的起始页');
                return;
            }
            Object.assign(params, { start: (nStart - 1) * this.limit, limit: (nEnd - nStart + 1) * this.limit });
        }
        else {
            Object.assign(params, { start: (this.curPage * this.limit) - this.limit, limit: this.curPage * this.limit });
        }
        if (this.gridSortField.length > 0) {
            Object.assign(params, { sort: this.gridSortField[0].property, sortdir: this.gridSortField[0].direction });
        }
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (res) {
            _this.endLoading();
            if (res.ret !== 0) {
                _this.iBizNotification.warning('警告', res.info);
                return;
            }
            // if (res.downloadurl) {
            //     let downloadurl = `/${IBizEnvironment.AppName.toLowerCase()}${res.downloadurl} `;
            //     downloadurl = (IBizEnvironment.LocalDeve ? '' : `/${IBizEnvironment.BaseUrl}`) + downloadurl;
            //     IBizUtil.download(downloadurl);
            // }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', error.errorMessage);
        });
    };
    /**
     * 导出模型
     *
     * @param {*} [arg={}]
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.exportModel = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var params = {};
        Object.assign(params, arg);
        Object.assign(params, { srfaction: 'exportmodel', srfctrlid: this.getName() });
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (res) {
            _this.endLoading();
            if (res.ret === 0) {
                if (res.downloadurl) {
                    var downloadurl = "/" + IBizEnvironment.AppName.toLowerCase() + res.downloadurl + " ";
                    downloadurl = (IBizEnvironment.LocalDeve ? '' : "/" + IBizEnvironment.BaseUrl) + downloadurl;
                    IBizUtil.download(downloadurl);
                }
            }
            else {
                _this.iBizNotification.warning('警告', res.info);
            }
        }, function (error) {
            _this.endLoading();
            _this.iBizNotification.error('', error.errorMessage);
        });
    };
    /**
     * 重置分页
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.resetStart = function () {
        this.start = 0;
    };
    /**
     * 分页页数改变
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.pageIndexChange = function () {
        this.refresh();
    };
    /**
     * 每页显示条数
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.pageSizeChange = function () {
        this.curPage = 1;
        this.refresh();
    };
    /**
     * 单击行选中
     *
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.clickRowSelect = function (data) {
        if (data === void 0) { data = {}; }
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizDataGrid.ROWCLICK, this.selection);
    };
    /**
     * 双击行选中
     *
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.dblClickRowSelection = function (data) {
        if (data === void 0) { data = {}; }
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizDataGrid.ROWDBLCLICK, this.selection);
    };
    /**
     * 表格排序
     *
     * @param {string} name 字段明显
     * @param {string} type 排序类型
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.sort = function (name, type) {
        if (!name || !type) {
            return;
        }
        var _name = name;
        this.columns.some(function (solumn) {
            if (solumn.dataItemName && Object.is(solumn.dataItemName, name)) {
                _name = solumn.name;
                return true;
            }
            return false;
        });
        var _item = this.gridSortField[0];
        if (_item && Object.is(_item.property, _name) && Object.is(_item.direction, type)) {
            return;
        }
        var sort = { property: _name };
        Object.assign(sort, { direction: type });
        this.gridSortField = [];
        this.gridSortField.push(sort);
        this.refresh({});
    };
    /**
     * 设置表格数据当前页
     *
     * @param {number} page 分页数量
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setCurPage = function (page) {
        this.curPage = page;
    };
    /**
     * 设置是否支持多选
     *
     * @param {boolean} state 是否支持多选
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setMultiSelect = function (state) {
        this.multiSelect = state;
    };
    /**
     * 界面行为
     *
     * @param {string} tag
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.uiAction = function (tag, data) {
        if (data === void 0) { data = {}; }
        if (data.disabled) {
            return;
        }
        if (this.doRowDataSelect(data)) {
            return;
        }
        this.fire(IBizMDControl.UIACTION, { tag: tag, data: data });
    };
    /**
     * 处理非复选框行数据选中,并处理是否激活数据
     *
     * @private
     * @param {*} [data={}]
     * @returns {boolean} 是否激活
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.doRowDataSelect = function (data) {
        // if (this.isEnableRowEdit && this.openRowEdit) {
        //     return;
        // }
        if (data === void 0) { data = {}; }
        this.selection.forEach(function (item) {
            item.checked = false;
        });
        this.selection = [];
        data.checked = true;
        this.selection.push(data);
        this.indeterminate = (!this.allChecked) && (this.selection.length > 0);
        // if (this.rowsSelection) {
        //     this.rowsSelection = false;
        //     return true;
        // }
        return false;
    };
    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.rendererDatas = function (items) {
        var _this = this;
        _super.prototype.rendererDatas.call(this, items);
        items.forEach(function (item) {
            var names = Object.keys(item);
            names.forEach(function (name) { item[name] = item[name] ? item[name] : ''; });
            item.checked = false;
        });
        if (this.isEnableRowEdit) {
            items.forEach(function (item) { item.openeditrow = (_this.openRowEdit) ? false : true; });
            if (this.openRowEdit) {
                this.selection = [];
                this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
                this.backupDatas = JSON.parse(JSON.stringify(items)).slice();
                items.forEach(function (item, index) {
                    _this.setEditItemState(item.srfkey);
                    _this.editRow(item, index, false);
                });
            }
            else {
                this.backupDatas = [];
                this.state = {};
            }
        }
        return items;
    };
    /**
     * 注册表格所有编辑项
     *
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.regEditItems = function () {
    };
    /**
     * 注册表格编辑项
     *
     * @param {*} [item={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.regEditItem = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.keys(item).length === 0) {
            return;
        }
        this.editItems[item.name] = item;
    };
    /**
     * 获取编辑项
     *
     * @param {string} name
     * @returns {*}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.getEditItem = function (name) {
        if (name) {
            return this.editItems[name];
        }
    };
    /**
     * 设置编辑项状态
     *
     * @param {string} srfkey
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setEditItemState = function (srfkey) {
        var _this = this;
        if (!this.state) {
            return;
        }
        if (!srfkey) {
            this.iBizNotification.warning('警告', '数据异常');
        }
        // tslint:disable-next-line:prefer-const
        var editItems = {};
        var itemsName = Object.keys(this.editItems);
        itemsName.forEach(function (name) {
            // tslint:disable-next-line:prefer-const
            var item = {};
            var _editor = JSON.stringify(_this.editItems[name]);
            Object.assign(item, JSON.parse(_editor));
            editItems[name] = item;
        });
        this.state[srfkey] = editItems;
    };
    /**
     * 删除信息编辑项状态
     *
     * @param {string} srfkey
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.deleteEditItemState = function (srfkey) {
        if (srfkey && this.state.hasOwnProperty(srfkey)) {
            delete this.state.srfkey;
        }
    };
    /**
     * 设置编辑项是否启用
     *
     * @param {string} srfkey
     * @param {number} type
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.setEditItemDisabled = function (srfkey, type) {
        if (this.state && this.state.hasOwnProperty(srfkey)) {
            // tslint:disable-next-line:prefer-const
            var item_1 = this.state[srfkey];
            var itemsName = Object.keys(item_1);
            itemsName.forEach(function (name) {
                var isReadOk = true;
                var state = true;
                if (isReadOk) {
                    if (type === 1) {
                        if ((item_1[name].enabledcond & 2) === 2) {
                            state = false;
                        }
                    }
                    else {
                        if ((item_1[name].enabledcond & 1) === 1) {
                            state = false;
                        }
                    }
                }
                item_1[name].disabled = state;
            });
            Object.assign(this.state[srfkey], item_1);
        }
    };
    /**
     * 获取行编辑状态
     *
     * @returns {boolean}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.getOpenEdit = function () {
        return this.openRowEdit;
    };
    /**
     * 保存表格所有编辑行 <在插件模板中提供重写>
     *
     * @param {*} [params={}]
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.saveAllEditRow = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        var saveCount = 0;
        this.items.forEach(function (_item, index) {
            var data = _this.backupDatas.find(function (_data) { return Object.is(_data.srfkey, _item.srfkey); });
            if (!data) {
                return;
            }
            Object.keys(data).some(function (name) {
                var fieldIsSave = !Object.is(name, 'openeditrow') && (typeof _item[name] === 'string' && typeof data[name] === 'string') && !Object.is(_item[name], data[name]);
                if (!fieldIsSave) {
                    return false;
                }
                var templData = {};
                Object.assign(templData, params, _item);
                var subject = _this.editRowSave(templData, index);
                subject.subscribe(function (result) {
                    _this.editorRowsSaveResult.push(result);
                    if (_this.editorRowsSaveResult.length === saveCount) {
                        _this.showSaveResultInfo();
                        _this.editorRowsSaveResult = [];
                        _this.fire(IBizMDControl.LOADED, _this.items);
                    }
                });
                saveCount++;
                return true;
            });
        });
    };
    /**
     * 显示保存结果信息
     *
     * @private
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.showSaveResultInfo = function () {
        var successInfo = '';
        var errorInfo = '';
        var info = '';
        this.editorRowsSaveResult.sort(function (a, b) {
            return a.index - b.index;
        });
        this.editorRowsSaveResult.forEach(function (data) {
            if (Object.is(data.state, 'success')) {
                if (!Object.is(successInfo, '')) {
                    successInfo = successInfo + "\u3001";
                }
                successInfo = "" + successInfo + (data.index + 1);
            }
            if (Object.is(data.state, 'error')) {
                var _info_1 = "\u7B2C " + (data.index + 1) + " \u6761\u4FDD\u5B58\u5931\u8D25\uFF0C\u9519\u8BEF\u4FE1\u606F\u5982\u4E0B\uFF1A";
                if (data.result && Array.isArray(data.result) && data.result.length > 0) {
                    var items = data.result;
                    items.forEach(function (item, index) {
                        if (index > 0) {
                            _info_1 = _info_1 + "\u3001";
                        }
                        _info_1 = "" + _info_1 + item.info;
                    });
                }
                else if (!Object.is(data.errorMessage, '')) {
                    // _info = !Object.is(_info, '') ? `${_info}、` : _info;
                    _info_1 = "" + _info_1 + data.errorMessage;
                }
                errorInfo = errorInfo + "<div>" + _info_1 + "</div>";
            }
        });
        if (!Object.is(successInfo, '')) {
            info = info + "<div>\u7B2C " + successInfo + " \u6761\u4FDD\u5B58\u6210\u529F\uFF01</div>";
        }
        if (!Object.is(errorInfo, '')) {
            info = info + "<div style='color: red;'>" + errorInfo + "</div>";
        }
        this.iBizNotification.info('', info);
    };
    /**
     * 启用行编辑
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.openEdit = function (params) {
        var _this = this;
        if (!this.isEnableRowEdit) {
            this.iBizNotification.info('提示', '未启用行编辑');
            return;
        }
        if (!this.openRowEdit) {
            this.openRowEdit = true;
            this.items.forEach(function (item) { item.openeditrow = true; });
            this.selection.forEach(function (data) {
                data.checked = false;
            });
            this.selection = [];
            this.indeterminate = false;
            this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
            this.backupDatas = JSON.parse(JSON.stringify(this.items)).slice();
            this.items.forEach(function (item, index) {
                _this.setEditItemState(item.srfkey);
                _this.editRow(item, index, false);
            });
        }
    };
    /**
     * 关闭行编辑
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.closeEdit = function (params) {
        if (!this.isEnableRowEdit) {
            this.iBizNotification.info('提示', '未启用行编辑');
            return;
        }
        if (this.openRowEdit) {
            this.openRowEdit = false;
            this.backupDatas = [];
            this.items = [];
            this.state = {};
            this.refresh({});
        }
    };
    /**
     * 编辑行数据
     *
     * @param {*} [data={}]
     * @param {number} rowindex
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.editRow = function (data, rowindex, state) {
        if (data === void 0) { data = {}; }
        data.openeditrow = state;
        this.setEditItemState(data.srfkey);
        if (data.openeditrow) {
            var index = this.backupDatas.findIndex(function (item) { return Object.is(item.srfkey, data.srfkey); });
            if (index !== -1) {
                Object.assign(data, this.backupDatas[index]);
            }
            if (Object.is(data.srfkey, '')) {
                this.items.splice(rowindex, 1);
            }
        }
        else {
            this.setEditItemDisabled(data.srfkey, 1);
        }
    };
    /**
     * 保存编辑行数据
     *
     * @param {*} [data={}]
     * @param {number} rowindex
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.editRowSave = function (data, rowindex) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var subject = new rxjs.Subject();
        var srfaction = data.rowdatamodal && Object.is(data.rowdatamodal, 'gridloaddraft') ? 'create' : 'update';
        var params = { srfaction: srfaction, srfctrlid: 'grid' };
        var _names = Object.keys(data);
        _names.forEach(function (name) {
            data[name] = data[name] ? data[name] : '';
        });
        Object.assign(params, data);
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (responce) {
            if (responce.ret === 0) {
                var index = _this.backupDatas.findIndex(function (item) { return Object.is(data.srfkey, item.srfkey); });
                if (index !== -1) {
                    Object.assign(_this.backupDatas[index], responce.data);
                    if (_this.backupDatas[index].hasOwnProperty('srfisnewdata')) {
                        delete _this.backupDatas[index].srfisnewdata;
                    }
                    if (_this.backupDatas[index].hasOwnProperty('rowdatamodal')) {
                        delete _this.backupDatas[index].rowdatamodal;
                    }
                    _this.deleteEditItemState(data.srfkey);
                    _this.setEditItemState(responce.data.srfkey);
                    _this.setEditItemDisabled(responce.data.srfkey, 1);
                }
                if (_this.items[rowindex].hasOwnProperty('srfisnewdata')) {
                    delete _this.items[rowindex].srfisnewdata;
                }
                if (_this.items[rowindex].hasOwnProperty('rowdatamodal')) {
                    delete _this.items[rowindex].rowdatamodal;
                }
                Object.assign(_this.items[rowindex], responce.data);
                subject.next({ state: 'success', index: rowindex, result: [] });
            }
            else {
                var state = responce;
                _this.doSaveErrorResult({ subject: subject, state: state, data: data, rowindex: rowindex });
            }
        }, function (error) {
            var state = error;
            _this.doSaveErrorResult({ subject: subject, state: state, data: data, rowindex: rowindex });
        });
        return subject;
    };
    /**
     * 保存错误处理
     *
     * @private
     * @param {{ subject: Subject<any>, state: any, data: any, rowindex: number }} { subject, state, data, rowindex }
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.doSaveErrorResult = function (_a) {
        var _this = this;
        var subject = _a.subject, state = _a.state, data = _a.data, rowindex = _a.rowindex;
        var _result = [];
        if (state.error && (state.error.items && Array.isArray(state.error.items) && state.error.items.length > 0)) {
            var items = state.error.items;
            items.forEach(function (item, index) {
                Object.assign(_this.state[data.srfkey][item.id].styleCss, { 'border': '1px solid #f04134', 'border-radius': '4px' });
            });
            _result = items.slice();
        }
        var message = state.errorMessage ? state.errorMessage : '保存异常!';
        subject.next({ state: 'error', index: rowindex, result: _result, errorMessage: message });
    };
    /**
     * 行编辑文本框光标移出事件
     *
     * @param {*} event
     * @param {string} name
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.onBlur = function (event, name, data) {
        if (data === void 0) { data = {}; }
        if ((!event) || Object.keys(data).length === 0) {
            return;
        }
        if (Object.is(event.target.value, data[name])) {
            return;
        }
        this.colValueChange(name, event.target.value, data);
    };
    /**
     * 行编辑文本框键盘事件
     *
     * @param {*} event
     * @param {string} name
     * @param {*} [data={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.onKeydown = function (event, name, data) {
        if (data === void 0) { data = {}; }
        if ((!event) || Object.keys(data).length === 0) {
            return;
        }
        if (event.keyCode !== 13) {
            return;
        }
        if (Object.is(event.target.value, data[name])) {
            return;
        }
        this.colValueChange(name, event.target.value, data);
    };
    /**
     * 行编辑单元格值变化
     *
     * @param {string} name
     * @param {*} data
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.colValueChange = function (name, value, data) {
        var srfkey = data.srfkey;
        var _data = this.items.find(function (back) { return Object.is(back.srfkey, srfkey); });
        if (this.state[srfkey] && this.state[srfkey][name]) {
            if (_data && !Object.is(_data[name], value)) {
                Object.assign(this.state[srfkey][name].styleCss, { 'border': '1px solid #49a9ee', 'border-radius': '4px' });
            }
            else {
                Object.assign(this.state[srfkey][name].styleCss, { 'border': '0px', 'border-radius': '0px' });
            }
            data[name] = value;
            this.fire(IBizDataGrid.UPDATEGRIDITEMCHANGE, { name: name, data: data });
        }
    };
    /**
     * 更新表格编辑列值
     *
     * @param {string} srfufimode
     * @param {*} [data={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.updateGridEditItems = function (srfufimode, data) {
        var _this = this;
        if (data === void 0) { data = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = { srfaction: 'updategridedititem', srfufimode: srfufimode, srfctrlid: 'grid' };
        var _names = Object.keys(data);
        _names.forEach(function (name) {
            data[name] = data[name] ? data[name] : '';
        });
        Object.assign(opt, { srfactivedata: JSON.stringify(data) });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (success) {
            if (success.ret === 0) {
                var index = _this.items.findIndex(function (item) { return Object.is(item.srfkey, data.srfkey); });
                if (index !== -1) {
                    Object.assign(_this.items[index], success.data);
                }
            }
            else {
                _this.iBizNotification.error('错误', success.info);
            }
        }, function (error) {
            _this.iBizNotification.error('错误', error.info);
        });
    };
    /**
     * 新建编辑行
     *
     * @param {*} [param={}]
     * @memberof IBizGrid
     */
    IBizDataGrid.prototype.newRowAjax = function (param) {
        var _this = this;
        if (param === void 0) { param = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        Object.assign(opt, param);
        this.fire(IBizMDControl.BEFORELOAD, opt);
        Object.assign(opt, { srfaction: 'loaddraft', srfctrlid: 'grid' });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (success) {
            if (success.ret === 0) {
                var srfkey = (Object.is(success.data.srfkey, '')) ? IBizUtil.createUUID() : success.data.srfkey;
                success.data.srfkey = srfkey;
                _this.setEditItemState(srfkey);
                _this.setEditItemDisabled(srfkey, 0);
                _this.items.splice(0, 0, Object.assign(success.data, { openeditrow: false, rowdatamodal: 'gridloaddraft', srfisnewdata: true }));
                var backdata = JSON.parse(JSON.stringify(success.data));
                _this.backupDatas.push(backdata);
            }
            else {
                _this.iBizNotification.error('错误', "\u83B7\u53D6\u9ED8\u8BA4\u6570\u636E\u5931\u8D25, " + success.info);
            }
        }, function (error) {
            _this.iBizNotification.error('错误', "\u83B7\u53D6\u9ED8\u8BA4\u6570\u636E\u5931\u8D25, " + error.info);
        });
    };
    // BEGIN：Element UI 表格部件内置方法
    /**
     * 复选框行选中 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSelect = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var selections = argu[0], row = argu[1];
        var index = selections.findIndex(function (data) { return Object.is(data.srfkey, row.srfkey); });
        this.onItemSelect(index !== -1 ? false : true, row);
    };
    /**
     * 复选框全选 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSelectAll = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var selections = argu[0];
        if (Array.isArray(selections)) {
            this.selectAll(selections.length !== 0 ? true : false);
        }
    };
    /**
     * 行单击 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementRowClick = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var row = argu[0];
        if (!this.multiSelect) {
            var selections = this.$vue.$refs.multipleTable.selection;
            var index = selections.findIndex(function (data) { return Object.is(data.srfkey, row.srfkey); });
            row = index !== -1 ? {} : row;
        }
        this.$vue.$refs.multipleTable.clearSelection();
        this.$vue.$refs.multipleTable.toggleRowSelection(row);
        this.clickRowSelect(row);
    };
    /**
     * 行双击 (表格)
     *
     * @param {*} argu
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementRowDblclick = function (argu) {
        // if (this.openRowEdit) {
        //     this.$vue.$refs.multipleTable.clearSelection();
        //     return;
        // }
        var row = argu[0];
        this.$vue.$refs.multipleTable.clearSelection();
        this.$vue.$refs.multipleTable.toggleRowSelection(row);
        if (this.openRowEdit) {
            return;
        }
        this.dblClickRowSelection(row);
    };
    /**
     * 改变页码
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementOnChange = function (argu) {
        this.curPage = argu[0];
        this.pageIndexChange();
    };
    /**
     * 分页数量改变
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementOnPageSizeChange = function (argu) {
        this.limit = argu[0];
        this.pageSizeChange();
    };
    /**
     * 排序
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSortChange = function (argu) {
        var event = argu[0];
        var type = event.order.indexOf('descending') >= 0 ? 'desc' : 'asc';
        this.sort(event.prop, type);
    };
    /**
     * 是否支持选中
     *
     * @param {*} argu
     * @returns {boolean}
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.elementSelectable = function () {
        return function (row, index) {
            // return this.openRowEdit ? false : true;
            return true;
        };
    };
    /**
     * 绘制表格行选中样式
     *
     * @returns
     * @memberof IBizDataGrid
     */
    IBizDataGrid.prototype.renderElementTabelRowSelectClass = function () {
        var _this = this;
        return function (_a) {
            var row = _a.row, rowIndex = _a.rowIndex;
            var rowClass = '';
            if (row) {
                var index = _this.selection.findIndex(function (_select) { return Object.is(_select.srfkey, row.srfkey); });
                if (index !== -1) {
                    rowClass = 'ibiz-data-grid-row-select';
                }
            }
            return rowClass;
        };
    };
    // END：Element UI 表格部件内置方法
    /*****************事件声明************************/
    /**
     * 改变启用行编辑按钮信息
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.CHANGEEDITSTATE = 'CHANGEEDITSTATE';
    /**
     * 表格行数据变化
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.UPDATEGRIDITEMCHANGE = 'UPDATEGRIDITEMCHANGE';
    /**
     * 数据删除完成
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.REMOVED = 'REMOVED';
    /**
     * 行单击选中
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.ROWCLICK = 'ROWCLICK';
    /**
     * 行数据双击选中
     *
     * @static
     * @memberof IBizDataGrid
     */
    IBizDataGrid.ROWDBLCLICK = 'ROWDBLCLICK';
    return IBizDataGrid;
}(IBizMDControl));
