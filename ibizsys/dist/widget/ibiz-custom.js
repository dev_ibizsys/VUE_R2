"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 自定义部件
 *
 * @class IBizCustom
 * @extends {IBizControl}
 */
var IBizCustom = /** @class */ (function (_super) {
    __extends(IBizCustom, _super);
    /**
     * Creates an instance of IBizCustom.
     * 创建 IBizCustom 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizCustom
     */
    function IBizCustom(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    return IBizCustom;
}(IBizControl));
