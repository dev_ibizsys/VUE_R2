"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 部件对象
 *
 * @class IBizControl
 * @extends {IBizObject}
 */
var IBizControl = /** @class */ (function (_super) {
    __extends(IBizControl, _super);
    /**
     * Creates an instance of IBizControl.
     * 创建 IBizControl 实例。
     *
     * @param {*} [opts={}]
     * @memberof IBizControl
     */
    function IBizControl(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 后台交互URL
         *
         * @private
         * @type {string}
         * @memberof IBizControl
         */
        _this.url = '';
        /**
         * 视图控制器对象
         *
         * @private
         * @type {*}
         * @memberof IBizControl
         */
        _this.viewController = null;
        /**
         * 部件http请求状态
         *
         * @type {boolean}
         * @memberof IBizControl
         */
        _this.isLoading = false;
        /**
         * vue 路由对象
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.$router = null;
        /**
         * vue 实例对象
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.$vue = null;
        /**
         * vue 当前路由对象
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.$route = null;
        /**
         * 部件模型
         *
         * @type {*}
         * @memberof IBizControl
         */
        _this.model = {};
        _this.url = opts.url;
        _this.viewController = opts.viewController;
        _this.$route = opts.$route;
        _this.$router = opts.$router;
        _this.$vue = opts.$vue;
        return _this;
    }
    ;
    ;
    /**
     * 获取后台路径
     *
     * @returns {*}
     * @memberof IBizControl
     */
    IBizControl.prototype.getBackendUrl = function () {
        var url = '';
        var viewController = this.getViewController();
        if (this.url) {
            url = this.url;
        }
        if (Object.is(url, '') && viewController) {
            url = viewController.getBackendUrl();
        }
        return url;
    };
    /**
     * 获取视图控制器
     *
     * @returns {*}
     * @memberof IBizControl
     */
    IBizControl.prototype.getViewController = function () {
        if (this.viewController) {
            return this.viewController;
        }
        return undefined;
    };
    /**
     * 部件http请求
     *
     * @memberof IBizControl
     */
    IBizControl.prototype.beginLoading = function () {
        this.isLoading = true;
    };
    /**
     * 部件结束http请求
     *
     * @memberof IBizControl
     */
    IBizControl.prototype.endLoading = function () {
        this.isLoading = false;
    };
    /**
     * 设置部件模型
     *
     * @param {*} model
     * @memberof IBizControl
     */
    IBizControl.prototype.setModel = function (model) {
        if (model === void 0) { model = {}; }
        this.model = model;
    };
    /**
     * 获取部件模型
     *
     * @returns {*}
     * @memberof IBizControl
     */
    IBizControl.prototype.getModel = function () {
        return this.model;
    };
    /**
     * 部件模型变化
     *
     * @static
     * @memberof IBizControl
     */
    IBizControl.CONTROLMODELCHANGE = 'CONTROLMODELCHANGE';
    return IBizControl;
}(IBizObject));
