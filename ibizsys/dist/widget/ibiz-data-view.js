"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 数据视图部件控制器
 *
 * @class IBizDataView
 * @extends {IBizMDControl}
 */
var IBizDataView = /** @class */ (function (_super) {
    __extends(IBizDataView, _super);
    /**
     * Creates an instance of IBizDataView.
     * 创建 IBizDataView 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizDataView
     */
    function IBizDataView(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 双击选中项
         *
         * @private
         * @type {Array<any>}
         * @memberof IBizDataView
         */
        _this.dblselection = [];
        /**
         * 选中项
         *
         * @type {*}
         * @memberof IBizDataView
         */
        _this.selectItem = {};
        return _this;
    }
    /**
     * 数据加载
     *
     * @param {*} [arg={}]
     * @memberof IBizDataView
     */
    IBizDataView.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            _this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = response.items;
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            _this.endLoading();
            console.log(error.info);
        });
    };
    /**
     * 选择变化
     *
     * @param {*} [item={}]
     * @memberof IBizDataView
     */
    IBizDataView.prototype.selectChange = function (data) {
        if (data === void 0) { data = {}; }
        var arr = this.selection.filter(function (item) { return Object.is(data.srfkey, item.srfkey); });
        this.selection = [];
        this.selectItem = {};
        if (arr.length !== 1) {
            this.selection.push(data);
            Object.assign(this.selectItem, data);
        }
        this.fire(IBizDataView.SELECTIONCHANGE, this.selection);
    };
    /**
     * 双击选择变化
     *
     * @param {*} [data={}]
     * @memberof IBizDataView
     */
    IBizDataView.prototype.DBClickSelectChange = function (data) {
        if (data === void 0) { data = {}; }
        var arr = this.dblselection.filter(function (item) { return Object.is(data.srfkey, item.srfkey); });
        this.dblselection = [];
        this.selectItem = {};
        if (arr.length !== 1) {
            this.dblselection.push(data);
            Object.assign(this.selectItem, data);
        }
        this.fire(IBizDataView.DATAACTIVATED, this.dblselection);
    };
    /**
     * 数据项选中
     *
     * @static
     * @memberof IBizDataView
     */
    IBizDataView.SELECTIONCHANGE = 'SELECTIONCHANGE';
    /**
     * 数据项双击激活
     *
     * @static
     * @memberof IBizDataView
     */
    IBizDataView.DATAACTIVATED = 'DATAACTIVATED';
    return IBizDataView;
}(IBizMDControl));
