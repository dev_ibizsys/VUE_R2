"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 多编辑视图面板部件服务对象
 *
 * @class IBizMultiEditViewPanel
 * @extends {IBizControl}
 */
var IBizMultiEditViewPanel = /** @class */ (function (_super) {
    __extends(IBizMultiEditViewPanel, _super);
    /**
     * Creates an instance of IBizMultiEditViewPanel.
     * 创建 IBizMultiEditViewPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizMultiEditViewPanel
     */
    function IBizMultiEditViewPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 编辑数据集合
         *
         * @type {Array<any>}
         * @memberof IBizMultiEditViewPanel
         */
        _this.editCtrls = [];
        /**
         * 当前激活tab
         *
         * @type {Number}
         * @memberof IBizMultiEditViewPanel
         */
        _this.activeTab = 0;
        /**
         * 待删除主键集合
         *
         * @type {Array<String>}
         * @memberof IBizMultiEditViewPanel
         */
        _this.removeKeys = [];
        /**
         * 提交数量
         *
         * @memberof IBizMultiEditViewPanel
         */
        _this.submitCount = 0;
        return _this;
    }
    /**
     * 数据记载
     *
     * @param {*} [params={}]
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.load = function (params) {
        var _this = this;
        if (params === void 0) { params = {}; }
        var opt = { srfctrlid: this.getName(), srfaction: 'fetch' };
        if (params) {
            Object.assign(opt, params);
        }
        this.fire(IBizMDControl.BEFORELOAD, opt);
        Object.assign(opt, { start: 0, limit: 500 });
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (success) {
            if (success.ret == 0) {
                success.items.forEach(function (item) {
                    item.saveRefView = 0;
                    item.isDirty = false;
                });
                _this.editCtrls = success.items;
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 刷新加载数据
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.refresh = function () {
        this.load();
    };
    /**
     * 添加编辑页
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.addEditView = function () {
        this.editCtrls.push({ srfmajortext: 'New', isDirty: true, saveRefView: 0 });
        this.activeTab = this.editCtrls.length - 1;
        this.onDataChanged();
    };
    /**
     * 表单保存完成后回调
     *
     * @param {*} [data={}]
     * @param {*} [item={}]
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onEditFormSaved = function (data, item) {
        if (data === void 0) { data = {}; }
        if (item === void 0) { item = {}; }
        Object.assign(item, data);
        item.isDirty = false;
        item.saveRefView = 0;
        if (this.submitCount > 0) {
            this.submitCount--;
            if (this.submitCount === 0) {
                if (this.removeKeys.length > 0) {
                    this.doRemove();
                }
                else {
                    this.fire(IBizMultiEditViewPanel.DATASAVED, this);
                }
            }
        }
        else {
            this.onDataChanged();
        }
    };
    IBizMultiEditViewPanel.prototype.onBeforeRemove = function () {
        var _this = this;
        return function (num) {
            var item = _this.editCtrls[num];
            if (item) {
                if (item.srfkey && _this.removeKeys.indexOf(item.srfkey) < 0) {
                    _this.removeKeys.push(item.srfkey);
                }
                _this.editCtrls.splice(num, 1);
            }
            if (num == _this.activeTab) {
                if (num >= _this.editCtrls.length) {
                    _this.activeTab = _this.editCtrls.length - 1;
                }
            }
            _this.onDataChanged();
            return new Promise(function (resolve, reject) { });
        };
    };
    /**
     * 删除编辑数据
     *
     * @param {*} item
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onRemoveEditView = function (num) {
        var item = this.editCtrls[num];
        if (item) {
            if (item.srfkey && this.removeKeys.indexOf(item.srfkey) < 0) {
                this.removeKeys.push(item.srfkey);
            }
            this.editCtrls.splice(num, 1);
        }
        this.onDataChanged();
    };
    /**
     * 是否发生值变更
     *
     * @returns {boolean}
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.isDirty = function () {
        if (this.removeKeys.length > 0) {
            return true;
        }
        var items = this.editCtrls.filter(function (item) { return item.isDirty; });
        if (items.length > 0) {
            return true;
        }
        return false;
    };
    /**
     * 执行保存
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.doSave = function () {
        var _this = this;
        var items = this.editCtrls.filter(function (item) { return item.isDirty; });
        if (items && items.length > 0) {
            items.forEach(function (item) {
                _this.submitCount++;
                item.saveRefView++;
            });
        }
        else {
            if (this.removeKeys.length > 0) {
                this.doRemove();
            }
        }
    };
    /**
     * 删除数据
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.doRemove = function () {
        var _this = this;
        var params = { srfaction: 'remove', srfctrlid: 'meditviewpanel', 'srfkeys': this.removeKeys.join(';') };
        this.iBizHttp.post(this.getBackendUrl(), params).subscribe(function (success) {
            if (success.ret == 0) {
                _this.fire(IBizMultiEditViewPanel.DATASAVED, _this);
            }
        }, function (error) {
            console.log(error);
        });
    };
    /**
     * 数据发生改变
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onDataChanged = function () {
        var isDirty = false;
        var items = this.editCtrls.filter(function (item) { return item.isDirty; });
        if ((items && items.length > 0) || this.removeKeys.length > 0) {
            isDirty = true;
        }
        this.fire(IBizMultiEditViewPanel.EDITVIEWCHANGED, isDirty);
    };
    /**
     * 表单项改变
     *
     * @memberof IBizMultiEditViewPanel
     */
    IBizMultiEditViewPanel.prototype.onEditFormFieldChanged = function (param, item) {
        if (!param) {
            return;
        }
        item[param.name] = param.value;
        item.isDirty = true;
        this.onDataChanged();
    };
    /**
     * 获取选项编辑页控制对象事件
     */
    IBizMultiEditViewPanel.FINDEDITVIEWCONTROLLER = 'FINDEDITVIEWCONTROLLER';
    /**
     * 编辑页变化
     */
    IBizMultiEditViewPanel.EDITVIEWCHANGED = 'EDITVIEWCHANGED';
    /**
     * 保存完成
     */
    IBizMultiEditViewPanel.DATASAVED = 'DATASAVED';
    return IBizMultiEditViewPanel;
}(IBizControl));
