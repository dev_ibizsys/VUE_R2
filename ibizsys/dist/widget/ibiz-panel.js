"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 面板
 *
 * @class IBizPanel
 * @extends {IBizControl}
 */
var IBizPanel = /** @class */ (function (_super) {
    __extends(IBizPanel, _super);
    /**
     *Creates an instance of IBizPanel.
     *创建IBizPanel的一个实例
     * @param {*} [opt={}]
     * @memberof IBizPanel
     */
    function IBizPanel(opt) {
        if (opt === void 0) { opt = {}; }
        var _this_1 = _super.call(this, opt) || this;
        /**
         * 面板模型对象
         *
         * @type {*}
         * @memberof IBizPanel
         */
        _this_1.model = {};
        /**
         * 面板项集合
         *
         * @type {*}
         * @memberof IBizPanel
         */
        _this_1.items = {};
        _this_1.regItems();
        _this_1.regModelItems();
        _this_1.listenModel();
        _this_1.regPanelLogic();
        return _this_1;
    }
    /**
     * 注册面板项
     *
     * @memberof IBizPanel
     */
    IBizPanel.prototype.regItems = function () {
    };
    /**
     * 注册面板模型项
     *
     * @memberof IBizPanel
     */
    IBizPanel.prototype.regModelItems = function () {
    };
    /**
     * 注册面板逻辑
     *
     * @memberof IBizPanel
     */
    IBizPanel.prototype.regPanelLogic = function () {
    };
    /**
     * 注册面板项属性
     *
     * @param {*} item
     * @memberof IBizPanel
     */
    IBizPanel.prototype.regItem = function (item) {
        if (!this.items) {
            this.items = {};
        }
        if (item) {
            this.items[item.getName()] = item;
        }
    };
    /**
     * 获取面板项属性
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizPanel
     */
    IBizPanel.prototype.getItem = function (name) {
        return this.items[name];
    };
    /**
     * 注册面板模型属性项
     *
     * @param {string} name
     * @param {*} val
     * @param {*} item
     * @memberof IBizPanel
     */
    IBizPanel.prototype.regModelItem = function (name, val, item) {
        if (!this.model) {
            this.model = {};
        }
        if (name) {
            this.model[name] = item;
        }
    };
    /**
     * 获取面板模型属性项
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizPanel
     */
    IBizPanel.prototype.getModelItem = function (name) {
        return this.model[name];
    };
    /**
     * 获取面板模型
     *
     * @returns {*}
     * @memberof IBizPanel
     */
    IBizPanel.prototype.getModel = function () {
        return this.model;
    };
    IBizPanel.prototype.listenModel = function () {
        var _this = this;
        IBizListenProperty.regListenObject(_this.model).subscribe(function (args) {
            _this.onModelChanged(args);
        });
    };
    /**
     * 模型变化
     *
     * @memberof IBizPanel
     */
    IBizPanel.prototype.onModelChanged = function (args) {
        this.fire(IBizPanel.MODELCHANGED, args);
    };
    /**
     * 模型变化事件
     *
     * @static
     * @memberof IBizPanel
     */
    IBizPanel.MODELCHANGED = 'MODELCHANGED';
    return IBizPanel;
}(IBizControl));
