"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 工具栏
 *
 * @class IBizToolbar
 * @extends {IBizControl}
 */
var IBizToolbar = /** @class */ (function (_super) {
    __extends(IBizToolbar, _super);
    /**
     * Creates an instance of IBizToolbar.
     * 创建IBizToolbar的一个实例。
     *
     * @param {*} [opts={}]
     * @memberof IBizToolbar
     */
    function IBizToolbar(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 数据导出下拉菜单状态
         *
         * @type {boolean}
         * @memberof IBizToolbar
         */
        _this.exportMenuState = false;
        /**
         * 工具栏按钮
         *
         * @type {Array<any>}
         * @memberof IBizToolbar
         */
        _this.items = [];
        _this.regToolBarItems();
        return _this;
    }
    /**
     * 注册所有工具栏按钮
     *
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.regToolBarItems = function () {
    };
    /**
     * 注册工具栏按钮
     *
     * @param {*} [item={}]
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.regToolBarItem = function (item) {
        if (item === void 0) { item = {}; }
        if (Object.keys(item).length === 0) {
            return;
        }
        this.items.push(item);
    };
    /**
     * 获取工具栏按钮
     *
     * @returns {Array<any>}
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.getItems = function () {
        if (!this.items) {
            this.items = [];
        }
        return this.items;
    };
    /**
     * 获取工具栏项
     *
     * @param {Array<any>} items
     * @param {string} name
     * @returns {*}
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.getItem = function (items, name) {
        var _this = this;
        var data = {};
        items.some(function (_item) {
            if (Object.is(_item.name, name)) {
                data = _item;
                return true;
            }
            if (_item.items && _item.items.length > 0) {
                var subItem = _this.getItem(_item.items, name);
                if (Object.keys(subItem).length > 0) {
                    data = subItem;
                    return true;
                }
            }
        });
        return data;
    };
    /**
     * 设置工具栏按钮项是否启用
     *
     * @param {Array<any>} items
     * @param {boolean} disabled
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.setItemDisabled = function (items, disabled) {
        var _this = this;
        items.forEach(function (_item) {
            if (_item.uiaction && (Object.is(_item.uiaction.target, 'SINGLEKEY') || Object.is(_item.uiaction.target, 'MULTIKEY'))) {
                _item.disabled = disabled;
            }
            if (_item.uiaction && Object.is(_item.uiaction.tag, 'NewRow')) {
                _item.disabled = false;
            }
            if (_item.items && _item.items.length > 0) {
                _this.setItemDisabled(_item.items, disabled);
            }
        });
    };
    /**
     * 更新权限
     *
     * @param {Array<any>} items
     * @param {*} [action={}]
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.updateAccAction = function (items, action) {
        var _this = this;
        if (action === void 0) { action = {}; }
        items.forEach(function (_item) {
            var dataaccaction = _item.dataaccaction;
            var state = (dataaccaction && !Object.is(dataaccaction, '')) && (action && Object.keys(action).length > 0 && action[dataaccaction] !== 1);
            _item._dataaccaction = state ? false : true;
            if (_item.items && _item.items.length > 0) {
                _this.updateAccAction(_item.items, action);
            }
        });
    };
    /**
     * 工具栏导出功能设置
     *
     * @param {string} type
     * @param {string} [itemTag]
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.itemExportExcel = function (type, itemTag) {
        // tslint:disable-next-line:prefer-const
        var params = { tag: type };
        if (itemTag && Object.is(itemTag, 'all')) {
            Object.assign(params, { itemTag: 'all' });
        }
        else if (itemTag && Object.is(itemTag, 'custom')) {
            if (!this.exportStartPage || !this.exportEndPage) {
                this.iBizNotification.warning('警告', '请输入起始页');
                return;
            }
            var startPage = Number.parseInt(this.exportStartPage, 10);
            var endPage = Number.parseInt(this.exportEndPage, 10);
            if (Number.isNaN(startPage) || Number.isNaN(endPage)) {
                this.iBizNotification.warning('警告', '请输入有效的起始页');
                return;
            }
            if (startPage < 1 || endPage < 1 || startPage > endPage) {
                this.iBizNotification.warning('警告', '请输入有效的起始页');
                return;
            }
            Object.assign(params, { exportPageStart: startPage, exportPageEnd: endPage, itemTag: 'custom' });
        }
        this.exportMenuState = false;
        this.fire(IBizToolbar.ITEMCLICK, params);
    };
    /**
     * 点击按钮
     *
     * @param {string} type  界面行为类型
     * @memberof IBizToolbar
     */
    IBizToolbar.prototype.itemclick = function (_item) {
        if (_item === void 0) { _item = {}; }
        var item = this.getItem(this.items, _item.name);
        if (Object.is(item.tag, 'ToggleRowEdit')) {
            item.rowedit = !item.rowedit;
            item.caption = item.rowedit ? '开启行编辑' : '关闭行编辑';
        }
        if (Object.is(item.tag, 'ToggleFilter')) {
            item.opensearchform = !item.opensearchform;
            item.caption = item.opensearchform ? '收起' : '过滤';
        }
        if (!_item.uiaction) {
            return;
        }
        this.fire(IBizToolbar.ITEMCLICK, { tag: _item.uiaction.tag });
    };
    /**
     * 点击按钮事件
     *
     * @static
     * @memberof IBizToolbar
     */
    IBizToolbar.ITEMCLICK = 'ITEMCLICK';
    return IBizToolbar;
}(IBizControl));
