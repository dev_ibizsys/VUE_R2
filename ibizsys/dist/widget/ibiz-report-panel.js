"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 报表
 *
 * @class IBizReportPanel
 * @extends {IBizControl}
 */
var IBizReportPanel = /** @class */ (function (_super) {
    __extends(IBizReportPanel, _super);
    /**
     * Creates an instance of IBizReportPanel.
     * 创建 IBizReportPanel 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizReportPanel
     */
    function IBizReportPanel(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 是否显示报表，本地开发不显示报表
         *
         * @type {boolean}
         * @memberof IBizReportPanel
         */
        _this.showReport = false;
        /**
         * 报表ID
         *
         * @type {string}
         * @memberof IBizReportPanel
         */
        _this.reportid = '';
        return _this;
    }
    /**
     * 数据加载
     *
     * @memberof IBizReportPanel
     */
    IBizReportPanel.prototype.load = function () {
        this.buildReport();
    };
    /**
     * 处理报表内容
     *
     * @memberof IBizReportPanel
     */
    IBizReportPanel.prototype.buildReport = function () {
        var params = {};
        this.fire(IBizReportPanel.BEFORELOAD, params);
        if (params.srfaction) {
            delete params.srfaction;
        }
        if (Object.is(this.reportid, '')) {
            return;
        }
        Object.assign(params, { srfreportid: this.reportid });
        if (!IBizEnvironment.LocalDeve) {
            if (Object.keys(params).length !== 0) {
                this.showReport = true;
                var _data_1 = [];
                var _paramsArr = Object.keys(params);
                _paramsArr.forEach(function (key) {
                    _data_1.push("{key}={params[key]}");
                });
                var urlData = _data_1.join('&');
                this.viewurl = "..{IBizEnvironment.PDFReport}?{urlData}";
            }
        }
    };
    /**
     * 加载之前
     *
     * @static
     * @memberof IBizReportPanel
     */
    IBizReportPanel.BEFORELOAD = 'BEFORELOAD';
    return IBizReportPanel;
}(IBizControl));
