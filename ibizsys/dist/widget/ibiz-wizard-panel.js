"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 向导面板
 *
 * @class IBizWizardPanel
 * @extends {IBizControl}
 */
var IBizWizardPanel = /** @class */ (function (_super) {
    __extends(IBizWizardPanel, _super);
    /**
     * Creates an instance of IBizWizardPanel.
     * 创建 IBizWizardPanel 实例
     *
     * @param {*} [otps={}]
     * @memberof IBizWizardPanel
     */
    function IBizWizardPanel(opt) {
        if (opt === void 0) { opt = {}; }
        var _this = _super.call(this, opt) || this;
        /**
         * 向导表单集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.forms = {};
        /**
         * 向导表单名称集合
         *
         * @type {Array<string>}
         * @memberof IBizWizardPanel
         */
        _this.formNames = [];
        /**
         * 向导表单行为集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.acitons = {};
        /**
         * 当前激活表单名称
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.curForm = {};
        /**
         * 当前激活表单名称
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.curFormName = null;
        /**
         * 向导面板参数
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.params = {};
        /**
         * 执行过的表单集合
         *
         * @type {Array<String>}
         * @memberof IBizWizardPanel
         */
        _this.prevForms = [];
        /**
         * 步骤集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.steps = [];
        /**
         * 当前步骤
         *
         * @type {String}
         * @memberof IBizWizardPanel
         */
        _this.curStep = '';
        /**
         * 表单步骤集合
         *
         * @type {*}
         * @memberof IBizWizardPanel
         */
        _this.formSteps = {};
        /**
         * 下一步后续操作行为
         *
         * @type {String}
         * @memberof IBizWizardPanel
         */
        _this.afterformsaveaction = '';
        _this.regWizardForms();
        _this.regFormActions();
        _this.regSteps();
        _this.regFormSteps();
        _this.setDefFrom();
        return _this;
    }
    /**
     * 注册向导表单
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regWizardForms = function () {
    };
    /**
     * 注册表单面板行为
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regFormActions = function () {
    };
    /**
 * 注册向导表单项
 *
 * @param {*} form
 * @memberof IBizWizardPanel
 */
    IBizWizardPanel.prototype.regForm = function (form) {
        var _this = this;
        if (form) {
            // 表单保存之前
            form.on(IBizEditForm.FORMBEFORESAVE).subscribe(function (data) {
                // this.onFormBeforeSaved(data);
            });
            // 表单保存完成
            form.on(IBizForm.FORMSAVED).subscribe(function (data) {
                _this.onFormSaved(data);
            });
            // 表单加载完成
            form.on(IBizForm.FORMLOADED).subscribe(function (data) {
                _this.fire(IBizWizardPanel.WIZARDFORMLOADED, data);
            });
            // 表单属性值变化
            form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
                if (data) {
                    Object.assign(data, { formName: form.getName() });
                    _this.fire(IBizWizardPanel.WIZARDFORMFIELDCHANGED, data);
                }
                else {
                    _this.fire(IBizWizardPanel.WIZARDFORMFIELDCHANGED, { formName: form.getName(), name: '' });
                }
            });
            this.formNames.push(form.getName());
            this.forms[form.getName()] = form;
        }
    };
    /**
     * 注册表单行为项
     *
     * @param {*} name
     * @param {*} action
     * @returns {void}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regAction = function (name, action) {
        if (name) {
            this.acitons[name] = action;
        }
    };
    /**
     * 注册步骤
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regSteps = function () {
    };
    /**
     * 注册表单步骤集合
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regFormSteps = function () {
    };
    /**
     * 注册表单步骤项
     *
     * @param {*} name
     * @param {*} item
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.regFormStep = function (name, item) {
        if (name) {
            this.formSteps[name] = item;
        }
    };
    /**
     * 设置默认表单
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.setDefFrom = function () {
    };
    /**
     * 获取表单
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getForm = function (name) {
        return this.forms[name];
    };
    /**
     * 获取表单对应步骤
     *
     * @param {*} name
     * @returns {*}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getStep = function (name) {
        if (name) {
            return this.formSteps[name];
        }
    };
    /**
     * 获取步骤对应下标
     *
     * @param {*} tag
     * @returns {Number}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getStepIndex = function (tag) {
        if (tag) {
            var index = this.steps.findIndex(function (step) { return Object.is(step.tag, tag); });
            if (index >= 0) {
                return index;
            }
            else if (Object.is(tag, 'finish')) {
                return this.steps.length;
            }
        }
        return 0;
    };
    /**
     * 向导面板加载
     *
     * @param {*} arg
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.autoLoad = function (arg) {
        var _this = this;
        var param = { srfaction: 'init', srfctrlid: this.getName() };
        Object.assign(param, arg);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (data) {
            if (data.ret === 0) {
                _this.params = data.data;
                if (_this.curForm) {
                    _this.curForm.autoLoad(_this.params);
                }
                _this.fire(IBizWizardPanel.WIZARDPANEL_INITED, data);
            }
            else {
            }
        }, function (data) {
            console.log(data);
        });
    };
    /**
     * 表单保存完成
     *
     * @param {*} data
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.onFormSaved = function (data) {
        var field = this.curForm.findField("srfnextform");
        if (Object.is(this.afterformsaveaction, 'NEXT') && field && !Object.is(field.getValue(), '')) {
            this.changeForm(field.getValue());
        }
        else if (Object.is(this.afterformsaveaction, 'NEXT') && this.getNext()) {
            this.changeForm(this.getNext());
        }
        else {
            this.curFormName = "finish";
            this.curForm = null;
            this.curStep = 'finish';
            this.finish();
        }
        this.fire(IBizWizardPanel.WIZARDFORMSAVED, data);
    };
    /**
     * 执行上一步
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.goPrev = function () {
        var length = this.prevForms.length;
        if (length > 0) {
            var name_1 = this.prevForms[length - 1];
            this.changeForm(name_1);
            this.prevForms.splice(length - 1, 1);
        }
    };
    /**
     * 执行下一步
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.goNext = function () {
        if (this.curForm) {
            this.afterformsaveaction = 'NEXT';
            this.prevForms.push(this.curFormName);
            this.curForm.save2();
        }
    };
    /**
     * 执行完成
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.goFinish = function () {
        if (this.curForm) {
            this.afterformsaveaction = 'FINISH';
            this.curForm.save2();
        }
    };
    /**
     * 切换表单
     *
     * @param {*} name
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.changeForm = function (name) {
        if (name && this.getForm(name)) {
            this.curFormName = name;
            this.curForm = this.getForm(name);
            this.curStep = this.getStep(name);
            this.curForm.autoLoad(this.params);
            this.fire(IBizWizardPanel.WIZARDFORMCHANGED, this);
        }
    };
    /**
     * 获取下一个表单名称
     *
     * @returns {String}
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.getNext = function () {
        var index = this.formNames.indexOf(this.curFormName);
        if (index >= 0) {
            if (this.formNames[index + 1]) {
                return this.formNames[index + 1];
            }
        }
        return undefined;
    };
    /**
     * 完成
     *
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.finish = function () {
        var _this = this;
        var param = { srfaction: "finish", srfctrlid: this.getName() };
        Object.assign(param, this.params);
        this.iBizHttp.post(this.getBackendUrl(), param).subscribe(function (data) {
            if (data.ret === 0) {
                _this.fire(IBizWizardPanel.WIZARDPANEL_FINISH, data);
            }
        }, function (data) {
            console.log(data);
        });
    };
    /**
     * 是否显示该行为按钮
     *
     * @param {*} form
     * @param {*} action
     * @returns
     * @memberof IBizWizardPanel
     */
    IBizWizardPanel.prototype.isVisible = function (name, action) {
        if (name) {
            var formActions = this.acitons[name];
            if (formActions) {
                var index = formActions.findIndex(function (item) { return Object.is(item, action); });
                if (index >= 0) {
                    return true;
                }
            }
        }
        return false;
    };
    /**
     * 向导表单项值改变
     */
    IBizWizardPanel.WIZARDFORMFIELDCHANGED = 'WIZARDFORMFIELDCHANGE';
    /**
     * 向导表单保存完成
     */
    IBizWizardPanel.WIZARDFORMSAVED = 'WIZARDFORMSAVED';
    /**
     * 向导表单加载完成
     */
    IBizWizardPanel.WIZARDFORMLOADED = 'WIZARDFORMLOADED';
    /**
     * 向导面板初始化完成
     */
    IBizWizardPanel.WIZARDPANEL_INITED = 'WIZARDPANEL_INITED';
    /**
     * 向导面板完成
     */
    IBizWizardPanel.WIZARDPANEL_FINISH = 'WIZARDPANEL_FINISH';
    /**
     * 向导表单切换
     */
    IBizWizardPanel.WIZARDFORMCHANGED = 'WIZARDFORMCHANGED';
    return IBizWizardPanel;
}(IBizControl));
