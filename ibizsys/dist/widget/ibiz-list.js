"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 列表部件
 *
 * @class IBizList
 * @extends {IBizMDControl}
 */
var IBizList = /** @class */ (function (_super) {
    __extends(IBizList, _super);
    /**
     * Creates an instance of IBizList.
     * 创建 IBizList 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizList
     */
    function IBizList(opts) {
        if (opts === void 0) { opts = {}; }
        return _super.call(this, opts) || this;
    }
    /**
     *
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizList
     */
    IBizList.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { 'srfctrlid': this.getName(), 'srfaction': 'fetch' });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = response.items;
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            console.log(error.info);
        });
    };
    /**
     * 删除单条数据
     *
     * @param {*} item
     * @memberof IBizList
     */
    IBizList.prototype.doRemove = function (item) {
        if (item) {
        }
    };
    /**
     * 删除所选数据
     *
     * @memberof IBizList
     */
    IBizList.prototype.doRemoveAll = function () {
    };
    return IBizList;
}(IBizMDControl));
