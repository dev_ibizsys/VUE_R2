"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 时间轴
 *
 * @class IBizTimeline
 * @extends {IBizControl}
 */
var IBizTimeline = /** @class */ (function (_super) {
    __extends(IBizTimeline, _super);
    function IBizTimeline() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * 数据集合
         *
         * @type {Array<any>}
         * @memberof IBizTimeline
         */
        _this.items = [];
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @memberof IBizTimeline
     */
    IBizTimeline.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        this.fire(IBizMDControl.BEFORELOAD, opt);
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            if (response.ret === 0) {
                _this.items = response.items;
            }
            else {
                _this.iBizNotification.error('', response.errorMessage);
            }
        }, function (error) {
            console.log(error.info);
        });
    };
    /**
     * 刷新数据
     *
     * @param {*} [arg={}]
     * @memberof IBizTimeline
     */
    IBizTimeline.prototype.refresh = function (arg) {
        if (arg === void 0) { arg = {}; }
        this.load(arg);
    };
    return IBizTimeline;
}(IBizControl));
