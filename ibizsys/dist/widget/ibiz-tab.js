"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * 分页
 *
 * @class IBizTab
 * @extends {IBizControl}
 */
var IBizTab = /** @class */ (function (_super) {
    __extends(IBizTab, _super);
    /**
     * Creates an instance of IBizTab.
     * 创建 IBizTab 实例
     * @param {*} [opts={}]
     * @memberof IBizTab
     */
    function IBizTab(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 激活分页
         *
         * @type {*}
         * @memberof IBizTab
         */
        _this.activeTab = {};
        /**
         * 分页部件对象
         *
         * @type {*}
         * @memberof IBizTab
         */
        _this.tabs = {};
        _this.regTabs();
        return _this;
    }
    /**
     * 注册所有分页部件对象
     *
     * @memberof IBizTab
     */
    IBizTab.prototype.regTabs = function () {
    };
    /**
     * 注册分页部件对象
     *
     * @param {*} [tab={}]
     * @memberof IBizTab
     */
    IBizTab.prototype.regTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        if (Object.keys(tab).length > 0 && tab.name) {
            var viewController = this.getViewController();
            var view = viewController.getDRItemView({ viewid: tab.name });
            if (view) {
                Object.assign(tab, view);
            }
            this.tabs[tab.name] = tab;
        }
    };
    /**
     * 获取分页部件对象
     *
     * @param {string} name
     * @param {string} [routepath]
     * @returns {*}
     * @memberof IBizTab
     */
    IBizTab.prototype.getTab = function (name, routepath) {
        var tab_arr = Object.values(this.tabs);
        var tab = {};
        tab_arr.some(function (_tab) {
            if (Object.is(_tab.name, name) || (_tab.routepath && Object.is(_tab.routepath, routepath))) {
                Object.assign(tab, _tab);
                return true;
            }
        });
        return tab;
    };
    /**
     * 设置激活分页
     *
     * @param {*} [tab={}]
     * @memberof IBizTab
     */
    IBizTab.prototype.setActiveTab = function (tab) {
        if (tab === void 0) { tab = {}; }
        if (!Object.is(tab.name, this.activeTab.name)) {
            this.activeTab = {};
            Object.assign(this.activeTab, tab);
        }
    };
    /**
     * 设置分页状态
     *
     * @param {boolean} state
     * @memberof IBizTab
     */
    IBizTab.prototype.setTabState = function (state) {
    };
    return IBizTab;
}(IBizControl));
