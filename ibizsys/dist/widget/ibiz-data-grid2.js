"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
/**
 * agGrid控件
 *
 * @class IBizDataGridPro
 * @extends {IBizDataGrid}
 */
var IBizDataGrid2 = /** @class */ (function (_super) {
    __extends(IBizDataGrid2, _super);
    /**
     *Creates an instance of IBizDataGrid2.
     * @param {*} [opts={}]
     * @memberof IBizDataGrid2
     */
    function IBizDataGrid2(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * agGrid控件api对象
         *
         * @type {*}
         * @memberof IBizDataGridPro
         */
        _this.agGridApi = null;
        /**
         * agGrid列api对象
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.agGridColumnApi = null;
        /**
         * agGrid列数组
         *
         * @type {Array<any>}
         * @memberof IBizDataGrid2
         */
        _this.columnDefs = [];
        /**
         * 树表格列模型
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.treeColMode = {};
        /**
         * 自定义分组列
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.autoGroupColumn = null;
        /**
         * 列过滤参数
         *
         * @type {*}
         * @memberof IBizDataGrid2
         */
        _this.colFilterParams = {};
        /**
         * 是否开启列过滤
         *
         * @type {boolean}
         * @memberof IBizDataGrid2
         */
        _this.isOpenFilter = false;
        if (Object.is(_this.getGridStyle(), 'TREEGRID')) {
            _this.setTreeColMode();
        }
        _this.setIsOpenFilter();
        _this.regColumnDefs();
        if (!_this.autoGroupColumn) {
            _this.setCheckboxColumn();
        }
        return _this;
    }
    /**
     * 加载数据
     *
     * @param {*} [arg={}]
     * @returns {void}
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.load = function (arg) {
        var _this = this;
        if (arg === void 0) { arg = {}; }
        // tslint:disable-next-line:prefer-const
        var opt = {};
        Object.assign(opt, arg);
        Object.assign(opt, { srfctrlid: this.getName(), srfaction: 'fetch' });
        if (!opt.start) {
            Object.assign(opt, { start: (this.curPage - 1) * this.limit });
        }
        if (!opt.limit) {
            Object.assign(opt, { limit: this.limit });
        }
        Object.assign(opt, { sort: JSON.stringify(this.gridSortField) });
        // 发送加载数据前事件
        this.fire(IBizMDControl.BEFORELOAD, opt);
        if (this.setIsOpenFilter) {
            Object.assign(opt, this.colFilterParams);
        }
        this.allChecked = false;
        this.indeterminate = false;
        this.selection = [];
        this.fire(IBizMDControl.SELECTIONCHANGE, this.selection);
        this.beginLoading();
        this.iBizHttp.post(this.getBackendUrl(), opt).subscribe(function (response) {
            _this.endLoading();
            if (!response.items || response.ret !== 0) {
                if (response.errorMessage) {
                    _this.iBizNotification.error('', response.errorMessage);
                }
                return;
            }
            _this.items = _this.rendererDatas(response.items);
            _this.totalrow = response.totalrow;
            if (response.summaryitem) {
                _this.setPinnedBottomRow(response.summaryitem);
            }
            _this.fire(IBizMDControl.LOADED, response.items);
        }, function (error) {
            _this.endLoading();
            console.log(error.info);
        });
    };
    /**
     * 表格准备完成
     *
     * @param {*} obj
     * @memberof IBizDataGridPro
     */
    IBizDataGrid2.prototype.onGridReady = function () {
        var _this = this;
        return function (params) {
            _this.agGridApi = params.api;
            _this.agGridColumnApi = params.columnApi;
        };
    };
    /**
     * 排序
     *
     * @param {*} argu
     * @memberof IBizDataGrid
     */
    IBizDataGrid2.prototype.elementSortChange = function () {
        var _this = this;
        return function () {
            var sorts = _this.agGridApi.getSortModel();
            if (sorts.length > 0) {
                if (Object.is(sorts[0].colId, 'ag-Grid-AutoColumn')) {
                    _this.sort(_this.autoGroupColumn.field, sorts[0].sort);
                }
                else {
                    _this.sort(sorts[0].colId, sorts[0].sort);
                }
            }
        };
    };
    /**
     * 选中值变化
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.selectionChanged = function () {
        var _this = this;
        return function () {
            if (_this.openRowEdit) {
                _this.agGridApi.deselectAll();
                return;
            }
            _this.selection = _this.agGridApi.getSelectedRows();
            _this.fire(IBizMDControl.SELECTIONCHANGE, _this.selection);
        };
    };
    IBizDataGrid2.prototype.isRowSelectable = function () {
        var _this = this;
        return function () {
            return !_this.openRowEdit;
        };
    };
    /**
     * 行双击
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.rowDbClicked = function () {
        var _this = this;
        return function (param) {
            if (!_this.openRowEdit && !Object.is(param.rowPinned, 'bottom') && !Object.is(param.rowPinned, 'top')) {
                _this.fire(IBizDataGrid.ROWDBLCLICK, _this.selection);
            }
        };
    };
    /**
     * 设置是否支持多选
     *
     * @param {boolean} state 是否支持多选
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.setMultiSelect = function (state) {
        _super.prototype.setMultiSelect.call(this, state);
        this.autoGroupColumn.headerCheckboxSelection = state;
        if (this.agGridApi) {
            this.agGridApi.refreshHeader();
        }
    };
    /**
     * 注册多数据列
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.regColumnDefs = function () {
    };
    /**
     * 注册多数据列
     *
     * @param {*} column
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.regColumnDef = function (column) {
        if (Object.keys(column).length === 0) {
            return;
        }
        this.columnDefs.push(column);
        this.regColumn(column);
    };
    /**
     * 设置多数据列头
     *
     * @param {*} [column={}]
     * @returns {void}
     * @memberof IBizMDControl
     */
    IBizDataGrid2.prototype.regColumn = function (column) {
        var _this = this;
        if (column === void 0) { column = {}; }
        if (Object.keys(column).length === 0) {
            return;
        }
        if (!this.columns) {
            this.columns = [];
        }
        if (Object.is(column.colType, 'GROUPGRIDCOLUMN') && column.children) {
            column.children.forEach(function (col) {
                _this.regColumn(col);
            });
        }
        else {
            if (Object.is(this.getGridStyle(), 'TREEGRID') && this.treeColMode.parentText && Object.is(this.treeColMode.text, column.field)) {
                this.setAutoTreeColumn(column);
                column.hide = true;
            }
            else if (column.rowGroupIndex === 1) {
                this.setAutoGroupColumn(column);
                column.hide = true;
            }
            if (column.filter) {
                this.colFilterParams[column.searchName] = '';
            }
            this.columns.push(column);
        }
    };
    /**
     * 默认展开分组层级
     * {-1: 全部展开, 0: 不展开, 1: 展开一级}
     * @returns {number}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getGroupDefExpand = function () {
        return -1;
    };
    /**
     * 分组列
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getAutoGroupCol = function () {
        return this.autoGroupColumn;
    };
    /**
     * 获取树数据层级结构
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getTreePath = function () {
        return function (data) {
            return data.paths;
        };
    };
    /**
     * 获取表格样式类型
     *
     * @returns {string}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getGridStyle = function () {
        return '';
    };
    /**
     * 设置树模型
     *
     * @param {*} [column={}]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setTreeColMode = function () {
    };
    /**
     * 设置自定义树列
     *
     * @param {*} [column={}]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setAutoTreeColumn = function (column) {
        if (column === void 0) { column = {}; }
        if (!this.autoGroupColumn) {
            this.autoGroupColumn = {};
        }
        Object.assign(this.autoGroupColumn, column);
        delete this.autoGroupColumn.hide;
        this.autoGroupColumn.cellRendererParams = {
            checkbox: true,
            suppressCount: true,
        };
    };
    /**
     * 设置自定义分组列
     *
     * @param {*} [column={}]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setAutoGroupColumn = function (column) {
        if (column === void 0) { column = {}; }
        if (!this.autoGroupColumn) {
            this.autoGroupColumn = {};
        }
        Object.assign(this.autoGroupColumn, column);
        delete this.autoGroupColumn.hide;
        this.autoGroupColumn.cellRendererParams = {
            checkbox: true,
            suppressCount: true,
        };
    };
    /**
     * 创建复选框列
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setCheckboxColumn = function () {
        if (!this.autoGroupColumn) {
            this.autoGroupColumn = {};
        }
        Object.assign(this.autoGroupColumn, {
            width: 42,
            maxWidth: 42,
            pinned: 'left',
            field: 'srf_grid_checkbox',
            suppressMovable: true,
            suppressResize: true,
            suppressMenu: true,
            suppressSorting: true,
            suppressFilter: true,
            checkboxSelection: true,
            suppressSizeToFit: true,
            headerCheckboxSelection: true
        });
        if (this.columnDefs) {
            this.columnDefs.push(this.autoGroupColumn);
        }
    };
    /**
     * 渲染绘制多项数据
     *
     * @param {Array<any>} items
     * @returns {Array<any>}
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.rendererDatas = function (items) {
        var _this = this;
        if (Object.is(this.getGridStyle(), 'TREEGRID') && this.treeColMode.parentId && this.treeColMode.id) {
            var arr = items.slice();
            arr.forEach(function (item) {
                item.paths = [];
                _this.setTreePath(items, item, item.paths);
            });
        }
        return items;
    };
    /**
     * 设置树路径
     *
     * @param {Array<any>} items
     * @param {*} [item={}]
     * @param {Array<string>} [paths=[]]
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setTreePath = function (items, item, paths) {
        var _this = this;
        if (item === void 0) { item = {}; }
        if (paths === void 0) { paths = []; }
        var id = this.treeColMode.id;
        var text = this.treeColMode.text ? this.treeColMode.text : 'srfmajortext';
        var parentId = this.treeColMode.parentId;
        var parentText = this.treeColMode.parentText;
        if (item[parentId] && !Object.is(item[parentId], '')) {
            var hesParent_1 = true;
            items.forEach(function (data) {
                if (Object.is(data[id], item[parentId])) {
                    _this.setTreePath(items, data, paths);
                    hesParent_1 = false;
                }
            });
            if (hesParent_1 && parentText) {
                var index = items.indexOf(item);
                var data = {};
                data[id] = item[parentId];
                data[text] = item[parentText];
                data.paths = [item[parentText]];
                items.splice(index, 0, data);
                paths.push(item[parentText]);
            }
        }
        paths.push(item[text]);
    };
    /**
     * 是否开启行编辑
     *
     * @returns {boolean}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getIsOpenEdit = function () {
        var _this = this;
        return function (param) {
            return (_this.openRowEdit && param.node && !param.node.rowPinned);
        };
    };
    /**
     * 启用行编辑
     *
     * @param {*} params
     * @returns {void}
     * @memberof IBizDataGrid
     */
    IBizDataGrid2.prototype.openEdit = function (params) {
        var _this = this;
        if (!this.isEnableRowEdit) {
            this.iBizNotification.info('提示', '未启用行编辑');
            return;
        }
        this.agGridApi.deselectAll();
        if (!this.openRowEdit) {
            this.openRowEdit = true;
            this.items.forEach(function (item) { item.openeditrow = true; });
            this.backupDatas = JSON.parse(JSON.stringify(this.items)).slice();
            this.items.forEach(function (item, index) {
                _this.setEditItemState(item.srfkey);
                _this.editRow(item, index, false);
            });
        }
    };
    /**
     * 行编辑单元格值变化
     *
     * @param {string} name
     * @param {*} data
     * @memberof IBizGrid
     */
    IBizDataGrid2.prototype.colValueChange = function (name, value, data) {
        var srfkey = data.srfkey;
        var _data = this.items.find(function (back) { return Object.is(back.srfkey, srfkey); });
        if (this.state[srfkey] && this.state[srfkey][name]) {
            data[name] = value;
            this.fire(IBizDataGrid.UPDATEGRIDITEMCHANGE, { name: name, data: data });
        }
    };
    /**
     * 获取语言资源
     *
     * @returns {*}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getLocaleText = function () {
        return IBizDataGrid2.LOCALETEXT;
    };
    /**
     * 设置是否开启列过滤
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setIsOpenFilter = function () {
    };
    /**
     * 获取列过滤字段值
     *
     * @param {*} param
     * @returns {*}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getColFilterValue = function (param) {
        if (!param) {
            return;
        }
        var filterModel = this.agGridApi.getFilterModel();
        var defCol = param.colDef;
        if (defCol && filterModel[defCol.field]) {
            return filterModel[defCol.field].filter;
        }
        return '';
    };
    /**
     * 过滤值改变
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.filterChanged = function () {
        var _this = this;
        return function () {
            var filterModel = _this.agGridApi.getFilterModel();
            var keys = Object.keys(_this.colFilterParams);
            var isChange = false;
            keys.forEach(function (key) {
                if (filterModel) {
                    var index = _this.columns.findIndex(function (column) { return Object.is(column.searchName, key); });
                    var column = _this.columns[index];
                    if (filterModel.hasOwnProperty(column.field)) {
                        if (Object.is(_this.colFilterParams[key], filterModel[column.field].filter)) {
                            return;
                        }
                        else {
                            _this.colFilterParams[key] = filterModel[column.field].filter;
                            isChange = true;
                        }
                    }
                    else if (Object.is(_this.colFilterParams[key], '')) {
                        return;
                    }
                    else {
                        _this.colFilterParams[key] = '';
                        isChange = true;
                    }
                }
                else if (Object.is(_this.colFilterParams[key], '')) {
                    return;
                }
                else {
                    _this.colFilterParams[key] = '';
                    isChange = true;
                }
            });
            if (isChange) {
                _this.refresh();
            }
        };
    };
    /**
     * 获取预置组件
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getFrameworkComponents = function () {
        return {
            customTextColumnFilter: Vue.options.components['costom-text-column-filter'],
            customSelectColumnFilter: Vue.options.components['costom-select-column-filter'],
        };
    };
    /**
     * 获取列默认设置
     *
     * @returns
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getDefColOption = function () {
        var _this = this;
        return {
            menuTabs: ['generalMenuTab', 'columnsMenuTab'],
            pinnedRowCellRenderer: function (data) {
                return _this.getPinnedRowCellRender(data);
            }
        };
    };
    /**
     * 设置底部固定行
     *
     * @param {*} item
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.setPinnedBottomRow = function (item) {
        if (item) {
            if (Array.isArray(item)) {
                this.agGridApi.setPinnedBottomRowData(item);
            }
            else {
                this.agGridApi.setPinnedBottomRowData([item]);
            }
        }
    };
    /**
     * 获取固定行列绘制
     *
     * @param {*} data
     * @returns {string}
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.getPinnedRowCellRender = function (data) {
        return data.value;
    };
    /**
     * 表格模型变化
     *
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.prototype.onGridModelChange = function () {
        var _this = this;
        var subject = new rxjs.Subject();
        subject.pipe(rxjs.operators.debounceTime(2000)).subscribe(function () {
            _this.setModel(_this.agGridColumnApi.getColumnState());
        });
        return function () {
            subject.next();
        };
    };
    /**
     * 多语言设置
     *
     * @static
     * @memberof IBizDataGrid2
     */
    IBizDataGrid2.LOCALETEXT = {
        pinColumn: '锁定列',
        autosizeThiscolumn: '自动调整此列的大小',
        autosizeAllColumns: '自动调整所有列的大小',
        resetColumns: '重置所有列',
        pinLeft: '锁定左侧',
        pinRight: '锁定右侧',
        noPin: '不锁定',
        noRowsToShow: '暂无数据'
    };
    return IBizDataGrid2;
}(IBizDataGrid));
