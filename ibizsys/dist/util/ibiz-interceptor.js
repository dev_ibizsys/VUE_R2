"use strict";
// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});
// Add a response interceptor
axios.interceptors.response.use(function (response) {
    // Do something with response data
    var data = null;
    if (response.data) {
        data = response.data;
    }
    // no login
    if (data && data.ret === 2 && data.notlogin) {
        var curUrl = decodeURIComponent(window.location.href);
        if (IBizEnvironment.CustomLoginRedirect) {
            if (window.location.href.indexOf(IBizEnvironment.LoginRedirect) === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
        else {
            if (window.location.href.indexOf('/ibizutil/login.html') === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
    }
    return data;
}, function (error) {
    // Do something with response error
    error = error ? error : { response: {} };
    var res = error.response;
    var _data = res.data;
    // no login
    if (_data && _data.ret === 2 && _data.notlogin) {
        var curUrl = decodeURIComponent(window.location.href);
        if (IBizEnvironment.CustomLoginRedirect) {
            if (window.location.href.indexOf(IBizEnvironment.LoginRedirect) === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
        else {
            if (window.location.href.indexOf('/ibizutil/login.html') === -1) {
                window.location.href = "/" + IBizEnvironment.BaseUrl + IBizEnvironment.LoginRedirect + "?RU=" + encodeURIComponent(curUrl);
            }
        }
    }
    // 其他状态码信息
    if (!_data) {
        _data = IBizHandResponseData.doErrorResponseData(res);
    }
    return Promise.reject(_data);
});
