"use strict";
/**
 * IBizApp 应用
 * 调用 getInstance() 获取实列
 *
 * @class IBizApp
 */
var IBizApp = /** @class */ (function () {
    /**
     * Creates an instance of IBizApp.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof IBizApp
     */
    function IBizApp() {
        var _this = this;
        /**
         * 当前窗口所有视图控制器
         *
         * @type {Array<any>}
         * @memberof IBizApp
         */
        this.viewControllers = [];
        /**
         * 父窗口window对象
         *
         * @type {*}
         * @memberof IBizApp
         */
        this.parentWindow = null;
        /**
         * rxjs 流观察对象
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizApp
         */
        this.subject = new rxjs.Subject();
        /**
         * postMessage 流观察对象
         *
         * @private
         * @type {Subject<any>}
         * @memberof IBizApp
         */
        this.postMessage = new rxjs.Subject();
        /**
         * 主题颜色
         *
         * @type {String}
         * @memberof IBizApp
         */
        this.themeClass = '';
        /**
         * 字体
         *
         * @private
         * @type {String}
         * @memberof IBizApp
         */
        this.fontFamily = '';
        /**
         * app应用功能数据
         *
         * @private
         * @type {string}
         * @memberof IBizApp
         */
        this.appData = '';
        var win = window;
        win.addEventListener('message', function (message) {
            if (message.data && Object.is(message.data.ret, 'OK') && !Object.is(message.data.type, '')) {
                _this.firePostMessage(message.data);
            }
        }, false);
    }
    /**
     * 获取 IBizApp 单列对象
     *
     * @static
     * @returns {IBizApp}
     * @memberof IBizApp
     */
    IBizApp.getInstance = function () {
        if (!IBizApp.iBizApp) {
            IBizApp.iBizApp = new IBizApp();
        }
        return IBizApp.iBizApp;
    };
    /**
     * 注册视图控制
     *
     * @param {*} ctrler
     * @memberof IBizApp
     */
    IBizApp.prototype.regSRFController = function (ctrler) {
        this.viewControllers.push(ctrler);
    };
    /**
     * 注销视图控制器
     *
     * @param {*} ctrler
     * @memberof IBizApp
     */
    IBizApp.prototype.unRegSRFController = function (ctrler) {
        var id = ctrler.getId();
        var viewUsage = ctrler.getViewUsage();
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            this.viewControllers[index] = null;
            this.viewControllers.splice(index, 1);
        }
    };
    /**
     * 注销视图控制器
     *
     * @param {string} id 视图id
     * @param {number} viewUsage 视图使用模式
     * @memberof IBizApp
     */
    IBizApp.prototype.unRegSRFController2 = function (id, viewUsage) {
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            this.viewControllers[index] = null;
            this.viewControllers.splice(index, 1);
        }
    };
    /**
     * 获取视图控制器
     *
     * @param {string} id 视图id
     * @param {number} viewUsage 视图使用模式
     * @returns {*}
     * @memberof IBizApp
     */
    IBizApp.prototype.getSRFController = function (id, viewUsage) {
        var viewController = null;
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            viewController = this.viewControllers[index];
        }
        return viewController;
    };
    /**
     * 获取父视图控制器
     *
     * @param {string} id 视图id
     * @param {number} viewUsage 视图使用模式
     * @returns {*}
     * @memberof IBizApp
     */
    IBizApp.prototype.getParentController = function (id, viewUsage) {
        var viewController = null;
        var index = this.viewControllers.findIndex(function (ctrl) { return Object.is(id, ctrl.getId()) && Object.is(viewUsage, ctrl.getViewUsage()); });
        if (index !== -1) {
            viewController = this.viewControllers[index - 1];
        }
        return viewController;
    };
    /**
     * 注册父窗口window 对象
     *
     * @param {Window} win
     * @memberof IBizApp
     */
    IBizApp.prototype.regParentWindow = function (win) {
        this.parentWindow = win;
    };
    /**
     * 获取父窗口window 对象
     *
     * @returns {Window}
     * @memberof IBizApp
     */
    IBizApp.prototype.getParentWindow = function () {
        return this.parentWindow;
    };
    /**
     * 订阅刷新视图事件
     *
     * @returns {Subject<any>}
     * @memberof IBizApp
     */
    IBizApp.prototype.onRefreshView = function () {
        return this.subject;
    };
    /**
     * 通知视图刷新事件
     *
     * @param {*} data
     * @memberof IBizApp
     */
    IBizApp.prototype.fireRefreshView = function (data) {
        if (data === void 0) { data = {}; }
        this.subject.next(data);
    };
    /**
     * 订阅postMessage对象
     *
     * @returns {Subject<any>}
     * @memberof IBizApp
     */
    IBizApp.prototype.onPostMessage = function () {
        return this.postMessage;
    };
    /**
     * 处理postMessage对象
     *
     * @param {*} [data={}]
     * @memberof IBizApp
     */
    IBizApp.prototype.firePostMessage = function (data) {
        if (data === void 0) { data = {}; }
        this.postMessage.next(data);
    };
    /**
     * 设置主题颜色
     *
     * @param {String} name
     * @memberof IBizApp
     */
    IBizApp.prototype.setThemeClass = function (name) {
        this.themeClass = name;
        Cookies.set('theme-class', name);
    };
    /**
     * 获取主题颜色
     *
     * @returns {String}
     * @memberof IBizApp
     */
    IBizApp.prototype.getThemeClass = function () {
        if (Object.is(this.themeClass, '')) {
            var name_1 = Cookies.get('theme-class');
            if (name_1) {
                return name_1;
            }
            return 'ibiz-default-theme';
        }
        else {
            return this.themeClass;
        }
    };
    /**
     * 设置字体
     *
     * @param {String} font
     * @memberof IBizApp
     */
    IBizApp.prototype.setFontFamily = function (font) {
        this.fontFamily = font;
        Cookies.set('font-family', font);
    };
    /**
     * 获取字体
     *
     * @returns {String}
     * @memberof IBizApp
     */
    IBizApp.prototype.getFontFamily = function () {
        if (Object.is(this.fontFamily, '')) {
            var font = Cookies.get('font-family');
            if (font) {
                return font;
            }
            return '';
        }
        else {
            return this.fontFamily;
        }
    };
    /**
     * 获取应用功能数据
     *
     * @returns {string}
     * @memberof IBizApp
     */
    IBizApp.prototype.getAppData = function () {
        return this.appData;
    };
    /**
     * 设置应用功能数据
     *
     * @param {string} appdata
     * @memberof IBizApp
     */
    IBizApp.prototype.setAppData = function (appdata) {
        this.appData = appdata;
    };
    /**
     * 单列变量声明
     *
     * @private
     * @static
     * @type {IBizApp}
     * @memberof IBizApp
     */
    IBizApp.iBizApp = null;
    return IBizApp;
}());
// 初始化IBizApp 对象， 挂载在window对象下
(function () {
    var win = window;
    if (!win.iBizApp) {
        win.iBizApp = IBizApp.getInstance();
    }
    win.getIBizApp = function () {
        if (!win.iBizApp) {
            win.iBizApp = IBizApp.getInstance();
        }
        return win.iBizApp;
    };
    if (window.opener && window.opener.window) {
        IBizApp.getInstance().regParentWindow(window.opener.window);
    }
})();
