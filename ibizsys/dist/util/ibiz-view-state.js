"use strict";
/**
 * 视图工具
 *
 * @class IBizViewState
 */
var IBizViewState = /** @class */ (function () {
    /**
     * Creates an instance of IBizViewState.
     * 创建 IBizViewState 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizViewState
     */
    function IBizViewState(opts) {
        if (opts === void 0) { opts = {}; }
    }
    /**
     * 获取 IBizViewState 单例
     *
     * @private
     * @static
     * @returns {IBizViewState}
     * @memberof IBizViewState
     */
    IBizViewState.getInstance = function () {
        if (!this.iBizViewState) {
            this.iBizViewState = new IBizViewState();
        }
        return this.iBizViewState;
    };
    /**
     * 加载视图状态
     *
     * @param {string} viewid
     * @param {*} [viewstate={}]
     * @returns {*}
     * @memberof IBizViewState
     */
    IBizViewState.prototype.loadViewStates = function (viewid) {
        var subject = new rxjs.Subject();
        if (Object.is(IBizEnvironment.address, 'remote')) {
            subject.next(Cookies.get(viewid));
            subject.complete();
        }
        else {
            var data = { srfaction: 'load', viewid: viewid };
            IBizHttp.getInstance().post(IBizEnvironment.url, data).subscribe(function (success) {
                var viewstate = '';
                if (success.ret === 0) {
                    viewstate = success.viewstate;
                }
                subject.next(viewstate);
                subject.complete();
            }, function (error) {
                subject.next('');
                subject.complete();
            });
        }
        return subject;
    };
    /**
     * 保存视图状态
     *
     * @param {string} viewid
     * @param {*} [viewstate={}]
     * @memberof IBizViewState
     */
    IBizViewState.prototype.saveViewStates = function (viewid, viewstate) {
        if (viewstate === void 0) { viewstate = {}; }
        if (Object.is(IBizEnvironment.address, 'remote')) {
            Cookies.set(viewid, JSON.stringify(viewstate));
        }
        else {
            var data = { srfaction: 'save', viewid: viewid, viewstate: JSON.stringify(viewstate) };
            IBizHttp.getInstance().post(IBizEnvironment.url, data);
        }
    };
    /**
     * 视图状态实例管理对象
     *
     * @private
     * @static
     * @type {IBizViewState}
     * @memberof IBizViewState
     */
    IBizViewState.iBizViewState = null;
    return IBizViewState;
}());
