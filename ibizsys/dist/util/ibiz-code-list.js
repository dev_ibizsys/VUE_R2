"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
/**
 * 代码表对象
 *
 * @class IBizCodeList
 * @extends {IBizObject}
 */
var IBizCodeList = /** @class */ (function (_super) {
    __extends(IBizCodeList, _super);
    /**
     * Creates an instance of IBizCodeList.
     * 创建 IBizCodeList 实例
     *
     * @param {*} [opts={}]
     * @memberof IBizCodeList
     */
    function IBizCodeList(opts) {
        if (opts === void 0) { opts = {}; }
        var _this = _super.call(this, opts) || this;
        /**
         * 静态代码表数据
         *
         * @type {Array<any>}
         * @memberof IBizCodeList
         */
        _this.data = [];
        _this.data = opts.datas.slice();
        return _this;
    }
    /**
     * 获取代码表数据
     *
     * @returns {Array<any>}
     * @memberof IBizCodeList
     */
    IBizCodeList.prototype.getData = function () {
        return this.data;
    };
    /**
     * 获取数据项的值
     *
     * @param {string} value
     * @param {*} cascade
     * @returns {*}
     * @memberof IBizCodeList
     */
    IBizCodeList.prototype.getItemByValue = function (value, cascade) {
        var result = {};
        var arr = this.data.filter(function (item) { return Object.is(item.value, value); });
        if (arr.length !== 1) {
            return undefined;
        }
        result = __assign({}, arr[0]);
        return result;
    };
    /**
     * 获取代码表文本值
     *
     * @param {*} [item={}]
     * @returns {string}
     * @memberof IBizCodeList
     */
    IBizCodeList.prototype.getCodeItemText = function (item) {
        if (item === void 0) { item = {}; }
        var color = item.color;
        var textCls = item.textcls;
        var iconCls = item.iconcls;
        var realText = item.text;
        var ret = '';
        if (iconCls) {
            ret = "<i class='" + iconCls + "'></i>&nbsp;";
        }
        if (textCls || color) {
            ret = ret + "<span class='" + (textCls ? textCls : '') + "' style='color: " + (color ? color : '') + "'>" + realText + "</span>";
        }
        else {
            ret += realText;
        }
        return ret;
    };
    return IBizCodeList;
}(IBizObject));
