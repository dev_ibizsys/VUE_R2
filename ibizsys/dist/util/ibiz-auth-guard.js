"use strict";
/**
 * 守卫认证
 *
 * @class IBizAuthguard
 */
var IBizAuthguard = /** @class */ (function () {
    /**
     * Creates an instance of IBizAuthguard.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof IBizAuthguard
     */
    function IBizAuthguard() {
    }
    /**
     * 获取单例
     *
     * @static
     * @returns {IBizAuthguard}
     * @memberof IBizAuthguard
     */
    IBizAuthguard.getInstance = function () {
        if (!IBizAuthguard.iBizAuthguard) {
            IBizAuthguard.iBizAuthguard = new IBizAuthguard();
        }
        return this.iBizAuthguard;
    };
    /**
     * 激活守卫处理
     *
     * @param {string} url
     * @returns {Subject<boolean>}
     * @memberof IBizAuthguard
     */
    IBizAuthguard.prototype.canActivate = function (url, params) {
        if (params === void 0) { params = {}; }
        var subject = new rxjs.Subject();
        var http = IBizHttp.getInstance();
        var opt = { srfaction: 'loadappdata' };
        Object.assign(opt, params);
        var _params = [];
        Object.keys(params).forEach(function (name) {
            if (params[name] && !Object.is(params[name], '')) {
                try {
                    JSON.parse(params[name]);
                }
                catch (e) {
                    _params.push(name + "=" + params[name]);
                }
            }
        });
        url = url.indexOf('?') !== -1 ? url + "&" + _params.join('&') : url + "?" + _params.join('&');
        http.post(url, opt).subscribe(function (success) {
            if (success && success.ret === 0) {
                if (success.remotetag) {
                    var win = window;
                    var iBizApp = win.getIBizApp();
                    iBizApp.setAppData(success.remotetag);
                }
            }
            subject.next(true);
        }, function (error) {
            subject.next(true);
        });
        return subject;
    };
    /**
     * 单列变量
     *
     * @private
     * @static
     * @type {IBizAuthguard}
     * @memberof IBizAuthguard
     */
    IBizAuthguard.iBizAuthguard = null;
    return IBizAuthguard;
}());
