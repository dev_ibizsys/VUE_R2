"use strict";
/**
 * IBizHttp net 对象
 * 调用 getInstance() 获取实例
 *
 * @class IBizHttp
 */
var IBizHttp = /** @class */ (function () {
    /**
     * Creates an instance of IBizHttp.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof IBizHttp
     */
    function IBizHttp() {
        /**
         * 是否正在加载中
         *
         * @type {boolean}
         * @memberof IBizHttp
         */
        this.isLoading = false;
        /**
         * 统计加载
         *
         * @type {number}
         * @memberof IBizHttp
         */
        this.loadingCount = 0;
    }
    /**
     * 获取 IBizHttp 单例对象
     *
     * @static
     * @returns {IBizHttp}
     * @memberof IBizHttp
     */
    IBizHttp.getInstance = function () {
        if (!IBizHttp.iBizHttp) {
            IBizHttp.iBizHttp = new IBizHttp();
        }
        return this.iBizHttp;
    };
    /**
     * post请求
     *
     * @param {string} url 请求路径
     * @param {*} [params={}] 请求参数
     * @returns {Subject<any>} 可订阅请求对象
     * @memberof IBizHttp
     */
    IBizHttp.prototype.post = function (url, params) {
        if (params === void 0) { params = {}; }
        var _this = this;
        var subject = new rxjs.Subject();
        var win = window;
        var iBizApp = win.getIBizApp();
        if (!Object.is(iBizApp.getAppData(), '')) {
            Object.assign(params, { srfappdata: iBizApp.getAppData() });
        }
        var _strParams = _this.transformationOpt(params);
        var _url = '';
        var BaseUrl = IBizEnvironment.BaseUrl;
        _url = "/" + BaseUrl + url;
        _this.beginLoading();
        axios({
            method: 'post',
            url: _url,
            data: _strParams,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8', 'Accept': 'application/json' },
            transformResponse: [function (data) {
                    var _data = null;
                    try {
                        _data = JSON.parse(JSON.parse(JSON.stringify(data)));
                    }
                    catch (error) {
                    }
                    return _data;
                }],
        }).then(function (data) {
            _this.endLoading();
            if (data && data.notlogin) {
                return;
            }
            if (data.ret !== 0) {
                data.failureType = 'CLIENT_INVALID';
                data.info = data.info ? data.info : '本地网络异常，请重试';
                data.info = data.errorMessage ? data.errorMessage : '本地网络异常，请重试';
            }
            IBizUtil.processResult(data);
            subject.next(data);
        }).catch(function (error) {
            _this.endLoading();
            subject.error(error);
        });
        return subject;
    };
    /**
     * post请求,不处理loading加载
     *
     * @param {string} url
     * @param {*} [params={}]
     * @returns {Subject<any>}
     * @memberof IBizHttp
     */
    IBizHttp.prototype.post2 = function (url, params) {
        if (params === void 0) { params = {}; }
        var subject = new rxjs.Subject();
        var iBizApp = IBizApp.getInstance();
        if (!Object.is(iBizApp.getAppData(), '')) {
            Object.assign(params, { srfappdata: iBizApp.getAppData() });
        }
        var _strParams = this.transformationOpt(params);
        var _url = '';
        var BaseUrl = IBizEnvironment.BaseUrl;
        _url = "/" + BaseUrl + url;
        axios({
            method: 'post',
            url: _url,
            data: _strParams,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8', 'Accept': 'application/json' },
            transformResponse: [function (data) {
                    var _data = null;
                    try {
                        _data = JSON.parse(JSON.parse(JSON.stringify(data)));
                    }
                    catch (error) {
                    }
                    return _data;
                }],
        }).then(function (data) {
            if (data && data.notlogin) {
                return;
            }
            if (data.ret !== 0) {
                data.failureType = 'CLIENT_INVALID';
                data.info = data.info ? data.info : '本地网络异常，请重试';
                data.info = data.errorMessage ? data.errorMessage : '本地网络异常，请重试';
            }
            IBizUtil.processResult(data);
            subject.next(data);
        }).catch(function (error) {
            subject.error(error);
        });
        return subject;
    };
    /**
     * get请求
     *
     * @param {string} url 请求路径
     * @param {*} [params={}] 请求参数
     * @returns {Subject<any>} 可订阅请求对象
     * @memberof IBizHttp
     */
    IBizHttp.prototype.get = function (url, params) {
        if (params === void 0) { params = {}; }
        var _this = this;
        var subject = new rxjs.Subject();
        var iBizApp = IBizApp.getInstance();
        if (!Object.is(iBizApp.getAppData(), '')) {
            Object.assign(params, { srfappdata: iBizApp.getAppData() });
        }
        if (Object.keys(params).length > 0) {
            var _strParams = _this.transformationOpt(params);
            url = url.indexOf('?') ? url + "&" + _strParams : url + "?&" + _strParams;
        }
        var _url = '';
        var BaseUrl = IBizEnvironment.BaseUrl;
        _url = "/" + BaseUrl + url;
        _this.beginLoading();
        axios.get(_url).then(function (response) {
            _this.endLoading();
            subject.next(response);
        }).catch(function (error) {
            _this.endLoading();
            subject.error(error);
        });
        return subject;
    };
    /**
     * 请求参数转义处理
     *
     * @private
     * @param {*} [opt={}]
     * @returns {string}
     * @memberof IBizHttp
     */
    IBizHttp.prototype.transformationOpt = function (opt) {
        if (opt === void 0) { opt = {}; }
        var params = {};
        var postData = [];
        Object.assign(params, opt);
        var keys = Object.keys(params);
        keys.forEach(function (key) {
            var val = params[key];
            if (val instanceof Array || val instanceof Object) {
                postData.push(key + "=" + encodeURIComponent(JSON.stringify(val)));
            }
            else {
                postData.push(key + "=" + encodeURIComponent(val));
            }
        });
        return postData.join('&');
    };
    /**
     * 开始加载
     *
     * @private
     * @memberof IBizHttp
     */
    IBizHttp.prototype.beginLoading = function () {
        var _this_1 = this;
        if (this.loadingCount === 0) {
            setTimeout(function () {
                _this_1.isLoading = true;
            });
        }
        this.loadingCount++;
    };
    /**
     * 加载结束
     *
     * @private
     * @memberof IBizHttp
     */
    IBizHttp.prototype.endLoading = function () {
        var _this_1 = this;
        if (this.loadingCount > 0) {
            this.loadingCount--;
        }
        if (this.loadingCount === 0) {
            setTimeout(function () {
                _this_1.isLoading = false;
            });
        }
    };
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {IBizHttp}
     * @memberof IBizHttp
     */
    IBizHttp.iBizHttp = null;
    return IBizHttp;
}());
