"use strict";
Vue.component('ibiz-grid-select', {
    template: "\n    <i-select v-model=\"value\" style=\"width:100%\" transfer label-in-value @on-change=\"onValueChange\">\n        <template v-for=\"(item, index) of items\">\n            <i-option :value=\"item.value\" :key=\"index\">{{ item.text }}</i-option>\n        </template>\n    </i-select>\n    ",
    data: function () {
        return {
            value: '',
            text: '',
            items: [],
            http: IBizHttp.getInstance(),
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        var _this = this;
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        if (Object.is(this.params.selectType, 'STATIC')) {
            this.value = this.params.value;
            var viewController = this.grid.getViewController();
            this.items = viewController.getCodeList(this.params.codelistId).data;
        }
        else if (Object.is(this.params.selectType, 'DYNAMIC')) {
            this.value = this.rowData[this.column.name];
            this.text = this.params.value;
            var param = {};
            param.srfreferdata = JSON.stringify(this.rowData);
            this.http.post(this.params.url, param).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.items = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    methods: {
        getValue: function () {
            if (Object.is(this.params.selectType, 'DYNAMIC')) {
                return this.text;
            }
            else {
                return this.value;
            }
        },
        onValueChange: function (item) {
            if (Object.is(this.params.selectType, 'DYNAMIC')) {
                this.text = item.label;
            }
            this.grid.colValueChange(this.column.name, item.value, this.rowData);
        }
    }
});
