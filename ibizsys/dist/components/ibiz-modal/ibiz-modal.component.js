"use strict";
Vue.component('ibiz-modal', {
    template: "\n        <modal class=\"ibiz-model\" :class-name=\"modalviewname\" v-model=\"showmodal\" @on-visible-change=\"onVisibleChange($event)\" :title=\"title\" :footer-hide=\"true\" :mask-closable=\"false\" :width=\"width\" :styles=\"{height: height}\">\n            <component v-if=\"showmodal\" :is=\"modalviewname\" :params=\"viewparam\" :viewType=\"'modalview'\" @close=\"close\" @dataChange=\"dataChange\"></component>\n        </modal>\n    ",
    props: ['key', 'params', 'index'],
    data: function () {
        var data = {
            showmodal: true,
            width: 0,
            title: '',
            height: '',
            modalviewname: '',
            subject: null,
            viewparam: {},
            tempResult: {},
        };
        var width = 600;
        if (window && window.innerWidth > 100) {
            if (window.innerWidth > 100) {
                width = window.innerWidth - 100;
            }
            else {
                width = window.innerWidth;
            }
        }
        Object.assign(data, { width: width });
        return data;
    },
    mounted: function () {
        this.modalviewname = this.params.modalviewname;
        if (this.params.subject) {
            this.subject = this.params.subject;
        }
        if (this.params.width && this.params.width !== 0) {
            this.width = this.params.width;
        }
        if (this.params.height && this.params.height !== 0) {
            this.height = this.params.height + "px";
        }
        if (this.params.title) {
            this.title = this.params.title;
        }
        if (this.params.viewparam) {
            Object.assign(this.viewparam, this.params.viewparam);
        }
    },
    methods: {
        close: function (result) {
            console.log(result);
            if (result && Object.keys(result).length > 0) {
                Object.assign(this.tempResult, JSON.parse(JSON.stringify(result)));
            }
            if (this.subject) {
                if (this.tempResult && Object.is(this.tempResult.ret, 'OK')) {
                    this.subject.next(this.tempResult);
                    this.subject = null;
                }
            }
            this.showmodal = false;
            // this.$emit("on-close", this.index)
        },
        dataChange: function (result) {
            console.log(result);
            this.tempResult = {};
            if (result && Object.keys(result).length > 0) {
                Object.assign(this.tempResult, JSON.parse(JSON.stringify(result)));
            }
        },
        onVisibleChange: function ($event) {
            console.log($event);
            if ($event) {
                return;
            }
            if (this.subject) {
                if (this.tempResult && Object.is(this.tempResult.ret, 'OK')) {
                    this.subject.next(this.tempResult);
                    this.subject = null;
                }
            }
        }
    }
});
