"use strict";
Vue.component('ibiz-group-menu', {
    template: "\n        <div class=\"ibiz-group-menu\">\n            <row>\n                <i-col span=\"24\" class=\"first-level\" v-for=\"first in menu.items\" v-if=\"first.items && first.items.length > 0\">\n                    <div class=\"first-level-title\">\n                        <h2>\n                            <span class=\"first-level-title-icon\">\n                                <i v-if=\"first.icon == ''\" :class=\"[first.iconcls == '' ? '' : first.iconcls ]\"></i>\n                                <img v-else :src=\"first.icon\"/>\n                            </span>\n                            <span class=\"first-level-title-text\">{{first.text}}</span>\n                        </h2>\n                    </div>\n                    <row>\n\t                    <i-col :xs=\"6\" v-for=\"second in first.items\" class=\"second-level\">\n\t\t                    <div @click=\"openMenu(second)\">\n                                <div class=\"second-level-icon\">\n                                    <i v-if=\"second.icon == ''\" :class=\"[second.iconcls == '' ? 'fa fa-cogs' : second.iconcls ]\"></i>\n                                    <img v-else :src=\"second.icon\"/>\n                                </div>\n\t\t                        <h4 class=\"second-level-text\">{{second.text}}</h4>\n\t\t                    </div>\n\t\t                </i-col>\n                    </row>\n                </i-col>\n            </row>\n        </div>\n    ",
    props: {
        menu: {
            type: Object
        },
        viewController: {
            type: Object
        }
    },
    data: function () {
        var data = {};
        return data;
    },
    mounted: function () {
    },
    methods: {
        // 打开菜单节点
        openMenu: function (second) {
            if (second === void 0) { second = {}; }
            if (this.menu) {
                this.menu.onSelectChange(second);
            }
        }
    }
});
