"use strict";
Vue.component("ibiz-file-upload", {
    template: "\n    <el-upload :disabled=\"field.disabled\" :file-list=\"files\" :action=\"uploadUrl\" :before-upload=\"beforeUpload\" :on-success=\"onSuccess\" :on-error=\"onError\" :before-remove=\"onRemove\" :on-preview=\"onDownload\">\n        <el-button size=\"small\" icon=\"el-icon-upload\">\u4E0A\u4F20</el-button>\n    </el-upload>\n    ",
    props: ['field', 'name'],
    data: function () {
        var data = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        var _this = this;
        var form = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        var editorParams = {};
        var uploadparams = '';
        var exportparams = '';
        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }
        var upload_keys = uploadparams.split(';');
        var export_keys = exportparams.split(';');
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach(function (name) {
                if (editorParams.customparams[name]) {
                    _this.custom_arr.push(name + "=" + editorParams.customparams[name]);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
            _this.upload_arr = [];
            _this.export_arr = [];
            upload_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.upload_arr.push(key + "=" + _value);
            });
            export_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.export_arr.push(key + "=" + _value);
            });
            var uploadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            var downloadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
            if (_this.upload_arr.length > 0 || _this.custom_arr.length > 0) {
                uploadUrl = uploadUrl + "?" + _this.upload_arr.join('&') + (_this.upload_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.uploadUrl = uploadUrl;
            if (_this.export_arr.length > 0 || _this.custom_arr.length > 0) {
                downloadUrl = downloadUrl + "?" + _this.export_arr.join('&') + (_this.export_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.downloadUrl = downloadUrl;
            _this.files.forEach(function (file) {
                var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = _url + "?fileid=" + file.id;
                if (_this.export_arr.length > 0) {
                    _url = _url + "&" + _this.export_arr.join('&');
                }
                if (_this.custom_arr.length > 0) {
                    _url = _url + "&" + _this.custom_arr.join('&');
                }
                file.url = _url;
            });
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach(function (file) {
                    var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = _url + "?fileid=" + file.id;
                    if (_this.export_arr.length > 0) {
                        _url = _url + "&" + _this.export_arr.join('&');
                    }
                    if (_this.custom_arr.length > 0) {
                        _url = _url + "&" + _this.custom_arr.join('&');
                    }
                    file.url = _url;
                });
            }
            else {
                this.files = [];
            }
        }
    },
    methods: {
        beforeUpload: function (file) {
            console.log('上传之前');
        },
        onSuccess: function (response, file, fileList) {
            var arr = [];
            arr = response.files.slice();
            this.files.forEach(function (f) {
                arr.push({ name: f.name, id: f.id });
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onError: function (error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove: function (file, fileList) {
            var arr = [];
            fileList.forEach(function (f) {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
        },
        onDownload: function (file) {
            window.open(file.url);
        }
    }
});
