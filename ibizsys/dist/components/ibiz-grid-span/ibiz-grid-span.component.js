"use strict";
Vue.component('ibiz-grid-span', {
    template: "\n    <i-input :value=\"value\" style=\"width: 100%\" disabled readonly/>\n    ",
    data: function () {
        return {
            value: ''
        };
    },
    created: function () {
        this.value = this.params.value;
    },
    methods: {
        getValue: function () {
            return this.value;
        }
    }
});
