"use strict";
Vue.component('ibiz-grid-datepicker', {
    template: "\n    <date-picker style=\"width: 100%;\" type=\"datetime\" :format=\"fmt\" v-model=\"value\" transfer @on-change=\"setValue\"></date-picker>\n    ",
    data: function () {
        return {
            value: '',
            fmt: '',
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.value = this.params.value;
        this.fmt = this.params.fmt;
    },
    methods: {
        getValue: function () {
            return this.value;
        },
        setValue: function (val) {
            this.grid.colValueChange(this.column.name, val, this.rowData);
        }
    }
});
