"use strict";
Vue.component('ibiz-range-picker', {
    template: "\n\t<div style=\"display: flex;\">\n\t\t<template v-for=\"(formitem,index) of formItems\">\n\t\t\t<div v-if=\"index > 0\" style=\"text-align: center;width: 50px;\">\u2014</div>\n\t\t\t<template v-if=\"format\">\n\t\t\t\t<date-picker v-if=\"format == 'yyyy-MM-dd HH:mm:ss'\" type=\"datetime\" :format=\"format\"\n\t\t\t\t\t:value=\"formitem.value\"\n\t\t\t\t\t:disabled=\"field.disabled\" transfer\n\t\t\t\t\t@on-change=\"formitem.setValue($event)\" style=\"flex-grow: 1;\">\n\t\t\t\t</date-picker>\n\t\t\t\t<date-picker v-else-if=\"format == 'yyyy-MM-dd' || format == 'YYYY-MM-DD'\" type=\"date\" format=\"yyyy-MM-dd\"\n\t\t\t\t\t:value=\"formitem.value\"\n\t\t\t\t\t:disabled=\"field.disabled\" transfer\n\t\t\t\t\t@on-change=\"formitem.setValue($event)\" style=\"flex-grow: 1;\">\n\t\t\t\t</date-picker>\n\t\t\t\t<time-picker v-else type=\"time\" :format=\"format\" :value=\"formitem.value\" \n\t\t\t\t\t:disabled=\"field.disabled\" transfer \n\t\t\t\t\t@on-change=\"formitem.setValue($event)\">\n\t\t\t\t</time-picker>\n\t\t\t</template>\n\t\t\t<template v-else>\n\t\t\t\t<i-input v-model=\"formitem.value\" :disabled=\"field.disabled\"></i-input>\n\t\t\t</template>\n\t\t</template>\n\t</div>\n\t",
    props: ['field', 'format', 'editorType', 'refFormItem'],
    data: function () {
        var data = {
            formItems: []
        };
        var formItems = [];
        Object.assign(data, { form: this.field.getForm() });
        formItems = this.refFormItem.split(';');
        formItems.forEach(function (item) {
            var field = data.form.findField(item);
            if (field) {
                data.formItems.push(field);
            }
        });
        return data;
    }
});
