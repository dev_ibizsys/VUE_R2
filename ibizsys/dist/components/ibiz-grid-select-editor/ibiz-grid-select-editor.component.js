"use strict";
Vue.component('ibiz-grid-select-editor', {
    template: "\n            <i-select :value=\"itemvalue\" :placeholder=\"placeHolder\" :disabled=\"disabled\" clearable :transfer=\"true\" @on-change=\"onChange($event)\">\n                <i-option v-for=\"(item, index) of codelist\" :value=\"item.value\" :key=\"item.value\">{{ item.text }}</i-option>\n            </i-select>\n        ",
    props: {
        itemvalue: {
            type: String
        },
        codelist: {
            type: Array,
            default: []
        },
        codelisttype: {
            type: String,
        },
        url: {
            type: String,
        },
        name: {
            type: String,
        },
        text: {
            type: String,
        },
        disabled: {
            type: Boolean
        },
        placeHolder: {
            type: String
        },
        grid: {
            type: Object
        },
        data: {
            type: Object
        }
    },
    data: function () {
        var data = {};
        return data;
    },
    mounted: function () {
        var _this = this;
        if (Object.is(this.codelisttype, 'DYNAMIC')) {
            var iBizHttp = IBizHttp.getInstance();
            iBizHttp.post(this.url, {}).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.codelist = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    methods: {
        onChange: function ($event) {
            if (!this.grid) {
                return;
            }
            this.grid.colValueChange(this.name, $event, this.data);
            if (Object.is(this.codelisttype, 'DYNAMIC')) {
                var code = this.codelist.filter(function (item) { return Object.is(item.value, $event); });
                var text = (code && code.length === 1) ? code[0].text : '';
                this.grid.colValueChange(this.text, text, this.data);
            }
        }
    }
});
