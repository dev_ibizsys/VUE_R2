"use strict";
Vue.component("ibiz-autocomplete", {
    template: "\n    <el-autocomplete size=\"small\" :value=\"field.value\" value-key=\"text\" clearable :fetch-suggestions=\"onSearch\" @select=\"onSelect\" @blur=\"onBlur\" @clear=\"onClear\" :disabled=\"field.disabled\" :trigger-on-focus=\"false\" style=\"width: 100%;\"></el-autocomplete>\n    ",
    props: ['field', 'name', 'width'],
    data: function () {
        var data = {
            http: IBizHttp.getInstance()
        };
        Object.assign(data, this.field.editorParams);
        Object.assign(data, { form: this.field.getForm() });
        return data;
    },
    methods: {
        onClear: function () {
            if (this.form) {
                var valueField = this.form.findField(this.valueItem);
                if (valueField) {
                    valueField.setValue('');
                }
                var itemField = this.form.findField(this.name);
                if (itemField) {
                    itemField.setValue('');
                }
            }
        },
        onBlur: function (e) {
            var val = e.target.value;
            if (!Object.is(val, this.field.value)) {
                this.onSelect({ text: val, value: '' });
            }
        },
        onSelect: function (item) {
            if (this.form) {
                var valueField = this.form.findField(this.valueItem);
                if (valueField) {
                    valueField.setValue(item.value);
                }
                var itemField = this.form.findField(this.name);
                if (itemField) {
                    itemField.setValue(item.text);
                }
            }
        },
        onSearch: function (query, callback) {
            var _this = this;
            var param = {
                srfaction: 'itemfetch',
                query: query,
            };
            if (_this.form) {
                Object.assign(param, { srfreferdata: JSON.stringify(_this.form.getActiveData()) });
            }
            _this.http.post(_this.url, param).subscribe(function (response) {
                var items = [];
                if (response.ret == 0) {
                    items = response.items;
                }
                callback(items);
            }, function () {
                callback([]);
            });
        }
    }
});
