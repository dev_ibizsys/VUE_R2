"use strict";
Vue.component("ibiz-card", {
    template: "\n    <div :class=\"classes\">\n        <div :class=\"headClasses\" v-if=\"showHead\">\n            <slot name=\"title\">\n                <p v-if=\"title\">\n                    <Icon v-if=\"icon\" :type=\"icon\"></Icon>\n                    <span>{{title}}</span>\n                </p>\n            </slot>\n            <div :class=\"extraClasses\" v-if=\"showExtra\"><slot name=\"extra\"></slot></div>\n            <div style=\"clear: both;\"></div>\n        </div>\n        <div :class=\"bodyClasses\" :style=\"bodyStyles\"><slot></slot></div>\n    </div>\n    ",
    props: {
        bordered: {
            type: Boolean,
            default: true
        },
        disHover: {
            type: Boolean,
            default: false
        },
        shadow: {
            type: Boolean,
            default: false
        },
        padding: {
            type: Number,
            default: 16
        },
        title: {
            type: String,
        },
        icon: {
            type: String,
        }
    },
    data: function () {
        return {
            showHead: true,
            showExtra: true,
            prefixCls: 'ivu-card'
        };
    },
    computed: {
        classes: function () {
            var _a;
            return [
                "" + this.prefixCls,
                (_a = {},
                    _a[this.prefixCls + "-bordered"] = this.bordered && !this.shadow,
                    _a[this.prefixCls + "-dis-hover"] = this.disHover || this.shadow,
                    _a[this.prefixCls + "-shadow"] = this.shadow,
                    _a)
            ];
        },
        headClasses: function () {
            return "ibiz-card-head";
        },
        extraClasses: function () {
            return this.prefixCls + "-extra";
        },
        bodyClasses: function () {
            return this.prefixCls + "-body";
        },
        bodyStyles: function () {
            if (this.padding !== 16) {
                return {
                    padding: this.padding + "px"
                };
            }
            else {
                return '';
            }
        }
    },
    mounted: function () {
        this.showHead = this.title || this.$slots.title !== undefined;
        this.showExtra = this.$slots.extra !== undefined;
    }
});
