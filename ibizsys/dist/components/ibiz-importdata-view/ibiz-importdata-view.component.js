"use strict";
Vue.component('ibiz-importdata-view', {
    template: "\n        <div class=\"ibiz-import-data\">\n            <el-upload accept=\"application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\" ref=\"upload\"\n                :action=\"url\" :on-change=\"handleChange\" :file-list=\"fileList\" :auto-upload=\"false\" :show-file-list=\"false\">\n                <el-button slot=\"trigger\" size=\"small\" type=\"primary\">\u9009\u53D6\u6587\u4EF6</el-button>\n                <el-button style=\"margin-left: 10px;\" size=\"small\" type=\"primary\" @click=\"submitUpload\">\u4E0A\u4F20\u5230\u670D\u52A1\u5668</el-button>\n                <div class=\"down-data-template\"><a :href=\"downUrl\">\u4E0B\u8F7D\u5BFC\u5165\u6570\u636E\u6A21\u677F</a></div>\n            </el-upload>\n            <div class=\"import-files\">\n                <ul class=\"el-upload-list el-upload-list--text\">\n                    <li class=\"el-upload-list__item is-ready\" v-for=\"file in result\">\n                        <div>\n                            <a class=\"el-upload-list__item-name\">\n                                <i class=\"el-icon-document\"></i>{{file.name}}\n                            </a>\n                        </div>\n                        <div class=\"file-info\">\n                            <a v-if=\"file.response.errorfileid && file.response.errorfileid != ''\" class=\"errorLink\">\u4E0B\u8F7D\u5BFC\u5165\u5931\u8D25\u6570\u636E\u6587\u4EF6</a>\n                            <div v-if=\"file.response.processinfo && file.response.processinfo != ''\" v-html=\"file.response.processinfo\"></div>\n                        </div>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    ",
    props: ['params'],
    data: function () {
        var data = {
            fileList: [],
            url: "/" + IBizEnvironment.BaseUrl + IBizEnvironment.UploadDEData,
            downUrl: "/" + IBizEnvironment.BaseUrl + IBizEnvironment.ExportExcel,
            result: []
        };
        if (this.params) {
            Object.assign(data, this.params);
        }
        data.url = !Object.is(data.dename, '') ? data.url + "?srfdeid=" + data.dename : data.url;
        data.downUrl = !Object.is(data.dename, '') ? data.downUrl + "?srfdeid=" + data.dename : data.downUrl;
        return data;
    },
    methods: {
        submitUpload: function () {
            this.$refs.upload.submit();
        },
        handleChange: function (file, fileList) {
            var _this = this;
            if (!file.response) {
                Object.assign(file, { response: {} });
            }
            this.result = [];
            setTimeout(function () {
                _this.result = fileList.slice();
            });
            fileList.some(function (item) {
                if (item.response && item.response.ret === 0) {
                    _this.$emit('dataChange', { ret: 'OK' });
                    return true;
                }
            });
        }
    }
});
