"use strict";
Vue.component("costom-text-column-filter", {
    template: "\n        <div class=\"ibiz-ag-filter text-filter\">\n            <input type=\"text\" :value=\"value\" @change=\"valueChanged($event)\"/>\n            <Icon :class=\"{selectClear: value != ''}\" type=\"ios-close-circle\" @click=\"clear\"/>\n        </div>\n    ",
    data: function () {
        return {
            value: ""
        };
    },
    beforeMount: function () {
    },
    mounted: function () { },
    methods: {
        valueChanged: function (event) {
            this.value = event.target.value;
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        onParentModelChanged: function (parentModel) {
            var _this = this;
            if (parentModel) {
                this.value = !parentModel ? "" : parentModel.filter;
            }
            else {
                if (this.value) {
                    setTimeout(function () {
                        _this.params.onFloatingFilterChanged({
                            model: _this.buildModel()
                        });
                    }, 50);
                }
            }
        },
        clear: function () {
            this.value = "";
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        buildModel: function () {
            return {
                filterType: 'text',
                type: 'contains',
                filter: this.value
            };
        }
    }
});
