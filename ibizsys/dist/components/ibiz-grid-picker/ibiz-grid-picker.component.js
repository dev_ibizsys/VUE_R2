"use strict";
Vue.component('ibiz-grid-picker', {
    template: "\n    <div class=\"ibiz-picker\">\n        <el-autocomplete class=\"text-value\" v-if=\"editorType != 'dropdown' && editorType != 'pickup-no-ac'\" value-key=\"text\" v-model=\"value\" size=\"small\" :fetch-suggestions=\"onSearch\" @select=\"onACSelect\" @blur=\"onBlur\" style=\"width:100%;\">\n            <template slot=\"suffix\">\n                <i v-if=\"value != ''\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                <i v-if=\"editorType != 'ac'\" class=\"el-icon-search\"  @click=\"openView\"></i>\n            </template>\n        </el-autocomplete>\n        <el-input class=\"text-value\" :value=\"value\" v-if=\"editorType == 'pickup-no-ac'\" readonly size=\"small\">\n            <template slot=\"suffix\">\n                <i v-if=\"value != ''\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                <i class=\"el-icon-search\"  @click=\"openView\"></i>\n            </template>\n        </el-input>\n        <el-select v-if=\"editorType == 'dropdown'\" remote :remote-method=\"onSearch\" :value=\"value\" size=\"small\" filterable @change=\"onSelect\" style=\"width:100%;\" clearable @clear=\"onClear\" @visible-change=\"onSelectOpen\">\n            <el-option v-for=\"(item, index) of items\" :value=\"item.value\" :label=\"item.text\" :disabled=\"item.disabled\"></el-option>\n        </el-select>\n        <span v-if=\"editorType == 'dropdown'\" style=\"position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;\">\n            <i v-if=\"!open\" class=\"el-icon-arrow-down\"></i>\n            <i v-if=\"open\" class=\"el-icon-arrow-up\"></i>\n        </span>\n    </div>\n    ",
    data: function () {
        return {
            value: '',
            editorType: '',
            iBizHttp: IBizHttp.getInstance(),
            iBizNotification: IBizNotification.getInstance(),
            items: [],
            editorParams: null,
            rowData: null,
            column: null,
            grid: null
        };
    },
    created: function () {
        this.value = this.params.value;
        this.grid = this.params.grid;
        this.rowData = this.params.node.data;
        this.column = this.params.column.colDef;
        this.editorType = this.params.editorType;
        this.editorParams = this.grid.getEditItem(this.column.name).editorParams;
    },
    methods: {
        getValue: function () {
            return this.value;
        },
        onSelectOpen: function (flag) {
            this.open = flag;
        },
        onBlur: function () {
            if (this.value != this.rowData[this.column.name]) {
                this.value = this.rowData[this.column.name];
            }
        },
        // 选中项设置
        onACSelect: function (item) {
            if (item === void 0) { item = {}; }
            if (this.grid) {
                if (this.editorParams && !Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                    this.grid.colValueChange(this.editorParams.valueItem, item.value, this.rowData);
                }
                this.grid.colValueChange(this.column.name, item.text, this.rowData);
            }
        },
        onSearch: function (query, func) {
            var _this = this;
            if (this.editorParams && (!this.editorParams.url || Object.is(this.editorParams.url, ''))) {
                return;
            }
            var param = { srfaction: 'itemfetch', query: query };
            var bcancel = this.fillPickupCondition(param);
            if (!bcancel) {
                this.iBizNotification.warning({ title: '异常', desc: '条件不满足' });
                return;
            }
            this.iBizHttp.post(this.editorParams.url, param).subscribe(function (data) {
                _this.items = data.items;
                if (typeof func == 'function') {
                    func(data.items);
                }
            });
        },
        onSelect: function (value) {
            var index = this.items.findIndex(function (item) { return Object.is(item.value, value); });
            if (index >= 0) {
                var item = this.items[index];
                this.onACSelect(item);
            }
        },
        // 清空选中数据
        onClear: function () {
            if (this.grid) {
                if (this.editorParams && !Object.is(this.editorParams.valueItem, '') && this.rowData.hasOwnProperty(this.editorParams.valueItem)) {
                    this.grid.colValueChange(this.editorParams.valueItem, '', this.rowData);
                }
                this.grid.colValueChange(this.column.name, '', this.rowData);
            }
        },
        //  填充条件
        fillPickupCondition: function (arg) {
            if (this.grid) {
                if (this.editorParams && this.editorParams.itemParam && this.editorParams.itemParam.fetchcond) {
                    var fetchparam = {};
                    var fetchCond = this.editorParams.itemParam.fetchcond;
                    if (fetchCond) {
                        for (var cond in fetchCond) {
                            var value = this.rowData[fetchCond[cond]];
                            if (!value) {
                                this.iBizNotification.error('操作失败', '未能找到当前表格数据项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                Object.assign(arg, { srfreferitem: this.column.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.rowData) });
                return true;
            }
            else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        // 打开选择视图
        openView: function () {
            var _this = this;
            var view = { viewparam: {} };
            var viewController = null;
            if (this.grid) {
                viewController = this.grid.getViewController();
            }
            if (this.rowData && Object.keys(this.rowData).length > 0) {
                Object.assign(view.viewparam, { srfkey: this.rowData['srfkey'] });
            }
            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }
            var bcancel = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }
            if (this.editorParams && this.editorParams.pickupView && Object.keys(this.editorParams.pickupView).length > 0) {
                var subject = new rxjs.Subject();
                Object.assign(view, this.editorParams.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe(function (result) {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    var item = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }
                    if (_this.grid) {
                        if (!Object.is(_this.editorParams.valueItem, '') && _this.rowData.hasOwnProperty(_this.editorParams.valueItem)) {
                            _this.grid.colValueChange(_this.editorParams.valueItem, item.srfkey, _this.rowData);
                        }
                        _this.value = item.srfmajortext;
                        _this.grid.colValueChange(_this.column.name, item.srfmajortext, _this.rowData);
                    }
                });
            }
        }
    }
});
