"use strict";
Vue.component('ibiz-exp-bar', {
    template: "\n        <el-menu class=\"ibiz-exp-bar\" @open=\"subMenuSelect\" @close=\"subMenuSelect\" @select=\"onSelect\" :default-active=\"ctrl.selectItem.id\" :default-openeds=\"ctrl.expandItems\">\n            <template v-for=\"(item0, index0) in ctrl.items\">\n\n                <!---  \u4E00\u7EA7\u83DC\u5355\u6709\u5B50\u9879 begin  --->\n                <template v-if=\"item0.items && item0.items.length > 0\">\n                    <el-submenu :index=\"item0.id\" v-show=\"item0.show\" :disabled=\"item0.disabled\">\n                        <template slot=\"title\">\n                            <img v-if=\"item0.icon && item0.icon != ''\" :src=\"item0.icon\"/>\n                            <i v-else class=\"ibiz-menu-icon\" :class=\"[item0.iconcls == '' ? 'el-icon-share' : item0.iconcls ]\" aria-hidden=\"true\"></i>\n                            <span :class=\"ctrl.selectItem.id == item0.id ? 'active-subemnu': ''\">{{ item0.text }}</span>\n                            <span>&nbsp;&nbsp;<badge :count=\"item0.counterdata\" :class-name=\"item0.class\"></badge></span>\n                        </template>\n                        <template v-for=\"(item1, index1) in item0.items\">\n\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u6709\u5B50\u9879 begin  --->\n                            <template v-if=\"item1.items && item1.items.length > 0\">\n                                <el-submenu :index=\"item1.id\" v-show=\"item1.show\" :disabled=\"item1.disabled\">\n                                    <template slot=\"title\">\n                                        <img v-if=\"item1.icon && item1.icon != ''\" :src=\"item1.icon\"/>\n                                        <i v-else class=\"ibiz-menu-icon\" v-if=\"item1.iconcls != ''\" :class=\"item1.iconcls\" aria-hidden=\"true\"></i>\n                                        <span :class=\"ctrl.selectItem.id == item1.id ? 'active-subemnu': ''\">{{ item1.text }}</span>\n                                        <span>&nbsp;&nbsp;<badge :count=\"item1.counterdata\" :class-name=\"item1.class\"></badge></span>\n                                    </template>\n\n                                    <!---  \u4E09\u7EA7\u83DC\u5355 begin  --->\n                                    <template v-for=\"(item2, index2) in item1.items\">\n                                        <el-menu-item :index=\"item2.id\" v-show=\"item2.show\" :disabled=\"item2.disabled\">\n                                            <img v-if=\"item2.icon && item2.icon != ''\" :src=\"item2.icon\"/>\n                                            <i v-else class=\"ibiz-menu-icon\" v-if=\"item2.iconcls != ''\" :class=\"item2.iconcls\" aria-hidden=\"true\"></i>\n                                            <span>{{ item2.text }}</span>\n                                            <span>&nbsp;&nbsp;<badge :count=\"item2.counterdata\" :class-name=\"item2.class\"></badge></span>\n                                        </el-menu-item>\n                                    </template>\n                                    <!---  \u4E09\u7EA7\u83DC\u5355\u6709 begin  --->\n\n                                </el-submenu>\n                            </template>\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u6709\u5B50\u9879 end  --->\n\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 begin  --->\n                            <template v-else>\n                                <el-menu-item :index=\"item1.id\" v-show=\"item1.show\" :disabled=\"item1.disabled\">\n                                    <img v-if=\"item1.icon && item1.icon != ''\" :src=\"item1.icon\"/>\n                                    <i v-else class=\"ibiz-menu-icon\" v-if=\"item1.iconcls != ''\" :class=\"item1.iconcls\" aria-hidden=\"true\"></i>\n                                    <span>{{ item1.text }}</span>\n                                    <span>&nbsp;&nbsp;<badge :count=\"item1.counterdata\" :class-name=\"item1.class\"></badge></span>\n                                </el-menu-item>\n                            </template>\n                            <!---  \u4E8C\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 end  --->\n\n                        </template>\n                    </el-submenu>\n                </template>\n                <!---  \u4E00\u7EA7\u83DC\u5355\u6709\u5B50\u9879 end  --->\n\n                <!---  \u4E00\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 begin  --->\n                <template v-else>\n                    <el-menu-item :index=\"item0.id\" v-show=\"item0.show\" :disabled=\"item0.disabled\">\n                        <img v-if=\"item0.icon && item0.icon != ''\" :src=\"item0.icon\"/>\n                        <i v-else class=\"ibiz-menu-icon\" :class=\"[item0.iconcls == '' ? 'el-icon-share' : item0.iconcls ]\" aria-hidden=\"true\"></i>\n                        <span>{{ item0.text }}</span>\n                        <span>&nbsp;&nbsp;<badge :count=\"item0.counterdata\" :class-name=\"item0.class\"></badge></span>\n                    </el-menu-item>\n                </template>\n                <!---  \u4E00\u7EA7\u83DC\u5355\u65E0\u5B50\u9879 end  --->\n\n            </template>\n        </el-menu>\n    ",
    props: ['ctrl', 'viewController'],
    data: function () {
        var data = { opendata: [] };
        return data;
    },
    mounted: function () {
    },
    methods: {
        // 获取选中项数据
        getItem: function (items, id) {
            var _this = this;
            var data = {};
            items.some(function (_item) {
                if (Object.is(id, _item.id)) {
                    Object.assign(data, _item);
                    return true;
                }
                if (_item.items && _item.items.length > 0) {
                    var subItem = _this.getItem(_item.items, id);
                    if (Object.keys(subItem).length > 0) {
                        Object.assign(data, subItem);
                        return true;
                    }
                }
            });
            return data;
        },
        // 菜单项选中
        onSelect: function (name) {
            var _this = this;
            var _data = _this.getItem(_this.ctrl.items, name);
            _this.ctrl.selection(_data);
        },
        // 菜单节点选中
        subMenuSelect: function (index, indexpath) {
            var _this = this;
            var _data = _this.getItem(_this.ctrl.items, index);
            _this.ctrl.expandedAndSelectSubMenu(_data);
        }
    }
});
