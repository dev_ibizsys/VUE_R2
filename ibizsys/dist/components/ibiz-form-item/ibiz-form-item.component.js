"use strict";
Vue.component('ibiz-form-item', {
    template: "\n        <div>\n            <form-item :class=\"item.hasError ? 'ivu-form-item-error' :''\" :label-width=\"item.showCaption ? item.labelWidth : 0\" :required=\"!item.allowEmpty\">\n                <span slot=\"label\" class=\"\" v-if=\"item.labelWidth === 0 ? false : item.showCaption\">{{ item.emptyCaption ? '' : item.caption }}</span>\n                <slot></slot>\n            </form-item>\n        </div>\n    ",
    props: ['form', 'item', 'name'],
    data: function () {
        var data = {};
        return data;
    }
});
