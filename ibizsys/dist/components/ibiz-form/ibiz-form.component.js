"use strict";
Vue.component('ibiz-form', {
    template: "\n        <div style=\"height: 100%;\">\n            <i-form :model=\"form\" style=\"height: 100%;\">\n                <row style=\"height: 100%;\">\n                    <slot :scope=\"fields\"></slot>\n                </row>\n            </i-form>\n        </div>\n    ",
    props: ['form'],
    data: function () {
        var data = { fields: this.form.fields };
        return data;
    }
});
