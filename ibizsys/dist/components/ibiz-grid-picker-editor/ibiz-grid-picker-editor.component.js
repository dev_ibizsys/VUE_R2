"use strict";
Vue.component('ibiz-grid-picker-editor', {
    template: "\n            <div class=\"ibiz-picker ibiz-grid-picker-editor\">\n            <el-autocomplete class=\"text-value\" v-if=\"editorType != 'dropdown' && editorType != 'pickup-no-ac'\" value-key=\"text\" :disabled=\"disabled\" v-model=\"value\" size=\"small\" :fetch-suggestions=\"onSearch\" @select=\"onACSelect\" @blur=\"onBlur\" style=\"width:100%;\">\n                <template slot=\"suffix\">\n                    <i v-if=\"value != '' && !disabled\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                    <i v-if=\"editorType != 'ac'\" class=\"el-icon-search\"  @click=\"openView\"></i>\n                </template>\n            </el-autocomplete>\n            <el-input class=\"text-value\" :value=\"value\" v-if=\"editorType == 'pickup-no-ac'\" readonly size=\"small\" :disabled=\"disabled\">\n                <template slot=\"suffix\">\n                    <i v-if=\"value != '' && !disabled\" class=\"el-icon-circle-close\" @click=\"onClear\"></i>\n                    <i class=\"el-icon-search\"  @click=\"openView\"></i>\n                </template>\n            </el-input>\n            <el-select v-if=\"editorType == 'dropdown'\" remote :remote-method=\"onSearch\" :value=\"value\" size=\"small\" filterable @change=\"onSelect\" :disabled=\"disabled\" style=\"width:100%;\" clearable @clear=\"onClear\" @visible-change=\"onSelectOpen\">\n                <el-option v-for=\"(item, index) of items\" :value=\"item.value\" :label=\"item.text\" :disabled=\"item.disabled\"></el-option>\n            </el-select>\n            <span v-if=\"editorType == 'dropdown'\" style=\"position: absolute;right: 5px;color: #c0c4cc;top: 0;font-size: 13px;\">\n                <i v-if=\"!open\" class=\"el-icon-arrow-down\"></i>\n                <i v-if=\"open\" class=\"el-icon-arrow-up\"></i>\n            </span>\n        </div>\n    ",
    props: {
        itemvalue: {
            type: String
        },
        editorType: {
            type: String,
        },
        editorParams: {
            type: Object
        },
        name: {
            type: String,
        },
        text: {
            type: String,
        },
        disabled: {
            type: Boolean
        },
        placeHolder: {
            type: String
        },
        grid: {
            type: Object
        },
        data: {
            type: Object
        }
    },
    data: function () {
        var data = {
            iBizHttp: IBizHttp.getInstance(),
            iBizNotification: IBizNotification.getInstance(),
            value: '',
            open: false,
            items: []
        };
        data.value = this.itemvalue;
        Object.assign(data, this.editorParams);
        return data;
    },
    mounted: function () {
        if (this.editorType == 'dropdown') {
            this.onSearch('');
        }
    },
    watch: {
        'itemvalue': function (newVal, oldVal) {
            this.value = newVal;
        }
    },
    methods: {
        onSelectOpen: function (flag) {
            this.open = flag;
        },
        onBlur: function () {
            if (this.field && this.value != this.field.value) {
                if (this.forceSelection) {
                    this.value = this.field.value;
                }
                else {
                    this.onACSelect({ text: this.value, value: '' });
                }
            }
        },
        //  填充条件
        fillPickupCondition: function (arg) {
            if (this.grid) {
                if (!this.data) {
                    this.iBizNotification.error('操作失败', '未能找到当前数据，无法继续操作');
                    return false;
                }
                if (this.itemParam && this.itemParam.fetchcond) {
                    var fetchparam = {};
                    var fetchCond = this.itemParam.fetchcond;
                    if (fetchCond) {
                        for (var cond in fetchCond) {
                            var value = this.data[fetchCond[cond]];
                            if (!value) {
                                this.iBizNotification.error('操作失败', '未能找到当前表格数据项' + fetchCond[cond] + '，无法继续操作');
                                return false;
                            }
                            fetchparam[cond] = value;
                        }
                    }
                    Object.assign(arg, { srffetchcond: JSON.stringify(fetchparam) });
                }
                Object.assign(arg, { srfreferitem: this.name });
                Object.assign(arg, { srfreferdata: JSON.stringify(this.data) });
                return true;
            }
            else {
                this.iBizNotification.error('操作失败', '部件对象异常');
                return false;
            }
        },
        onSearch: function (query, func) {
            var _this = this;
            if (!this.url || Object.is(this.url, '')) {
                return;
            }
            var param = { srfaction: 'itemfetch', query: query };
            var bcancel = this.fillPickupCondition(param);
            if (!bcancel) {
                this.iBizNotification.warning({ title: '异常', desc: '条件不满足' });
                return;
            }
            this.iBizHttp.post(this.url, param).subscribe(function (data) {
                _this.items = data.items;
                if (typeof func == 'function') {
                    func(data.items);
                }
            });
        },
        onSelect: function (value) {
            var index = this.items.findIndex(function (item) { return Object.is(item.value, value); });
            if (index >= 0) {
                var item = this.items[index];
                this.onACSelect(item);
            }
        },
        // 清空选中数据
        onClear: function () {
            if (this.grid) {
                if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                    this.grid.colValueChange(this.valueItem, '', this.data);
                }
                this.grid.colValueChange(this.name, '', this.data);
            }
        },
        // 选中项设置
        onACSelect: function (item) {
            if (item === void 0) { item = {}; }
            if (this.grid) {
                if (!Object.is(this.valueItem, '') && this.data.hasOwnProperty(this.valueItem)) {
                    this.grid.colValueChange(this.valueItem, item.value, this.data);
                }
                this.grid.colValueChange(this.name, item.text, this.data);
            }
        },
        // 打开选择视图
        openView: function () {
            var _this = this;
            if (this.disabled) {
                return;
            }
            var view = { viewparam: {} };
            var viewController = null;
            if (this.grid) {
                viewController = this.grid.getViewController();
            }
            if (this.data && Object.keys(this.data).length > 0) {
                Object.assign(view.viewparam, { srfkey: this.data['srfkey'] });
            }
            if (viewController) {
                Object.assign(view.viewparam, viewController.getViewParam());
            }
            var bcancel = this.fillPickupCondition(view.viewparam);
            if (!bcancel) {
                this.iBizNotification.warning('异常', '条件不满足');
                return;
            }
            if (this.pickupView && Object.keys(this.pickupView).length > 0) {
                var subject = new rxjs.Subject();
                Object.assign(view, this.pickupView, { subject: subject });
                this.$root.addModal(view);
                subject.subscribe(function (result) {
                    if (!result || !Object.is(result.ret, 'OK')) {
                        return;
                    }
                    var item = {};
                    if (result.selections && Array.isArray(result.selections)) {
                        Object.assign(item, result.selections[0]);
                    }
                    if (_this.grid) {
                        if (!Object.is(_this.valueItem, '') && _this.data.hasOwnProperty(_this.valueItem)) {
                            _this.grid.colValueChange(_this.valueItem, item.srfkey, _this.data);
                        }
                        _this.grid.colValueChange(_this.name, item.srfmajortext, _this.data);
                    }
                });
            }
        }
    }
});
