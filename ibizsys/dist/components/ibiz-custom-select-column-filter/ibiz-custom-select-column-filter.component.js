"use strict";
Vue.component("costom-select-column-filter", {
    template: "\n    \t<div class=\"ibiz-ag-filter select-filter\">\n\t        <select ref=\"select\" @change=\"valueChanged\">\n\t            <option style='display: none'></option>\n\t            <template v-for=\"item of items\">\n\t            \t<option :value=\"item.value\">{{ item.text }}</option>\n\t            </template>\n\t        </select>\n\t        <Icon :class=\"{selectClear: value != ''}\" type=\"ios-close-circle\" @click=\"clear\"/>\n        </div>\n    ",
    data: function () {
        return {
            value: '',
            items: [],
            http: IBizHttp.getInstance(),
            grid: null
        };
    },
    beforeMount: function () {
        var _this = this;
        this.grid = this.params.grid;
        if (Object.is(this.params.selectType, 'STATIC')) {
            var viewController = this.grid.getViewController();
            this.items = viewController.getCodeList(this.params.codelistId).data;
        }
        else if (Object.is(this.params.selectType, 'DYNAMIC')) {
            var param = {};
            this.http.post(this.params.url, param).subscribe(function (success) {
                if (success.ret === 0) {
                    _this.items = success.items.slice();
                }
            }, function (error) {
                console.log(error);
            });
        }
    },
    mounted: function () {
    },
    methods: {
        valueChanged: function (event) {
            this.value = event.target.value;
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        clear: function () {
            this.value = "";
            this.$refs.select.value = "";
            this.params.onFloatingFilterChanged({
                model: this.buildModel()
            });
        },
        onParentModelChanged: function (parentModel) {
            var _this_1 = this;
            if (parentModel) {
                this.value = !parentModel ? "" : parentModel.filter;
            }
            else {
                if (this.value) {
                    setTimeout(function () {
                        _this_1.params.onFloatingFilterChanged({
                            model: _this_1.buildModel()
                        });
                    }, 50);
                }
            }
        },
        buildModel: function () {
            return {
                filterType: 'text',
                type: 'contains',
                filter: this.value
            };
        }
    }
});
