"use strict";
Vue.component('ibiz-form-editor', {
    template: "\n        <div class=\"ibiz-form-editor\">\n            <slot></slot>\n            <div class=\"editor-tipinfo\" :class=\"item.hasError ? 'show-tip-info': 'hide-tip-into'\" >\n                <tooltip max-width=\"200\" transfer=\"true\" theme=\"light\">\n                    <icon type=\"ios-warning-outline\"></icon>\n                    <div slot=\"content\">\n                        <p style=\"color:red;\">{{item.errorInfo}}</p>\n                    </div>\n                </tooltip>\n            </div>\n        </div>\n    ",
    props: ['item'],
    data: function () {
        var data = {};
        return data;
    }
});
