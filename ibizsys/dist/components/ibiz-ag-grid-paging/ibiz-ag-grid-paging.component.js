"use strict";
Vue.component('ibiz-ag-gird-paging', {
    template: "\n\t\t<page @on-change=\"onChange\" @on-page-size-change=\"onPageSizeChange\" :transfer=\"true\" :total=\"grid.totalrow\"\n            show-sizer :page-size=\"20\" :page-size-opts=\"[10, 20, 30, 40, 50, 60, 70, 80, 90, 100]\" show-elevator show-total>\n            <span><span class=\"page-refresh\"><i-button icon=\"md-refresh\" title=\"\u5237\u65B0\" @click=\"onRefresh\"></i-button></span>\u663E\u793A {{ (grid.curPage - 1)*grid.limit + 1}} - {{grid.totalrow > grid.curPage*grid.limit ? grid.curPage*grid.limit : grid.totalrow}} \u6761\uFF0C\u5171 {{grid.totalrow}} \u6761</span>\n        </page>\n\t",
    props: ['agGrid', 'grid'],
    data: function () {
        return {};
    },
    methods: {
        'onPageSizeChange': function (data) {
            this.$emit('on-page-size-change', data);
        },
        'onChange': function (data) {
            this.$emit('on-change', data);
        },
        'onRefresh': function () {
            this.$emit('refresh');
        }
    }
});
