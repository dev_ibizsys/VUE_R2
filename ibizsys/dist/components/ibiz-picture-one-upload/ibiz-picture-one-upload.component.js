"use strict";
Vue.component("ibiz-picture-one-upload", {
    template: "\n    <div class=\"ibiz-picture-upload\">\n        <ul class=\"el-upload-list el-upload-list--picture-card\">\n            <template v-for=\"(file,index) of files\">\n                <li :key=\"index\" class=\"el-upload-list__item is-success\" :style=\"sytleObj\">\n                    <img :src=\"file.url\" class=\"el-upload-list__item-thumbnail\" :style=\"sytleObj\" style=\"min-height:100px;min-width:100px;\">\n                    <a class=\"el-upload-list__item-name\">\n                        <i class=\"el-icon-document\"></i> {{ file.name }}\n                    </a>\n                    <i class=\"el-icon-close\"></i>\n                    <label class=\"el-upload-list__item-status-label\">\n                        <i class=\"el-icon-upload-success el-icon-check\"></i>\n                    </label>\n                    <span class=\"el-upload-list__item-actions\">\n                        <span class=\"el-upload-list__item-preview\">\n                            <i class=\"el-icon-zoom-in\" @click=\"onPreview(file)\"></i>\n                        </span>\n                        <span class=\"el-upload-list__item-download\">\n                            <i class=\"el-icon-download\" @click=\"onDownload(file)\"></i>\n                        </span>\n                        <span v-if=\"!field.disabled\" class=\"el-upload-list__item-delete\">\n                            <i class=\"el-icon-delete\" @click=\"onRemove(file)\"></i>\n                        </span>\n                    </span>\n                </li>\n            </template>\n        </ul>\n        <el-upload v-if=\"showUpload\" :show-file-list=\"false\" list-type=\"picture-card\" :file-list=\"files\" :action=\"uploadUrl\" :before-upload=\"beforeUpload\" :on-success=\"onSuccess\" :on-error=\"onError\">\n            <i class=\"el-icon-plus\"></i>\n        </el-upload>\n        <Modal v-model=\"dialogVisible\" footer-hide>\n            <img width=\"100%\" :src=\"dialogImageUrl\" alt=\"\">\n        </Modal>\n    <div>\n    ",
    props: ['field', 'name', 'width', 'height'],
    data: function () {
        var data = {
            uploadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile,
            downloadUrl: '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile,
            files: [],
            showUpload: true,
            dialogVisible: false,
            dialogImageUrl: '',
            sytleObj: {
                width: this.width ? this.width + "px !important" : '100%',
                height: this.height ? this.height + "px !important" : '100%',
            },
            upload_arr: [],
            export_arr: [],
            custom_arr: [],
        };
        return data;
    },
    mounted: function () {
        var _this = this;
        var form = this.field.getForm();
        if (!form) {
            return;
        }
        if (!this.field.editorParams || Object.keys(this.field.editorParams).length === 0) {
            return;
        }
        var editorParams = {};
        var uploadparams = '';
        var exportparams = '';
        Object.assign(editorParams, this.field.editorParams);
        if (editorParams.uploadparams && !Object.is(editorParams.uploadparams, '')) {
            uploadparams = editorParams.uploadparams;
        }
        if (editorParams.exportparams && !Object.is(editorParams.exportparams, '')) {
            exportparams = editorParams.exportparams;
        }
        var upload_keys = uploadparams.split(';');
        var export_keys = exportparams.split(';');
        var custom_arr = [];
        if (editorParams.customparams && !Object.is(editorParams.customparams, '')) {
            Object.keys(editorParams.customparams).forEach(function (name) {
                if (editorParams.customparams[name]) {
                    custom_arr.push(name + "=" + editorParams.customparams[name]);
                }
            });
        }
        form.on(IBizForm.FORMFIELDCHANGED).subscribe(function (data) {
            _this.upload_arr = [];
            _this.export_arr = [];
            upload_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.upload_arr.push(key + "=" + _value);
            });
            export_keys.forEach(function (key) {
                var _field = form.findField(key);
                if (!_field) {
                    return;
                }
                var _value = _field.getValue();
                if (!_value || Object.is(_value, '')) {
                    return;
                }
                _this.export_arr.push(key + "=" + _value);
            });
            var uploadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.UploadFile;
            var downloadUrl = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
            if (_this.upload_arr.length > 0 || _this.custom_arr.length > 0) {
                uploadUrl = uploadUrl + "?" + _this.upload_arr.join('&') + (_this.upload_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.uploadUrl = uploadUrl;
            if (_this.export_arr.length > 0 || _this.custom_arr.length > 0) {
                downloadUrl = downloadUrl + "?" + _this.export_arr.join('&') + (_this.export_arr.length > 0 ? '&' : '') + _this.custom_arr.join('&');
            }
            _this.downloadUrl = downloadUrl;
            _this.files.forEach(function (file) {
                var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                _url = _url + "?fileid=" + file.id;
                if (_this.export_arr.length > 0) {
                    _url = _url + "&" + _this.export_arr.join('&');
                }
                if (_this.custom_arr.length > 0) {
                    _url = _url + "&" + _this.custom_arr.join('&');
                }
                file.url = _url;
            });
            _this.showUpload = !_this.field.disabled && _this.files.length !== 1;
        });
    },
    watch: {
        'field.value': function (newVal, oldVal) {
            var _this = this;
            if (newVal) {
                this.files = JSON.parse(newVal);
                this.files.forEach(function (file) {
                    var _url = '/' + IBizEnvironment.BaseUrl + IBizEnvironment.ExportFile;
                    _url = _url + "?fileid=" + file.id;
                    if (_this.export_arr.length > 0) {
                        _url = _url + "&" + _this.export_arr.join('&');
                    }
                    if (_this.custom_arr.length > 0) {
                        _url = _url + "&" + _this.custom_arr.join('&');
                    }
                    file.url = _url;
                });
            }
            else {
                this.files = [];
            }
            this.showUpload = !this.field.disabled && this.files.length !== 1;
        }
    },
    methods: {
        onPreview: function (file) {
            this.dialogImageUrl = file.url;
            this.dialogVisible = true;
        },
        beforeUpload: function (file) {
            console.log('上传之前');
        },
        onSuccess: function (response, file, fileList) {
            var arr = [];
            arr = response.files.slice();
            this.files.forEach(function (f) {
                arr.push({ name: f.name, id: f.id });
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
            this.showUpload = !this.field.disabled && this.files.length !== 1;
        },
        onError: function (error, file, fileList) {
            this.$Notice.error({ title: '上传失败' });
        },
        onRemove: function (file) {
            if (this.field && this.field.disabled) {
                return;
            }
            var arr = [];
            this.files.forEach(function (f) {
                if (f.id != file.id) {
                    arr.push({ name: f.name, id: f.id });
                }
            });
            var value = arr.length > 0 ? JSON.stringify(arr) : '';
            if (this.field) {
                this.field.setValue(value);
            }
            this.showUpload = !this.field.disabled && this.files.length !== 1;
        },
        onDownload: function (file) {
            window.open(file.url);
        }
    }
});
